.class final Lcom/google/android/gms/drive/a/a/d;
.super Ljava/util/AbstractQueue;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/BlockingQueue;


# instance fields
.field private final a:Lcom/google/android/gms/drive/a/a/m;

.field private final b:Lcom/google/android/gms/drive/g/aw;

.field private final c:Lcom/google/android/gms/drive/database/r;

.field private final d:Lcom/google/android/gms/drive/g/n;

.field private final e:Lcom/google/android/gms/drive/a/a/j;

.field private final f:Lcom/google/android/gms/drive/b/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/m;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/util/AbstractQueue;-><init>()V

    .line 39
    iput-object p6, p0, Lcom/google/android/gms/drive/a/a/d;->a:Lcom/google/android/gms/drive/a/a/m;

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/drive/a/a/d;->b:Lcom/google/android/gms/drive/g/aw;

    .line 41
    iput-object p2, p0, Lcom/google/android/gms/drive/a/a/d;->c:Lcom/google/android/gms/drive/database/r;

    .line 42
    iput-object p3, p0, Lcom/google/android/gms/drive/a/a/d;->d:Lcom/google/android/gms/drive/g/n;

    .line 43
    iput-object p4, p0, Lcom/google/android/gms/drive/a/a/d;->e:Lcom/google/android/gms/drive/a/a/j;

    .line 44
    iput-object p5, p0, Lcom/google/android/gms/drive/a/a/d;->f:Lcom/google/android/gms/drive/b/d;

    .line 45
    return-void
.end method

.method private a()Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 74
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/a/a/d;->b()Ljava/lang/Runnable;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Ljava/lang/Runnable;
    .locals 8

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/d;->a:Lcom/google/android/gms/drive/a/a/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/m;->b()Lcom/google/android/gms/drive/a/a/k;

    move-result-object v7

    .line 89
    if-nez v7, :cond_0

    .line 91
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/a/a/e;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/d;->b:Lcom/google/android/gms/drive/g/aw;

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/d;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/d;->d:Lcom/google/android/gms/drive/g/n;

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/d;->e:Lcom/google/android/gms/drive/a/a/j;

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/d;->f:Lcom/google/android/gms/drive/b/d;

    iget-object v6, p0, Lcom/google/android/gms/drive/a/a/d;->a:Lcom/google/android/gms/drive/a/a/m;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/a/a/e;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/m;Lcom/google/android/gms/drive/a/a/k;)V

    goto :goto_0
.end method


# virtual methods
.method public final drainTo(Ljava/util/Collection;)I
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    return v0
.end method

.method public final drainTo(Ljava/util/Collection;I)I
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    return v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic offer(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public final bridge synthetic offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public final bridge synthetic peek()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return-object v0
.end method

.method public final synthetic poll()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/drive/a/a/d;->a()Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/drive/a/a/d;->b()Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic put(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 21
    return-void
.end method

.method public final remainingCapacity()I
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method public final size()I
    .locals 1

    .prologue
    .line 120
    const/4 v0, 0x0

    return v0
.end method

.method public final synthetic take()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/drive/a/a/d;->b()Ljava/lang/Runnable;

    move-result-object v0

    return-object v0
.end method
