.class final Lcom/google/android/gms/notifications/k;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/c/e/a/a/a/a/c;)I
    .locals 2

    .prologue
    .line 51
    if-eqz p0, :cond_2

    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    if-eqz v0, :cond_2

    .line 52
    sget-object v0, Lcom/google/android/gms/notifications/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget v0, Lcom/google/android/gms/h;->bJ:I

    .line 67
    :goto_0
    return v0

    .line 55
    :cond_0
    sget-object v0, Lcom/google/android/gms/notifications/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    sget v0, Lcom/google/android/gms/h;->bI:I

    goto :goto_0

    .line 58
    :cond_1
    sget-object v0, Lcom/google/android/gms/notifications/a/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->d:Lcom/google/c/e/a/a/a/a/f;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/f;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 59
    sget v0, Lcom/google/android/gms/h;->bH:I

    goto :goto_0

    .line 62
    :cond_2
    if-eqz p0, :cond_3

    iget-object v0, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/notifications/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v1, v1, Lcom/google/c/e/a/a/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 64
    sget v0, Lcom/google/android/gms/h;->bJ:I

    goto :goto_0

    .line 67
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/c/e/a/a/a/a/c;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 34
    const-string v1, "gms.gnots.payload"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 35
    const-string v1, "GnotsPayloadUtil"

    const-string v2, "Intent did not contain the payload key."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_0
    :goto_0
    return-object v0

    .line 38
    :cond_1
    const-string v1, "gms.gnots.payload"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/notifications/k;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 40
    invoke-static {v1}, Lcom/google/android/gms/notifications/k;->a([B)Lcom/google/c/e/a/a/a/a/c;

    move-result-object v1

    .line 41
    if-eqz v1, :cond_2

    iget-object v2, v1, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/c/e/a/a/a/a/c;->b:Lcom/google/c/e/a/a/a/a/b;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/b;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, v1, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/c/e/a/a/a/a/c;->a:Lcom/google/c/e/a/a/a/a/d;

    iget-object v2, v2, Lcom/google/c/e/a/a/a/a/d;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_1
    if-eqz v2, :cond_0

    move-object v0, v1

    .line 44
    goto :goto_0

    .line 41
    :cond_2
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private static a([B)Lcom/google/c/e/a/a/a/a/c;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 95
    if-nez p0, :cond_0

    move-object v0, v1

    .line 104
    :goto_0
    return-object v0

    .line 100
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/c/e/a/a/a/a/c;

    invoke-direct {v0}, Lcom/google/c/e/a/a/a/a/c;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/c/e/a/a/a/a/c;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    const-string v2, "GnotsPayloadUtil"

    const-string v3, "Failed to parse payload to it\'s proto form."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 104
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 86
    const/16 v0, 0x9

    :try_start_0
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    const-string v1, "GnotsPayloadUtil"

    const-string v2, "Failed to parse payload string into bytes."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 89
    const/4 v0, 0x0

    goto :goto_0
.end method
