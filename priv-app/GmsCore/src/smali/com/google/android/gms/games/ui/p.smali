.class public abstract Lcom/google/android/gms/games/ui/p;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/games/ui/cn;


# instance fields
.field protected a:Lcom/google/android/gms/games/ui/q;

.field protected b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected final c:I

.field private d:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 49
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->d:Z

    .line 62
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 63
    iput p1, p0, Lcom/google/android/gms/games/ui/p;->c:I

    .line 64
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)I
    .locals 2

    .prologue
    .line 306
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 227
    iget v0, p0, Lcom/google/android/gms/games/ui/p;->c:I

    const/4 v1, 0x1

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 238
    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 174
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method public a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public final a_(Z)V
    .locals 2

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 347
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/p;->d:Z

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->a(IZ)V

    .line 350
    :cond_0
    return-void
.end method

.method protected final b()Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 150
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 151
    return-object v0
.end method

.method public final b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 119
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 120
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/p;->a(Lcom/google/android/gms/common/api/v;)V

    .line 121
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->x()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->t()V

    .line 196
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 125
    const-string v0, "GamesFragment"

    const-string v1, "Unexpected call to onConnectionSuspended - subclasses should unregister as a listener in onStop() and clear data in onDestroyView()"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 222
    const/4 v0, 0x0

    return v0
.end method

.method public h()F
    .locals 1

    .prologue
    .line 243
    const v0, 0x3f333333    # 0.7f

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x1

    return v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 258
    const/4 v0, 0x1

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x2

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Landroid/app/Activity;
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    return-object v0
.end method

.method public n()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 317
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/ui/cn;)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    return-object v0
.end method

.method public o()V
    .locals 0

    .prologue
    .line 332
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    .line 70
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/q;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/p;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Lcom/google/android/gms/games/ui/ck;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/ck;-><init>(Lcom/google/android/gms/games/ui/cn;)V

    .line 79
    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/ck;->a(Landroid/view/LayoutInflater;)Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 80
    iget-object v1, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ck;->a(Lcom/google/android/play/headerlist/PlayHeaderListLayout;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->K()V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 87
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/p;->c:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 95
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 96
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 100
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/api/x;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/api/y;)V

    .line 103
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->b(Lcom/google/android/gms/common/api/x;)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->a:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/q;->b(Lcom/google/android/gms/common/api/y;)V

    .line 109
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 110
    return-void
.end method

.method public p()Z
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    return v0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 341
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/p;->d:Z

    return v0
.end method

.method public final r()V
    .locals 4

    .prologue
    .line 356
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->b()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    .line 358
    if-eqz v0, :cond_0

    .line 359
    const/4 v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    sget v3, Lcom/google/android/gms/f;->y:I

    aput v3, v1, v2

    const/4 v2, 0x1

    sget v3, Lcom/google/android/gms/f;->A:I

    aput v3, v1, v2

    const/4 v2, 0x2

    sget v3, Lcom/google/android/gms/f;->B:I

    aput v3, v1, v2

    const/4 v2, 0x3

    sget v3, Lcom/google/android/gms/f;->z:I

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->a([I)V

    .line 365
    :cond_0
    return-void
.end method

.method public final s()F
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/games/ui/p;->b:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->d()F

    move-result v0

    .line 372
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
