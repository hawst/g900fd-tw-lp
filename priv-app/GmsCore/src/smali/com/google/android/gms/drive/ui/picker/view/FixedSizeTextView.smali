.class public Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/graphics/Typeface;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 45
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 46
    iput-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    .line 48
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 50
    if-eqz p2, :cond_0

    .line 51
    invoke-virtual {p0, p2}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->setTypeface(Landroid/graphics/Typeface;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    .line 55
    return-void

    :cond_1
    move v0, v2

    .line 45
    goto :goto_0

    .line 54
    :catchall_0
    move-exception v0

    iput-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    throw v0
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a:Z

    if-nez v0, :cond_0

    .line 61
    invoke-super {p0}, Landroid/widget/TextView;->requestLayout()V

    .line 63
    :cond_0
    return-void
.end method
