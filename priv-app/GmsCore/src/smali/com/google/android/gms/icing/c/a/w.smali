.class public final Lcom/google/android/gms/icing/c/a/w;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:J

.field public d:D

.field public e:Lcom/google/android/gms/icing/c/a/v;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1424
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1425
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/w;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/w;->cachedSize:I

    .line 1426
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 1521
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1522
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    if-eqz v1, :cond_0

    .line 1523
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1526
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1527
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1530
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 1531
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1534
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 1536
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1539
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    if-eqz v1, :cond_4

    .line 1540
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1543
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1441
    if-ne p1, p0, :cond_1

    .line 1442
    const/4 v0, 0x1

    .line 1476
    :cond_0
    :goto_0
    return v0

    .line 1444
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/w;

    if-eqz v1, :cond_0

    .line 1447
    check-cast p1, Lcom/google/android/gms/icing/c/a/w;

    .line 1448
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/w;->a:Z

    if-ne v1, v2, :cond_0

    .line 1451
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 1452
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1458
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/w;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1462
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 1463
    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/w;->d:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1467
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    if-nez v1, :cond_5

    .line 1468
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    if-nez v1, :cond_0

    .line 1476
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/w;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1455
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1472
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 1481
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1483
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1485
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1488
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 1489
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1491
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1493
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/w;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1494
    return v0

    .line 1481
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 1483
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1491
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/c/a/v;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1392
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/w;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/c/a/v;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x21 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 1500
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    if-eqz v0, :cond_0

    .line 1501
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/w;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1503
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1504
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1506
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 1507
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1509
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    .line 1511
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/w;->d:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 1513
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    if-eqz v0, :cond_4

    .line 1514
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/w;->e:Lcom/google/android/gms/icing/c/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1516
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1517
    return-void
.end method
