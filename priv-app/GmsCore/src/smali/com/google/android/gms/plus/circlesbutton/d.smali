.class public abstract Lcom/google/android/gms/plus/circlesbutton/d;
.super Landroid/view/ViewGroup;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static d:Z

.field private static e:Landroid/graphics/drawable/Drawable;

.field private static f:Landroid/graphics/drawable/Drawable;

.field private static g:Landroid/graphics/drawable/Drawable;

.field private static h:Landroid/graphics/drawable/Drawable;

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:I

.field private static n:I

.field private static o:Landroid/widget/FrameLayout$LayoutParams;


# instance fields
.field a:Landroid/content/res/Resources;

.field b:I

.field c:Lcom/google/android/gms/plus/d/c;

.field private p:I

.field private q:Landroid/graphics/drawable/Drawable;

.field private final r:Landroid/widget/TextView;

.field private s:I

.field private t:Ljava/lang/String;

.field private u:Landroid/widget/ProgressBar;

.field private v:Landroid/graphics/Rect;

.field private w:I

.field private x:Z

.field private y:Z

.field private z:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/plus/circlesbutton/d;-><init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;B)V

    .line 93
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/content/Context;Landroid/util/AttributeSet;B)V
    .locals 9

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v8, -0x2

    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 97
    invoke-direct {p0, p1, p3, v4}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    .line 81
    iput-boolean v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    .line 83
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->z:Landroid/graphics/Rect;

    .line 99
    const-string v0, "http://schemas.android.com/apk/lib/com.google.android.gms.plus"

    const-string v5, "size"

    const-string v6, "BaseCirclesButtonView"

    invoke-static {v0, v5, p1, p3, v6}, Lcom/google/android/gms/common/internal/ce;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v5, "SMALL"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v1

    .line 100
    :goto_0
    const-string v5, "http://schemas.android.com/apk/lib/com.google.android.gms.plus"

    const-string v6, "type"

    const-string v7, "BaseCirclesButtonView"

    invoke-static {v5, v6, p1, p3, v7}, Lcom/google/android/gms/common/internal/ce;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "FOLLOW"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v2, v1

    .line 102
    :cond_0
    :goto_1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    .line 104
    sget-boolean v3, Lcom/google/android/gms/plus/circlesbutton/d;->d:Z

    if-nez v3, :cond_1

    .line 105
    sput-boolean v1, Lcom/google/android/gms/plus/circlesbutton/d;->d:Z

    .line 107
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/g;->g:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/plus/circlesbutton/d;->i:I

    .line 109
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/g;->j:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/plus/circlesbutton/d;->j:I

    .line 111
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/g;->f:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/plus/circlesbutton/d;->k:I

    .line 113
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/g;->i:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/plus/circlesbutton/d;->l:I

    .line 115
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/g;->h:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/plus/circlesbutton/d;->m:I

    .line 117
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/g;->k:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/plus/circlesbutton/d;->n:I

    .line 119
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/h;->cu:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->e:Landroid/graphics/drawable/Drawable;

    .line 121
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/h;->bd:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->f:Landroid/graphics/drawable/Drawable;

    .line 123
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/h;->be:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->g:Landroid/graphics/drawable/Drawable;

    .line 125
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/h;->bc:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    sput-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->h:Landroid/graphics/drawable/Drawable;

    .line 127
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v8, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    sput-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->o:Landroid/widget/FrameLayout$LayoutParams;

    .line 130
    sget-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 131
    sget-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 132
    sget-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 133
    sget-object v3, Lcom/google/android/gms/plus/circlesbutton/d;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3, v1}, Landroid/graphics/drawable/Drawable;->setFilterBitmap(Z)V

    .line 136
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    new-instance v5, Landroid/widget/TextView;

    invoke-direct {v5, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    invoke-static {v3, v5, v1}, Lcom/google/android/gms/plus/circlesbutton/i;->a(Landroid/content/res/Resources;Landroid/widget/TextView;I)V

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v3, -0x1

    invoke-direct {v1, v8, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v5}, Landroid/widget/TextView;->setSingleLine()V

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    const/16 v1, 0x10

    invoke-virtual {v5, v1}, Landroid/widget/TextView;->setGravity(I)V

    iput-object v5, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    .line 138
    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/circlesbutton/d;->addView(Landroid/view/View;)V

    .line 140
    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/plus/circlesbutton/d;->a(IZ)V

    .line 141
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(I)V

    .line 142
    return-void

    .line 99
    :cond_2
    const-string v5, "MEDIUM"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v0, v2

    goto/16 :goto_0

    :cond_3
    const-string v5, "LARGE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v3

    goto/16 :goto_0

    :cond_4
    move v0, v4

    goto/16 :goto_0

    .line 100
    :cond_5
    const-string v6, "ONE_CLICK_FOLLOW"

    invoke-virtual {v6, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v6

    if-nez v6, :cond_0

    const-string v2, "BLOCKED"

    invoke-virtual {v2, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v3

    goto/16 :goto_1

    :cond_6
    move v2, v4

    goto/16 :goto_1
.end method

.method private a(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 508
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 509
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/circlesbutton/d;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 514
    :goto_0
    return-void

    .line 512
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/circlesbutton/d;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private b(IZ)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 458
    packed-switch p1, :pswitch_data_0

    .line 504
    :goto_0
    return-void

    .line 460
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->cH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Landroid/graphics/drawable/Drawable;)V

    .line 462
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->getContext()Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    const/4 v2, 0x3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/circlesbutton/i;->a(Landroid/content/res/Resources;Landroid/widget/TextView;I)V

    .line 464
    iput-boolean v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    .line 465
    sget-object v0, Lcom/google/android/gms/plus/circlesbutton/d;->e:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->t:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(Ljava/lang/String;Z)V

    goto :goto_0

    .line 471
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->cG:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Landroid/graphics/drawable/Drawable;)V

    .line 473
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->getContext()Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/plus/circlesbutton/i;->a(Landroid/content/res/Resources;Landroid/widget/TextView;I)V

    .line 475
    iput-boolean v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    .line 476
    sget-object v0, Lcom/google/android/gms/plus/circlesbutton/d;->g:Landroid/graphics/drawable/Drawable;

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    .line 477
    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    if-eq v0, v3, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    if-ne v0, v2, :cond_1

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/p;->dY:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(Ljava/lang/String;Z)V

    goto :goto_0

    .line 482
    :cond_1
    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->s:I

    if-ne v0, v2, :cond_2

    .line 483
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/p;->ea:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(Ljava/lang/String;Z)V

    goto :goto_0

    .line 487
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/p;->dW:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(Ljava/lang/String;Z)V

    goto :goto_0

    .line 495
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->t:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Landroid/graphics/drawable/Drawable;)V

    .line 496
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->getContext()Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/circlesbutton/i;->a(Landroid/content/res/Resources;Landroid/widget/TextView;I)V

    .line 498
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    .line 499
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/p;->eb:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 458
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private b(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 329
    if-eqz p2, :cond_0

    .line 330
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->requestLayout()V

    .line 333
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(IZ)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 367
    iput p1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->s:I

    .line 368
    packed-switch p1, :pswitch_data_0

    .line 392
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->m:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->w:I

    .line 393
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->i:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->p:I

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 397
    :goto_0
    if-eqz p2, :cond_0

    .line 398
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->requestLayout()V

    .line 400
    :cond_0
    return-void

    .line 370
    :pswitch_0
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->m:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->w:I

    .line 371
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->i:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->p:I

    .line 372
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    sget-object v0, Lcom/google/android/gms/plus/circlesbutton/d;->o:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 377
    :pswitch_1
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->n:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->w:I

    .line 378
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->j:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->p:I

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 380
    sget-object v0, Lcom/google/android/gms/plus/circlesbutton/d;->o:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 384
    :pswitch_2
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->m:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->w:I

    .line 385
    sget v0, Lcom/google/android/gms/plus/circlesbutton/d;->i:I

    iput v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->p:I

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 387
    sget-object v0, Lcom/google/android/gms/plus/circlesbutton/d;->o:Landroid/widget/FrameLayout$LayoutParams;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 368
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->t:Ljava/lang/String;

    .line 311
    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 320
    :goto_0
    return-void

    .line 315
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    const/4 v0, 0x1

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(IZ)V

    goto :goto_0

    .line 318
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/plus/circlesbutton/d;->b(IZ)V

    goto :goto_0
.end method

.method protected a(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 336
    iget-boolean v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->y:Z

    if-ne v2, p1, :cond_0

    .line 353
    :goto_0
    return v0

    .line 340
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->y:Z

    .line 341
    if-eqz p1, :cond_3

    .line 342
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    if-nez v2, :cond_1

    .line 343
    new-instance v2, Landroid/widget/ProgressBar;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    .line 344
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v1}, Landroid/widget/ProgressBar;->setIndeterminate(Z)V

    .line 345
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/circlesbutton/d;->addView(Landroid/view/View;)V

    .line 348
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 352
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->invalidate()V

    move v0, v1

    .line 353
    goto :goto_0

    .line 349
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_2

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_1
.end method

.method protected final b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 403
    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    .line 404
    iput p1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    .line 407
    packed-switch p1, :pswitch_data_0

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->t:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Ljava/lang/String;Z)V

    .line 418
    :goto_0
    return-void

    .line 409
    :pswitch_0
    const/4 v0, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/circlesbutton/d;->b(IZ)V

    goto :goto_0

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 438
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    if-ne v0, v1, :cond_2

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    if-eq v0, v1, :cond_1

    .line 440
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->s:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Landroid/graphics/drawable/Drawable;)V

    .line 444
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    :goto_1
    return-void

    .line 442
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->t:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 446
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->t:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->cG:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Landroid/graphics/drawable/Drawable;)V

    .line 453
    :goto_2
    invoke-virtual {p0, p0}, Lcom/google/android/gms/plus/circlesbutton/d;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 450
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->a:Landroid/content/res/Resources;

    sget v1, Lcom/google/android/gms/h;->cH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method protected final c(I)V
    .locals 2

    .prologue
    .line 425
    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->s:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 427
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->setVisibility(I)V

    .line 431
    :goto_0
    return-void

    .line 429
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/circlesbutton/d;->setVisibility(I)V

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->z:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 302
    :cond_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 303
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->c:Lcom/google/android/gms/plus/d/c;

    if-eqz v0, :cond_0

    .line 520
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->c:Lcom/google/android/gms/plus/d/c;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/circlesbutton/d;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/d/c;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 525
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 238
    sub-int v2, p4, p2

    .line 239
    sub-int v5, p5, p3

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, v2, v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int v4, v0, v1

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v5, v0

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int v6, v0, v1

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    sub-int v1, v2, v0

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    sub-int v3, v5, v0

    .line 245
    const/4 v0, 0x0

    .line 246
    iget-object v7, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    .line 248
    iget-boolean v8, p0, Lcom/google/android/gms/plus/circlesbutton/d;->y:Z

    if-eqz v8, :cond_0

    .line 249
    iget-object v8, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v8}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v8

    .line 250
    iget-object v9, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v9}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v9

    .line 251
    sub-int/2addr v2, v8

    div-int/lit8 v2, v2, 0x2

    .line 252
    sub-int v9, v5, v9

    div-int/lit8 v9, v9, 0x2

    .line 253
    iget-object v10, p0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    add-int v11, v2, v8

    add-int/2addr v8, v9

    invoke-virtual {v10, v2, v9, v11, v8}, Landroid/widget/ProgressBar;->layout(IIII)V

    .line 257
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    if-eqz v2, :cond_1

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 259
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-nez v2, :cond_1

    .line 260
    iget v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->p:I

    add-int/2addr v0, v2

    .line 264
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int v8, v0, v2

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v2, v4, v8

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v0, v2

    .line 267
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    if-ge v0, v2, :cond_2

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    .line 270
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    sub-int v4, v6, v7

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v2, v4

    .line 271
    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    if-ge v2, v4, :cond_7

    .line 272
    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    move v4, v2

    .line 274
    :goto_0
    add-int v2, v0, v8

    .line 275
    if-le v2, v1, :cond_6

    .line 278
    :goto_1
    invoke-static {v5, v7}, Ljava/lang/Math;->min(II)I

    move-result v2

    add-int/2addr v2, v4

    .line 279
    if-le v2, v3, :cond_3

    move v2, v3

    .line 283
    :cond_3
    iget-boolean v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    if-eqz v3, :cond_4

    .line 284
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    .line 285
    iget-object v5, p0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 286
    iget-object v7, p0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v3

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v7

    .line 287
    iget-object v7, p0, Lcom/google/android/gms/plus/circlesbutton/d;->z:Landroid/graphics/Rect;

    add-int v8, v0, v5

    add-int/2addr v3, v6

    invoke-virtual {v7, v0, v6, v8, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 288
    iget v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->p:I

    add-int/2addr v3, v5

    add-int/2addr v0, v3

    .line 291
    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    .line 292
    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v4, v1, v2}, Landroid/widget/TextView;->layout(IIII)V

    .line 294
    :cond_5
    return-void

    :cond_6
    move v1, v2

    goto :goto_1

    :cond_7
    move v4, v2

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 16

    .prologue
    .line 165
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/plus/circlesbutton/d;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 166
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumWidth()I

    move-result v8

    .line 167
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getMinimumHeight()I

    move-result v9

    .line 168
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_7

    const/4 v1, 0x1

    move v7, v1

    .line 170
    :goto_0
    if-eqz v2, :cond_0

    .line 171
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 173
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/gms/plus/circlesbutton/d;->w:I

    iput v3, v2, Landroid/graphics/Rect;->right:I

    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 175
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v10

    .line 177
    if-nez v10, :cond_8

    .line 178
    const v1, 0x7fffffff

    .line 183
    :goto_1
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v11

    .line 185
    if-nez v11, :cond_9

    .line 186
    const v2, 0x7fffffff

    .line 191
    :goto_2
    const/4 v4, 0x0

    .line 192
    const/4 v3, 0x0

    .line 193
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/plus/circlesbutton/d;->x:Z

    if-eqz v5, :cond_2

    .line 194
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    .line 195
    if-eqz v7, :cond_1

    .line 196
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/gms/plus/circlesbutton/d;->p:I

    add-int/2addr v3, v4

    .line 198
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/circlesbutton/d;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v4

    move v15, v4

    move v4, v3

    move v3, v15

    .line 201
    :cond_2
    const/high16 v5, -0x80000000

    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    .line 203
    if-eqz v7, :cond_a

    sub-int v5, v1, v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    sub-int/2addr v5, v6

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v5, v6

    move v6, v5

    .line 204
    :goto_3
    const/4 v5, 0x0

    .line 205
    if-eqz v7, :cond_3

    .line 206
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    const/high16 v14, -0x80000000

    invoke-static {v6, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-virtual {v13, v6, v12}, Landroid/widget/TextView;->measure(II)V

    .line 210
    :cond_3
    const/high16 v6, 0x40000000    # 2.0f

    if-eq v10, v6, :cond_4

    .line 211
    if-eqz v7, :cond_b

    .line 212
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    .line 214
    :goto_4
    add-int/2addr v1, v4

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v4

    .line 216
    :cond_4
    const/high16 v4, 0x40000000    # 2.0f

    if-eq v11, v4, :cond_5

    .line 217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/circlesbutton/d;->r:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    add-int/lit8 v2, v2, 0x6

    .line 220
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/gms/plus/circlesbutton/d;->s:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_5

    sget v3, Lcom/google/android/gms/plus/circlesbutton/d;->l:I

    if-ge v2, v3, :cond_5

    .line 221
    sget v2, Lcom/google/android/gms/plus/circlesbutton/d;->l:I

    .line 225
    :cond_5
    move/from16 v0, p1

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->resolveSize(II)I

    move-result v1

    .line 226
    move/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/gms/plus/circlesbutton/d;->resolveSize(II)I

    move-result v2

    .line 228
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/plus/circlesbutton/d;->y:Z

    if-eqz v3, :cond_6

    .line 229
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    sub-int v3, v2, v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/circlesbutton/d;->v:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v3, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 231
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/plus/circlesbutton/d;->u:Landroid/widget/ProgressBar;

    invoke-virtual {v4, v3, v3}, Landroid/widget/ProgressBar;->measure(II)V

    .line 233
    :cond_6
    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/circlesbutton/d;->setMeasuredDimension(II)V

    .line 234
    return-void

    .line 168
    :cond_7
    const/4 v1, 0x0

    move v7, v1

    goto/16 :goto_0

    .line 180
    :cond_8
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    goto/16 :goto_1

    .line 188
    :cond_9
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    goto/16 :goto_2

    .line 203
    :cond_a
    const/4 v5, 0x0

    move v6, v5

    goto/16 :goto_3

    :cond_b
    move v1, v5

    goto :goto_4
.end method
