.class public abstract Lcom/google/android/gms/drive/g/f;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private volatile a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/g/f;->a:Z

    .line 24
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 21
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/g/f;->a:Z

    .line 28
    return-void
.end method

.method public static a()Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 68
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    .line 70
    instance-of v0, v1, Lcom/google/android/gms/drive/g/f;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 71
    check-cast v0, Lcom/google/android/gms/drive/g/f;

    .line 73
    invoke-direct {v0}, Lcom/google/android/gms/drive/g/f;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    .line 82
    :goto_0
    return v0

    .line 78
    :cond_0
    invoke-virtual {v1}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 79
    goto :goto_0

    .line 82
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized b()Z
    .locals 1

    .prologue
    .line 53
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/g/f;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
