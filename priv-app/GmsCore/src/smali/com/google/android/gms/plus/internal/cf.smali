.class final Lcom/google/android/gms/plus/internal/cf;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/at;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/at;)V
    .locals 0

    .prologue
    .line 199
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/cf;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 200
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/cf;->b:Lcom/google/android/gms/plus/internal/at;

    .line 201
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 5

    .prologue
    .line 205
    const/4 v0, 0x0

    .line 206
    if-eqz p2, :cond_0

    .line 207
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 209
    :cond_0
    new-instance v1, Lcom/google/android/gms/common/c;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/cf;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v2, Lcom/google/android/gms/plus/internal/cg;

    iget-object v3, p0, Lcom/google/android/gms/plus/internal/cf;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/cf;->b:Lcom/google/android/gms/plus/internal/at;

    invoke-direct {v2, v3, v4, v1, p3}, Lcom/google/android/gms/plus/internal/cg;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/at;Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Post;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 211
    return-void
.end method
