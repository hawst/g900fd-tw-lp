.class final Lcom/google/android/gms/car/qi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/qh;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/qh;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const-wide/16 v12, 0x1f4

    .line 43
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 44
    const/4 v2, 0x1

    .line 45
    iget-object v3, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    monitor-enter v3

    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Lcom/google/android/gms/car/qh;)I

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->b(Lcom/google/android/gms/car/qh;)J

    move-result-wide v6

    sub-long v6, v4, v6

    sub-long/2addr v6, v12

    .line 48
    cmp-long v0, v6, v12

    if-lez v0, :cond_0

    .line 50
    long-to-int v0, v6

    div-int/lit16 v0, v0, 0x1f4

    add-int/lit8 v0, v0, 0x1

    .line 51
    mul-int v8, v0, v0

    .line 52
    const-string v0, "CAR.TIME"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "too much delay in timer. system heavily loaded? delay:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " back-off:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->c(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/qj;

    .line 55
    iget v7, v0, Lcom/google/android/gms/car/qj;->a:I

    add-int/2addr v7, v8

    iput v7, v0, Lcom/google/android/gms/car/qj;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 58
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->c(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;

    move-result-object v0

    const/4 v6, 0x0

    invoke-virtual {v0, v6}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v6

    .line 59
    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/qj;

    .line 61
    iget v7, v0, Lcom/google/android/gms/car/qj;->a:I

    iget-object v8, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v8}, Lcom/google/android/gms/car/qh;->d(Lcom/google/android/gms/car/qh;)I

    move-result v8

    if-gt v7, v8, :cond_1

    .line 62
    invoke-interface {v6}, Ljava/util/ListIterator;->remove()V

    .line 63
    iget-object v7, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v7}, Lcom/google/android/gms/car/qh;->e(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->c(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_7

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->f(Lcom/google/android/gms/car/qh;)V

    .line 69
    invoke-static {}, Lcom/google/android/gms/car/qh;->a()Lcom/google/android/gms/car/qh;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    if-ne v0, v2, :cond_3

    .line 70
    invoke-static {}, Lcom/google/android/gms/car/qh;->b()Lcom/google/android/gms/car/qh;

    .line 73
    :cond_3
    :goto_2
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->e(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/qj;

    .line 75
    invoke-virtual {v0}, Lcom/google/android/gms/car/qj;->a()V

    goto :goto_3

    .line 77
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->e(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/car/qh;->a(Lcom/google/android/gms/car/qh;J)J

    .line 79
    const-string v0, "CAR.TIME"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 80
    const-string v0, "CAR.TIME"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "timer run, time:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " priority:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v3

    invoke-static {v3}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_5
    if-eqz v1, :cond_6

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/car/qi;->a:Lcom/google/android/gms/car/qh;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->g(Lcom/google/android/gms/car/qh;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, p0, v12, v13}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 87
    :cond_6
    return-void

    :cond_7
    move v1, v2

    goto :goto_2
.end method
