.class public final Lcom/google/android/gms/drive/a/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/events/k;

.field private final b:Lcom/google/android/gms/drive/database/r;

.field private final c:Lcom/google/android/gms/drive/b/f;

.field private final d:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/events/k;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/f;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/events/k;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/j;->a:Lcom/google/android/gms/drive/events/k;

    .line 41
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/j;->b:Lcom/google/android/gms/drive/database/r;

    .line 42
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/j;->c:Lcom/google/android/gms/drive/b/f;

    .line 43
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/j;->d:Landroid/content/Context;

    .line 44
    return-void
.end method


# virtual methods
.method final a(ILcom/google/android/gms/drive/a/a/i;Z)V
    .locals 13

    .prologue
    .line 48
    iget-object v11, p2, Lcom/google/android/gms/drive/a/a/i;->a:Lcom/google/android/gms/drive/a/a/l;

    .line 49
    invoke-virtual {v11}, Lcom/google/android/gms/drive/a/a/l;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    :goto_0
    return-void

    .line 53
    :cond_0
    iget-object v9, p2, Lcom/google/android/gms/drive/a/a/i;->a:Lcom/google/android/gms/drive/a/a/l;

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Lcom/google/android/gms/drive/a/a/l;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v9}, Lcom/google/android/gms/drive/a/a/l;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p2, Lcom/google/android/gms/drive/a/a/i;->f:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v3, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-virtual {v9}, Lcom/google/android/gms/drive/a/a/l;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    :goto_1
    const/4 v4, 0x0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    iget-object v4, p2, Lcom/google/android/gms/drive/a/a/i;->c:Ljava/lang/String;

    if-nez v4, :cond_2

    const-string v0, "CompletionHandler"

    const-string v1, "Conflict without baseContentHash!"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance v0, Lcom/google/android/gms/drive/events/f;

    iget-object v1, p2, Lcom/google/android/gms/drive/a/a/i;->b:Lcom/google/android/gms/drive/DriveId;

    iget-object v5, p2, Lcom/google/android/gms/drive/a/a/i;->d:Ljava/lang/String;

    iget-object v6, p2, Lcom/google/android/gms/drive/a/a/i;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    const/4 v8, 0x1

    new-array v8, v8, [Lcom/google/android/gms/drive/a/e;

    const/4 v10, 0x0

    iget-object v12, p2, Lcom/google/android/gms/drive/a/a/i;->h:Lcom/google/android/gms/drive/a/e;

    aput-object v12, v8, v10

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-virtual {v9}, Lcom/google/android/gms/drive/a/a/l;->c()Ljava/lang/String;

    move-result-object v10

    move v9, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/drive/events/f;-><init>(Lcom/google/android/gms/drive/DriveId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Ljava/util/List;Ljava/util/List;ILjava/lang/String;)V

    .line 59
    :try_start_0
    iget-object v1, p2, Lcom/google/android/gms/drive/a/a/i;->f:Lcom/google/android/gms/drive/auth/g;

    .line 60
    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/j;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p2, Lcom/google/android/gms/drive/a/a/i;->g:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-interface {v2, v1, v3}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v2

    .line 61
    if-nez v2, :cond_4

    .line 63
    const-string v0, "CompletionHandler"

    const-string v1, "Could not get Entry: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p2, Lcom/google/android/gms/drive/a/a/i;->g:Lcom/google/android/gms/drive/database/model/EntrySpec;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 84
    const-string v1, "Exception in handleCompletion"

    .line 85
    const-string v2, "CompletionHandler"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/j;->d:Landroid/content/Context;

    const-string v2, "CompletionHandler"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move-object v2, v3

    .line 53
    goto :goto_1

    .line 67
    :cond_4
    const/4 v3, 0x2

    if-ne p1, v3, :cond_5

    .line 68
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/j;->c:Lcom/google/android/gms/drive/b/f;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/b/f;->a(Lcom/google/android/gms/drive/database/model/ah;)Z

    move-result v3

    if-nez v3, :cond_5

    if-nez p3, :cond_5

    .line 72
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ah;->i(Ljava/lang/String;)V

    .line 73
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 77
    :cond_5
    const-string v2, "CompletionHandler"

    const-string v3, "Raising completion event for action with tag: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v11}, Lcom/google/android/gms/drive/a/a/l;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/j;->a:Lcom/google/android/gms/drive/events/k;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/drive/events/k;->a(Lcom/google/android/gms/drive/events/f;Lcom/google/android/gms/drive/auth/g;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method
