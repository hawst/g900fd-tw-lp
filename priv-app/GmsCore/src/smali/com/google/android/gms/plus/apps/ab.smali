.class public final Lcom/google/android/gms/plus/apps/ab;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/internal/al;


# instance fields
.field private a:Lcom/google/android/gms/plus/internal/ad;

.field private b:Lcom/google/android/gms/plus/internal/ab;

.field private c:Landroid/accounts/Account;

.field private d:Z

.field private e:Lcom/google/android/gms/plus/apps/ac;

.field private f:Z

.field private g:Ljava/lang/String;

.field private h:Lcom/google/android/gms/common/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 65
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    .line 66
    return-void
.end method

.method public static a(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/ab;
    .locals 3

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    new-instance v1, Lcom/google/android/gms/plus/apps/ab;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/ab;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/plus/apps/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "account"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/ab;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method


# virtual methods
.method public final T_()V
    .locals 2

    .prologue
    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    .line 149
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ab;->g:Ljava/lang/String;

    invoke-interface {v0, p0, v1}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/al;Ljava/lang/String;)V

    .line 152
    :cond_0
    return-void
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 158
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    .line 162
    :goto_0
    return-void

    .line 160
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 138
    iput-boolean v3, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    .line 139
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ab;->h:Lcom/google/android/gms/common/c;

    .line 140
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->e:Lcom/google/android/gms/plus/apps/ac;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->e:Lcom/google/android/gms/plus/apps/ac;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ab;->g:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/apps/ac;->a(Ljava/lang/String;Z)V

    .line 143
    :cond_0
    iput-boolean v3, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    .line 144
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 166
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ab;->h:Lcom/google/android/gms/common/c;

    .line 167
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->e:Lcom/google/android/gms/plus/apps/ac;

    if-eqz v0, :cond_1

    .line 168
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ab;->e:Lcom/google/android/gms/plus/apps/ac;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-interface {v2, p2, v0}, Lcom/google/android/gms/plus/apps/ac;->a(Ljava/lang/String;Z)V

    .line 170
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    .line 171
    return-void

    :cond_2
    move v0, v1

    .line 168
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 116
    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    if-eqz v1, :cond_2

    .line 117
    const-string v0, "DeleteMomentFragment"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    const-string v0, "DeleteMomentFragment"

    const-string v1, "Can only delete one app activity at a time."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    const/4 v0, 0x0

    .line 133
    :cond_1
    :goto_0
    return v0

    .line 123
    :cond_2
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    .line 124
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ab;->g:Ljava/lang/String;

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 127
    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ab;->T_()V

    goto :goto_0

    .line 128
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    if-nez v1, :cond_1

    .line 129
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v1}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 130
    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 78
    instance-of v0, p1, Lcom/google/android/gms/plus/apps/ac;

    if-eqz v0, :cond_0

    .line 79
    check-cast p1, Lcom/google/android/gms/plus/apps/ac;

    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ab;->e:Lcom/google/android/gms/plus/apps/ac;

    return-void

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DeleteMomentFragment must be hosted by an activity that implements DeleteMomentCallbacks."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 95
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ab;->setRetainInstance(Z)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ab;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->c:Landroid/accounts/Account;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ab;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ab;->c:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1, p0, p0, v2}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    .line 99
    return-void
.end method

.method public final onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 103
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 108
    :cond_1
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ab;->b:Lcom/google/android/gms/plus/internal/ab;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/ab;->d:Z

    .line 110
    iput-boolean v1, p0, Lcom/google/android/gms/plus/apps/ab;->f:Z

    .line 111
    iput-object v2, p0, Lcom/google/android/gms/plus/apps/ab;->g:Ljava/lang/String;

    .line 113
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 89
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ab;->e:Lcom/google/android/gms/plus/apps/ac;

    .line 90
    return-void
.end method
