.class public final Lcom/google/android/gms/ads/internal/purchase/h;
.super Lcom/google/android/gms/ads/internal/purchase/a/l;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private a:Z

.field private b:Landroid/content/Context;

.field private c:I

.field private d:Landroid/content/Intent;

.field private e:Lcom/google/android/gms/ads/internal/purchase/b;

.field private f:Lcom/google/android/gms/ads/internal/purchase/g;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ZILandroid/content/Intent;Lcom/google/android/gms/ads/internal/purchase/g;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/purchase/a/l;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->a:Z

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/purchase/h;->g:Ljava/lang/String;

    .line 44
    iput p4, p0, Lcom/google/android/gms/ads/internal/purchase/h;->c:I

    .line 45
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/purchase/h;->d:Landroid/content/Intent;

    .line 46
    iput-boolean p3, p0, Lcom/google/android/gms/ads/internal/purchase/h;->a:Z

    .line 47
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/purchase/h;->b:Landroid/content/Context;

    .line 48
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/purchase/h;->f:Lcom/google/android/gms/ads/internal/purchase/g;

    .line 49
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->a:Z

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->d:Landroid/content/Intent;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->c:I

    return v0
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->d:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/k;->a(Landroid/content/Intent;)I

    move-result v0

    .line 84
    iget v1, p0, Lcom/google/android/gms/ads/internal/purchase/h;->c:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 87
    :cond_1
    new-instance v0, Lcom/google/android/gms/ads/internal/purchase/b;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/h;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/purchase/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.vending.billing.InAppBillingService.BIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 89
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 90
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/h;->b:Landroid/content/Context;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, p0, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    goto :goto_0
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 102
    const-string v0, "In-app billing service connected."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/internal/purchase/b;->a(Landroid/os/IBinder;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->d:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/k;->b(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 105
    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/k;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    if-nez v0, :cond_0

    .line 118
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/h;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/purchase/h;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/ads/internal/purchase/b;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 112
    if-nez v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/purchase/i;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/internal/purchase/i;

    move-result-object v0

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/h;->f:Lcom/google/android/gms/ads/internal/purchase/g;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/purchase/i;->a(Lcom/google/android/gms/ads/internal/purchase/g;)V

    .line 116
    :cond_1
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/purchase/h;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/purchase/b;->a:Ljava/lang/Object;

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 96
    const-string v0, "In-app billing service disconnected."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/purchase/h;->e:Lcom/google/android/gms/ads/internal/purchase/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/ads/internal/purchase/b;->a:Ljava/lang/Object;

    .line 98
    return-void
.end method
