.class public Lcom/google/android/gms/plus/model/posts/Comment;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/model/posts/b;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/plus/model/posts/b;

    invoke-direct {v0}, Lcom/google/android/gms/plus/model/posts/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/model/posts/Comment;->CREATOR:Lcom/google/android/gms/plus/model/posts/b;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/google/android/gms/plus/model/posts/Comment;->a:I

    .line 70
    iput-object p2, p0, Lcom/google/android/gms/plus/model/posts/Comment;->b:Ljava/lang/String;

    .line 71
    iput-object p3, p0, Lcom/google/android/gms/plus/model/posts/Comment;->c:Ljava/lang/String;

    .line 72
    iput-object p4, p0, Lcom/google/android/gms/plus/model/posts/Comment;->d:Ljava/lang/String;

    .line 73
    iput-object p5, p0, Lcom/google/android/gms/plus/model/posts/Comment;->e:Ljava/lang/String;

    .line 74
    iput-object p6, p0, Lcom/google/android/gms/plus/model/posts/Comment;->f:Ljava/lang/String;

    .line 75
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 83
    const/4 v1, 0x1

    const/4 v2, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/model/posts/Comment;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Comment;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Comment;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Comment;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Comment;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Comment;->e:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 142
    instance-of v1, p1, Lcom/google/android/gms/plus/model/posts/Comment;

    if-nez v1, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v0

    .line 146
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/model/posts/Comment;

    .line 147
    iget v1, p0, Lcom/google/android/gms/plus/model/posts/Comment;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/model/posts/Comment;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/model/posts/Comment;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/model/posts/Comment;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/model/posts/Comment;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/model/posts/Comment;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/model/posts/Comment;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/model/posts/Comment;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/model/posts/Comment;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/model/posts/Comment;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/plus/model/posts/Comment;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/google/android/gms/plus/model/posts/Comment;->a:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 137
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/model/posts/Comment;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Comment;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Comment;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Comment;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/plus/model/posts/Comment;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 161
    invoke-static {p0, p1}, Lcom/google/android/gms/plus/model/posts/b;->a(Lcom/google/android/gms/plus/model/posts/Comment;Landroid/os/Parcel;)V

    .line 162
    return-void
.end method
