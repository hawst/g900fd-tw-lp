.class public interface abstract Lcom/google/android/gms/drive/api/d;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;IIIZLjava/lang/String;)Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract a(Ljava/lang/String;Z)Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract a()Lcom/google/android/gms/drive/auth/g;
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/c/a;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
.end method

.method public abstract a(Ljava/util/Set;)Ljava/util/Set;
.end method

.method public abstract a(JLcom/google/android/gms/drive/DriveId;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;I)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/DriveId;Ljava/util/List;Lcom/google/android/gms/drive/c/a;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/events/DriveEvent;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;)V
.end method

.method public abstract a(Lcom/google/android/gms/drive/query/Query;Lcom/google/android/gms/drive/api/a/al;)V
.end method

.method public abstract a(Ljava/lang/Runnable;)V
.end method

.method public abstract a(Ljava/util/List;)V
.end method

.method public abstract b()Lcom/google/android/gms/drive/c/b;
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;I)V
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V
.end method

.method public abstract b(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)V
.end method

.method public abstract b(Lcom/google/android/gms/drive/query/Query;)Z
.end method

.method public abstract c(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public abstract c()Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract d()Lcom/google/android/gms/drive/DriveId;
.end method

.method public abstract e()Z
.end method

.method public abstract f()Lcom/google/android/gms/drive/auth/g;
.end method

.method public abstract g()Z
.end method

.method public abstract h()Z
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()V
.end method

.method public abstract k()Z
.end method

.method public abstract l()Z
.end method

.method public abstract m()V
.end method

.method public abstract n()V
.end method

.method public abstract o()Lcom/google/android/gms/drive/StorageStats;
.end method

.method public abstract p()I
.end method

.method public abstract q()Lcom/google/android/gms/drive/g/aw;
.end method

.method public abstract r()Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;
.end method
