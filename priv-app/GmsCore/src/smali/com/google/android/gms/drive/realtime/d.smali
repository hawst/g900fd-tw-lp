.class public final Lcom/google/android/gms/drive/realtime/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Ljava/util/List;

.field private final c:Lcom/google/android/gms/drive/realtime/internal/a/d;

.field private d:Z

.field private e:Z


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/d;->a:Ljava/util/List;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/d;->b:Ljava/util/List;

    .line 59
    iput-boolean v2, p0, Lcom/google/android/gms/drive/realtime/d;->e:Z

    .line 62
    new-instance v0, Lcom/google/android/gms/drive/realtime/a/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/realtime/a/b;-><init>(Lcom/google/c/a/a/b/b/a/b;)V

    new-instance v1, Lcom/google/android/gms/drive/realtime/internal/a/c;

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/realtime/internal/a/c;-><init>(Lcom/google/android/gms/drive/realtime/internal/a/b;B)V

    iput-object v1, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    .line 63
    return-void
.end method

.method private static a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 179
    invoke-interface {p0}, Lcom/google/android/gms/drive/realtime/internal/a/d;->a()I

    move-result v0

    .line 180
    invoke-interface {p0, p1}, Lcom/google/android/gms/drive/realtime/internal/a/d;->a(Ljava/lang/Object;)V

    .line 181
    return v0
.end method

.method private static a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/util/List;)I
    .locals 3

    .prologue
    .line 189
    invoke-interface {p0}, Lcom/google/android/gms/drive/realtime/internal/a/d;->a()I

    move-result v0

    .line 190
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 191
    invoke-interface {p0, v2}, Lcom/google/android/gms/drive/realtime/internal/a/d;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 193
    :cond_0
    return v0
.end method

.method private a(Lcom/google/c/a/a/b/b/a/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;
    .locals 11

    .prologue
    const/4 v9, 0x0

    .line 92
    iget-object v0, p1, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    .line 93
    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    .line 94
    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    .line 95
    iget-boolean v3, p1, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    .line 96
    iget-object v5, p1, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    .line 97
    invoke-interface {v5}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v4

    .line 98
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/f;

    if-eqz v6, :cond_0

    .line 99
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/f;

    .line 100
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/TextInsertedDetails;

    iget v6, p1, Lcom/google/c/a/a/b/b/a/a/f;->g:I

    iget-object v7, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v8, p1, Lcom/google/c/a/a/b/b/a/a/f;->h:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/lang/Object;)I

    move-result v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/realtime/internal/event/TextInsertedDetails;-><init>(II)V

    .line 102
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/TextInsertedDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    .line 163
    :goto_0
    return-object v0

    .line 104
    :cond_0
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/e;

    if-eqz v6, :cond_1

    .line 105
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/e;

    .line 106
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/TextDeletedDetails;

    iget v6, p1, Lcom/google/c/a/a/b/b/a/a/e;->g:I

    iget-object v7, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v8, p1, Lcom/google/c/a/a/b/b/a/a/e;->h:Ljava/lang/String;

    invoke-static {v7, v8}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/lang/Object;)I

    move-result v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/realtime/internal/event/TextDeletedDetails;-><init>(II)V

    .line 108
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/TextDeletedDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto :goto_0

    .line 110
    :cond_1
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/h;

    if-eqz v6, :cond_3

    .line 111
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/h;

    .line 112
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v6, p1, Lcom/google/c/a/a/b/b/a/a/h;->h:Ljava/util/List;

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/util/List;)I

    move-result v7

    .line 113
    iget-object v5, p1, Lcom/google/c/a/a/b/b/a/a/h;->i:Lcom/google/c/a/a/b/b/a/e;

    .line 114
    if-nez v5, :cond_2

    .line 115
    :goto_1
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/ValuesAddedDetails;

    iget v6, p1, Lcom/google/c/a/a/b/b/a/a/h;->g:I

    iget-object v8, p1, Lcom/google/c/a/a/b/b/a/a/h;->h:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    iget-object v10, p1, Lcom/google/c/a/a/b/b/a/a/h;->j:Ljava/lang/Integer;

    invoke-direct/range {v5 .. v10}, Lcom/google/android/gms/drive/realtime/internal/event/ValuesAddedDetails;-><init>(IIILjava/lang/String;Ljava/lang/Integer;)V

    .line 118
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/ValuesAddedDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto :goto_0

    .line 114
    :cond_2
    invoke-interface {v5}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v9

    goto :goto_1

    .line 120
    :cond_3
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/i;

    if-eqz v6, :cond_5

    .line 121
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/i;

    .line 122
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v6, p1, Lcom/google/c/a/a/b/b/a/a/i;->h:Ljava/util/List;

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/util/List;)I

    move-result v7

    .line 123
    iget-object v5, p1, Lcom/google/c/a/a/b/b/a/a/i;->i:Lcom/google/c/a/a/b/b/a/e;

    .line 124
    if-nez v5, :cond_4

    .line 125
    :goto_2
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/ValuesRemovedDetails;

    iget v6, p1, Lcom/google/c/a/a/b/b/a/a/i;->g:I

    iget-object v8, p1, Lcom/google/c/a/a/b/b/a/a/i;->h:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    iget-object v10, p1, Lcom/google/c/a/a/b/b/a/a/i;->j:Ljava/lang/Integer;

    invoke-direct/range {v5 .. v10}, Lcom/google/android/gms/drive/realtime/internal/event/ValuesRemovedDetails;-><init>(IIILjava/lang/String;Ljava/lang/Integer;)V

    .line 127
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/ValuesRemovedDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto :goto_0

    .line 124
    :cond_4
    invoke-interface {v5}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v9

    goto :goto_2

    .line 129
    :cond_5
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/j;

    if-eqz v6, :cond_6

    .line 130
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/j;

    .line 131
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v6, p1, Lcom/google/c/a/a/b/b/a/a/j;->i:Ljava/util/List;

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/util/List;)I

    move-result v6

    .line 132
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v7, p1, Lcom/google/c/a/a/b/b/a/a/j;->h:Ljava/util/List;

    invoke-static {v5, v7}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/util/List;)I

    .line 133
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/ValuesSetDetails;

    iget v7, p1, Lcom/google/c/a/a/b/b/a/a/j;->g:I

    iget-object v8, p1, Lcom/google/c/a/a/b/b/a/a/j;->i:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    invoke-direct {v5, v7, v6, v8}, Lcom/google/android/gms/drive/realtime/internal/event/ValuesSetDetails;-><init>(III)V

    .line 135
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/ValuesSetDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto/16 :goto_0

    .line 137
    :cond_6
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/g;

    if-eqz v6, :cond_7

    .line 138
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/g;

    .line 139
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v6, p1, Lcom/google/c/a/a/b/b/a/a/g;->g:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/lang/Object;)I

    move-result v6

    .line 140
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v7, p1, Lcom/google/c/a/a/b/b/a/a/g;->h:Lcom/google/c/a/a/b/b/a/r;

    invoke-static {v5, v7}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/lang/Object;)I

    .line 141
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    iget-object v7, p1, Lcom/google/c/a/a/b/b/a/a/g;->i:Lcom/google/c/a/a/b/b/a/r;

    invoke-static {v5, v7}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/lang/Object;)I

    .line 142
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/ValueChangedDetails;

    invoke-direct {v5, v6}, Lcom/google/android/gms/drive/realtime/internal/event/ValueChangedDetails;-><init>(I)V

    .line 143
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/ValueChangedDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto/16 :goto_0

    .line 145
    :cond_7
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/d;

    if-eqz v6, :cond_8

    .line 146
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/d;

    .line 147
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/ReferenceShiftedDetails;

    iget-object v6, p1, Lcom/google/c/a/a/b/b/a/a/d;->j:Ljava/lang/String;

    iget-object v7, p1, Lcom/google/c/a/a/b/b/a/a/d;->g:Ljava/lang/String;

    iget v8, p1, Lcom/google/c/a/a/b/b/a/a/d;->i:I

    iget v9, p1, Lcom/google/c/a/a/b/b/a/a/d;->h:I

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/drive/realtime/internal/event/ReferenceShiftedDetails;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    .line 149
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/ReferenceShiftedDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto/16 :goto_0

    .line 151
    :cond_8
    instance-of v6, p1, Lcom/google/c/a/a/b/b/a/a/b;

    if-eqz v6, :cond_a

    .line 152
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/b;

    .line 153
    iget-object v6, p1, Lcom/google/c/a/a/b/b/a/a/b;->h:Ljava/util/List;

    .line 154
    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    invoke-interface {v5}, Lcom/google/android/gms/drive/realtime/internal/a/d;->a()I

    move-result v7

    .line 155
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/c/a/a/b/b/a/g;

    .line 156
    iget-object v9, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    invoke-interface {v5}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v9, v5}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/android/gms/drive/realtime/internal/a/d;Ljava/lang/Object;)I

    goto :goto_3

    .line 158
    :cond_9
    new-instance v5, Lcom/google/android/gms/drive/realtime/internal/event/ObjectChangedDetails;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-direct {v5, v7, v6}, Lcom/google/android/gms/drive/realtime/internal/event/ObjectChangedDetails;-><init>(II)V

    .line 160
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/event/ObjectChangedDetails;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto/16 :goto_0

    .line 163
    :cond_a
    invoke-interface {v5}, Lcom/google/c/a/a/b/b/a/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;->a(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/d;
    .locals 4

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/d;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "build() has already been called"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 71
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/a;->a()Ljava/util/List;

    move-result-object v0

    .line 72
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/a;->b()Ljava/util/Collection;

    move-result-object v1

    .line 74
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/a/a;

    .line 75
    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/d;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/c/a/a/b/b/a/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 77
    :cond_1
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/a/a;

    .line 78
    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/d;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/c/a/a/b/b/a/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEvent;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 80
    :cond_2
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/a;->d()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/d;->b:Ljava/util/List;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 81
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/d;->d:Z

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/a;->c()Lcom/google/c/a/a/b/b/a/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/q;->b()Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/d;->d:Z

    .line 82
    return-object p0
.end method

.method public final a()Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;
    .locals 5

    .prologue
    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/d;->e:Z

    .line 87
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/d;->a:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/d;->c:Lcom/google/android/gms/drive/realtime/internal/a/d;

    invoke-interface {v2}, Lcom/google/android/gms/drive/realtime/internal/a/d;->b()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/gms/drive/realtime/d;->d:Z

    iget-object v4, p0, Lcom/google/android/gms/drive/realtime/d;->b:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;-><init>(Ljava/util/List;Lcom/google/android/gms/common/data/DataHolder;ZLjava/util/List;)V

    return-object v0
.end method
