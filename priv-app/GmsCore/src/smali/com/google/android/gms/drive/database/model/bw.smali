.class public final Lcom/google/android/gms/drive/database/model/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/a/c;


# static fields
.field public static final a:Lcom/google/android/gms/drive/database/model/bw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    new-instance v0, Lcom/google/android/gms/drive/database/model/bw;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/model/bw;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bw;->a:Lcom/google/android/gms/drive/database/model/bw;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->a:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/by;->b:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->intValue()I

    move-result v5

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    :goto_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->f:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->g:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    new-instance v0, Lcom/google/android/gms/drive/database/model/bv;

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/database/model/bv;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;JIJ)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bx;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/bv;->d(J)V

    return-object v0

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
