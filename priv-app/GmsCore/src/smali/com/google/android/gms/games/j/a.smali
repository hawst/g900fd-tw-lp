.class public final Lcom/google/android/gms/games/j/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 27
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 51
    :goto_0
    return v0

    .line 35
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 36
    if-nez v0, :cond_1

    .line 37
    const-string v0, "PanoUtils"

    const-string v2, "isPano: getPackageManager() failed"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 38
    goto :goto_0

    .line 40
    :cond_1
    const-string v3, "android.software.leanback"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    .line 42
    const-string v0, "uimode"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 44
    if-nez v0, :cond_2

    .line 45
    const-string v0, "PanoUtils"

    const-string v2, "isPano: getSystemService(UI_MODE_SERVICE) failed"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 46
    goto :goto_0

    .line 48
    :cond_2
    invoke-virtual {v0}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v0

    const/4 v4, 0x4

    if-ne v0, v4, :cond_3

    move v0, v2

    .line 51
    :goto_1
    if-eqz v3, :cond_4

    if-eqz v0, :cond_4

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    .line 48
    goto :goto_1

    :cond_4
    move v0, v1

    .line 51
    goto :goto_0
.end method
