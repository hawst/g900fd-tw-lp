.class final Lcom/google/android/gms/analytics/ak;
.super Ljava/util/TimerTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/analytics/ag;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/analytics/ag;)V
    .locals 0

    .prologue
    .line 440
    iput-object p1, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/analytics/ag;B)V
    .locals 0

    .prologue
    .line 440
    invoke-direct {p0, p1}, Lcom/google/android/gms/analytics/ak;-><init>(Lcom/google/android/gms/analytics/ag;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 443
    iget-object v0, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    iget-object v0, v0, Lcom/google/android/gms/analytics/ag;->b:Lcom/google/android/gms/analytics/aj;

    sget-object v1, Lcom/google/android/gms/analytics/aj;->b:Lcom/google/android/gms/analytics/aj;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    iget-object v0, v0, Lcom/google/android/gms/analytics/ag;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    iget-wide v0, v0, Lcom/google/android/gms/analytics/ag;->a:J

    iget-object v2, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    iget-wide v2, v2, Lcom/google/android/gms/analytics/ag;->f:J

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    iget-object v2, v2, Lcom/google/android/gms/analytics/ag;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 446
    const-string v0, "Disconnecting due to inactivity"

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/ag;->i()V

    .line 451
    :goto_0
    return-void

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    iget-object v0, v0, Lcom/google/android/gms/analytics/ag;->d:Ljava/util/Timer;

    new-instance v1, Lcom/google/android/gms/analytics/ak;

    iget-object v2, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    invoke-direct {v1, v2}, Lcom/google/android/gms/analytics/ak;-><init>(Lcom/google/android/gms/analytics/ag;)V

    iget-object v2, p0, Lcom/google/android/gms/analytics/ak;->a:Lcom/google/android/gms/analytics/ag;

    iget-wide v2, v2, Lcom/google/android/gms/analytics/ag;->f:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    goto :goto_0
.end method
