.class final Lcom/google/android/gms/feedback/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Class;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Object;

.field public f:Ljava/lang/String;

.field final synthetic g:Lcom/google/android/gms/feedback/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/b;ILjava/lang/Object;Ljava/lang/Class;ILjava/lang/String;)V
    .locals 8

    .prologue
    .line 109
    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/feedback/c;-><init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Object;Ljava/lang/Class;ILjava/lang/String;)V

    .line 110
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Object;Ljava/lang/Class;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/gms/feedback/c;->g:Lcom/google/android/gms/feedback/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p2, p0, Lcom/google/android/gms/feedback/c;->d:Ljava/lang/String;

    .line 100
    iput-object p4, p0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    .line 101
    iput-object p3, p0, Lcom/google/android/gms/feedback/c;->a:Ljava/lang/Integer;

    .line 102
    iput-object p5, p0, Lcom/google/android/gms/feedback/c;->b:Ljava/lang/Class;

    .line 103
    iput p6, p0, Lcom/google/android/gms/feedback/c;->c:I

    .line 104
    iput-object p7, p0, Lcom/google/android/gms/feedback/c;->f:Ljava/lang/String;

    .line 105
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 94
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, v3

    move-object v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/feedback/c;-><init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Object;Ljava/lang/Class;ILjava/lang/String;)V

    .line 95
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/feedback/c;-><init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/Object;)V

    .line 90
    return-void
.end method


# virtual methods
.method final a()Z
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/feedback/c;->b:Ljava/lang/Class;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/c;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
