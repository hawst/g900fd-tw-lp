.class final Lcom/google/android/gms/cast/media/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/b/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/media/y;

.field final synthetic b:Lcom/google/android/gms/cast/CastDevice;

.field final synthetic c:Lcom/google/android/gms/cast/media/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/media/y;Lcom/google/android/gms/cast/CastDevice;)V
    .locals 0

    .prologue
    .line 659
    iput-object p1, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iput-object p2, p0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iput-object p3, p0, Lcom/google/android/gms/cast/media/d;->b:Lcom/google/android/gms/cast/CastDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 915
    return-void
.end method

.method public final a(I)V
    .locals 5

    .prologue
    .line 719
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onConnectionFailed: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 721
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/media/h;-><init>(Lcom/google/android/gms/cast/media/d;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 745
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 5

    .prologue
    .line 846
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "onApplicationDisconnected: statusCode=%d, sessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 848
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/l;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/cast/media/l;-><init>(Lcom/google/android/gms/cast/media/d;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 863
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 793
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onApplicationConnected: appId=%s, sessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 795
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v6, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v0, Lcom/google/android/gms/cast/media/j;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/cast/media/j;-><init>(Lcom/google/android/gms/cast/media/d;Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 817
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/internal/ApplicationStatus;)V
    .locals 2

    .prologue
    .line 946
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/media/f;-><init>(Lcom/google/android/gms/cast/media/d;Lcom/google/android/gms/cast/internal/ApplicationStatus;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 967
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/internal/DeviceStatus;)V
    .locals 2

    .prologue
    .line 919
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/n;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/media/n;-><init>(Lcom/google/android/gms/cast/media/d;Lcom/google/android/gms/cast/internal/DeviceStatus;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 942
    return-void
.end method

.method public final a(Ljava/lang/String;DZ)V
    .locals 2

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/m;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/cast/media/m;-><init>(Lcom/google/android/gms/cast/media/d;Ljava/lang/String;D)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 890
    return-void
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 0

    .prologue
    .line 910
    return-void
.end method

.method public final a(Ljava/lang/String;JI)V
    .locals 0

    .prologue
    .line 905
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 662
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onCastNearbyAuthenticated"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 665
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/e;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/cast/media/e;-><init>(Lcom/google/android/gms/cast/media/d;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 684
    return-void
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 900
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 688
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onConnected. rejoinedApp: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 691
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/media/g;-><init>(Lcom/google/android/gms/cast/media/d;Z)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 715
    return-void
.end method

.method public final b(I)V
    .locals 5

    .prologue
    .line 749
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "CastDeviceController.Listener.onDisconnected: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 751
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/i;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/media/i;-><init>(Lcom/google/android/gms/cast/media/d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 787
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 895
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 821
    iget-object v0, p0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    new-instance v1, Lcom/google/android/gms/cast/media/k;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast/media/k;-><init>(Lcom/google/android/gms/cast/media/d;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 836
    return-void
.end method

.method public final d(I)V
    .locals 0

    .prologue
    .line 841
    return-void
.end method
