.class public Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ah;


# instance fields
.field final a:I

.field public final b:I

.field public final c:[Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:[Ljava/lang/String;

.field public final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/appdatasearch/ah;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ah;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ah;

    return-void
.end method

.method constructor <init>(II[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput p1, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->a:I

    .line 94
    iput p2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->b:I

    .line 95
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->c:[Ljava/lang/String;

    .line 96
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->d:Ljava/lang/String;

    .line 97
    iput-object p5, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->e:Ljava/lang/String;

    .line 98
    iput-object p6, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->f:[Ljava/lang/String;

    .line 99
    iput-object p7, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->g:Ljava/lang/String;

    .line 100
    return-void
.end method

.method public constructor <init>(I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 109
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;-><init>(II[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 116
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ah;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/ah;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/ah;->a(Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;Landroid/os/Parcel;)V

    .line 123
    return-void
.end method
