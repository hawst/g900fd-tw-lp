.class public final Lcom/google/android/gms/drive/a/n;
.super Lcom/google/android/gms/drive/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/a/w;


# instance fields
.field c:Lcom/google/android/gms/drive/DriveId;

.field public d:Lcom/google/android/gms/drive/DriveId;

.field private final e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/ac;)V
    .locals 3

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/gms/drive/a/e;->j:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2, p5}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    .line 74
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v2, "application/vnd.google-apps.folder"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 78
    :cond_0
    const-string v0, "application/vnd.google-apps.folder"

    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 81
    iput-object p4, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    .line 82
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/gms/drive/a/e;->j:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 87
    const-string v0, "metadata"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 89
    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    .line 94
    :goto_0
    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    .line 97
    :cond_0
    return-void

    .line 92
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/n;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method

.method private static a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 268
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    invoke-static {p2}, Lcom/google/android/gms/drive/metadata/sync/b/d;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/d;

    move-result-object v0

    .line 273
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 274
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/b/d;->a()Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/b/c;->b()Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Lcom/google/android/gms/drive/metadata/sync/b/c;Lcom/google/android/gms/drive/metadata/sync/b/c;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v2

    .line 277
    if-eqz v2, :cond_0

    .line 278
    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 281
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/b/d;->b()Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/b/c;->c()Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Lcom/google/android/gms/drive/metadata/sync/b/c;Lcom/google/android/gms/drive/metadata/sync/b/c;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_1

    .line 284
    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 287
    :cond_1
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    .line 288
    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/metadata/sync/a/k;-><init>(Ljava/lang/String;)V

    .line 289
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {p0, p1, v2, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v0

    .line 291
    invoke-virtual {v0, v6, v6}, Lcom/google/android/gms/drive/database/model/bb;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 292
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bb;->i()V

    goto :goto_0

    .line 294
    :cond_2
    return-void
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 192
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    .line 193
    invoke-super {p0, v0}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    .line 194
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/n;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v2

    .line 195
    const-string v0, "App got unauthorized before folder was created on server, should never happen."

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v4

    invoke-interface {v0, p1, v3, v1, v4}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;Z)Lcom/google/android/gms/drive/d/d;

    move-result-object v0

    .line 201
    invoke-interface {v0}, Lcom/google/android/gms/drive/d/d;->g()Ljava/lang/String;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 204
    :goto_0
    return-object v0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v1, v1, Lcom/android/volley/m;->a:I

    const/16 v3, 0x199

    if-ne v1, v3, :cond_0

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_1

    .line 204
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, p1, v0, v1}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-interface {v1, p1, v0, v2}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/d/e;

    goto :goto_0

    .line 203
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 206
    :cond_1
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 8

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    .line 110
    new-instance v0, Lcom/google/android/gms/drive/a/ab;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/a/ab;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0

    :cond_0
    move-object v7, v0

    .line 119
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    iget-wide v4, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v6

    move-object v1, p1

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_2

    .line 127
    new-instance v1, Lcom/google/android/gms/drive/a/t;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/a/t;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    throw v1

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    const-string v1, "root"

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    move-object v7, v0

    goto :goto_0

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 133
    const-string v1, "application/vnd.google-apps.folder"

    .line 134
    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->u()Lcom/google/android/gms/drive/database/t;

    move-result-object v3

    invoke-interface {p1, v2, v0, v1, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/t;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v0, p2, v1}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 139
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 142
    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/am;

    move-result-object v1

    .line 144
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/am;->i()V

    .line 147
    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ah;->C()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/Long;)V

    .line 150
    iget-wide v2, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Ljava/util/Set;)V

    .line 152
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 154
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    .line 155
    new-instance v0, Lcom/google/android/gms/drive/a/ao;

    iget-object v1, p2, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p2, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/n;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/ao;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)V
    .locals 6

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 179
    new-instance v1, Ljava/util/HashSet;

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :cond_0
    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 185
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/n;->b(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)Ljava/lang/String;

    move-result-object v1

    .line 186
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/n;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {v3, v1}, Lcom/google/android/gms/drive/database/model/ah;->c(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/drive/ai;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v0, 0x0

    :cond_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/database/model/ah;->j(Ljava/lang/String;)V

    :cond_3
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/drive/a/n;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;)V

    .line 187
    return-void

    .line 186
    :cond_4
    const-string v4, "CreateFolderAction"

    const-string v5, "ResourceId has already been set, probably by a metadata feed, so no need to set it again."

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/a/c;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 334
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/a/c;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v0

    .line 339
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/drive/a/w;

    if-eqz v1, :cond_2

    .line 340
    check-cast p1, Lcom/google/android/gms/drive/a/w;

    invoke-interface {p1}, Lcom/google/android/gms/drive/a/w;->k()Ljava/util/Set;

    move-result-object v1

    .line 341
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/n;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 345
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->b(Lcom/google/android/gms/drive/g/aw;)V

    return-void
.end method

.method protected final c(Lcom/google/android/gms/drive/g/aw;)V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/a/n;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 167
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    .line 169
    :cond_0
    return-void
.end method

.method public final bridge synthetic d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 363
    if-ne p0, p1, :cond_1

    .line 378
    :cond_0
    :goto_0
    return v0

    .line 364
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 366
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/a/n;

    .line 367
    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 368
    goto :goto_0

    .line 370
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 372
    goto :goto_0

    .line 370
    :cond_6
    iget-object v2, p1, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    if-nez v2, :cond_5

    .line 374
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 376
    goto :goto_0

    .line 374
    :cond_8
    iget-object v2, p1, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 350
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 351
    const-string v1, "metadata"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/a/ax;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 352
    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    .line 353
    const-string v1, "parent"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 355
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_1

    .line 356
    const-string v1, "newDriveId"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 358
    :cond_1
    return-object v0
.end method

.method public final bridge synthetic g()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    .prologue
    .line 56
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->g()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v0

    .line 384
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 385
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 386
    return v0

    :cond_1
    move v0, v1

    .line 384
    goto :goto_0
.end method

.method public final j()Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 316
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/util/Set;
    .locals 4

    .prologue
    .line 324
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 325
    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    .line 327
    iget-object v1, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 329
    :cond_0
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 391
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CreateFolderAction[%s, initialMetadata=%s, parent=%s, newDriveId=%s]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/n;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/a/n;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/drive/a/n;->c:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
