.class public Lcom/google/android/gms/social/location/DeviceLocationSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/social/location/d;

.field private static a:J

.field private static b:Lcom/google/android/gms/social/location/DeviceLocationSettings;


# instance fields
.field private final c:I

.field private final d:Ljava/lang/Boolean;

.field private final e:Lcom/google/android/gms/common/people/data/Audience;

.field private final f:Lcom/google/android/gms/common/people/data/Audience;

.field private final g:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/social/location/d;

    invoke-direct {v0}, Lcom/google/android/gms/social/location/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->CREATOR:Lcom/google/android/gms/social/location/d;

    return-void
.end method

.method constructor <init>(ILjava/lang/Boolean;Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c:I

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d:Ljava/lang/Boolean;

    .line 66
    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0, p3}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e:Lcom/google/android/gms/common/people/data/Audience;

    .line 68
    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0, p4}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->f:Lcom/google/android/gms/common/people/data/Audience;

    .line 70
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    iput-object p5, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->g:Ljava/util/List;

    .line 72
    return-void
.end method

.method public constructor <init>(ZLcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 88
    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    move-object v0, p0

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/social/location/DeviceLocationSettings;-><init>(ILjava/lang/Boolean;Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/List;)V

    .line 90
    return-void
.end method

.method public constructor <init>(ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 78
    const/4 v1, 0x2

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v4

    move-object v0, p0

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/social/location/DeviceLocationSettings;-><init>(ILjava/lang/Boolean;Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/List;)V

    .line 82
    return-void
.end method

.method public static a(Lcom/google/android/gms/social/location/DeviceLocationSettings;)V
    .locals 2

    .prologue
    .line 125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->a:J

    .line 126
    sput-object p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b:Lcom/google/android/gms/social/location/DeviceLocationSettings;

    .line 127
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c:I

    return v0
.end method

.method public final b()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d:Ljava/lang/Boolean;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->e:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->f:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/social/location/DeviceLocationSettings;->g:Ljava/util/List;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 118
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/social/location/d;->a(Lcom/google/android/gms/social/location/DeviceLocationSettings;Landroid/os/Parcel;I)V

    .line 119
    return-void
.end method
