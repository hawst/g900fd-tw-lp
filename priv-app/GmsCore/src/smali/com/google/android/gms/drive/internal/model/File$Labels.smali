.class public final Lcom/google/android/gms/drive/internal/model/File$Labels;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/o;

.field private static final g:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Z

.field d:Z

.field e:Z

.field f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 2210
    new-instance v0, Lcom/google/android/gms/drive/internal/model/o;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->CREATOR:Lcom/google/android/gms/drive/internal/model/o;

    .line 2242
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2245
    sput-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->g:Ljava/util/HashMap;

    const-string v1, "restricted"

    const-string v2, "restricted"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2246
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->g:Ljava/util/HashMap;

    const-string v1, "starred"

    const-string v2, "starred"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2247
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->g:Ljava/util/HashMap;

    const-string v1, "trashed"

    const-string v2, "trashed"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2248
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->g:Ljava/util/HashMap;

    const-string v1, "viewed"

    const-string v2, "viewed"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2249
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2292
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 2293
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->b:I

    .line 2294
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->a:Ljava/util/Set;

    .line 2295
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IZZZZ)V
    .locals 0

    .prologue
    .line 2305
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 2306
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->a:Ljava/util/Set;

    .line 2307
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->b:I

    .line 2308
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->c:Z

    .line 2309
    iput-boolean p4, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->d:Z

    .line 2310
    iput-boolean p5, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->e:Z

    .line 2311
    iput-boolean p6, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->f:Z

    .line 2312
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/gms/drive/internal/model/File$Labels;
    .locals 2

    .prologue
    .line 2405
    iput-boolean p1, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->d:Z

    .line 2406
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->a:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2407
    return-object p0
.end method

.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 2253
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2498
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2499
    packed-switch v0, :pswitch_data_0

    .line 2513
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2501
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->c:Z

    .line 2516
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2517
    return-void

    .line 2504
    :pswitch_1
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->d:Z

    goto :goto_0

    .line 2507
    :pswitch_2
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->e:Z

    goto :goto_0

    .line 2510
    :pswitch_3
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->f:Z

    goto :goto_0

    .line 2499
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 2452
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Z)Lcom/google/android/gms/drive/internal/model/File$Labels;
    .locals 2

    .prologue
    .line 2423
    iput-boolean p1, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->f:Z

    .line 2424
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->a:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2425
    return-object p0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2457
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2467
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2459
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 2465
    :goto_0
    return-object v0

    .line 2461
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2463
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2465
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2457
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2442
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2337
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->c:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 2352
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->d:Z

    return v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2447
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 2367
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->e:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2431
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->CREATOR:Lcom/google/android/gms/drive/internal/model/o;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 2382
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File$Labels;->f:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2534
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/File$Labels;

    if-nez v0, :cond_0

    move v0, v1

    .line 2565
    :goto_0
    return v0

    .line 2539
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 2540
    goto :goto_0

    .line 2543
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/File$Labels;

    .line 2544
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2545
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2546
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2548
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2550
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2555
    goto :goto_0

    .line 2558
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2560
    goto :goto_0

    :cond_5
    move v0, v2

    .line 2565
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2521
    const/4 v0, 0x0

    .line 2522
    sget-object v1, Lcom/google/android/gms/drive/internal/model/File$Labels;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2523
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2524
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 2525
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File$Labels;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 2527
    goto :goto_0

    .line 2528
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2436
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File$Labels;->CREATOR:Lcom/google/android/gms/drive/internal/model/o;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/model/o;->a(Lcom/google/android/gms/drive/internal/model/File$Labels;Landroid/os/Parcel;)V

    .line 2437
    return-void
.end method
