.class public final Lcom/google/android/gms/games/ui/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 20
    invoke-virtual {p0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 21
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 23
    invoke-virtual {v0, p2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 24
    if-eqz v0, :cond_0

    .line 25
    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 27
    :cond_0
    invoke-virtual {v1, p1, p2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 28
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 29
    return-void
.end method

.method public static dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 37
    invoke-virtual {v0, p1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 38
    if-eqz v0, :cond_0

    .line 39
    check-cast v0, Landroid/support/v4/app/m;

    invoke-virtual {v0}, Landroid/support/v4/app/m;->dismiss()V

    .line 41
    :cond_0
    return-void
.end method
