.class public final Lcom/google/android/gms/plus/service/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Landroid/content/Context;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 62
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 63
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 64
    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 65
    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/c/e/b/a/a/b;
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 36
    if-nez p2, :cond_0

    .line 37
    const/16 v0, 0x50

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    .line 40
    :cond_0
    new-instance v2, Lf/a/a/b;

    invoke-direct {v2}, Lf/a/a/b;-><init>()V

    .line 42
    invoke-static {p0}, Lcom/google/android/gms/plus/service/b/a;->a(Landroid/content/Context;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lf/a/a/b;->j:Ljava/lang/Integer;

    .line 43
    iput-object p1, v2, Lf/a/a/b;->i:Ljava/lang/String;

    .line 45
    new-instance v3, Lcom/google/ac/a/a/b;

    invoke-direct {v3}, Lcom/google/ac/a/a/b;-><init>()V

    .line 46
    iput-object p2, v3, Lcom/google/ac/a/a/b;->b:Ljava/lang/Integer;

    .line 47
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/ac/a/a/b;->a:Ljava/lang/Integer;

    .line 49
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/ac/a/a/b;->c:Ljava/lang/Integer;

    .line 51
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v3, Lcom/google/ac/a/a/b;->d:Ljava/lang/Integer;

    .line 53
    iput-object v3, v2, Lf/a/a/b;->n:Lcom/google/ac/a/a/b;

    .line 54
    new-instance v0, Lcom/google/c/e/b/a/a/b;

    invoke-direct {v0}, Lcom/google/c/e/b/a/a/b;-><init>()V

    .line 55
    iput-object v2, v0, Lcom/google/c/e/b/a/a/b;->b:Lf/a/a/b;

    .line 57
    return-object v0

    :cond_1
    move v0, v1

    .line 47
    goto :goto_0
.end method
