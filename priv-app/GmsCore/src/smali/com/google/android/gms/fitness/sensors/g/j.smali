.class public final Lcom/google/android/gms/fitness/sensors/g/j;
.super Lcom/google/android/gms/fitness/sensors/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/wearable/f;
.implements Lcom/google/android/gms/wearable/p;
.implements Lcom/google/android/gms/wearable/v;


# static fields
.field private static final a:[B


# instance fields
.field private final b:Lcom/google/android/gms/fitness/sensors/a;

.field private final c:Lcom/google/android/gms/common/api/v;

.field private final d:Lcom/google/android/gms/wearable/d;

.field private final e:Lcom/google/android/gms/wearable/o;

.field private final f:Lcom/google/android/gms/wearable/t;

.field private final g:Ljava/util/Map;

.field private final h:Ljava/util/Map;

.field private final i:Ljava/util/Map;

.field private final j:Ljava/util/Map;

.field private final k:Ljava/util/Map;

.field private final l:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final m:Lcom/google/android/gms/fitness/data/Device;

.field private n:Lcom/google/android/gms/wearable/s;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/fitness/sensors/g/j;->a:[B

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;)V
    .locals 6

    .prologue
    .line 108
    sget-object v3, Lcom/google/android/gms/wearable/z;->b:Lcom/google/android/gms/wearable/o;

    sget-object v4, Lcom/google/android/gms/wearable/z;->a:Lcom/google/android/gms/wearable/d;

    sget-object v5, Lcom/google/android/gms/wearable/z;->c:Lcom/google/android/gms/wearable/t;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/sensors/g/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/wearable/o;Lcom/google/android/gms/wearable/d;Lcom/google/android/gms/wearable/t;)V

    .line 109
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/wearable/o;Lcom/google/android/gms/wearable/d;Lcom/google/android/gms/wearable/t;)V
    .locals 2

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/b/a;-><init>()V

    .line 91
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    .line 93
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->h:Ljava/util/Map;

    .line 95
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->i:Ljava/util/Map;

    .line 97
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->j:Ljava/util/Map;

    .line 99
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->k:Ljava/util/Map;

    .line 102
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 115
    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->b:Lcom/google/android/gms/fitness/sensors/a;

    .line 116
    iput-object p4, p0, Lcom/google/android/gms/fitness/sensors/g/j;->d:Lcom/google/android/gms/wearable/d;

    .line 117
    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->e:Lcom/google/android/gms/wearable/o;

    .line 118
    iput-object p5, p0, Lcom/google/android/gms/fitness/sensors/g/j;->f:Lcom/google/android/gms/wearable/t;

    .line 119
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Device;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->m:Lcom/google/android/gms/fitness/data/Device;

    .line 121
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/wearable/z;->g:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    .line 126
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/sensors/g/j;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/sensors/g/j;Lcom/google/android/gms/wearable/s;)Lcom/google/android/gms/wearable/s;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    return-object p1
.end method

.method private static a(Ljava/util/Collection;Lcom/google/android/gms/fitness/data/Device;)Ljava/util/List;
    .locals 4

    .prologue
    .line 172
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 173
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 174
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v3

    invoke-virtual {p1, v3}, Lcom/google/android/gms/fitness/data/Device;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 175
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 178
    :cond_1
    return-object v1
.end method

.method private a(Lcom/google/android/gms/fitness/data/Device;)V
    .locals 3

    .prologue
    .line 447
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 448
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    .line 449
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 450
    if-eqz v0, :cond_0

    .line 451
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 452
    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 453
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 454
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/Device;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 461
    :cond_2
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 5

    .prologue
    .line 433
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 434
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v3

    .line 435
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 436
    if-nez v1, :cond_0

    .line 437
    new-instance v1, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    .line 438
    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-interface {v4, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 440
    :cond_0
    invoke-virtual {v1, v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 441
    const-string v1, "adding data source %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 443
    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/fitness/sensors/g/j;)Lcom/google/android/gms/wearable/d;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->d:Lcom/google/android/gms/wearable/d;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 373
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/g/j;->b(I)V

    goto :goto_0

    .line 375
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->k:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/l;

    .line 483
    if-eqz v0, :cond_0

    .line 484
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->b:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/data/l;)Z

    .line 488
    :goto_0
    return-void

    .line 486
    :cond_0
    const-string v0, "There is no listener for registration Id [%d]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private c()Ljava/util/List;
    .locals 5

    .prologue
    .line 395
    invoke-static {}, Lcom/google/android/gms/fitness/sensors/local/d;->values()[Lcom/google/android/gms/fitness/sensors/local/d;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v1, v0

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/sensors/local/d;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->P:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->h:Lcom/google/android/gms/fitness/data/DataType;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->d:Lcom/google/android/gms/fitness/data/DataType;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 396
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 397
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    .line 398
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->b:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v3, v0}, Lcom/google/android/gms/fitness/sensors/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 400
    :cond_2
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 189
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    if-eqz v0, :cond_0

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->h:Ljava/util/Map;

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Device;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/data/Device;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v4, "Request for wrong device %s, peer %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v1

    aput-object v0, v5, v2

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    goto :goto_0

    :cond_2
    const-string v3, "WearablesAdapter:Registering for wearable %s"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v1

    new-instance v3, Lcom/google/android/gms/wearable/m;

    invoke-direct {v3}, Lcom/google/android/gms/wearable/m;-><init>()V

    const-string v0, "sensor_registration_request_id"

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/wearable/m;->a(Ljava/lang/String;I)V

    const-string v4, "sensor_registration_request"

    sget-object v0, Lcom/google/android/gms/fitness/sensors/g/b;->f:Lcom/google/android/gms/fitness/sensors/g/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/sensors/g/a;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/m;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/wearable/m;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/m;)V

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->e:Lcom/google/android/gms/wearable/o;

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    iget-object v5, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    invoke-interface {v5}, Lcom/google/android/gms/wearable/s;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "/WearablesAdapter/registerSensor"

    invoke-virtual {v3}, Lcom/google/android/gms/wearable/m;->a()[B

    move-result-object v3

    invoke-interface {v0, v4, v5, v6, v3}, Lcom/google/android/gms/wearable/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->i:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->j:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move v0, v2

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 131
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 282
    const-string v0, "WearablesAdapter:GoogleApiClient could not connect due to: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 283
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/i;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 287
    invoke-static {p1}, Lcom/google/android/gms/common/data/v;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    .line 288
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/h;

    .line 289
    invoke-interface {v0}, Lcom/google/android/gms/wearable/h;->d()I

    move-result v1

    if-ne v1, v6, :cond_0

    .line 290
    invoke-interface {v0}, Lcom/google/android/gms/wearable/h;->a()Lcom/google/android/gms/wearable/j;

    move-result-object v0

    .line 291
    invoke-interface {v0}, Lcom/google/android/gms/wearable/j;->a()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    .line 292
    const-string v3, "WearablesAdapter:onDataItemChanged DataItem %s , Path %s "

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 294
    const-string v3, "/WearablesAdapter/sensor_data_point"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    invoke-static {v0}, Lcom/google/android/gms/wearable/n;->a(Lcom/google/android/gms/wearable/j;)Lcom/google/android/gms/wearable/n;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/wearable/n;->a:Lcom/google/android/gms/wearable/m;

    const-string v3, "sensor_registration_request_id"

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wearable/m;->d(Ljava/lang/String;)I

    move-result v1

    sget-object v3, Lcom/google/android/gms/fitness/sensors/g/b;->b:Lcom/google/android/gms/fitness/sensors/g/a;

    iget-object v0, v0, Lcom/google/android/gms/wearable/n;->a:Lcom/google/android/gms/wearable/m;

    const-string v4, "data_point"

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wearable/m;->i(Ljava/lang/String;)Lcom/google/android/gms/wearable/m;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/google/android/gms/fitness/sensors/g/a;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->j:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v3

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/data/l;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "Could not send remote DataPoint to local registration "

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 299
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/wearable/r;)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 303
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->a()Ljava/lang/String;

    move-result-object v0

    .line 304
    const-string v1, "WearablesAdapter:onMessageReceived with %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 305
    const-string v1, "/WearablesAdapter/registerSensor"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 306
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/m;->a([B)Lcom/google/android/gms/wearable/m;

    move-result-object v0

    .line 307
    const-string v1, "sensor_registration_request_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/m;->d(Ljava/lang/String;)I

    move-result v1

    .line 308
    new-instance v2, Lcom/google/android/gms/fitness/sensors/g/l;

    invoke-direct {v2, p0, v1, v5}, Lcom/google/android/gms/fitness/sensors/g/l;-><init>(Lcom/google/android/gms/fitness/sensors/g/j;IB)V

    .line 309
    sget-object v3, Lcom/google/android/gms/fitness/sensors/g/b;->f:Lcom/google/android/gms/fitness/sensors/g/a;

    const-string v4, "sensor_registration_request"

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wearable/m;->i(Ljava/lang/String;)Lcom/google/android/gms/wearable/m;

    move-result-object v0

    invoke-interface {v3, v0}, Lcom/google/android/gms/fitness/sensors/g/a;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    .line 312
    if-eqz v0, :cond_0

    .line 313
    const-string v3, "SensorRegistrationRequest: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 314
    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a(Lcom/google/android/gms/fitness/data/l;)V

    .line 317
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->m:Lcom/google/android/gms/fitness/data/Device;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/fitness/data/Device;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "Registration request DataSource does not have the local device."

    invoke-static {v3, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 321
    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->k:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->b:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    const-string v1, "/WearablesAdapter/unregisterSensor"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 325
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/m;->a([B)Lcom/google/android/gms/wearable/m;

    move-result-object v0

    .line 326
    const-string v1, "sensor_registration_request_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/m;->d(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/g/j;->b(I)V

    goto :goto_0

    .line 327
    :cond_2
    const-string v1, "/WearablesAdapter/requestDataSources"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 328
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->c()Ljava/lang/String;

    move-result-object v1

    const-string v0, "Setting Local Sensor Sources."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->e:Lcom/google/android/gms/wearable/o;

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    const-string v4, "/WearablesAdapter/sendDataSources"

    new-instance v5, Lcom/google/android/gms/fitness/sensors/g/i;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/g/j;->c()Ljava/util/List;

    move-result-object v0

    iget-object v6, p0, Lcom/google/android/gms/fitness/sensors/g/j;->m:Lcom/google/android/gms/fitness/data/Device;

    invoke-direct {v5, v0, v6}, Lcom/google/android/gms/fitness/sensors/g/i;-><init>(Ljava/util/List;Lcom/google/android/gms/fitness/data/Device;)V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, v5, Lcom/google/android/gms/fitness/sensors/g/i;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    sget-object v8, Lcom/google/android/gms/fitness/sensors/g/b;->c:Lcom/google/android/gms/fitness/sensors/g/a;

    invoke-interface {v8, v0}, Lcom/google/android/gms/fitness/sensors/g/a;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v7, Lcom/google/android/gms/wearable/m;

    invoke-direct {v7}, Lcom/google/android/gms/wearable/m;-><init>()V

    const-string v8, "local_device"

    sget-object v0, Lcom/google/android/gms/fitness/sensors/g/b;->e:Lcom/google/android/gms/fitness/sensors/g/a;

    iget-object v5, v5, Lcom/google/android/gms/fitness/sensors/g/i;->b:Lcom/google/android/gms/fitness/data/Device;

    invoke-interface {v0, v5}, Lcom/google/android/gms/fitness/sensors/g/a;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/m;

    invoke-virtual {v7, v8, v0}, Lcom/google/android/gms/wearable/m;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/m;)V

    const-string v0, "all_data_sources"

    invoke-virtual {v7, v0, v6}, Lcom/google/android/gms/wearable/m;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v7}, Lcom/google/android/gms/wearable/m;->a()[B

    move-result-object v0

    invoke-interface {v2, v3, v1, v4, v0}, Lcom/google/android/gms/wearable/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;

    goto :goto_0

    .line 329
    :cond_4
    const-string v1, "/WearablesAdapter/sendDataSources"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    invoke-interface {p1}, Lcom/google/android/gms/wearable/r;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/m;->a([B)Lcom/google/android/gms/wearable/m;

    move-result-object v0

    .line 331
    const-string v1, "Received SourcesDataMap %s"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {v0}, Lcom/google/android/gms/fitness/sensors/g/i;->a(Lcom/google/android/gms/wearable/m;)Lcom/google/android/gms/fitness/sensors/g/i;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/fitness/sensors/g/i;->b:Lcom/google/android/gms/fitness/data/Device;

    const-string v2, "Remote device %s"

    new-array v3, v6, [Ljava/lang/Object;

    aput-object v1, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->m:Lcom/google/android/gms/fitness/data/Device;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/fitness/data/Device;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v0, "Received /sources_data request from local device"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->h:Ljava/util/Map;

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/sensors/g/j;->a(Lcom/google/android/gms/fitness/data/Device;)V

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/g/i;->a:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/g/j;->a(Ljava/util/List;)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/wearable/s;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 337
    const-string v0, "WearablesAdapter:onPeerConnected"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 340
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/g/j;->b()V

    .line 341
    const-string v0, "currentPeer is now %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 342
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    .line 343
    const-string v0, "Requesting data sources from peer %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->e:Lcom/google/android/gms/wearable/o;

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {p1}, Lcom/google/android/gms/wearable/s;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "/WearablesAdapter/requestDataSources"

    sget-object v4, Lcom/google/android/gms/fitness/sensors/g/j;->a:[B

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/wearable/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->j:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/sensors/g/j;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->j:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 346
    :cond_0
    const-string v0, "Previous node %s is reconnected."

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 348
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;)Z
    .locals 3

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    .line 141
    if-eqz v0, :cond_1

    .line 142
    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 143
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 144
    const/4 v0, 0x1

    .line 148
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataType;)Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/l;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 232
    const-string v0, "WearablesAdapter::unregister"

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 249
    :goto_0
    return v0

    .line 237
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 238
    if-nez v0, :cond_2

    .line 239
    const-string v0, "Could not find registration for this listener."

    new-array v2, v1, [Ljava/lang/Object;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v1

    .line 240
    goto :goto_0

    .line 243
    :cond_2
    new-instance v1, Lcom/google/android/gms/wearable/m;

    invoke-direct {v1}, Lcom/google/android/gms/wearable/m;-><init>()V

    .line 244
    const-string v2, "sensor_registration_request_id"

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/wearable/m;->a(Ljava/lang/String;I)V

    .line 245
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->e:Lcom/google/android/gms/wearable/o;

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    iget-object v4, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    invoke-interface {v4}, Lcom/google/android/gms/wearable/s;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "/WearablesAdapter/unregisterSensor"

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/m;->a()[B

    move-result-object v1

    invoke-interface {v2, v3, v4, v5, v1}, Lcom/google/android/gms/wearable/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[B)Lcom/google/android/gms/common/api/am;

    .line 247
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->i:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 248
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->j:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->g:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 154
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WearablesAdapter:findDataSources full results: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    if-nez v1, :cond_0

    .line 157
    const-string v1, "findDataSources with no connected wearable peer %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 158
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 165
    :goto_0
    return-object v0

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->h:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/data/Device;

    .line 162
    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    .line 163
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 165
    :cond_2
    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/sensors/g/j;->a(Ljava/util/Collection;Lcom/google/android/gms/fitness/data/Device;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/wearable/s;)V
    .locals 2

    .prologue
    .line 366
    const-string v0, "WearablesAdapter:onPeerDisconnected."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 367
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/g/j;->b()V

    .line 368
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->n:Lcom/google/android/gms/wearable/s;

    .line 369
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 255
    const-string v0, "WearablesAdapter:GMSClient has connected."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/g/j;->f:Lcom/google/android/gms/wearable/t;

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/j;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wearable/t;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/sensors/g/k;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/sensors/g/k;-><init>(Lcom/google/android/gms/fitness/sensors/g/j;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 272
    return-void
.end method

.method public final f_(I)V
    .locals 4

    .prologue
    .line 276
    const-string v0, "WearablesAdapter:GMSClient connection has suspended with cause %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 277
    return-void
.end method
