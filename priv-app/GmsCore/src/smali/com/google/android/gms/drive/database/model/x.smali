.class final Lcom/google/android/gms/drive/database/model/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

.field final b:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 43
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/x;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    .line 46
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/x;->b:Ljava/lang/Long;

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/database/model/x;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/x;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/x;->b:Ljava/lang/Long;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/w;->a(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/Long;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
