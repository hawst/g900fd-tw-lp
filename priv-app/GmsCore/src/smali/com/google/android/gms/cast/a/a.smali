.class public final Lcom/google/android/gms/cast/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;

.field public static final l:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    const-string v0, "gms:cast:verbose_logging_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 24
    const-string v0, "gms:cast:debug_logging_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 28
    const-string v0, "gms:cast:dogfood_logging_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 32
    const-string v0, "gms:cast:wifi_lock_enabled"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->d:Lcom/google/android/gms/common/a/d;

    .line 36
    const-string v0, "gms:cast:wake_lock_enabled"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->e:Lcom/google/android/gms/common/a/d;

    .line 41
    const-string v0, "gms:cast:wake_lock_max_hold_time"

    const-wide/16 v2, 0x3e8

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->f:Lcom/google/android/gms/common/a/d;

    .line 45
    const-string v0, "gms:cast:mirroring_app_id"

    const-string v1, "674A0243"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->g:Lcom/google/android/gms/common/a/d;

    .line 50
    const-string v0, "gms:cast:remote_display_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->h:Lcom/google/android/gms/common/a/d;

    .line 55
    const-string v0, "gms:cast:mirroring_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->i:Lcom/google/android/gms/common/a/d;

    .line 59
    const-string v0, "gms:cast:ambient_state_app_ids"

    const-string v1, "00000000-0000-0000-0000-000000000000 E8C28D3C"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->j:Lcom/google/android/gms/common/a/d;

    .line 64
    const-string v0, "gms:cast:analytics_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->k:Lcom/google/android/gms/common/a/d;

    .line 67
    const-string v0, "gms:cast:high_frequency_analytic_actions_enabled"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/a/a;->l:Lcom/google/android/gms/common/a/d;

    return-void
.end method
