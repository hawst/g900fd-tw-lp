.class public final Lcom/google/android/gms/identity/accounts/service/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/identity/accounts/service/d;


# instance fields
.field private final a:Lcom/google/android/gms/identity/accounts/service/l;

.field private final b:Lcom/google/android/gms/identity/accounts/api/AccountData;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/identity/accounts/service/l;Lcom/google/android/gms/identity/accounts/api/AccountData;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/identity/accounts/service/k;->b:Lcom/google/android/gms/identity/accounts/api/AccountData;

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/service/k;->a:Lcom/google/android/gms/identity/accounts/service/l;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/k;->a:Lcom/google/android/gms/identity/accounts/service/l;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/identity/accounts/service/l;->a([B)V

    .line 41
    return-void
.end method

.method public final a(Lcom/google/android/gms/identity/accounts/security/a;)V
    .locals 5

    .prologue
    .line 35
    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/service/k;->a:Lcom/google/android/gms/identity/accounts/service/l;

    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/k;->b:Lcom/google/android/gms/identity/accounts/api/AccountData;

    const-string v2, "Account data must not be null."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v2, 0x10

    new-array v2, v2, [B

    iget-object v3, p1, Lcom/google/android/gms/identity/accounts/security/a;->a:Ljava/security/SecureRandom;

    invoke-virtual {v3, v2}, Ljava/security/SecureRandom;->nextBytes([B)V

    invoke-virtual {p1, v0, v2}, Lcom/google/android/gms/identity/accounts/security/a;->a(Lcom/google/android/gms/identity/accounts/api/AccountData;[B)[B

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/identity/accounts/service/l;->a([B)V

    .line 36
    return-void

    .line 35
    :cond_0
    invoke-virtual {p1, v2, v0}, Lcom/google/android/gms/identity/accounts/security/a;->a([B[B)[B

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;

    invoke-direct {v4, v0, v2, v3}, Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;-><init>([B[B[B)V

    invoke-static {v4}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;)[B

    move-result-object v0

    goto :goto_0
.end method
