.class final Lcom/google/android/gms/plus/plusone/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:I

.field final synthetic i:Lcom/google/android/gms/plus/plusone/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/plusone/e;Landroid/widget/FrameLayout;)V
    .locals 9

    .prologue
    const/16 v1, 0x30

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 679
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/o;->i:Lcom/google/android/gms/plus/plusone/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 671
    iput v2, p0, Lcom/google/android/gms/plus/plusone/o;->c:I

    .line 673
    iput v2, p0, Lcom/google/android/gms/plus/plusone/o;->e:I

    .line 681
    invoke-virtual {p2, v2, v2}, Landroid/widget/FrameLayout;->measure(II)V

    .line 684
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 685
    iget-object v4, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v4, v0}, Landroid/widget/FrameLayout;->getLocationOnScreen([I)V

    .line 686
    aget v5, v0, v2

    .line 687
    aget v6, v0, v3

    .line 691
    iget-object v0, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v6

    iget-object v4, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getPaddingBottom()I

    move-result v4

    sub-int v4, v0, v4

    .line 693
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    add-int v7, v4, v0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0xb

    if-lt v0, v8, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v7

    invoke-static {p1}, Lcom/google/android/gms/plus/plusone/e;->k(Lcom/google/android/gms/plus/plusone/e;)Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getHeight()I

    move-result v7

    if-le v0, v7, :cond_1

    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/o;->a:Z

    .line 700
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v5

    invoke-static {p1}, Lcom/google/android/gms/plus/plusone/e;->k(Lcom/google/android/gms/plus/plusone/e;)Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/Display;->getWidth()I

    move-result v7

    if-ge v0, v7, :cond_2

    move v0, v3

    .line 706
    :goto_2
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v5

    iget-object v8, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v8}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int/2addr v7, v8

    invoke-static {p1}, Lcom/google/android/gms/plus/plusone/e;->k(Lcom/google/android/gms/plus/plusone/e;)Landroid/view/Display;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/Display;->getWidth()I

    move-result v8

    if-ge v7, v8, :cond_3

    .line 714
    :goto_3
    iget-object v7, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v7}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    invoke-static {p1}, Lcom/google/android/gms/plus/plusone/e;->l(Lcom/google/android/gms/plus/plusone/e;)Landroid/widget/ImageView;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v7, v8

    .line 716
    if-eqz v0, :cond_5

    .line 717
    iget-boolean v8, p0, Lcom/google/android/gms/plus/plusone/o;->a:Z

    if-eqz v8, :cond_4

    .line 718
    const/16 v1, 0x50

    iput v1, p0, Lcom/google/android/gms/plus/plusone/o;->f:I

    .line 722
    :goto_4
    iput v7, p0, Lcom/google/android/gms/plus/plusone/o;->b:I

    .line 723
    iput v2, p0, Lcom/google/android/gms/plus/plusone/o;->d:I

    .line 743
    :goto_5
    if-eqz v0, :cond_9

    .line 744
    iput v5, p0, Lcom/google/android/gms/plus/plusone/o;->g:I

    .line 754
    :goto_6
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/o;->a:Z

    if-eqz v0, :cond_b

    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v0

    sub-int v0, v6, v0

    iget-object v1, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    :goto_7
    iput v0, p0, Lcom/google/android/gms/plus/plusone/o;->h:I

    .line 758
    return-void

    :cond_0
    move v0, v2

    .line 693
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    .line 700
    goto :goto_2

    :cond_3
    move v3, v2

    .line 706
    goto :goto_3

    .line 720
    :cond_4
    iput v1, p0, Lcom/google/android/gms/plus/plusone/o;->f:I

    goto :goto_4

    .line 724
    :cond_5
    if-eqz v3, :cond_7

    .line 725
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/o;->a:Z

    if-eqz v1, :cond_6

    .line 726
    const/16 v1, 0x51

    iput v1, p0, Lcom/google/android/gms/plus/plusone/o;->f:I

    .line 730
    :goto_8
    iput v2, p0, Lcom/google/android/gms/plus/plusone/o;->b:I

    .line 731
    iput v2, p0, Lcom/google/android/gms/plus/plusone/o;->d:I

    goto :goto_5

    .line 728
    :cond_6
    const/16 v1, 0x31

    iput v1, p0, Lcom/google/android/gms/plus/plusone/o;->f:I

    goto :goto_8

    .line 733
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/o;->a:Z

    if-eqz v1, :cond_8

    .line 734
    const/16 v1, 0x55

    iput v1, p0, Lcom/google/android/gms/plus/plusone/o;->f:I

    .line 738
    :goto_9
    iput v2, p0, Lcom/google/android/gms/plus/plusone/o;->b:I

    .line 739
    iput v7, p0, Lcom/google/android/gms/plus/plusone/o;->d:I

    goto :goto_5

    .line 736
    :cond_8
    const/16 v1, 0x35

    iput v1, p0, Lcom/google/android/gms/plus/plusone/o;->f:I

    goto :goto_9

    .line 745
    :cond_9
    if-eqz v3, :cond_a

    .line 746
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, v5, v0

    iget-object v1, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/plus/plusone/o;->g:I

    goto :goto_6

    .line 751
    :cond_a
    invoke-virtual {p2}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v0

    sub-int v0, v5, v0

    iget-object v1, p1, Lcom/google/android/gms/plus/plusone/e;->b:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/plus/plusone/o;->g:I

    goto :goto_6

    :cond_b
    move v0, v4

    .line 754
    goto :goto_7
.end method
