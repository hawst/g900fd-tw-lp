.class public final Lcom/google/android/gms/plus/audience/z;
.super Lcom/google/android/gms/common/ui/widget/e;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/plus/audience/o;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;)V
    .locals 0

    .prologue
    .line 1035
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/z;->c:Lcom/google/android/gms/plus/audience/o;

    .line 1036
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/ui/widget/e;-><init>(Lcom/google/android/gms/common/ui/widget/a;Lcom/google/android/gms/common/data/d;)V

    .line 1037
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;II)V
    .locals 2

    .prologue
    .line 1031
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/z;->c:Lcom/google/android/gms/plus/audience/o;

    .line 1032
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/common/ui/widget/e;-><init>(Lcom/google/android/gms/common/ui/widget/a;Lcom/google/android/gms/common/data/d;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 1033
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 13

    .prologue
    .line 1030
    move-object v1, p1

    check-cast v1, Lcom/google/android/gms/people/model/j;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/z;->c:Lcom/google/android/gms/plus/audience/o;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/j;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1}, Lcom/google/android/gms/people/model/j;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lcom/google/android/gms/people/model/j;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/plus/audience/z;->c:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v7}, Lcom/google/android/gms/plus/audience/o;->d(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/plus/audience/bg;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v7

    invoke-static {v1}, Lcom/google/android/gms/common/people/data/e;->a(Lcom/google/android/gms/people/model/j;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    sget v8, Lcom/google/android/gms/l;->dw:I

    const/4 v11, 0x0

    const/4 v12, 0x1

    move-object v9, p2

    move/from16 v10, p4

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/gms/plus/audience/o;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/z;->c:Lcom/google/android/gms/plus/audience/o;

    iget v0, v0, Lcom/google/android/gms/plus/audience/o;->i:I

    return v0
.end method
