.class public final Lcom/google/android/gms/blescanner/compat/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:I

.field private c:I

.field private d:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    iput v1, p0, Lcom/google/android/gms/blescanner/compat/ab;->c:I

    .line 149
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/blescanner/compat/ab;->a:I

    .line 150
    iput v1, p0, Lcom/google/android/gms/blescanner/compat/ab;->b:I

    .line 151
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/blescanner/compat/ab;->d:J

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/blescanner/compat/ScanSettings;
    .locals 7

    .prologue
    .line 233
    new-instance v0, Lcom/google/android/gms/blescanner/compat/ScanSettings;

    iget v1, p0, Lcom/google/android/gms/blescanner/compat/ab;->c:I

    iget v2, p0, Lcom/google/android/gms/blescanner/compat/ab;->a:I

    iget v3, p0, Lcom/google/android/gms/blescanner/compat/ab;->b:I

    iget-wide v4, p0, Lcom/google/android/gms/blescanner/compat/ab;->d:J

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/blescanner/compat/ScanSettings;-><init>(IIIJB)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/blescanner/compat/ab;
    .locals 3

    .prologue
    .line 162
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-le p1, v0, :cond_1

    .line 163
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid scan mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_1
    iput p1, p0, Lcom/google/android/gms/blescanner/compat/ab;->c:I

    .line 166
    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/blescanner/compat/ab;
    .locals 3

    .prologue
    .line 222
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 223
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "reportDelayMillis must be > 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 225
    :cond_0
    iput-wide p1, p0, Lcom/google/android/gms/blescanner/compat/ab;->d:J

    .line 226
    return-object p0
.end method
