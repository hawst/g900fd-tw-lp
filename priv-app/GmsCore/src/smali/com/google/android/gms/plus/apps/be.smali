.class public final Lcom/google/android/gms/plus/apps/be;
.super Landroid/support/v4/a/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/internal/am;


# instance fields
.field a:Lcom/google/android/gms/common/c;

.field private final b:Lcom/google/android/gms/plus/internal/ad;

.field private c:Lcom/google/android/gms/plus/internal/ab;

.field private d:Z

.field private final e:Landroid/accounts/Account;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:I

.field private final i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    .prologue
    .line 42
    sget-object v7, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/apps/be;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V

    .line 44
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/support/v4/a/j;-><init>(Landroid/content/Context;)V

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/be;->e:Landroid/accounts/Account;

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/plus/apps/be;->f:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/google/android/gms/plus/apps/be;->g:Ljava/lang/String;

    .line 53
    iput p5, p0, Lcom/google/android/gms/plus/apps/be;->h:I

    .line 54
    iput-object p6, p0, Lcom/google/android/gms/plus/apps/be;->i:Ljava/lang/String;

    .line 55
    iput-object p7, p0, Lcom/google/android/gms/plus/apps/be;->b:Lcom/google/android/gms/plus/internal/ad;

    .line 56
    return-void
.end method

.method private a(Lcom/google/android/gms/plus/internal/ab;)V
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->f:Ljava/lang/String;

    .line 70
    :goto_0
    iget v1, p0, Lcom/google/android/gms/plus/apps/be;->h:I

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/be;->i:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/am;Ljava/lang/String;ILjava/lang/String;)V

    .line 76
    :goto_1
    return-void

    .line 68
    :cond_0
    const-string v0, "all"

    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->g:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/plus/apps/be;->h:I

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/be;->i:Ljava/lang/String;

    invoke-interface {p1, p0, v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/plus/internal/am;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public final T_()V
    .locals 1

    .prologue
    .line 142
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/be;->d:Z

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/be;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 144
    return-void
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 148
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/be;->e()V

    .line 152
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/be;->d:Z

    .line 153
    return-void
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Landroid/support/v4/a/j;->a()V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/be;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/be;->d:Z

    if-nez v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 135
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/be;->a:Lcom/google/android/gms/common/c;

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/be;->d:Z

    .line 137
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/be;->b(Ljava/lang/Object;)V

    .line 138
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/be;->a:Lcom/google/android/gms/common/c;

    .line 81
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/be;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    .line 82
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/apps/be;->b(Ljava/lang/Object;)V

    .line 83
    return-void
.end method

.method protected final e()V
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    if-nez v0, :cond_0

    .line 95
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/be;->b:Lcom/google/android/gms/plus/internal/ad;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/be;->e:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v1, v0, p0, p0, v2}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/be;->b(Ljava/lang/Object;)V

    .line 102
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/be;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->j:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    if-nez v0, :cond_3

    .line 103
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 105
    :cond_3
    return-void
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/be;->d:Z

    if-eqz v0, :cond_2

    .line 121
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/be;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 122
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/be;->d:Z

    .line 124
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 0

    .prologue
    .line 128
    invoke-super {p0}, Landroid/support/v4/a/j;->g()V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/be;->f()V

    .line 131
    return-void
.end method
