.class public Lcom/google/android/gms/photos/autobackup/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/a/b;
.implements Lcom/google/android/libraries/social/i/ai;
.implements Lcom/google/android/libraries/social/i/aj;
.implements Lcom/google/android/libraries/social/i/ak;


# instance fields
.field private final a:Ljava/lang/Class;

.field private final b:Lcom/google/android/libraries/social/p/e;

.field private c:Lcom/google/android/libraries/social/p/c;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/i/w;Ljava/lang/Class;Lcom/google/android/libraries/social/p/e;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/z;->a:Ljava/lang/Class;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/z;->b:Lcom/google/android/libraries/social/p/e;

    .line 51
    invoke-virtual {p1, p0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;

    .line 52
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/z;->c:Lcom/google/android/libraries/social/p/c;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/z;->b:Lcom/google/android/libraries/social/p/e;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/p/c;->a(Lcom/google/android/libraries/social/p/e;)V

    .line 63
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/a/a;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/z;->a:Ljava/lang/Class;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/p/b;

    .line 57
    invoke-interface {v0}, Lcom/google/android/libraries/social/p/b;->a()Lcom/google/android/libraries/social/p/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/z;->c:Lcom/google/android/libraries/social/p/c;

    .line 58
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/z;->c:Lcom/google/android/libraries/social/p/c;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/z;->b:Lcom/google/android/libraries/social/p/e;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/p/c;->b(Lcom/google/android/libraries/social/p/e;)V

    .line 68
    return-void
.end method
