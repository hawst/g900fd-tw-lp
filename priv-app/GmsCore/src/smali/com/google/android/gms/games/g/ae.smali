.class public final Lcom/google/android/gms/games/g/ae;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3481
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3482
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ae;->a:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/ae;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ae;->cachedSize:I

    .line 3483
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 3547
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3548
    iget v2, p0, Lcom/google/android/gms/games/g/ae;->a:I

    if-eq v2, v3, :cond_0

    .line 3549
    iget v2, p0, Lcom/google/android/gms/games/g/ae;->a:I

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3552
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 3555
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 3556
    iget-object v4, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 3557
    if-eqz v4, :cond_1

    .line 3558
    add-int/lit8 v3, v3, 0x1

    .line 3559
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3555
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3563
    :cond_2
    add-int/2addr v0, v2

    .line 3564
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 3566
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/g/ae;->c:I

    if-eqz v1, :cond_4

    .line 3567
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/ae;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3570
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3495
    if-ne p1, p0, :cond_1

    .line 3512
    :cond_0
    :goto_0
    return v0

    .line 3498
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ae;

    if-nez v2, :cond_2

    move v0, v1

    .line 3499
    goto :goto_0

    .line 3501
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ae;

    .line 3502
    iget v2, p0, Lcom/google/android/gms/games/g/ae;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/ae;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 3503
    goto :goto_0

    .line 3505
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3507
    goto :goto_0

    .line 3509
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/g/ae;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/ae;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3510
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3517
    iget v0, p0, Lcom/google/android/gms/games/g/ae;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 3519
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3521
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/ae;->c:I

    add-int/2addr v0, v1

    .line 3522
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3447
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/ae;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/ae;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 3528
    iget v0, p0, Lcom/google/android/gms/games/g/ae;->a:I

    if-eq v0, v1, :cond_0

    .line 3529
    iget v0, p0, Lcom/google/android/gms/games/g/ae;->a:I

    invoke-virtual {p1, v1, v0}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3531
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3532
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 3533
    iget-object v1, p0, Lcom/google/android/gms/games/g/ae;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 3534
    if-eqz v1, :cond_1

    .line 3535
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3532
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3539
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/g/ae;->c:I

    if-eqz v0, :cond_3

    .line 3540
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/ae;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3542
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3543
    return-void
.end method
