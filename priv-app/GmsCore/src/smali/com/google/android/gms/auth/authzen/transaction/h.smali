.class public final Lcom/google/android/gms/auth/authzen/transaction/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/ab/b/a/a/q;

.field final b:Landroid/os/Bundle;

.field final c:J

.field private final d:Landroid/app/Activity;

.field private final e:Lcom/google/android/gms/auth/authzen/transaction/k;

.field private f:Landroid/app/admin/DevicePolicyManager;

.field private g:Landroid/content/BroadcastReceiver;

.field private h:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/ab/b/a/a/q;Landroid/os/Bundle;JLcom/google/android/gms/auth/authzen/transaction/k;)V
    .locals 4

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    iput-object p1, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    .line 106
    iput-object p2, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->a:Lcom/google/ab/b/a/a/q;

    .line 107
    iput-object p3, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->b:Landroid/os/Bundle;

    .line 108
    iput-wide p4, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->c:J

    .line 109
    iput-object p6, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->e:Lcom/google/android/gms/auth/authzen/transaction/k;

    .line 110
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/auth/authzen/transaction/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/authzen/transaction/i;-><init>(Lcom/google/android/gms/auth/authzen/transaction/h;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->g:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 111
    new-instance v0, Lcom/google/android/gms/auth/authzen/transaction/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/authzen/transaction/j;-><init>(Lcom/google/android/gms/auth/authzen/transaction/h;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->h:Landroid/content/ServiceConnection;

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    const-class v3, Lcom/google/android/gms/auth/devicesignals/DeviceSignalsService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->h:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Activity;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/authzen/transaction/h;Z)V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->e:Lcom/google/android/gms/auth/authzen/transaction/k;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/e;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/auth/authzen/transaction/k;->a(Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/h;->a()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->g:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->g:Landroid/content/BroadcastReceiver;

    .line 131
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/h;->c()V

    .line 132
    return-void
.end method

.method final b()Z
    .locals 4

    .prologue
    .line 245
    const-string v0, "AuthZen"

    const-string v1, "Falling back to lock via DevicePolicyManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->f:Landroid/app/admin/DevicePolicyManager;

    .line 250
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->f:Landroid/app/admin/DevicePolicyManager;

    invoke-virtual {v0}, Landroid/app/admin/DevicePolicyManager;->lockNow()V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 259
    const v1, 0x10000006

    const-string v2, "AuthZenForceLock"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    .line 262
    const-wide/16 v2, 0x64

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 263
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 252
    :catch_0
    move-exception v0

    const-string v0, "AuthZen"

    const-string v1, "Unable to lock the screen, skipping"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()V
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->h:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->d:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->h:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 275
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/h;->h:Landroid/content/ServiceConnection;

    .line 277
    :cond_0
    return-void
.end method
