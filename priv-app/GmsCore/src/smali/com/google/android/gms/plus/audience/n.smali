.class public abstract Lcom/google/android/gms/plus/audience/n;
.super Lcom/google/android/gms/plus/audience/aw;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/plus/audience/ae;


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Z

.field f:Lcom/google/android/gms/plus/audience/bg;

.field private t:Lcom/google/android/gms/common/people/data/AudienceMember;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/aw;-><init>()V

    return-void
.end method

.method private b(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "audienceSelectionList"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 237
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/audience/n;->a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/plus/audience/al;

    move-result-object v1

    .line 238
    if-eq v1, v0, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/google/android/gms/j;->gq:I

    const-string v3, "audienceSelectionList"

    invoke-virtual {v0, v2, v1, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 243
    :cond_0
    return-void
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 6

    .prologue
    .line 254
    if-nez p1, :cond_0

    .line 255
    new-instance v0, Lcom/google/android/gms/plus/audience/a/d;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/a/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    .line 258
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown loader ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/plus/audience/al;
.end method

.method protected final a(ILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/n;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/n;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 389
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/audience/n;->setResult(ILandroid/content/Intent;)V

    .line 390
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->finish()V

    .line 391
    return-void
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 377
    const/4 v0, -0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/audience/n;->a(ILandroid/content/Intent;)V

    .line 378
    return-void
.end method

.method protected a(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 201
    return-void
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 351
    return-void
.end method

.method public a(Landroid/support/v4/a/j;Lcom/google/android/gms/people/model/h;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 263
    iget v0, p1, Landroid/support/v4/a/j;->m:I

    if-nez v0, :cond_0

    .line 264
    check-cast p1, Lcom/google/android/gms/plus/audience/a/d;

    .line 265
    iget-object v0, p1, Lcom/google/android/gms/common/ui/k;->c:Lcom/google/android/gms/common/api/Status;

    .line 266
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/people/model/h;->c()I

    move-result v1

    if-ne v1, v7, :cond_2

    .line 267
    invoke-virtual {p2, v6}, Lcom/google/android/gms/people/model/h;->b(I)Lcom/google/android/gms/people/model/g;

    move-result-object v0

    .line 268
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/people/model/g;->i()Z

    move-result v1

    if-nez v1, :cond_1

    .line 269
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/plus/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput v7, v1, Lcom/google/android/gms/plus/n;->b:I

    invoke-virtual {v1}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    iput-boolean v6, p0, Lcom/google/android/gms/plus/audience/n;->w:Z

    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/plus/audience/n;->startActivityForResult(Landroid/content/Intent;I)V

    iput-boolean v7, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->a(Lcom/google/android/gms/people/model/g;)V

    goto :goto_0

    .line 273
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 274
    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    if-nez v1, :cond_0

    :try_start_0
    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/Status;->a(Landroid/app/Activity;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->u:Z
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v6, v1, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 53
    check-cast p2, Lcom/google/android/gms/people/model/h;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/audience/n;->a(Landroid/support/v4/a/j;Lcom/google/android/gms/people/model/h;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2, v1}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 483
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V
    .locals 7

    .prologue
    .line 470
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    move-object v0, p0

    move-object v3, p1

    move-object v6, p2

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    .line 473
    return-void
.end method

.method protected a(Lcom/google/android/gms/people/model/g;)V
    .locals 0

    .prologue
    .line 293
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 373
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->e()Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/audience/n;->a(ILandroid/content/Intent;)V

    .line 374
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 382
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/n;->a(ILandroid/content/Intent;)V

    .line 383
    return-void
.end method

.method protected abstract d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
.end method

.method protected e()Lcom/google/android/gms/common/audience/a/d;
    .locals 6

    .prologue
    .line 403
    new-instance v0, Lcom/google/android/gms/common/audience/a/d;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/audience/a/d;-><init>(Landroid/content/Intent;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/audience/a/d;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    iget-object v2, v2, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->c(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/audience/a/d;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/audience/a/d;->b(Ljava/util/ArrayList;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->c(I)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method protected f()Z
    .locals 1

    .prologue
    .line 287
    const/4 v0, 0x1

    return v0
.end method

.method public final g()Lcom/google/android/gms/plus/audience/bg;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    return-object v0
.end method

.method protected final h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 489
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v2, v2}, Lcom/google/android/gms/common/analytics/e;->a(Ljava/util/List;ZZ)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v0

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 338
    if-nez p1, :cond_0

    .line 339
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->w:Z

    .line 341
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->c()V

    .line 346
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->c()V

    .line 400
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 355
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 356
    sget v1, Lcom/google/android/gms/j;->mN:I

    if-ne v0, v1, :cond_1

    .line 357
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->b()V

    .line 369
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    sget v1, Lcom/google/android/gms/j;->bh:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/google/android/gms/j;->aU:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/google/android/gms/j;->Z:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/google/android/gms/j;->V:I

    if-eq v0, v1, :cond_2

    sget v1, Lcom/google/android/gms/j;->X:I

    if-ne v0, v1, :cond_0

    .line 363
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 366
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->b()V

    goto :goto_0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 12
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/16 v4, 0x8

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getIntent()Landroid/content/Intent;

    move-result-object v7

    .line 90
    const-string v0, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "com.google.android.gms.common.acl.EXTRA_UPDATE_PERSON_ID"

    invoke-virtual {v7, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    .line 92
    :goto_1
    invoke-static {v7}, Lcom/google/android/gms/common/audience/a/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/n;->t:Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 94
    const-string v5, "com.google.android.gms.common.acl.EXTRA_TITLE_TEXT"

    invoke-virtual {v7, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 96
    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/n;->e:Z

    .line 97
    const/16 v5, 0xb

    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getWindow()Landroid/view/Window;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/view/Window;->hasFeature(I)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 99
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    if-eqz v0, :cond_3

    .line 100
    :cond_0
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/audience/n;->requestWindowFeature(I)Z

    .line 109
    :goto_2
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/aw;->onCreate(Landroid/os/Bundle;)V

    .line 111
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 112
    const-string v0, "AudienceSelectionActivi"

    const-string v2, "This activity is not available for restricted profile."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    invoke-virtual {p0, v3, v1}, Lcom/google/android/gms/plus/audience/n;->a(ILandroid/content/Intent;)V

    .line 191
    :goto_3
    return-void

    .line 90
    :cond_1
    invoke-static {v7}, Lcom/google/android/gms/common/audience/a/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    .line 102
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 103
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/n;->e:Z

    goto :goto_2

    .line 107
    :cond_4
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/audience/n;->requestWindowFeature(I)Z

    goto :goto_2

    .line 119
    :cond_5
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 125
    const-string v5, "com.google.android.gms.common.acl.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v7, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    .line 126
    const-string v5, "com.google.android.gms.common.acl.EXTRA_PLUS_PAGE_ID"

    invoke-virtual {v7, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    .line 127
    const-string v5, "com.google.android.gms.common.acl.EXTRA_CLIENT_APPLICATION_ID"

    invoke-virtual {v7, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    .line 128
    const-string v5, "OK_TEXT"

    invoke-virtual {v7, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/n;->v:Ljava/lang/String;

    .line 130
    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 131
    sget-object v5, Lcom/google/android/gms/common/analytics/a;->b:Ljava/lang/String;

    iput-object v5, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    .line 134
    :cond_6
    iget-object v5, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 135
    const-string v0, "AudienceSelectionActivi"

    const-string v2, "Account name must not be empty."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    invoke-virtual {p0, v3, v1}, Lcom/google/android/gms/plus/audience/n;->a(ILandroid/content/Intent;)V

    goto :goto_3

    .line 121
    :catch_0
    move-exception v0

    invoke-virtual {p0, v3, v1}, Lcom/google/android/gms/plus/audience/n;->a(ILandroid/content/Intent;)V

    goto :goto_3

    .line 141
    :cond_7
    if-nez p1, :cond_8

    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/n;->n()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 143
    sget-object v5, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/gms/plus/audience/n;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 146
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getIntent()Landroid/content/Intent;

    move-result-object v9

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-eqz v0, :cond_c

    iput v3, p0, Lcom/google/android/gms/plus/audience/aw;->o:I

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/aw;->k:Z

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/aw;->l:Z

    invoke-virtual {p0, v3, v3}, Lcom/google/android/gms/plus/audience/aw;->overridePendingTransition(II)V

    sget v0, Lcom/google/android/gms/l;->dm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->setContentView(I)V

    sget v0, Lcom/google/android/gms/j;->ba:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->a(Lcom/google/android/gms/plus/audience/av;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->a(Lcom/google/android/gms/plus/audience/au;)V

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->n:Z

    if-eqz v0, :cond_10

    move v0, v3

    :goto_4
    invoke-virtual {v5, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/k;->x:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->r:I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/g;->br:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/gms/g;->bq:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v5

    add-float/2addr v0, v5

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    sget v0, Lcom/google/android/gms/j;->Z:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->j:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const-string v0, "navigation_bar_height"

    const-string v5, "dimen"

    const-string v6, "android"

    invoke-virtual {v10, v0, v5, v6}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    const-string v0, "navigation_bar_height_landscape"

    const-string v5, "dimen"

    const-string v11, "android"

    invoke-virtual {v10, v0, v5, v11}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-static {p0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    move v5, v0

    :goto_5
    if-lez v6, :cond_16

    if-nez v5, :cond_16

    invoke-virtual {v10, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v6, v0

    :goto_6
    if-lez v11, :cond_15

    if-nez v5, :cond_15

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    move v5, v0

    :goto_7
    new-instance v10, Landroid/util/DisplayMetrics;

    invoke-direct {v10}, Landroid/util/DisplayMetrics;-><init>()V

    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    const/16 v11, 0x11

    invoke-static {v11}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v11

    if-eqz v11, :cond_11

    invoke-virtual {v0, v10}, Landroid/view/Display;->getRealMetrics(Landroid/util/DisplayMetrics;)V

    :goto_8
    iget v0, v10, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v10, v10, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v0, v10, :cond_12

    if-ne v5, v6, :cond_12

    invoke-static {p0}, Lcom/google/android/gms/common/util/d;->a(Landroid/app/Activity;)I

    move-result v0

    sub-int v0, v10, v0

    :goto_9
    iget-object v5, p0, Lcom/google/android/gms/plus/audience/aw;->j:Landroid/widget/FrameLayout;

    new-instance v6, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x1

    invoke-direct {v6, v10, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->j:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->V:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->X:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v0, Lcom/google/android/gms/j;->gq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->h:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/p;->sE:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v5, "com.google.android.gms.common.acl.EXTRA_TITLE_TEXT"

    invoke-virtual {v9, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_a

    invoke-static {v9}, Lcom/google/android/gms/common/audience/a/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_9

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v6, Lcom/google/android/gms/p;->sF:I

    new-array v10, v2, [Ljava/lang/Object;

    aput-object v5, v10, v3

    invoke-virtual {v0, v6, v10}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_9
    move-object v5, v0

    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v6, Lcom/google/android/gms/f;->ac:I

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v6, "com.google.android.gms.common.acl.EXTRA_HEADER_TEXT_COLOR"

    invoke-virtual {v9, v6, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    sget v0, Lcom/google/android/gms/j;->Y:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {v6}, Lcom/google/android/gms/plus/audience/aw;->d(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v5, Lcom/google/android/gms/f;->aa:I

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const-string v5, "com.google.android.gms.common.acl.EXTRA_HEADER_BACKGROUND_COLOR"

    invoke-virtual {v9, v5, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    sget v0, Lcom/google/android/gms/j;->U:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    if-eqz v0, :cond_b

    invoke-static {v5}, Lcom/google/android/gms/plus/audience/aw;->d(I)I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    :cond_b
    iput v3, p0, Lcom/google/android/gms/plus/audience/aw;->q:I

    iput v3, p0, Lcom/google/android/gms/plus/audience/aw;->p:I

    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/aw;->s:Z

    invoke-super {p0}, Lcom/google/android/gms/plus/audience/aw;->k()V

    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    .line 148
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-nez v0, :cond_e

    .line 149
    sget v0, Lcom/google/android/gms/l;->dp:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->setContentView(I)V

    .line 151
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->e:Z

    if-eqz v0, :cond_d

    .line 152
    sget v0, Lcom/google/android/gms/j;->oY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 154
    :cond_d
    const-wide v4, 0x3fe570a3d70a3d71L    # 0.67

    invoke-static {p0, v4, v5}, Lcom/google/android/gms/common/util/d;->a(Landroid/app/Activity;D)V

    .line 156
    sget v0, Lcom/google/android/gms/j;->mN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 157
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->v:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_e

    .line 159
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->v:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 163
    :cond_e
    if-nez p1, :cond_13

    .line 164
    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    .line 165
    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/audience/a/d;->b(Landroid/content/Intent;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "com.google.android.gms.common.acl.EXTRA_DOMAIN_RESTRICTED"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/people/data/a;->a(I)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    .line 171
    new-instance v2, Lcom/google/android/gms/plus/audience/bg;

    invoke-direct {v2, v0}, Lcom/google/android/gms/plus/audience/bg;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v2, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    .line 178
    :goto_a
    invoke-direct {p0, v7}, Lcom/google/android/gms/plus/audience/n;->b(Landroid/content/Intent;)V

    .line 180
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 181
    invoke-virtual {p0, v8}, Lcom/google/android/gms/plus/audience/n;->setTitle(Ljava/lang/CharSequence;)V

    .line 186
    :cond_f
    :goto_b
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, v3, v1, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 190
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/n;->a(Landroid/os/Bundle;)V

    goto/16 :goto_3

    :cond_10
    move v0, v4

    .line 146
    goto/16 :goto_4

    :cond_11
    invoke-virtual {v0, v10}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    goto/16 :goto_8

    :cond_12
    sub-int v0, v10, v6

    invoke-static {p0}, Lcom/google/android/gms/common/util/d;->a(Landroid/app/Activity;)I

    move-result v5

    sub-int/2addr v0, v5

    goto/16 :goto_9

    .line 173
    :cond_13
    const-string v0, "resolvingError"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    .line 174
    new-instance v2, Lcom/google/android/gms/plus/audience/bg;

    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v2, v0}, Lcom/google/android/gms/plus/audience/bg;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v2, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    goto :goto_a

    .line 182
    :cond_14
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->e:Z

    if-eqz v0, :cond_f

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->a()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->setTitle(I)V

    goto :goto_b

    :cond_15
    move v5, v3

    goto/16 :goto_7

    :cond_16
    move v6, v3

    goto/16 :goto_6

    :cond_17
    move v5, v3

    goto/16 :goto_5
.end method

.method protected onPostResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 326
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/aw;->onPostResume()V

    .line 327
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->w:Z

    if-nez v0, :cond_0

    .line 330
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 331
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/audience/n;->b(Landroid/content/Intent;)V

    .line 332
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    .line 334
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 247
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/aw;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 248
    const-string v0, "audience"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 249
    const-string v0, "resolvingError"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/n;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 250
    return-void
.end method

.method public setTitle(I)V
    .locals 1

    .prologue
    .line 222
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/n;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->setTitle(Ljava/lang/CharSequence;)V

    .line 223
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-eqz v0, :cond_0

    .line 218
    :goto_0
    return-void

    .line 213
    :cond_0
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/n;->e:Z

    if-eqz v0, :cond_1

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/n;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 216
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/aw;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
