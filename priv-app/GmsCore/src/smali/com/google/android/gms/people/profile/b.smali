.class final Lcom/google/android/gms/people/profile/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/profile/AvatarActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/profile/AvatarActivity;)V
    .locals 0

    .prologue
    .line 453
    iput-object p1, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 453
    check-cast p1, Lcom/google/android/gms/people/p;

    invoke-interface {p1}, Lcom/google/android/gms/people/p;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/p;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v4, :cond_1

    :cond_0
    move-object v3, v2

    :goto_0
    if-nez v3, :cond_2

    const-string v0, "People.Avatar"

    const-string v1, "Failed to decode remote photo"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    sget v1, Lcom/google/android/gms/p;->rH:I

    invoke-static {v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/people/profile/AvatarActivity;I)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-static {v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->c(Lcom/google/android/gms/people/profile/AvatarActivity;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v4}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-static {v4}, Lcom/google/android/gms/people/af;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v3, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    const-string v1, "remote-avatar.jpg"

    const/4 v5, 0x1

    invoke-static {v0, v1, v5}, Lcom/google/android/gms/people/profile/f;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v5

    if-nez v5, :cond_3

    const-string v0, "People.Avatar"

    const-string v1, "Failed to get temp file for remote photo"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    sget v1, Lcom/google/android/gms/p;->rH:I

    invoke-static {v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/people/profile/AvatarActivity;I)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-static {v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->c(Lcom/google/android/gms/people/profile/AvatarActivity;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v4}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    :cond_3
    :try_start_2
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    invoke-virtual {v5}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v0, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    sget-object v0, Lcom/google/android/gms/people/a/a;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v3, v2, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-static {v0, v5}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/people/profile/AvatarActivity;Landroid/net/Uri;)Landroid/net/Uri;

    iget-object v0, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/b;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-static {v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->d(Lcom/google/android/gms/people/profile/AvatarActivity;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :goto_2
    invoke-static {v4}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_3
    :try_start_5
    const-string v2, "People.Avatar"

    const-string v3, "Failed to compress remove photo to temp file"

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-static {v4}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_7
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catchall_2
    move-exception v0

    goto :goto_4

    :catch_1
    move-exception v0

    goto :goto_3
.end method
