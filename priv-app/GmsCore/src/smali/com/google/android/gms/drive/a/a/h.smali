.class public final Lcom/google/android/gms/drive/a/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static a(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)Lcom/google/android/gms/drive/a/c;
    .locals 4

    .prologue
    .line 45
    const-string v0, "operationType"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {v0}, Lcom/google/android/gms/drive/a/e;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/a/e;

    move-result-object v1

    .line 47
    if-nez v1, :cond_0

    .line 48
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Action type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not recognized."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/a/e;->b()Lcom/google/android/gms/drive/a/d;

    move-result-object v0

    invoke-interface {v0, p0, p1}, Lcom/google/android/gms/drive/a/d;->a(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)Lcom/google/android/gms/drive/a/c;

    move-result-object v0

    return-object v0
.end method
