.class final Lcom/google/android/gms/wearable/internal/bi;
.super Lcom/google/android/gms/wearable/internal/be;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/m;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/gms/wearable/internal/be;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 146
    iput-object p2, p0, Lcom/google/android/gms/wearable/internal/bi;->a:Ljava/util/List;

    .line 147
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/internal/PutDataResponse;)V
    .locals 3

    .prologue
    .line 151
    new-instance v0, Lcom/google/android/gms/wearable/internal/k;

    iget v1, p1, Lcom/google/android/gms/wearable/internal/PutDataResponse;->b:I

    invoke-static {v1}, Lcom/google/android/gms/wearable/internal/ba;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/wearable/internal/PutDataResponse;->c:Lcom/google/android/gms/wearable/internal/DataItemParcelable;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wearable/internal/k;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/wearable/j;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/internal/bi;->a(Ljava/lang/Object;)V

    .line 153
    iget v0, p1, Lcom/google/android/gms/wearable/internal/PutDataResponse;->b:I

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/wearable/internal/bi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    .line 156
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    goto :goto_0

    .line 159
    :cond_0
    return-void
.end method
