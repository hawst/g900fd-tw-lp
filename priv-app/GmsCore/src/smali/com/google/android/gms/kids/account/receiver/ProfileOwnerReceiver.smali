.class public Lcom/google/android/gms/kids/account/receiver/ProfileOwnerReceiver;
.super Landroid/app/admin/DeviceAdminReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/admin/DeviceAdminReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onEnabled(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 27
    const-string v0, "ProfileOwnerReceiver"

    const-string v1, "onEnabled"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 29
    const-string v0, "device_policy"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 31
    if-nez v0, :cond_0

    .line 32
    const-string v0, "ProfileOwnerReceiver"

    const-string v1, "No DevicePolicyManager found."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    :goto_0
    return-void

    .line 36
    :cond_0
    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lcom/google/android/gms/kids/account/receiver/RestrictionsProviderReceiver;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/kids/account/receiver/ProfileOwnerReceiver;->getWho(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Landroid/app/admin/DevicePolicyManager;->setRestrictionsProvider(Landroid/content/ComponentName;Landroid/content/ComponentName;)V

    .line 41
    const-string v0, "ProfileOwnerReceiver"

    const-string v1, "Registered restrictions provider"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_1
    invoke-static {p1}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/kids/account/b;->a()Landroid/accounts/Account;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->f()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/util/List;)Z

    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 44
    const-string v1, "ProfileOwnerReceiver"

    const-string v2, "SecurityException"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method
