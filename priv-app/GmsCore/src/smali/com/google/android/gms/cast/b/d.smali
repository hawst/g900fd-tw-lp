.class public final Lcom/google/android/gms/cast/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/net/ssl/X509KeyManager;


# static fields
.field private static final a:Lcom/google/android/gms/cast/e/h;

.field private static final b:I

.field private static g:Lcom/google/android/gms/cast/b/d;


# instance fields
.field private final c:Landroid/content/Context;

.field private d:Ljava/security/KeyStore;

.field private e:Ljava/security/KeyStore$PrivateKeyEntry;

.field private f:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastClientAuthKeyManager"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    .line 47
    sget-object v0, Lcom/google/android/gms/cast/a/b;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/b/d;->b:I

    .line 54
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/cast/b/d;->g:Lcom/google/android/gms/cast/b/d;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object v0, p0, Lcom/google/android/gms/cast/b/d;->d:Ljava/security/KeyStore;

    .line 57
    iput-object p1, p0, Lcom/google/android/gms/cast/b/d;->c:Landroid/content/Context;

    .line 59
    :try_start_0
    const-string v0, "AndroidKeyStore"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/b/d;->d:Ljava/security/KeyStore;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/cast/b/d;->d:Ljava/security/KeyStore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 70
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    .line 62
    sget-object v1, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Can\'t retrieve keystore"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :catch_1
    move-exception v0

    .line 64
    sget-object v1, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Can\'t retrieve keystore"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 65
    :catch_2
    move-exception v0

    .line 66
    sget-object v1, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Can\'t retrieve keystore"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :catch_3
    move-exception v0

    .line 68
    sget-object v1, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Can\'t retrieve keystore"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/cast/b/d;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/cast/b/d;->g:Lcom/google/android/gms/cast/b/d;

    if-nez v0, :cond_0

    .line 77
    new-instance v0, Lcom/google/android/gms/cast/b/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/b/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/cast/b/d;->g:Lcom/google/android/gms/cast/b/d;

    .line 79
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/b/d;->g:Lcom/google/android/gms/cast/b/d;

    return-object v0
.end method

.method private b()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    .line 129
    move v4, v5

    :goto_0
    const/4 v2, 0x3

    if-ge v4, v2, :cond_2

    .line 131
    :try_start_0
    sget-object v2, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Attempting to retrieve client auth cert."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 132
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/cast/b/d;->f:[B

    .line 133
    iget-object v2, p0, Lcom/google/android/gms/cast/b/d;->d:Ljava/security/KeyStore;

    const-string v3, "cast_nearby_client_auth"

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6}, Ljava/security/KeyStore;->getEntry(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;

    move-result-object v2

    check-cast v2, Ljava/security/KeyStore$PrivateKeyEntry;

    iput-object v2, p0, Lcom/google/android/gms/cast/b/d;->e:Ljava/security/KeyStore$PrivateKeyEntry;

    .line 134
    iget-object v2, p0, Lcom/google/android/gms/cast/b/d;->e:Ljava/security/KeyStore$PrivateKeyEntry;

    if-nez v2, :cond_1

    .line 135
    sget-object v2, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Attempting to create a new client auth cert."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 137
    sget-object v2, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Creating a new privatekey pair for Cast auth."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v2, Ljava/util/GregorianCalendar;

    invoke-direct {v2}, Ljava/util/GregorianCalendar;-><init>()V

    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    const/16 v6, 0xc

    sget v7, Lcom/google/android/gms/cast/b/d;->b:I

    invoke-virtual {v3, v6, v7}, Ljava/util/Calendar;->add(II)V

    new-instance v6, Landroid/security/KeyPairGeneratorSpec$Builder;

    iget-object v7, p0, Lcom/google/android/gms/cast/b/d;->c:Landroid/content/Context;

    invoke-direct {v6, v7}, Landroid/security/KeyPairGeneratorSpec$Builder;-><init>(Landroid/content/Context;)V

    const-string v7, "cast_nearby_client_auth"

    invoke-virtual {v6, v7}, Landroid/security/KeyPairGeneratorSpec$Builder;->setAlias(Ljava/lang/String;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v6

    new-instance v7, Ljavax/security/auth/x500/X500Principal;

    const-string v8, "CN=cast_nearby_client_auth"

    invoke-direct {v7, v8}, Ljavax/security/auth/x500/X500Principal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSubject(Ljavax/security/auth/x500/X500Principal;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v6

    new-instance v7, Ljava/math/BigInteger;

    const/16 v8, 0x200

    new-instance v9, Ljava/security/SecureRandom;

    invoke-direct {v9}, Ljava/security/SecureRandom;-><init>()V

    invoke-direct {v7, v8, v9}, Ljava/math/BigInteger;-><init>(ILjava/util/Random;)V

    invoke-virtual {v6, v7}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSerialNumber(Ljava/math/BigInteger;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v6

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v6, v2}, Landroid/security/KeyPairGeneratorSpec$Builder;->setStartDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v2

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/security/KeyPairGeneratorSpec$Builder;->setEndDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/security/KeyPairGeneratorSpec$Builder;->build()Landroid/security/KeyPairGeneratorSpec;

    move-result-object v2

    const-string v3, "RSA"

    const-string v6, "AndroidKeyStore"

    invoke-static {v3, v6}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    invoke-virtual {v3}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;

    .line 129
    :cond_0
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 141
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/cast/b/d;->e:Ljava/security/KeyStore$PrivateKeyEntry;

    invoke-virtual {v2}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v3

    .line 142
    move-object v0, v3

    check-cast v0, Ljava/security/cert/X509Certificate;

    move-object v2, v0

    .line 143
    if-nez v2, :cond_3

    .line 144
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->c()Z

    move-result v2

    if-nez v2, :cond_0

    .line 193
    :cond_2
    :goto_2
    return-void

    .line 151
    :cond_3
    new-instance v6, Ljava/util/Date;

    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    const-wide/32 v10, 0xea60

    add-long/2addr v8, v10

    invoke-direct {v6, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v6}, Ljava/security/cert/X509Certificate;->checkValidity(Ljava/util/Date;)V

    .line 152
    const-string v2, "SHA-256"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 153
    invoke-virtual {v3}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v3

    invoke-interface {v3}, Ljava/security/PublicKey;->getEncoded()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/cast/b/d;->f:[B

    .line 154
    sget-object v2, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "successfully created hash of public key. %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/gms/cast/b/d;->f:[B

    const/4 v9, 0x0

    invoke-static {v8, v9}, Landroid/util/Base64;->encode([BI)[B

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v2, v3, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateExpiredException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/cert/CertificateNotYetValidException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Ljava/security/NoSuchProviderException; {:try_start_0 .. :try_end_0} :catch_6
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_7

    goto :goto_2

    .line 157
    :catch_0
    move-exception v2

    .line 158
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v6, "UnrecoverableEntryException detected."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 159
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 160
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v6, "deleting key and regenerating."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 163
    :catch_1
    move-exception v2

    .line 164
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v6, "RuntimeExeception detected."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 166
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v6, "deleting key and regenerating."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 169
    :catch_2
    move-exception v2

    .line 170
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 171
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v6, "deleting key and regenerating."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 174
    :catch_3
    move-exception v2

    .line 175
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 176
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v6, "deleting key and regenerating."

    new-array v7, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v6, v7}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 179
    :catch_4
    move-exception v2

    .line 180
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v4, "No algorithm available."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 182
    :catch_5
    move-exception v2

    .line 183
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v4, "unable to use key from keystore."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 185
    :catch_6
    move-exception v2

    .line 186
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v4, "no provider."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    .line 188
    :catch_7
    move-exception v2

    .line 189
    sget-object v3, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v4, "invalid algorithm parameter."

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2
.end method

.method private c()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 197
    :try_start_0
    sget-object v2, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Deleting key %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "cast_nearby_client_auth"

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    iget-object v2, p0, Lcom/google/android/gms/cast/b/d;->d:Ljava/security/KeyStore;

    const-string v3, "cast_nearby_client_auth"

    invoke-virtual {v2, v3}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :goto_0
    return v0

    .line 200
    :catch_0
    move-exception v0

    .line 201
    sget-object v2, Lcom/google/android/gms/cast/b/d;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "unable to delete key from keystore."

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 202
    goto :goto_0
.end method


# virtual methods
.method public final a()[B
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->b()V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/cast/b/d;->f:[B

    return-object v0
.end method

.method public final chooseClientAlias([Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    const-string v0, "cast_nearby_client_auth"

    return-object v0
.end method

.method public final chooseServerAlias(Ljava/lang/String;[Ljava/security/Principal;Ljava/net/Socket;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getCertificateChain(Ljava/lang/String;)[Ljava/security/cert/X509Certificate;
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->b()V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/cast/b/d;->e:Ljava/security/KeyStore$PrivateKeyEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/d;->e:Ljava/security/KeyStore$PrivateKeyEntry;

    invoke-virtual {v0}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificateChain()[Ljava/security/cert/Certificate;

    move-result-object v0

    check-cast v0, [Ljava/security/cert/X509Certificate;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/security/cert/X509Certificate;

    goto :goto_0
.end method

.method public final getClientAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 103
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "cast_nearby_client_auth"

    aput-object v2, v0, v1

    return-object v0
.end method

.method public final getPrivateKey(Ljava/lang/String;)Ljava/security/PrivateKey;
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/cast/b/d;->b()V

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/cast/b/d;->e:Ljava/security/KeyStore$PrivateKeyEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/d;->e:Ljava/security/KeyStore$PrivateKeyEntry;

    invoke-virtual {v0}, Ljava/security/KeyStore$PrivateKeyEntry;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getServerAliases(Ljava/lang/String;[Ljava/security/Principal;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    return-object v0
.end method
