.class public final Lcom/google/android/gms/plus/sharebox/p;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private j:Ljava/util/ArrayList;

.field private k:Lcom/google/android/gms/plus/sharebox/r;

.field private l:Landroid/widget/EditText;

.field private m:Landroid/widget/TextView;

.field private n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 56
    return-void
.end method

.method public static a(Ljava/util/ArrayList;)Lcom/google/android/gms/plus/sharebox/p;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 80
    const-string v1, "circles"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 81
    new-instance v1, Lcom/google/android/gms/plus/sharebox/p;

    invoke-direct {v1}, Lcom/google/android/gms/plus/sharebox/p;-><init>()V

    .line 82
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/p;->setArguments(Landroid/os/Bundle;)V

    .line 83
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/p;Ljava/lang/CharSequence;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/p;->a(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/16 v6, 0x8

    const/4 v2, 0x0

    .line 166
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/p;->b(Z)V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 180
    :goto_0
    return-void

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->j:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    .line 170
    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/p;->b(Z)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1

    .line 172
    :cond_5
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v3

    sget-object v0, Lcom/google/android/gms/plus/c/a;->ae:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v3, v0, :cond_6

    .line 173
    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/sharebox/p;->b(Z)V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 176
    :cond_6
    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/sharebox/p;->b(Z)V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_1

    .line 185
    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 189
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    const-string v0, "ShareBox"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "ShareBox"

    const-string v1, "Unable to toggle create circle dialog button enabled state"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->eR:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 108
    sget v0, Lcom/google/android/gms/j;->cN:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->l:Landroid/widget/EditText;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->l:Landroid/widget/EditText;

    new-instance v2, Lcom/google/android/gms/plus/sharebox/q;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/sharebox/q;-><init>(Lcom/google/android/gms/plus/sharebox/p;B)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 110
    sget v0, Lcom/google/android/gms/j;->cR:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->m:Landroid/widget/TextView;

    .line 111
    sget v0, Lcom/google/android/gms/j;->cQ:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->n:Landroid/widget/TextView;

    .line 114
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v2, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/p;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    sget v4, Lcom/google/android/gms/q;->C:I

    invoke-direct {v2, v3, v4}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 116
    sget v2, Lcom/google/android/gms/p;->vw:I

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x104000a

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 125
    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onAttach(Landroid/app/Activity;)V

    .line 90
    :try_start_0
    check-cast p1, Lcom/google/android/gms/plus/sharebox/r;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/p;->k:Lcom/google/android/gms/plus/sharebox/r;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    return-void

    .line 92
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/ClassCastException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/r;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ClassCastException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCancel(Landroid/content/DialogInterface;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->k:Lcom/google/android/gms/plus/sharebox/r;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/r;->c()V

    .line 149
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/p;->dismiss()V

    .line 150
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 136
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/p;->b(Z)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->k:Lcom/google/android/gms/plus/sharebox/r;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/p;->l:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/sharebox/r;->a(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/p;->dismiss()V

    .line 143
    :goto_0
    return-void

    .line 141
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/sharebox/p;->onCancel(Landroid/content/DialogInterface;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/p;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "circles"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->j:Ljava/util/ArrayList;

    .line 101
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 130
    invoke-super {p0}, Landroid/support/v4/app/m;->onResume()V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/p;->l:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/p;->a(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method
