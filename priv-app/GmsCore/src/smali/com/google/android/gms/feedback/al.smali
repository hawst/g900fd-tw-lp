.class public final Lcom/google/android/gms/feedback/al;
.super Lcom/google/android/gms/feedback/ai;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/gms/feedback/ai;-><init>(Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 17
    return-void
.end method

.method private static a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/feedback/at;->a([Ljava/lang/String;)[Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 47
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final bridge synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Lcom/google/android/gms/feedback/FeedbackAsyncService;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/feedback/al;->a(Lcom/google/android/gms/feedback/FeedbackAsyncService;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/feedback/FeedbackAsyncService;)V
    .locals 4

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/feedback/al;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-boolean v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    if-nez v0, :cond_1

    .line 23
    iget-object v0, p0, Lcom/google/android/gms/feedback/al;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v0, v0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    new-instance v1, Lcom/google/android/gms/feedback/Screenshot;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/Screenshot;-><init>()V

    .line 25
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackAsyncService;->getCacheDir()Ljava/io/File;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/feedback/al;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-static {v0, v2, v1, v3}, Lcom/google/android/gms/feedback/Screenshot;->a(Ljava/io/File;Ljava/lang/String;Lcom/google/android/gms/feedback/Screenshot;Z)Landroid/os/AsyncTask;

    .line 27
    new-instance v0, Lcom/google/android/gms/feedback/e;

    iget-object v2, p0, Lcom/google/android/gms/feedback/al;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0, p1, v2, v1}, Lcom/google/android/gms/feedback/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    .line 32
    :goto_0
    sget-object v1, Lcom/google/android/gms/feedback/at;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/feedback/al;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 33
    sget-object v2, Lcom/google/android/gms/feedback/at;->b:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/feedback/al;->a([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 34
    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/feedback/e;->a([Ljava/lang/String;[Ljava/lang/String;)V

    .line 39
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/gms/feedback/e;->a()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/al;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/feedback/ai;->a(Lcom/google/android/gms/feedback/FeedbackAsyncService;)V

    .line 41
    return-void

    .line 29
    :cond_0
    new-instance v0, Lcom/google/android/gms/feedback/e;

    iget-object v1, p0, Lcom/google/android/gms/feedback/al;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/feedback/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    goto :goto_0

    .line 37
    :cond_1
    new-instance v0, Lcom/google/android/gms/feedback/e;

    iget-object v1, p0, Lcom/google/android/gms/feedback/al;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/feedback/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    goto :goto_1
.end method
