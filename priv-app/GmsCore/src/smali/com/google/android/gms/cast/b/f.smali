.class final Lcom/google/android/gms/cast/b/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/d/d;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/b/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/b/e;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/e/h;

    move-result-object v2

    const-string v3, "Unsuccessful cast nearby session %d"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 208
    const/4 v2, 0x7

    if-ne p1, v2, :cond_0

    .line 209
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v1, p1, v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;IZ)V

    .line 210
    invoke-static {}, Lcom/google/android/gms/cast/b/e;->q()Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 211
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/cast/b/e;->q()Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    move v0, v1

    .line 208
    goto :goto_0

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "Successful cast nearby session initiated"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)Ljava/lang/String;

    .line 199
    invoke-static {}, Lcom/google/android/gms/cast/b/e;->q()Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 200
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/cast/b/e;->q()Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->b(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/util/Pair;

    iget-object v4, p0, Lcom/google/android/gms/cast/b/f;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v4}, Lcom/google/android/gms/cast/b/e;->c(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
