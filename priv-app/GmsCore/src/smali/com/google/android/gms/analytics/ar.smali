.class final Lcom/google/android/gms/analytics/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/analytics/ao;

.field private final b:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/gms/analytics/ao;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 347
    iput-object p1, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 348
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p2}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    .line 353
    const-string v0, "&ht"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 354
    if-eqz v0, :cond_0

    .line 358
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 365
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 366
    iget-object v2, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    const-string v3, "&ht"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    :cond_1
    return-void

    .line 361
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4059000000000000L    # 100.0

    const/4 v1, 0x0

    const/4 v3, 0x1

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-static {v2}, Lcom/google/android/gms/analytics/ao;->a(Lcom/google/android/gms/analytics/ao;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/analytics/a;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/w;

    move-result-object v2

    const-string v4, "&adid"

    invoke-static {v0, v4, v2}, Lcom/google/android/gms/analytics/bz;->a(Ljava/util/Map;Ljava/lang/String;Lcom/google/android/gms/analytics/w;)V

    const-string v4, "&ate"

    invoke-static {v0, v4, v2}, Lcom/google/android/gms/analytics/bz;->a(Ljava/util/Map;Ljava/lang/String;Lcom/google/android/gms/analytics/w;)V

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    const-string v2, "&cid"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    const-string v2, "&cid"

    invoke-static {}, Lcom/google/android/gms/analytics/o;->a()Lcom/google/android/gms/analytics/o;

    move-result-object v4

    const-string v5, "&cid"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/analytics/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 388
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->a(Lcom/google/android/gms/analytics/ao;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/ax;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    const-string v0, "&sf"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v0, "&sf"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/analytics/bz;->b(Ljava/lang/String;)D

    move-result-wide v4

    cmpl-double v0, v4, v8

    if-gez v0, :cond_3

    const-string v0, "&cid"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->a(Ljava/lang/String;)I

    move-result v0

    rem-int/lit16 v0, v0, 0x2710

    int-to-double v6, v0

    mul-double/2addr v4, v8

    cmpl-double v0, v6, v4

    if-ltz v0, :cond_3

    const-string v0, "&t"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "unknown"

    :goto_0
    const-string v2, "%s hit sampled out"

    new-array v4, v3, [Ljava/lang/Object;

    aput-object v0, v4, v1

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    move v0, v3

    :goto_1
    if-eqz v0, :cond_4

    .line 406
    :cond_1
    :goto_2
    return-void

    .line 388
    :cond_2
    const-string v0, "&t"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1

    .line 392
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->b(Lcom/google/android/gms/analytics/ao;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 393
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/analytics/as;->a(Z)V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/analytics/bh;

    invoke-direct {v2}, Lcom/google/android/gms/analytics/bh;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-static {v4}, Lcom/google/android/gms/analytics/ao;->b(Lcom/google/android/gms/analytics/ao;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/analytics/bh;->d(Ljava/lang/String;)Lcom/google/android/gms/analytics/bh;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/analytics/bh;->a()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 396
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/as;->a(Z)V

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->c(Lcom/google/android/gms/analytics/ao;)Ljava/lang/String;

    .line 399
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    invoke-static {}, Lcom/google/android/gms/analytics/j;->a()Lcom/google/android/gms/analytics/j;

    move-result-object v1

    const-string v2, "&an"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/analytics/bz;->a(Ljava/util/Map;Ljava/lang/String;Lcom/google/android/gms/analytics/w;)V

    const-string v2, "&av"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/analytics/bz;->a(Ljava/util/Map;Ljava/lang/String;Lcom/google/android/gms/analytics/w;)V

    const-string v2, "&aid"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/analytics/bz;->a(Ljava/util/Map;Ljava/lang/String;Lcom/google/android/gms/analytics/w;)V

    const-string v2, "&aiid"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/analytics/bz;->a(Ljava/util/Map;Ljava/lang/String;Lcom/google/android/gms/analytics/w;)V

    const-string v1, "&v"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_6
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v5, "&"

    invoke-virtual {v2, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 401
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-static {v0}, Lcom/google/android/gms/analytics/ao;->e(Lcom/google/android/gms/analytics/ao;)Lcom/google/android/gms/analytics/bt;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    const-string v3, "&ht"

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/gms/analytics/ar;->b:Ljava/util/Map;

    const-string v5, "useSecure"

    invoke-interface {v4, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v5, "useSecure"

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/analytics/bz;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "http:"

    :goto_4
    iget-object v5, p0, Lcom/google/android/gms/analytics/ar;->a:Lcom/google/android/gms/analytics/ao;

    invoke-static {v5}, Lcom/google/android/gms/analytics/ao;->d(Lcom/google/android/gms/analytics/ao;)Ljava/util/List;

    move-result-object v5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/analytics/bt;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    goto/16 :goto_2

    :cond_8
    const-string v4, "https:"

    goto :goto_4
.end method
