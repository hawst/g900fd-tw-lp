.class final Lcom/google/android/gms/games/ui/common/a/c;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final k:Landroid/support/v7/widget/CardView;

.field private final n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    move-object v0, p1

    .line 60
    check-cast v0, Landroid/support/v7/widget/CardView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->k:Landroid/support/v7/widget/CardView;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->k:Landroid/support/v7/widget/CardView;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/CardView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 63
    sget v0, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->n:Landroid/widget/TextView;

    .line 64
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/a/a;

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/a/c;->n:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/a/a;->a(Lcom/google/android/gms/games/ui/common/a/a;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 71
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/a/c;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/a/a;

    .line 76
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/a/a;->d(Lcom/google/android/gms/games/ui/common/a/a;)Lcom/google/android/gms/games/ui/common/a/b;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/a/a;->b(Lcom/google/android/gms/games/ui/common/a/a;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/a/a;->c(Lcom/google/android/gms/games/ui/common/a/a;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/ui/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    return-void
.end method
