.class public final Lcom/google/android/gms/people/f/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/people/f/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/f/c;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/gms/people/f/b;->a:Lcom/google/android/gms/people/f/c;

    .line 51
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 313
    :try_start_0
    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 315
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/d/r;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 136
    new-instance v2, Lcom/google/android/gms/people/d/r;

    invoke-direct {v2}, Lcom/google/android/gms/people/d/r;-><init>()V

    .line 138
    invoke-static {p0}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    move v0, v1

    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_2

    aget-object v4, v3, v0

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v4}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    :goto_1
    iput v0, v2, Lcom/google/android/gms/people/d/r;->a:I

    .line 139
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    iput-boolean v1, v2, Lcom/google/android/gms/people/d/r;->b:Z

    .line 141
    return-object v2

    .line 138
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a()Lcom/google/android/gms/people/d/s;
    .locals 4

    .prologue
    .line 145
    new-instance v0, Lcom/google/android/gms/people/d/s;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/s;-><init>()V

    .line 146
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/people/d/s;->a:Ljava/lang/String;

    .line 147
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->d()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/people/d/s;->b:J

    .line 148
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/people/d/s;->c:I

    .line 149
    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/String;IIILjava/lang/String;IILjava/lang/Throwable;IJLjava/util/List;JLcom/google/android/gms/people/d/p;)V
    .locals 10

    .prologue
    .line 193
    sget-object v2, Lcom/google/android/gms/people/internal/at;->k:Ljava/security/SecureRandom;

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextFloat()F

    move-result v3

    sget-object v2, Lcom/google/android/gms/people/a/a;->av:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpl-float v2, v3, v2

    if-ltz v2, :cond_0

    .line 240
    :goto_0
    return-void

    .line 196
    :cond_0
    new-instance v4, Lcom/google/android/gms/people/d/o;

    invoke-direct {v4}, Lcom/google/android/gms/people/d/o;-><init>()V

    .line 197
    const/4 v2, 0x0

    invoke-static {p1, p2, v2}, Lcom/google/android/gms/people/f/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/d/r;

    move-result-object v2

    iput-object v2, v4, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    .line 198
    iput p3, v4, Lcom/google/android/gms/people/d/o;->b:I

    .line 199
    invoke-static/range {p6 .. p6}, Lcom/google/android/gms/people/f/b;->a(Ljava/lang/String;)I

    move-result v2

    iput v2, v4, Lcom/google/android/gms/people/d/o;->e:I

    .line 200
    iput p4, v4, Lcom/google/android/gms/people/d/o;->c:I

    .line 201
    iput p5, v4, Lcom/google/android/gms/people/d/o;->d:I

    .line 202
    move/from16 v0, p7

    iput v0, v4, Lcom/google/android/gms/people/d/o;->f:I

    .line 203
    move-wide/from16 v0, p14

    iput-wide v0, v4, Lcom/google/android/gms/people/d/o;->m:J

    .line 204
    const-wide/16 v2, 0x0

    iput-wide v2, v4, Lcom/google/android/gms/people/d/o;->n:J

    .line 207
    if-eqz p16, :cond_1

    .line 208
    move-object/from16 v0, p16

    iput-object v0, v4, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    .line 209
    iget-wide v2, v4, Lcom/google/android/gms/people/d/o;->n:J

    move-object/from16 v0, p16

    iget-wide v6, v0, Lcom/google/android/gms/people/d/p;->b:J

    add-long/2addr v2, v6

    iput-wide v2, v4, Lcom/google/android/gms/people/d/o;->n:J

    .line 212
    :cond_1
    const/4 v2, 0x1

    move/from16 v0, p7

    if-eq v0, v2, :cond_4

    .line 213
    move/from16 v0, p8

    iput v0, v4, Lcom/google/android/gms/people/d/o;->g:I

    .line 214
    if-eqz p9, :cond_3

    .line 215
    invoke-virtual/range {p9 .. p9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v4, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    .line 216
    sget-object v2, Lcom/google/android/gms/people/a/a;->ax:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static/range {p9 .. p9}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_5

    const-string v2, ""

    :cond_2
    :goto_1
    iput-object v2, v4, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    .line 218
    :cond_3
    move/from16 v0, p10

    iput v0, v4, Lcom/google/android/gms/people/d/o;->j:I

    .line 219
    move-wide/from16 v0, p11

    iput-wide v0, v4, Lcom/google/android/gms/people/d/o;->k:J

    .line 222
    :cond_4
    if-eqz p13, :cond_7

    .line 223
    invoke-interface/range {p13 .. p13}, Ljava/util/List;->size()I

    move-result v2

    new-array v5, v2, [Lcom/google/android/gms/people/d/m;

    .line 224
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    array-length v2, v5

    if-ge v3, v2, :cond_6

    .line 225
    move-object/from16 v0, p13

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/people/sync/y;

    .line 226
    new-instance v6, Lcom/google/android/gms/people/d/m;

    invoke-direct {v6}, Lcom/google/android/gms/people/d/m;-><init>()V

    iget-object v7, v2, Lcom/google/android/gms/people/sync/y;->a:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/gms/people/sync/y;->b:Ljava/lang/String;

    invoke-static {p1, v7, v8}, Lcom/google/android/gms/people/f/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/d/r;

    move-result-object v7

    iput-object v7, v6, Lcom/google/android/gms/people/d/m;->a:Lcom/google/android/gms/people/d/r;

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->c:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->b:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->d:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->c:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->e:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->d:I

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->f:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->e:Z

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->h:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->f:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->i:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->g:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->j:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->h:I

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->k:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->i:Z

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->l:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->j:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->m:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->k:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->n:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->l:I

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->o:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->m:Z

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->r:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->s:Z

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->q:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->t:Z

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->p:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->u:Z

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->s:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->n:Z

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->t:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->o:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->u:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->p:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->v:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->q:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->w:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->r:I

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->x:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->v:Z

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->y:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->w:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->z:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->x:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->B:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->y:I

    iget-boolean v7, v2, Lcom/google/android/gms/people/sync/y;->C:Z

    iput-boolean v7, v6, Lcom/google/android/gms/people/d/m;->A:Z

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->D:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->B:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->E:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->C:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->F:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->D:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->G:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->E:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->H:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->F:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->I:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->G:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->J:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->H:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->K:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->I:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->L:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->J:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->M:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->K:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->N:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->L:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->O:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->M:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->P:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->N:I

    iget v7, v2, Lcom/google/android/gms/people/sync/y;->Q:I

    iput v7, v6, Lcom/google/android/gms/people/d/m;->O:I

    iget-wide v8, v2, Lcom/google/android/gms/people/sync/y;->R:J

    iput-wide v8, v6, Lcom/google/android/gms/people/d/m;->P:J

    aput-object v6, v5, v3

    .line 228
    iget-wide v6, v4, Lcom/google/android/gms/people/d/o;->n:J

    iget-wide v8, v2, Lcom/google/android/gms/people/sync/y;->S:J

    add-long/2addr v6, v8

    iput-wide v6, v4, Lcom/google/android/gms/people/d/o;->n:J

    .line 224
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_2

    .line 216
    :cond_5
    const-string v5, "^.*?\\s+"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "foreign key constraint failed.*?\\s+at\\s+"

    const-string v6, "FK "

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\\s+"

    const-string v6, " "

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, " at "

    const-string v6, " "

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "People"

    const-string v6, "P"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "SQLite"

    const-string v6, "S"

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\\(Native Method\\)"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "SourceFile\\:"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v5, "\\b(?:android|com|org)\\.(?:[a-z0-9_]+\\.)*"

    const-string v6, ""

    invoke-virtual {v2, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-le v5, v3, :cond_2

    const/4 v5, 0x0

    invoke-virtual {v2, v5, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 231
    :cond_6
    iput-object v5, v4, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    .line 234
    :cond_7
    new-instance v2, Lcom/google/android/gms/people/d/l;

    invoke-direct {v2}, Lcom/google/android/gms/people/d/l;-><init>()V

    .line 235
    invoke-static {}, Lcom/google/android/gms/people/f/b;->a()Lcom/google/android/gms/people/d/s;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    .line 236
    iput-object v4, v2, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    .line 237
    invoke-static {p1}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v3

    iput-boolean v3, v2, Lcom/google/android/gms/people/d/l;->d:Z

    .line 239
    const-string v3, "sync_result"

    invoke-virtual {p0, v3, p2, v2}, Lcom/google/android/gms/people/f/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d/l;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d/l;)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/people/f/b;->a:Lcom/google/android/gms/people/f/c;

    const/16 v0, 0x10

    invoke-static {p1, p2, v0, p3}, Lcom/google/android/gms/people/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILcom/google/protobuf/nano/j;)V

    .line 119
    return-void
.end method
