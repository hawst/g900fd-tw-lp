.class public Lcom/google/android/gms/plus/audience/e;
.super Lcom/google/android/gms/plus/audience/o;
.source "SourceFile"


# instance fields
.field public e:Lcom/google/android/gms/plus/audience/f;

.field public f:Lcom/google/android/gms/plus/audience/aa;

.field private r:Lcom/google/android/gms/common/ui/widget/c;

.field private s:Z

.field private final t:Lcom/google/android/gms/plus/audience/y;

.field private final u:Lcom/google/android/gms/common/ui/widget/f;

.field private v:Lcom/google/android/gms/common/ui/widget/c;

.field private final w:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 43
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 28
    new-instance v0, Lcom/google/android/gms/common/ui/widget/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/ui/widget/f;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->r:Lcom/google/android/gms/common/ui/widget/c;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/e;->s:Z

    .line 31
    new-instance v0, Lcom/google/android/gms/plus/audience/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/audience/y;-><init>(Lcom/google/android/gms/plus/audience/o;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->t:Lcom/google/android/gms/plus/audience/y;

    .line 32
    new-instance v0, Lcom/google/android/gms/common/ui/widget/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/ui/widget/f;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->u:Lcom/google/android/gms/common/ui/widget/f;

    .line 46
    iput-boolean p5, p0, Lcom/google/android/gms/plus/audience/e;->w:Z

    .line 47
    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/e;->e:Lcom/google/android/gms/plus/audience/f;

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/e;->e:Lcom/google/android/gms/plus/audience/f;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/f;->X_()V

    .line 84
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/audience/o;->a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/ui/widget/c;
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lcom/google/android/gms/plus/audience/z;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/audience/z;-><init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/people/model/d;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/people/model/d;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->f()V

    .line 62
    return-object v0
.end method

.method public a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
    .locals 2

    .prologue
    .line 70
    invoke-super/range {p0 .. p12}, Lcom/google/android/gms/plus/audience/o;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->f()V

    .line 75
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b(Z)V

    .line 76
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/plus/audience/f;)Lcom/google/android/gms/plus/audience/e;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/e;->e:Lcom/google/android/gms/plus/audience/f;

    .line 52
    return-object p0
.end method

.method public final a(Landroid/database/Cursor;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 94
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 95
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 97
    if-eqz p1, :cond_2

    .line 98
    const/4 v2, -0x1

    invoke-interface {p1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 99
    :cond_0
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 100
    const/4 v2, 0x3

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 101
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 102
    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 103
    const/4 v3, 0x2

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 105
    const/4 v4, 0x0

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 108
    invoke-static {v2, v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v5

    .line 110
    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 112
    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v6, "secondaryText"

    invoke-virtual {v3, v6, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_1
    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "contactsAvatarUri"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    invoke-virtual {v5}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "contactType"

    invoke-virtual {v2, v3, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 121
    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 126
    new-instance v0, Lcom/google/android/gms/plus/audience/s;

    sget v2, Lcom/google/android/gms/p;->sU:I

    new-instance v3, Lcom/google/android/gms/plus/audience/t;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/plus/audience/t;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;)V

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;ILcom/google/android/gms/common/ui/widget/c;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->v:Lcom/google/android/gms/common/ui/widget/c;

    .line 133
    :goto_1
    return-void

    .line 131
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->v:Lcom/google/android/gms/common/ui/widget/c;

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/data/d;Z)V
    .locals 3

    .prologue
    .line 88
    iput-boolean p2, p0, Lcom/google/android/gms/plus/audience/e;->s:Z

    .line 89
    new-instance v0, Lcom/google/android/gms/plus/audience/z;

    const/4 v1, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    invoke-direct {v0, p0, p1, v1, v2}, Lcom/google/android/gms/plus/audience/z;-><init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;II)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->r:Lcom/google/android/gms/common/ui/widget/c;

    .line 90
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/e;->m()V

    .line 91
    return-void
.end method

.method public a(Lcom/google/android/gms/people/model/j;)V
    .locals 3

    .prologue
    .line 139
    new-instance v0, Lcom/google/android/gms/plus/audience/aa;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/people/model/j;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/audience/aa;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->f:Lcom/google/android/gms/plus/audience/aa;

    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/e;->m()V

    .line 141
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 148
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/e;->s:Z

    .line 149
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/e;->f:Lcom/google/android/gms/plus/audience/aa;

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/e;->m()V

    .line 154
    return-void
.end method

.method protected final d()Lcom/google/android/gms/common/ui/widget/c;
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 163
    new-instance v1, Lcom/google/android/gms/common/ui/widget/b;

    const/4 v0, 0x4

    new-array v2, v0, [Lcom/google/android/gms/common/ui/widget/c;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/e;->v:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v0, v2, v7

    new-instance v0, Lcom/google/android/gms/plus/audience/s;

    sget v3, Lcom/google/android/gms/p;->sQ:I

    new-instance v4, Lcom/google/android/gms/common/ui/widget/b;

    new-array v5, v9, [Lcom/google/android/gms/common/ui/widget/c;

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/o;->o:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/o;->p:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v6, v5, v8

    invoke-direct {v4, p0, v5}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-direct {v0, p0, v3, v4}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;ILcom/google/android/gms/common/ui/widget/c;)V

    aput-object v0, v2, v8

    new-instance v0, Lcom/google/android/gms/plus/audience/s;

    sget v3, Lcom/google/android/gms/p;->sV:I

    new-instance v4, Lcom/google/android/gms/common/ui/widget/b;

    new-array v5, v10, [Lcom/google/android/gms/common/ui/widget/c;

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/e;->f:Lcom/google/android/gms/plus/audience/aa;

    aput-object v6, v5, v7

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/o;->q:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v6, v5, v8

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/e;->r:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v6, v5, v9

    invoke-direct {v4, p0, v5}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-direct {v0, p0, v3, v4}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;ILcom/google/android/gms/common/ui/widget/c;)V

    aput-object v0, v2, v9

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/e;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/e;->t:Lcom/google/android/gms/plus/audience/y;

    :goto_0
    aput-object v0, v2, v10

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/e;->u:Lcom/google/android/gms/common/ui/widget/f;

    goto :goto_0
.end method

.method protected final e()Z
    .locals 1

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/e;->s:Z

    return v0
.end method
