.class final Lcom/google/android/gms/update/s;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/SystemUpdateService;

.field private b:I

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/update/SystemUpdateService;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 668
    iput-object p1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 670
    iput-boolean v0, p0, Lcom/google/android/gms/update/s;->c:Z

    .line 671
    iput-boolean v0, p0, Lcom/google/android/gms/update/s;->d:Z

    .line 1810
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/update/SystemUpdateService;B)V
    .locals 0

    .prologue
    .line 668
    invoke-direct {p0, p1}, Lcom/google/android/gms/update/s;-><init>(Lcom/google/android/gms/update/SystemUpdateService;)V

    return-void
.end method

.method private static a(Lcom/google/android/gms/update/u;)I
    .locals 2

    .prologue
    .line 1858
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 1863
    :goto_0
    return v0

    .line 1859
    :cond_0
    iget v0, p0, Lcom/google/android/gms/update/u;->a:I

    iget v1, p0, Lcom/google/android/gms/update/u;->b:I

    if-ne v0, v1, :cond_1

    const/16 v0, 0x5a0

    goto :goto_0

    .line 1860
    :cond_1
    iget v0, p0, Lcom/google/android/gms/update/u;->b:I

    iget v1, p0, Lcom/google/android/gms/update/u;->a:I

    if-le v0, v1, :cond_2

    .line 1861
    iget v0, p0, Lcom/google/android/gms/update/u;->b:I

    iget v1, p0, Lcom/google/android/gms/update/u;->a:I

    sub-int/2addr v0, v1

    goto :goto_0

    .line 1863
    :cond_2
    iget v0, p0, Lcom/google/android/gms/update/u;->a:I

    rsub-int v0, v0, 0x5a0

    iget v1, p0, Lcom/google/android/gms/update/u;->b:I

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1605
    const/4 v1, 0x0

    .line 1609
    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1610
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v3

    .line 1612
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    .line 1613
    :goto_0
    if-ge v2, v4, :cond_1

    .line 1614
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 1615
    iget-object v5, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v5, v5, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v5, v5, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_0

    .line 1619
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    iget-object v2, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v2, v2, Landroid/content/pm/ActivityInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 1625
    :goto_1
    return-object v0

    .line 1613
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method private a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 715
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a;->c()V

    .line 716
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/android/a/a;->a(J)V

    .line 717
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 718
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 1490
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "status"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 1491
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1492
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    .line 1494
    :cond_0
    return-void
.end method

.method private a(JZ)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 1198
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1199
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "notify_snooze"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1200
    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    cmp-long v4, v0, p1

    if-gez v4, :cond_0

    move-wide p1, v0

    .line 1205
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1206
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->p(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 1207
    if-eqz p3, :cond_1

    const/4 v1, 0x0

    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v4}, Lcom/google/android/gms/update/SystemUpdateService;->p(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, p1, p2, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 1208
    const-string v0, "SystemUpdateService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "retry (wakeup: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ") in "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sub-long v2, p1, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1209
    return-void

    .line 1207
    :cond_1
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private a(Z)V
    .locals 14

    .prologue
    const/16 v13, 0x15

    const/16 v12, 0x10

    const-wide/16 v10, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1673
    sget-boolean v0, Lcom/google/android/gms/update/SystemUpdateService;->b:Z

    if-eqz v0, :cond_0

    .line 1675
    const-string v0, "SystemUpdateService"

    const-string v1, "skipping notification"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1778
    :goto_0
    return-void

    .line 1678
    :cond_0
    const-string v0, "android.settings.SYSTEM_UPDATE_SETTINGS"

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    .line 1679
    if-nez v4, :cond_1

    .line 1680
    const-string v0, "SystemUpdateService"

    const-string v1, "no activity screen to notify"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1684
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    .line 1685
    if-eqz p1, :cond_9

    sget v0, Lcom/google/android/gms/p;->yC:I

    :goto_1
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1690
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "update_title"

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1691
    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 1693
    :cond_2
    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 1694
    const/16 v7, 0x13

    invoke-static {v7}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v7

    if-eqz v7, :cond_a

    iget v1, v1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v1, v1, 0xf

    const/4 v7, 0x6

    if-ne v1, v7, :cond_a

    move v1, v2

    .line 1697
    :goto_2
    if-eqz v1, :cond_3

    .line 1698
    if-eqz p1, :cond_b

    sget v0, Lcom/google/android/gms/p;->yB:I

    :goto_3
    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1704
    :cond_3
    const-string v7, "SystemUpdateService"

    const-string v8, "showing system update notification"

    invoke-static {v7, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1706
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v7

    .line 1708
    iget-object v8, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v7, v8, v4}, Lcom/google/android/gms/a/a;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v4

    .line 1711
    new-instance v8, Landroid/app/Notification$Builder;

    iget-object v9, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v8, v9}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v6}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v8

    sget v9, Lcom/google/android/gms/h;->ce:I

    invoke-virtual {v8, v9}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v2}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v8

    invoke-virtual {v8, v6}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-virtual {v6, v4}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v6

    invoke-virtual {v6, v10, v11}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v6

    .line 1721
    invoke-static {v13}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 1723
    const v8, 0x673ab7

    invoke-virtual {v6, v8}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    .line 1726
    :cond_4
    if-eqz v0, :cond_5

    invoke-virtual {v6, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1729
    :cond_5
    invoke-static {v12}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    if-nez v1, :cond_7

    .line 1730
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-class v8, Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v0, v1, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1731
    const-string v1, "notify_snooze"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1732
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1, v3, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 1734
    sget v1, Lcom/google/android/gms/h;->bb:I

    sget v8, Lcom/google/android/gms/p;->yw:I

    invoke-virtual {v5, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v1, v8, v0}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 1738
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "notify_repeat"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1739
    invoke-virtual {v6, v2}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    move-result-object v0

    sget-object v1, Landroid/provider/Settings$System;->DEFAULT_NOTIFICATION_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setSound(Landroid/net/Uri;)Landroid/app/Notification$Builder;

    .line 1741
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "notify_repeat"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1751
    :cond_6
    :goto_4
    if-eqz p1, :cond_e

    .line 1752
    sget v0, Lcom/google/android/gms/h;->ba:I

    sget v1, Lcom/google/android/gms/p;->yu:I

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1, v4}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    .line 1762
    :cond_7
    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 1771
    invoke-static {v12}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {v13}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1773
    sget v1, Lcom/google/android/gms/h;->ce:I

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1776
    :cond_8
    sget v1, Lcom/google/android/gms/h;->ce:I

    invoke-virtual {v6}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v7, v0, v1, v2}, Lcom/google/android/gms/a/a;->a(Landroid/app/NotificationManager;ILandroid/app/Notification;)V

    goto/16 :goto_0

    .line 1685
    :cond_9
    sget v0, Lcom/google/android/gms/p;->yF:I

    goto/16 :goto_1

    :cond_a
    move v1, v3

    .line 1694
    goto/16 :goto_2

    .line 1698
    :cond_b
    sget v0, Lcom/google/android/gms/p;->yE:I

    goto/16 :goto_3

    .line 1743
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "notify_snooze"

    invoke-interface {v0, v1, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 1744
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-gez v2, :cond_d

    .line 1745
    const/4 v0, -0x2

    invoke-virtual {v6, v0}, Landroid/app/Notification$Builder;->setPriority(I)Landroid/app/Notification$Builder;

    goto :goto_4

    .line 1746
    :cond_d
    cmp-long v0, v0, v10

    if-lez v0, :cond_6

    .line 1747
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "notify_snooze"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_4

    .line 1756
    :cond_e
    sget v0, Lcom/google/android/gms/h;->ba:I

    sget v1, Lcom/google/android/gms/p;->yv:I

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v0, v1, v4}, Landroid/app/Notification$Builder;->addAction(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    goto :goto_5
.end method

.method private a(J)Z
    .locals 1

    .prologue
    .line 1905
    const-string v0, "update_maintenance_window"

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/google/android/gms/update/s;->a(JLcom/google/android/gms/update/u;)Z

    move-result v0

    return v0
.end method

.method private static a(JLcom/google/android/gms/update/u;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1881
    if-nez p2, :cond_1

    .line 1894
    :cond_0
    :goto_0
    return v0

    .line 1882
    :cond_1
    iget v2, p2, Lcom/google/android/gms/update/u;->a:I

    iget v3, p2, Lcom/google/android/gms/update/u;->b:I

    if-ne v2, v3, :cond_2

    move v0, v1

    .line 1887
    goto :goto_0

    .line 1890
    :cond_2
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    .line 1891
    invoke-virtual {v2, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1892
    const/16 v3, 0xb

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    mul-int/lit8 v3, v3, 0x3c

    const/16 v4, 0xc

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/2addr v2, v3

    .line 1894
    iget v3, p2, Lcom/google/android/gms/update/u;->a:I

    if-gt v3, v2, :cond_3

    iget v3, p2, Lcom/google/android/gms/update/u;->b:I

    if-le v2, v3, :cond_4

    :cond_3
    iget v3, p2, Lcom/google/android/gms/update/u;->a:I

    iget v4, p2, Lcom/google/android/gms/update/u;->b:I

    if-le v3, v4, :cond_0

    iget v3, p2, Lcom/google/android/gms/update/u;->a:I

    if-le v3, v2, :cond_4

    iget v3, p2, Lcom/google/android/gms/update/u;->b:I

    if-gt v2, v3, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)Z
    .locals 22

    .prologue
    .line 726
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/a/a;->b()I

    move-result v2

    if-lez v2, :cond_0

    sget-object v2, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v3, "user"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    .line 909
    :goto_0
    return v2

    .line 728
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/a/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 729
    const/4 v2, 0x0

    goto :goto_0

    .line 732
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 733
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "provisioned"

    const-wide/16 v6, 0x0

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 734
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-eqz v6, :cond_2

    cmp-long v6, v2, v4

    if-lez v6, :cond_3

    .line 738
    :cond_2
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v3}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/a/a;->a(Landroid/content/ContentResolver;)I

    move-result v2

    .line 739
    if-nez v2, :cond_5

    const-wide/16 v2, 0x0

    .line 740
    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v6}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    const-string v7, "provisioned"

    invoke-interface {v6, v7, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    move-wide v6, v2

    .line 742
    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-lez v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/gms/update/s;->c:Z

    .line 744
    if-eqz p1, :cond_4

    const-string v2, "boot"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 745
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "pending_filename"

    const/4 v8, 0x0

    invoke-interface {v2, v3, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v3}, Lcom/google/android/gms/update/SystemUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/a/a;->b(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_4

    :try_start_0
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v8, Ljava/io/FileReader;

    const-string v9, "/cache/recovery/last_install"

    invoke-direct {v8, v9}, Ljava/io/FileReader;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v8}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v8

    const-string v9, "1"

    invoke-virtual {v3}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v8, :cond_7

    const-string v2, "SystemUpdateService"

    const-string v3, "can\'t determine last-installed OTA package name"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 748
    :cond_4
    :goto_3
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/gms/update/s;->b:I

    .line 749
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "update_url"

    invoke-static {v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 750
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 751
    const-string v2, "SystemUpdateService"

    const-string v3, "cancelUpdate (empty URL)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/gms/update/SystemUpdateService;->d(Lcom/google/android/gms/update/SystemUpdateService;Z)V

    .line 753
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->f()V

    .line 754
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->g()V

    .line 755
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_5
    move-wide v2, v4

    .line 739
    goto/16 :goto_1

    .line 742
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 745
    :catch_0
    move-exception v2

    const-string v3, "SystemUpdateService"

    const-string v8, "failed to read last_install"

    invoke-static {v3, v8, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :cond_7
    const-string v3, "SystemUpdateService"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "first boot since "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " install of "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v3, v10}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    const-string v2, "@"

    invoke-virtual {v8, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    new-instance v2, Ljava/io/File;

    const/4 v3, 0x1

    invoke-virtual {v8, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :goto_4
    if-eqz v9, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "success_message"

    const/4 v8, 0x0

    invoke-interface {v2, v3, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_5
    if-eqz v2, :cond_8

    const-string v3, "android.settings.SYSTEM_UPDATE_COMPLETE"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/update/s;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/high16 v9, 0x10040000

    invoke-virtual {v3, v9}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v3

    const-string v9, "message"

    invoke-virtual {v3, v9, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/android/gms/update/SystemUpdateService;->startActivity(Landroid/content/Intent;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "pending_filename"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_3

    :cond_9
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v8}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    goto :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "failure_message"

    const/4 v8, 0x0

    invoke-interface {v2, v3, v8}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    .line 758
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/gms/update/SystemUpdateService;->d(Lcom/google/android/gms/update/SystemUpdateService;Z)V

    .line 760
    if-eqz p1, :cond_c

    const-string v2, "download_now"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 761
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->a()V

    .line 764
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "update_required_setup"

    invoke-static {v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 765
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 766
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/gms/update/s;->d:Z

    .line 767
    const-string v2, "SystemUpdateService"

    const-string v3, "update required during setup wizard"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "status"

    const/4 v9, -0x1

    invoke-interface {v2, v3, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "url"

    const/4 v9, 0x0

    invoke-interface {v2, v3, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "install_time"

    const-wide/16 v10, 0x0

    invoke-interface {v2, v3, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v9}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "update_retry_delay_sec"

    const v11, 0x3f480

    invoke-static {v9, v10, v11}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    int-to-long v10, v9

    const-wide/16 v12, 0x3e8

    mul-long/2addr v10, v12

    add-long/2addr v2, v10

    cmp-long v2, v4, v2

    if-gtz v2, :cond_e

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "install_time"

    const-wide/16 v10, 0x0

    invoke-interface {v2, v3, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/32 v10, 0x36ee80

    sub-long/2addr v2, v10

    cmp-long v2, v4, v2

    if-gez v2, :cond_f

    .line 783
    :cond_e
    const-string v2, "SystemUpdateService"

    const-string v3, "cancelUpdate (willing to retry now)"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->f()V

    .line 787
    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->c(Landroid/content/Context;)I

    move-result v3

    .line 788
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v9, "url"

    const/4 v10, 0x0

    invoke-interface {v2, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    .line 790
    const-string v2, "SystemUpdateService"

    const-string v9, "cancelUpdate (update URL has changed)"

    invoke-static {v2, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->f()V

    .line 792
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v9

    .line 793
    const-string v2, "url"

    invoke-interface {v9, v2, v8}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    const-string v11, "status"

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->e(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v2

    if-eqz v2, :cond_12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->f(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v2

    if-nez v2, :cond_12

    const/16 v2, 0xa

    :goto_6
    invoke-interface {v10, v11, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v10, "url_change"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    invoke-interface {v2, v10, v12, v13}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v10, "required_setup"

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/gms/update/s;->d:Z

    invoke-interface {v2, v10, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 798
    packed-switch v3, :pswitch_data_0

    .line 811
    :pswitch_0
    const-string v2, "download_approved"

    invoke-interface {v9, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "install_approved"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 813
    const/4 v2, 0x2

    .line 816
    :goto_7
    invoke-interface {v9}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 817
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->a()V

    .line 818
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    move v3, v2

    .line 821
    :cond_10
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v9, "update_provisioning_delay_sec"

    const/4 v10, 0x0

    invoke-static {v2, v9, v10}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 823
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/gms/update/s;->d:Z

    if-nez v9, :cond_14

    if-ltz v2, :cond_14

    const-wide/16 v10, 0x0

    cmp-long v9, v6, v10

    if-eqz v9, :cond_11

    mul-int/lit16 v9, v2, 0x3e8

    int-to-long v10, v9

    add-long/2addr v10, v6

    cmp-long v9, v4, v10

    if-gez v9, :cond_14

    .line 826
    :cond_11
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-nez v3, :cond_13

    .line 830
    const/16 v3, 0x384

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v2, v4

    .line 839
    :goto_8
    const-string v4, "SystemUpdateService"

    const-string v5, "cancelUpdate (not provisioned)"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 840
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->f()V

    .line 841
    const/4 v4, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/update/s;->a(JZ)V

    .line 842
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->g()V

    .line 843
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 793
    :cond_12
    const/4 v2, 0x1

    goto :goto_6

    .line 801
    :pswitch_1
    const-string v2, "download_approved"

    const/4 v10, 0x1

    invoke-interface {v9, v2, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v10, "install_approved"

    invoke-interface {v2, v10}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v2, v3

    .line 803
    goto :goto_7

    .line 806
    :pswitch_2
    const-string v2, "download_approved"

    const/4 v10, 0x1

    invoke-interface {v9, v2, v10}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v10, "install_approved"

    const/4 v11, 0x1

    invoke-interface {v2, v10, v11}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move v2, v3

    .line 808
    goto :goto_7

    .line 837
    :cond_13
    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v2, v6

    goto :goto_8

    .line 846
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v6, "status"

    const/4 v7, -0x1

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v6, 0x6

    if-ne v2, v6, :cond_17

    const/4 v2, 0x4

    if-eq v3, v2, :cond_15

    const/4 v2, 0x3

    if-eq v3, v2, :cond_15

    const/4 v2, 0x6

    if-ne v3, v2, :cond_16

    :cond_15
    const/4 v2, 0x1

    :goto_9
    if-eqz v2, :cond_17

    .line 848
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v6}, Lcom/google/android/gms/update/SystemUpdateService;->m(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/b;

    move-result-object v6

    invoke-virtual {v2, v6}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v6

    .line 849
    cmp-long v2, v4, v6

    if-gez v2, :cond_17

    .line 850
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->g()V

    .line 851
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 846
    :cond_16
    const/4 v2, 0x0

    goto :goto_9

    .line 855
    :cond_17
    const/4 v6, 0x0

    .line 856
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v7, "status"

    const/4 v9, -0x1

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v7, 0x5

    if-eq v2, v7, :cond_35

    .line 858
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v7, "status"

    const/4 v9, -0x1

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v7, 0x4

    if-eq v2, v7, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v7, "status"

    const/4 v9, -0x1

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v7, 0x3

    if-eq v2, v7, :cond_18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v7, "status"

    const/4 v9, -0x1

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const/4 v7, 0x2

    if-ne v2, v7, :cond_1a

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    if-eqz v2, :cond_1a

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/update/c;->a()I

    move-result v2

    if-nez v2, :cond_1a

    .line 865
    :cond_18
    const-string v2, "SystemUpdateService"

    const-string v4, "Already downloaded, skipping offpeak checks."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3}, Lcom/google/android/gms/update/s;->a(Ljava/lang/String;I)Z

    move-result v2

    .line 907
    :goto_a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->g()V

    .line 908
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->n(Lcom/google/android/gms/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 909
    if-nez v2, :cond_19

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->o(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/v;

    move-result-object v2

    if-eqz v2, :cond_36

    :cond_19
    const/4 v2, 0x1

    :goto_b
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 910
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 867
    :cond_1a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v7, "download_approved"

    const/4 v9, 0x0

    invoke-interface {v2, v7, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_31

    .line 868
    if-eqz p1, :cond_1c

    const-string v2, "download_now"

    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1c

    const/4 v2, 0x1

    move v5, v2

    .line 870
    :goto_c
    const/4 v2, 0x1

    .line 871
    const/4 v4, 0x3

    if-ne v3, v4, :cond_1d

    const/4 v4, 0x1

    :goto_d
    if-eqz v4, :cond_1b

    .line 872
    if-eqz v5, :cond_1e

    .line 873
    const-string v4, "SystemUpdateService"

    const-string v5, "OffPeak download checking is skipped as user approved."

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 880
    :cond_1b
    :goto_e
    if-nez v2, :cond_30

    .line 881
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->g()V

    .line 882
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 868
    :cond_1c
    const/4 v2, 0x0

    move v5, v2

    goto :goto_c

    .line 871
    :cond_1d
    const/4 v4, 0x0

    goto :goto_d

    .line 875
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    const-string v2, "update_offpeak_download_window"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v2

    if-eqz v2, :cond_20

    const/4 v2, 0x1

    :goto_f
    if-eqz v2, :cond_2f

    const-string v2, "SystemUpdateService"

    const-string v4, "OffPeak download feature is ON."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    if-eqz v2, :cond_1f

    const-string v2, "SystemUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v7, "OffPeak download sAttempt status: "

    invoke-direct {v4, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/update/c;->a()I

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v4, "update_offpeak_download_operator_restricted"

    const/4 v7, 0x0

    invoke-static {v2, v4, v7}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v4, "connectivity"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    if-eqz v2, :cond_21

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v2

    if-eqz v2, :cond_21

    invoke-virtual {v2}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v2

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    if-ne v2, v4, :cond_21

    const/4 v2, 0x1

    :goto_10
    if-eqz v2, :cond_22

    if-eqz v7, :cond_22

    const-string v2, "SystemUpdateService"

    const-string v4, "OffPeak download continues as WiFi is ON though operator is restricted."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    goto/16 :goto_e

    :cond_20
    const/4 v2, 0x0

    goto :goto_f

    :cond_21
    const/4 v2, 0x0

    goto :goto_10

    :cond_22
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    const-string v2, "offpeak_download_initial_tried_time"

    const-wide/16 v12, 0x0

    invoke-interface {v5, v2, v12, v13}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    const-string v2, "update_offpeak_download_max_trying_days"

    const v4, 0x7fffffff

    invoke-static {v6, v2, v4}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v9

    const/4 v2, 0x0

    const-wide/16 v14, 0x0

    cmp-long v4, v12, v14

    if-lez v4, :cond_23

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2, v12, v13}, Ljava/util/Date;-><init>(J)V

    :cond_23
    new-instance v14, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy-MM-dd HH:mm:ss.SSSZ"

    invoke-direct {v14, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const-string v15, "SystemUpdateService"

    new-instance v16, Ljava/lang/StringBuilder;

    const-string v4, "OffPeak download has initially tried at "

    move-object/from16 v0, v16

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_24

    invoke-virtual {v14, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    :goto_11
    move-object/from16 v0, v16

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v16, " and max trying days is: "

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v15, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v2, :cond_25

    const v2, 0x7fffffff

    if-eq v9, v2, :cond_25

    sub-long v16, v10, v12

    mul-int/lit8 v2, v9, 0x18

    mul-int/lit16 v2, v2, 0xe10

    int-to-long v0, v2

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x3e8

    mul-long v18, v18, v20

    cmp-long v2, v16, v18

    if-lez v2, :cond_25

    const-string v2, "SystemUpdateService"

    const-string v4, "OffPeak download trying is over limit fall back to a normal full-day OTA."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    goto/16 :goto_e

    :cond_24
    const-string v4, " 0"

    goto :goto_11

    :cond_25
    const-string v2, "update_offpeak_download_window"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v2

    invoke-static {v10, v11, v2}, Lcom/google/android/gms/update/s;->a(JLcom/google/android/gms/update/u;)Z

    move-result v2

    if-eqz v2, :cond_29

    const-string v2, "SystemUpdateService"

    const-string v4, "OffPeak download is executed now."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v16, 0x0

    cmp-long v2, v12, v16

    if-nez v2, :cond_26

    const-string v2, "SystemUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "OffPeak download set initial trying time at: "

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/util/Date;

    invoke-direct {v6, v10, v11}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v14, v6}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "offpeak_download_initial_tried_time"

    invoke-interface {v2, v4, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_26
    if-eqz v7, :cond_27

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->b()Z

    move-result v2

    if-eqz v2, :cond_28

    :cond_27
    const/4 v2, 0x1

    goto/16 :goto_e

    :cond_28
    const/4 v2, 0x0

    goto/16 :goto_e

    :cond_29
    const-string v2, "SystemUpdateService"

    const-string v4, "OffPeak download is to be scheduled."

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "update_offpeak_download_window"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v2

    invoke-static {v10, v11, v2}, Lcom/google/android/gms/update/s;->b(JLcom/google/android/gms/update/u;)J

    move-result-wide v4

    const-wide v12, 0x7fffffffffffffffL

    cmp-long v2, v4, v12

    if-eqz v2, :cond_2c

    const-string v2, "update_offpeak_download_window"

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/update/s;->a(Lcom/google/android/gms/update/u;)I

    move-result v2

    if-lez v2, :cond_2a

    const-string v9, "android_id"

    const-wide/16 v12, 0x0

    invoke-static {v6, v9, v12, v13}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v12

    int-to-long v0, v2

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x3c

    mul-long v16, v16, v18

    const-wide/16 v18, 0x3e8

    mul-long v16, v16, v18

    rem-long v12, v12, v16

    add-long/2addr v4, v12

    :cond_2a
    add-long/2addr v4, v10

    const-string v2, "SystemUpdateService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "OffPeak download is scheduled at: "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/util/Date;

    invoke-direct {v9, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v14, v9}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v2}, Lcom/google/android/gms/update/s;->a(JZ)V

    if-eqz v7, :cond_2b

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "status"

    const/16 v5, 0x10b

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "download_progress"

    const/4 v5, -0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "verified"

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    :goto_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    const/4 v2, 0x0

    goto/16 :goto_e

    :cond_2b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "status"

    const/16 v5, 0x20b

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "download_progress"

    const/4 v5, -0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "verified"

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_12

    :cond_2c
    const-string v2, "SystemUpdateService"

    const-string v4, "OffPeak download is executed right away!"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v7, :cond_2d

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->b()Z

    move-result v2

    if-eqz v2, :cond_2e

    :cond_2d
    const/4 v2, 0x1

    goto/16 :goto_e

    :cond_2e
    const/4 v2, 0x0

    goto/16 :goto_e

    :cond_2f
    const-string v2, "SystemUpdateService"

    const-string v4, "OffPeak download feature is not enabled!"

    invoke-static {v2, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    goto/16 :goto_e

    .line 884
    :cond_30
    move-object/from16 v0, p0

    invoke-direct {v0, v8, v3}, Lcom/google/android/gms/update/s;->a(Ljava/lang/String;I)Z

    move-result v2

    goto/16 :goto_a

    .line 886
    :cond_31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "update_download_notify_time"

    const-wide/16 v8, 0x0

    invoke-static {v2, v3, v8, v9}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v8, 0x3e8

    mul-long/2addr v2, v8

    .line 888
    sub-long v8, v2, v4

    const-wide v10, 0x9a7ec800L

    cmp-long v7, v8, v10

    if-lez v7, :cond_32

    .line 889
    const-string v7, "SystemUpdateService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "download_notify_time too far in future; ignoring ("

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v7, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 891
    const-wide/16 v2, 0x0

    .line 893
    :cond_32
    cmp-long v7, v4, v2

    if-ltz v7, :cond_34

    .line 894
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->e(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v2

    if-eqz v2, :cond_33

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->f(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v2

    if-nez v2, :cond_33

    const/16 v2, 0xa

    :goto_13
    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/s;->a(I)V

    .line 896
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/update/s;->a(Z)V

    .line 897
    const-wide/32 v2, 0x36ee80

    add-long/2addr v2, v4

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/update/s;->a(JZ)V

    move v2, v6

    goto/16 :goto_a

    .line 894
    :cond_33
    const/4 v2, 0x1

    goto :goto_13

    .line 899
    :cond_34
    const-string v7, "SystemUpdateService"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "deferring notification for "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v4, v2, v4

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/update/s;->a(JZ)V

    move v2, v6

    .line 902
    goto/16 :goto_a

    .line 904
    :cond_35
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "offpeak_download_initial_tried_time"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    move v2, v6

    goto/16 :goto_a

    .line 909
    :cond_36
    const/4 v2, 0x0

    goto/16 :goto_b

    .line 798
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 16

    .prologue
    .line 1218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2, v3}, Lcom/google/android/gms/update/SystemUpdateService;->a(Landroid/content/SharedPreferences;Landroid/content/Context;)J

    move-result-wide v4

    .line 1220
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v3, "connectivity"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/ConnectivityManager;

    .line 1222
    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v13

    .line 1223
    const-string v6, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "network: "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "; metered: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "; mobile allowed: "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    cmp-long v3, v4, v8

    if-gtz v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    invoke-virtual {v2}, Landroid/net/ConnectivityManager;->isActiveNetworkMetered()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/update/c;->a()I

    move-result v2

    if-eqz v2, :cond_3

    .line 1233
    :cond_0
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 1234
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/update/c;->cancel(Z)Z

    .line 1235
    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/c;)Lcom/google/android/gms/update/c;

    .line 1237
    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5, v2}, Lcom/google/android/gms/update/s;->a(JZ)V

    .line 1238
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "status"

    const/16 v4, 0xc

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "download_progress"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "verified"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1243
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    .line 1244
    const/4 v2, 0x0

    .line 1320
    :goto_1
    return v2

    .line 1223
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 1247
    :cond_3
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    if-nez v2, :cond_9

    .line 1249
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->q(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/http/GoogleHttpClient;

    move-result-object v2

    if-nez v2, :cond_4

    .line 1250
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    new-instance v4, Lcom/google/android/gms/http/GoogleHttpClient;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "SystemUpdate-"

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v6, 0x6768a8

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, "/1.0"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    sget-object v2, Lcom/google/android/gms/common/security/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {v4, v5, v6, v7, v2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;ZZ)V

    invoke-static {v3, v4}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;Lcom/google/android/gms/http/GoogleHttpClient;)Lcom/google/android/gms/http/GoogleHttpClient;

    .line 1259
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "update_token"

    invoke-static {v2, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1260
    new-instance v2, Lcom/google/android/gms/update/c;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v4}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v5}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v7, "update_download_prefer_data"

    const/4 v8, 0x0

    invoke-static {v5, v7, v8}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v7

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v5}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v8, "update_download_min_cache_space"

    const-wide/32 v10, 0x6400000

    invoke-static {v5, v8, v10, v11}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v8

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v5}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v10, "update_download_min_data_space"

    const-wide/32 v14, 0x1f400000

    invoke-static {v5, v10, v14, v15}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v10

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v5}, Lcom/google/android/gms/update/SystemUpdateService;->q(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/http/GoogleHttpClient;

    move-result-object v12

    move-object/from16 v5, p1

    invoke-direct/range {v2 .. v12}, Lcom/google/android/gms/update/c;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;ZJJLcom/google/android/gms/http/GoogleHttpClient;)V

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/c;)Lcom/google/android/gms/update/c;

    .line 1266
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/update/c;->a()I

    move-result v2

    .line 1267
    const-string v3, "SystemUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "status is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    invoke-static {v2}, Lcom/google/android/gms/update/c;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1268
    const/4 v3, 0x1

    if-ne v2, v3, :cond_6

    .line 1269
    if-eqz v13, :cond_5

    invoke-virtual {v13}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1270
    :cond_5
    const-string v2, "SystemUpdateService"

    const-string v3, "network down"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1271
    const/4 v2, 0x5

    .line 1272
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/gms/update/SystemUpdateService;->e(Lcom/google/android/gms/update/SystemUpdateService;Z)Z

    .line 1293
    :cond_6
    :goto_2
    const-string v3, "SystemUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "now status is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    invoke-static {v2}, Lcom/google/android/gms/update/c;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1295
    packed-switch v2, :pswitch_data_0

    .line 1318
    :pswitch_0
    const-string v3, "SystemUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "checkDownload unexpected status "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1319
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->c()V

    .line 1320
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1274
    :cond_7
    const-string v2, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "starting download of "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1275
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->r(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 1276
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/a/a;->c()V

    .line 1278
    :cond_8
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/gms/update/c;->n:Z

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    invoke-static {}, Lcom/google/android/gms/a/a;->k()Ljava/util/concurrent/Executor;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/a/a;->a(Landroid/os/AsyncTask;Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1279
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "status"

    const/16 v4, 0xb

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "download_progress"

    const/4 v4, -0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "verified"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1284
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    .line 1285
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/google/android/gms/update/SystemUpdateService;->e(Lcom/google/android/gms/update/SystemUpdateService;Z)Z

    .line 1286
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/update/c;->a()I

    move-result v2

    goto/16 :goto_2

    .line 1290
    :cond_9
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/update/c;->a()I

    move-result v2

    goto/16 :goto_2

    .line 1297
    :pswitch_1
    const/4 v2, 0x1

    goto/16 :goto_1

    .line 1300
    :pswitch_2
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->d()V

    .line 1301
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/update/c;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 1302
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1305
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/android/a/a;->c()V

    .line 1306
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->c()V

    .line 1307
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1310
    :pswitch_4
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/update/s;->c()V

    .line 1311
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 1314
    :pswitch_5
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    if-nez v3, :cond_a

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;I)Z

    move-result v2

    goto/16 :goto_1

    :cond_a
    iget-object v2, v2, Lcom/google/android/gms/update/c;->g:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    goto :goto_3

    .line 1295
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private static b(JLcom/google/android/gms/update/u;)J
    .locals 4

    .prologue
    .line 1934
    if-nez p2, :cond_0

    const-wide v0, 0x7fffffffffffffffL

    .line 1944
    :goto_0
    return-wide v0

    .line 1935
    :cond_0
    iget v0, p2, Lcom/google/android/gms/update/u;->a:I

    iget v1, p2, Lcom/google/android/gms/update/u;->b:I

    if-ne v0, v1, :cond_1

    const-wide/16 v0, 0x0

    goto :goto_0

    .line 1937
    :cond_1
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 1938
    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1939
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    mul-int/lit8 v1, v1, 0x3c

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/2addr v0, v1

    .line 1941
    iget v1, p2, Lcom/google/android/gms/update/u;->a:I

    if-le v0, v1, :cond_2

    .line 1942
    iget v1, p2, Lcom/google/android/gms/update/u;->a:I

    add-int/lit16 v1, v1, 0x5a0

    iput v1, p2, Lcom/google/android/gms/update/u;->a:I

    .line 1944
    :cond_2
    iget v1, p2, Lcom/google/android/gms/update/u;->a:I

    sub-int v0, v1, v0

    mul-int/lit8 v0, v0, 0x3c

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Lcom/google/android/gms/update/u;
    .locals 9

    .prologue
    const/16 v8, 0x3b

    const/16 v7, 0x17

    const/4 v0, 0x0

    .line 1816
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v1}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1817
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "none"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1847
    :cond_0
    :goto_0
    return-object v0

    .line 1824
    :cond_1
    sget-object v2, Lcom/google/android/gms/update/SystemUpdateService;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 1825
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1826
    const-string v2, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "malformed maintenance window \""

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1831
    :cond_2
    const/4 v3, 0x1

    :try_start_0
    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1832
    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1833
    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1834
    const/4 v6, 0x4

    invoke-virtual {v2, v6}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 1840
    if-gt v3, v7, :cond_3

    if-gt v4, v8, :cond_3

    if-gt v5, v7, :cond_3

    if-le v2, v8, :cond_4

    .line 1841
    :cond_3
    const-string v2, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "malformed "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " window \""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\""

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1835
    :catch_0
    move-exception v0

    .line 1837
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 1844
    :cond_4
    new-instance v0, Lcom/google/android/gms/update/u;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/update/u;-><init>(Lcom/google/android/gms/update/s;B)V

    .line 1845
    mul-int/lit8 v1, v3, 0x3c

    add-int/2addr v1, v4

    iput v1, v0, Lcom/google/android/gms/update/u;->a:I

    .line 1846
    mul-int/lit8 v1, v5, 0x3c

    add-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/gms/update/u;->b:I

    goto/16 :goto_0
.end method

.method private b()Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1022
    .line 1023
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v3, "phone"

    invoke-virtual {v0, v3}, Lcom/google/android/gms/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1024
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/checkin/CheckinService;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 1027
    const-string v4, "SystemUpdateService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Last checkin carrier is: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " current sim data carrier is: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1029
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    move v0, v2

    .line 1034
    :goto_1
    if-eqz v0, :cond_2

    .line 1035
    const-string v0, "SystemUpdateService"

    const-string v2, "OffPeak download should not happen as operator is different!"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1036
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "status"

    const/16 v3, 0x30b

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "download_progress"

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "verified"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    move v0, v1

    .line 1044
    :goto_2
    return v0

    .line 1023
    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1044
    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private b(Ljava/lang/String;I)Z
    .locals 12

    .prologue
    const/4 v10, 0x2

    const-wide v6, 0x7fffffffffffffffL

    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1325
    const-string v0, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "processDownloadedFile "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1327
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1328
    const-string v0, "SystemUpdateService"

    const-string v1, "download completed but no filename available"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1329
    invoke-direct {p0}, Lcom/google/android/gms/update/s;->c()V

    .line 1330
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/update/c;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 1457
    :cond_0
    :goto_0
    return v2

    .line 1334
    :cond_1
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "filename"

    invoke-interface {v0, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1335
    const-string v0, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "download filename now "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1336
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "filename"

    invoke-interface {v0, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v3, "verified"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1340
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->n(Lcom/google/android/gms/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1341
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->o(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/v;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1342
    const-string v0, "SystemUpdateService"

    const-string v4, "checkDownload: cancelling verifier"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1343
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->o(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/v;

    move-result-object v0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/google/android/gms/update/v;->cancel(Z)Z

    .line 1344
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;Lcom/google/android/gms/update/v;)Lcom/google/android/gms/update/v;

    .line 1346
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1349
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 1351
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "verified"

    invoke-interface {v0, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 1352
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->n(Lcom/google/android/gms/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1353
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v4, 0x0

    invoke-static {v0, v4}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;Lcom/google/android/gms/update/v;)Lcom/google/android/gms/update/v;

    .line 1354
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1355
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "verified"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 1356
    const-string v0, "SystemUpdateService"

    const-string v3, "file has been verified"

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1357
    invoke-direct {p0}, Lcom/google/android/gms/update/s;->e()Ljava/lang/String;

    .line 1358
    const/4 v0, 0x6

    if-ne p2, v0, :cond_7

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->c()J

    move-result-wide v4

    cmp-long v0, v8, v4

    if-lez v0, :cond_7

    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/update/s;->a(J)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "update_battery_aware_automatic_timed"

    invoke-static {v0, v3, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->d(Lcom/google/android/gms/update/SystemUpdateService;)I

    move-result v0

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    const-string v3, "SystemUpdateService"

    invoke-static {v3, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v3, "SystemUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Battery aware automatic timed update. Is battery sufficient? "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    :goto_2
    if-eqz v0, :cond_c

    .line 1362
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    iget-object v4, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v4}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "update_download_min_data_space"

    const-wide/32 v6, 0x1f400000

    invoke-static {v4, v5, v6, v7}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/gms/a/a;->a(Landroid/content/Context;J)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1365
    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(I)V

    .line 1366
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "update_retry_insufficient_space_frequency"

    const-wide/32 v6, 0x5265c00

    invoke-static {v0, v3, v6, v7}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/gms/update/s;->a(JZ)V

    .line 1370
    const-string v0, "SystemUpdateService"

    const-string v1, "Not enough size on /data, will not install."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 1346
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1354
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_5
    move v0, v2

    .line 1358
    goto :goto_1

    :cond_6
    move v0, v1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "install_approved"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_2

    .line 1373
    :cond_8
    const-string v0, "SystemUpdateService"

    const-string v2, "Enough size on /data, ready to install."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    const-string v0, "SystemUpdateService"

    const-string v2, "called install()"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/update/s;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_b

    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "status"

    const/4 v4, 0x5

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "install_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface {v3, v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "pending_filename"

    invoke-interface {v3, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    iget-object v3, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v3}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "update_install_success_message"

    invoke-static {v3, v4}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_9

    const-string v4, "success_message"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_9
    iget-object v3, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v3}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "update_install_failure_message"

    invoke-static {v3, v4}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    const-string v4, "failure_message"

    invoke-interface {v2, v4, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    :cond_a
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-direct {p0}, Lcom/google/android/gms/update/s;->g()V

    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v3, "SystemUpdateService"

    const-string v4, "calling installPackage()"

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Lcom/google/android/gms/update/t;

    invoke-direct {v3, p0, v2, v0}, Lcom/google/android/gms/update/t;-><init>(Lcom/google/android/gms/update/s;Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/android/gms/update/t;->start()V

    :cond_b
    move v2, v1

    .line 1375
    goto/16 :goto_0

    .line 1377
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->d(Lcom/google/android/gms/update/SystemUpdateService;)I

    move-result v0

    if-ne v0, v1, :cond_e

    .line 1378
    const/16 v0, 0xe

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(I)V

    .line 1384
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a;->a()V

    .line 1386
    const/4 v0, 0x6

    if-ne p2, v0, :cond_d

    .line 1387
    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/update/s;->a(J)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 1389
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->c()J

    move-result-wide v4

    cmp-long v0, v4, v8

    if-lez v0, :cond_10

    .line 1390
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->c()J

    move-result-wide v4

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/gms/update/s;->a(JZ)V

    .line 1425
    :cond_d
    :goto_4
    sget-boolean v0, Lcom/google/android/gms/update/SystemUpdateService;->b:Z

    if-nez v0, :cond_0

    .line 1426
    invoke-direct {p0, v2}, Lcom/google/android/gms/update/s;->a(Z)V

    goto/16 :goto_0

    .line 1379
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->d(Lcom/google/android/gms/update/SystemUpdateService;)I

    move-result v0

    if-ne v0, v10, :cond_f

    .line 1380
    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(I)V

    goto :goto_3

    .line 1382
    :cond_f
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(I)V

    goto :goto_3

    .line 1396
    :cond_10
    const-string v0, "update_maintenance_window"

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v0

    invoke-static {v8, v9, v0}, Lcom/google/android/gms/update/s;->b(JLcom/google/android/gms/update/u;)J

    move-result-wide v4

    .line 1397
    cmp-long v0, v4, v6

    if-eqz v0, :cond_d

    const-wide/32 v6, 0xea60

    cmp-long v0, v4, v6

    if-lez v0, :cond_d

    .line 1399
    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    add-long/2addr v4, v8

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/gms/update/s;->a(JZ)V

    goto :goto_4

    .line 1403
    :cond_11
    const-string v0, "update_maintenance_window"

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v0

    if-nez v0, :cond_13

    move-wide v4, v6

    :goto_5
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->c()J

    move-result-wide v10

    sub-long/2addr v10, v8

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 1405
    cmp-long v0, v4, v6

    if-eqz v0, :cond_d

    .line 1416
    const-string v0, "update_maintenance_window"

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->b(Ljava/lang/String;)Lcom/google/android/gms/update/u;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/update/s;->a(Lcom/google/android/gms/update/u;)I

    move-result v0

    const/16 v3, 0x41

    if-lt v0, v3, :cond_12

    .line 1417
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "android_id"

    const-wide/16 v6, 0x0

    invoke-static {v0, v3, v6, v7}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    .line 1419
    const-wide/32 v10, 0x36ee80

    rem-long/2addr v6, v10

    add-long/2addr v4, v6

    .line 1421
    :cond_12
    add-long/2addr v4, v8

    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/gms/update/s;->a(JZ)V

    goto :goto_4

    .line 1403
    :cond_13
    iget v3, v0, Lcom/google/android/gms/update/u;->a:I

    iget v4, v0, Lcom/google/android/gms/update/u;->b:I

    if-ne v3, v4, :cond_14

    const-wide/16 v4, 0x0

    goto :goto_5

    :cond_14
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/16 v4, 0xb

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    mul-int/lit8 v4, v4, 0x3c

    const/16 v5, 0xc

    invoke-virtual {v3, v5}, Ljava/util/Calendar;->get(I)I

    move-result v3

    add-int/2addr v3, v4

    iget v4, v0, Lcom/google/android/gms/update/u;->a:I

    if-le v3, v4, :cond_15

    iget v4, v0, Lcom/google/android/gms/update/u;->a:I

    add-int/lit16 v4, v4, 0x5a0

    iput v4, v0, Lcom/google/android/gms/update/u;->a:I

    :cond_15
    iget v0, v0, Lcom/google/android/gms/update/u;->a:I

    sub-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x3c

    int-to-long v4, v0

    const-wide/16 v10, 0x3e8

    mul-long/2addr v4, v10

    goto :goto_5

    .line 1430
    :cond_16
    const-string v0, "SystemUpdateService"

    const-string v1, "verification of system update package failed"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1431
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v3, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v3}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "update_verify_redownload_delay_sec"

    const v5, 0xa8c0

    invoke-static {v3, v4, v5}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    int-to-long v4, v3

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    add-long/2addr v0, v4

    .line 1435
    iget-object v3, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v3}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Lcom/android/a/a;->b(J)V

    .line 1436
    invoke-direct {p0}, Lcom/google/android/gms/update/s;->c()V

    .line 1437
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/update/c;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 1438
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(I)V

    goto/16 :goto_0

    .line 1443
    :cond_17
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->n(Lcom/google/android/gms/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1444
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/update/s;->e()Ljava/lang/String;

    .line 1445
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->o(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/v;

    move-result-object v0

    if-eqz v0, :cond_18

    .line 1447
    const-string v0, "SystemUpdateService"

    const-string v1, "verification already in progress"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1448
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto/16 :goto_0

    .line 1458
    :catchall_2
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1451
    :cond_18
    :try_start_3
    const-string v0, "SystemUpdateService"

    const-string v2, "starting package verification"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(I)V

    .line 1453
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    new-instance v2, Lcom/google/android/gms/update/v;

    iget-object v4, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v6}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v6

    invoke-direct {v2, v4, v5, v6}, Lcom/google/android/gms/update/v;-><init>(Landroid/content/Context;Ljava/io/File;Landroid/content/SharedPreferences;)V

    invoke-static {v0, v2}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;Lcom/google/android/gms/update/v;)Lcom/google/android/gms/update/v;

    .line 1455
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->o(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/v;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    invoke-static {}, Lcom/google/android/gms/a/a;->k()Ljava/util/concurrent/Executor;

    move-result-object v4

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/gms/a/a;->a(Landroid/os/AsyncTask;Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1457
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move v2, v1

    goto/16 :goto_0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v5, 0x6

    .line 1164
    invoke-direct {p0, v5}, Lcom/google/android/gms/update/s;->a(I)V

    .line 1165
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a;->b()V

    .line 1166
    const-string v0, "SystemUpdateService"

    const-string v1, "download failed; clearing attempt"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1167
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1168
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/gms/update/c;->l:J

    .line 1169
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 1170
    const-string v2, "SystemUpdateService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "server-suggested retry time is "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1171
    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/android/a/a;->b(J)V

    .line 1174
    :cond_0
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/c;)Lcom/google/android/gms/update/c;

    .line 1175
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1176
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "status"

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-eq v1, v5, :cond_1

    .line 1177
    const-string v1, "status"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1178
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    .line 1180
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->l(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->m(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/android/a/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/update/s;->a(JZ)V

    .line 1181
    return-void
.end method

.method private d()V
    .locals 7

    .prologue
    const/16 v3, 0x9

    const/4 v6, 0x1

    .line 1184
    const-string v0, "SystemUpdateService"

    const-string v1, "download failed due to space; clearing attempt"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1185
    invoke-direct {p0, v3}, Lcom/google/android/gms/update/s;->a(I)V

    .line 1186
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/c;)Lcom/google/android/gms/update/c;

    .line 1187
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "status"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 1188
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1189
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    .line 1191
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/update/SystemUpdateService;->b:Z

    if-eqz v0, :cond_1

    const-string v0, "SystemUpdateService"

    const-string v1, "skipping notification"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1192
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v2}, Lcom/google/android/gms/update/SystemUpdateService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "update_retry_insufficient_space_frequency"

    const-wide/32 v4, 0x5265c00

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    add-long/2addr v0, v2

    invoke-direct {p0, v0, v1, v6}, Lcom/google/android/gms/update/s;->a(JZ)V

    .line 1195
    return-void

    .line 1191
    :cond_1
    const-string v0, "android.settings.SYSTEM_UPDATE_SETTINGS"

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "SystemUpdateService"

    const-string v1, "no activity screen to notify"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v1}, Lcom/google/android/gms/update/SystemUpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->ys:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->yD:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/a/a;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v4, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    sget v4, Lcom/google/android/gms/h;->cf:I

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/Notification$Builder;->setWhen(J)Landroid/app/Notification$Builder;

    move-result-object v1

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "sys"

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v2, "notification"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->cf:I

    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {v2, v0, v3, v1}, Lcom/google/android/gms/a/a;->a(Landroid/app/NotificationManager;ILandroid/app/Notification;)V

    goto/16 :goto_0
.end method

.method private e()Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1497
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "filename"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1498
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1499
    const-string v1, "SystemUpdateService"

    const-string v2, "OTA package filename empty"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1509
    :goto_0
    return-object v0

    .line 1502
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1503
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1504
    const-string v1, "SystemUpdateService"

    const-string v2, "OTA package doesn\'t exist!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1507
    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 1508
    const-string v0, "SystemUpdateService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "OTA package size = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 1509
    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1558
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/update/SystemUpdateService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 1559
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->p(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 1561
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->n(Lcom/google/android/gms/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1562
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->o(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/v;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1563
    const-string v0, "SystemUpdateService"

    const-string v2, "cancelUpdate: cancelling verifier"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1564
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->o(Lcom/google/android/gms/update/SystemUpdateService;)Lcom/google/android/gms/update/v;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/update/v;->cancel(Z)Z

    .line 1565
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/SystemUpdateService;Lcom/google/android/gms/update/v;)Lcom/google/android/gms/update/v;

    .line 1567
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1569
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1570
    const-string v0, "SystemUpdateService"

    const-string v1, "cancelling current attempt"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1571
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->b()Lcom/google/android/gms/update/c;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/update/c;->cancel(Z)Z

    .line 1572
    invoke-static {v4}, Lcom/google/android/gms/update/SystemUpdateService;->a(Lcom/google/android/gms/update/c;)Lcom/google/android/gms/update/c;

    .line 1574
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/update/c;->a(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 1576
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "download_approved"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "install_approved"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "url"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "url_change"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "download_mobile"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "filename"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "download_id"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "verified"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "install_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "started_download"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "pending_filename"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "success_message"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "failure_message"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "verify_progress"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "notify_repeat"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "notify_snooze"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "offpeak_download_initial_tried_time"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "status"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1597
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->a(Landroid/content/Context;)V

    .line 1598
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->g(Lcom/google/android/gms/update/SystemUpdateService;)V

    .line 1599
    return-void

    .line 1567
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 1782
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "status"

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    and-int/lit8 v0, v0, 0x1f

    .line 1784
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "download_approved"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1785
    or-int/lit8 v0, v0, 0x20

    .line 1788
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "install_approved"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1789
    or-int/lit8 v0, v0, 0x40

    .line 1793
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "verified"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1794
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "verified"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_0
    shl-int/lit8 v1, v1, 0x7

    or-int/2addr v0, v1

    .line 1797
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/update/s;->c:Z

    if-eqz v1, :cond_3

    .line 1798
    or-int/lit16 v0, v0, 0x200

    .line 1802
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v4, "download_mobile"

    invoke-interface {v1, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1803
    or-int/lit16 v0, v0, 0x400

    .line 1805
    :cond_4
    const v1, 0x31129

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    iget v0, p0, Lcom/google/android/gms/update/s;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v3

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateService;->b(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "url"

    const/4 v5, 0x0

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v0

    invoke-static {v1, v4}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 1808
    return-void

    :cond_5
    move v1, v3

    .line 1794
    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 668
    check-cast p1, [Landroid/content/Intent;

    array-length v0, p1

    if-lez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/update/s;->a(Landroid/content/Intent;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 7

    .prologue
    .line 668
    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->h(Lcom/google/android/gms/update/SystemUpdateService;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/gms/update/SystemUpdateService;->c(Lcom/google/android/gms/update/SystemUpdateService;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->i(Lcom/google/android/gms/update/SystemUpdateService;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateService;->j(Lcom/google/android/gms/update/SystemUpdateService;)Z

    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/gms/update/SystemUpdateService;->c(Lcom/google/android/gms/update/SystemUpdateService;Z)Z

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/update/s;

    iget-object v3, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-direct {v2, v3}, Lcom/google/android/gms/update/s;-><init>(Lcom/google/android/gms/update/SystemUpdateService;)V

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    invoke-static {}, Lcom/google/android/gms/a/a;->k()Ljava/util/concurrent/Executor;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Landroid/content/Intent;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-static {v6}, Lcom/google/android/gms/update/SystemUpdateService;->k(Lcom/google/android/gms/update/SystemUpdateService;)Landroid/content/Intent;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/a/a;->a(Landroid/os/AsyncTask;Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/update/s;->a:Lcom/google/android/gms/update/SystemUpdateService;

    invoke-virtual {v0}, Lcom/google/android/gms/update/SystemUpdateService;->stopSelf()V

    :cond_3
    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->a()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->a()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/update/SystemUpdateService;->a()Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
