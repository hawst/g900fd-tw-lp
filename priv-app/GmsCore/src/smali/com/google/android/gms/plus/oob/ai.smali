.class public final Lcom/google/android/gms/plus/oob/ai;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/plus/oob/aj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/gms/plus/oob/aj;

    invoke-direct {v0}, Lcom/google/android/gms/plus/oob/aj;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/ai;->a:Lcom/google/android/gms/plus/oob/aj;

    .line 35
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)Lcom/google/android/gms/plus/oob/ai;
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/plus/oob/ai;

    invoke-direct {v0}, Lcom/google/android/gms/plus/oob/ai;-><init>()V

    .line 28
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/oob/aj;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/ai;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/oob/ag;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ai;->a:Lcom/google/android/gms/plus/oob/aj;

    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ai;->a:Lcom/google/android/gms/plus/oob/aj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/oob/aj;->a(Landroid/app/Activity;)V

    .line 41
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 51
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/ai;->setRetainInstance(Z)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ai;->a:Lcom/google/android/gms/plus/oob/aj;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ai;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/aj;->a(Landroid/os/Bundle;)V

    .line 54
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ai;->a:Lcom/google/android/gms/plus/oob/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/aj;->e()V

    .line 66
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ai;->a:Lcom/google/android/gms/plus/oob/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/aj;->a()V

    .line 47
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ai;->a:Lcom/google/android/gms/plus/oob/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/aj;->c()V

    .line 60
    return-void
.end method
