.class public final Lcom/google/android/gms/googlehelp/c/m;
.super Landroid/widget/EditText;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/c;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/googlehelp/c/n;

.field private final b:Landroid/widget/TextView;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Z

.field private g:Lcom/google/android/gms/googlehelp/c/d;

.field private h:Lcom/google/android/gms/googlehelp/c/ac;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/h;Lcom/google/android/gms/googlehelp/c/n;Z)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x3
    .end annotation

    .prologue
    const/4 v2, 0x5

    const/4 v5, 0x3

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 75
    invoke-direct {p0, p1}, Landroid/widget/EditText;-><init>(Landroid/content/Context;)V

    .line 77
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/m;->g:Lcom/google/android/gms/googlehelp/c/d;

    .line 78
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/c/m;->a:Lcom/google/android/gms/googlehelp/c/n;

    .line 79
    iget-object v0, p3, Lcom/google/ad/a/a/h;->l:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->d:Ljava/lang/String;

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/m;->getTextColors()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/googlehelp/c/m;->e:I

    .line 81
    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/m;->h:Lcom/google/android/gms/googlehelp/c/ac;

    .line 82
    iput-boolean v3, p0, Lcom/google/android/gms/googlehelp/c/m;->i:Z

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/m;->f()V

    .line 88
    :cond_0
    if-eqz p5, :cond_2

    .line 89
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/m;->i:Z

    if-nez v0, :cond_1

    iget-object v0, p3, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 90
    iget-object v0, p3, Lcom/google/ad/a/a/h;->f:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setText(Ljava/lang/CharSequence;)V

    .line 92
    :cond_1
    iget-boolean v0, p3, Lcom/google/ad/a/a/h;->c:Z

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/m;->f:Z

    .line 99
    :goto_0
    iget-object v0, p3, Lcom/google/ad/a/a/h;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v4, p0, Lcom/google/android/gms/googlehelp/c/m;->f:Z

    invoke-static {p0, v0, v4}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/EditText;Ljava/lang/String;Z)V

    .line 102
    iget v0, p3, Lcom/google/ad/a/a/h;->d:I

    if-ne v0, v5, :cond_7

    .line 103
    invoke-virtual {p0, v3}, Lcom/google/android/gms/googlehelp/c/m;->setSingleLine(Z)V

    .line 104
    iget-object v0, p3, Lcom/google/ad/a/a/h;->h:Lcom/google/ad/a/a/j;

    if-eqz v0, :cond_3

    iget-object v0, p3, Lcom/google/ad/a/a/h;->h:Lcom/google/ad/a/a/j;

    iget-object v0, v0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    .line 105
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v0, v2

    :goto_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setLines(I)V

    .line 106
    const/16 v0, 0x33

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setGravity(I)V

    .line 107
    iget-object v0, p3, Lcom/google/ad/a/a/h;->h:Lcom/google/ad/a/a/j;

    if-eqz v0, :cond_5

    iget-object v0, p3, Lcom/google/ad/a/a/h;->h:Lcom/google/ad/a/a/j;

    iget v0, v0, Lcom/google/ad/a/a/j;->c:I

    :goto_3
    iput v0, p0, Lcom/google/android/gms/googlehelp/c/m;->c:I

    .line 110
    iget v0, p0, Lcom/google/android/gms/googlehelp/c/m;->c:I

    if-lez v0, :cond_6

    .line 111
    const/4 v0, 0x1

    new-array v0, v0, [Landroid/text/InputFilter;

    .line 112
    new-instance v1, Landroid/text/InputFilter$LengthFilter;

    iget v4, p0, Lcom/google/android/gms/googlehelp/c/m;->c:I

    invoke-direct {v1, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v1, v0, v3

    .line 113
    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setFilters([Landroid/text/InputFilter;)V

    .line 114
    iget v0, p0, Lcom/google/android/gms/googlehelp/c/m;->c:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/f;->J:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setGravity(I)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->b:Landroid/widget/TextView;

    .line 124
    :goto_4
    iget v0, p3, Lcom/google/ad/a/a/h;->d:I

    packed-switch v0, :pswitch_data_0

    .line 132
    :goto_5
    :pswitch_0
    return-void

    .line 95
    :cond_2
    iput-boolean v3, p0, Lcom/google/android/gms/googlehelp/c/m;->f:Z

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 104
    goto :goto_1

    .line 105
    :cond_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    :cond_5
    move v0, v3

    .line 107
    goto :goto_3

    .line 116
    :cond_6
    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/m;->b:Landroid/widget/TextView;

    goto :goto_4

    .line 119
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/m;->setSingleLine()V

    .line 120
    iput-object v1, p0, Lcom/google/android/gms/googlehelp/c/m;->b:Landroid/widget/TextView;

    .line 121
    iput v3, p0, Lcom/google/android/gms/googlehelp/c/m;->c:I

    goto :goto_4

    .line 126
    :pswitch_1
    invoke-virtual {p0, v5}, Lcom/google/android/gms/googlehelp/c/m;->setInputType(I)V

    goto :goto_5

    .line 130
    :pswitch_2
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setInputType(I)V

    goto :goto_5

    .line 124
    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private f()V
    .locals 2

    .prologue
    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/m;->i:Z

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->d:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setText(Ljava/lang/CharSequence;)V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/m;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->N:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setTextColor(I)V

    .line 172
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/c/ac;)V
    .locals 0

    .prologue
    .line 213
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/m;->h:Lcom/google/android/gms/googlehelp/c/ac;

    .line 214
    return-void
.end method

.method public final a()Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/m;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->g:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/d;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->b:Landroid/widget/TextView;

    return-object v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/m;->i:Z

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 196
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/m;->f:Z

    return v0
.end method

.method protected final onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 156
    if-eqz p1, :cond_1

    .line 157
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/m;->i:Z

    if-eqz v0, :cond_0

    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/m;->i:Z

    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setText(Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/google/android/gms/googlehelp/c/m;->e:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/m;->setTextColor(I)V

    .line 165
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 166
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/m;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/m;->f()V

    goto :goto_0
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->h:Lcom/google/android/gms/googlehelp/c/ac;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->h:Lcom/google/android/gms/googlehelp/c/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/ac;->a()V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->b:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->b:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/googlehelp/c/m;->c:I

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->a:Lcom/google/android/gms/googlehelp/c/n;

    if-eqz v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/m;->a:Lcom/google/android/gms/googlehelp/c/n;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/n;->a()V

    .line 152
    :cond_2
    return-void
.end method
