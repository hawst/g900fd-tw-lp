.class final Lcom/google/android/gms/wallet/common/ui/b;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/content/Context;[Lcom/google/android/gms/wallet/common/ui/a;)V
    .locals 1

    .prologue
    .line 225
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 226
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/b;->a:Landroid/view/LayoutInflater;

    .line 227
    return-void
.end method

.method private a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 241
    if-nez p3, :cond_0

    .line 242
    if-eqz p2, :cond_2

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/b;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->gN:I

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 250
    :cond_0
    :goto_0
    if-ltz p1, :cond_1

    .line 251
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/b;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/a;

    .line 252
    sget v1, Lcom/google/android/gms/j;->mi:I

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 253
    if-nez p2, :cond_3

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/a;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 254
    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 261
    :cond_1
    return-object p3

    .line 246
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/b;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->gM:I

    invoke-virtual {v0, v1, p4, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto :goto_0

    .line 256
    :cond_3
    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/a;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 231
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/b;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/b;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
