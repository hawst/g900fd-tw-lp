.class public final Lcom/google/android/gms/deviceconnection/service/a;
.super Lcom/google/android/gms/deviceconnection/c/g;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/deviceconnection/service/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/deviceconnection/service/c;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/deviceconnection/c/g;-><init>()V

    .line 122
    iput-object p1, p0, Lcom/google/android/gms/deviceconnection/service/a;->a:Landroid/content/Context;

    .line 123
    iput-object p2, p0, Lcom/google/android/gms/deviceconnection/service/a;->b:Ljava/lang/String;

    .line 124
    iput-object p3, p0, Lcom/google/android/gms/deviceconnection/service/a;->c:Lcom/google/android/gms/deviceconnection/service/c;

    .line 127
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 171
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/a;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/service/a;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 174
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/deviceconnection/c/c;)V
    .locals 2

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/deviceconnection/service/a;->a()V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/a;->c:Lcom/google/android/gms/deviceconnection/service/c;

    new-instance v1, Lcom/google/android/gms/deviceconnection/service/a/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/deviceconnection/service/a/a;-><init>(Lcom/google/android/gms/deviceconnection/c/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/deviceconnection/service/c;->a(Lcom/google/android/gms/deviceconnection/service/b;)V

    .line 150
    return-void
.end method

.method public final a(Lcom/google/android/gms/deviceconnection/c/c;[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/deviceconnection/service/a;->a()V

    .line 135
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    array-length v0, p2

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    move v0, v2

    .line 137
    :goto_1
    array-length v3, p2

    if-ge v0, v3, :cond_2

    aget-object v3, p2, v0

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    aget-object v3, p2, v0

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    :goto_2
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    .line 136
    goto :goto_0

    :cond_1
    move v3, v2

    .line 137
    goto :goto_2

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/a;->c:Lcom/google/android/gms/deviceconnection/service/c;

    new-instance v1, Lcom/google/android/gms/deviceconnection/service/a/b;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/deviceconnection/service/a/b;-><init>(Lcom/google/android/gms/deviceconnection/c/c;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/deviceconnection/service/c;->a(Lcom/google/android/gms/deviceconnection/service/b;)V

    .line 141
    return-void
.end method
