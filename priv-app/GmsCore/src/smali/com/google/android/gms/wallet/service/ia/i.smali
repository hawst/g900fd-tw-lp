.class public final Lcom/google/android/gms/wallet/service/ia/i;
.super Lcom/google/android/gms/wallet/service/ia/m;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ia/m;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ia/m;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ia/m;-><init>()V

    .line 22
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->b:Landroid/content/Context;

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    .line 24
    return-void
.end method

.method private a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->b:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/Throwable;)V

    .line 158
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    .line 121
    :catch_0
    move-exception v0

    .line 122
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 142
    :goto_0
    return-object v0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 132
    :goto_0
    return-object v0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 52
    :goto_0
    return-object v0

    .line 51
    :catch_0
    move-exception v0

    .line 52
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 30
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 32
    :goto_0
    return-object v0

    .line 31
    :catch_0
    move-exception v0

    .line 32
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 92
    :goto_0
    return-object v0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 112
    :goto_0
    return-object v0

    .line 111
    :catch_0
    move-exception v0

    .line 112
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 100
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 102
    :goto_0
    return-object v0

    .line 101
    :catch_0
    move-exception v0

    .line 102
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 150
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 152
    :goto_0
    return-object v0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    .line 81
    :catch_0
    move-exception v0

    .line 82
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 61
    :catch_0
    move-exception v0

    .line 62
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/i;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    .line 41
    :catch_0
    move-exception v0

    .line 42
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/service/ia/i;->a(Ljava/lang/Throwable;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
