.class public Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/wallet/ow/t;


# instance fields
.field a:Lcom/google/android/gms/wallet/ow/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Lcom/google/android/gms/wallet/ow/v;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/wallet/ow/v;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Lcom/google/android/gms/wallet/ow/v;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/ow/v;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v0, Lcom/google/android/gms/wallet/ow/v;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/ow/v;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/aa/b/a/h;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/v;->a(Lcom/google/aa/b/a/h;)V

    .line 53
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/ow/u;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ow/v;->b:Lcom/google/android/gms/wallet/ow/u;

    .line 43
    return-void
.end method

.method public final a([Lcom/google/aa/b/a/h;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/v;->a([Lcom/google/aa/b/a/h;)V

    .line 48
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/ow/v;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 58
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/v;->onNothingSelected(Landroid/widget/AdapterView;)V

    .line 63
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->isEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 68
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/w;

    .line 70
    if-eqz v0, :cond_0

    .line 71
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/w;->notifyDataSetChanged()V

    .line 74
    :cond_0
    return-void
.end method
