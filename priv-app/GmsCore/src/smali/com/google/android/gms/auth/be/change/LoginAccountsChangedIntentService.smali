.class public Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "GLSUser"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "LoginAccountsChangedIntentService"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    const-string v0, "LoginAccountsChangedIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 67
    return-void
.end method

.method private a(Z)Lcom/google/android/gms/auth/be/change/b;
    .locals 3

    .prologue
    .line 105
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/auth/be/change/a;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/change/b;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/change/b;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_1

    .line 107
    :cond_0
    sget-object v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Summary factory needs to be re-calculated."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 112
    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/change/b;->b()V

    .line 113
    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/change/b;->a()Lcom/google/android/gms/auth/be/change/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :cond_1
    return-object v0

    .line 116
    :catch_0
    move-exception v0

    .line 118
    new-instance v1, Lcom/google/android/gms/auth/q;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/q;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a()Ljava/util/List;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 259
    const-string v0, "user"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    .line 260
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getApplicationRestrictions(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_1

    .line 264
    :try_start_0
    const-string v1, "factoryResetProtectionAdmin"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 265
    if-eqz v1, :cond_0

    .line 266
    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 276
    :goto_0
    return-object v0

    .line 268
    :cond_0
    const-string v1, "factoryResetProtectionAdmin"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 269
    if-eqz v0, :cond_1

    .line 270
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 272
    :catch_0
    move-exception v0

    .line 273
    sget-object v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Failed to read application restriction."

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 276
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v5

    .line 163
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a()Ljava/util/List;

    move-result-object v0

    .line 164
    :goto_0
    const-string v1, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v3

    :goto_1
    if-nez v1, :cond_4

    .line 165
    sget-object v0, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "No need to update account challenges."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 213
    :goto_2
    return-void

    .line 163
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 164
    :cond_1
    const-string v1, "com.google.android.gms.auth.FRP_CONFIG_CHANGED"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    sget-object v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Get application restrictions changed intent."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    move v1, v3

    goto :goto_1

    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_3

    move v1, v3

    goto :goto_1

    :cond_3
    move v1, v4

    goto :goto_1

    .line 169
    :cond_4
    sget-object v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Updating account challenges..."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 170
    new-instance v1, Lcom/google/android/gms/auth/firstparty/dataservice/w;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;-><init>(Landroid/content/Context;)V

    .line 172
    invoke-static {p0}, Lcom/google/android/gms/auth/be/a/b;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/a/b;

    move-result-object v6

    .line 173
    invoke-virtual {v6}, Lcom/google/android/gms/auth/be/a/b;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 177
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 178
    sget-object v0, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "No accountId in application restrictions."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 179
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 180
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 186
    :try_start_0
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 196
    :goto_4
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 197
    sget-object v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v5, "Critial error: accountId is empty for account %s."

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-static {v5, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 202
    invoke-interface {v2}, Ljava/util/List;->clear()V

    move-object v0, v2

    .line 209
    :cond_5
    :goto_5
    invoke-virtual {v6, v0}, Lcom/google/android/gms/auth/be/a/b;->a(Ljava/util/List;)Z

    goto/16 :goto_2

    .line 187
    :catch_0
    move-exception v1

    .line 188
    :goto_6
    sget-object v7, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v8, "Cannot get accountId."

    invoke-virtual {v7, v8, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 189
    const/4 v1, 0x0

    goto :goto_4

    .line 205
    :cond_6
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 211
    :cond_7
    sget-object v0, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "FRP is not supported on current device."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 187
    :catch_1
    move-exception v1

    goto :goto_6

    :cond_8
    move-object v0, v2

    goto :goto_5
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 72
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    .line 73
    const-string v0, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_4

    .line 75
    const/4 v0, 0x0

    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a(Z)Lcom/google/android/gms/auth/be/change/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/change/b;->a()Lcom/google/android/gms/auth/be/change/a;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/auth/be/change/a;->a:Lcom/google/android/gms/auth/be/change/a;

    if-eq v4, v0, :cond_3

    new-instance v0, Landroid/content/Intent;

    const-string v5, "com.google.android.gms.auth.GOOGLE_ACCOUNT_CHANGE"

    invoke-direct {v0, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, v4, Lcom/google/android/gms/auth/be/change/a;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    const-string v2, "com.google.android.gms.auth.category.ACCOUNT_ADDED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "added"

    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, v4, Lcom/google/android/gms/auth/be/change/a;->c:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move v2, v1

    :cond_0
    iget-object v5, v4, Lcom/google/android/gms/auth/be/change/a;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_1

    const-string v2, "com.google.android.gms.auth.category.ACCOUNT_REMOVED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "removed"

    new-instance v5, Ljava/util/ArrayList;

    iget-object v6, v4, Lcom/google/android/gms/auth/be/change/a;->d:Ljava/util/List;

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move v2, v1

    :cond_1
    iget-object v5, v4, Lcom/google/android/gms/auth/be/change/a;->e:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    const-string v2, "com.google.android.gms.auth.category.ACCOUNT_MUTATED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "mutated"

    new-instance v5, Ljava/util/ArrayList;

    iget-object v4, v4, Lcom/google/android/gms/auth/be/change/a;->e:Ljava/util/List;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0, v2, v5}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    :goto_0
    if-eqz v1, :cond_3

    .line 77
    :goto_1
    if-eqz v0, :cond_2

    .line 78
    const-string v1, "com.google.android.gms.auth.permission.GOOGLE_ACCOUNT_CHANGE"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    :cond_2
    :goto_2
    :try_start_2
    invoke-direct {p0, v3}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    invoke-static {p1}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedWakefulBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 95
    return-void

    .line 75
    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    .line 80
    :catch_0
    move-exception v0

    .line 81
    :try_start_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.auth.UNKNOWN_ACCOUNTS_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.auth.permission.GOOGLE_ACCOUNT_CHANGE"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 82
    sget-object v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Unable to get AccountStateFactory."

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 94
    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedWakefulBroadcastReceiver;->a(Landroid/content/Intent;)Z

    throw v0

    .line 86
    :cond_4
    :try_start_4
    const-string v0, "com.google.android.gms.GMS_UPDATED"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a(Z)Lcom/google/android/gms/auth/be/change/b;
    :try_end_4
    .catch Lcom/google/android/gms/auth/q; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 87
    :catch_1
    move-exception v0

    .line 88
    :try_start_5
    sget-object v1, Lcom/google/android/gms/auth/be/change/LoginAccountsChangedIntentService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Unable to get AccountStateFactory."

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_0
.end method
