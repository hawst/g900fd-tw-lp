.class public final enum Lcom/google/android/gms/drive/ui/o;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/o;

.field private static final synthetic b:[Lcom/google/android/gms/drive/ui/o;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/drive/ui/o;

    const-string v1, "INSTANCE"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/ui/o;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/o;->a:Lcom/google/android/gms/drive/ui/o;

    .line 34
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/o;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/ui/o;->a:Lcom/google/android/gms/drive/ui/o;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/ui/o;->b:[Lcom/google/android/gms/drive/ui/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/o;
    .locals 1

    .prologue
    .line 34
    const-class v0, Lcom/google/android/gms/drive/ui/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/o;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/o;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/drive/ui/o;->b:[Lcom/google/android/gms/drive/ui/o;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/o;

    return-object v0
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0x54

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
