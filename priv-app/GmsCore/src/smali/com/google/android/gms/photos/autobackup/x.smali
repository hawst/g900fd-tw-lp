.class public final Lcom/google/android/gms/photos/autobackup/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private a:Lcom/google/android/gms/photos/autobackup/a/ac;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/x;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/x;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILjava/util/List;)V

    .line 72
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/photos/autobackup/x;->a(Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v3, 0x0

    .line 37
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 38
    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {p1, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/autobackup/o;

    .line 40
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/ap;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/ap;

    move-result-object v5

    .line 41
    invoke-interface {v5}, Lcom/google/android/libraries/social/autobackup/ao;->a()V

    .line 43
    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/o;->c()Z

    move-result v6

    .line 44
    if-eqz v6, :cond_0

    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v1

    .line 47
    :goto_0
    if-eq v1, v4, :cond_1

    .line 48
    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 53
    :goto_1
    new-instance v8, Lcom/google/android/gms/photos/autobackup/model/a;

    invoke-direct {v8, v0}, Lcom/google/android/gms/photos/autobackup/model/a;-><init>(Ljava/lang/String;)V

    iput-boolean v6, v8, Lcom/google/android/gms/photos/autobackup/model/a;->a:Z

    invoke-interface {v5}, Lcom/google/android/libraries/social/autobackup/ao;->j()Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/photos/autobackup/model/a;->f:Z

    invoke-interface {v5}, Lcom/google/android/libraries/social/autobackup/ao;->b()Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/photos/autobackup/model/a;->b:Z

    invoke-interface {v5}, Lcom/google/android/libraries/social/autobackup/ao;->d()Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/photos/autobackup/model/a;->c:Z

    invoke-interface {v5}, Lcom/google/android/libraries/social/autobackup/ao;->e()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    :goto_2
    iput-boolean v0, v8, Lcom/google/android/gms/photos/autobackup/model/a;->d:Z

    invoke-interface {v5}, Lcom/google/android/libraries/social/autobackup/ao;->c()Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/android/gms/photos/autobackup/model/a;->e:Z

    invoke-interface {v5}, Lcom/google/android/libraries/social/autobackup/ao;->l()Lcom/google/android/libraries/social/mediaupload/w;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    iget-wide v2, v0, Lcom/google/android/libraries/social/mediaupload/w;->a:J

    iget-wide v4, v0, Lcom/google/android/libraries/social/mediaupload/w;->b:J

    iget-boolean v6, v0, Lcom/google/android/libraries/social/mediaupload/w;->c:Z

    iget-boolean v7, v0, Lcom/google/android/libraries/social/mediaupload/w;->d:Z

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/photos/autobackup/model/UserQuota;-><init>(JJZZ)V

    iput-object v1, v8, Lcom/google/android/gms/photos/autobackup/model/a;->g:Lcom/google/android/gms/photos/autobackup/model/UserQuota;

    invoke-virtual {v8}, Lcom/google/android/gms/photos/autobackup/model/a;->a()Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    move-result-object v0

    .line 63
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/x;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILjava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_3
    return-void

    :cond_0
    move v1, v4

    .line 44
    goto :goto_0

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 53
    goto :goto_2

    .line 65
    :catch_0
    move-exception v0

    const-string v0, "GetSettingsOp"

    const-string v1, "Failed trying to deliver"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3
.end method
