.class public final Lcom/google/android/gms/plus/apps/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static a:Lcom/google/android/gms/plus/apps/as;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/util/HashSet;

.field private final e:Lcom/google/android/gms/common/server/n;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/graphics/BitmapFactory$Options;

.field private final h:Landroid/content/res/Resources;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/as;->b:Landroid/os/Handler;

    .line 55
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/as;->c:Ljava/util/concurrent/ExecutorService;

    .line 56
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/as;->d:Ljava/util/HashSet;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/as;->f:Landroid/content/Context;

    .line 58
    new-instance v0, Lcom/google/android/gms/common/server/n;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/as;->f:Landroid/content/Context;

    const/4 v5, 0x0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/as;->e:Lcom/google/android/gms/common/server/n;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/as;->h:Landroid/content/res/Resources;

    .line 62
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/as;->g:Landroid/graphics/BitmapFactory$Options;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->g:Landroid/graphics/BitmapFactory$Options;

    const/16 v1, 0x140

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->g:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/as;->h:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->g:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/as;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->f:Landroid/content/Context;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/as;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/android/gms/plus/apps/as;->a:Lcom/google/android/gms/plus/apps/as;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lcom/google/android/gms/plus/apps/as;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/apps/as;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/plus/apps/as;->a:Lcom/google/android/gms/plus/apps/as;

    .line 76
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/as;->a:Lcom/google/android/gms/plus/apps/as;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/apps/as;)Lcom/google/android/gms/common/server/n;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->e:Lcom/google/android/gms/common/server/n;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/apps/as;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/apps/as;)Landroid/graphics/BitmapFactory$Options;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->g:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/apps/au;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 145
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/plus/apps/at;

    invoke-static {p1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v2

    invoke-direct {v1, p0, v2, p2}, Lcom/google/android/gms/plus/apps/at;-><init>(Lcom/google/android/gms/plus/apps/as;Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 81
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/apps/au;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/as;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 149
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 122
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 139
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 124
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/plus/apps/at;

    .line 125
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/as;->h:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/at;->a(Lcom/google/android/gms/plus/apps/at;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/as;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/au;

    .line 127
    invoke-static {v0}, Lcom/google/android/gms/plus/apps/at;->b(Lcom/google/android/gms/plus/apps/at;)Lcom/google/android/gms/plus/model/a/a;

    move-result-object v5

    invoke-interface {v1, v5, v3}, Lcom/google/android/gms/plus/apps/au;->a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 129
    goto :goto_0

    .line 132
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/plus/apps/at;

    .line 133
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/as;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/au;

    .line 134
    invoke-static {v0}, Lcom/google/android/gms/plus/apps/at;->b(Lcom/google/android/gms/plus/apps/at;)Lcom/google/android/gms/plus/model/a/a;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Lcom/google/android/gms/plus/apps/au;->a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_1
    move v0, v2

    .line 136
    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
