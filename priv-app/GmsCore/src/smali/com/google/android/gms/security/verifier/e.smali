.class final Lcom/google/android/gms/security/verifier/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/x;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/gms/security/verifier/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/security/verifier/a;Z)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/gms/security/verifier/e;->b:Lcom/google/android/gms/security/verifier/a;

    iput-boolean p2, p0, Lcom/google/android/gms/security/verifier/e;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 215
    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/e;->b:Lcom/google/android/gms/security/verifier/a;

    iget-object v0, v0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/e;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/e;->b:Lcom/google/android/gms/security/verifier/a;

    iget-object v0, v0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/e;->b:Lcom/google/android/gms/security/verifier/a;

    invoke-static {v0}, Lcom/google/android/gms/security/verifier/a;->a(Lcom/google/android/gms/security/verifier/a;)V

    goto :goto_0

    :cond_2
    const-string v0, "Unexpected result from uploading chunk: %s"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected result from uploading chunk: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
