.class public final Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/gencode/authzen/server/api/an;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ao;

.field private static final h:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ao;

    invoke-direct {v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ao;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ao;

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 59
    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    const-string v1, "applicationId"

    const-string v2, "applicationId"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    const-string v1, "challenge"

    const-string v2, "challenge"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    const-string v1, "challengeSessionId"

    const-string v2, "challengeSessionId"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    const-string v1, "keyHandle"

    const-string v2, "keyHandle"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 114
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->b:I

    .line 115
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a:Ljava/util/Set;

    .line 116
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 128
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a:Ljava/util/Set;

    .line 129
    iput p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->b:I

    .line 130
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->c:Ljava/lang/String;

    .line 131
    iput-object p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->d:Ljava/lang/String;

    .line 132
    iput-object p5, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->e:Ljava/lang/String;

    .line 133
    iput-object p6, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->f:Ljava/lang/String;

    .line 134
    iput-object p7, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->g:Ljava/lang/String;

    .line 135
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 292
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 293
    packed-switch v0, :pswitch_data_0

    .line 310
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 295
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->c:Ljava/lang/String;

    .line 313
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 314
    return-void

    .line 298
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->d:Ljava/lang/String;

    goto :goto_0

    .line 301
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 304
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->f:Ljava/lang/String;

    goto :goto_0

    .line 307
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->g:Ljava/lang/String;

    goto :goto_0

    .line 293
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 260
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 272
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 262
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->c:Ljava/lang/String;

    .line 270
    :goto_0
    return-object v0

    .line 264
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->d:Ljava/lang/String;

    goto :goto_0

    .line 266
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 268
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->f:Ljava/lang/String;

    goto :goto_0

    .line 270
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->g:Ljava/lang/String;

    goto :goto_0

    .line 260
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 22
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 245
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ao;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 342
    instance-of v0, p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 373
    :goto_0
    return v0

    .line 347
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 348
    goto :goto_0

    .line 351
    :cond_1
    check-cast p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;

    .line 352
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 353
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 354
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 356
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 358
    goto :goto_0

    :cond_3
    move v0, v1

    .line 363
    goto :goto_0

    .line 366
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 368
    goto :goto_0

    :cond_5
    move v0, v2

    .line 373
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 329
    const/4 v0, 0x0

    .line 330
    sget-object v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 331
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 332
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 333
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 335
    goto :goto_0

    .line 336
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 250
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ao;

    invoke-static {p0, p1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ao;->a(Lcom/google/android/gms/auth/gencode/authzen/server/api/LoginChallengeInfoEntity;Landroid/os/Parcel;)V

    .line 251
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    return v0
.end method
