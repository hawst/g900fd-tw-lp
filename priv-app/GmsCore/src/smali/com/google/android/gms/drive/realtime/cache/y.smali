.class final Lcom/google/android/gms/drive/realtime/cache/y;
.super Lcom/google/android/gms/drive/realtime/cache/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/drive/realtime/cache/i;

.field private final c:Lcom/google/c/a/a/b/a/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/c/a/a/b/a/g;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/a;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;)V

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/y;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 32
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/y;->c:Lcom/google/c/a/a/b/a/g;

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 5

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a()Lcom/google/android/gms/drive/realtime/cache/a/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/y;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/y;->c:Lcom/google/c/a/a/b/a/g;

    iget-object v2, v2, Lcom/google/c/a/a/b/a/g;->a:Lcom/google/c/b/a/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/drive/realtime/cache/a/c;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V

    .line 40
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a()Lcom/google/android/gms/drive/realtime/cache/a/e;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/y;->b:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/y;->c:Lcom/google/c/a/a/b/a/g;

    iget-object v0, v0, Lcom/google/c/a/a/b/a/g;->b:Ljava/util/List;

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/e;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/realtime/cache/i;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1, v3}, Lcom/google/android/gms/drive/realtime/cache/a/e;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/List;)V

    .line 43
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/y;->c:Lcom/google/c/a/a/b/a/g;

    iget v1, v1, Lcom/google/c/a/a/b/a/g;->c:I

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 44
    return-void
.end method
