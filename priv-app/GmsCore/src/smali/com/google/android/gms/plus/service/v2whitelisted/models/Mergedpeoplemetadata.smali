.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/g;

.field private static final l:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Z

.field h:Z

.field i:Z

.field j:Ljava/lang/String;

.field k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/g;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/g;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 91
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "affinities"

    const-string v2, "affinities"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata$Affinities;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "container"

    const-string v2, "container"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "containerContactId"

    const-string v2, "containerContactId"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "containerId"

    const-string v2, "containerId"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "edgeKey"

    const-string v2, "edgeKey"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "primary"

    const-string v2, "primary"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "verified"

    const-string v2, "verified"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "visibility"

    const-string v2, "visibility"

    const/16 v3, 0x9

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    const-string v1, "writeable"

    const-string v2, "writeable"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 101
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 186
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 187
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->b:I

    .line 188
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a:Ljava/util/Set;

    .line 189
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;Z)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 205
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a:Ljava/util/Set;

    .line 206
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->b:I

    .line 207
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c:Ljava/util/List;

    .line 208
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d:Ljava/lang/String;

    .line 209
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->e:Ljava/lang/String;

    .line 210
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->f:Ljava/lang/String;

    .line 211
    iput-boolean p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->g:Z

    .line 212
    iput-boolean p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->h:Z

    .line 213
    iput-boolean p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->i:Z

    .line 214
    iput-object p10, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->j:Ljava/lang/String;

    .line 215
    iput-boolean p11, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->k:Z

    .line 216
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 751
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 752
    packed-switch v0, :pswitch_data_0

    .line 766
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 754
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d:Ljava/lang/String;

    .line 769
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 770
    return-void

    .line 757
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->e:Ljava/lang/String;

    goto :goto_0

    .line 760
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->f:Ljava/lang/String;

    goto :goto_0

    .line 763
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->j:Ljava/lang/String;

    goto :goto_0

    .line 752
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 775
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 776
    packed-switch v0, :pswitch_data_0

    .line 781
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 778
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c:Ljava/util/List;

    .line 785
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 786
    return-void

    .line 776
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 727
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 728
    packed-switch v0, :pswitch_data_0

    .line 742
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 730
    :pswitch_1
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->g:Z

    .line 745
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 746
    return-void

    .line 733
    :pswitch_2
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->h:Z

    goto :goto_0

    .line 736
    :pswitch_3
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->i:Z

    goto :goto_0

    .line 739
    :pswitch_4
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->k:Z

    goto :goto_0

    .line 728
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 682
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 687
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 707
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 689
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c:Ljava/util/List;

    .line 705
    :goto_0
    return-object v0

    .line 691
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d:Ljava/lang/String;

    goto :goto_0

    .line 693
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->e:Ljava/lang/String;

    goto :goto_0

    .line 695
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->f:Ljava/lang/String;

    goto :goto_0

    .line 697
    :pswitch_4
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->g:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 699
    :pswitch_5
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->h:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 701
    :pswitch_6
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->i:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 703
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->j:Ljava/lang/String;

    goto :goto_0

    .line 705
    :pswitch_8
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 687
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->c:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 672
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/g;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->g:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 803
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    if-nez v0, :cond_0

    move v0, v1

    .line 834
    :goto_0
    return v0

    .line 808
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 809
    goto :goto_0

    .line 812
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 813
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 814
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 815
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 817
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 819
    goto :goto_0

    :cond_3
    move v0, v1

    .line 824
    goto :goto_0

    .line 827
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 829
    goto :goto_0

    :cond_5
    move v0, v2

    .line 834
    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 334
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->h:Z

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->i:Z

    return v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 790
    const/4 v0, 0x0

    .line 791
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->l:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 792
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 793
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 794
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 796
    goto :goto_0

    .line 797
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 677
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/g;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/g;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Landroid/os/Parcel;)V

    .line 678
    return-void
.end method
