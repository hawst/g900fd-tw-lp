.class public final Lcom/google/android/gms/udc/e/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/udc/e/p;

.field public apiHeader:Lcom/google/android/gms/udc/e/b;

.field public b:Lcom/google/android/gms/udc/e/j;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 140
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 141
    iput-object v1, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-static {}, Lcom/google/android/gms/udc/e/p;->a()[Lcom/google/android/gms/udc/e/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/i;->cachedSize:I

    .line 142
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 222
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 224
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 225
    iget-object v2, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    aget-object v2, v2, v0

    .line 226
    if-eqz v2, :cond_0

    .line 227
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 224
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-eqz v0, :cond_2

    .line 233
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 236
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    if-eqz v0, :cond_3

    .line 237
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 240
    :cond_3
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 155
    if-ne p1, p0, :cond_1

    .line 156
    const/4 v0, 0x1

    .line 184
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/i;

    if-eqz v1, :cond_0

    .line 161
    check-cast p1, Lcom/google/android/gms/udc/e/i;

    .line 162
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v1, :cond_4

    .line 163
    iget-object v1, p1, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v1, :cond_0

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    if-nez v1, :cond_5

    .line 176
    iget-object v1, p1, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    if-nez v1, :cond_0

    .line 184
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/i;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 167
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 180
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 192
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 194
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 196
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/i;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 197
    return v0

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 194
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/e/j;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/udc/e/p;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/udc/e/p;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/udc/e/p;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/udc/e/b;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/udc/e/j;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 204
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->a:[Lcom/google/android/gms/udc/e/p;

    aget-object v1, v1, v0

    .line 206
    if-eqz v1, :cond_0

    .line 207
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 204
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-eqz v0, :cond_2

    .line 212
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    if-eqz v0, :cond_3

    .line 215
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/udc/e/i;->b:Lcom/google/android/gms/udc/e/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 217
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 218
    return-void
.end method
