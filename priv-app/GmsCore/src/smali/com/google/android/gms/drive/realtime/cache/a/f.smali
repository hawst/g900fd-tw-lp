.class public final enum Lcom/google/android/gms/drive/realtime/cache/a/f;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/realtime/cache/a/f;

.field public static final enum b:Lcom/google/android/gms/drive/realtime/cache/a/f;

.field private static final synthetic d:[Lcom/google/android/gms/drive/realtime/cache/a/f;


# instance fields
.field private final c:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/f;

    const-string v1, "MUTATION"

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->d()Lcom/google/android/gms/drive/realtime/cache/a/e;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "mutation"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/realtime/cache/a/f;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/f;->a:Lcom/google/android/gms/drive/realtime/cache/a/f;

    .line 63
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/a/f;

    const-string v1, "SENT"

    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/e;->d()Lcom/google/android/gms/drive/realtime/cache/a/e;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "sent"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v6, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/realtime/cache/a/f;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/f;->b:Lcom/google/android/gms/drive/realtime/cache/a/f;

    .line 52
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/realtime/cache/a/f;

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/f;->a:Lcom/google/android/gms/drive/realtime/cache/a/f;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/realtime/cache/a/f;->b:Lcom/google/android/gms/drive/realtime/cache/a/f;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/a/f;->d:[Lcom/google/android/gms/drive/realtime/cache/a/f;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 70
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/a/f;->c:Lcom/google/android/gms/drive/database/model/ab;

    .line 71
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/realtime/cache/a/f;
    .locals 1

    .prologue
    .line 52
    const-class v0, Lcom/google/android/gms/drive/realtime/cache/a/f;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/cache/a/f;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/realtime/cache/a/f;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/a/f;->d:[Lcom/google/android/gms/drive/realtime/cache/a/f;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/realtime/cache/a/f;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/realtime/cache/a/f;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/a/f;->c:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
