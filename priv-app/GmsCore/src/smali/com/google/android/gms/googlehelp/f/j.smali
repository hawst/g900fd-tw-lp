.class public final Lcom/google/android/gms/googlehelp/f/j;
.super Lcom/google/android/gms/googlehelp/f/a;
.source "SourceFile"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/googlehelp/f/k;

.field private final e:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private final f:Lcom/google/android/gms/googlehelp/d/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Lcom/google/android/gms/googlehelp/f/k;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/f/a;-><init>(Lcom/google/android/gms/googlehelp/f/n;)V

    .line 48
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/j;->b:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/f/j;->c:Ljava/lang/String;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/f/j;->d:Lcom/google/android/gms/googlehelp/f/k;

    .line 51
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/j;->e:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 52
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d()Lcom/google/android/gms/googlehelp/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/j;->f:Lcom/google/android/gms/googlehelp/d/b;

    .line 53
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/j;->d:Lcom/google/android/gms/googlehelp/f/k;

    invoke-interface {v0, p0}, Lcom/google/android/gms/googlehelp/f/k;->a(Lcom/google/android/gms/googlehelp/f/a;)V

    .line 58
    return-void
.end method

.method protected final bridge synthetic a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 22
    check-cast p1, Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/j;->d:Lcom/google/android/gms/googlehelp/f/k;

    invoke-interface {v0, p1}, Lcom/google/android/gms/googlehelp/f/k;->a(Ljava/util/List;)V

    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 22
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/j;->f:Lcom/google/android/gms/googlehelp/d/b;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/j;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/j;->c:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/gms/googlehelp/a/n;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "SUGGESTION_CLICKED"

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/googlehelp/d/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/j;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/f/j;->e:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    const-string v4, "locale"

    const-string v5, ""

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gms/googlehelp/common/i;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/j;->f:Lcom/google/android/gms/googlehelp/d/b;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/j;->b:Landroid/content/Context;

    const-string v3, "recent_articles:"

    const-string v4, "RECENT_ARTICLE_CLICKED"

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/googlehelp/d/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v0

    :goto_0
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/gms/googlehelp/common/t;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/j;->f:Lcom/google/android/gms/googlehelp/d/b;

    const-string v4, "recent_articles:"

    invoke-virtual {v2, v4}, Lcom/google/android/gms/googlehelp/d/b;->b(Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/j;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/f/j;->e:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    const-string v5, "locale"

    new-instance v6, Lcom/google/android/gms/googlehelp/common/g;

    invoke-direct {v6, v2, v4}, Lcom/google/android/gms/googlehelp/common/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-virtual {v6}, Lcom/google/android/gms/googlehelp/common/g;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v2

    invoke-virtual {v2, v5, v3}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method
