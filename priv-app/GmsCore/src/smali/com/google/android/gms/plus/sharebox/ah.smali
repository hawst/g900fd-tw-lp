.class public final Lcom/google/android/gms/plus/sharebox/ah;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/ai;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static b:J


# instance fields
.field private c:Lcom/google/android/gms/plus/internal/ad;

.field private d:Lcom/google/android/gms/plus/internal/ab;

.field private e:Lcom/google/android/gms/plus/sharebox/aj;

.field private f:Z

.field private g:Lcom/google/android/gms/common/api/v;

.field private h:Lcom/google/android/gms/plus/sharebox/ai;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Lcom/google/android/gms/plus/model/posts/Comment;

.field private l:Lcom/google/android/gms/plus/sharebox/ak;

.field private m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.settings"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://www.googleapis.com/auth/plus.pages.manage"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/sharebox/ah;->a:[Ljava/lang/String;

    .line 46
    sget-object v0, Lcom/google/android/gms/plus/c/a;->af:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/plus/sharebox/ah;->b:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 254
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->c:Lcom/google/android/gms/plus/internal/ad;

    .line 255
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/ah;
    .locals 3

    .prologue
    .line 240
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "specified_account_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/plus/sharebox/ah;

    invoke-direct {v2}, Lcom/google/android/gms/plus/sharebox/ah;-><init>()V

    iput-object v0, v2, Lcom/google/android/gms/plus/sharebox/ah;->c:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/sharebox/ah;->setArguments(Landroid/os/Bundle;)V

    return-object v2
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/ah;)Lcom/google/android/gms/plus/sharebox/ak;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/ah;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/ah;->i:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/ah;)Lcom/google/android/gms/plus/internal/ab;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    return-object v0
.end method

.method static synthetic c()J
    .locals 2

    .prologue
    .line 34
    sget-wide v0, Lcom/google/android/gms/plus/sharebox/ah;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/sharebox/ah;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/sharebox/ah;)Z
    .locals 1

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->j:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/sharebox/ah;)Lcom/google/android/gms/plus/model/posts/Comment;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->k:Lcom/google/android/gms/plus/model/posts/Comment;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/sharebox/ah;)Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->f:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/sharebox/ah;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->i:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 298
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->j:Z

    if-eqz v0, :cond_0

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/sharebox/ak;->b(Lcom/google/android/gms/common/c;)V

    .line 303
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->j:Z

    .line 304
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 6

    .prologue
    .line 311
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ah;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 312
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->i:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->m:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 318
    :cond_0
    :goto_0
    return-void

    .line 315
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/ak;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/common/analytics/v;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/ah;->m:Ljava/lang/String;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 2

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->j:Z

    if-eqz v0, :cond_0

    .line 285
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "One comment at a time please"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 287
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->j:Z

    .line 288
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/ah;->k:Lcom/google/android/gms/plus/model/posts/Comment;

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->e:Lcom/google/android/gms/plus/sharebox/aj;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aj;->T_()V

    .line 294
    :cond_1
    :goto_0
    return-void

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 276
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->f:Z

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 163
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ah;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    .line 166
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ak;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->m:Ljava/lang/String;

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ak;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 175
    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ah;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "specified_account_name"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 177
    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/ah;->m:Ljava/lang/String;

    invoke-static {v2, v3, v4, v0}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    new-instance v3, Lcom/google/android/gms/plus/internal/cn;

    invoke-direct {v3, v2}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/ah;->m:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ak;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/plus/internal/cn;->e:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/plus/sharebox/ah;->a:[Ljava/lang/String;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v0

    .line 186
    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/ak;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 188
    new-array v3, v1, [Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    .line 191
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    if-nez v3, :cond_3

    .line 192
    new-instance v3, Lcom/google/android/gms/plus/sharebox/aj;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/plus/sharebox/aj;-><init>(Lcom/google/android/gms/plus/sharebox/ah;B)V

    iput-object v3, p0, Lcom/google/android/gms/plus/sharebox/ah;->e:Lcom/google/android/gms/plus/sharebox/aj;

    .line 194
    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ah;->c:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/ah;->e:Lcom/google/android/gms/plus/sharebox/aj;

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/ah;->e:Lcom/google/android/gms/plus/sharebox/aj;

    invoke-interface {v3, v4, v0, v5, v6}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 204
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ak;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 209
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ak;->a()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 215
    :goto_1
    new-instance v3, Lcom/google/android/gms/plus/sharebox/ai;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/plus/sharebox/ai;-><init>(Lcom/google/android/gms/plus/sharebox/ah;B)V

    iput-object v3, p0, Lcom/google/android/gms/plus/sharebox/ah;->h:Lcom/google/android/gms/plus/sharebox/ai;

    .line 216
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->c:Lcom/google/android/gms/plus/internal/ad;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ah;->m:Ljava/lang/String;

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->h:Lcom/google/android/gms/plus/sharebox/ai;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->h:Lcom/google/android/gms/plus/sharebox/ai;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 140
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 141
    instance-of v0, p1, Lcom/google/android/gms/plus/sharebox/ak;

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/ak;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/sharebox/ak;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    .line 146
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 156
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 157
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ah;->setRetainInstance(Z)V

    .line 159
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 225
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 229
    :cond_1
    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->d:Lcom/google/android/gms/plus/internal/ab;

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 232
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 234
    :cond_3
    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->g:Lcom/google/android/gms/common/api/v;

    .line 236
    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/ah;->i:Ljava/lang/String;

    .line 237
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 150
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ah;->l:Lcom/google/android/gms/plus/sharebox/ak;

    .line 152
    return-void
.end method
