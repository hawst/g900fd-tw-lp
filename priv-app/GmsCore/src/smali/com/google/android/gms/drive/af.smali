.class public final enum Lcom/google/android/gms/drive/af;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/af;

.field public static final enum b:Lcom/google/android/gms/drive/af;

.field public static final enum c:Lcom/google/android/gms/drive/af;

.field public static final enum d:Lcom/google/android/gms/drive/af;

.field public static final enum e:Lcom/google/android/gms/drive/af;

.field public static final enum f:Lcom/google/android/gms/drive/af;

.field private static final synthetic g:[Lcom/google/android/gms/drive/af;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 17
    new-instance v0, Lcom/google/android/gms/drive/af;

    const-string v1, "BINARY_DIFF_UPLOAD"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/af;->a:Lcom/google/android/gms/drive/af;

    .line 22
    new-instance v0, Lcom/google/android/gms/drive/af;

    const-string v1, "PARANOID_CHECKS"

    invoke-direct {v0, v1, v4}, Lcom/google/android/gms/drive/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/af;->b:Lcom/google/android/gms/drive/af;

    .line 27
    new-instance v0, Lcom/google/android/gms/drive/af;

    const-string v1, "SYNC_MORE"

    invoke-direct {v0, v1, v5}, Lcom/google/android/gms/drive/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/af;->c:Lcom/google/android/gms/drive/af;

    .line 32
    new-instance v0, Lcom/google/android/gms/drive/af;

    const-string v1, "_TEST1"

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/af;->d:Lcom/google/android/gms/drive/af;

    .line 37
    new-instance v0, Lcom/google/android/gms/drive/af;

    const-string v1, "_TEST2"

    invoke-direct {v0, v1, v7}, Lcom/google/android/gms/drive/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/af;->e:Lcom/google/android/gms/drive/af;

    .line 42
    new-instance v0, Lcom/google/android/gms/drive/af;

    const-string v1, "TRASH_FOLDERS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/af;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/af;->f:Lcom/google/android/gms/drive/af;

    .line 13
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/gms/drive/af;

    sget-object v1, Lcom/google/android/gms/drive/af;->a:Lcom/google/android/gms/drive/af;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/af;->b:Lcom/google/android/gms/drive/af;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/af;->c:Lcom/google/android/gms/drive/af;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/drive/af;->d:Lcom/google/android/gms/drive/af;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/drive/af;->e:Lcom/google/android/gms/drive/af;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/af;->f:Lcom/google/android/gms/drive/af;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/af;->g:[Lcom/google/android/gms/drive/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method protected static a()Z
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x1

    return v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/af;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/android/gms/drive/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/af;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/af;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/gms/drive/af;->g:[Lcom/google/android/gms/drive/af;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/af;

    return-object v0
.end method
