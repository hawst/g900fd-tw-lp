.class public final Lcom/google/android/gms/feedback/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    const-string v0, "gms:feedback:log_line_limit"

    const/16 v1, 0x3e8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 22
    const-string v0, "gms:feedback:ctl_message_popup"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 29
    const-string v0, "gms:feedback:ctl_whitelist_package_names"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 40
    const-string v0, "gms:feedback:ctl_whitelist_report_types"

    const-string v1, "11"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->d:Lcom/google/android/gms/common/a/d;

    .line 46
    const-string v0, "gms:feedback:enable_suggestions"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->e:Lcom/google/android/gms/common/a/d;

    .line 53
    const-string v0, "gms:feedback:suggestion_whitelist_package_names"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->f:Lcom/google/android/gms/common/a/d;

    .line 64
    const-string v0, "gms:feedback:whitelist_report_types_for_support"

    const-string v1, "11"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->g:Lcom/google/android/gms/common/a/d;

    .line 70
    const-string v0, "gms:feedback:bitmap_compression_ratio"

    const/16 v1, 0x46

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->h:Lcom/google/android/gms/common/a/d;

    .line 77
    const-string v0, "gms:feedback:save_screenshot_timeout_millis"

    const/16 v1, 0x1f4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->i:Lcom/google/android/gms/common/a/d;

    .line 83
    const-string v0, "gms:feedback:serve_suggestion_timeout_millis"

    const/16 v1, 0x1388

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/feedback/a/a;->j:Lcom/google/android/gms/common/a/d;

    return-void
.end method
