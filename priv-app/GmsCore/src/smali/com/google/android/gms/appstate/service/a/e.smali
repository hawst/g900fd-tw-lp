.class public final Lcom/google/android/gms/appstate/service/a/e;
.super Lcom/google/android/gms/appstate/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/appstate/c/h;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 26
    const-string v0, "LoadStateOp"

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/appstate/service/a/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/appstate/service/a/e;->b:Lcom/google/android/gms/appstate/c/h;

    .line 28
    iput-object p3, p0, Lcom/google/android/gms/appstate/service/a/e;->c:Ljava/lang/String;

    .line 29
    iput p4, p0, Lcom/google/android/gms/appstate/service/a/e;->d:I

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/e;->b:Lcom/google/android/gms/appstate/c/h;

    iget v1, p0, Lcom/google/android/gms/appstate/service/a/e;->d:I

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/appstate/c/h;->a(ILcom/google/android/gms/common/data/DataHolder;)V

    .line 41
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/appstate/a/e;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/e;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a/e;->c:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/appstate/service/a/e;->d:I

    invoke-virtual {p2, p1, v0, v1, v2}, Lcom/google/android/gms/appstate/a/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
