.class public final Lcom/google/android/gms/security/verifier/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2195
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2196
    iput-object v0, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/w;->cachedSize:I

    .line 2197
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2256
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2257
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2258
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2261
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2262
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2265
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2208
    if-ne p1, p0, :cond_1

    .line 2229
    :cond_0
    :goto_0
    return v0

    .line 2211
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 2212
    goto :goto_0

    .line 2214
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/w;

    .line 2215
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2216
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2217
    goto :goto_0

    .line 2219
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2220
    goto :goto_0

    .line 2222
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2223
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2224
    goto :goto_0

    .line 2226
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2227
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2234
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2237
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2239
    return v0

    .line 2234
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2237
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2172
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2245
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2246
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/w;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2248
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2249
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/w;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2251
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2252
    return-void
.end method
