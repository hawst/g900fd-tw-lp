.class public final Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 60
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 83
    sget v0, Lcom/google/android/gms/l;->ee:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 116
    sget v0, Lcom/google/android/gms/p;->uP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    .line 118
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 93
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    sget-object v0, Lcom/google/android/gms/plus/c/a;->S:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;-><init>()V

    iput-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->b:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->d:Ljava/util/Set;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    :goto_0
    return-object v0

    .line 103
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    return v0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 72
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;

    if-nez v0, :cond_0

    .line 73
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 79
    :goto_0
    return-void

    .line 76
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;

    .line 77
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/d;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 65
    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;->d:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday$SavedState;->a:Ljava/lang/String;

    .line 67
    return-object v1
.end method
