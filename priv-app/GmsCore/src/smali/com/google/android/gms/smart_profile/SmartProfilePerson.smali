.class public Lcom/google/android/gms/smart_profile/SmartProfilePerson;
.super Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
.source "SourceFile"


# instance fields
.field private J:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;-><init>()V

    .line 21
    return-void
.end method


# virtual methods
.method public final K()Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->J:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->J:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final L()Ljava/util/List;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->J:Ljava/util/List;

    return-object v0
.end method

.method public final a(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->J:Ljava/util/List;

    .line 69
    return-void
.end method
