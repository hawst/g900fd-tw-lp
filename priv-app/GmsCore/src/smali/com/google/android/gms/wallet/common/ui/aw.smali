.class public final Lcom/google/android/gms/wallet/common/ui/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/am;
.implements Lcom/google/android/gms/wallet/common/ui/dn;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private final c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

.field private final d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;I)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->a:Landroid/content/Context;

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 34
    iput p3, p0, Lcom/google/android/gms/wallet/common/ui/aw;->d:I

    .line 35
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aw;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->a:Landroid/content/Context;

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 27
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/ui/aw;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    .line 28
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/aw;->d:I

    .line 29
    return-void
.end method

.method private a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 46
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move p1, v0

    .line 61
    :goto_0
    :pswitch_0
    return p1

    .line 50
    :cond_0
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->d:I

    .line 51
    :goto_1
    packed-switch v1, :pswitch_data_0

    move p1, v0

    .line 61
    goto :goto_0

    .line 50
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->c:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v1

    goto :goto_1

    :cond_2
    move v1, v0

    goto :goto_1

    .line 57
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a(I)I

    move-result v1

    if-ne v2, v1, :cond_3

    const/4 p1, 0x1

    goto :goto_0

    :cond_3
    move p1, v0

    goto :goto_0

    .line 51
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/aw;->a(Z)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d()I

    move-result v2

    if-ne v1, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public final e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/aw;->a(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 83
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    move-result v2

    if-nez v2, :cond_0

    .line 96
    :goto_0
    return v0

    .line 87
    :cond_0
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/aw;->a(Z)Z

    move-result v2

    if-nez v2, :cond_1

    .line 88
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/aw;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->Bq:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 93
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/aw;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    :cond_2
    move v0, v1

    .line 96
    goto :goto_0
.end method
