.class public Lcom/google/android/gms/wallet/common/DisplayHints;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private final c:Lcom/google/checkout/inapp/proto/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/wallet/common/f;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/DisplayHints;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/checkout/inapp/proto/d;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->a:Ljava/lang/String;

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Ljava/lang/String;

    .line 24
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->c:Lcom/google/checkout/inapp/proto/d;

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Ljava/lang/String;

    .line 41
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Lcom/google/checkout/inapp/proto/d;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->c:Lcom/google/checkout/inapp/proto/d;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->c:Lcom/google/checkout/inapp/proto/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->c:Lcom/google/checkout/inapp/proto/d;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/DisplayHints;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 53
    return-void

    .line 51
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
