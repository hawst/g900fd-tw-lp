.class public final Lcom/google/android/gms/googlehelp/c/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private final b:Landroid/view/MenuItem;


# direct methods
.method public constructor <init>(Ljava/util/List;Landroid/view/MenuItem;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/ac;->a:Ljava/util/List;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/ac;->b:Landroid/view/MenuItem;

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/ac;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/c;

    .line 40
    invoke-interface {v0, p0}, Lcom/google/android/gms/googlehelp/c/c;->a(Lcom/google/android/gms/googlehelp/c/ac;)V

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 49
    const/4 v1, 0x1

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/ac;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/c;

    .line 51
    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/c;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/c;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 52
    const/4 v0, 0x0

    .line 56
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/ac;->b:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 57
    return-void

    :cond_1
    move v0, v1

    goto :goto_0
.end method
