.class public Lcom/google/android/gms/kids/account/KidsAccountSetupService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/kids/account/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    const-string v0, "KidsAccountSetupService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 30
    invoke-static {p0}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->a:Lcom/google/android/gms/kids/account/b;

    .line 31
    return-void
.end method

.method public static a()V
    .locals 4

    .prologue
    .line 56
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->c()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->send()V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :goto_0
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 58
    const-string v1, "KidsAccountSetupService"

    const-string v2, "pending intent cancelled "

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method private static b()V
    .locals 8

    .prologue
    .line 63
    const-string v0, "KidsAccountSetupService"

    const-string v1, "Setting periodic alarm"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->b()Landroid/app/AlarmManager;

    move-result-object v1

    .line 65
    const/4 v2, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/kids/common/b/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-static {}, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->c()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v1, v2, v4, v5, v0}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 68
    return-void
.end method

.method private static c()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->b()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/kids/account/KidsAccountSetupService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 74
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->d()Lcom/google/android/gms/kids/common/g;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->b()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    const/high16 v3, 0x10000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 79
    return-object v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 35
    const-string v0, "KidsAccountSetupService"

    const-string v1, "onReceive. now=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 39
    :cond_1
    invoke-static {}, Lcom/google/android/gms/kids/common/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 40
    const-string v0, "KidsAccountSetupService"

    const-string v1, "Setup is marked as complete."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 43
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->a:Lcom/google/android/gms/kids/account/b;

    iget-object v0, v0, Lcom/google/android/gms/kids/account/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 44
    const-string v0, "KidsAccountSetupService"

    const-string v1, "Not connected to network"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 45
    invoke-static {}, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->b()V

    .line 47
    :cond_3
    invoke-static {p0}, Lcom/google/android/gms/kids/account/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    const-string v0, "KidsAccountSetupService"

    const-string v1, "Setup failed. Will retry after delta time"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 49
    invoke-static {}, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->b()V

    goto :goto_0
.end method
