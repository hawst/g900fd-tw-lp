.class public Lcom/google/android/gms/games/ui/cv;
.super Lcom/google/android/gms/games/ui/o;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Ljava/lang/Object;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/cv;-><init>(B)V

    .line 41
    return-void
.end method

.method private constructor <init>(B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/o;-><init>()V

    .line 31
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    .line 64
    iput-object v1, p0, Lcom/google/android/gms/games/ui/cv;->a:Landroid/view/View;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/cv;->b:Ljava/lang/Object;

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    .line 65
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    if-eq v0, p1, :cond_0

    .line 91
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/cv;->notifyDataSetChanged()V

    .line 94
    :cond_0
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 163
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/cv;->isEnabled(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 126
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cv;->b:Ljava/lang/Object;

    return-object v0

    .line 126
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 132
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 133
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 134
    const-wide/16 v0, 0x0

    return-wide v0

    .line 133
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 144
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 145
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 146
    return v1

    :cond_0
    move v0, v1

    .line 145
    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 169
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cv;->a:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x1

    return v0
.end method

.method public isEnabled(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 156
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cv;->c:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 157
    if-nez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cv;->a:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cv;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 157
    goto :goto_0

    :cond_1
    move v1, v2

    .line 158
    goto :goto_1
.end method
