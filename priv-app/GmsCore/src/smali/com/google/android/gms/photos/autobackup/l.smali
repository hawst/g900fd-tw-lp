.class public Lcom/google/android/gms/photos/autobackup/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/p/b;


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:Z

.field private final h:Lcom/google/android/libraries/social/p/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lcom/google/android/libraries/social/p/a;

    invoke-direct {v0, p0}, Lcom/google/android/libraries/social/p/a;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/libraries/social/p/c;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->a:Z

    .line 68
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->b:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->c:Z

    .line 70
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->d:Z

    .line 71
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->g()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->e:Z

    .line 72
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->f:Z

    .line 73
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->g:Z

    .line 74
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/l;->b:Ljava/lang/String;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    .line 120
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/google/android/gms/photos/autobackup/l;->a:Z

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    .line 105
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 54
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->c:Z

    .line 55
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->d:Z

    .line 56
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->e:Z

    .line 57
    iput-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->f:Z

    .line 58
    iput-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->g:Z

    .line 59
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 129
    iput-boolean p1, p0, Lcom/google/android/gms/photos/autobackup/l;->c:Z

    .line 130
    if-eqz p1, :cond_0

    .line 131
    invoke-virtual {p0, p1}, Lcom/google/android/gms/photos/autobackup/l;->c(Z)V

    .line 135
    :goto_0
    return-void

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/gms/photos/autobackup/model/a;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/l;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/photos/autobackup/model/a;-><init>(Ljava/lang/String;)V

    .line 83
    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->a:Z

    iput-boolean v1, v0, Lcom/google/android/gms/photos/autobackup/model/a;->a:Z

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->c:Z

    iput-boolean v1, v0, Lcom/google/android/gms/photos/autobackup/model/a;->b:Z

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->d:Z

    iput-boolean v1, v0, Lcom/google/android/gms/photos/autobackup/model/a;->e:Z

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->e:Z

    iput-boolean v1, v0, Lcom/google/android/gms/photos/autobackup/model/a;->f:Z

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->f:Z

    iput-boolean v1, v0, Lcom/google/android/gms/photos/autobackup/model/a;->c:Z

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/l;->g:Z

    iput-boolean v1, v0, Lcom/google/android/gms/photos/autobackup/model/a;->d:Z

    .line 89
    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/a;->a()Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    move-result-object v0

    return-object v0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/google/android/gms/photos/autobackup/l;->d:Z

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    .line 146
    return-void
.end method

.method public final d(Z)V
    .locals 1

    .prologue
    .line 152
    iput-boolean p1, p0, Lcom/google/android/gms/photos/autobackup/l;->e:Z

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    .line 154
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/l;->a:Z

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 160
    iput-boolean p1, p0, Lcom/google/android/gms/photos/autobackup/l;->f:Z

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    .line 162
    return-void
.end method

.method public final f(Z)V
    .locals 1

    .prologue
    .line 169
    iput-boolean p1, p0, Lcom/google/android/gms/photos/autobackup/l;->g:Z

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/l;->h:Lcom/google/android/libraries/social/p/c;

    invoke-interface {v0}, Lcom/google/android/libraries/social/p/c;->a()V

    .line 171
    return-void
.end method
