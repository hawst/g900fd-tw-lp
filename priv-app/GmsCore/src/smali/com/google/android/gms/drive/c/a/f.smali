.class final Lcom/google/android/gms/drive/c/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/query/internal/f;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/c/a/d;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/c/a/d;)V
    .locals 0

    .prologue
    .line 745
    iput-object p1, p0, Lcom/google/android/gms/drive/c/a/f;->a:Lcom/google/android/gms/drive/c/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/c/a/d;B)V
    .locals 0

    .prologue
    .line 745
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/c/a/f;-><init>(Lcom/google/android/gms/drive/c/a/d;)V

    return-void
.end method

.method private b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 759
    sget-object v1, Lcom/google/android/gms/drive/query/i;->a:Lcom/google/android/gms/drive/metadata/i;

    if-ne p1, v1, :cond_0

    .line 760
    const/4 v0, 0x1

    .line 794
    :goto_0
    return v0

    .line 761
    :cond_0
    sget-object v1, Lcom/google/android/gms/drive/query/i;->b:Lcom/google/android/gms/drive/metadata/i;

    if-ne p1, v1, :cond_1

    .line 762
    const/4 v0, 0x2

    goto :goto_0

    .line 763
    :cond_1
    sget-object v1, Lcom/google/android/gms/drive/query/i;->c:Lcom/google/android/gms/drive/metadata/i;

    if-ne p1, v1, :cond_2

    .line 764
    const/4 v0, 0x3

    goto :goto_0

    .line 765
    :cond_2
    sget-object v1, Lcom/google/android/gms/drive/query/i;->d:Lcom/google/android/gms/drive/metadata/h;

    if-ne p1, v1, :cond_3

    .line 766
    const/4 v0, 0x4

    goto :goto_0

    .line 767
    :cond_3
    sget-object v1, Lcom/google/android/gms/drive/query/i;->e:Lcom/google/android/gms/drive/metadata/j;

    if-ne p1, v1, :cond_4

    .line 768
    const/4 v0, 0x5

    goto :goto_0

    .line 769
    :cond_4
    sget-object v1, Lcom/google/android/gms/drive/query/i;->f:Lcom/google/android/gms/drive/metadata/i;

    if-ne p1, v1, :cond_5

    .line 770
    const/4 v0, 0x6

    goto :goto_0

    .line 771
    :cond_5
    sget-object v1, Lcom/google/android/gms/drive/query/i;->g:Lcom/google/android/gms/drive/metadata/j;

    if-ne p1, v1, :cond_6

    .line 772
    const/4 v0, 0x7

    goto :goto_0

    .line 773
    :cond_6
    sget-object v1, Lcom/google/android/gms/drive/query/i;->h:Lcom/google/android/gms/drive/metadata/j;

    if-ne p1, v1, :cond_7

    .line 774
    const/16 v0, 0x8

    goto :goto_0

    .line 775
    :cond_7
    sget-object v1, Lcom/google/android/gms/drive/query/i;->i:Lcom/google/android/gms/drive/metadata/i;

    if-ne p1, v1, :cond_8

    .line 776
    const/16 v0, 0x9

    goto :goto_0

    .line 777
    :cond_8
    sget-object v1, Lcom/google/android/gms/drive/query/i;->j:Lcom/google/android/gms/drive/metadata/i;

    if-ne p1, v1, :cond_b

    .line 778
    if-nez p2, :cond_9

    .line 782
    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/f;->a:Lcom/google/android/gms/drive/c/a/d;

    iget-object v1, v1, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v2, "ImpressionLogEvent"

    const-string v3, "FieldOnly() with CUSTOM_FILE_PROPERTIES??"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 785
    :cond_9
    check-cast p2, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    .line 786
    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v0

    if-nez v0, :cond_a

    .line 788
    const/16 v0, 0xa

    goto :goto_0

    .line 790
    :cond_a
    const/16 v0, 0xb

    goto :goto_0

    .line 793
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/f;->a:Lcom/google/android/gms/drive/c/a/d;

    iget-object v1, v1, Lcom/google/android/gms/drive/c/a/d;->b:Landroid/content/Context;

    const-string v2, "ImpressionLogEvent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "What field is this? "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 745
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 745
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/c/a/f;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 745
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/c/a/f;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 745
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/c/a/f;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 745
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gms/drive/c/a/f;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Ljava/util/List;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 745
    new-instance v1, Lcom/google/android/gms/common/util/g;

    invoke-direct {v1}, Lcom/google/android/gms/common/util/g;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .prologue
    .line 745
    check-cast p1, Ljava/util/Set;

    return-object p1
.end method
