.class final Lcom/google/android/gms/cast/media/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Lcom/google/android/gms/cast/g;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 92
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 94
    const-string v0, "android.media.metadata.DURATION"

    iget-wide v2, p0, Lcom/google/android/gms/cast/g;->e:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 96
    iget-object v2, p0, Lcom/google/android/gms/cast/g;->d:Lcom/google/android/gms/cast/i;

    .line 98
    if-nez v2, :cond_0

    move-object v0, v1

    .line 159
    :goto_0
    return-object v0

    .line 104
    :cond_0
    const-string v0, "com.google.android.gms.cast.metadata.ALBUM_ARTIST"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 105
    if-eqz v0, :cond_1

    .line 106
    const-string v3, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    :cond_1
    const-string v0, "com.google.android.gms.cast.metadata.ALBUM_TITLE"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_2

    .line 111
    const-string v3, "android.media.metadata.ALBUM_TITLE"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    :cond_2
    const-string v0, "com.google.android.gms.cast.metadata.ARTIST"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 115
    if-nez v0, :cond_3

    .line 116
    const-string v0, "com.google.android.gms.cast.metadata.STUDIO"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    :cond_3
    if-eqz v0, :cond_4

    .line 119
    const-string v3, "android.media.metadata.ARTIST"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_4
    const-string v0, "com.google.android.gms.cast.metadata.COMPOSER"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 123
    if-eqz v0, :cond_5

    .line 124
    const-string v3, "android.media.metadata.COMPOSER"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    :cond_5
    const-string v0, "com.google.android.gms.cast.metadata.DISC_NUMBER"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 128
    const-string v0, "com.google.android.gms.cast.metadata.DISC_NUMBER"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->c(Ljava/lang/String;)I

    move-result v0

    .line 129
    const-string v3, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 132
    :cond_6
    const-string v0, "com.google.android.gms.cast.metadata.TITLE"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 133
    if-eqz v0, :cond_7

    .line 134
    const-string v3, "android.media.metadata.TITLE"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_7
    const-string v0, "com.google.android.gms.cast.metadata.TRACK_NUMBER"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 138
    const-string v0, "com.google.android.gms.cast.metadata.TRACK_NUMBER"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->c(Ljava/lang/String;)I

    move-result v0

    .line 139
    const-string v3, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 143
    :cond_8
    const-string v0, "com.google.android.gms.cast.metadata.BROADCAST_DATE"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->d(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 144
    if-nez v0, :cond_9

    .line 145
    const-string v0, "com.google.android.gms.cast.metadata.RELEASE_DATE"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->d(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 147
    :cond_9
    if-nez v0, :cond_a

    .line 148
    const-string v0, "com.google.android.gms.cast.metadata.CREATION_DATE"

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/i;->d(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    .line 150
    :cond_a
    if-eqz v0, :cond_b

    .line 151
    const-string v3, "android.media.metadata.YEAR"

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    :cond_b
    invoke-virtual {v2}, Lcom/google/android/gms/cast/i;->c()Z

    move-result v0

    if-eqz v0, :cond_c

    .line 155
    invoke-virtual {v2}, Lcom/google/android/gms/cast/i;->b()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/WebImage;

    .line 156
    const-string v2, "android.media.metadata.ARTWORK_URI"

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/WebImage;->b()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_c
    move-object v0, v1

    .line 159
    goto/16 :goto_0
.end method

.method static a(Lorg/json/JSONObject;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 54
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 56
    if-nez p0, :cond_0

    move-object v0, v2

    .line 85
    :goto_0
    return-object v0

    .line 60
    :cond_0
    invoke-virtual {p0}, Lorg/json/JSONObject;->keys()Ljava/util/Iterator;

    move-result-object v3

    .line 61
    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 63
    :try_start_0
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 64
    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 66
    sget-object v4, Lorg/json/JSONObject;->NULL:Ljava/lang/Object;

    if-ne v1, v4, :cond_2

    .line 67
    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    goto :goto_1

    .line 83
    :catch_0
    move-exception v0

    goto :goto_1

    .line 68
    :cond_2
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_3

    .line 69
    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 70
    :cond_3
    instance-of v4, v1, Ljava/lang/Boolean;

    if-eqz v4, :cond_4

    .line 71
    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1

    .line 72
    :cond_4
    instance-of v4, v1, Ljava/lang/Integer;

    if-eqz v4, :cond_5

    .line 73
    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    .line 74
    :cond_5
    instance-of v4, v1, Ljava/lang/Long;

    if-eqz v4, :cond_6

    .line 75
    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_1

    .line 76
    :cond_6
    instance-of v4, v1, Ljava/lang/Double;

    if-eqz v4, :cond_7

    .line 77
    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    goto :goto_1

    .line 78
    :cond_7
    instance-of v4, v1, Lorg/json/JSONObject;

    if-eqz v4, :cond_1

    .line 79
    check-cast v1, Lorg/json/JSONObject;

    invoke-static {v1}, Lcom/google/android/gms/cast/media/ak;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :cond_8
    move-object v0, v2

    .line 85
    goto :goto_0
.end method

.method static a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;
    .locals 5

    .prologue
    .line 29
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 31
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 32
    if-eqz p1, :cond_1

    invoke-interface {p1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 33
    :cond_1
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 39
    :try_start_0
    instance-of v4, v1, Landroid/os/Bundle;

    if-eqz v4, :cond_2

    .line 40
    check-cast v1, Landroid/os/Bundle;

    invoke-static {v1, p1}, Lcom/google/android/gms/cast/media/ak;->a(Landroid/os/Bundle;Ljava/util/Set;)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    goto :goto_0

    .line 42
    :cond_2
    invoke-virtual {v2, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 47
    :cond_3
    return-object v2
.end method
