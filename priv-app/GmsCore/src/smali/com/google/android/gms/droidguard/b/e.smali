.class public final Lcom/google/android/gms/droidguard/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static d:Ljava/lang/Class;


# instance fields
.field a:Lcom/google/android/gms/droidguard/b/m;

.field b:Landroid/content/Context;

.field c:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/droidguard/b/e;->d:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/droidguard/b/m;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    .line 66
    iput-object p2, p0, Lcom/google/android/gms/droidguard/b/e;->b:Landroid/content/Context;

    .line 67
    return-void
.end method

.method private static declared-synchronized a(Lcom/google/android/gms/droidguard/b/e;Ljava/lang/String;)Ljava/lang/Class;
    .locals 10

    .prologue
    .line 121
    const-class v6, Lcom/google/android/gms/droidguard/b/e;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lcom/google/android/gms/droidguard/b/e;->d:Ljava/lang/Class;

    if-nez v0, :cond_0

    .line 122
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    const/4 v1, 0x4

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    const/16 v2, 0x26

    new-array v4, v2, [I

    fill-array-data v4, :array_2

    const/16 v2, 0x28

    new-array v3, v2, [I

    fill-array-data v3, :array_3

    const/4 v2, 0x4

    new-array v2, v2, [I

    fill-array-data v2, :array_4

    const/16 v5, 0x69

    new-array v7, v5, [I

    fill-array-data v7, :array_5

    iget-object v5, p0, Lcom/google/android/gms/droidguard/b/e;->b:Landroid/content/Context;

    iget-object v8, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v8, v1}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v1

    const/4 v8, 0x0

    invoke-virtual {v5, v1, v8}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v8

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/droidguard/b/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ldalvik/system/DexClassLoader;

    const-string v1, ""

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-direct {v0, v5, v9, v1, v2}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-static {v8}, Lcom/google/android/gms/common/util/v;->a(Ljava/io/File;)Z

    sput-object v0, Lcom/google/android/gms/droidguard/b/e;->d:Ljava/lang/Class;

    .line 124
    :cond_0
    sget-object v0, Lcom/google/android/gms/droidguard/b/e;->d:Ljava/lang/Class;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v6

    return-object v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 122
    nop

    :array_0
    .array-data 4
        0xde
        0xdb
        0xc8
        0x66
        0xd8
        0xc8
        0x25
        0xb5
        0xbf
        0xce
        0xe5
        0x91
        0xd6
        0xc9
        0xd9
        0xa5
    .end array-data

    :array_1
    .array-data 4
        0xd5
        0xe3
        0x25
        0x85
    .end array-data

    :array_2
    .array-data 4
        0xe1
        0xcf
        0xc0
        0xcf
        0x9a
        0xbe
        0xc3
        0xc0
        0xb6
        0xc1
        0xbf
        0xcb
        0x9a
        0xc0
        0x66
        0xc0
        0xca
        0x9a
        0xbe
        0xda
        0xc3
        0xc0
        0xd8
        0xce
        0xb6
        0xc1
        0xbf
        0xc4
        0xd0
        0xc4
        0xce
        0xdb
        0xbe
        0xde
        0xb5
        0xb7
        0x95
        0xbc
    .end array-data

    :array_3
    .array-data 4
        0xcd
        0xd0
        0xdd
        0xc5
        0x44
        0x7f
        0xca
        0x66
        0x79
        0xd4
        0xce
        0xd6
        0xd6
        0x89
        0xcb
        0xdd
        0x79
        0xd6
        0x66
        0x47
        0xcf
        0xe4
        0xdb
        0xd6
        0xde
        0x2d
        0x79
        0x7f
        0x73
        0xd7
        0x47
        0xe4
        0xbf
        0xcd
        0xd4
        0xc0
        0xc0
        0xc3
        0x95
        0xb6
    .end array-data

    :array_4
    .array-data 4
        0xc5
        0xc8
        0x44
        0x7f
    .end array-data

    :array_5
    .array-data 4
        0xcb
        0xbc
        0xbe
        0x26
        0xd2
        0xb5
        0xcd
        0xe0
        0xe3
        0xca
        0xb6
        0xbe
        0xbe
        0xe2
        0xb6
        0xca
        0xd6
        0x75
        0xb7
        0xc8
        0xc8
        0xb5
        0xcf
        0xbc
        0xbc
        0xe2
        0xd5
        0xbc
        0xb5
        0xbf
        0x85
        0xc1
        0xc4
        0xdf
        0xd7
        0xb7
        0xb5
        0xc0
        0xd9
        0x9a
        0xbe
        0xd6
        0xc3
        0xc0
        0xc8
        0xd2
        0xb6
        0xd2
        0xca
        0xc1
        0xbf
        0xca
        0xe2
        0x9a
        0xe3
        0xc0
        0xd0
        0xe2
        0xcb
        0xdd
        0xb5
        0xc0
        0xd9
        0x9a
        0xbe
        0xda
        0xce
        0xd2
        0xc3
        0xe4
        0xda
        0xc8
        0xc0
        0xd1
        0xc9
        0xb6
        0xca
        0xd6
        0xc1
        0xbf
        0xc4
        0xdc
        0xdb
        0xc4
        0xbe
        0xb5
        0x14
        0x9a
        0xbe
        0xc3
        0xce
        0xdf
        0xc0
        0xd9
        0xe1
        0xdb
        0xcc
        0xb6
        0xcf
        0xc1
        0xbf
        0xc4
        0xc4
        0xd1
        0xbe
    .end array-data
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 245
    const/16 v0, 0x10

    :try_start_0
    new-array v1, v0, [B

    .line 246
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 247
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 248
    aget-byte v3, v2, v0

    aput-byte v3, v1, v0

    .line 247
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_0
    new-instance v0, Ljavax/crypto/spec/SecretKeySpec;

    invoke-direct {v0, v1, p2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 253
    const/16 v1, 0x10

    new-array v1, v1, [B

    fill-array-data v1, :array_0

    .line 254
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 257
    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/e;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    invoke-virtual {v1, p4}, Landroid/content/res/AssetManager;->openFd(Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    .line 258
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->getLength()J

    move-result-wide v4

    long-to-int v3, v4

    .line 259
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->close()V

    .line 260
    new-array v1, v3, [B

    .line 261
    iget-object v3, p0, Lcom/google/android/gms/droidguard/b/e;->b:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v3

    invoke-virtual {v3, p4}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v3

    .line 262
    invoke-virtual {v3, v1}, Ljava/io/InputStream;->read([B)I

    .line 263
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    .line 266
    invoke-static {p3}, Lcom/google/android/gms/common/util/e;->f(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v3

    .line 267
    if-nez v3, :cond_1

    .line 280
    :goto_1
    return-void

    .line 270
    :cond_1
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v0, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 273
    invoke-virtual {v3, v1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v0

    .line 274
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p5}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    .line 275
    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 276
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 280
    :catch_0
    move-exception v0

    goto :goto_1

    .line 253
    :array_0
    .array-data 1
        0x1t
        0x21t
        0xdt
        0x1ct
        0x57t
        0x7at
        0x19t
        0x5t
        0x4t
        0x1et
        0x16t
        0x65t
        0x5t
        0x32t
        0xct
        0x0t
    .end array-data
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)Lcom/google/android/gms/droidguard/b/d;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 75
    new-instance v2, Lcom/google/android/gms/droidguard/b/d;

    invoke-direct {v2}, Lcom/google/android/gms/droidguard/b/d;-><init>()V

    .line 78
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/gms/droidguard/b/e;->a(Lcom/google/android/gms/droidguard/b/e;Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    .line 81
    const/16 v0, 0xc

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    if-eqz p2, :cond_0

    array-length v4, p2

    array-length v5, v0

    mul-int/lit8 v5, v5, 0x4

    if-ge v4, v5, :cond_1

    .line 87
    :cond_0
    const/16 v1, 0xd

    new-array v1, v1, [I

    fill-array-data v1, :array_1

    .line 90
    const/4 v4, 0x6

    new-array v4, v4, [I

    fill-array-data v4, :array_2

    .line 92
    const/16 v5, 0x16

    new-array v5, v5, [I

    fill-array-data v5, :array_3

    .line 95
    const/16 v6, 0x10

    new-array v6, v6, [I

    fill-array-data v6, :array_4

    .line 97
    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gms/droidguard/b/e;->c:Ljava/lang/Object;

    .line 99
    iget-object v7, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v7, v4}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v4

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v3, v4, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 101
    iget-object v7, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v7, v6}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, [F

    aput-object v9, v7, v8

    invoke-virtual {v3, v6, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    .line 103
    iget-object v7, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v7, v1}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v1

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v3, v1, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 104
    iget-object v7, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v7, v5}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v5

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Class;

    invoke-virtual {v3, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 105
    iget-object v5, p0, Lcom/google/android/gms/droidguard/b/e;->c:Ljava/lang/Object;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v0, v7, v8

    invoke-virtual {v6, v5, v7}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/e;->c:Ljava/lang/Object;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/droidguard/b/d;->a:Ljava/lang/String;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/e;->c:Ljava/lang/Object;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/droidguard/b/d;->b:Ljava/lang/String;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/droidguard/b/e;->c:Ljava/lang/Object;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v3, v0, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/droidguard/b/d;->c:Ljava/lang/String;

    .line 116
    :goto_0
    return-object v2

    .line 81
    :cond_1
    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    sget-object v5, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v4

    :goto_1
    array-length v5, v0

    if-ge v1, v5, :cond_0

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x4f000000

    div-float/2addr v5, v6

    aput v5, v0, v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0

    :array_0
    .array-data 4
        0x3eb5c662
        0x3ef1a5a4
        0x3e7cd254
        0x3f2b22d5
        0x3f464de2
        0x3f18c441
        0x3d806d58
        0x3e449ecc
        0x3eb3a9c2
        0x3f0eaedf
        0x3ef0c5ca
        0x3f7c940d
    .end array-data

    .line 87
    :array_1
    .array-data 4
        0xe5
        0xb6
        0xd6
        0xce
        0xb7
        0xba
        0x4
        0xbf
        0xc4
        0xd3
        0xd0
        0xab
        0x35
    .end array-data

    .line 90
    :array_2
    .array-data 4
        0xb6
        0xb7
        0xba
        0x90
        0x91
        0xc1
    .end array-data

    .line 92
    :array_3
    .array-data 4
        0xde
        0xb6
        0xdb
        0xe0
        0xd6
        0xcf
        0xd2
        0xb7
        0xca
        0xc8
        0xd2
        0xba
        0xe1
        0x4
        0xbf
        0xcd
        0xc4
        0xcc
        0xd8
        0xab
        0xd4
        0xc7
    .end array-data

    .line 95
    :array_4
    .array-data 4
        0x9a
        0xd6
        0xd6
        0xb7
        0xd1
        0x95
        0xce
        0xe2
        0xd3
        0xd8
        0xc0
        0xd0
        0xd2
        0xb7
        0xd1
        0x9a
    .end array-data
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/droidguard/b/d;
    .locals 4

    .prologue
    .line 71
    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/e;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/droidguard/b/e;->a(Ljava/lang/String;[B)Lcom/google/android/gms/droidguard/b/d;

    move-result-object v0

    return-object v0

    :cond_0
    array-length v2, p2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, p2, v0

    invoke-virtual {v3}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->update([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    goto :goto_0

    :array_0
    .array-data 4
        0x7f
        0x4
        0xcc
        0xd1
        0xc5
        0xd3
        0xe2
        0xda
        0x13
        0xca
        0xd8
        0x73
        0xe0
        0xd3
        0xe3
        0xce
        0xce
        0x35
        0xc7
    .end array-data
.end method
