.class public final Lcom/google/android/gms/drive/database/model/ce;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Z

.field g:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/UserMetadata;)V
    .locals 7

    .prologue
    .line 47
    invoke-virtual {p2}, Lcom/google/android/gms/drive/UserMetadata;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/drive/UserMetadata;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/drive/UserMetadata;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/drive/UserMetadata;->d()Z

    move-result v5

    invoke-virtual {p2}, Lcom/google/android/gms/drive/UserMetadata;->e()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/ce;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 35
    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 36
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ce;->a:Ljava/lang/String;

    .line 37
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/ce;->b:Ljava/lang/String;

    .line 38
    iput-object p4, p0, Lcom/google/android/gms/drive/database/model/ce;->c:Ljava/lang/String;

    .line 39
    iput-boolean p5, p0, Lcom/google/android/gms/drive/database/model/ce;->d:Z

    .line 40
    iput-object p6, p0, Lcom/google/android/gms/drive/database/model/ce;->g:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 100
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->a:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ce;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->b:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ce;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->d:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ce;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->e:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/database/model/ce;->d:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 105
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->c:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ce;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    return-void
.end method
