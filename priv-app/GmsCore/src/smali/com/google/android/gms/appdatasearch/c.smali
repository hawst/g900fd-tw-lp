.class public final Lcom/google/android/gms/appdatasearch/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/er;


# instance fields
.field final a:Landroid/os/ConditionVariable;

.field b:Lcom/google/android/gms/common/c;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/appdatasearch/a/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0}, Landroid/os/ConditionVariable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Landroid/os/ConditionVariable;

    .line 97
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/c;->c:Landroid/content/Context;

    .line 98
    new-instance v0, Lcom/google/android/gms/appdatasearch/a/a;

    new-instance v1, Lcom/google/android/gms/appdatasearch/d;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/appdatasearch/d;-><init>(Lcom/google/android/gms/appdatasearch/c;B)V

    new-instance v2, Lcom/google/android/gms/appdatasearch/e;

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/appdatasearch/e;-><init>(Lcom/google/android/gms/appdatasearch/c;B)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/appdatasearch/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    .line 101
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 485
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    .line 486
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 488
    const-string v0, "AppDataSearchClient"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    const-string v0, "AppDataSearchClient"

    const-string v1, "verifyContentProviderClient: caller is current process"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 511
    :cond_0
    return-void

    .line 495
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.google.android.gms"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    .line 497
    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    if-eq v1, v0, :cond_2

    .line 498
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Calling UID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not Google Play Services."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 501
    :catch_0
    move-exception v0

    .line 502
    new-instance v1, Ljava/lang/SecurityException;

    const-string v2, "Google Play Services not installed"

    invoke-direct {v1, v2, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 505
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/Context;)I

    move-result v0

    .line 506
    if-eqz v0, :cond_0

    .line 507
    new-instance v1, Ljava/lang/SecurityException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Calling package problem: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;[Ljava/lang/String;ILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 7

    .prologue
    .line 531
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->f_()Lcom/google/android/gms/appdatasearch/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/c;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x64

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/a/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 539
    :goto_0
    return-object v0

    .line 537
    :catch_0
    move-exception v0

    .line 538
    const-string v1, "AppDataSearchClient"

    const-string v2, "Query failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 539
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)Lcom/google/android/gms/common/c;
    .locals 3

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/c;->a()V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0, p1, p2}, Landroid/os/ConditionVariable;->block(J)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->b()V

    .line 125
    new-instance v0, Lcom/google/android/gms/common/c;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 132
    :goto_0
    return-object v0

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->b:Lcom/google/android/gms/common/c;

    if-eqz v0, :cond_1

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->b:Lcom/google/android/gms/common/c;

    goto :goto_0

    .line 132
    :cond_1
    sget-object v0, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    goto :goto_0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->b:Lcom/google/android/gms/common/c;

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->a:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->a()V

    .line 108
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/es;)V
    .locals 1

    .prologue
    .line 921
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/appdatasearch/a/a;->a(Lcom/google/android/gms/common/es;)V

    .line 922
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/et;)V
    .locals 1

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/appdatasearch/a/a;->a(Lcom/google/android/gms/common/et;)V

    .line 940
    return-void
.end method

.method public final a(Ljava/lang/String;J)Z
    .locals 8

    .prologue
    .line 448
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->f_()Lcom/google/android/gms/appdatasearch/a/b;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v6, 0x0

    move-object v3, p1

    move-wide v4, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/appdatasearch/a/b;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/appdatasearch/RequestIndexingSpecification;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 452
    :goto_0
    return v0

    .line 450
    :catch_0
    move-exception v0

    .line 451
    const-string v1, "AppDataSearchClient"

    const-string v2, "Request indexing failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 452
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->b()V

    .line 139
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/es;)Z
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/appdatasearch/a/a;->b(Lcom/google/android/gms/common/es;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/gms/common/es;)V
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/appdatasearch/a/a;->c(Lcom/google/android/gms/common/es;)V

    .line 934
    return-void
.end method

.method public final c_()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->c_()Z

    move-result v0

    return v0
.end method

.method public final e()Lcom/google/android/gms/appdatasearch/StorageStats;
    .locals 3

    .prologue
    .line 358
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->f_()Lcom/google/android/gms/appdatasearch/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/appdatasearch/a/b;->c()Lcom/google/android/gms/appdatasearch/StorageStats;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 361
    :goto_0
    return-object v0

    .line 359
    :catch_0
    move-exception v0

    .line 360
    const-string v1, "AppDataSearchClient"

    const-string v2, "Get storage statistics failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 361
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 3

    .prologue
    .line 835
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->f_()Lcom/google/android/gms/appdatasearch/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/appdatasearch/a/b;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 838
    :goto_0
    return v0

    .line 836
    :catch_0
    move-exception v0

    .line 837
    const-string v1, "AppDataSearchClient"

    const-string v2, "clearUsageReportData failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 838
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n_()Z
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/c;->d:Lcom/google/android/gms/appdatasearch/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/a/a;->n_()Z

    move-result v0

    return v0
.end method
