.class public abstract Lcom/google/android/gms/car/mp;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/mo;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/car/mo;
    .locals 2

    .prologue
    .line 24
    if-nez p0, :cond_0

    .line 25
    const/4 v0, 0x0

    .line 31
    :goto_0
    return-object v0

    .line 27
    :cond_0
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 28
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/car/mo;

    if-eqz v1, :cond_1

    .line 29
    check-cast v0, Lcom/google/android/gms/car/mo;

    goto :goto_0

    .line 31
    :cond_1
    new-instance v0, Lcom/google/android/gms/car/mq;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/mq;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 39
    sparse-switch p1, :sswitch_data_0

    .line 209
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v4

    :goto_0
    return v4

    .line 43
    :sswitch_0
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 48
    :sswitch_1
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    if-nez v2, :cond_0

    move-object v0, v1

    .line 52
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/car/jy;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/jx;

    move-result-object v1

    .line 53
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/mp;->a(Lcom/google/android/gms/car/mr;Lcom/google/android/gms/car/jx;)V

    goto :goto_0

    .line 50
    :cond_0
    const-string v0, "com.google.android.gms.car.ICarProjectionCallback"

    invoke-interface {v2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/car/mr;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gms/car/mr;

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/gms/car/mt;

    invoke-direct {v0, v2}, Lcom/google/android/gms/car/mt;-><init>(Landroid/os/IBinder;)V

    goto :goto_1

    .line 58
    :sswitch_2
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    sget-object v0, Lcom/google/android/gms/car/DrawingSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/DrawingSpec;

    move-object v2, v0

    .line 69
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 70
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    move-object v3, v0

    .line 76
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 77
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 82
    :goto_4
    invoke-virtual {p0, v5, v2, v3, v0}, Lcom/google/android/gms/car/mp;->a(ILcom/google/android/gms/car/DrawingSpec;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_2
    move-object v2, v1

    .line 66
    goto :goto_2

    :cond_3
    move-object v3, v1

    .line 73
    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 80
    goto :goto_4

    .line 87
    :sswitch_3
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 90
    sget-object v0, Landroid/content/Intent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 95
    :goto_5
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/mp;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_5
    move-object v0, v1

    .line 93
    goto :goto_5

    .line 100
    :sswitch_4
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 102
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 103
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/mp;->a(I)V

    goto/16 :goto_0

    .line 108
    :sswitch_5
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 110
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 111
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/mp;->b(I)V

    goto/16 :goto_0

    .line 116
    :sswitch_6
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 120
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    .line 122
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 123
    sget-object v0, Lcom/google/android/gms/car/DrawingSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/DrawingSpec;

    move-object v2, v0

    .line 129
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 130
    sget-object v0, Landroid/content/res/Configuration;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Configuration;

    .line 135
    :goto_7
    invoke-virtual {p0, v3, v5, v2, v0}, Lcom/google/android/gms/car/mp;->a(IILcom/google/android/gms/car/DrawingSpec;Landroid/content/res/Configuration;)V

    goto/16 :goto_0

    :cond_6
    move-object v2, v1

    .line 126
    goto :goto_6

    :cond_7
    move-object v0, v1

    .line 133
    goto :goto_7

    .line 140
    :sswitch_7
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 142
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 144
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 145
    sget-object v0, Landroid/view/MotionEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/MotionEvent;

    .line 150
    :goto_8
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/car/mp;->a(ILandroid/view/MotionEvent;)V

    goto/16 :goto_0

    :cond_8
    move-object v0, v1

    .line 148
    goto :goto_8

    .line 155
    :sswitch_8
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 159
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 160
    sget-object v0, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 165
    :goto_9
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/car/mp;->a(ILandroid/view/KeyEvent;)V

    goto/16 :goto_0

    :cond_9
    move-object v0, v1

    .line 163
    goto :goto_9

    .line 170
    :sswitch_9
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 172
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    .line 173
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/mp;->c(I)V

    goto/16 :goto_0

    .line 178
    :sswitch_a
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    move v0, v4

    .line 181
    :goto_a
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/mp;->a(Z)V

    goto/16 :goto_0

    .line 180
    :cond_a
    const/4 v0, 0x0

    goto :goto_a

    .line 186
    :sswitch_b
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/car/mp;->a()V

    goto/16 :goto_0

    .line 192
    :sswitch_c
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/gms/car/mp;->b()V

    goto/16 :goto_0

    .line 198
    :sswitch_d
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 199
    invoke-virtual {p0}, Lcom/google/android/gms/car/mp;->c()V

    goto/16 :goto_0

    .line 204
    :sswitch_e
    const-string v0, "com.google.android.gms.car.ICarProjection"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/gms/car/mp;->d()V

    goto/16 :goto_0

    .line 39
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
