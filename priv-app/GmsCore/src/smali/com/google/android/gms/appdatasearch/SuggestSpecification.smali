.class public Lcom/google/android/gms/appdatasearch/SuggestSpecification;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ay;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/gms/appdatasearch/ay;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ay;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ay;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/appdatasearch/SuggestSpecification;-><init>(I)V

    .line 31
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput p1, p0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->a:I

    .line 27
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ay;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/gms/appdatasearch/SuggestSpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ay;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/ay;->a(Lcom/google/android/gms/appdatasearch/SuggestSpecification;Landroid/os/Parcel;)V

    .line 43
    return-void
.end method
