.class public Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field public static a:Landroid/os/Handler;

.field private static final b:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 62
    const-string v0, "CastMirroringIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 63
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a:Landroid/os/Handler;

    .line 64
    return-void
.end method

.method public static a(Landroid/content/Context;ILcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 1

    .prologue
    .line 216
    new-instance v0, Lcom/google/android/gms/cast_mirroring/c;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/cast_mirroring/c;-><init>(ILcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast_mirroring/e;)V

    .line 217
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 1

    .prologue
    .line 211
    new-instance v0, Lcom/google/android/gms/cast_mirroring/a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast_mirroring/a;-><init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast_mirroring/e;)V

    .line 212
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 6

    .prologue
    .line 205
    new-instance v0, Lcom/google/android/gms/cast_mirroring/f;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/cast_mirroring/f;-><init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast_mirroring/e;)V

    .line 207
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/cast_mirroring/e;)V
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 56
    sget-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 57
    const-string v0, "com.google.android.gms.cast_mirroring.service.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 58
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast_mirroring/e;

    .line 69
    if-nez v0, :cond_0

    .line 70
    const-string v0, "CastMirroringIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :goto_0
    return-void

    .line 81
    :cond_0
    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast_mirroring/e;->a(Landroid/content/Context;)V

    goto :goto_0
.end method
