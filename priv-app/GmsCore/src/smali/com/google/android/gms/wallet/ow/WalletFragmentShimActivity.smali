.class public Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->setResult(ILandroid/content/Intent;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->finish()V

    .line 115
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const/16 v4, 0x194

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 40
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    if-eqz p1, :cond_0

    .line 110
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 46
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 47
    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->a(I)V

    goto :goto_0

    .line 52
    :cond_1
    new-instance v2, Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v7}, Lcom/google/android/gms/wallet/service/ow/ar;-><init>(Landroid/content/Context;Z)V

    .line 54
    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/service/ow/as;

    move-result-object v3

    .line 56
    if-eqz v3, :cond_2

    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v2, :cond_3

    .line 57
    :cond_2
    const-string v1, "WalletFragmentShimActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid transaction key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->a(I)V

    goto :goto_0

    .line 64
    :cond_3
    iget-object v0, v3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    if-eqz v0, :cond_8

    .line 67
    iget-object v0, v3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-object v0, v0, Lcom/google/aa/a/a/a/d;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 68
    iget-object v0, v3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-object v0, v0, Lcom/google/aa/a/a/a/d;->a:Ljava/lang/String;

    .line 70
    :goto_1
    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-object v2, v2, Lcom/google/aa/a/a/a/d;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 71
    iget-object v2, v3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-object v2, v2, Lcom/google/aa/a/a/a/d;->b:Ljava/lang/String;

    .line 73
    :goto_2
    iget-object v4, v3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-object v4, v4, Lcom/google/aa/a/a/a/d;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 74
    iget-object v1, v3, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-object v1, v1, Lcom/google/aa/a/a/a/d;->c:Ljava/lang/String;

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    .line 77
    :goto_3
    iget-object v4, v3, Lcom/google/android/gms/wallet/service/ow/as;->e:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v4, v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v1

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.google.android.gms.wallet.EXTRA_PARAMETERS"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 80
    if-nez v4, :cond_4

    .line 81
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 83
    :cond_4
    const-string v5, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    iget-object v6, v3, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    invoke-static {v4}, Lcom/google/android/gms/wallet/common/c;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v4

    .line 85
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v5

    .line 86
    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a()Lcom/google/android/gms/wallet/shared/d;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/google/android/gms/wallet/shared/d;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/gms/wallet/shared/d;->d(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/google/android/gms/wallet/shared/d;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v5

    const-string v6, "onlinewallet"

    invoke-virtual {v5, v6}, Lcom/google/android/gms/wallet/shared/d;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/gms/wallet/shared/d;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/d;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    .line 93
    new-instance v5, Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/wallet/cache/h;-><init>(Landroid/content/Context;Z)V

    .line 95
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v4

    iget-object v6, v3, Lcom/google/android/gms/wallet/service/ow/as;->d:Landroid/accounts/Account;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v4, v6, v7}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/i;

    move-result-object v4

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v6, "com.google.android.gms.wallet.ANALYTICS_SESSION_ID"

    invoke-virtual {v5, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string v7, "com.google.android.gms.wallet.MASKED_WALLET_FLOW_TYPE"

    const/4 v8, 0x0

    invoke-virtual {v5, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 101
    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/MaskedWalletRequest;Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;Lcom/google/android/gms/wallet/cache/i;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 105
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const/high16 v2, 0x2000000

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 106
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->startActivity(Landroid/content/Intent;)V

    .line 109
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/WalletFragmentShimActivity;->a(I)V

    goto/16 :goto_0

    :cond_5
    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    goto/16 :goto_3

    :cond_6
    move-object v2, v1

    goto/16 :goto_2

    :cond_7
    move-object v0, v1

    goto/16 :goto_1

    :cond_8
    move-object v0, v1

    move-object v2, v1

    goto/16 :goto_3
.end method
