.class public Lcom/google/android/gms/plus/apps/ManageMomentActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/plus/apps/aa;
.implements Lcom/google/android/gms/plus/apps/ac;
.implements Lcom/google/android/gms/plus/apps/au;
.implements Lcom/google/android/gms/plus/apps/bc;
.implements Lcom/google/android/gms/plus/apps/bi;
.implements Lcom/google/android/gms/plus/internal/ak;


# instance fields
.field private a:Landroid/accounts/Account;

.field private b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

.field private c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:Z

.field private h:Lcom/google/android/gms/plus/apps/ab;

.field private i:Lcom/google/android/gms/plus/apps/ba;

.field private j:Lcom/google/android/gms/plus/apps/as;

.field private k:Landroid/app/AlertDialog;

.field private final l:Lcom/google/android/gms/plus/internal/ad;

.field private m:Lcom/google/android/gms/plus/internal/ab;

.field private n:Lcom/google/android/gms/plus/apps/bg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;-><init>(Lcom/google/android/gms/plus/internal/ad;)V

    .line 105
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/plus/internal/ad;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f:Z

    .line 109
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->l:Lcom/google/android/gms/plus/internal/ad;

    .line 110
    return-void
.end method

.method private e()V
    .locals 3

    .prologue
    .line 398
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 399
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 400
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 405
    :goto_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 408
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v1, :cond_0

    .line 409
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/a;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/apps/b;

    move-result-object v1

    .line 410
    iget-object v2, v1, Lcom/google/android/gms/plus/apps/b;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 411
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->e()Ljava/lang/String;

    move-result-object v0

    .line 412
    iget-boolean v1, v1, Lcom/google/android/gms/plus/apps/b;->c:Z

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 413
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/as;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/as;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->j:Lcom/google/android/gms/plus/apps/as;

    .line 414
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->j:Lcom/google/android/gms/plus/apps/as;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/plus/apps/as;->a(Lcom/google/android/gms/plus/apps/au;)V

    .line 415
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->j:Lcom/google/android/gms/plus/apps/as;

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/apps/as;->a(Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V

    .line 418
    :cond_0
    return-void

    .line 403
    :cond_1
    sget v1, Lcom/google/android/gms/p;->ur:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->c(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 5

    .prologue
    .line 491
    sget v0, Lcom/google/android/gms/j;->m:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 492
    sget v1, Lcom/google/android/gms/p;->um:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/plus/apps/z;->a(Landroid/accounts/Account;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/z;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "delete_moment_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/apps/z;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 503
    :cond_0
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 506
    sget v0, Lcom/google/android/gms/p;->un:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 507
    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ak;Ljava/lang/String;)V

    .line 255
    return-void
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->g()V

    .line 264
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/apps/bh;)V
    .locals 4

    .prologue
    .line 374
    iget-object v0, p1, Lcom/google/android/gms/plus/apps/bh;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 375
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iget-object v1, p1, Lcom/google/android/gms/plus/apps/bh;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/apps/bh;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/plus/apps/bh;->a:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 378
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->e()V

    .line 380
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 359
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/support/v7/app/a;->a(Landroid/graphics/drawable/Drawable;)V

    .line 360
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/a;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/a;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/plus/apps/b;

    .line 362
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->h:Lcom/google/android/gms/plus/apps/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/apps/ab;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    sget v0, Lcom/google/android/gms/p;->to:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "progress_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 336
    :goto_0
    return-void

    .line 332
    :cond_0
    sget v0, Lcom/google/android/gms/p;->tn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/f/d;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/f/d;

    move-result-object v0

    .line 334
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "error_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/f/d;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 367
    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 368
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/az;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/az;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/apps/az;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    .line 370
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 342
    :cond_0
    if-eqz p2, :cond_1

    .line 343
    sget v0, Lcom/google/android/gms/p;->tn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/f/d;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/f/d;

    move-result-object v0

    .line 345
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 346
    const-string v2, "error_dialog"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 347
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 354
    :goto_0
    return-void

    .line 349
    :cond_1
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 350
    const-string v1, "deleted_moment_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(ILandroid/content/Intent;)V

    .line 352
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 5

    .prologue
    .line 268
    if-eqz p1, :cond_3

    .line 269
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    sget v0, Lcom/google/android/gms/p;->uh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    .line 282
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f()V

    .line 286
    :goto_1
    return-void

    .line 272
    :cond_0
    sget v0, Lcom/google/android/gms/p;->uo:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 274
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 275
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_1

    .line 276
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 278
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 280
    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    goto :goto_0

    .line 284
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->g()V

    goto :goto_1
.end method

.method public final i_()Z
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    .line 306
    const/4 v0, 0x1

    return v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f:Z

    .line 487
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->k:Landroid/app/AlertDialog;

    .line 488
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 479
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f:Z

    .line 480
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 481
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->k:Landroid/app/AlertDialog;

    .line 482
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 290
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 291
    sget v1, Lcom/google/android/gms/j;->ed:I

    if-ne v0, v1, :cond_1

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->g:Z

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/apps/z;->a(Landroid/accounts/Account;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;Ljava/lang/String;)Lcom/google/android/gms/plus/apps/z;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "delete_moment_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/apps/z;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 298
    :cond_1
    sget v1, Lcom/google/android/gms/j;->ka:I

    if-ne v0, v1, :cond_0

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->ad_()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->ad_()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->c()Ljava/lang/String;

    move-result-object v0

    :goto_1
    sget v1, Lcom/google/android/gms/p;->tX:I

    invoke-static {v0, v1, p0, p0}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/String;ILandroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v3, Lcom/google/android/gms/common/analytics/c;->x:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/common/analytics/d;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1

    :cond_3
    sget-object v3, Lcom/google/android/gms/common/analytics/c;->y:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 114
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 115
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    const-string v0, "AppSettings"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 125
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    goto :goto_0

    .line 131
    :cond_2
    if-eqz p1, :cond_3

    .line 132
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    .line 133
    const-string v0, "moment"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    .line 134
    const-string v0, "application"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 135
    const-string v0, "moment_acl"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    .line 136
    const-string v0, "manage_error"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f:Z

    .line 140
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    if-nez v0, :cond_5

    .line 141
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "app_activity"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "application"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 145
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    if-nez v0, :cond_8

    .line 146
    :cond_6
    const-string v0, "AppSettings"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 147
    const-string v0, "AppSettings"

    const-string v1, "Missing required extra(s): account=%s moment=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    aput-object v3, v2, v4

    iget-object v3, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_7
    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->finish()V

    goto/16 :goto_0

    .line 155
    :cond_8
    sget v0, Lcom/google/android/gms/l;->dS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->setContentView(I)V

    .line 157
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_c

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->e()V

    :goto_1
    sget v0, Lcom/google/android/gms/j;->t:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->sv:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->ac_()Z

    move-result v0

    if-eqz v0, :cond_9

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->h()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/x;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/apps/x;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/x;->a()J

    move-result-wide v0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/apps/y;->a(Landroid/content/Context;J)Ljava/lang/CharSequence;

    move-result-object v1

    sget v0, Lcom/google/android/gms/j;->sM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_9
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->m()Z

    move-result v0

    if-eqz v0, :cond_d

    sget v0, Lcom/google/android/gms/j;->ed:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lcom/google/android/gms/p;->uq:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 159
    :cond_a
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    if-nez v0, :cond_e

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->l:Lcom/google/android/gms/plus/internal/ad;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, p0, p0, p0, v1}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/internal/ad;Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;Ljava/lang/String;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    .line 169
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 170
    const-string v0, "delete_moment_fragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ab;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->h:Lcom/google/android/gms/plus/apps/ab;

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->h:Lcom/google/android/gms/plus/apps/ab;

    if-nez v0, :cond_b

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/ab;->a(Landroid/accounts/Account;)Lcom/google/android/gms/plus/apps/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->h:Lcom/google/android/gms/plus/apps/ab;

    .line 174
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->h:Lcom/google/android/gms/plus/apps/ab;

    const-string v3, "delete_moment_fragment"

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 176
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 180
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->ad_()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->ad_()Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ItemScope;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 181
    :goto_5
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/az;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/az;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/az;->a(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 182
    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 183
    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setClickable(Z)V

    .line 185
    if-eqz v2, :cond_10

    .line 186
    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_0

    .line 157
    :cond_c
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->e()V

    goto/16 :goto_1

    :cond_d
    sget v0, Lcom/google/android/gms/j;->lH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->lK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->dI:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->us:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    sget-object v1, Lcom/google/android/gms/plus/c/a;->F:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v4, v1}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/gms/p;->tA:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->k:Landroid/app/AlertDialog;

    goto/16 :goto_3

    .line 165
    :cond_e
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f()V

    goto/16 :goto_4

    :cond_f
    move-object v1, v2

    .line 180
    goto/16 :goto_5

    .line 188
    :cond_10
    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/h;->cs:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 189
    if-eqz v1, :cond_0

    .line 190
    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->e:Ljava/lang/String;

    .line 191
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/ba;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ba;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->i:Lcom/google/android/gms/plus/apps/ba;

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->i:Lcom/google/android/gms/plus/apps/ba;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/ba;->a(Lcom/google/android/gms/plus/apps/bc;)V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->i:Lcom/google/android/gms/plus/apps/ba;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ba;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto/16 :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 227
    invoke-super {p0}, Landroid/support/v7/app/d;->onDestroy()V

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->j:Lcom/google/android/gms/plus/apps/as;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->j:Lcom/google/android/gms/plus/apps/as;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/as;->b(Lcom/google/android/gms/plus/apps/au;)V

    .line 230
    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->j:Lcom/google/android/gms/plus/apps/as;

    .line 232
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->i:Lcom/google/android/gms/plus/apps/ba;

    if-eqz v0, :cond_1

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->i:Lcom/google/android/gms/plus/apps/ba;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/ba;->b(Lcom/google/android/gms/plus/apps/bc;)V

    .line 234
    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->i:Lcom/google/android/gms/plus/apps/ba;

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->k:Landroid/app/AlertDialog;

    if-eqz v0, :cond_2

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->k:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 238
    iput-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->k:Landroid/app/AlertDialog;

    .line 240
    :cond_2
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 214
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 216
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 218
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->n:Lcom/google/android/gms/plus/apps/bg;

    if-eqz v0, :cond_2

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->n:Lcom/google/android/gms/plus/apps/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/apps/bg;->a(Lcom/google/android/gms/plus/apps/bi;)V

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->n:Lcom/google/android/gms/plus/apps/bg;

    .line 222
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->g:Z

    .line 223
    return-void
.end method

.method public onResumeFragments()V
    .locals 3

    .prologue
    .line 200
    invoke-super {p0}, Landroid/support/v7/app/d;->onResumeFragments()V

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->m:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    .line 206
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/plus/apps/bg;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->n:Lcom/google/android/gms/plus/apps/bg;

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->n:Lcom/google/android/gms/plus/apps/bg;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/plus/apps/bg;->a(Lcom/google/android/gms/plus/apps/bi;Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->g:Z

    .line 210
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 244
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 245
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->a:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 246
    const-string v0, "moment"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 247
    const-string v0, "application"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->c:Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 248
    const-string v0, "moment_acl"

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 249
    const-string v0, "manage_error"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/apps/ManageMomentActivity;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 250
    return-void
.end method
