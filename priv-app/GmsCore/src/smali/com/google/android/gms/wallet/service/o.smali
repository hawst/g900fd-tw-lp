.class public abstract Lcom/google/android/gms/wallet/service/o;
.super Lcom/google/android/gms/wallet/service/n;
.source "SourceFile"


# instance fields
.field protected final b:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/n;-><init>(Landroid/accounts/Account;)V

    .line 85
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/o;->b:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 86
    return-void
.end method


# virtual methods
.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/o;->b:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
