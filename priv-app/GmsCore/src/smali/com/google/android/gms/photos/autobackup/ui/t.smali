.class public final Lcom/google/android/gms/photos/autobackup/ui/t;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/a/b;
.implements Lcom/google/android/libraries/social/i/af;
.implements Lcom/google/android/libraries/social/i/ag;
.implements Lcom/google/android/libraries/social/i/ak;


# instance fields
.field final a:Lcom/google/android/libraries/social/b/a;

.field b:Lcom/google/android/gms/photos/autobackup/l;

.field c:Lcom/google/android/gms/common/api/v;

.field d:Lcom/google/android/gms/photos/autobackup/ui/r;

.field e:Z

.field f:Ljava/lang/Runnable;

.field final g:Lcom/google/android/gms/common/api/aq;

.field private final h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/libraries/social/i/w;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/u;-><init>(Lcom/google/android/gms/photos/autobackup/ui/t;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->f:Ljava/lang/Runnable;

    .line 50
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/v;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/ui/v;-><init>(Lcom/google/android/gms/photos/autobackup/ui/t;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->g:Lcom/google/android/gms/common/api/aq;

    .line 66
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->h:Landroid/content/Context;

    .line 67
    new-instance v0, Lcom/google/android/libraries/social/b/a;

    invoke-direct {v0, p2}, Lcom/google/android/libraries/social/b/a;-><init>(Lcom/google/android/libraries/social/i/w;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->a:Lcom/google/android/libraries/social/b/a;

    .line 68
    invoke-virtual {p2, p0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;

    .line 69
    return-void
.end method

.method private declared-synchronized c()V
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 84
    :goto_0
    monitor-exit p0

    return-void

    .line 75
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->e:Z

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->h:Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/ui/ae;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->c:Lcom/google/android/gms/common/api/v;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->h:Landroid/content/Context;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/s;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/ui/s;

    .line 79
    if-nez v0, :cond_1

    .line 80
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/w;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/photos/autobackup/ui/w;-><init>(B)V

    .line 82
    :cond_1
    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/r;

    invoke-direct {v1, v0}, Lcom/google/android/gms/photos/autobackup/ui/r;-><init>(Lcom/google/android/gms/photos/autobackup/ui/s;)V

    iput-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->d:Lcom/google/android/gms/photos/autobackup/ui/r;

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->a:Lcom/google/android/libraries/social/b/a;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/b/a;->a(Ljava/lang/Runnable;)Lcom/google/android/libraries/social/b/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 87
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    monitor-exit p0

    return-void

    .line 87
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/t;->d()V

    .line 98
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/a/a;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 92
    const-class v0, Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/l;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/t;->b:Lcom/google/android/gms/photos/autobackup/l;

    .line 93
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/ui/t;->c()V

    .line 103
    return-void
.end method
