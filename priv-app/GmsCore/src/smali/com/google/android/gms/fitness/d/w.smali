.class final Lcom/google/android/gms/fitness/d/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/v;

.field final synthetic b:Lcom/google/android/gms/fitness/d/t;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/d/t;Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/gms/fitness/d/w;->b:Lcom/google/android/gms/fitness/d/t;

    iput-object p2, p0, Lcom/google/android/gms/fitness/d/w;->a:Lcom/google/android/gms/common/api/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 2

    .prologue
    .line 228
    check-cast p1, Lcom/google/android/gms/wearable/l;

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/l;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/common/data/v;->a(Ljava/lang/Iterable;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/d/w;->b:Lcom/google/android/gms/fitness/d/t;

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/d/t;->a(Lcom/google/android/gms/fitness/d/t;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/l;->w_()V

    iget-object v0, p0, Lcom/google/android/gms/fitness/d/w;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WearableAccountManager error getting dataItem: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/l;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/l;->w_()V

    iget-object v1, p0, Lcom/google/android/gms/fitness/d/w;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    throw v0
.end method
