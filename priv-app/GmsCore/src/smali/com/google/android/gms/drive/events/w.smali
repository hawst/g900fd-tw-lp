.class public final Lcom/google/android/gms/drive/events/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/database/r;

.field public final b:Lcom/google/android/gms/drive/database/i;

.field public final c:Lcom/google/android/gms/drive/g/i;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/drive/events/u;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/events/u;Lcom/google/android/gms/drive/g/i;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/w;->d:Landroid/content/Context;

    .line 49
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/w;->b:Lcom/google/android/gms/drive/database/i;

    .line 50
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    .line 51
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/events/u;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/w;->e:Lcom/google/android/gms/drive/events/u;

    .line 52
    iput-object p5, p0, Lcom/google/android/gms/drive/events/w;->c:Lcom/google/android/gms/drive/g/i;

    .line 53
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/events/DriveEvent;)Lcom/google/android/gms/drive/database/b/d;
    .locals 6

    .prologue
    .line 147
    instance-of v0, p1, Lcom/google/android/gms/drive/events/ResourceEvent;

    const-string v1, "Only resource subscriptions are currently supported"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    move-object v0, p1

    .line 149
    check-cast v0, Lcom/google/android/gms/drive/events/ResourceEvent;

    invoke-interface {v0}, Lcom/google/android/gms/drive/events/ResourceEvent;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    .line 150
    invoke-interface {p1}, Lcom/google/android/gms/drive/events/DriveEvent;->a()I

    move-result v2

    .line 152
    if-eqz v1, :cond_0

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 153
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v3, v0, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/b/d;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 152
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v3, 0x8

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to query subscriptions: id = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", type = "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/events/DriveEvent;Lcom/google/android/gms/drive/database/model/ah;)V
    .locals 7

    .prologue
    .line 113
    const/4 v1, 0x0

    .line 115
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/events/w;->a(Lcom/google/android/gms/drive/events/DriveEvent;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v1

    .line 116
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bv;

    .line 117
    iget-object v3, p0, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/bv;->a:J

    invoke-interface {v3, v4, v5}, Lcom/google/android/gms/drive/database/r;->f(J)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v3

    .line 119
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 120
    iget-wide v4, v3, Lcom/google/android/gms/drive/database/model/f;->a:J

    .line 121
    iget-object v6, p0, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v3, v3, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v6, v4, v5, v3}, Lcom/google/android/gms/drive/database/r;->a(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v3

    .line 123
    if-eqz v3, :cond_2

    .line 124
    invoke-virtual {p2, v3}, Lcom/google/android/gms/drive/database/model/ah;->b(Lcom/google/android/gms/drive/auth/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, v3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/drive/events/w;->e:Lcom/google/android/gms/drive/events/u;

    invoke-virtual {v3, v0, p1}, Lcom/google/android/gms/drive/events/u;->a(Ljava/lang/String;Lcom/google/android/gms/drive/events/DriveEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v3, "SubscriptionStore"

    const-string v4, "Error raising event to one subscriber"

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 131
    :catch_1
    move-exception v0

    .line 132
    :try_start_3
    const-string v2, "SubscriptionStore"

    const-string v3, "Error raising event to subscribers"

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 134
    if-eqz v1, :cond_1

    .line 135
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 138
    :cond_1
    :goto_1
    return-void

    .line 128
    :cond_2
    :try_start_4
    const-string v3, "SubscriptionStore"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No authorized app found for subscription: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 135
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    :cond_3
    throw v0

    .line 134
    :cond_4
    if-eqz v1, :cond_1

    .line 135
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    goto :goto_1
.end method
