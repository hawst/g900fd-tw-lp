.class abstract Lcom/google/android/gms/people/service/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/service/a/q;


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/service/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/service/a/f;)V
    .locals 0

    .prologue
    .line 503
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/n;->b:Lcom/google/android/gms/people/service/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/service/a/f;B)V
    .locals 0

    .prologue
    .line 503
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/a/n;-><init>(Lcom/google/android/gms/people/service/a/f;)V

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/gms/people/a/b;
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Landroid/os/Bundle;)Z
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 507
    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/n;->a()Lcom/google/android/gms/people/a/b;

    move-result-object v1

    .line 508
    if-nez v1, :cond_0

    move v0, v7

    .line 578
    :goto_0
    return v0

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/n;->b:Lcom/google/android/gms/people/service/a/f;

    invoke-static {p1, v1}, Lcom/google/android/gms/people/service/a/f;->a(Landroid/content/Context;Lcom/google/android/gms/people/a/b;)Lcom/google/android/gms/people/service/g;

    move-result-object v0

    .line 513
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/n;->b:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v2}, Lcom/google/android/gms/people/service/a/f;->b(Lcom/google/android/gms/people/service/a/f;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v9, v2, [Landroid/os/Bundle;

    .line 516
    :try_start_0
    iget-object v2, v1, Lcom/google/android/gms/people/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 517
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/g;->c()V

    :cond_1
    move v8, v7

    move v3, v7

    .line 521
    :goto_1
    array-length v2, v9

    if-ge v8, v2, :cond_2

    .line 522
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/n;->b:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v2}, Lcom/google/android/gms/people/service/a/f;->b(Lcom/google/android/gms/people/service/a/f;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/n;->b:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v4}, Lcom/google/android/gms/people/service/a/f;->c(Lcom/google/android/gms/people/service/a/f;)Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->f()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {p0, v2, v4}, Lcom/google/android/gms/people/service/a/n;->a(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v2

    .line 525
    if-eqz v2, :cond_9

    .line 526
    add-int/lit8 v6, v3, 0x1

    .line 527
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/n;->b:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v2}, Lcom/google/android/gms/people/service/a/f;->c(Lcom/google/android/gms/people/service/a/f;)Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->f()Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    new-instance v4, Lcom/google/android/gms/people/service/a/o;

    invoke-direct {v4, p0, v10, v1}, Lcom/google/android/gms/people/service/a/o;-><init>(Lcom/google/android/gms/people/service/a/n;Landroid/os/Bundle;Lcom/google/android/gms/people/a/b;)V

    new-instance v5, Lcom/google/android/gms/people/service/a/p;

    invoke-direct {v5, p0}, Lcom/google/android/gms/people/service/a/p;-><init>(Lcom/google/android/gms/people/service/a/n;)V

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/d;->a(Lcom/google/android/gms/people/service/g;Lcom/google/android/gms/people/a/b;Landroid/os/Bundle;Lcom/google/android/gms/common/server/ClientContext;Lcom/android/volley/x;Lcom/android/volley/w;)V

    aput-object v10, v9, v8

    move v2, v6

    .line 521
    :goto_2
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    move v3, v2

    goto :goto_1

    .line 535
    :cond_2
    if-lez v3, :cond_7

    .line 536
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 537
    const-string v2, "PeopleService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Loading data from endpoint \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/google/android/gms/people/a/b;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    :cond_3
    const-string v2, "get.server_blob"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 542
    const-string v2, "get.server_blob"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    move v3, v7

    .line 545
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v3, v2, :cond_5

    .line 546
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    .line 547
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Landroid/os/Bundle;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 548
    aput-object v2, v9, v3

    .line 545
    :cond_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 553
    :cond_5
    iget-object v1, v1, Lcom/google/android/gms/people/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 554
    sget-object v1, Lcom/google/android/gms/people/a/a;->aB:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v2, v1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/people/service/g;->a(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 556
    if-nez v0, :cond_6

    .line 557
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    .line 576
    :catch_0
    move-exception v0

    .line 577
    const-string v1, "PeopleService"

    const-string v2, "Request Timed Out"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v7

    .line 578
    goto/16 :goto_0

    .line 561
    :cond_6
    :try_start_1
    const-string v0, "get.server_blob"

    new-instance v1, Ljava/util/ArrayList;

    invoke-static {v9}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 564
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 566
    :cond_7
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 567
    const-string v2, "PeopleService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No data to load from endpoint \'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/android/gms/people/a/b;->a:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/g;->d()V
    :try_end_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_0

    move v0, v7

    .line 574
    goto/16 :goto_0

    :cond_9
    move v2, v3

    goto/16 :goto_2
.end method

.method protected abstract a(Ljava/lang/String;Landroid/os/Bundle;)Z
.end method
