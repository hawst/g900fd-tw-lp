.class final Lcom/google/android/gms/wallet/service/ia/ae;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ia/ad;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/ae;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ia/ae;->c:Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ia/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 11

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ae;->c:Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->b()Lcom/google/checkout/inapp/proto/aa;

    move-result-object v0

    .line 48
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    .line 49
    iget v2, v1, Lcom/google/checkout/a/a/a/d;->a:I

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 50
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Create instrument currently only supports credit cards."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    iget-object v2, v1, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v2, v2, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iget-object v6, v2, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    .line 55
    iget-object v1, v1, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iget-object v7, v1, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    .line 56
    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v5

    check-cast v5, Lcom/google/checkout/inapp/proto/aa;

    .line 58
    iget-object v0, v5, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    iget-object v0, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ae;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/service/ia/ad;)Lcom/google/android/gms/wallet/service/ia/o;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ae;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ae;->a:Landroid/accounts/Account;

    iget-object v8, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ae;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v9

    iget-object v0, v5, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/o;->a(Lcom/google/checkout/a/a/a/d;)V

    iget-object v10, v1, Lcom/google/android/gms/wallet/service/ia/o;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/w;

    move-object v4, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/wallet/service/ia/w;-><init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/checkout/inapp/proto/aa;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "create_instrument"

    invoke-static {v10, v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method
