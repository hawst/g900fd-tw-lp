.class final Lcom/google/android/gms/fitness/service/b;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    .line 112
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 113
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/b;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/fitness/service/a;
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/fitness/service/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/fitness/service/a;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/fitness/service/a;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-static {v0}, Lcom/google/android/gms/fitness/service/BrokeredFitnessService;->b(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;)Lcom/google/android/gms/fitness/h/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->e()Lcom/google/android/gms/fitness/d/h;

    move-result-object v0

    .line 154
    :try_start_0
    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/fitness/d/h;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/fitness/d/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 160
    if-nez v1, :cond_1

    .line 161
    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/d/h;->c(Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    .line 163
    if-nez v2, :cond_0

    const/4 v0, 0x5

    .line 165
    :goto_0
    new-instance v1, Lcom/google/android/gms/fitness/service/a;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-direct {v1, v3, v0, v8, v2}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;ILandroid/os/IBinder;Landroid/app/PendingIntent;)V

    move-object v0, v1

    .line 208
    :goto_1
    return-object v0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    const-string v1, "BrokeredFitnessService"

    const-string v2, "Resolving account name failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 157
    new-instance v0, Lcom/google/android/gms/fitness/service/a;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;I)V

    goto :goto_1

    .line 163
    :cond_0
    const/4 v0, 0x4

    goto :goto_0

    .line 169
    :cond_1
    const-string v2, "com.google.android.gms"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-static {v0}, Lcom/google/android/gms/fitness/service/BrokeredFitnessService;->c(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;)Lcom/google/android/gms/fitness/service/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/service/e;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/service/i;

    move-result-object v1

    .line 172
    new-instance v0, Lcom/google/android/gms/fitness/service/a;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/service/i;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-direct {v0, v2, v6, v1}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;ILandroid/os/IBinder;)V

    goto :goto_1

    .line 176
    :cond_2
    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/d/h;->d(Ljava/lang/String;)Lcom/google/android/gms/fitness/d/d;

    move-result-object v2

    .line 177
    invoke-interface {v2, p2, p3}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v2

    .line 178
    iget-boolean v3, v2, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v3, :cond_4

    .line 179
    const-string v3, "unable to authenticate: %s package: %s scopes: %s"

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    aput-object p2, v4, v5

    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 181
    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/fitness/d/h;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 182
    iget-object v0, v2, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    .line 183
    if-eqz v0, :cond_3

    .line 184
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/b;->b:Landroid/content/Context;

    const/high16 v2, 0x8000000

    invoke-static {v1, v6, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 187
    new-instance v0, Lcom/google/android/gms/fitness/service/a;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    const/16 v3, 0x1388

    invoke-direct {v0, v2, v3, v8, v1}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;ILandroid/os/IBinder;Landroid/app/PendingIntent;)V

    goto :goto_1

    .line 190
    :cond_3
    new-instance v0, Lcom/google/android/gms/fitness/service/a;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    const/16 v2, 0x138d

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;I)V

    goto :goto_1

    .line 192
    :cond_4
    const-string v2, "authenticated: %s package: %s scopes: %s"

    new-array v3, v4, [Ljava/lang/Object;

    aput-object v1, v3, v6

    aput-object p2, v3, v5

    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 194
    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/fitness/d/h;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    .line 197
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-static {v0}, Lcom/google/android/gms/fitness/service/BrokeredFitnessService;->c(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;)Lcom/google/android/gms/fitness/service/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/service/e;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 198
    new-instance v0, Lcom/google/android/gms/fitness/service/a;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    const/16 v3, 0x1395

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;I)V
    :try_end_1
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 200
    :catch_1
    move-exception v0

    const-string v2, "Failed to determine if account %s supported"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 203
    new-instance v0, Lcom/google/android/gms/fitness/service/a;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;I)V

    goto/16 :goto_1

    .line 206
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-static {v0}, Lcom/google/android/gms/fitness/service/BrokeredFitnessService;->c(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;)Lcom/google/android/gms/fitness/service/e;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/service/e;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/service/i;

    move-result-object v1

    .line 208
    new-instance v0, Lcom/google/android/gms/fitness/service/a;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/service/i;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-direct {v0, v2, v6, v1}, Lcom/google/android/gms/fitness/service/a;-><init>(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;ILandroid/os/IBinder;)V

    goto/16 :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/fitness/service/a;)V
    .locals 4

    .prologue
    .line 109
    const/4 v0, 0x0

    iget-object v1, p1, Lcom/google/android/gms/fitness/service/a;->c:Landroid/app/PendingIntent;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "pendingIntent"

    iget-object v2, p1, Lcom/google/android/gms/fitness/service/a;->c:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    :try_start_0
    iget v1, p1, Lcom/google/android/gms/fitness/service/a;->a:I

    iget-object v2, p1, Lcom/google/android/gms/fitness/service/a;->b:Landroid/os/IBinder;

    invoke-interface {p0, v1, v2, v0}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Could not send response back to caller: "

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/b;->a:Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-static {v0}, Lcom/google/android/gms/fitness/service/BrokeredFitnessService;->a(Lcom/google/android/gms/fitness/service/BrokeredFitnessService;)Landroid/os/Handler;

    move-result-object v6

    new-instance v0, Lcom/google/android/gms/fitness/service/c;

    move-object v1, p0

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/service/c;-><init>(Lcom/google/android/gms/fitness/service/b;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/gms/common/internal/bg;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 130
    return-void
.end method
