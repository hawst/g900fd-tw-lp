.class final Lcom/google/android/gms/wallet/ow/w;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/v;

.field private b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/ow/v;Landroid/content/Context;[Lcom/google/aa/b/a/h;)V
    .locals 1

    .prologue
    .line 112
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/w;->a:Lcom/google/android/gms/wallet/ow/v;

    .line 113
    invoke-static {p1}, Lcom/google/android/gms/wallet/ow/v;->a(Lcom/google/android/gms/wallet/ow/v;)I

    move-result v0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 114
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/w;->b:Landroid/view/LayoutInflater;

    .line 115
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/aa/b/a/h;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 168
    if-nez p1, :cond_0

    move v0, v1

    .line 176
    :goto_0
    return v0

    .line 171
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/w;->getCount()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_2

    .line 172
    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/ow/w;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/h;

    iget-object v0, v0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 173
    goto :goto_0

    .line 171
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 176
    goto :goto_0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 180
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/ow/w;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/h;

    .line 181
    const-string v1, "dont_send_loyalty_wob_id"

    iget-object v2, v0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 184
    iget-object v0, v0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    .line 197
    :goto_0
    return-object v0

    .line 186
    :cond_0
    iget-object v1, v0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 187
    iget-object v1, v0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 188
    const-string v1, "%s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v0, v0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 191
    :cond_1
    iget-object v0, v0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    goto :goto_0

    .line 194
    :cond_2
    iget-object v0, v0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 119
    if-nez p2, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/w;->b:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/w;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/v;->b(Lcom/google/android/gms/wallet/ow/v;)I

    move-result v1

    invoke-virtual {v0, v1, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 123
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/ow/w;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/h;

    .line 124
    sget v1, Lcom/google/android/gms/j;->e:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 125
    iget-object v2, v0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 126
    iget-object v2, v0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 127
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 133
    :goto_0
    sget v1, Lcom/google/android/gms/j;->pP:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 134
    iget-object v2, v0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 135
    iget-object v0, v0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 142
    :goto_1
    return-object p2

    .line 129
    :cond_1
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 130
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 138
    :cond_2
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 139
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 163
    int-to-long v0, p1

    return-wide v0
.end method

.method public final synthetic getPosition(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 109
    check-cast p1, Lcom/google/aa/b/a/h;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/ow/w;->a(Lcom/google/aa/b/a/h;)I

    move-result v0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 152
    if-nez p2, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/w;->b:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/w;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/v;->a(Lcom/google/android/gms/wallet/ow/v;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 155
    :cond_0
    sget v0, Lcom/google/android/gms/j;->uj:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 156
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/ow/w;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/w;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/v;->c(Lcom/google/android/gms/wallet/ow/v;)Landroid/widget/AdapterView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AdapterView;->isEnabled()Z

    move-result v0

    invoke-static {p2, v0}, Lcom/google/android/gms/common/util/aw;->a(Landroid/view/View;Z)V

    .line 158
    return-object p2
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 147
    const/4 v0, 0x1

    return v0
.end method
