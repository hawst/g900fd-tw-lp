.class final Lcom/google/android/gms/plus/sharebox/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/ae;

.field private final b:Lcom/google/android/gms/plus/sharebox/aa;

.field private final c:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/sharebox/ae;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lcom/google/android/gms/plus/sharebox/aa;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/aa;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->b:Lcom/google/android/gms/plus/sharebox/aa;

    .line 68
    sget v0, Lcom/google/android/gms/g;->bJ:I

    invoke-virtual {p2, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/af;->c:I

    .line 70
    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/ae;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->a(Lcom/google/android/gms/plus/sharebox/ae;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->d(Lcom/google/android/gms/plus/sharebox/ae;)Z

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->e(Lcom/google/android/gms/plus/sharebox/ae;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->f(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->c()Lcom/google/android/gms/plus/sharebox/ah;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->g(Lcom/google/android/gms/plus/sharebox/ae;)Z

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->f(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/ag;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/ag;->f()V

    goto :goto_0
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 74
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/ae;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->a(Lcom/google/android/gms/plus/sharebox/ae;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    if-nez v0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return-void

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getSelectionEnd()I

    move-result v0

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/af;->b:Lcom/google/android/gms/plus/sharebox/aa;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/plus/sharebox/aa;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getThreshold()I

    move-result v2

    add-int/2addr v1, v2

    if-gt v1, v0, :cond_0

    .line 85
    const/4 v0, 0x2

    new-array v0, v0, [I

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getLocationOnScreen([I)V

    .line 87
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 88
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/ae;->a(Lcom/google/android/gms/plus/sharebox/ae;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 90
    const/4 v2, 0x1

    aget v0, v0, v2

    .line 91
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a()I

    move-result v2

    add-int/2addr v0, v2

    .line 92
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    .line 94
    sub-int v0, v1, v0

    iget v1, p0, Lcom/google/android/gms/plus/sharebox/af;->c:I

    if-ge v0, v1, :cond_0

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ae;->c(Lcom/google/android/gms/plus/sharebox/ae;)Landroid/widget/ScrollView;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/af;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/ae;->b(Lcom/google/android/gms/plus/sharebox/ae;)Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->b()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ScrollView;->smoothScrollTo(II)V

    goto :goto_0
.end method
