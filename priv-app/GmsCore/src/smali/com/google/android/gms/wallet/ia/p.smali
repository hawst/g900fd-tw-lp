.class public final Lcom/google/android/gms/wallet/ia/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Ljava/util/Map;

.field private c:Ljava/util/Map;

.field private d:Z

.field private e:[Lcom/google/checkout/inapp/proto/a/b;

.field private f:[Lcom/google/checkout/inapp/proto/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/wallet/ia/p;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/n;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 130
    if-nez p2, :cond_0

    move-object v0, v2

    .line 142
    :goto_0
    return-object v0

    .line 133
    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    move-object v1, v0

    .line 134
    :goto_1
    iget-object v3, p2, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/p;->d:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v2

    .line 137
    goto :goto_0

    .line 133
    :cond_1
    sget-object v0, Lcom/google/android/gms/wallet/ia/p;->a:Ljava/lang/String;

    move-object v1, v0

    goto :goto_1

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/n;

    goto :goto_0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/n;
    .locals 2

    .prologue
    .line 114
    if-nez p1, :cond_0

    .line 115
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    .line 117
    :cond_0
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    .line 118
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/p;->d:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/n;

    goto :goto_0

    .line 118
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a([Lcom/google/checkout/inapp/proto/n;Z)V
    .locals 7

    .prologue
    .line 42
    iput-boolean p2, p0, Lcom/google/android/gms/wallet/ia/p;->d:Z

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    .line 58
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->c:Ljava/util/Map;

    .line 59
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 60
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 61
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_4

    .line 62
    aget-object v6, p1, v1

    .line 63
    const/4 v0, 0x0

    .line 64
    iget-object v2, v6, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v2, :cond_7

    .line 65
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    move-object v3, v0

    .line 67
    :goto_1
    if-eqz p2, :cond_3

    .line 68
    sget-object v0, Lcom/google/android/gms/wallet/ia/p;->a:Ljava/lang/String;

    .line 69
    iget-object v2, v6, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_6

    .line 70
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    move-object v2, v0

    .line 74
    :goto_2
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_0
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 86
    :goto_3
    invoke-interface {v0, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v3, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    :cond_1
    :goto_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 82
    :cond_2
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    goto :goto_3

    .line 90
    :cond_3
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->c:Ljava/util/Map;

    invoke-interface {v0, v3, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    .line 96
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 97
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    .line 101
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->c:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->c:Ljava/util/Map;

    .line 105
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    .line 107
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->f:[Lcom/google/checkout/inapp/proto/j;

    .line 108
    return-void

    :cond_6
    move-object v2, v0

    goto/16 :goto_2

    :cond_7
    move-object v3, v0

    goto/16 :goto_1
.end method

.method public final a()[Lcom/google/checkout/inapp/proto/a/b;
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/p;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    return-object v0

    .line 149
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;)[Lcom/google/checkout/inapp/proto/j;
    .locals 5

    .prologue
    .line 166
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/p;->d:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 167
    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    .line 170
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 171
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/p;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v3, v0, [Lcom/google/checkout/inapp/proto/j;

    const/4 v0, 0x0

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/n;

    add-int/lit8 v2, v1, 0x1

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    aput-object v0, v3, v1

    move v1, v2

    goto :goto_1

    .line 167
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/ia/p;->a:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 174
    :goto_2
    return-object v0

    :cond_2
    invoke-static {}, Lcom/google/checkout/inapp/proto/j;->a()[Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    goto :goto_2
.end method

.method public final b()[Lcom/google/checkout/inapp/proto/j;
    .locals 1

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/p;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->f:[Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/p;->f:[Lcom/google/checkout/inapp/proto/j;

    return-object v0

    .line 157
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
