.class public final Lcom/google/android/gms/ads/internal/request/t;
.super Lcom/google/android/gms/ads/internal/request/s;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/r;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/ads/internal/request/s;-><init>(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/r;)V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/t;->a:Landroid/content/Context;

    .line 45
    return-void
.end method


# virtual methods
.method public final c()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method public final d()Lcom/google/android/gms/ads/internal/request/z;
    .locals 8

    .prologue
    .line 49
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->i()Landroid/os/Bundle;

    move-result-object v6

    .line 50
    new-instance v0, Lcom/google/android/gms/ads/internal/d/a;

    const-string v1, "gads:sdk_core_location"

    invoke-virtual {v6, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "gads:sdk_core_experiment_id"

    invoke-virtual {v6, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "gads:block_autoclicks_experiment_id"

    invoke-virtual {v6, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "gads:spam_app_context:experiment_id"

    invoke-virtual {v6, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "gads:request_builder:singleton_webview"

    invoke-virtual {v6, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    const-string v7, "gads:request_builder:singleton_webview_experiment_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/ads/internal/d/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 60
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/t;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/ads/internal/j/b;

    invoke-direct {v2}, Lcom/google/android/gms/ads/internal/j/b;-><init>()V

    new-instance v3, Lcom/google/android/gms/ads/internal/n/b;

    invoke-direct {v3}, Lcom/google/android/gms/ads/internal/n/b;-><init>()V

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/ads/internal/request/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/j/a;Lcom/google/android/gms/ads/internal/n/a;)Lcom/google/android/gms/ads/internal/request/a/a;

    move-result-object v0

    return-object v0
.end method
