.class public final Lcom/google/android/gms/googlehelp/c/t;
.super Landroid/widget/RatingBar;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/af;
.implements Lcom/google/android/gms/googlehelp/c/c;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:[Lcom/google/ad/a/a/k;

.field private final b:Landroid/widget/TextView;

.field private final c:Z

.field private d:Lcom/google/android/gms/googlehelp/c/d;

.field private e:Lcom/google/android/gms/googlehelp/c/ac;

.field private f:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/k;[Lcom/google/ad/a/a/k;Landroid/widget/TextView;)V
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 58
    invoke-direct {p0, p1}, Landroid/widget/RatingBar;-><init>(Landroid/content/Context;)V

    .line 60
    iget-object v0, p3, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/t;->setTag(Ljava/lang/Object;)V

    .line 61
    array-length v0, p4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/t;->setNumStars(I)V

    .line 62
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/t;->setStepSize(F)V

    .line 63
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/t;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 64
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/c/t;->a:[Lcom/google/ad/a/a/k;

    .line 65
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/c/t;->b:Landroid/widget/TextView;

    .line 67
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/t;->d:Lcom/google/android/gms/googlehelp/c/d;

    .line 68
    iget-boolean v0, p3, Lcom/google/ad/a/a/k;->g:Z

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/t;->c:Z

    .line 70
    new-instance v0, Lcom/google/android/gms/googlehelp/c/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/c/u;-><init>(Lcom/google/android/gms/googlehelp/c/t;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/t;->setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/c/t;)Lcom/google/android/gms/googlehelp/c/ac;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->e:Lcom/google/android/gms/googlehelp/c/ac;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->a:[Lcom/google/ad/a/a/k;

    array-length v0, v0

    if-le p1, v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->a:[Lcom/google/ad/a/a/k;

    array-length p1, v0

    .line 86
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/t;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->oE:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 87
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->a:[Lcom/google/ad/a/a/k;

    add-int/lit8 v2, p1, -0x1

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->a:[Lcom/google/ad/a/a/k;

    add-int/lit8 v2, p1, -0x1

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 90
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/t;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    return-void

    .line 87
    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/googlehelp/c/ac;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/t;->e:Lcom/google/android/gms/googlehelp/c/ac;

    .line 141
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/t;->f:Ljava/util/List;

    .line 96
    new-instance v0, Lcom/google/android/gms/googlehelp/c/v;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/c/v;-><init>(Lcom/google/android/gms/googlehelp/c/t;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/t;->setOnRatingBarChangeListener(Landroid/widget/RatingBar$OnRatingBarChangeListener;)V

    .line 103
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/t;->getRating()F

    move-result v0

    float-to-int v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->d:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/d;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 124
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/t;->c:Z

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/t;->getRating()F

    move-result v0

    float-to-int v0, v0

    .line 120
    if-gtz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/t;->a:[Lcom/google/ad/a/a/k;

    add-int/lit8 v0, v0, -0x1

    aget-object v0, v1, v0

    iget-object v0, v0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->f:Ljava/util/List;

    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->f:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/c/ad;->a(Ljava/util/List;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->e:Lcom/google/android/gms/googlehelp/c/ac;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/t;->e:Lcom/google/android/gms/googlehelp/c/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/ac;->a()V

    goto :goto_0
.end method
