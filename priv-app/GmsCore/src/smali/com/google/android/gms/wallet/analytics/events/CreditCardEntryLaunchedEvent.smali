.class public Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Ljava/lang/String;

.field public final d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/b;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->c:Ljava/lang/String;

    .line 39
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 40
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 28
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->j:Ljava/lang/String;

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->c:Ljava/lang/String;

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->a:Ljava/lang/String;

    .line 32
    invoke-virtual {p0, p2}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    .line 33
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;-><init>(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V

    .line 46
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 47
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 60
    return-void
.end method
