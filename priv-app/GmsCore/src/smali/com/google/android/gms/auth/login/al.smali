.class public final Lcom/google/android/gms/auth/login/al;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/login/al;-><init>(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/login/LoginActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 268
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "request_type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 186
    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "options"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 136
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "calling_app_description"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 100
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/FACLConfig;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "facl"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 154
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "pacl"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 145
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 171
    invoke-static {p1}, Lcom/google/android/gms/auth/a/h;->a(Lcom/google/android/gms/auth/firstparty/shared/k;)Lcom/google/android/gms/auth/a/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/h;->a(Landroid/os/Bundle;)V

    .line 172
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    return-object p0
.end method

.method public final a(Ljava/util/ArrayList;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "allowed_domains"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 213
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "suppress_progress_screen"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "url"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/auth/login/al;
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "is_browser_only"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 95
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "url"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "is_setup_wizard"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 195
    return-object p0
.end method

.method public final c()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "calling_app_description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    return-object v0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    return-object p0
.end method

.method public final c(Z)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "allow_credit_card"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 231
    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/auth/login/al;
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "service"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    return-object p0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "suppress_progress_screen"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "service"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "pacl"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/PACLConfig;

    return-object v0
.end method

.method public final h()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "grant_credential_response_status"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    return-object v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 180
    iget-object v1, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v2, "request_type"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final j()I
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "request_type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final k()Lcom/google/android/gms/auth/login/al;
    .locals 3

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "is_minute_maid"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 204
    return-object p0
.end method

.method public final l()Z
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/auth/login/al;->a:Landroid/os/Bundle;

    const-string v1, "is_minute_maid"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method
