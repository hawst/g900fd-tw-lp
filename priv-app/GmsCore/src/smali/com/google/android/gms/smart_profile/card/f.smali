.class public final Lcom/google/android/gms/smart_profile/card/f;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;
.implements Lcom/google/android/gms/smart_profile/card/a/c;
.implements Lcom/google/android/gms/smart_profile/card/h;


# instance fields
.field private a:Lcom/google/android/gms/smart_profile/card/g;

.field private b:Landroid/view/ViewGroup;

.field private final c:Ljava/util/ArrayList;

.field private d:Ljava/util/ArrayList;

.field private e:I

.field private f:Lcom/google/android/gms/people/identity/models/Person;

.field private g:Lcom/google/android/gms/smart_profile/card/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->c:Ljava/util/ArrayList;

    .line 67
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/f;->e:I

    return-void
.end method

.method public static a()Lcom/google/android/gms/smart_profile/card/f;
    .locals 1

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/gms/smart_profile/card/f;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/card/f;-><init>()V

    return-object v0
.end method

.method private d()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v7, 0x0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "smartProfileUtilFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/bg;

    .line 163
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    if-nez v1, :cond_3

    move-object v5, v4

    :cond_0
    move v6, v7

    .line 164
    :goto_0
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-ge v6, v1, :cond_1c

    .line 165
    invoke-interface {v5, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ac/c/a/a/a/f;

    .line 166
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v6, v2, :cond_e

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/Bundle;

    move-object v8, v2

    .line 167
    :goto_1
    sget-object v2, Lcom/google/android/gms/smart_profile/card/c;->a:Lcom/google/android/gms/smart_profile/card/c;

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/gms/smart_profile/card/c;

    invoke-direct {v2}, Lcom/google/android/gms/smart_profile/card/c;-><init>()V

    sput-object v2, Lcom/google/android/gms/smart_profile/card/c;->a:Lcom/google/android/gms/smart_profile/card/c;

    :cond_1
    sget-object v2, Lcom/google/android/gms/smart_profile/card/c;->a:Lcom/google/android/gms/smart_profile/card/c;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/f;->b:Landroid/view/ViewGroup;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget-object v9, v1, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    if-eqz v9, :cond_f

    sget v9, Lcom/google/android/gms/l;->fa:I

    invoke-virtual {v2, v9, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    :goto_2
    instance-of v2, v3, Landroid/support/v7/widget/CardView;

    if-eqz v2, :cond_1a

    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    if-eqz v2, :cond_15

    new-instance v9, Lcom/google/android/gms/smart_profile/card/a/d;

    invoke-direct {v9}, Lcom/google/android/gms/smart_profile/card/a/d;-><init>()V

    move-object v2, v3

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v9, v1, v2}, Lcom/google/android/gms/smart_profile/card/a/d;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object v1, v9

    :goto_3
    if-eqz v1, :cond_1b

    iput-object p0, v1, Lcom/google/android/gms/smart_profile/card/a/b;->c:Lcom/google/android/gms/smart_profile/card/h;

    invoke-virtual {v1, p0, v8, v0}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/a/b;->a()Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Landroid/support/v4/app/au;)V

    .line 168
    :goto_4
    if-eqz v3, :cond_2

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/f;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 164
    :cond_2
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_0

    .line 163
    :cond_3
    iget-object v8, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    invoke-virtual {v8}, Lcom/google/android/gms/smart_profile/card/d;->b()Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v8, Lcom/google/android/gms/smart_profile/card/d;->a:[Lcom/google/ac/c/a/a/a/f;

    if-eqz v1, :cond_4

    iget-object v2, v8, Lcom/google/android/gms/smart_profile/card/d;->a:[Lcom/google/ac/c/a/a/a/f;

    array-length v3, v2

    move v1, v7

    :goto_5
    if-ge v1, v3, :cond_4

    aget-object v6, v2, v1

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_4
    iget-object v1, v8, Lcom/google/android/gms/smart_profile/card/d;->d:Ljava/lang/String;

    if-eqz v1, :cond_1e

    invoke-virtual {v8}, Lcom/google/android/gms/smart_profile/card/d;->e()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v8}, Lcom/google/android/gms/smart_profile/card/d;->c()Lcom/google/ac/c/a/a/a/f;

    move-result-object v1

    :goto_6
    invoke-virtual {v8}, Lcom/google/android/gms/smart_profile/card/d;->f()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {v8}, Lcom/google/android/gms/smart_profile/card/d;->d()Ljava/util/List;

    move-result-object v2

    :goto_7
    invoke-static {v2}, Lcom/google/android/gms/smart_profile/card/d;->c(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/smart_profile/card/d;->a(Lcom/google/ac/c/a/a/a/f;Lcom/google/ac/c/a/a/a/f;)Lcom/google/ac/c/a/a/a/f;

    move-result-object v1

    move-object v3, v2

    move-object v2, v1

    :goto_8
    iget-object v1, v8, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v9

    move v6, v7

    :goto_9
    if-ge v6, v9, :cond_0

    iget-object v1, v8, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    invoke-interface {v1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ac/c/a/a/a/g;

    if-eqz v1, :cond_d

    iget-object v10, v1, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    if-eqz v10, :cond_8

    new-instance v1, Lcom/google/ac/c/a/a/a/f;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/f;-><init>()V

    new-instance v10, Lcom/google/ac/c/a/a/a/b;

    invoke-direct {v10}, Lcom/google/ac/c/a/a/a/b;-><init>()V

    iput-object v10, v1, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    :goto_a
    if-eqz v1, :cond_5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    goto :goto_9

    :cond_6
    move-object v1, v4

    goto :goto_6

    :cond_7
    move-object v2, v4

    goto :goto_7

    :cond_8
    iget-object v10, v1, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    if-eqz v10, :cond_9

    new-instance v1, Lcom/google/ac/c/a/a/a/f;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/f;-><init>()V

    new-instance v10, Lcom/google/ac/c/a/a/a/j;

    invoke-direct {v10}, Lcom/google/ac/c/a/a/a/j;-><init>()V

    iput-object v10, v1, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    goto :goto_a

    :cond_9
    iget-object v10, v8, Lcom/google/android/gms/smart_profile/card/d;->e:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_a

    iget-object v10, v1, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    if-eqz v10, :cond_a

    iget-object v1, v8, Lcom/google/android/gms/smart_profile/card/d;->e:Ljava/util/List;

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ac/c/a/a/a/f;

    goto :goto_a

    :cond_a
    iget-object v10, v1, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    if-eqz v10, :cond_b

    if-eqz v2, :cond_b

    move-object v1, v2

    goto :goto_a

    :cond_b
    if-eqz v3, :cond_d

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_d

    iget-object v10, v1, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    if-eqz v10, :cond_c

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/card/d;->a(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;

    move-result-object v1

    goto :goto_a

    :cond_c
    iget-object v1, v1, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    if-eqz v1, :cond_d

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/card/d;->b(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;

    move-result-object v1

    goto :goto_a

    :cond_d
    move-object v1, v4

    goto :goto_a

    :cond_e
    move-object v8, v4

    .line 166
    goto/16 :goto_1

    .line 167
    :cond_f
    iget-object v9, v1, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-eqz v9, :cond_10

    sget v9, Lcom/google/android/gms/l;->fa:I

    invoke-virtual {v2, v9, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_2

    :cond_10
    iget-object v9, v1, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    if-eqz v9, :cond_11

    sget v9, Lcom/google/android/gms/l;->fe:I

    invoke-virtual {v2, v9, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_2

    :cond_11
    iget-object v9, v1, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    if-eqz v9, :cond_12

    sget v9, Lcom/google/android/gms/l;->fg:I

    invoke-virtual {v2, v9, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_2

    :cond_12
    iget-object v9, v1, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    if-eqz v9, :cond_13

    sget v9, Lcom/google/android/gms/l;->fa:I

    invoke-virtual {v2, v9, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_2

    :cond_13
    iget-object v9, v1, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    if-eqz v9, :cond_14

    sget v9, Lcom/google/android/gms/l;->fa:I

    invoke-virtual {v2, v9, v3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    goto/16 :goto_2

    :cond_14
    const-string v2, "CardFactory"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v9, "No card view created for "

    invoke-direct {v3, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v3, v4

    goto/16 :goto_2

    :cond_15
    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    if-eqz v2, :cond_16

    new-instance v9, Lcom/google/android/gms/smart_profile/card/a/g;

    invoke-direct {v9}, Lcom/google/android/gms/smart_profile/card/a/g;-><init>()V

    move-object v2, v3

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v9, v1, v2}, Lcom/google/android/gms/smart_profile/card/a/g;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object v1, v9

    goto/16 :goto_3

    :cond_16
    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    if-eqz v2, :cond_17

    new-instance v9, Lcom/google/android/gms/smart_profile/card/a/j;

    invoke-direct {v9}, Lcom/google/android/gms/smart_profile/card/a/j;-><init>()V

    move-object v2, v3

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v9, v1, v2}, Lcom/google/android/gms/smart_profile/card/a/j;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object v1, v9

    goto/16 :goto_3

    :cond_17
    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-eqz v2, :cond_18

    new-instance v9, Lcom/google/android/gms/smart_profile/card/a/f;

    invoke-direct {v9}, Lcom/google/android/gms/smart_profile/card/a/f;-><init>()V

    move-object v2, v3

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v9, v1, v2}, Lcom/google/android/gms/smart_profile/card/a/f;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object v1, v9

    goto/16 :goto_3

    :cond_18
    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    if-eqz v2, :cond_19

    new-instance v9, Lcom/google/android/gms/smart_profile/card/a/e;

    invoke-direct {v9}, Lcom/google/android/gms/smart_profile/card/a/e;-><init>()V

    move-object v2, v3

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v9, v1, v2}, Lcom/google/android/gms/smart_profile/card/a/e;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object v1, v9

    goto/16 :goto_3

    :cond_19
    iget-object v2, v1, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    if-eqz v2, :cond_1a

    new-instance v9, Lcom/google/android/gms/smart_profile/card/a/a;

    invoke-direct {v9}, Lcom/google/android/gms/smart_profile/card/a/a;-><init>()V

    move-object v2, v3

    check-cast v2, Landroid/support/v7/widget/CardView;

    invoke-virtual {v9, v1, v2}, Lcom/google/android/gms/smart_profile/card/a/a;->a(Ljava/lang/Object;Landroid/view/View;)V

    move-object v1, v9

    goto/16 :goto_3

    :cond_1a
    const-string v2, "CardFactory"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "No controller created for "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v4

    goto/16 :goto_3

    :cond_1b
    move-object v3, v4

    goto/16 :goto_4

    .line 173
    :cond_1c
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->e()V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->a:Lcom/google/android/gms/smart_profile/card/g;

    if-eqz v0, :cond_1d

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->a:Lcom/google/android/gms/smart_profile/card/g;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/card/g;->c()V

    .line 178
    :cond_1d
    return-void

    :cond_1e
    move-object v2, v4

    move-object v3, v4

    goto/16 :goto_8
.end method

.method private e()V
    .locals 3

    .prologue
    .line 214
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    .line 215
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/a/b;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Landroid/os/Bundle;)V

    .line 215
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 219
    :cond_0
    return-void
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/d;->b()Z

    move-result v0

    goto :goto_0
.end method

.method private g()Lcom/google/android/gms/smart_profile/ak;
    .locals 2

    .prologue
    .line 320
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "smartProfileUtilFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/bg;

    .line 323
    iget-object v0, v0, Lcom/google/android/gms/smart_profile/bg;->d:Lcom/google/android/gms/smart_profile/ak;

    return-object v0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 8

    .prologue
    .line 223
    const/4 v1, 0x0

    .line 225
    packed-switch p1, :pswitch_data_0

    .line 254
    const-string v0, "CardsFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled loader id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object v0, v1

    .line 258
    :goto_0
    return-object v0

    .line 227
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/smart_profile/b/c;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/f;->f:Lcom/google/android/gms/people/identity/models/Person;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/smart_profile/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/identity/models/Person;)V

    goto :goto_0

    .line 231
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/smart_profile/b/d;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/f;->f:Lcom/google/android/gms/people/identity/models/Person;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/smart_profile/b/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/identity/models/Person;)V

    goto :goto_0

    .line 235
    :pswitch_2
    new-instance v0, Lcom/google/android/gms/smart_profile/b/j;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/f;->f:Lcom/google/android/gms/people/identity/models/Person;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/smart_profile/b/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/identity/models/Person;)V

    goto :goto_0

    .line 239
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v2, "smartProfileUtilFragment"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/bg;

    .line 242
    if-eqz v0, :cond_0

    .line 243
    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/bg;->b:Ljava/lang/String;

    iget v3, v0, Lcom/google/android/gms/smart_profile/bg;->c:I

    new-instance v4, Lcom/google/ac/c/a/a/a/q;

    invoke-direct {v4}, Lcom/google/ac/c/a/a/a/q;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, v1, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/ac/c/a/a/a/g;

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/card/d;->a(Lcom/google/ac/c/a/a/a/g;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v5, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    new-array v1, v1, [Lcom/google/ac/c/a/a/a/g;

    invoke-interface {v5, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/ac/c/a/a/a/g;

    iput-object v1, v4, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    invoke-static {v4}, Lcom/google/ac/c/a/a/a/q;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    new-instance v4, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;-><init>([BLjava/lang/String;I)V

    .line 247
    new-instance v1, Lcom/google/android/gms/smart_profile/b/i;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/bg;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-direct {v1, v2, v0, v4}, Lcom/google/android/gms/smart_profile/b/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/ab;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V

    move-object v0, v1

    .line 249
    goto/16 :goto_0

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Landroid/content/Intent;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    .prologue
    .line 293
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 294
    if-eqz p2, :cond_1

    .line 295
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->g()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 302
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->g()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 304
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "smartProfileUtilFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/bg;

    .line 307
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/bg;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/bg;->b:Ljava/lang/String;

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/smart_profile/aj;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 310
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.action.SIGN_UP"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 311
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 317
    :cond_0
    :goto_1
    return-void

    .line 300
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->g()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/b;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2, p3}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    .line 314
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/q;->startActivity(Landroid/content/Intent;)V

    goto :goto_1
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 5

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    iget v1, p1, Landroid/support/v4/a/j;->m:I

    const-string v2, "CardMixer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "clearLoaderData(), loaderId: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->remove(I)V

    .line 288
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 45
    check-cast p2, Ljava/util/List;

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    iget v1, p1, Landroid/support/v4/a/j;->m:I

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/d;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    const-string v1, "CardMixer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addLoadedData(), #loaders started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/d;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\t#loaders finished: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->d()V

    :cond_0
    return-void

    :cond_1
    const-string v0, "CardMixer"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addLoadedData() passed unexpected loaderId value, loaderId="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/people/identity/models/Person;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 110
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/f;->f:Lcom/google/android/gms/people/identity/models/Person;

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->f:Lcom/google/android/gms/people/identity/models/Person;

    if-nez v0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    new-instance v0, Lcom/google/android/gms/smart_profile/card/d;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/smart_profile/card/d;-><init>(Lcom/google/android/gms/people/identity/models/Person;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->g:Lcom/google/android/gms/smart_profile/card/d;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/d;->a()Ljava/util/List;

    move-result-object v2

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/au;->a()Z

    move-result v3

    .line 121
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 122
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/au;->b(I)Landroid/support/v4/a/j;

    move-result-object v0

    if-eqz v0, :cond_2

    if-eqz v3, :cond_3

    .line 125
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v4

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0, v5, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 121
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 129
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/f;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v4

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0, v5, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_2

    .line 133
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->d()V

    goto :goto_0
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 150
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/f;->e:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gms/smart_profile/card/f;->e:I

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 78
    instance-of v0, p1, Lcom/google/android/gms/smart_profile/card/g;

    if-eqz v0, :cond_0

    .line 79
    check-cast p1, Lcom/google/android/gms/smart_profile/card/g;

    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/f;->a:Lcom/google/android/gms/smart_profile/card/g;

    return-void

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Hosting Activity must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/smart_profile/card/g;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 90
    if-eqz p3, :cond_0

    .line 91
    const-string v0, "childrenSavedInstanceState"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    .line 94
    const-string v0, "nextLoaderId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "nextLoaderId"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/f;->e:I

    .line 99
    :cond_0
    sget v0, Lcom/google/android/gms/l;->fd:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 101
    sget v0, Lcom/google/android/gms/j;->cw:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/f;->b:Landroid/view/ViewGroup;

    .line 103
    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 142
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/f;->e()V

    .line 143
    const-string v0, "childrenSavedInstanceState"

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/f;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 145
    const-string v0, "nextLoaderId"

    iget v1, p0, Lcom/google/android/gms/smart_profile/card/f;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    return-void
.end method
