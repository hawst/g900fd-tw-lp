.class final Lcom/google/android/gms/drive/v;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/g/aw;

.field final synthetic b:Lcom/google/android/gms/drive/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/u;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 60
    iput-object p1, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    iput-object p3, p0, Lcom/google/android/gms/drive/v;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 64
    :try_start_0
    const-string v0, "DriveInitializer"

    const-string v1, "Background init thread started"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->i()V

    .line 70
    const-string v0, "DriveInitializer"

    const-string v1, "All delete locks released"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v0}, Lcom/google/android/gms/drive/u;->a(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/database/s;

    move-result-object v1

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, v1, Lcom/google/android/gms/drive/database/s;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->a()Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/b/d;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/c;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/c;->h()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 87
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v1}, Lcom/google/android/gms/drive/u;->f(Lcom/google/android/gms/drive/u;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 88
    const-string v1, "DriveInitializer"

    const-string v2, "Background init thread ended"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    .line 71
    :cond_0
    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/drive/database/s;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/drive/g/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v5, v3, v0

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v2, v5}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/gms/drive/database/s;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/a;

    move-result-object v0

    iget-object v3, v1, Lcom/google/android/gms/drive/database/s;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_2
    :try_start_2
    iget-object v2, v1, Lcom/google/android/gms/drive/database/s;->b:Lcom/google/android/gms/drive/g/w;

    new-instance v0, Ljava/io/File;

    iget-object v2, v2, Lcom/google/android/gms/drive/g/w;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    const-string v3, "filecache2"

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v2

    if-nez v2, :cond_4

    :cond_3
    const/4 v0, 0x0

    :cond_4
    if-eqz v0, :cond_5

    invoke-static {v0}, Lcom/google/android/gms/common/util/v;->a(Ljava/io/File;)Z

    :cond_5
    iget-object v0, v1, Lcom/google/android/gms/drive/database/s;->b:Lcom/google/android/gms/drive/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/w;->d()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-static {v0}, Lcom/google/android/gms/common/util/v;->a(Ljava/io/File;)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    :cond_6
    :goto_3
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v0}, Lcom/google/android/gms/drive/u;->b(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/e/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/e/b;->b()Ljava/util/concurrent/Future;

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->C()Lcom/google/android/gms/drive/events/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/events/k;->a()V

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->l()Lcom/google/android/gms/drive/a/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/a;->c()V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v0}, Lcom/google/android/gms/drive/u;->d(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/d/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v1}, Lcom/google/android/gms/drive/u;->c(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/a/a/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/d/b;->a(Lcom/google/android/gms/drive/d/c;)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v0}, Lcom/google/android/gms/drive/u;->d(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/d/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v1}, Lcom/google/android/gms/drive/u;->b(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/e/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/d/b;->a(Lcom/google/android/gms/drive/d/c;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v0}, Lcom/google/android/gms/drive/u;->d(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/d/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v1}, Lcom/google/android/gms/drive/u;->e(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/realtime/cache/w;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/d/b;->a(Lcom/google/android/gms/drive/d/c;)V

    .line 82
    new-instance v0, Lcom/google/android/gms/drive/g/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/v;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/g/c;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    .line 84
    iget-object v1, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v1}, Lcom/google/android/gms/drive/u;->c(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/a/a/c;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/g/c;->a(Lcom/google/android/gms/drive/g/d;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v0}, Lcom/google/android/gms/drive/u;->c(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/a/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/c;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/drive/v;->b:Lcom/google/android/gms/drive/u;

    invoke-static {v0}, Lcom/google/android/gms/drive/u;->f(Lcom/google/android/gms/drive/u;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 88
    const-string v0, "DriveInitializer"

    const-string v1, "Background init thread ended"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    return-void

    .line 71
    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "ObsoleteDataCleaner"

    const-string v2, "Error removing legacy content files"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3
.end method
