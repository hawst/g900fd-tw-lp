.class public final Lcom/google/android/gms/drive/g/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:[Landroid/accounts/Account;


# direct methods
.method public static a([Landroid/accounts/Account;Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 40
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 41
    aget-object v1, p0, v0

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 45
    :goto_1
    return v0

    .line 40
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 54
    invoke-static {p0}, Lcom/google/android/gms/drive/g/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 55
    invoke-static {v0, p1}, Lcom/google/android/gms/drive/g/a;->a([Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    .line 56
    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    aget-object v0, v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/drive/g/a;->a:[Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 29
    sget-object v0, Lcom/google/android/gms/drive/g/a;->a:[Landroid/accounts/Account;

    .line 32
    :goto_0
    return-object v0

    .line 31
    :cond_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 32
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0
.end method
