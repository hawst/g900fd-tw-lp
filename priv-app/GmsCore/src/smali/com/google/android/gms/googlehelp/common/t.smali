.class public final Lcom/google/android/gms/googlehelp/common/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/Map;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/t;->a:Ljava/lang/String;

    .line 59
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    .line 60
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/common/t;->c:Ljava/lang/String;

    .line 61
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/t;->e:Ljava/lang/String;

    .line 62
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/common/t;->d:Ljava/lang/String;

    .line 63
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/googlehelp/common/t;
    .locals 1

    .prologue
    .line 75
    sget v0, Lcom/google/android/gms/p;->pb:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/common/t;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/t;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/t;
    .locals 4

    .prologue
    .line 80
    invoke-static {}, Lcom/google/android/gms/googlehelp/common/k;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    .line 81
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/k;->b(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v1

    .line 82
    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/k;->c(Lcom/google/android/gms/googlehelp/common/k;)V

    .line 84
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 85
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    new-instance v1, Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v0

    const-string v3, ""

    invoke-direct {v1, v0, v2, p0, v3}, Lcom/google/android/gms/googlehelp/common/t;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private f()Ljava/util/List;
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/t;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->t()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/t;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/t;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    return-object v0
.end method

.method public final a(Ljava/util/Map;)V
    .locals 6

    .prologue
    .line 169
    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 170
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/googlehelp/common/k;

    .line 171
    if-eqz v1, :cond_0

    .line 172
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    .line 176
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/googlehelp/common/k;->d(Ljava/lang/String;)V

    .line 177
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->q()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/googlehelp/common/k;->a(J)V

    .line 178
    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/k;->b(Lcom/google/android/gms/googlehelp/common/k;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/k;->b(Z)V

    goto :goto_0

    .line 180
    :cond_1
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 205
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    .line 206
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 207
    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/common/k;->a(Z)V

    .line 208
    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/common/k;->b(Z)V

    .line 209
    const-string v2, ""

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/common/k;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 212
    :cond_1
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/t;->c:Ljava/lang/String;

    const-string v1, "SEARCH_RESULTS_CLICKED"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/t;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->h()Z

    move-result v0

    return v0
.end method

.method public final e()Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/t;->a:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    return-object v0
.end method
