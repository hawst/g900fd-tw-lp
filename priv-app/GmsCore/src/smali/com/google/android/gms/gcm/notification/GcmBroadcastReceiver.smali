.class public Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;
.super Landroid/support/v4/a/z;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    const-string v0, "gms:gcm:notification_enable"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;->a:Lcom/google/android/gms/common/a/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/support/v4/a/z;-><init>()V

    return-void
.end method

.method private static a(Lcom/google/android/gms/gcm/notification/b;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 226
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/b;->d:Ljava/lang/String;

    invoke-direct {v1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/b;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/b;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/gcm/notification/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 232
    :cond_0
    :goto_0
    iget v0, p0, Lcom/google/android/gms/gcm/notification/b;->f:I

    if-eqz v0, :cond_1

    .line 233
    iget v0, p0, Lcom/google/android/gms/gcm/notification/b;->f:I

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 235
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/gcm/notification/b;->e:[Ljava/lang/String;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 236
    invoke-virtual {v1, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 235
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 229
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/b;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 238
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/b;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/gcm/notification/b;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 241
    :cond_4
    return-object v1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    sget-object v0, Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    invoke-static {p1}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/ag;

    .line 45
    invoke-static {p2}, Lcom/google/android/gms/gcm/ag;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    .line 46
    const-string v4, "gcm"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_18

    .line 47
    const-string v0, "GCM-Notification"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const-string v0, "GCM-Notification"

    const-string v4, "Received message"

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    :cond_0
    const-string v0, "from"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, "google.com"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string v1, "GCM-Notification"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid from address: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;->isOrderedBroadcast()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;->setResultCode(I)V

    .line 60
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 61
    return-void

    .line 50
    :cond_3
    const-string v0, "rawData"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "rawData"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    :goto_1
    if-nez v0, :cond_6

    const-string v0, "GCM-Notification"

    const-string v1, "Payload missing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const-string v0, "p"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    move-object v0, v1

    goto :goto_1

    :cond_5
    invoke-static {v0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    goto :goto_1

    :cond_6
    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/gcm/notification/d;->a([B)Lcom/google/android/gms/gcm/notification/d;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    iget-object v0, v4, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    if-nez v0, :cond_7

    move v0, v2

    :goto_2
    if-nez v0, :cond_12

    const-string v0, "GCM-Notification"

    const-string v1, "Payload is not valid"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "GCM-Notification"

    const-string v1, "Unable to parse payload"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_7
    iget-object v0, v4, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/c;->a:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v0, v0, Lcom/google/android/gms/gcm/notification/c;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    move v0, v2

    goto :goto_2

    :cond_9
    iget-object v0, v4, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    if-eqz v0, :cond_11

    iget-object v0, v4, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    iget v5, v0, Lcom/google/android/gms/gcm/notification/b;->a:I

    if-eq v5, v3, :cond_a

    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/b;->d:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_b

    :cond_a
    move v0, v2

    goto :goto_2

    :cond_b
    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/b;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_c

    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/b;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_c

    move v0, v2

    goto :goto_2

    :cond_c
    iget v5, v0, Lcom/google/android/gms/gcm/notification/b;->a:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_d

    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/b;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_d

    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/b;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    :cond_d
    move v0, v2

    :goto_3
    if-nez v0, :cond_11

    move v0, v2

    goto :goto_2

    :cond_e
    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/b;->d:Ljava/lang/String;

    const-string v6, "android.settings."

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_f

    move v0, v3

    goto :goto_3

    :cond_f
    const-string v6, "android.intent.action.VIEW"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_10

    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/b;->g:Ljava/lang/String;

    if-eqz v5, :cond_10

    iget-object v0, v0, Lcom/google/android/gms/gcm/notification/b;->g:Ljava/lang/String;

    const-string v5, "market://details?id="

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    move v0, v3

    goto :goto_3

    :cond_10
    move v0, v2

    goto :goto_3

    :cond_11
    move v0, v3

    goto/16 :goto_2

    :cond_12
    iget-object v0, v4, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    if-eqz v0, :cond_16

    iget-object v0, v4, Lcom/google/android/gms/gcm/notification/d;->a:Lcom/google/android/gms/gcm/notification/c;

    iget-object v3, v4, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    const-string v4, "GCM-Notification"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_13

    const-string v4, "GCM-Notification"

    const-string v5, "Showing notification"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_13
    new-instance v4, Landroid/support/v4/app/bk;

    invoke-direct {v4, p1}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v4

    const v5, 0x1080027

    invoke-virtual {v4, v5}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/gms/gcm/notification/c;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v4

    iget-object v0, v0, Lcom/google/android/gms/gcm/notification/c;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v4

    if-eqz v3, :cond_14

    if-nez v3, :cond_15

    move-object v0, v1

    :goto_4
    iput-object v0, v4, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    :cond_14
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    const-string v1, "GCM-Notification"

    invoke-virtual {v4}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    goto/16 :goto_0

    :cond_15
    invoke-static {v3}, Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;->a(Lcom/google/android/gms/gcm/notification/b;)Landroid/content/Intent;

    move-result-object v0

    iget v5, v3, Lcom/google/android/gms/gcm/notification/b;->a:I

    packed-switch v5, :pswitch_data_0

    const-string v0, "GCM-Notification"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Invalid intent type: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v3, Lcom/google/android/gms/gcm/notification/b;->a:I

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_4

    :pswitch_0
    invoke-static {p1, v2, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_4

    :pswitch_1
    invoke-static {p1, v2, v0, v8}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_4

    :pswitch_2
    invoke-static {p1, v2, v0, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_4

    :cond_16
    iget-object v0, v4, Lcom/google/android/gms/gcm/notification/d;->b:Lcom/google/android/gms/gcm/notification/b;

    const-string v1, "GCM-Notification"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_17

    const-string v1, "GCM-Notification"

    const-string v2, "Triggering intent"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_17
    invoke-static {v0}, Lcom/google/android/gms/gcm/notification/GcmBroadcastReceiver;->a(Lcom/google/android/gms/gcm/notification/b;)Landroid/content/Intent;

    move-result-object v1

    iget v2, v0, Lcom/google/android/gms/gcm/notification/b;->a:I

    packed-switch v2, :pswitch_data_1

    const-string v1, "GCM-Notification"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid intent type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/google/android/gms/gcm/notification/b;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {p1, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    :pswitch_5
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 52
    :cond_18
    const-string v1, "GCM-Notification"

    invoke-static {v1, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 53
    const-string v1, "GCM-Notification"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received message with unhandled type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
