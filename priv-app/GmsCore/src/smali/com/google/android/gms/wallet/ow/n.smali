.class final Lcom/google/android/gms/wallet/ow/n;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 0

    .prologue
    .line 2541
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 2796
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 2797
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    const/16 v1, 0x19b

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;I)V

    .line 2798
    return-void
.end method

.method public final a(Lcom/google/aa/a/a/a/f;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 2545
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 2546
    iget v0, p1, Lcom/google/aa/a/a/a/f;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 2547
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->j(Lcom/google/android/gms/wallet/ow/g;)V

    .line 2549
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;)V

    .line 2643
    :cond_0
    :goto_0
    return-void

    .line 2553
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iput-boolean v9, v0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    .line 2554
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->k(Lcom/google/android/gms/wallet/ow/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2559
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->l(Lcom/google/android/gms/wallet/ow/g;)Z

    .line 2560
    iget-object v1, p1, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 2561
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v1, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->f:Ljava/lang/String;

    .line 2562
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v2, v1, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;[I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2564
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->m(Lcom/google/android/gms/wallet/ow/g;)V

    goto :goto_0

    .line 2568
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;)V

    .line 2570
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2572
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->m(Lcom/google/android/gms/wallet/ow/g;)V

    goto :goto_0

    .line 2576
    :cond_3
    const/4 v0, 0x0

    .line 2577
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v2, v9, :cond_4

    .line 2578
    new-instance v0, Lcom/google/android/gms/wallet/cache/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/cache/a;-><init>()V

    .line 2579
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    packed-switch v2, :pswitch_data_0

    .line 2595
    :cond_4
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2, v1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)V

    .line 2596
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2, p1, v0}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    .line 2597
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2, p1, v0}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    .line 2598
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2, p1, v0}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/a;)V

    .line 2599
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2, v1}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)V

    .line 2600
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2, v1}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;)V

    .line 2601
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2, v1, v0}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/aa/b/a/a/a/a/p;Lcom/google/android/gms/wallet/cache/a;)V

    .line 2602
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->n(Lcom/google/android/gms/wallet/ow/g;)V

    .line 2603
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->o(Lcom/google/android/gms/wallet/ow/g;)V

    .line 2605
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1, v10}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Z)V

    .line 2606
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->h:I

    if-ne v1, v9, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eq v1, v9, :cond_6

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->p(Lcom/google/android/gms/wallet/ow/g;)Z

    move-result v1

    if-eqz v1, :cond_a

    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->q(Lcom/google/android/gms/wallet/ow/g;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 2610
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->r(Lcom/google/android/gms/wallet/ow/g;)Z

    .line 2611
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->c(Lcom/google/android/gms/wallet/ow/g;)V

    .line 2615
    :cond_7
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->m(Lcom/google/android/gms/wallet/ow/g;)V

    .line 2617
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->s(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->t(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v1

    if-eqz v1, :cond_8

    .line 2619
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->s(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/g;->t(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v2

    new-array v3, v9, [Ljava/lang/String;

    const-string v4, "create_to_ui_populated"

    aput-object v4, v3, v10

    invoke-virtual {v1, v2, v3}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 2621
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/g;->s(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 2622
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->u(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/i;

    .line 2623
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->v(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/apps/common/a/a/h;

    .line 2625
    :cond_8
    if-eqz v0, :cond_9

    .line 2626
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/g;->g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/g;->g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v1, Lcom/google/android/gms/wallet/common/b;->a:Landroid/content/Context;

    const-string v6, "wallet_items_cache"

    const-string v7, "update_ui_loaded_from_cache"

    iget-object v8, v0, Lcom/google/android/gms/wallet/cache/a;->b:[Ljava/lang/String;

    iget-boolean v2, v0, Lcom/google/android/gms/wallet/cache/a;->a:Z

    if-eqz v2, :cond_b

    const-wide/16 v2, 0x1

    :goto_3
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v5, v6, v7, v8, v2}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Long;)V

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/cache/a;->a:Z

    if-nez v0, :cond_9

    const-string v0, "ui_disruption_due_to_cache"

    invoke-static {v1, v4, v0}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 2632
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 2633
    if-eqz v0, :cond_0

    .line 2634
    const-string v1, "com.google.android.gms.wallet.MASKED_WALLET_FLOW_TYPE"

    invoke-virtual {v0, v1, v10}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 2638
    if-ne v0, v9, :cond_0

    .line 2639
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    .line 2581
    :pswitch_0
    const-string v2, "continue"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    goto/16 :goto_1

    .line 2585
    :pswitch_1
    const-string v2, "updateInstrument"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    goto/16 :goto_1

    .line 2589
    :pswitch_2
    const-string v2, "updateAddress"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/cache/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/a;

    goto/16 :goto_1

    .line 2612
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v1, :cond_7

    .line 2613
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/android/gms/wallet/ow/g;)V

    goto/16 :goto_2

    .line 2626
    :cond_b
    const-wide/16 v2, 0x0

    goto :goto_3

    .line 2579
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/j;J)V
    .locals 6

    .prologue
    .line 2704
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v0, v0

    .line 2705
    const/4 v1, 0x0

    .line 2708
    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 2709
    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    aget v1, v1, v0

    packed-switch v1, :pswitch_data_0

    .line 2715
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;I)V

    .line 2751
    :goto_1
    return-void

    .line 2711
    :pswitch_0
    const/4 v1, 0x1

    .line 2708
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2720
    :cond_0
    if-eqz v1, :cond_1

    .line 2721
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->z(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/payform/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->y(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v2, v2, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/payform/f;->a(Lcom/google/aa/b/a/a/a/a/i;Lcom/google/checkout/inapp/proto/j;)V

    goto :goto_1

    .line 2726
    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->x(Lcom/google/android/gms/wallet/ow/g;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v0, v1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/j;Lcom/google/aa/b/a/a/a/a/i;Ljava/lang/String;J)Lcom/google/android/gms/wallet/FullWallet;

    move-result-object v2

    .line 2730
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->x:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->dQ:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;

    .line 2733
    invoke-virtual {v2}, Lcom/google/android/gms/wallet/FullWallet;->c()Lcom/google/android/gms/wallet/ProxyCard;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ProxyCard;->b()Ljava/lang/String;

    move-result-object v1

    .line 2734
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x4

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 2735
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ow/g;->x:Landroid/view/View;

    sget v4, Lcom/google/android/gms/j;->dO:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 2738
    sget v4, Lcom/google/android/gms/p;->BB:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 2739
    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/payform/CreditCardGenerationView;->a(Ljava/lang/String;)V

    .line 2740
    new-instance v0, Lcom/google/android/gms/wallet/ow/o;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/wallet/ow/o;-><init>(Lcom/google/android/gms/wallet/ow/n;Lcom/google/android/gms/wallet/FullWallet;)V

    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/wallet/ow/n;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 2709
    nop

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/l;)V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v1, 0x0

    .line 2648
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 2649
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;[I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2699
    :goto_0
    return-void

    .line 2652
    :cond_0
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v0, v0

    if-lez v0, :cond_4

    .line 2653
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget v4, v2, v0

    .line 2654
    packed-switch v4, :pswitch_data_0

    .line 2663
    const-string v0, "ChooseMethodsFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled required action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2664
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, v8}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;I)V

    goto :goto_0

    .line 2657
    :pswitch_0
    const-string v5, "ChooseMethodsFragment"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Required action: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2658
    iget-object v4, p1, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v4, :cond_1

    .line 2659
    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    .line 2653
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2668
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->C:Lcom/google/aa/a/a/a/f;

    if-nez v0, :cond_3

    .line 2669
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->j(Lcom/google/android/gms/wallet/ow/g;)V

    goto :goto_0

    .line 2672
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Z)V

    goto :goto_0

    .line 2677
    :cond_4
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v0, :cond_5

    .line 2678
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->w(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/g;->x(Lcom/google/android/gms/wallet/ow/g;)Landroid/accounts/Account;

    move-result-object v2

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    .line 2683
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 2684
    const-string v2, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 2685
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    const/4 v2, -0x1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;ILandroid/content/Intent;)V

    .line 2686
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-boolean v1, p1, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    iget-object v2, v2, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2690
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "buyer_selection_masked_wallet"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2695
    :cond_5
    const-string v0, "ChooseMethodsFragment"

    const-string v1, "GetMaskedWalletForBuyerSelectionResponse has neither required action nor MerchantResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2697
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, v8}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;I)V

    goto/16 :goto_0

    .line 2654
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/b/a/c;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2766
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v3, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 2767
    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;)I

    move-result v0

    .line 2768
    const/16 v1, 0x198

    if-ne v0, v1, :cond_0

    .line 2769
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    const/4 v2, 0x0

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;ILandroid/content/Intent;)V

    .line 2770
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v3}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->a(Landroid/content/Context;IILjava/lang/String;)Ljava/lang/String;

    .line 2786
    :goto_0
    return-void

    .line 2772
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->D(Lcom/google/android/gms/wallet/ow/g;)Z

    move-result v1

    invoke-static {p1, v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2773
    iget-object v1, p1, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    iget-object v1, v1, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    .line 2774
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v3, v1, Lcom/google/aa/b/a/k;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/aa/b/a/k;->b:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 2776
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->D(Lcom/google/android/gms/wallet/ow/g;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2781
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->z(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/payform/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->x(Lcom/google/android/gms/wallet/ow/g;)Landroid/accounts/Account;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/payform/f;->b(Landroid/accounts/Account;)V

    goto :goto_0

    .line 2783
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2790
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 2791
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    const/16 v1, 0x19d

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;I)V

    .line 2792
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 2755
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 2756
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->A(Lcom/google/android/gms/wallet/ow/g;)Z

    .line 2757
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_0

    .line 2758
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->B(Lcom/google/android/gms/wallet/ow/g;)V

    .line 2762
    :goto_0
    return-void

    .line 2760
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/n;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->C(Lcom/google/android/gms/wallet/ow/g;)V

    goto :goto_0
.end method
