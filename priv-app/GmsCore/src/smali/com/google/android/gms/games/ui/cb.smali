.class public abstract Lcom/google/android/gms/games/ui/cb;
.super Lcom/google/android/gms/games/ui/b;
.source "SourceFile"


# instance fields
.field d:I

.field e:Z

.field f:I

.field private g:Ljava/util/Stack;

.field private h:Ljava/util/Stack;

.field private final i:I

.field private final j:I

.field private final k:Z

.field private l:I

.field private m:I

.field private n:I

.field private o:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 127
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/cb;-><init>(Landroid/content/Context;IB)V

    .line 128
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IB)V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/cb;-><init>(Landroid/content/Context;II)V

    .line 137
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;II)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 147
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b;-><init>(Landroid/content/Context;)V

    .line 106
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cb;->g:Ljava/util/Stack;

    .line 107
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cb;->h:Ljava/util/Stack;

    .line 149
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 152
    :try_start_0
    invoke-virtual {v2, p2}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 157
    :goto_0
    if-gtz v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "numColumns must be at least 1."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 153
    :catch_0
    move-exception v0

    .line 154
    const-string v3, "MultiColDBufferAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unable to find resource: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0

    .line 160
    :cond_0
    iput v0, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    .line 162
    iput v1, p0, Lcom/google/android/gms/games/ui/cb;->j:I

    .line 176
    sget v0, Lcom/google/android/gms/g;->aN:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/cb;->m:I

    iput v0, p0, Lcom/google/android/gms/games/ui/cb;->l:I

    .line 177
    sget v0, Lcom/google/android/gms/g;->aM:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/cb;->n:I

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/cb;->k:Z

    .line 180
    return-void
.end method

.method private a(ILandroid/view/View;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, -0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 271
    if-nez p2, :cond_3

    .line 272
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/cb;->h()Landroid/widget/LinearLayout;

    move-result-object p2

    .line 276
    :goto_0
    if-lez p1, :cond_0

    .line 277
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getPaddingLeft()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/cb;->n:I

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getPaddingRight()I

    move-result v3

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {p2, v0, v1, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 281
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    mul-int v4, p1, v0

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->c:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v5

    move v1, v2

    .line 284
    :goto_1
    iget v0, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    if-ge v1, v0, :cond_d

    .line 285
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 288
    add-int v6, v4, v1

    .line 289
    if-ge v6, v5, :cond_1

    if-gez v6, :cond_4

    :cond_1
    const/4 v3, 0x1

    .line 292
    :goto_2
    if-eqz v3, :cond_7

    .line 293
    instance-of v3, v0, Lcom/google/android/gms/games/ui/cc;

    if-nez v3, :cond_6

    .line 294
    if-eqz v0, :cond_2

    .line 295
    iget-object v3, p0, Lcom/google/android/gms/games/ui/cb;->g:Ljava/util/Stack;

    invoke-virtual {v3, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 296
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 299
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 305
    :goto_3
    invoke-virtual {p2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    move-object v3, v0

    .line 350
    :goto_4
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 352
    if-nez v1, :cond_b

    .line 353
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 354
    iget v6, p0, Lcom/google/android/gms/games/ui/cb;->n:I

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 361
    :goto_5
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 284
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 274
    :cond_3
    check-cast p2, Landroid/widget/LinearLayout;

    goto :goto_0

    :cond_4
    move v3, v2

    .line 289
    goto :goto_2

    .line 302
    :cond_5
    new-instance v0, Lcom/google/android/gms/games/ui/cc;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-direct {v0, v3}, Lcom/google/android/gms/games/ui/cc;-><init>(Landroid/content/Context;)V

    goto :goto_3

    :cond_6
    move-object v3, v0

    .line 307
    goto :goto_4

    .line 310
    :cond_7
    iget-object v3, p0, Lcom/google/android/gms/games/ui/cb;->c:Lcom/google/android/gms/common/data/d;

    invoke-interface {v3, v6}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v3

    .line 312
    instance-of v7, v0, Lcom/google/android/gms/games/ui/cc;

    if-eqz v7, :cond_a

    .line 313
    iget-object v7, p0, Lcom/google/android/gms/games/ui/cb;->h:Ljava/util/Stack;

    check-cast v0, Lcom/google/android/gms/games/ui/cc;

    invoke-virtual {v7, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->removeViewAt(I)V

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 327
    :goto_6
    invoke-virtual {p2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    .line 343
    :cond_8
    :goto_7
    iget-object v7, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, v6, v3}, Lcom/google/android/gms/games/ui/cb;->a(Landroid/view/View;ILjava/lang/Object;)V

    .line 346
    iget-object v3, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/games/ui/cb;->a(Landroid/view/View;Landroid/content/Context;)V

    move-object v3, v0

    goto :goto_4

    .line 319
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/cb;->g()Landroid/view/View;

    move-result-object v0

    .line 321
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v2, v9, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 324
    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    .line 329
    :cond_a
    if-nez v0, :cond_8

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/cb;->g()Landroid/view/View;

    move-result-object v0

    .line 334
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v2, v9, v8}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 337
    invoke-virtual {v0, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 339
    invoke-virtual {p2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    goto :goto_7

    .line 355
    :cond_b
    iget v6, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    add-int/lit8 v6, v6, -0x1

    if-ne v1, v6, :cond_c

    .line 356
    iget v6, p0, Lcom/google/android/gms/games/ui/cb;->n:I

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 357
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_5

    .line 359
    :cond_c
    iget v6, p0, Lcom/google/android/gms/games/ui/cb;->n:I

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iput v6, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    goto :goto_5

    .line 364
    :cond_d
    return-object p2
.end method

.method private a(Landroid/view/View;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 462
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 465
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cb;->e:Z

    if-nez v0, :cond_2

    .line 466
    invoke-virtual {p1, v1, v1}, Landroid/view/View;->measure(II)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, p0, Lcom/google/android/gms/games/ui/cb;->l:I

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/games/ui/cb;->m:I

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/games/ui/cb;->n:I

    iget v3, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    mul-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/games/ui/cb;->d:I

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    div-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/gms/games/ui/cb;->f:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/cd;

    iget v0, v0, Lcom/google/android/gms/games/ui/cd;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 467
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/cb;->e:Z

    .line 471
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_4

    .line 472
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/cd;

    .line 473
    iget v3, v0, Lcom/google/android/gms/games/ui/cd;->a:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 474
    if-eqz v3, :cond_3

    .line 475
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 479
    if-eqz v4, :cond_3

    .line 480
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/cd;->a()I

    move-result v0

    iput v0, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 484
    invoke-virtual {v3, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 471
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 487
    :cond_4
    return-void
.end method

.method private h()Landroid/widget/LinearLayout;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 441
    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 444
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    .line 445
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    .line 448
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->c(Landroid/view/View;)V

    .line 450
    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    .line 453
    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 454
    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 455
    iget v1, p0, Lcom/google/android/gms/games/ui/cb;->l:I

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingTop()I

    move-result v2

    iget v3, p0, Lcom/google/android/gms/games/ui/cb;->m:I

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 457
    return-object v0
.end method


# virtual methods
.method protected final a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 262
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cb;->k:Z

    if-eqz v0, :cond_6

    .line 263
    if-nez p2, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/cb;->h()Landroid/widget/LinearLayout;

    move-result-object p2

    :goto_0
    iget v0, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    mul-int v6, p1, v0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->c:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v7

    move v1, v2

    :goto_1
    iget v0, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    if-ge v1, v0, :cond_7

    add-int v8, v6, v1

    add-int/lit8 v0, v7, -0x1

    if-ne v8, v0, :cond_3

    move v3, v4

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->c:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0, v8}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v9

    if-nez v3, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move v5, v4

    :goto_3
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v11, -0x1

    int-to-float v5, v5

    invoke-direct {v10, v2, v11, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    invoke-virtual {v0, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;I)V

    iget-object v5, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, v8, v9}, Lcom/google/android/gms/games/ui/cb;->a(Landroid/view/View;ILjava/lang/Object;)V

    if-nez v3, :cond_7

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_0
    check-cast p2, Landroid/widget/LinearLayout;

    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    move v1, v2

    :goto_4
    if-ge v1, v3, :cond_2

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    instance-of v6, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v6, :cond_1

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/high16 v6, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v6

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->g:Ljava/util/Stack;

    invoke-virtual {v0, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_2
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->removeAllViews()V

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/cb;->g()Landroid/view/View;

    move-result-object v0

    move v5, v4

    goto :goto_3

    :cond_5
    iget v0, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    sub-int v5, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/cb;->g()Landroid/view/View;

    move-result-object v0

    goto :goto_3

    .line 265
    :cond_6
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/cb;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object p2

    :cond_7
    return-object p2
.end method

.method public final a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 542
    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract a(Landroid/view/View;ILjava/lang/Object;)V
.end method

.method public final a(Landroid/view/View;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 547
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cb;->c:Lcom/google/android/gms/common/data/d;

    if-nez v0, :cond_1

    .line 213
    const/4 v0, 0x0

    .line 220
    :cond_0
    :goto_0
    return v0

    .line 216
    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/games/ui/b;->b()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lcom/google/android/gms/games/ui/cb;->i:I

    div-int/2addr v0, v1

    .line 217
    iget v1, p0, Lcom/google/android/gms/games/ui/cb;->j:I

    if-lez v1, :cond_0

    .line 218
    iget v1, p0, Lcom/google/android/gms/games/ui/cb;->j:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public abstract g()Landroid/view/View;
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method
