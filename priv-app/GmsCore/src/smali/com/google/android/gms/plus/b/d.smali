.class public final Lcom/google/android/gms/plus/b/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/plus/service/v1whitelisted/h;

.field public final b:Lcom/google/android/gms/plus/service/lso/f;

.field private final c:Lcom/google/android/gms/plus/service/lso/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/h;

    invoke-direct {v0, p1}, Lcom/google/android/gms/plus/service/v1whitelisted/h;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/d;->a:Lcom/google/android/gms/plus/service/v1whitelisted/h;

    .line 67
    new-instance v0, Lcom/google/android/gms/plus/service/lso/e;

    invoke-direct {v0, p2}, Lcom/google/android/gms/plus/service/lso/e;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/d;->c:Lcom/google/android/gms/plus/service/lso/e;

    .line 68
    new-instance v0, Lcom/google/android/gms/plus/service/lso/f;

    invoke-direct {v0, p2}, Lcom/google/android/gms/plus/service/lso/f;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/b/d;->b:Lcom/google/android/gms/plus/service/lso/f;

    .line 69
    return-void
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;)Landroid/content/pm/ApplicationInfo;
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 214
    .line 215
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->d()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 217
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v2, v1

    :goto_0
    move v6, v3

    move-object v4, v5

    .line 218
    :goto_1
    if-ge v6, v2, :cond_0

    .line 219
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;

    .line 220
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->e()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 222
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v7, 0x0

    invoke-virtual {p0, v3, v7}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    .line 224
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->d()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x40

    invoke-virtual {p0, v7, v8}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v7

    .line 226
    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    if-eqz v8, :cond_4

    iget-object v8, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v8, v8

    if-lez v8, :cond_4

    .line 228
    iget-object v7, v7, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v8, 0x0

    aget-object v7, v7, v8

    invoke-virtual {v7}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v7

    .line 230
    const/4 v8, 0x0

    invoke-static {v7, v8}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v7

    .line 231
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps$Clients;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-eqz v1, :cond_4

    .line 232
    if-eqz v4, :cond_2

    move-object v4, v5

    .line 246
    :cond_0
    return-object v4

    :cond_1
    move v2, v3

    .line 217
    goto :goto_0

    :cond_2
    move-object v1, v3

    :goto_2
    move-object v4, v1

    .line 218
    :cond_3
    :goto_3
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_3

    :cond_4
    move-object v1, v4

    goto :goto_2
.end method

.method private static a()Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 250
    sget-object v0, Lcom/google/android/gms/plus/c/a;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 251
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 252
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 254
    :try_start_0
    aget-object v3, v1, v0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 259
    :cond_0
    return-object v2

    :catch_0
    move-exception v3

    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/util/Pair;
    .locals 21

    .prologue
    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/b/d;->c:Lcom/google/android/gms/plus/service/lso/e;

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "authapps"

    if-eqz p3, :cond_0

    const-string v4, "category"

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/plus/service/lso/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/plus/service/lso/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_0
    if-eqz v3, :cond_1

    const-string v4, "hl"

    invoke-static {v3}, Lcom/google/android/gms/plus/service/lso/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5, v4, v3}, Lcom/google/android/gms/plus/service/lso/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_1
    iget-object v2, v2, Lcom/google/android/gms/plus/service/lso/e;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-class v7, Lcom/google/android/gms/plus/service/lso/AuthApps;

    move-object/from16 v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/lso/AuthApps;

    .line 108
    new-instance v9, Landroid/util/SparseArray;

    invoke-direct {v9}, Landroid/util/SparseArray;-><init>()V

    .line 109
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps;->e()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 110
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/service/lso/AuthApps$Api_scopes;

    .line 111
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/lso/AuthApps$Api_scopes;->f()I

    move-result v5

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/lso/AuthApps$Api_scopes;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v9, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 115
    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps;->f()Ljava/util/List;

    move-result-object v10

    .line 116
    if-eqz v10, :cond_8

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    move v4, v2

    .line 117
    :goto_1
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 118
    invoke-static {}, Lcom/google/android/gms/plus/b/d;->a()Ljava/util/ArrayList;

    move-result-object v12

    .line 119
    sget-object v2, Lcom/google/android/gms/plus/c/a;->K:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 122
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v14

    .line 123
    const-string v2, "SHA1"

    invoke-static {v2}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v15

    .line 124
    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    .line 125
    sget v2, Lcom/google/android/gms/p;->uo:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 126
    const/4 v2, 0x0

    move v8, v2

    :goto_2
    if-ge v8, v4, :cond_f

    .line 127
    invoke-interface {v10, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;

    .line 128
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bp;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 129
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 130
    const-string v5, "display_name"

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->e()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v5, "icon_url"

    move-object/from16 v0, v18

    invoke-virtual {v0, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    const-string v3, "revoke_handle"

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->h()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    const/4 v6, 0x0

    .line 135
    const/4 v5, 0x0

    .line 136
    const/4 v3, 0x0

    .line 137
    const/4 v7, 0x0

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 138
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->k()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    move v7, v6

    move v6, v5

    move v5, v3

    :cond_3
    :goto_3
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v20

    .line 140
    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 141
    const/4 v7, 0x1

    .line 145
    :cond_4
    move/from16 v0, v20

    if-ne v13, v0, :cond_5

    .line 146
    const/4 v6, 0x1

    .line 149
    :cond_5
    const v3, 0xdea8

    move/from16 v0, v20

    if-gt v3, v0, :cond_9

    const v3, 0xdf0b

    move/from16 v0, v20

    if-gt v0, v3, :cond_9

    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_6

    .line 150
    const/4 v5, 0x1

    .line 153
    :cond_6
    move/from16 v0, v20

    invoke-virtual {v9, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 154
    if-eqz v3, :cond_3

    .line 155
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v20

    if-lez v20, :cond_7

    .line 156
    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    :cond_7
    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 116
    :cond_8
    const/4 v2, 0x0

    move v4, v2

    goto/16 :goto_1

    .line 149
    :cond_9
    const/4 v3, 0x0

    goto :goto_4

    .line 161
    :cond_a
    const/4 v3, 0x0

    .line 162
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g()Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_b

    .line 167
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;->g()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Lcom/google/android/gms/plus/j;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 172
    :cond_b
    :goto_5
    if-nez v3, :cond_c

    .line 177
    const/4 v7, 0x0

    .line 178
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    .line 180
    :cond_c
    const-string v19, "application_id"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    const-string v3, "is_aspen"

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 182
    const-string v3, "has_conn_read"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 183
    const-string v3, "is_fitness"

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 184
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    if-lez v3, :cond_d

    .line 185
    const-string v3, "scopes"

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    :cond_d
    invoke-static {v14, v15, v2}, Lcom/google/android/gms/plus/b/d;->a(Landroid/content/pm/PackageManager;Ljava/security/MessageDigest;Lcom/google/android/gms/plus/service/lso/AuthApps$Apps;)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    .line 189
    if-eqz v2, :cond_e

    .line 191
    const-string v3, "display_name"

    invoke-virtual {v14, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const-string v3, "application_info"

    invoke-static {v2}, Lcom/google/android/gms/plus/internal/model/apps/c;->a(Landroid/content/pm/ApplicationInfo;)[B

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 196
    :cond_e
    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto/16 :goto_2

    .line 200
    :cond_f
    new-instance v2, Lcom/google/android/gms/plus/b/e;

    invoke-direct {v2}, Lcom/google/android/gms/plus/b/e;-><init>()V

    invoke-static {v11, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 203
    sget-object v2, Lcom/google/android/gms/plus/internal/model/apps/c;->a:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v5

    .line 204
    const/4 v2, 0x0

    move v3, v2

    :goto_6
    if-ge v3, v4, :cond_10

    .line 205
    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    .line 204
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_6

    .line 208
    :cond_10
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    return-object v2

    :catch_0
    move-exception v19

    goto/16 :goto_5
.end method
