.class public final Lcom/google/android/gms/drive/api/a/e;
.super Lcom/google/android/gms/drive/api/a/c;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/drive/api/a/c;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/e;->e:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/e;->e:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    const-string v1, "Invalid authorize access request: no request"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/e;->e:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Invalid authorize access request: app id is zero"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/e;->e:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid authorize access request: no drive id"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/e;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/e;->e:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->a()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/e;->e:Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/drive/api/d;->a(JLcom/google/android/gms/drive/DriveId;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/e;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 40
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
