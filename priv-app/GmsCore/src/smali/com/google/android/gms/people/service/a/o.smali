.class final Lcom/google/android/gms/people/service/a/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/x;


# instance fields
.field final synthetic a:Landroid/os/Bundle;

.field final synthetic b:Lcom/google/android/gms/people/a/b;

.field final synthetic c:Lcom/google/android/gms/people/service/a/n;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/service/a/n;Landroid/os/Bundle;Lcom/google/android/gms/people/a/b;)V
    .locals 0

    .prologue
    .line 591
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/o;->c:Lcom/google/android/gms/people/service/a/n;

    iput-object p2, p0, Lcom/google/android/gms/people/service/a/o;->a:Landroid/os/Bundle;

    iput-object p3, p0, Lcom/google/android/gms/people/service/a/o;->b:Lcom/google/android/gms/people/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 591
    check-cast p1, Lcom/android/volley/m;

    iget-object v0, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/o;->a:Landroid/os/Bundle;

    const-string v3, "get.server_blob.format"

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/o;->b:Lcom/google/android/gms/people/a/b;

    iget-object v0, v0, Lcom/google/android/gms/people/a/b;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/o;->a:Landroid/os/Bundle;

    const-string v2, "get.server_blob.code"

    iget v3, p1, Lcom/android/volley/m;->a:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/o;->a:Landroid/os/Bundle;

    const-string v2, "get.server_blob.body"

    iget-object v3, p1, Lcom/android/volley/m;->b:[B

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/o;->a:Landroid/os/Bundle;

    const-string v2, "get.server_blob.headers"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    move-object v1, v0

    goto :goto_0
.end method
