.class public final Lcom/google/android/gms/drive/api/a/q;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/api/k;

.field private final f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/CloseContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0x8

    invoke-direct {p0, p1, p4, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/q;->e:Lcom/google/android/gms/drive/api/k;

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    const-string v3, "Invalid close request: no request"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    const-string v3, "Invalid close request: no contents"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->b()Ljava/lang/Boolean;

    move-result-object v0

    const-string v3, "Invalid close request: doesn\'t include save state"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "DiscardContentsOperation wants to save contents; this should never happen"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->e()I

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    const-string v0, "Invalid close request: invalid request"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/q;->e:Lcom/google/android/gms/drive/api/k;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/Contents;->e()I

    move-result v2

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v3

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/q;->f:Lcom/google/android/gms/drive/internal/CloseContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->b()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/drive/api/k;->a(Lcom/google/android/gms/drive/auth/g;ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;ZLcom/google/android/gms/drive/a/a/l;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/q;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 55
    return-void

    :cond_0
    move v0, v2

    .line 45
    goto :goto_0

    :cond_1
    move v1, v2

    .line 47
    goto :goto_1
.end method
