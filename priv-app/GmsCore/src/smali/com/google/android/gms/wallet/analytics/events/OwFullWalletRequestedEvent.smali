.class public Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/analytics/events/v;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:I

.field public final h:Z

.field public final i:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/j;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 71
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 72
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->d:I

    .line 73
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->e:I

    .line 74
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->f:I

    .line 75
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->g:I

    .line 76
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->h:Z

    .line 77
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->i:J

    .line 78
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;IIIIZJLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 57
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->d:I

    .line 58
    iput v1, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->e:I

    .line 59
    iput p4, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->f:I

    .line 60
    iput p5, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->g:I

    .line 61
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->h:Z

    .line 62
    iput-wide p7, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->i:J

    .line 63
    iput-object p10, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->a:Ljava/lang/String;

    .line 64
    iput-object p9, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->j:Ljava/lang/String;

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 66
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    .line 67
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;IIJLjava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 47
    new-instance v1, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;

    const/4 v3, 0x1

    const/4 v4, 0x0

    const/4 v7, 0x0

    move-object v2, p1

    move v5, p2

    move v6, p3

    move-wide/from16 v8, p4

    move-object/from16 v10, p6

    move-object/from16 v11, p7

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;IIIIZJLjava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 51
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 94
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 82
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 84
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 86
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 88
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->h:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 89
    iget-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;->i:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 90
    return-void

    .line 88
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
