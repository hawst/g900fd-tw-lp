.class public final Lcom/google/android/gms/smart_profile/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field private final a:Lcom/google/android/gms/smart_profile/b/b;

.field private final b:Lcom/google/android/gms/common/api/v;

.field private final c:Landroid/view/View;

.field private final d:Ljava/lang/String;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/smart_profile/b/b;Lcom/google/android/gms/common/api/v;Landroid/view/View;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/b/a;->a:Lcom/google/android/gms/smart_profile/b/b;

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/b/a;->b:Lcom/google/android/gms/common/api/v;

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/b/a;->c:Landroid/view/View;

    .line 52
    iput-object p4, p0, Lcom/google/android/gms/smart_profile/b/a;->d:Ljava/lang/String;

    .line 53
    iput-boolean p5, p0, Lcom/google/android/gms/smart_profile/b/a;->e:Z

    .line 54
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/a;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/am;

    .line 60
    if-eqz v0, :cond_0

    .line 61
    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->b()V

    .line 64
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/a;->a:Lcom/google/android/gms/smart_profile/b/b;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/a;->a:Lcom/google/android/gms/smart_profile/b/b;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/b/a;->c:Landroid/view/View;

    invoke-interface {v0, v1}, Lcom/google/android/gms/smart_profile/b/b;->a(Landroid/view/View;)V

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/a;->d:Ljava/lang/String;

    .line 73
    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/b/a;->e:Z

    if-eqz v1, :cond_2

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/a;->d:Ljava/lang/String;

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bp;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 77
    :cond_2
    sget-object v2, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/b/a;->b:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/b/a;->c:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/google/android/gms/e;->c:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/b/a;->e:Z

    if-nez v1, :cond_5

    :cond_3
    const/4 v1, 0x3

    :goto_0
    invoke-interface {v2, v3, v0, v1, v5}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_4

    .line 84
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/b/a;->c:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 85
    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 87
    :cond_4
    return-void

    .line 77
    :cond_5
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/gms/people/p;

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/people/p;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/af;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/b/a;->a:Lcom/google/android/gms/smart_profile/b/b;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/smart_profile/b/a;->e:Z

    if-eqz v1, :cond_1

    invoke-static {v0}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/a;->a:Lcom/google/android/gms/smart_profile/b/b;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/b/a;->c:Landroid/view/View;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/smart_profile/b/b;->a(Landroid/view/View;Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/b/a;->a:Lcom/google/android/gms/smart_profile/b/b;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/b/a;->c:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/smart_profile/b/b;->a(Landroid/view/View;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
