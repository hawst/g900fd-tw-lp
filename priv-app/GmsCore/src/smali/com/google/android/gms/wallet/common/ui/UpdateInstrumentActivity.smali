.class public Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;
.super Lcom/google/android/gms/wallet/common/ui/dq;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# static fields
.field private static final h:Ljava/lang/String;


# instance fields
.field protected a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field c:Landroid/widget/ProgressBar;

.field d:Lcom/google/android/gms/wallet/common/ui/bb;

.field e:Landroid/widget/TextView;

.field f:Lcom/google/android/gms/wallet/common/ui/cb;

.field g:Lcom/google/android/gms/wallet/common/ui/bb;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Lcom/google/checkout/inapp/proto/q;

.field private l:Z

.field private m:Lcom/google/android/gms/wallet/common/ui/dh;

.field private n:I

.field private o:I

.field private p:Lcom/google/checkout/inapp/proto/j;

.field private final q:Lcom/google/android/gms/wallet/service/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-string v0, "updateInstrument"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/dq;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i:Z

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:I

    .line 388
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/dl;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->q:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 354
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:I

    .line 355
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 357
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Z)V

    new-instance v0, Lcom/google/checkout/inapp/proto/ap;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ap;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/cb;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->k:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->k:Lcom/google/checkout/inapp/proto/q;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i:Z

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ap;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(I)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 313
    if-eqz p1, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 318
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    if-nez p1, :cond_2

    :goto_2
    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cb;->a(Z)V

    .line 320
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->l:Z

    .line 321
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->c:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 318
    goto :goto_1

    :cond_2
    move v1, v2

    .line 319
    goto :goto_2
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Lcom/google/checkout/inapp/proto/j;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/checkout/inapp/proto/j;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/bb;->b()Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "UpdateInstrumentActivit.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Z)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "UpdateInstrumentActivit.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method private h()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->m:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 326
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->m:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->m:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 360
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:I

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 362
    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    .prologue
    .line 301
    packed-switch p2, :pswitch_data_0

    .line 307
    const-string v0, "UpdateInstrumentActivit"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 310
    :goto_0
    return-void

    .line 304
    :pswitch_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Z)V

    goto :goto_0

    .line 301
    nop

    :pswitch_data_0
    .packed-switch 0x3e8
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cb;->e()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cb;->f()Z

    move-result v0

    return v0
.end method

.method public finish()V
    .locals 2

    .prologue
    .line 334
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->finish()V

    .line 335
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/b;->D:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->overridePendingTransition(II)V

    .line 336
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cb;->g()Z

    move-result v0

    return v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 97
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onCreate(Landroid/os/Bundle;)V

    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 103
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 104
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 106
    const-string v1, "com.google.android.gms.wallet.instrument"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    const-string v5, "Activity requires instrument extra!"

    invoke-static {v1, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 108
    const-string v1, "com.google.android.gms.wallet.instrument"

    const-class v5, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v4, v1, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/j;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/checkout/inapp/proto/j;

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/checkout/inapp/proto/j;

    iget v1, v1, Lcom/google/checkout/inapp/proto/j;->d:I

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    const-string v5, "Only credit card updates are currently supported"

    invoke-static {v1, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 114
    const-string v1, "com.google.android.gms.wallet.unadjustedCartId"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->j:Ljava/lang/String;

    .line 116
    const-string v1, "com.google.android.gms.wallet.accountReference"

    const-class v5, Lcom/google/checkout/inapp/proto/q;

    invoke-static {v4, v1, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/q;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->k:Lcom/google/checkout/inapp/proto/q;

    .line 119
    const-string v1, "com.google.android.gms.wallet.localMode"

    invoke-virtual {v4, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i:Z

    .line 121
    sget v1, Lcom/google/android/gms/l;->gq:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->setContentView(I)V

    .line 123
    if-eqz p1, :cond_0

    .line 124
    const-string v1, "errorMessageResourceId"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:I

    .line 127
    :cond_0
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i:Z

    if-nez v5, :cond_5

    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroid/support/v7/app/a;->a()V

    sget v5, Lcom/google/android/gms/p;->CU:I

    invoke-virtual {v1, v5}, Landroid/support/v7/app/a;->c(I)V

    :goto_1
    sget v1, Lcom/google/android/gms/j;->ch:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:I

    if-eqz v1, :cond_7

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(I)V

    :goto_2
    sget v1, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->c:Landroid/widget/ProgressBar;

    sget v1, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    new-instance v5, Lcom/google/android/gms/wallet/common/ui/dk;

    invoke-direct {v5, p0}, Lcom/google/android/gms/wallet/common/ui/dk;-><init>(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    sget v5, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v1, v5}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/cb;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i:Z

    if-nez v1, :cond_8

    const-string v1, "com.google.android.gms.wallet.addressHints"

    const-class v5, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v4, v1, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v5

    const-string v1, "com.google.android.gms.wallet.baseAddress"

    const-class v6, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v4, v1, v6}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/a/b;

    const-string v6, "com.google.android.gms.wallet.requireAddressUpgrade"

    invoke-virtual {v4, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v7, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v4, v7, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    iget-object v7, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v7, v6, v3, v5, v1}, Lcom/google/android/gms/wallet/common/ui/dm;->a(Lcom/google/checkout/inapp/proto/j;ZZLjava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/common/ui/dm;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v3

    sget v5, Lcom/google/android/gms/j;->gr:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    check-cast v1, Landroid/support/v4/app/Fragment;

    invoke-virtual {v3, v5, v1}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    :cond_1
    sget v1, Lcom/google/android/gms/j;->ua:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;)V

    .line 131
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    if-nez v1, :cond_2

    .line 132
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i:Z

    if-nez v1, :cond_9

    .line 133
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2, v1, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->m:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 140
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->m:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 144
    :cond_2
    return-void

    :cond_3
    move v1, v3

    .line 111
    goto/16 :goto_0

    .line 127
    :cond_4
    sget v1, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setVisibility(I)V

    sget v5, Lcom/google/android/gms/p;->CU:I

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(I)V

    goto/16 :goto_1

    :cond_5
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/support/v7/app/a;->a()V

    sget v5, Lcom/google/android/gms/p;->BU:I

    invoke-virtual {v1, v5}, Landroid/support/v7/app/a;->c(I)V

    goto/16 :goto_1

    :cond_6
    sget v1, Lcom/google/android/gms/j;->nK:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setVisibility(I)V

    sget v5, Lcom/google/android/gms/p;->BU:I

    invoke-virtual {v1, v5}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(I)V

    goto/16 :goto_1

    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->i()V

    goto/16 :goto_2

    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->p:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/cj;->a(Lcom/google/checkout/inapp/proto/j;)Lcom/google/android/gms/wallet/common/ui/cj;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    goto/16 :goto_3

    .line 136
    :cond_9
    const-string v1, "com.google.android.gms.wallet.sessionId"

    invoke-virtual {v4, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->m:Lcom/google/android/gms/wallet/common/ui/dh;

    goto :goto_4
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 282
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onPause()V

    .line 284
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 286
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 148
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onPostCreate(Landroid/os/Bundle;)V

    .line 150
    if-eqz p1, :cond_1

    .line 151
    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->l:Z

    .line 152
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:I

    .line 160
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->l:Z

    if-eqz v0, :cond_0

    .line 161
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Z)V

    .line 163
    :cond_0
    return-void

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "update_instrument"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 262
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/dq;->onResume()V

    .line 264
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "UpdateInstrumentActivit.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "UpdateInstrumentActivit.PossiblyRecoverableErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 275
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->q:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    .line 278
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 290
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dq;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 291
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->h()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:I

    .line 294
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->o:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 295
    const-string v0, "pendingRequest"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 296
    const-string v0, "errorMessageResourceId"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->n:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 297
    return-void
.end method
