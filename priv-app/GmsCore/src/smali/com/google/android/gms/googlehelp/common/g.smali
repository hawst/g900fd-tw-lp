.class public Lcom/google/android/gms/googlehelp/common/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/SharedPreferences;

.field protected final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "com.google.android.gms.googlehelp.SHARED_PREFS_FILE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/g;->a:Landroid/content/SharedPreferences;

    .line 30
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/g;->b:Ljava/lang/String;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/g;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/g;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 73
    const-string v0, "%s:gh_%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/common/g;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/g;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/Map;
    .locals 5

    .prologue
    .line 57
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 58
    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/d;

    .line 59
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/d;->a()Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/googlehelp/e/c;->a:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    if-eqz v3, :cond_0

    .line 61
    invoke-interface {v1, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 64
    :cond_1
    return-object v1
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/g;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/google/android/gms/googlehelp/common/h;
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "CommitPrefEdits"
        }
    .end annotation

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/gms/googlehelp/common/h;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/g;->a:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/googlehelp/common/h;-><init>(Lcom/google/android/gms/googlehelp/common/g;Landroid/content/SharedPreferences$Editor;)V

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/g;->a:Landroid/content/SharedPreferences;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/common/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
