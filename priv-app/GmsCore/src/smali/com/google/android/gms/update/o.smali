.class public final Lcom/google/android/gms/update/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Landroid/content/Context;

.field private static b:Lcom/google/android/gms/update/e;

.field private static c:Z

.field private static d:Landroid/content/ServiceConnection;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    sput-object v0, Lcom/google/android/gms/update/o;->a:Landroid/content/Context;

    .line 37
    sput-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    .line 38
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/update/o;->c:Z

    .line 39
    new-instance v0, Lcom/google/android/gms/update/p;

    invoke-direct {v0}, Lcom/google/android/gms/update/p;-><init>()V

    sput-object v0, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/update/e;)Lcom/google/android/gms/update/e;
    .locals 0

    .prologue
    .line 34
    sput-object p0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    return-object p0
.end method

.method public static a(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 226
    const-string v0, "update_title"

    invoke-static {p0, v0}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 79
    const-class v1, Lcom/google/android/gms/update/o;

    monitor-enter v1

    .line 80
    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/update/o;->c:Z

    if-nez v0, :cond_1

    .line 83
    sput-object p0, Lcom/google/android/gms/update/o;->a:Landroid/content/Context;

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.update.START_SERVICE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 86
    const-string v2, "com.google.android.gms"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 87
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v2, p0, v0, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    .line 89
    sput-boolean v0, Lcom/google/android/gms/update/o;->c:Z

    if-nez v0, :cond_0

    .line 90
    const-string v0, "SystemUpdateClient"

    const-string v2, "bindService returned false; the connection might never succeed."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 93
    :cond_1
    const-string v0, "SystemUpdateClient"

    const-string v2, "Already connected."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Z)V
    .locals 2

    .prologue
    .line 198
    if-eqz p0, :cond_0

    const v0, 0x31129

    .line 199
    :goto_0
    const-string v1, "install"

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 200
    sget-object v1, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    monitor-enter v1

    .line 201
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/update/o;->h()V

    .line 202
    sget-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    invoke-interface {v0}, Lcom/google/android/gms/update/e;->e()V

    .line 203
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 198
    :cond_0
    const v0, 0x3112a

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 273
    if-eqz p1, :cond_1

    .line 277
    :goto_0
    sget-object v0, Lcom/google/android/gms/update/SystemUpdateService;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 278
    if-eqz p1, :cond_0

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_2

    .line 279
    :cond_0
    const-string v0, "SystemUpdateClient"

    const-string v2, "Offpeak window either null or malformed."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 296
    :goto_1
    return-object v0

    .line 273
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "update_offpeak_download_window"

    invoke-static {v0, v2}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    .line 282
    :cond_2
    new-array v0, v5, [Ljava/lang/String;

    .line 283
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v2, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 284
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x4

    invoke-virtual {v2, v5}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 285
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v5, "HHmm"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 287
    :try_start_0
    invoke-virtual {v4, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v3

    .line 288
    invoke-static {p0}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v5

    .line 289
    const/4 v6, 0x0

    invoke-virtual {v5, v3}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v6

    .line 290
    invoke-virtual {v4, v2}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    .line 291
    const/4 v3, 0x1

    invoke-virtual {v5, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 293
    :catch_0
    move-exception v0

    const-string v0, "SystemUpdateClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to parse window: \""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 294
    goto :goto_1
.end method

.method public static b(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 234
    const-string v0, "update_size"

    invoke-static {p0, v0}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b()V
    .locals 3

    .prologue
    .line 108
    const-class v1, Lcom/google/android/gms/update/o;

    monitor-enter v1

    .line 109
    :try_start_0
    sget-boolean v0, Lcom/google/android/gms/update/o;->c:Z

    if-nez v0, :cond_0

    .line 110
    const-string v0, "SystemUpdateClient"

    const-string v2, "attempt to disconnect() when not connected"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    :goto_0
    return-void

    .line 113
    :cond_0
    monitor-exit v1

    .line 114
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/update/o;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 115
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/update/o;->c:Z

    .line 116
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 210
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.server.checkin.CHECKIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 211
    const-string v1, "force"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 212
    invoke-virtual {p0, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 213
    return-void
.end method

.method public static c()I
    .locals 2

    .prologue
    .line 149
    sget-object v1, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    monitor-enter v1

    .line 150
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/update/o;->h()V

    .line 151
    sget-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    invoke-interface {v0}, Lcom/google/android/gms/update/e;->a()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static c(Landroid/content/Context;)J
    .locals 2

    .prologue
    .line 219
    invoke-static {p0}, Lcom/google/android/gms/checkin/CheckinService;->c(Landroid/content/Context;)J

    move-result-wide v0

    return-wide v0
.end method

.method public static c(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 242
    const-string v0, "update_description"

    invoke-static {p0, v0}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d()J
    .locals 4

    .prologue
    .line 161
    sget-object v1, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    monitor-enter v1

    .line 162
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/update/o;->h()V

    .line 163
    sget-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    invoke-interface {v0}, Lcom/google/android/gms/update/e;->b()J

    move-result-wide v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static d(Landroid/content/ContentResolver;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 250
    const-string v0, "update_url"

    invoke-static {p0, v0}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static e()I
    .locals 2

    .prologue
    .line 173
    sget-object v1, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    monitor-enter v1

    .line 174
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/update/o;->h()V

    .line 175
    sget-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    invoke-interface {v0}, Lcom/google/android/gms/update/e;->c()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static e(Landroid/content/ContentResolver;)Z
    .locals 1

    .prologue
    .line 259
    const-string v0, "update_required_setup"

    invoke-static {p0, v0}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 262
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static f()V
    .locals 2

    .prologue
    .line 185
    const v0, 0x3112a

    const-string v1, "download"

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 186
    sget-object v1, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    monitor-enter v1

    .line 187
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/update/o;->h()V

    .line 188
    sget-object v0, Lcom/google/android/gms/update/o;->b:Lcom/google/android/gms/update/e;

    invoke-interface {v0}, Lcom/google/android/gms/update/e;->d()V

    .line 189
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic g()Landroid/content/Context;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/update/o;->a:Landroid/content/Context;

    return-object v0
.end method

.method private static h()V
    .locals 3

    .prologue
    .line 130
    sget-object v1, Lcom/google/android/gms/update/o;->d:Landroid/content/ServiceConnection;

    monitor-enter v1

    .line 131
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/update/o;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 132
    const/16 v0, 0xf

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    new-instance v0, Landroid/os/RemoteException;

    const-string v2, "No connection to the SystemUpdateService."

    invoke-direct {v0, v2}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 135
    :cond_0
    :try_start_1
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 138
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
