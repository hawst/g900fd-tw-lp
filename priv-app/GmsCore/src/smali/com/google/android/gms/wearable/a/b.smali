.class public final Lcom/google/android/gms/wearable/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;

.field public static final l:Lcom/google/android/gms/common/a/d;

.field public static final m:Lcom/google/android/gms/common/a/d;

.field public static final n:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 13
    const-string v0, "gms:wearable:service:logging"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->a:Lcom/google/android/gms/common/a/d;

    .line 23
    const-string v0, "gms:wearable:service:override_wearable_app_user_debug_signature"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->b:Lcom/google/android/gms/common/a/d;

    .line 32
    const-string v0, "gms:wearable:service:data_item_size_limit_bytes"

    const v1, 0x19000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->c:Lcom/google/android/gms/common/a/d;

    .line 40
    const-string v0, "gms:wearable:service:bt_retry_steps"

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->d:Lcom/google/android/gms/common/a/d;

    .line 49
    const-string v0, "gms:wearable:service:bt_retry_time_limit"

    const-wide/16 v2, -0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->e:Lcom/google/android/gms/common/a/d;

    .line 58
    const-string v0, "gms:wearable:service:bt_retry_time_at_limit"

    const-wide/32 v2, 0x36ee80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->f:Lcom/google/android/gms/common/a/d;

    .line 68
    const-string v0, "gms:wearable:service:show_bt_retry_notification"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->g:Lcom/google/android/gms/common/a/d;

    .line 74
    const-string v0, "gms:wearable:service:dataitem_gc_interval"

    const-wide/32 v2, 0x2932e00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->h:Lcom/google/android/gms/common/a/d;

    .line 85
    const-string v0, "gms:wearable:service:max_write_stuck_time"

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->i:Lcom/google/android/gms/common/a/d;

    .line 95
    const-string v0, "gms:wearable:service:max_write_stuck_time_v2"

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->j:Lcom/google/android/gms/common/a/d;

    .line 105
    const-string v0, "gms:wearable:service:run_foreground_with_notification"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->k:Lcom/google/android/gms/common/a/d;

    .line 114
    const-string v0, "gms:wearable:service:debug_notification_strings"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->l:Lcom/google/android/gms/common/a/d;

    .line 117
    const-string v0, "gms:wearable:service:reboot_delay_millis"

    const-wide/32 v2, 0x1d4c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->m:Lcom/google/android/gms/common/a/d;

    .line 123
    const-string v0, "gms:wearable:service:reboot_lollipop"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/a/b;->n:Lcom/google/android/gms/common/a/d;

    return-void
.end method
