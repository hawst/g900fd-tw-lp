.class public final Lcom/google/android/gms/security/verifier/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/security/verifier/s;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/android/gms/security/verifier/r;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 904
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 905
    iput-object v0, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/s;->cachedSize:I

    .line 906
    return-void
.end method

.method public static a()[Lcom/google/android/gms/security/verifier/s;
    .locals 2

    .prologue
    .line 884
    sget-object v0, Lcom/google/android/gms/security/verifier/s;->d:[Lcom/google/android/gms/security/verifier/s;

    if-nez v0, :cond_1

    .line 885
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 887
    :try_start_0
    sget-object v0, Lcom/google/android/gms/security/verifier/s;->d:[Lcom/google/android/gms/security/verifier/s;

    if-nez v0, :cond_0

    .line 888
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/security/verifier/s;

    sput-object v0, Lcom/google/android/gms/security/verifier/s;->d:[Lcom/google/android/gms/security/verifier/s;

    .line 890
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 892
    :cond_1
    sget-object v0, Lcom/google/android/gms/security/verifier/s;->d:[Lcom/google/android/gms/security/verifier/s;

    return-object v0

    .line 890
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 980
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 981
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 982
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 985
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    if-eqz v1, :cond_1

    .line 986
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 989
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 990
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 993
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 918
    if-ne p1, p0, :cond_1

    .line 948
    :cond_0
    :goto_0
    return v0

    .line 921
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 922
    goto :goto_0

    .line 924
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/s;

    .line 925
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 926
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 927
    goto :goto_0

    .line 929
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 930
    goto :goto_0

    .line 932
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    if-nez v2, :cond_5

    .line 933
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    if-eqz v2, :cond_6

    move v0, v1

    .line 934
    goto :goto_0

    .line 937
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/security/verifier/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 938
    goto :goto_0

    .line 941
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    if-nez v2, :cond_7

    .line 942
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    move v0, v1

    .line 943
    goto :goto_0

    .line 945
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 946
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 953
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 956
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 958
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 960
    return v0

    .line 953
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 956
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/r;->hashCode()I

    move-result v0

    goto :goto_1

    .line 958
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 870
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/security/verifier/r;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 967
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/s;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 969
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    if-eqz v0, :cond_1

    .line 970
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/s;->b:Lcom/google/android/gms/security/verifier/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 972
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 973
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/s;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 975
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 976
    return-void
.end method
