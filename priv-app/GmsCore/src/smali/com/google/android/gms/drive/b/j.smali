.class public final Lcom/google/android/gms/drive/b/j;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# static fields
.field private static a:I


# instance fields
.field private b:Z

.field private c:Ljava/security/MessageDigest;

.field private d:Ljava/security/MessageDigest;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const/16 v0, 0x400

    sput v0, Lcom/google/android/gms/drive/b/j;->a:I

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 39
    invoke-static {p2}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to create message digest for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/j;->b:Z

    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/j;->b()V

    .line 46
    :cond_0
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 117
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/MessageDigest;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/j;->d:Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    .line 119
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/j;->b:Z

    .line 120
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/j;->d:Ljava/security/MessageDigest;

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->a([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final mark(I)V
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/j;->b:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 102
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/j;->b()V

    .line 103
    invoke-super {p0, p1}, Ljava/io/FilterInputStream;->mark(I)V

    .line 104
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/j;->b:Z

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    .line 62
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    .line 63
    if-ltz v0, :cond_0

    .line 64
    iget-object v1, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    and-int/lit16 v2, v0, 0xff

    int-to-byte v2, v2

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 66
    :cond_0
    return v0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 71
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    .line 72
    if-lez v0, :cond_0

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    invoke-virtual {v1, p1, p2, v0}, Ljava/security/MessageDigest;->update([BII)V

    .line 75
    :cond_0
    return v0
.end method

.method public final declared-synchronized reset()V
    .locals 1

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/j;->b:Z

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/drive/b/j;->d:Ljava/security/MessageDigest;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/j;->b()V

    .line 111
    invoke-super {p0}, Ljava/io/FilterInputStream;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    monitor-exit p0

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final skip(J)J
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/4 v8, 0x0

    .line 80
    .line 81
    sget v0, Lcom/google/android/gms/drive/b/j;->a:I

    new-array v4, v0, [B

    move-wide v0, v2

    .line 82
    :goto_0
    cmp-long v5, p1, v2

    if-lez v5, :cond_0

    .line 83
    array-length v5, v4

    int-to-long v6, v5

    invoke-static {p1, p2, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    long-to-int v5, v6

    invoke-super {p0, v4, v8, v5}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v5

    .line 84
    if-ltz v5, :cond_0

    .line 85
    iget-object v6, p0, Lcom/google/android/gms/drive/b/j;->c:Ljava/security/MessageDigest;

    invoke-virtual {v6, v4, v8, v5}, Ljava/security/MessageDigest;->update([BII)V

    .line 88
    int-to-long v6, v5

    sub-long/2addr p1, v6

    .line 89
    int-to-long v6, v5

    add-long/2addr v0, v6

    .line 90
    goto :goto_0

    .line 91
    :cond_0
    return-wide v0
.end method
