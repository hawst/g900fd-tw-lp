.class public final Lcom/google/android/gms/people/d/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1546
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1547
    iput-wide v2, p0, Lcom/google/android/gms/people/d/q;->a:J

    iput v0, p0, Lcom/google/android/gms/people/d/q;->b:I

    iput v0, p0, Lcom/google/android/gms/people/d/q;->c:I

    iput v0, p0, Lcom/google/android/gms/people/d/q;->d:I

    iput v0, p0, Lcom/google/android/gms/people/d/q;->e:I

    iput v0, p0, Lcom/google/android/gms/people/d/q;->f:I

    iput v0, p0, Lcom/google/android/gms/people/d/q;->g:I

    iput-wide v2, p0, Lcom/google/android/gms/people/d/q;->h:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/q;->cachedSize:I

    .line 1548
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1647
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1648
    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1649
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1652
    :cond_0
    iget v1, p0, Lcom/google/android/gms/people/d/q;->b:I

    if-eqz v1, :cond_1

    .line 1653
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/people/d/q;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1656
    :cond_1
    iget v1, p0, Lcom/google/android/gms/people/d/q;->c:I

    if-eqz v1, :cond_2

    .line 1657
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/people/d/q;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1660
    :cond_2
    iget v1, p0, Lcom/google/android/gms/people/d/q;->e:I

    if-eqz v1, :cond_3

    .line 1661
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/people/d/q;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1664
    :cond_3
    iget v1, p0, Lcom/google/android/gms/people/d/q;->f:I

    if-eqz v1, :cond_4

    .line 1665
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/people/d/q;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1668
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->h:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 1669
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->h:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1672
    :cond_5
    iget v1, p0, Lcom/google/android/gms/people/d/q;->d:I

    if-eqz v1, :cond_6

    .line 1673
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/people/d/q;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1676
    :cond_6
    iget v1, p0, Lcom/google/android/gms/people/d/q;->g:I

    if-eqz v1, :cond_7

    .line 1677
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/people/d/q;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1680
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1565
    if-ne p1, p0, :cond_1

    .line 1596
    :cond_0
    :goto_0
    return v0

    .line 1568
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 1569
    goto :goto_0

    .line 1571
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/q;

    .line 1572
    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/q;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 1573
    goto :goto_0

    .line 1575
    :cond_3
    iget v2, p0, Lcom/google/android/gms/people/d/q;->b:I

    iget v3, p1, Lcom/google/android/gms/people/d/q;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1576
    goto :goto_0

    .line 1578
    :cond_4
    iget v2, p0, Lcom/google/android/gms/people/d/q;->c:I

    iget v3, p1, Lcom/google/android/gms/people/d/q;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1579
    goto :goto_0

    .line 1581
    :cond_5
    iget v2, p0, Lcom/google/android/gms/people/d/q;->d:I

    iget v3, p1, Lcom/google/android/gms/people/d/q;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1582
    goto :goto_0

    .line 1584
    :cond_6
    iget v2, p0, Lcom/google/android/gms/people/d/q;->e:I

    iget v3, p1, Lcom/google/android/gms/people/d/q;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1585
    goto :goto_0

    .line 1587
    :cond_7
    iget v2, p0, Lcom/google/android/gms/people/d/q;->f:I

    iget v3, p1, Lcom/google/android/gms/people/d/q;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1588
    goto :goto_0

    .line 1590
    :cond_8
    iget v2, p0, Lcom/google/android/gms/people/d/q;->g:I

    iget v3, p1, Lcom/google/android/gms/people/d/q;->g:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1591
    goto :goto_0

    .line 1593
    :cond_9
    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->h:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/q;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 1594
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 1601
    iget-wide v0, p0, Lcom/google/android/gms/people/d/q;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 1604
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/q;->b:I

    add-int/2addr v0, v1

    .line 1605
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/q;->c:I

    add-int/2addr v0, v1

    .line 1606
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/q;->d:I

    add-int/2addr v0, v1

    .line 1607
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/q;->e:I

    add-int/2addr v0, v1

    .line 1608
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/q;->f:I

    add-int/2addr v0, v1

    .line 1609
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/q;->g:I

    add-int/2addr v0, v1

    .line 1610
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->h:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/q;->h:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1612
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1505
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/q;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/q;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/q;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/q;->e:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/q;->f:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/q;->h:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/q;->d:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/q;->g:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1618
    iget-wide v0, p0, Lcom/google/android/gms/people/d/q;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 1619
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1621
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/d/q;->b:I

    if-eqz v0, :cond_1

    .line 1622
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/people/d/q;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1624
    :cond_1
    iget v0, p0, Lcom/google/android/gms/people/d/q;->c:I

    if-eqz v0, :cond_2

    .line 1625
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/people/d/q;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1627
    :cond_2
    iget v0, p0, Lcom/google/android/gms/people/d/q;->e:I

    if-eqz v0, :cond_3

    .line 1628
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/people/d/q;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1630
    :cond_3
    iget v0, p0, Lcom/google/android/gms/people/d/q;->f:I

    if-eqz v0, :cond_4

    .line 1631
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/people/d/q;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1633
    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/people/d/q;->h:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 1634
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/people/d/q;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1636
    :cond_5
    iget v0, p0, Lcom/google/android/gms/people/d/q;->d:I

    if-eqz v0, :cond_6

    .line 1637
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/people/d/q;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1639
    :cond_6
    iget v0, p0, Lcom/google/android/gms/people/d/q;->g:I

    if-eqz v0, :cond_7

    .line 1640
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/people/d/q;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1642
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1643
    return-void
.end method
