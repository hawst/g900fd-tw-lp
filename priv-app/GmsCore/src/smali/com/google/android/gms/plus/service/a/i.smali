.class public final Lcom/google/android/gms/plus/service/a/i;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

.field private final c:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/i;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/i;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/i;->c:Lcom/google/android/gms/plus/internal/f;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/i;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/i;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    iget-object v2, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v2, p1, v0, v1}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v0

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/i;->c:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 67
    :goto_0
    return-void

    .line 47
    :catch_0
    move-exception v0

    .line 48
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 49
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v4, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 50
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/i;->c:Lcom/google/android/gms/plus/internal/f;

    new-instance v2, Lcom/google/android/gms/plus/internal/model/acls/b;

    invoke-direct {v2}, Lcom/google/android/gms/plus/internal/model/acls/b;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/i;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/plus/internal/model/acls/b;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/acls/b;->a()Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v2

    invoke-interface {v0, v5, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V

    goto :goto_0

    .line 56
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/i;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/i;->c:Lcom/google/android/gms/plus/internal/f;

    new-instance v2, Lcom/google/android/gms/plus/internal/model/acls/b;

    invoke-direct {v2}, Lcom/google/android/gms/plus/internal/model/acls/b;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/i;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/plus/internal/model/acls/b;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/acls/b;->a()Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v2

    invoke-interface {v1, v5, v0, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V

    goto :goto_0

    .line 62
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/i;->c:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/gms/plus/internal/model/acls/b;

    invoke-direct {v2}, Lcom/google/android/gms/plus/internal/model/acls/b;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/i;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/plus/internal/model/acls/b;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/model/acls/b;->a()Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v2

    invoke-interface {v0, v1, v6, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 5

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/i;->c:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/i;->c:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    const/4 v2, 0x0

    new-instance v3, Lcom/google/android/gms/plus/internal/model/acls/b;

    invoke-direct {v3}, Lcom/google/android/gms/plus/internal/model/acls/b;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/i;->b:Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->c()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/plus/internal/model/acls/b;->a:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/internal/model/acls/b;->a()Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V

    .line 78
    :cond_0
    return-void
.end method
