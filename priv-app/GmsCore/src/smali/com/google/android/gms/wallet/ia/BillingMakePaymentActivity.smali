.class public Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;
.super Lcom/google/android/gms/wallet/common/ui/ds;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/bp;
.implements Lcom/google/android/gms/wallet/common/ui/cd;


# static fields
.field private static final r:Ljava/lang/String;


# instance fields
.field private A:Lcom/google/android/apps/common/a/a/h;

.field private B:Lcom/google/android/apps/common/a/a/i;

.field private C:Lcom/google/android/apps/common/a/a/h;

.field private D:Ljava/util/ArrayList;

.field private final E:Lcom/google/android/gms/wallet/service/l;

.field a:Landroid/widget/ProgressBar;

.field b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field d:Landroid/widget/TextView;

.field e:Lcom/google/android/gms/wallet/common/ui/cc;

.field f:Landroid/widget/CheckBox;

.field g:Landroid/widget/TextView;

.field h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

.field i:Lcom/google/android/gms/wallet/common/ui/bb;

.field j:Lcom/google/android/gms/wallet/common/ui/bb;

.field protected k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field protected l:Landroid/accounts/Account;

.field m:Lcom/google/android/gms/wallet/common/PaymentModel;

.field n:Ljava/lang/String;

.field o:Z

.field p:I

.field q:Lcom/google/checkout/inapp/proto/t;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private u:Lcom/google/checkout/inapp/proto/q;

.field private v:Lcom/google/checkout/inapp/proto/a/d;

.field private w:Lcom/google/android/gms/wallet/common/ui/dh;

.field private x:I

.field private y:Z

.field private z:Lcom/google/android/apps/common/a/a/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-string v0, "billingMakePayment"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ds;-><init>()V

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->n:Ljava/lang/String;

    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    .line 148
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:I

    .line 660
    new-instance v0, Lcom/google/android/gms/wallet/ia/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/e;-><init>(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/checkout/inapp/proto/a/d;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lcom/google/checkout/inapp/proto/a/d;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:Ljava/util/ArrayList;

    return-object p1
.end method

.method static a(Lcom/google/checkout/inapp/proto/t;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 645
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 647
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 648
    iget-object v4, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    .line 649
    if-eqz v4, :cond_0

    .line 650
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 647
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 653
    :cond_1
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Lcom/google/checkout/inapp/proto/t;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 64
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v2, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/t;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v3}, Lcom/google/android/gms/wallet/common/ui/cc;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Z)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->B:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 449
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:I

    .line 450
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h()V

    .line 451
    return-void

    .line 449
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->C:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private static c(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 602
    if-nez p0, :cond_1

    .line 607
    :cond_0
    :goto_0
    return v0

    .line 605
    :cond_1
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    if-ne v2, v1, :cond_3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v2, v1

    .line 607
    :goto_1
    if-nez v2, :cond_2

    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->d(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v2, v0

    .line 605
    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->B:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->C:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 265
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Z

    .line 266
    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h()V

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.BillingMakePaymentActivity.PaymentOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 270
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.BillingMakePaymentActivity.MakePaymentNetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 274
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 282
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 285
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:I

    .line 286
    return-void

    .line 279
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k()V

    goto :goto_0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 296
    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:I

    if-gez v0, :cond_0

    .line 297
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:I

    .line 302
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private g()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 329
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 432
    iget v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->x:I

    if-lez v0, :cond_1

    move v0, v1

    .line 434
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Z

    move-result v3

    if-eq v0, v3, :cond_0

    .line 435
    if-eqz v0, :cond_2

    .line 436
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 438
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->setEnabled(Z)V

    .line 444
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j()V

    .line 446
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 432
    goto :goto_0

    .line 440
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->setEnabled(Z)V

    goto :goto_1
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private i()Z
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 464
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 465
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 466
    return-void

    :cond_1
    move v0, v1

    .line 464
    goto :goto_0

    :cond_2
    move v2, v1

    .line 465
    goto :goto_1
.end method

.method private k()V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_1

    .line 510
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->E:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/t;)V

    .line 522
    :cond_0
    :goto_0
    return-void

    .line 515
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    new-instance v0, Lcom/google/checkout/inapp/proto/s;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/s;-><init>()V

    .line 517
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/checkout/inapp/proto/q;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/s;->a:Lcom/google/checkout/inapp/proto/q;

    .line 518
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/s;)V

    .line 520
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Z)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic l(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic m(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic n(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic o(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "inapp.BillingMakePaymentActivity.MakePaymentNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic q(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 3

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "inapp.BillingMakePaymentActivity.PaymentOptionsNetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic r(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic s(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 2

    .prologue
    .line 64
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l:Landroid/accounts/Account;

    const/4 v4, 0x1

    iget-object v10, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/checkout/inapp/proto/q;

    iget-object v11, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:Ljava/util/ArrayList;

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    move v8, v5

    move-object v9, v2

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    .line 592
    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 593
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 402
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 405
    packed-switch p1, :pswitch_data_0

    .line 414
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Z)V

    .line 420
    :goto_0
    return-void

    .line 407
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k()V

    goto :goto_0

    .line 410
    :pswitch_1
    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 418
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 405
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 550
    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l:Landroid/accounts/Account;

    const/4 v3, 0x1

    iget-object v6, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/checkout/inapp/proto/q;

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->D:Ljava/util/ArrayList;

    move-object v2, p1

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Landroid/content/Intent;

    move-result-object v0

    .line 561
    const/16 v1, 0x1f6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 575
    :goto_0
    return-void

    .line 563
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 564
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j()V

    .line 566
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 570
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0

    .line 572
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 470
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 471
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 472
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    :goto_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->n:Ljava/lang/String;

    .line 478
    return-void

    .line 474
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 425
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 427
    return-void

    :cond_0
    move v0, v2

    .line 425
    goto :goto_0

    :cond_1
    move v1, v2

    .line 426
    goto :goto_1
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;)V
    .locals 0

    .prologue
    .line 598
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 337
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    .line 339
    packed-switch p1, :pswitch_data_0

    .line 377
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Z

    if-eqz v0, :cond_0

    .line 384
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e()V

    .line 388
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ds;->onActivityResult(IILandroid/content/Intent;)V

    .line 389
    return-void

    .line 341
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 342
    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "Successfully added an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 346
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    .line 347
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    .line 348
    :cond_1
    if-nez p2, :cond_2

    .line 349
    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "User canceled adding an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 352
    :cond_2
    const-string v0, "BillingMakePaymentActiv"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 357
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    .line 371
    const-string v0, "BillingMakePaymentActiv"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating an instrument resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 359
    :pswitch_2
    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "Successfully updated an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 360
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 363
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    .line 364
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    goto/16 :goto_0

    .line 367
    :pswitch_3
    const-string v0, "BillingMakePaymentActiv"

    const-string v1, "User canceled updating an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 339
    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 357
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->g()Z

    .line 398
    :goto_0
    return-void

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/apps/common/a/a/h;

    if-nez v0, :cond_2

    :cond_1
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "billing_make_payment"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lcom/google/android/apps/common/a/a/i;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->z:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->A:Lcom/google/android/apps/common/a/a/h;

    :cond_2
    new-instance v0, Lcom/google/checkout/inapp/proto/u;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/u;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/checkout/inapp/proto/q;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a()Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_3

    iput-boolean v2, v0, Lcom/google/checkout/inapp/proto/u;->d:Z

    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/u;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 156
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onCreate(Landroid/os/Bundle;)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 161
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 162
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 163
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l:Landroid/accounts/Account;

    .line 164
    const-string v0, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "PCID is required"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 166
    const-string v0, "com.google.android.gms.wallet.currencyCode"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Currency code is required"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 168
    const-string v0, "com.google.android.gms.wallet.amountDueMicros"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    const-string v4, "Micro amount is required"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 170
    const-string v0, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    new-instance v4, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v4}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v4, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/checkout/inapp/proto/q;

    .line 172
    iget-object v4, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->u:Lcom/google/checkout/inapp/proto/q;

    iput-object v0, v4, Lcom/google/checkout/inapp/proto/q;->a:Ljava/lang/String;

    .line 173
    const-string v0, "com.google.android.gms.wallet.currencyCode"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 174
    const-string v0, "com.google.android.gms.wallet.amountDueMicros"

    const-wide/16 v6, -0x1

    invoke-virtual {v1, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    .line 176
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-ltz v0, :cond_4

    move v0, v2

    :goto_0
    const-string v1, "Micro amount must be >= 0"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 177
    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lcom/google/checkout/inapp/proto/a/d;

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v4, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->v:Lcom/google/checkout/inapp/proto/a/d;

    iput-wide v6, v0, Lcom/google/checkout/inapp/proto/a/d;->a:J

    .line 181
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 184
    sget v0, Lcom/google/android/gms/l;->ge:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->setContentView(I)V

    .line 185
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/Window;)V

    .line 186
    sget v0, Lcom/google/android/gms/j;->cc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s:Landroid/view/View;

    .line 187
    sget v0, Lcom/google/android/gms/j;->te:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->t:Landroid/view/View;

    .line 194
    :goto_1
    sget v0, Lcom/google/android/gms/j;->pO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a:Landroid/widget/ProgressBar;

    .line 195
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    .line 198
    sget v0, Lcom/google/android/gms/j;->kA:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->d:Landroid/widget/TextView;

    .line 199
    sget v0, Lcom/google/android/gms/j;->kz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, p0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 202
    sget v0, Lcom/google/android/gms/j;->lG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f:Landroid/widget/CheckBox;

    .line 203
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 204
    sget v0, Lcom/google/android/gms/j;->kw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g:Landroid/widget/TextView;

    .line 205
    sget v0, Lcom/google/android/gms/j;->nz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    .line 208
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    sget v0, Lcom/google/android/gms/j;->fQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    .line 210
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 211
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bp;)V

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 216
    if-eqz p1, :cond_6

    .line 217
    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 218
    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    .line 220
    const-string v0, "paymentOptionsPostResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    const-string v0, "paymentOptionsPostResponse"

    const-class v1, Lcom/google/checkout/inapp/proto/t;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/t;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    .line 225
    :cond_1
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 226
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:I

    .line 229
    :cond_2
    const-string v0, "instrumentErrorTextData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Ljava/lang/String;)V

    .line 244
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_3

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 247
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->w:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 251
    :cond_3
    return-void

    :cond_4
    move v0, v3

    .line 176
    goto/16 :goto_0

    .line 189
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 191
    sget v0, Lcom/google/android/gms/l;->gd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->setContentView(I)V

    goto/16 :goto_1

    .line 231
    :cond_6
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "billing_get_payment_options"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->B:Lcom/google/android/apps/common/a/a/i;

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->B:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->C:Lcom/google/android/apps/common/a/a/h;

    .line 234
    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 235
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v3, "make_payment"

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 306
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onPause()V

    .line 307
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f()V

    .line 308
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 255
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onResume()V

    .line 257
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    if-eqz v0, :cond_0

    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o:Z

    .line 262
    :goto_0
    return-void

    .line 260
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e()V

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 312
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 314
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f()V

    .line 316
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 317
    const-string v0, "model"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 318
    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_0

    .line 320
    const-string v0, "paymentOptionsPostResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 323
    :cond_0
    const-string v0, "instrumentErrorTextData"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 324
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 612
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->y:Z

    .line 613
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/ds;->startActivityForResult(Landroid/content/Intent;I)V

    .line 614
    sget v0, Lcom/google/android/gms/b;->E:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->overridePendingTransition(II)V

    .line 615
    return-void
.end method
