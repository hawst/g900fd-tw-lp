.class public final Lcom/google/android/gms/blescanner/compat/c;
.super Lcom/google/android/gms/blescanner/compat/a;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field private static final a:J

.field private static b:Lcom/google/android/gms/blescanner/compat/c;


# instance fields
.field private final c:Lcom/google/android/gms/blescanner/compat/h;

.field private final d:Lcom/google/android/gms/blescanner/compat/v;

.field private final e:Lcom/google/android/gms/blescanner/compat/v;

.field private final f:Lcom/google/android/gms/blescanner/b/d;

.field private g:J

.field private final h:Ljava/util/Map;

.field private final i:Ljava/util/Map;

.field private final j:Ljava/util/Map;

.field private k:Ljava/util/List;

.field private l:Lcom/google/android/gms/blescanner/compat/v;

.field private m:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 39
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/blescanner/compat/c;->a:J

    .line 77
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/blescanner/compat/c;->b:Lcom/google/android/gms/blescanner/compat/c;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/blescanner/compat/h;Lcom/google/android/gms/blescanner/b/d;)V
    .locals 4

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/a;-><init>()V

    .line 85
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0xa

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/blescanner/compat/c;->g:J

    .line 216
    iput-object p1, p0, Lcom/google/android/gms/blescanner/compat/c;->c:Lcom/google/android/gms/blescanner/compat/h;

    .line 217
    iput-object p2, p0, Lcom/google/android/gms/blescanner/compat/c;->f:Lcom/google/android/gms/blescanner/b/d;

    .line 218
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->h:Ljava/util/Map;

    .line 219
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->i:Ljava/util/Map;

    .line 220
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    .line 221
    new-instance v0, Lcom/google/android/gms/blescanner/compat/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/blescanner/compat/f;-><init>(Lcom/google/android/gms/blescanner/compat/c;)V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->d:Lcom/google/android/gms/blescanner/compat/v;

    .line 222
    new-instance v0, Lcom/google/android/gms/blescanner/compat/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/blescanner/compat/e;-><init>(Lcom/google/android/gms/blescanner/compat/c;)V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->e:Lcom/google/android/gms/blescanner/compat/v;

    .line 223
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    .line 224
    return-void
.end method

.method public static a(Lcom/google/android/gms/blescanner/compat/a;)Lcom/google/android/gms/blescanner/compat/c;
    .locals 3

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/blescanner/compat/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/blescanner/compat/d;-><init>(Lcom/google/android/gms/blescanner/compat/a;)V

    .line 116
    new-instance v1, Lcom/google/android/gms/blescanner/compat/c;

    new-instance v2, Lcom/google/android/gms/blescanner/b/f;

    invoke-direct {v2}, Lcom/google/android/gms/blescanner/b/f;-><init>()V

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/blescanner/compat/c;-><init>(Lcom/google/android/gms/blescanner/compat/h;Lcom/google/android/gms/blescanner/b/d;)V

    return-object v1
.end method

.method private declared-synchronized a(Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/gms/blescanner/compat/g;
    .locals 4

    .prologue
    .line 468
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v1

    .line 469
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->i:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    :goto_0
    monitor-exit p0

    return-object v0

    .line 474
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->b()Lcom/google/android/gms/blescanner/compat/y;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/blescanner/compat/y;->e:[B

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->b()Lcom/google/android/gms/blescanner/compat/y;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/blescanner/compat/y;->e:[B

    array-length v0, v0

    if-nez v0, :cond_2

    .line 476
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 480
    :cond_2
    new-instance v0, Lcom/google/android/gms/blescanner/compat/g;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/gms/blescanner/compat/g;-><init>(B)V

    .line 481
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/blescanner/compat/g;->a:Landroid/bluetooth/BluetoothDevice;

    .line 482
    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanResult;->b()Lcom/google/android/gms/blescanner/compat/y;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/blescanner/compat/g;->b:Lcom/google/android/gms/blescanner/compat/y;

    .line 483
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->f()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/blescanner/compat/g;->c:J

    .line 484
    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/c;->i:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/blescanner/compat/g;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 504
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->h:Ljava/util/Map;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 504
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/lang/String;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 515
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 515
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/blescanner/compat/c;Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/blescanner/compat/c;->a(Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/blescanner/compat/c;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/blescanner/compat/c;->a(Ljava/util/List;)V

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V
    .locals 4

    .prologue
    .line 326
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Serial scan: onScanResult("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    .line 327
    invoke-direct {p0, p2}, Lcom/google/android/gms/blescanner/compat/c;->a(Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/gms/blescanner/compat/g;

    move-result-object v1

    .line 328
    if-eqz v1, :cond_1

    .line 329
    invoke-static {p2}, Lcom/google/android/gms/blescanner/compat/c;->b(Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/gms/blescanner/compat/i;

    move-result-object v0

    .line 330
    invoke-direct {p0, v1}, Lcom/google/android/gms/blescanner/compat/c;->a(Lcom/google/android/gms/blescanner/compat/g;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 331
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->f()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/gms/blescanner/compat/g;->c:J

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 333
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "    Found "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " deferred entries for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    .line 335
    invoke-direct {p0, v1}, Lcom/google/android/gms/blescanner/compat/c;->a(Lcom/google/android/gms/blescanner/compat/g;)Ljava/util/Set;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 338
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    :goto_0
    monitor-exit p0

    return-void

    .line 340
    :cond_1
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Serial scan: onScanResult("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): received invalid payload"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 326
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized a(Ljava/util/List;)V
    .locals 6

    .prologue
    .line 285
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/ScanResult;

    .line 286
    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 292
    invoke-direct {p0, v0}, Lcom/google/android/gms/blescanner/compat/c;->a(Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/gms/blescanner/compat/g;

    move-result-object v2

    .line 293
    invoke-static {v0}, Lcom/google/android/gms/blescanner/compat/c;->b(Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/gms/blescanner/compat/i;

    move-result-object v3

    .line 294
    if-eqz v2, :cond_1

    .line 295
    invoke-direct {p0, v2}, Lcom/google/android/gms/blescanner/compat/c;->a(Lcom/google/android/gms/blescanner/compat/g;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 285
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 298
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanResult;->a()Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/blescanner/compat/c;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 299
    iget-wide v2, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->e()Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/blescanner/compat/ab;

    invoke-direct {v2}, Lcom/google/android/gms/blescanner/compat/ab;-><init>()V

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/blescanner/compat/ab;->a(I)Lcom/google/android/gms/blescanner/compat/ab;

    move-result-object v2

    const/4 v3, 0x1

    iput v3, v2, Lcom/google/android/gms/blescanner/compat/ab;->a:I

    invoke-virtual {v2}, Lcom/google/android/gms/blescanner/compat/ab;->a()Lcom/google/android/gms/blescanner/compat/ScanSettings;

    move-result-object v2

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/a;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/blescanner/compat/c;->k:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/gms/blescanner/compat/c;->d:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v0, v3, v2, v4}, Lcom/google/android/gms/blescanner/compat/a;->a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanSettings;Lcom/google/android/gms/blescanner/compat/v;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "BatchedBeaconsScanner serial scan launched."

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "    ** "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    const-string v0, "BatchedBeaconsScanner couldn\'t start serial scan"

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->f:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v0}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/blescanner/compat/c;->g:J

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    goto/16 :goto_0

    .line 307
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->l:Lcom/google/android/gms/blescanner/compat/v;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/c;->l:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/blescanner/compat/v;->a(Ljava/util/List;)V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->d()V

    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 311
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->b()V

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Batch scan: received "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " sightings; holding "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " deferred addresses."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 315
    monitor-exit p0

    return-void
.end method

.method private static b(Lcom/google/android/gms/blescanner/compat/ScanResult;)Lcom/google/android/gms/blescanner/compat/i;
    .locals 4

    .prologue
    .line 493
    new-instance v0, Lcom/google/android/gms/blescanner/compat/i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/blescanner/compat/i;-><init>(B)V

    .line 494
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/compat/ScanResult;->c()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/blescanner/compat/i;->a:I

    .line 495
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/compat/ScanResult;->d()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/blescanner/compat/i;->b:J

    .line 496
    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 377
    iget-wide v0, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/c;->f:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v2}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    const/4 v0, 0x1

    .line 378
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    .line 380
    iget-wide v2, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_2

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    .line 381
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->e()Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v2

    .line 382
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "BatchedBeaconsScanner serial scan stopping, because "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_4

    const-string v0, "[time limit expired] "

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v1, :cond_5

    const-string v0, "[no deferred sightings left to find] "

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    .line 387
    if-eqz v2, :cond_1

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->d:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/blescanner/compat/a;->a(Lcom/google/android/gms/blescanner/compat/v;)V

    .line 392
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 394
    iput-wide v6, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    .line 396
    :cond_2
    return-void

    .line 377
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 382
    :cond_4
    const-string v0, ""

    goto :goto_1

    :cond_5
    const-string v0, ""

    goto :goto_2
.end method

.method private c()Ljava/util/List;
    .locals 10

    .prologue
    .line 422
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Ljava/util/Map$Entry;

    .line 425
    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/blescanner/compat/i;

    .line 426
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/android/gms/blescanner/compat/g;

    new-instance v0, Lcom/google/android/gms/blescanner/compat/ScanResult;

    iget-object v1, v2, Lcom/google/android/gms/blescanner/compat/g;->a:Landroid/bluetooth/BluetoothDevice;

    iget-object v2, v2, Lcom/google/android/gms/blescanner/compat/g;->b:Lcom/google/android/gms/blescanner/compat/y;

    iget v3, v4, Lcom/google/android/gms/blescanner/compat/i;->a:I

    iget-wide v4, v4, Lcom/google/android/gms/blescanner/compat/i;->b:J

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/blescanner/compat/ScanResult;-><init>(Landroid/bluetooth/BluetoothDevice;Lcom/google/android/gms/blescanner/compat/y;IJ)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 429
    :cond_1
    return-object v7
.end method

.method private d()V
    .locals 6

    .prologue
    .line 447
    const-wide/16 v0, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->f()J

    move-result-wide v2

    sget-wide v4, Lcom/google/android/gms/blescanner/compat/c;->a:J

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 449
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 450
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/g;

    iget-wide v4, v0, Lcom/google/android/gms/blescanner/compat/g;->c:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_0

    .line 452
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 455
    :cond_1
    return-void
.end method

.method private e()Lcom/google/android/gms/blescanner/compat/a;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->c:Lcom/google/android/gms/blescanner/compat/h;

    invoke-interface {v0}, Lcom/google/android/gms/blescanner/compat/h;->a()Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v0

    return-object v0
.end method

.method private f()J
    .locals 4

    .prologue
    .line 530
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/c;->f:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v1}, Lcom/google/android/gms/blescanner/b/d;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public final a(IIJ)V
    .locals 3

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->e()Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v0

    .line 193
    iput-wide p3, p0, Lcom/google/android/gms/blescanner/compat/c;->g:J

    .line 194
    if-nez v0, :cond_0

    .line 195
    const-string v0, "BatchedBeaconsScanner setCustomScanTiming() error: no valid scanner"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    .line 199
    :goto_0
    return-void

    .line 197
    :cond_0
    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/blescanner/compat/a;->a(IIJ)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/blescanner/compat/v;)V
    .locals 4

    .prologue
    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->e()Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v0

    .line 168
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/blescanner/compat/c;->l:Lcom/google/android/gms/blescanner/compat/v;

    .line 169
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    .line 170
    if-eqz v0, :cond_0

    .line 171
    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/c;->d:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/blescanner/compat/a;->a(Lcom/google/android/gms/blescanner/compat/v;)V

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/c;->e:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/blescanner/compat/a;->a(Lcom/google/android/gms/blescanner/compat/v;)V

    .line 176
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 179
    return-void
.end method

.method public final a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanSettings;Lcom/google/android/gms/blescanner/compat/v;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 124
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/c;->e()Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v3

    .line 126
    if-nez v3, :cond_0

    .line 127
    const-string v0, "BatchedBeaconsScanner startScan() failed: called with no valid scanner"

    new-array v1, v2, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    .line 157
    :goto_0
    return v2

    .line 130
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->d()J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-nez v0, :cond_1

    .line 131
    const-string v0, "BatchedBeaconsScanner startScan() failed: called with zero report delay"

    new-array v1, v2, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    goto :goto_0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->d:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/blescanner/compat/a;->a(Lcom/google/android/gms/blescanner/compat/v;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->e:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/blescanner/compat/a;->a(Lcom/google/android/gms/blescanner/compat/v;)V

    .line 139
    iput-wide v6, p0, Lcom/google/android/gms/blescanner/compat/c;->m:J

    .line 140
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/c;->k:Ljava/util/List;

    .line 141
    iput-object p3, p0, Lcom/google/android/gms/blescanner/compat/c;->l:Lcom/google/android/gms/blescanner/compat/v;

    .line 143
    invoke-virtual {v3}, Lcom/google/android/gms/blescanner/compat/a;->a()Z

    move-result v4

    .line 144
    new-instance v0, Lcom/google/android/gms/blescanner/compat/ab;

    invoke-direct {v0}, Lcom/google/android/gms/blescanner/compat/ab;-><init>()V

    invoke-virtual {p2}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->a()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/gms/blescanner/compat/ab;->a(I)Lcom/google/android/gms/blescanner/compat/ab;

    move-result-object v0

    iput v1, v0, Lcom/google/android/gms/blescanner/compat/ab;->a:I

    invoke-virtual {p2}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->d()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/blescanner/compat/ab;->a(J)Lcom/google/android/gms/blescanner/compat/ab;

    move-result-object v5

    if-eqz v4, :cond_2

    move v0, v1

    :goto_1
    iput v0, v5, Lcom/google/android/gms/blescanner/compat/ab;->b:I

    invoke-virtual {v5}, Lcom/google/android/gms/blescanner/compat/ab;->a()Lcom/google/android/gms/blescanner/compat/ScanSettings;

    move-result-object v0

    .line 151
    iget-object v5, p0, Lcom/google/android/gms/blescanner/compat/c;->k:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/gms/blescanner/compat/c;->e:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v3, v5, v0, v6}, Lcom/google/android/gms/blescanner/compat/a;->a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanSettings;Lcom/google/android/gms/blescanner/compat/v;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "BatchedBeaconsScanner batch scan launched: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_3

    const-string v0, "SCAN_RESULT_TYPE_ABBREVIATED"

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    move v2, v1

    .line 154
    goto :goto_0

    :cond_2
    move v0, v2

    .line 144
    goto :goto_1

    .line 152
    :cond_3
    const-string v0, "SCAN_RESULT_TYPE_FULL"

    goto :goto_2

    .line 156
    :cond_4
    const-string v0, "BatchedBeaconsScanner couldn\'t start batch scan"

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
