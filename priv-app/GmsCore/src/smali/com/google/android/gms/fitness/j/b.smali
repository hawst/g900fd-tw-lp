.class public final Lcom/google/android/gms/fitness/j/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/gms/fitness/j/h;

.field public final c:Lcom/google/android/gms/fitness/j/h;

.field public final d:Lcom/google/android/gms/fitness/d/d;

.field public final e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/d/d;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/b;->a:Landroid/content/Context;

    .line 41
    new-instance v0, Lcom/google/android/gms/fitness/j/h;

    invoke-direct {v0, p2}, Lcom/google/android/gms/fitness/j/h;-><init>(Lcom/google/android/gms/fitness/sensors/a;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/b;->b:Lcom/google/android/gms/fitness/j/h;

    .line 42
    new-instance v0, Lcom/google/android/gms/fitness/j/h;

    invoke-direct {v0, p2}, Lcom/google/android/gms/fitness/j/h;-><init>(Lcom/google/android/gms/fitness/sensors/a;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/b;->c:Lcom/google/android/gms/fitness/j/h;

    .line 43
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/d/d;

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/b;->d:Lcom/google/android/gms/fitness/d/d;

    .line 44
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FitnessEventDispatcher"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 45
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 46
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/j/b;->e:Landroid/os/Handler;

    .line 47
    return-void
.end method
