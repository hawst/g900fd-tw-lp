.class final Lcom/google/android/gms/kids/account/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 22
    const-string v2, "KidAccountFixer"

    const-string v3, "fixing account"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 23
    new-instance v2, Lcom/google/android/gms/kids/account/b;

    invoke-direct {v2, p0}, Lcom/google/android/gms/kids/account/b;-><init>(Landroid/content/Context;)V

    .line 24
    invoke-virtual {v2}, Lcom/google/android/gms/kids/account/b;->c()Z

    move-result v3

    if-nez v3, :cond_0

    .line 25
    const-string v1, "KidAccountFixer"

    const-string v2, "Accounts are not ready."

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    :goto_0
    return v0

    .line 28
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/kids/account/b;->b()Landroid/accounts/Account;

    move-result-object v2

    .line 29
    if-nez v2, :cond_1

    .line 30
    const-string v2, "KidAccountFixer"

    const-string v3, "No required type accounts found"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v1

    .line 31
    goto :goto_0

    .line 33
    :cond_1
    iget-object v3, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/kids/common/h;->a(Ljava/lang/String;)V

    .line 35
    invoke-static {v1}, Lcom/google/android/gms/kids/common/h;->a(Z)V

    .line 36
    const-class v3, Lcom/google/android/gms/kids/timeouts/TimeoutsChangedReceiver;

    invoke-static {p0, v3}, Lcom/google/android/gms/kids/common/a;->a(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v3, Lcom/google/android/gms/kids/timeouts/DeviceTimeAndDateChangeReceiver;

    invoke-static {p0, v3}, Lcom/google/android/gms/kids/common/a;->a(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    const/16 v3, 0x15

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 38
    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.kids.account.receiver.UpdateProfileOwnerReceiver"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "is_kid_account_removed"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v0, "account_to_operate_on"

    iget-object v4, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 45
    :cond_2
    invoke-static {v2}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->b(Landroid/accounts/Account;)Z

    .line 46
    const/4 v0, 0x0

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->f()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/util/List;)Z

    .line 50
    invoke-static {v2}, Lcom/google/android/gms/kids/common/sync/KidsDataSyncService;->a(Landroid/accounts/Account;)Z

    move v0, v1

    .line 51
    goto :goto_0
.end method
