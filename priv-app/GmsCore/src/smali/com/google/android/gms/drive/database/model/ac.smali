.class public final Lcom/google/android/gms/drive/database/model/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:I

.field private c:Lcom/google/android/gms/drive/database/model/at;

.field private d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 298
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ac;->c:Lcom/google/android/gms/drive/database/model/at;

    .line 296
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/database/model/ac;->d:I

    .line 299
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ac;->a:Ljava/util/Map;

    .line 300
    const/16 v0, 0x2b

    iput v0, p0, Lcom/google/android/gms/drive/database/model/ac;->b:I

    .line 301
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/model/ab;
    .locals 3

    .prologue
    .line 382
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ac;->c:Lcom/google/android/gms/drive/database/model/at;

    if-eqz v0, :cond_0

    .line 384
    iget v0, p0, Lcom/google/android/gms/drive/database/model/ac;->b:I

    iget v1, p0, Lcom/google/android/gms/drive/database/model/ac;->d:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    .line 386
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/database/model/ab;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ac;->a:Ljava/util/Map;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/model/ab;-><init>(Ljava/util/Map;B)V

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/drive/database/model/ac;
    .locals 4

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ac;->c:Lcom/google/android/gms/drive/database/model/at;

    if-nez v0, :cond_0

    .line 362
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No field definition to remove"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 364
    :cond_0
    iget v0, p0, Lcom/google/android/gms/drive/database/model/ac;->d:I

    if-gt p1, v0, :cond_1

    .line 365
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Versions must be 0 or greater and specified in ascending order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 368
    :cond_1
    iget v0, p0, Lcom/google/android/gms/drive/database/model/ac;->d:I

    :goto_0
    if-ge v0, p1, :cond_2

    .line 369
    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ac;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/ac;->c:Lcom/google/android/gms/drive/database/model/at;

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 371
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ac;->c:Lcom/google/android/gms/drive/database/model/at;

    .line 372
    iput p1, p0, Lcom/google/android/gms/drive/database/model/ac;->d:I

    .line 373
    return-object p0
.end method

.method public final a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;
    .locals 11

    .prologue
    .line 317
    new-instance v0, Lcom/google/android/gms/drive/database/model/at;

    iget-object v1, p2, Lcom/google/android/gms/drive/database/model/au;->a:Ljava/lang/String;

    iget-object v2, p2, Lcom/google/android/gms/drive/database/model/au;->b:Lcom/google/android/gms/drive/database/model/aw;

    iget-boolean v3, p2, Lcom/google/android/gms/drive/database/model/au;->e:Z

    iget-object v4, p2, Lcom/google/android/gms/drive/database/model/au;->f:Ljava/util/Set;

    iget-boolean v5, p2, Lcom/google/android/gms/drive/database/model/au;->g:Z

    iget-object v6, p2, Lcom/google/android/gms/drive/database/model/au;->h:Ljava/lang/Object;

    iget-object v7, p2, Lcom/google/android/gms/drive/database/model/au;->c:Lcom/google/android/gms/drive/database/model/ae;

    iget-object v8, p2, Lcom/google/android/gms/drive/database/model/au;->d:Lcom/google/android/gms/drive/database/model/ab;

    iget-object v9, p2, Lcom/google/android/gms/drive/database/model/au;->i:Lcom/google/android/gms/drive/database/model/av;

    const/4 v10, 0x0

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/drive/database/model/at;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;ZLjava/util/Set;ZLjava/lang/Object;Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;B)V

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/ac;->c:Lcom/google/android/gms/drive/database/model/at;

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add a new field definition until the existing definition is removed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v1, p0, Lcom/google/android/gms/drive/database/model/ac;->d:I

    if-ge p1, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Versions must be 0 or greater and specified in ascending order"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ac;->c:Lcom/google/android/gms/drive/database/model/at;

    iput p1, p0, Lcom/google/android/gms/drive/database/model/ac;->d:I

    return-object p0
.end method
