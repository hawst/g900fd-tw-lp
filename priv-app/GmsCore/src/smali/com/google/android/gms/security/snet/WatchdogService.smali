.class public Lcom/google/android/gms/security/snet/WatchdogService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/gms/security/snet/WatchdogService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/WatchdogService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/gms/security/snet/WatchdogService;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method private static a()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 101
    const/4 v1, 0x1

    :try_start_0
    new-array v1, v1, [I

    const/4 v3, 0x0

    const v4, 0x534e4554

    aput v4, v1, v3

    invoke-static {v1, v0}, Landroid/util/EventLog;->readEvents([ILjava/util/Collection;)V

    .line 102
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/EventLog$Event;

    .line 103
    invoke-virtual {v0}, Landroid/util/EventLog$Event;->getData()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 106
    array-length v4, v0

    const/4 v5, 0x3

    if-ne v4, v5, :cond_1

    .line 107
    const-string v4, "do-not-log-execution-checkpoint"

    const/4 v5, 0x0

    aget-object v5, v0, v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 108
    const/4 v1, 0x2

    aget-object v0, v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    :goto_1
    move v1, v0

    .line 111
    goto :goto_0

    .line 113
    :catch_0
    move-exception v0

    move v1, v2

    .line 119
    :cond_0
    :goto_2
    return v1

    .line 115
    :catch_1
    move-exception v0

    move v1, v2

    goto :goto_2

    .line 117
    :catch_2
    move-exception v0

    move v1, v2

    goto :goto_2

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;IJ)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 164
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/security/snet/WatchdogService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 165
    const-string v1, "snet.watchdog.intent.extra.GMS_CORE_VERSION"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 166
    const-string v1, "snet.watchdog.intent.extra.TIMEOUT"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 167
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 142
    if-nez p0, :cond_1

    .line 152
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 146
    if-eqz v0, :cond_0

    .line 149
    const/4 v1, -0x1

    const-wide/16 v2, -0x1

    invoke-static {p0, v1, v2, v3}, Lcom/google/android/gms/security/snet/WatchdogService;->a(Landroid/content/Context;IJ)Landroid/app/PendingIntent;

    move-result-object v1

    .line 151
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 6

    .prologue
    .line 126
    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 127
    if-nez v0, :cond_0

    .line 134
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/security/a/a;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/security/a/a;->b()J

    move-result-wide v2

    .line 131
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    add-long/2addr v4, v2

    .line 132
    invoke-static {p0, p1, v2, v3}, Lcom/google/android/gms/security/snet/WatchdogService;->a(Landroid/content/Context;IJ)Landroid/app/PendingIntent;

    move-result-object v1

    .line 133
    const/4 v2, 0x0

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)J
    .locals 2

    .prologue
    .line 158
    invoke-static {p0}, Lcom/google/android/gms/security/a/a;->a(Landroid/content/Context;)V

    .line 159
    invoke-static {}, Lcom/google/android/gms/security/a/a;->b()J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public onCreate()V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 49
    return-void
.end method

.method public onHandleIntent(Landroid/content/Intent;)V
    .locals 8

    .prologue
    const v7, 0x534e4554

    const/4 v6, -0x1

    .line 63
    const-string v0, "snet.watchdog.intent.extra.GMS_CORE_VERSION"

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 64
    const-string v1, "snet.watchdog.intent.extra.TIMEOUT"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 66
    new-instance v1, Lcom/google/android/gms/security/snet/f;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/security/snet/f;-><init>(Landroid/content/Context;I)V

    .line 68
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/security/snet/SnetLaunchService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/snet/WatchdogService;->stopService(Landroid/content/Intent;)Z

    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/security/snet/WatchdogService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->xJ:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/security/snet/WatchdogService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 73
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/snet/WatchdogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 74
    if-nez v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 78
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 79
    iget-object v5, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->processName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 80
    invoke-static {v2, v3}, Lcom/google/android/gms/security/snet/f;->a(J)V

    .line 82
    sget-object v1, Lcom/google/android/gms/security/a/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 83
    invoke-static {}, Lcom/google/android/gms/security/snet/WatchdogService;->a()I

    move-result v1

    .line 84
    if-eqz v1, :cond_3

    .line 85
    int-to-long v2, v1

    invoke-static {v2, v3}, Lcom/google/android/gms/security/snet/f;->b(J)V

    .line 88
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "0"

    aput-object v3, v1, v2

    invoke-static {v7, v1}, Landroid/util/EventLog;->writeEvent(I[Ljava/lang/Object;)I

    .line 91
    :cond_3
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    invoke-static {v0}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0
.end method
