.class public final Lcom/google/android/gms/icing/c/a/aa;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/c/a/aa;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3118
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3119
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/aa;->cachedSize:I

    .line 3120
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/aa;
    .locals 2

    .prologue
    .line 3101
    sget-object v0, Lcom/google/android/gms/icing/c/a/aa;->c:[Lcom/google/android/gms/icing/c/a/aa;

    if-nez v0, :cond_1

    .line 3102
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3104
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/aa;->c:[Lcom/google/android/gms/icing/c/a/aa;

    if-nez v0, :cond_0

    .line 3105
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/aa;

    sput-object v0, Lcom/google/android/gms/icing/c/a/aa;->c:[Lcom/google/android/gms/icing/c/a/aa;

    .line 3107
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3109
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/aa;->c:[Lcom/google/android/gms/icing/c/a/aa;

    return-object v0

    .line 3107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3181
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3182
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3183
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3186
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3187
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3190
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3132
    if-ne p1, p0, :cond_1

    .line 3133
    const/4 v0, 0x1

    .line 3153
    :cond_0
    :goto_0
    return v0

    .line 3135
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/aa;

    if-eqz v1, :cond_0

    .line 3138
    check-cast p1, Lcom/google/android/gms/icing/c/a/aa;

    .line 3139
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 3140
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3146
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 3147
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3153
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/aa;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3143
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3150
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3158
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3161
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3163
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/aa;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3164
    return v0

    .line 3158
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3161
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3095
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/aa;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3170
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3171
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3173
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3174
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/aa;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3176
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3177
    return-void
.end method
