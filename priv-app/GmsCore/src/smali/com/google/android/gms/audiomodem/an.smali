.class public final Lcom/google/android/gms/audiomodem/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:I

.field public c:I

.field public d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 338
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/audiomodem/an;->e:I

    .line 339
    const v0, 0x472c4400    # 44100.0f

    iput v0, p0, Lcom/google/android/gms/audiomodem/an;->a:F

    .line 340
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/gms/audiomodem/an;->b:I

    .line 341
    const/16 v0, 0x32

    iput v0, p0, Lcom/google/android/gms/audiomodem/an;->c:I

    .line 342
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/audiomodem/an;->d:I

    .line 343
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/audiomodem/an;->f:I

    .line 344
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/audiomodem/Encoding;
    .locals 8

    .prologue
    .line 393
    new-instance v0, Lcom/google/android/gms/audiomodem/DtmfEncoding;

    iget v1, p0, Lcom/google/android/gms/audiomodem/an;->e:I

    iget v2, p0, Lcom/google/android/gms/audiomodem/an;->f:I

    invoke-static {v2}, Lcom/google/android/gms/audiomodem/Encoding;->b(I)Z

    move-result v2

    iget v3, p0, Lcom/google/android/gms/audiomodem/an;->a:F

    iget v4, p0, Lcom/google/android/gms/audiomodem/an;->b:I

    iget v5, p0, Lcom/google/android/gms/audiomodem/an;->c:I

    iget v6, p0, Lcom/google/android/gms/audiomodem/an;->d:I

    iget v7, p0, Lcom/google/android/gms/audiomodem/an;->f:I

    invoke-static {v7}, Lcom/google/android/gms/audiomodem/Encoding;->c(I)I

    move-result v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/audiomodem/DtmfEncoding;-><init>(IZFIIII)V

    .line 397
    new-instance v1, Lcom/google/android/gms/audiomodem/Encoding;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/audiomodem/Encoding;-><init>(Lcom/google/android/gms/audiomodem/DtmfEncoding;B)V

    return-object v1
.end method

.method public final a(I)Lcom/google/android/gms/audiomodem/an;
    .locals 2

    .prologue
    .line 351
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "tokenLengthBytes must be greater than zero"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 353
    iput p1, p0, Lcom/google/android/gms/audiomodem/an;->e:I

    .line 354
    return-object p0

    .line 351
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Lcom/google/android/gms/audiomodem/an;
    .locals 2

    .prologue
    .line 386
    invoke-static {p1}, Lcom/google/android/gms/audiomodem/Encoding;->a(I)Z

    move-result v0

    const-string v1, "Not a valid ErrorControlScheme"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 388
    iput p1, p0, Lcom/google/android/gms/audiomodem/an;->f:I

    .line 389
    return-object p0
.end method
