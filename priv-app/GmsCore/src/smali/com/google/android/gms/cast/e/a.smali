.class public final Lcom/google/android/gms/cast/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/cast/internal/k;


# instance fields
.field private final b:Lcom/google/android/gms/common/api/v;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "AudioModemHelper"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/e/a;->a:Lcom/google/android/gms/cast/internal/k;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/audiomodem/d;->b:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/e/a;->b:Lcom/google/android/gms/common/api/v;

    .line 27
    return-void
.end method

.method static synthetic c()Lcom/google/android/gms/cast/internal/k;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/gms/cast/e/a;->a:Lcom/google/android/gms/cast/internal/k;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/cast/e/a;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "connecting client"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/cast/e/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 32
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/bm;)V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/cast/e/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-static {v0, p1}, Lcom/google/android/gms/audiomodem/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/cast/e/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/e/c;-><init>(Lcom/google/android/gms/cast/e/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 86
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/cast/e/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/audiomodem/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/cast/e/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/e/b;-><init>(Lcom/google/android/gms/cast/e/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 59
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/cast/e/a;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "disconnecting client"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/cast/e/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 37
    return-void
.end method
