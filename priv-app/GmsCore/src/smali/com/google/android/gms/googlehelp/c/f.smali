.class public final Lcom/google/android/gms/googlehelp/c/f;
.super Landroid/widget/CheckBox;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/c/af;
.implements Lcom/google/android/gms/googlehelp/c/c;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# instance fields
.field private final a:Landroid/widget/EditText;

.field private final b:Z

.field private c:Lcom/google/android/gms/googlehelp/c/d;

.field private d:Lcom/google/android/gms/googlehelp/c/ac;

.field private e:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/c/d;Lcom/google/ad/a/a/k;)V
    .locals 2

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/f;->c:Lcom/google/android/gms/googlehelp/c/d;

    .line 56
    iget-boolean v0, p3, Lcom/google/ad/a/a/k;->g:Z

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/f;->b:Z

    .line 57
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/f;->b:Z

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Lcom/google/android/gms/googlehelp/c/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/c/g;-><init>(Lcom/google/android/gms/googlehelp/c/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/f;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 68
    :cond_0
    iget-object v0, p3, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/f;->setTag(Ljava/lang/Object;)V

    .line 69
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p3, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p3, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/f;->b:Z

    if-eqz v0, :cond_2

    const-string v0, " *"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/f;->setText(Ljava/lang/CharSequence;)V

    .line 71
    iget-boolean v0, p3, Lcom/google/ad/a/a/k;->c:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/f;->setChecked(Z)V

    .line 72
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/f;->b:Z

    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;Z)V

    .line 74
    iget-boolean v0, p3, Lcom/google/ad/a/a/k;->d:Z

    if-eqz v0, :cond_3

    .line 75
    invoke-static {p1, p0}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/content/Context;Landroid/widget/CompoundButton;)Landroid/widget/EditText;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->a:Landroid/widget/EditText;

    .line 79
    :goto_2
    return-void

    .line 69
    :cond_1
    const-string v0, ""

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1

    .line 77
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->a:Landroid/widget/EditText;

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/c/f;)Lcom/google/android/gms/googlehelp/c/ac;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->d:Lcom/google/android/gms/googlehelp/c/ac;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/c/ac;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/f;->d:Lcom/google/android/gms/googlehelp/c/ac;

    .line 135
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 91
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/f;->e:Ljava/util/List;

    .line 92
    new-instance v0, Lcom/google/android/gms/googlehelp/c/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/c/h;-><init>(Lcom/google/android/gms/googlehelp/c/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/f;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 98
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/f;->isChecked()Z

    move-result v0

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->c:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/d;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->a:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->a:Landroid/widget/EditText;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/f;->isChecked()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 111
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->e:Ljava/util/List;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/c/ad;->a(Ljava/util/List;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->d:Lcom/google/android/gms/googlehelp/c/ac;

    if-eqz v0, :cond_0

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/f;->d:Lcom/google/android/gms/googlehelp/c/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/c/ac;->a()V

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/c/f;->b:Z

    return v0
.end method
