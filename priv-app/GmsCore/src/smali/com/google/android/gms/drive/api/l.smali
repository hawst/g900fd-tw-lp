.class public final Lcom/google/android/gms/drive/api/l;
.super Lcom/google/android/gms/drive/realtime/internal/ah;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/drive/api/d;

.field private c:Landroid/os/IBinder;

.field private final d:Lcom/google/c/a/a/a/a/a;

.field private final e:Lcom/google/android/gms/drive/realtime/e;

.field private final f:Lcom/google/c/a/a/b/b/a/b;

.field private g:Lcom/google/android/gms/drive/realtime/internal/ad;

.field private final h:Lcom/google/android/gms/drive/realtime/b/a;

.field private final i:Lcom/google/android/gms/common/util/p;

.field private j:Z

.field private k:Lcom/google/android/gms/drive/realtime/internal/u;

.field private l:Lcom/google/android/gms/drive/g/n;

.field private m:Z

.field private n:Lcom/google/android/gms/drive/api/q;

.field private o:Ljava/util/concurrent/ExecutorService;

.field private p:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final q:Ljava/lang/Runnable;

.field private final r:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/c/a/a/a/a/a;Landroid/os/IBinder;Lcom/google/android/gms/drive/realtime/e;Lcom/google/android/gms/common/util/p;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    invoke-direct {p0}, Lcom/google/android/gms/drive/realtime/internal/ah;-><init>()V

    .line 94
    iput-boolean v2, p0, Lcom/google/android/gms/drive/api/l;->m:Z

    .line 95
    new-instance v0, Lcom/google/android/gms/drive/api/q;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/drive/api/q;-><init>(Lcom/google/android/gms/drive/api/l;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->n:Lcom/google/android/gms/drive/api/q;

    .line 96
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->o:Ljava/util/concurrent/ExecutorService;

    .line 98
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 100
    new-instance v0, Lcom/google/android/gms/drive/api/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/api/m;-><init>(Lcom/google/android/gms/drive/api/l;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->q:Ljava/lang/Runnable;

    .line 110
    new-instance v0, Lcom/google/android/gms/drive/api/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/api/n;-><init>(Lcom/google/android/gms/drive/api/l;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->r:Ljava/lang/Runnable;

    .line 126
    iput-object p1, p0, Lcom/google/android/gms/drive/api/l;->b:Lcom/google/android/gms/drive/api/d;

    .line 127
    iput-object p3, p0, Lcom/google/android/gms/drive/api/l;->c:Landroid/os/IBinder;

    .line 128
    iput-object p2, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    .line 129
    iget-object v0, p2, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    .line 130
    invoke-interface {p1}, Lcom/google/android/gms/drive/api/d;->q()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/api/l;->a:Landroid/content/Context;

    .line 132
    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/realtime/b/a;->a(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)Lcom/google/android/gms/drive/realtime/b/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->h:Lcom/google/android/gms/drive/realtime/b/a;

    .line 134
    iput-object p4, p0, Lcom/google/android/gms/drive/api/l;->e:Lcom/google/android/gms/drive/realtime/e;

    .line 135
    new-instance v0, Lcom/google/android/gms/drive/g/p;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/g/p;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->l:Lcom/google/android/gms/drive/g/n;

    .line 136
    iput-object p5, p0, Lcom/google/android/gms/drive/api/l;->i:Lcom/google/android/gms/common/util/p;

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->l:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->o:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->q:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 141
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 142
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 143
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 145
    invoke-interface {p3, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/l;->n:Lcom/google/android/gms/drive/api/q;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 147
    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/b/g/d;)Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;
    .locals 1

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/android/gms/drive/api/l;->b(Lcom/google/c/a/a/b/g/d;)Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;
    .locals 2

    .prologue
    .line 721
    new-instance v0, Lcom/google/android/gms/drive/realtime/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/realtime/d;-><init>(Lcom/google/c/a/a/b/b/a/b;)V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/d;->a()Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/f;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/f;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/data/DataHolder;)Ljava/util/List;
    .locals 4

    .prologue
    .line 682
    new-instance v1, Lcom/google/android/gms/drive/realtime/a/d;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-direct {v1, v0, p1}, Lcom/google/android/gms/drive/realtime/a/d;-><init>(Lcom/google/c/a/a/b/b/a/b;Lcom/google/android/gms/common/data/DataHolder;)V

    .line 683
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 684
    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 685
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 687
    :cond_0
    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->d()V

    .line 688
    return-object v2
.end method

.method private static a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    .locals 1

    .prologue
    .line 728
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 729
    if-eqz v0, :cond_0

    .line 730
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 732
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/api/l;)Z
    .locals 1

    .prologue
    .line 72
    iget-boolean v0, p0, Lcom/google/android/gms/drive/api/l;->m:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/api/l;Z)Z
    .locals 0

    .prologue
    .line 72
    iput-boolean p1, p0, Lcom/google/android/gms/drive/api/l;->m:Z

    return p1
.end method

.method private static b(Lcom/google/c/a/a/b/g/d;)Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;
    .locals 8

    .prologue
    .line 671
    new-instance v0, Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/g/d;->c()Z

    move-result v1

    invoke-virtual {p0}, Lcom/google/c/a/a/b/g/d;->b()Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/c/a/a/b/g/d;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/c/a/a/b/g/d;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/c/a/a/b/g/d;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0}, Lcom/google/c/a/a/b/g/d;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/c/a/a/b/g/d;->g()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;-><init>(ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/api/l;)Lcom/google/c/a/a/a/a/a;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/h;
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/h;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/drive/api/l;)Lcom/google/android/gms/drive/g/n;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->l:Lcom/google/android/gms/drive/g/n;

    return-object v0
.end method

.method private c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/e;

    return-object v0
.end method

.method private d(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/l;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/l;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/api/l;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->r:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/drive/api/l;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->o:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/drive/api/l;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->q:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 480
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->p:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->compareAndSet(ZZ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 482
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->h:Lcom/google/android/gms/drive/realtime/b/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/b/a;->d()Lcom/google/android/gms/drive/realtime/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->e:Lcom/google/android/gms/drive/realtime/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/realtime/b;->b(Lcom/google/android/gms/drive/realtime/e;)V

    .line 483
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->n:Lcom/google/android/gms/drive/api/q;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->b:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->j()V

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->c:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->c:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 487
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/l;->c:Landroid/os/IBinder;

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    sget-object v1, Lcom/google/c/a/a/b/e/g;->b:Lcom/google/c/a/a/b/e/g;

    iget-object v2, v0, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v2, v1}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/e/g;)V

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/c/a/a/a/a/a;->c:Lcom/google/c/a/a/b/a/e;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 494
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 490
    :catch_0
    move-exception v0

    .line 491
    :try_start_2
    const-string v1, "RealtimeService"

    const-string v2, "Failed to close the document properly."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 480
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(ILcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 554
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->i:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x5

    add-long/2addr v2, v4

    .line 555
    new-instance v1, Lcom/google/android/gms/drive/realtime/d;

    iget-object v4, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-direct {v1, v4}, Lcom/google/android/gms/drive/realtime/d;-><init>(Lcom/google/c/a/a/b/b/a/b;)V

    .line 557
    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    invoke-virtual {v4}, Lcom/google/c/a/a/a/a/a;->a()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 558
    add-int/lit8 v0, v0, 0x1

    .line 559
    iget-object v4, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v5, v4, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    if-eqz v5, :cond_3

    iget-object v4, v4, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v4}, Lcom/google/c/a/a/b/e/i;->a()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/gms/drive/realtime/d;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/d;

    .line 560
    iget-object v4, p0, Lcom/google/android/gms/drive/api/l;->i:Lcom/google/android/gms/common/util/p;

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v4

    cmp-long v4, v4, v2

    if-lez v4, :cond_0

    .line 561
    const-string v2, "RealtimeService"

    const-string v3, "Returning to UI thread after applying %d changes (time limit exceeded)."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 563
    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/d;->a()Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 568
    :try_start_1
    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 570
    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 574
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 575
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/l;->b()V

    .line 580
    :cond_2
    return-void

    .line 559
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OT manager has not been initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 577
    :catch_0
    move-exception v0

    .line 578
    const-string v1, "RealtimeService"

    const-string v2, "Package-side exception caught in apply changes."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 579
    throw v0

    .line 570
    :catchall_0
    move-exception v1

    :try_start_3
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->k:Lcom/google/android/gms/drive/realtime/internal/u;

    if-eqz v0, :cond_0

    .line 628
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->k:Lcom/google/android/gms/drive/realtime/internal/u;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/realtime/internal/u;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 634
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 635
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/l;->a()V

    .line 637
    :cond_1
    return-void

    .line 629
    :catch_0
    move-exception v0

    .line 630
    const-string v1, "RealtimeService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to deliver error: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/BeginCompoundOperationRequest;Lcom/google/android/gms/drive/realtime/internal/am;)V
    .locals 3

    .prologue
    .line 396
    invoke-virtual {p1}, Lcom/google/android/gms/drive/realtime/internal/BeginCompoundOperationRequest;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->b()V

    .line 401
    :goto_0
    invoke-interface {p2}, Lcom/google/android/gms/drive/realtime/internal/am;->a()V

    .line 402
    return-void

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/realtime/internal/BeginCompoundOperationRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/realtime/internal/BeginCompoundOperationRequest;->c()Z

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;Lcom/google/android/gms/drive/realtime/internal/am;)V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->c()Lcom/google/c/a/a/b/b/a/a;

    .line 429
    invoke-interface {p2}, Lcom/google/android/gms/drive/realtime/internal/am;->a()V

    .line 430
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/EndCompoundOperationRequest;Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 409
    const/4 v1, 0x0

    .line 411
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->c()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v1

    .line 413
    invoke-interface {p2, v1}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 415
    if-eqz v1, :cond_0

    .line 416
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 419
    :cond_0
    return-void

    .line 415
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 416
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    :cond_1
    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;Lcom/google/android/gms/drive/realtime/internal/aj;)V
    .locals 4

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->f()Lcom/google/c/a/a/b/b/a/l;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;->b()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;->c()Z

    move-result v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/c/a/a/b/b/a/l;->a(Lcom/google/c/a/a/b/b/a/g;IZ)V

    .line 507
    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/l;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/aj;->a(Ljava/lang/String;)V

    .line 508
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/ad;)V
    .locals 1

    .prologue
    .line 498
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->g()I

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/realtime/internal/ad;->a(I)V

    .line 499
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/am;)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->d()Z

    .line 170
    invoke-interface {p1}, Lcom/google/android/gms/drive/realtime/internal/am;->a()V

    .line 171
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/c;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->e()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/realtime/internal/c;->a(Z)V

    .line 165
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/f;)V
    .locals 2

    .prologue
    .line 601
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/a/a;->b:Lcom/google/c/a/a/a/b/b;

    new-instance v1, Lcom/google/android/gms/drive/api/o;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/drive/api/o;-><init>(Lcom/google/android/gms/drive/api/l;Lcom/google/android/gms/drive/realtime/internal/f;)V

    iput-object v1, v0, Lcom/google/c/a/a/a/b/b;->c:Lcom/google/c/a/a/a/b/a;

    .line 618
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/i;)V
    .locals 4

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/a/a;->b:Lcom/google/c/a/a/a/b/b;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/b/b;->a()Ljava/util/Collection;

    move-result-object v1

    .line 588
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v2, v0, [Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;

    .line 589
    const/4 v0, 0x0

    .line 590
    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/g/d;

    .line 591
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->b(Lcom/google/c/a/a/b/g/d;)Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;

    move-result-object v0

    aput-object v0, v2, v1

    .line 592
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 593
    goto :goto_0

    .line 594
    :cond_0
    invoke-interface {p1, v2}, Lcom/google/android/gms/drive/realtime/internal/i;->a([Lcom/google/android/gms/drive/realtime/internal/ParcelableCollaborator;)V

    .line 595
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/r;)V
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    new-instance v1, Lcom/google/android/gms/drive/api/p;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/drive/api/p;-><init>(Lcom/google/android/gms/drive/api/l;Lcom/google/android/gms/drive/realtime/internal/r;)V

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/e/d;)V

    .line 654
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/u;)V
    .locals 0

    .prologue
    .line 622
    iput-object p1, p0, Lcom/google/android/gms/drive/api/l;->k:Lcom/google/android/gms/drive/realtime/internal/u;

    .line 623
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->h()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 437
    :try_start_0
    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 439
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 440
    return-void

    .line 439
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;IILcom/google/android/gms/drive/realtime/internal/o;)V
    .locals 4

    .prologue
    .line 342
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;

    move-result-object v0

    .line 343
    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/e;->b()Ljava/util/List;

    move-result-object v1

    add-int v2, p2, p3

    invoke-interface {v1, p2, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 344
    iget-object v2, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-static {v2, v1}, Lcom/google/android/gms/drive/realtime/a/a;->a(Lcom/google/c/a/a/b/b/a/b;Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 345
    const/4 v1, 0x0

    .line 347
    add-int v3, p2, p3

    :try_start_0
    invoke-interface {v0, p2, v3}, Lcom/google/c/a/a/b/b/a/e;->a(II)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v1

    .line 349
    invoke-interface {p4, v2, v1}, Lcom/google/android/gms/drive/realtime/internal/o;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 351
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 352
    if-eqz v1, :cond_0

    .line 353
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 356
    :cond_0
    return-void

    .line 351
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 352
    if-eqz v1, :cond_1

    .line 353
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    :cond_1
    throw v0
.end method

.method public final a(Ljava/lang/String;IILcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/h;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/c/a/a/b/b/a/h;->a(II)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 272
    :try_start_0
    invoke-interface {p4, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 274
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 275
    return-void

    .line 274
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;ILcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/o;)V
    .locals 4

    .prologue
    .line 320
    new-instance v0, Lcom/google/android/gms/drive/realtime/a/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-direct {v0, v1, p3}, Lcom/google/android/gms/drive/realtime/a/d;-><init>(Lcom/google/c/a/a/b/b/a/b;Lcom/google/android/gms/common/data/DataHolder;)V

    .line 321
    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/a/d;->c()I

    move-result v0

    add-int/2addr v0, p2

    .line 322
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;

    move-result-object v2

    .line 323
    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v2}, Lcom/google/c/a/a/b/b/a/e;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/realtime/a/a;->a(Lcom/google/c/a/a/b/b/a/b;Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    .line 325
    const/4 v1, 0x0

    .line 327
    :try_start_0
    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/common/data/DataHolder;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v2, p2, v0}, Lcom/google/c/a/a/b/b/a/e;->a(ILjava/util/List;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v1

    .line 330
    invoke-interface {p4, v3, v1}, Lcom/google/android/gms/drive/realtime/internal/o;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 333
    if-eqz v1, :cond_0

    .line 334
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 337
    :cond_0
    return-void

    .line 332
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 333
    if-eqz v1, :cond_1

    .line 334
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    :cond_1
    throw v0
.end method

.method public final a(Ljava/lang/String;ILcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 308
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;

    move-result-object v0

    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/common/data/DataHolder;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Lcom/google/c/a/a/b/b/a/e;->a(ILjava/util/Collection;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 311
    :try_start_0
    invoke-interface {p4, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 313
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 314
    return-void

    .line 313
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;ILcom/google/android/gms/drive/realtime/internal/am;)V
    .locals 1

    .prologue
    .line 521
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->d(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/l;

    move-result-object v0

    .line 522
    invoke-interface {v0, p2}, Lcom/google/c/a/a/b/b/a/l;->a(I)Lcom/google/c/a/a/b/b/a/a;

    .line 523
    invoke-interface {p3}, Lcom/google/android/gms/drive/realtime/internal/am;->a()V

    .line 524
    return-void
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;ILcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 3

    .prologue
    .line 361
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;

    move-result-object v0

    .line 362
    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/api/l;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;

    move-result-object v2

    .line 363
    const/4 v1, 0x0

    .line 365
    :try_start_0
    invoke-interface {v0, p2, v2, p4}, Lcom/google/c/a/a/b/b/a/e;->a(ILcom/google/c/a/a/b/b/a/e;I)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v1

    .line 367
    invoke-interface {p5, v1}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    if-eqz v1, :cond_0

    .line 370
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 373
    :cond_0
    return-void

    .line 369
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 370
    invoke-static {v1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    :cond_1
    throw v0
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 257
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/h;

    move-result-object v0

    invoke-interface {v0, p2, p3}, Lcom/google/c/a/a/b/b/a/h;->a(ILjava/lang/String;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 260
    :try_start_0
    invoke-interface {p4, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 262
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 263
    return-void

    .line 262
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 5

    .prologue
    .line 189
    new-instance v1, Lcom/google/android/gms/drive/realtime/a/e;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-direct {v1, v0, p2}, Lcom/google/android/gms/drive/realtime/a/e;-><init>(Lcom/google/c/a/a/b/b/a/b;Lcom/google/android/gms/common/data/DataHolder;)V

    new-instance v2, Ljava/util/HashMap;

    invoke-virtual {p2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashMap;-><init>(I)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/realtime/a/e;->c(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/realtime/a/e;->b(I)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/a/e;->d()V

    .line 190
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/f;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/c/a/a/b/b/a/f;->a(Ljava/util/Map;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 191
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 194
    :try_start_0
    invoke-interface {p3, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 197
    return-void

    .line 196
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/aa;)V
    .locals 4

    .prologue
    .line 513
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->d(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/l;

    move-result-object v0

    .line 514
    new-instance v1, Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/l;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/l;->b()I

    move-result v3

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/l;->c()Z

    move-result v0

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;-><init>(Ljava/lang/String;IZ)V

    invoke-interface {p2, v1}, Lcom/google/android/gms/drive/realtime/internal/aa;->a(Lcom/google/android/gms/drive/realtime/internal/ParcelableIndexReference;)V

    .line 516
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/ad;)V
    .locals 1

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/f;->a()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/ad;->a(I)V

    .line 220
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/aj;)V
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/b/a/b;->b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v0

    .line 159
    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/aj;->a(Ljava/lang/String;)V

    .line 160
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/am;)V
    .locals 1

    .prologue
    .line 659
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/b/a/b;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/g;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/g;->m()V

    .line 660
    invoke-interface {p2}, Lcom/google/android/gms/drive/realtime/internal/am;->a()V

    .line 661
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/l;)V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/f;->c()Ljava/util/Set;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/realtime/a/c;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/realtime/a/c;-><init>(Lcom/google/c/a/a/b/b/a/b;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/drive/realtime/a/c;->a(Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 238
    :try_start_0
    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/l;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 241
    return-void

    .line 240
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v1
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 225
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/f;->b()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 228
    :try_start_0
    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 230
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 231
    return-void

    .line 230
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/l;)V
    .locals 2

    .prologue
    .line 176
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/f;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/c/a/a/b/b/a/f;->b(Ljava/lang/Object;)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v0

    .line 177
    iget-object v1, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/realtime/a/a;->a(Lcom/google/c/a/a/b/b/a/b;Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 180
    :try_start_0
    invoke-interface {p3, v0}, Lcom/google/android/gms/drive/realtime/internal/l;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 182
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 183
    return-void

    .line 182
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/o;)V
    .locals 3

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/f;

    move-result-object v0

    .line 204
    invoke-interface {v0, p2}, Lcom/google/c/a/a/b/b/a/f;->b(Ljava/lang/Object;)Lcom/google/c/a/a/b/b/a/r;

    move-result-object v1

    .line 205
    invoke-interface {v0, p2}, Lcom/google/c/a/a/b/b/a/f;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/drive/realtime/a/a;->a(Lcom/google/c/a/a/b/b/a/b;Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 210
    :try_start_0
    invoke-interface {p3, v1, v0}, Lcom/google/android/gms/drive/realtime/internal/o;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 213
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 214
    return-void

    .line 212
    :catchall_0
    move-exception v2

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 213
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v2
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 281
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/h;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/c/a/a/b/b/a/h;->a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 284
    :try_start_0
    invoke-interface {p3, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 286
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 287
    return-void

    .line 286
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->g:Lcom/google/android/gms/drive/realtime/internal/ad;

    if-eqz v0, :cond_0

    .line 542
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->g:Lcom/google/android/gms/drive/realtime/internal/ad;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/realtime/internal/ad;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 549
    :goto_0
    return-void

    .line 543
    :catch_0
    move-exception v0

    .line 544
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 547
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/api/l;->j:Z

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/drive/realtime/internal/ad;)V
    .locals 1

    .prologue
    .line 528
    iput-object p1, p0, Lcom/google/android/gms/drive/api/l;->g:Lcom/google/android/gms/drive/realtime/internal/ad;

    .line 529
    iget-boolean v0, p0, Lcom/google/android/gms/drive/api/l;->j:Z

    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/l;->b()V

    .line 531
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/api/l;->j:Z

    .line 533
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/realtime/internal/am;)V
    .locals 0

    .prologue
    .line 471
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/l;->a()V

    .line 472
    invoke-interface {p1}, Lcom/google/android/gms/drive/realtime/internal/am;->a()V

    .line 473
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/realtime/internal/c;)V
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->j()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/realtime/internal/c;->a(Z)V

    .line 457
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/realtime/internal/x;)V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->i()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/c/a/a/b/b/a/a;)Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;

    move-result-object v0

    .line 448
    :try_start_0
    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/realtime/internal/x;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 450
    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    .line 451
    return-void

    .line 450
    :catchall_0
    move-exception v1

    invoke-static {v0}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/drive/realtime/internal/event/ParcelableEventList;)V

    throw v1
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/ad;)V
    .locals 1

    .prologue
    .line 246
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/h;->a()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/ad;->a(I)V

    .line 247
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/aj;)V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->b(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/h;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/aj;->a(Ljava/lang/String;)V

    .line 252
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/am;)V
    .locals 0

    .prologue
    .line 667
    invoke-interface {p2}, Lcom/google/android/gms/drive/realtime/internal/am;->a()V

    .line 668
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/l;)V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/e;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/a/a;->a(Lcom/google/c/a/a/b/b/a/b;Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 294
    :try_start_0
    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/l;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 296
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 297
    return-void

    .line 296
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v1
.end method

.method public final binderDied()V
    .locals 0

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/l;->a()V

    .line 154
    return-void
.end method

.method public final c(Lcom/google/android/gms/drive/realtime/internal/c;)V
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-object v0, v0, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->k()Z

    move-result v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/realtime/internal/c;->a(Z)V

    .line 462
    return-void
.end method

.method public final c(Ljava/lang/String;Lcom/google/android/gms/drive/realtime/internal/ad;)V
    .locals 1

    .prologue
    .line 302
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/l;->c(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/e;->a()I

    move-result v0

    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/realtime/internal/ad;->a(I)V

    .line 303
    return-void
.end method

.method public final d(Lcom/google/android/gms/drive/realtime/internal/c;)V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/gms/drive/api/l;->d:Lcom/google/c/a/a/a/a/a;

    iget-boolean v0, v0, Lcom/google/c/a/a/a/a/a;->g:Z

    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/realtime/internal/c;->a(Z)V

    .line 467
    return-void
.end method
