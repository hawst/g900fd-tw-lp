.class final Lcom/google/android/gms/drive/events/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/events/l;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/events/l;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/gms/drive/events/m;->a:Lcom/google/android/gms/drive/events/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 84
    iget-object v1, p0, Lcom/google/android/gms/drive/events/m;->a:Lcom/google/android/gms/drive/events/l;

    const-string v0, "EventDistributorImpl"

    const-string v2, "renotify"

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v1, Lcom/google/android/gms/drive/events/l;->e:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, v1, Lcom/google/android/gms/drive/events/l;->b:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    iget-object v0, v1, Lcom/google/android/gms/drive/events/l;->a:Lcom/google/android/gms/drive/events/t;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/drive/database/r;->h(J)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bl;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/events/l;->a(Lcom/google/android/gms/drive/database/model/bl;)V

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/drive/events/l;->a()V

    .line 85
    :cond_1
    :goto_1
    return-void

    .line 84
    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/drive/events/l;->a:Lcom/google/android/gms/drive/events/t;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bp;->a()Lcom/google/android/gms/drive/database/model/bp;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ae;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/events/l;->a()V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method
