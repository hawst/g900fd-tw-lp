.class final Lcom/google/android/gms/drive/database/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/database/i;

.field private b:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/database/i;)V
    .locals 0

    .prologue
    .line 585
    iput-object p1, p0, Lcom/google/android/gms/drive/database/k;->a:Lcom/google/android/gms/drive/database/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private declared-synchronized b()Landroid/database/sqlite/SQLiteDatabase;
    .locals 4

    .prologue
    .line 590
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/k;->b:Landroid/database/sqlite/SQLiteDatabase;

    if-nez v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/drive/database/k;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/i;->a:Lcom/google/android/gms/drive/database/p;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/p;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/k;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 592
    iget-object v0, p0, Lcom/google/android/gms/drive/database/k;->b:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "PRAGMA foreign_keys=ON;"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 594
    sget-object v0, Lcom/google/android/gms/drive/database/f;->a:Lcom/google/android/gms/common/a/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/k;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->i()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/k;->b:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 590
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 585
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/k;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method
