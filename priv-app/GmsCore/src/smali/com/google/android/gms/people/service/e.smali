.class public final Lcom/google/android/gms/people/service/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/gms/people/service/g;

.field private final c:Lcom/google/android/gms/people/service/g;

.field private final d:Lcom/google/android/gms/people/service/g;

.field private final e:Lcom/google/android/gms/plus/service/v1whitelisted/d;

.field private final f:Lcom/google/android/gms/plus/service/v1whitelisted/i;

.field private final g:Lcom/google/android/gms/plus/service/v2whitelisted/b;

.field private final h:Lcom/google/android/gms/plus/service/v1whitelisted/e;

.field private final i:Lcom/google/android/gms/plus/service/v1whitelisted/b;

.field private final j:Lcom/google/android/gms/plus/service/v1whitelisted/l;

.field private final k:Lcom/google/android/gms/plus/service/v2whitelisted/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 110
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.circles.read"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/plus.circles.write"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.media.upload"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://www.googleapis.com/auth/plus.pages.manage"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "https://www.googleapis.com/auth/plus.profiles.read"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "https://www.googleapis.com/auth/plus.profiles.write"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "https://www.googleapis.com/auth/plus.stream.read"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "https://www.googleapis.com/auth/plus.peopleapi.readwrite"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "https://www.googleapis.com/auth/plus.applications.manage"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "https://www.googleapis.com/auth/plus.settings"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/service/e;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 169
    invoke-static {p1}, Lcom/google/android/gms/people/service/g;->c(Landroid/content/Context;)Lcom/google/android/gms/people/service/g;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/people/service/g;->b(Landroid/content/Context;)Lcom/google/android/gms/people/service/g;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/people/service/g;->d(Landroid/content/Context;)Lcom/google/android/gms/people/service/g;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/people/service/e;-><init>(Lcom/google/android/gms/people/service/g;Lcom/google/android/gms/people/service/g;Lcom/google/android/gms/people/service/g;)V

    .line 172
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/people/service/g;Lcom/google/android/gms/people/service/g;Lcom/google/android/gms/people/service/g;)V
    .locals 2

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p1, p0, Lcom/google/android/gms/people/service/e;->b:Lcom/google/android/gms/people/service/g;

    .line 181
    iput-object p2, p0, Lcom/google/android/gms/people/service/e;->c:Lcom/google/android/gms/people/service/g;

    .line 182
    iput-object p3, p0, Lcom/google/android/gms/people/service/e;->d:Lcom/google/android/gms/people/service/g;

    .line 183
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/d;

    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->b:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/d;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/e;->e:Lcom/google/android/gms/plus/service/v1whitelisted/d;

    .line 184
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->b:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/i;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/e;->f:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    .line 185
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->d:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v2whitelisted/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/e;->g:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    .line 187
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/e;

    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->b:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/e;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/e;->h:Lcom/google/android/gms/plus/service/v1whitelisted/e;

    .line 188
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/b;

    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->b:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/e;->i:Lcom/google/android/gms/plus/service/v1whitelisted/b;

    .line 189
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/l;

    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->b:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/l;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/e;->j:Lcom/google/android/gms/plus/service/v1whitelisted/l;

    .line 190
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/a;

    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->d:Lcom/google/android/gms/people/service/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/service/v2whitelisted/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/e;->k:Lcom/google/android/gms/plus/service/v2whitelisted/a;

    .line 192
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 3

    .prologue
    .line 147
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, p1, v2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    sget-object v1, Lcom/google/android/gms/people/service/e;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    .line 150
    const-string v1, "social_client_app_id"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/e/a;
    .locals 2

    .prologue
    .line 158
    invoke-static {p0, p1, p3}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 160
    new-instance v1, Lcom/google/android/gms/people/e/a;

    invoke-direct {v1, v0, p2}, Lcom/google/android/gms/people/e/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    return-object v1
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/people/service/e;
    .locals 1

    .prologue
    .line 195
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->e()Lcom/google/android/gms/people/service/e;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/people/e/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    invoke-virtual {p0}, Lcom/google/android/gms/people/e/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    .line 206
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "me"

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 315
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/net/Uri;)[B
    .locals 6

    .prologue
    .line 550
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/ac;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 551
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/common/util/ac;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 552
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not a image mime type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 556
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/a/a;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 557
    invoke-static {p0, p1, v0, v0}, Lcom/google/android/gms/common/util/ac;->a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 558
    if-nez v2, :cond_2

    .line 559
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No image decoded from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 563
    :cond_2
    const/4 v1, 0x0

    .line 564
    sget-object v0, Lcom/google/android/gms/people/a/a;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 566
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 567
    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v2, v4, v0, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 568
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 569
    :try_start_1
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    .line 573
    :goto_0
    return-object v0

    .line 570
    :catch_0
    move-exception v0

    move-object v5, v0

    move-object v0, v1

    move-object v1, v5

    .line 571
    :goto_1
    const-string v2, "PeopleService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not load image from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 570
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/gms/people/service/e;
    .locals 1

    .prologue
    .line 199
    new-instance v0, Lcom/google/android/gms/people/service/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/service/e;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/people/service/g;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->b:Lcom/google/android/gms/people/service/g;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;
    .locals 6

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->e:Lcom/google/android/gms/plus/service/v1whitelisted/d;

    iget-object v1, p2, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p2}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/service/v1whitelisted/d;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 355
    new-instance v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;-><init>()V

    iput-object p3, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->a:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->e:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object p2, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->b:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->e:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    iget-object v1, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->e:Ljava/util/Set;

    iget-object v2, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->a:Ljava/lang/String;

    iget-object v3, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->b:Ljava/lang/String;

    iget-boolean v4, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->c:Z

    iget-object v5, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/bq;->d:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->h:Lcom/google/android/gms/plus/service/v1whitelisted/e;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "people/%1$s/circles"

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move v2, v7

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;
    .locals 6

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->h:Lcom/google/android/gms/plus/service/v1whitelisted/e;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p4}, Lcom/google/android/gms/people/service/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/gms/people/service/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/e/a;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity;)Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse;
    .locals 6

    .prologue
    .line 448
    iget-object v2, p0, Lcom/google/android/gms/people/service/e;->g:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    const-string v4, "full"

    const-string v0, "people/mutateCircleMemberships"

    if-eqz v3, :cond_0

    const-string v5, "onBehalfOf"

    invoke-static {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v5, v3}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v3, "returnChangedPeople"

    invoke-static {v4}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse;

    move-object v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsResponse;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/a;
    .locals 9

    .prologue
    const/4 v8, 0x7

    .line 404
    new-instance v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;

    invoke-direct {v7}, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;-><init>()V

    .line 406
    if-eqz p3, :cond_0

    .line 407
    iput-object p3, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->e:Ljava/lang/String;

    iget-object v0, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->g:Ljava/util/Set;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_0
    if-eqz p4, :cond_1

    .line 410
    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->b:Z

    iget-object v0, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->g:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 412
    :cond_1
    if-eqz p5, :cond_2

    .line 413
    iput-object p5, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->a:Ljava/lang/String;

    iget-object v0, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->g:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 416
    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    iget-object v1, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->g:Ljava/util/Set;

    iget-object v2, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->a:Ljava/lang/String;

    iget-boolean v3, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->b:Z

    iget-object v4, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->c:Ljava/lang/String;

    iget-object v5, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->d:Ljava/lang/String;

    iget-object v6, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->e:Ljava/lang/String;

    iget-object v7, v7, Lcom/google/android/gms/plus/service/v2whitelisted/models/b;->f:Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;-><init>(Ljava/util/Set;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity$PeopleEntity;)V

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->k:Lcom/google/android/gms/plus/service/v2whitelisted/a;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    const-string v3, "circles/%1$s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v2, :cond_3

    const-string v5, "onBehalfOf"

    invoke-static {v2}, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v5, v2}, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v0, v0, Lcom/google/android/gms/plus/service/v2whitelisted/a;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    move v2, v8

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 520
    if-nez p4, :cond_0

    const-string p4, "me"

    .line 522
    :cond_0
    invoke-static {p1, p3}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Landroid/net/Uri;)[B

    move-result-object v0

    .line 525
    const-string v1, "cloud"

    const-string v2, "people/%1$s/media/%2$s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/plus/service/v1whitelisted/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 527
    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/gms/common/server/g;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/gms/common/server/g;

    const-string v5, "image/jpeg"

    const-string v6, ""

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/gms/common/server/g;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    aput-object v4, v2, v3

    const/4 v0, 0x1

    new-instance v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;

    invoke-direct {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/common/server/g;

    const-string v5, "application/json"

    const/4 v6, 0x0

    new-array v6, v6, [B

    invoke-direct {v4, v5, v3, v6}, Lcom/google/android/gms/common/server/g;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    aput-object v4, v2, v0

    .line 531
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->c:Lcom/google/android/gms/people/service/g;

    iget-object v3, p2, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-class v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;

    invoke-virtual {v0, v3, v1, v2, v4}, Lcom/google/android/gms/people/service/g;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;[Lcom/google/android/gms/common/server/g;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;

    .line 539
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/eq;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/eq;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Media;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/eq;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/eq;->c:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iget-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/eq;->c:Ljava/util/Set;

    iget-object v3, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/eq;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/eq;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    .line 540
    new-instance v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;

    invoke-direct {v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;-><init>()V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iput-object v0, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iget-object v0, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->k:Ljava/util/Set;

    const/16 v1, 0x10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    iget-object v1, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->k:Ljava/util/Set;

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->a:Ljava/lang/String;

    iget-object v3, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->b:Ljava/util/List;

    iget-object v4, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->c:Ljava/lang/String;

    iget-object v5, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->d:Ljava/lang/String;

    iget-object v6, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;

    iget-object v7, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->f:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$NameEntity;

    iget-object v8, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->g:Ljava/lang/String;

    iget-object v9, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->h:Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$StatusForViewerEntity;

    iget-object v10, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->i:Ljava/lang/String;

    iget-object v11, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/en;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$ImageEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$NameEntity;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity$StatusForViewerEntity;Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    .line 541
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->f:Lcom/google/android/gms/plus/service/v1whitelisted/i;

    iget-object v1, p2, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v2, "people/%1$s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x7

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    .line 546
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 289
    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->g:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v2, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    const-string v0, "people/%1$s/block"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_0

    const-string v4, "onBehalfOf"

    invoke-static {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v4, v3}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, v1, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v7, v0, v3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 292
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;)V
    .locals 7

    .prologue
    .line 381
    iget-object v2, p0, Lcom/google/android/gms/people/service/e;->j:Lcom/google/android/gms/plus/service/v1whitelisted/l;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p1}, Lcom/google/android/gms/people/e/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    :goto_0
    const-string v3, "settings/%1$s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v4, "onBehalfOf"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    .line 386
    return-void

    .line 381
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/people/service/g;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->d:Lcom/google/android/gms/people/service/g;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 297
    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->g:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v2, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    const-string v0, "people/%1$s/unblock"

    new-array v4, v7, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_0

    const-string v4, "onBehalfOf"

    invoke-static {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v4, v3}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, v1, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v7, v0, v3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 300
    return-void
.end method

.method public final b(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 9

    .prologue
    .line 332
    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->h:Lcom/google/android/gms/plus/service/v1whitelisted/e;

    iget-object v2, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p4}, Lcom/google/android/gms/people/service/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/gms/people/service/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    const-string v0, "circles/%1$s/people"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_0

    const-string v6, "email"

    const-string v7, "&email="

    invoke-static {v7, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v6, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v4, :cond_1

    const-string v3, "onBehalfOf"

    invoke-static {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-eqz v5, :cond_2

    const-string v3, "userId"

    const-string v4, "&userId="

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_2
    iget-object v1, v1, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 338
    return-void
.end method

.method public final c(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;
    .locals 6

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->h:Lcom/google/android/gms/plus/service/v1whitelisted/e;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p4}, Lcom/google/android/gms/people/service/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    invoke-static {p3}, Lcom/google/android/gms/people/service/e;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v5

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->g:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v3, "people/%1$s/star"

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    .line 306
    return-void
.end method

.method public final d(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->g:Lcom/google/android/gms/plus/service/v2whitelisted/b;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    const-string v3, "people/%1$s/unstar"

    new-array v4, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v2whitelisted/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    .line 312
    return-void
.end method

.method public final e(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/people/service/e;->j:Lcom/google/android/gms/plus/service/v1whitelisted/l;

    iget-object v1, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "circles"

    invoke-static {v4, v2, v3, v4, p2}, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a(Lcom/google/android/gms/plus/service/v1whitelisted/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/plus/service/v1whitelisted/l;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    return-object v0
.end method

.method public final f(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 391
    iget-object v1, p0, Lcom/google/android/gms/people/service/e;->h:Lcom/google/android/gms/plus/service/v1whitelisted/e;

    iget-object v2, p1, Lcom/google/android/gms/people/e/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/people/e/a;->b:Ljava/lang/String;

    const-string v0, "circles/%1$s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_0

    const-string v4, "onBehalfOf"

    invoke-static {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v4, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    iget-object v1, v1, Lcom/google/android/gms/plus/service/v1whitelisted/e;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 396
    return-void
.end method
