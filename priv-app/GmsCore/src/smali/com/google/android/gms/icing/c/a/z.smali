.class public final Lcom/google/android/gms/icing/c/a/z;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/c/a/z;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3255
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3256
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/z;->cachedSize:I

    .line 3257
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/z;
    .locals 2

    .prologue
    .line 3238
    sget-object v0, Lcom/google/android/gms/icing/c/a/z;->c:[Lcom/google/android/gms/icing/c/a/z;

    if-nez v0, :cond_1

    .line 3239
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3241
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/z;->c:[Lcom/google/android/gms/icing/c/a/z;

    if-nez v0, :cond_0

    .line 3242
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/z;

    sput-object v0, Lcom/google/android/gms/icing/c/a/z;->c:[Lcom/google/android/gms/icing/c/a/z;

    .line 3244
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3246
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/z;->c:[Lcom/google/android/gms/icing/c/a/z;

    return-object v0

    .line 3244
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3318
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3319
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3320
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3323
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3324
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3327
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3269
    if-ne p1, p0, :cond_1

    .line 3270
    const/4 v0, 0x1

    .line 3290
    :cond_0
    :goto_0
    return v0

    .line 3272
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/z;

    if-eqz v1, :cond_0

    .line 3275
    check-cast p1, Lcom/google/android/gms/icing/c/a/z;

    .line 3276
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 3277
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3283
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 3284
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3290
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/z;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3280
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3287
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3295
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3298
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3300
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/z;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3301
    return v0

    .line 3295
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3298
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3232
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/z;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3307
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3308
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3310
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3311
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/z;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3313
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3314
    return-void
.end method
