.class public Lcom/google/android/gms/feedback/PreviewActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/feedback/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 44
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    sget v0, Lcom/google/android/gms/l;->fn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/PreviewActivity;->setContentView(I)V

    .line 47
    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->f()Lcom/google/android/gms/feedback/t;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    if-nez v1, :cond_1

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/PreviewActivity;->finish()V

    .line 62
    :goto_0
    return-void

    .line 52
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/feedback/t;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    .line 55
    :try_start_0
    new-instance v1, Lcom/google/android/gms/feedback/b;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/feedback/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    iput-object v1, p0, Lcom/google/android/gms/feedback/PreviewActivity;->a:Lcom/google/android/gms/feedback/b;

    .line 56
    sget v0, Lcom/google/android/gms/j;->lh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/PreviewActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/feedback/PreviewActivity;->a:Lcom/google/android/gms/feedback/b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 58
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    const-string v1, "PreviewActivity"

    const-string v2, "failed to read in report fields"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/feedback/PreviewActivity;->a:Lcom/google/android/gms/feedback/b;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/feedback/b;->a(I)V

    .line 67
    return-void
.end method
