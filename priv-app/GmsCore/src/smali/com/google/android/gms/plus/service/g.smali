.class public final Lcom/google/android/gms/plus/service/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/service/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;
    .locals 2

    .prologue
    .line 152
    const/4 v0, 0x0

    .line 153
    if-eqz p3, :cond_0

    .line 154
    const-string v0, "com.google.android.gms.plus.intent.extra.AUTHENTICATED_CALLING_PACKAGE"

    invoke-virtual {p3, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 155
    const/4 v0, 0x0

    const/high16 v1, 0x8000000

    invoke-static {p0, v0, p3, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 159
    :cond_0
    new-instance v1, Lcom/google/android/gms/common/c;

    invoke-direct {v1, p2, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    return-object v1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 137
    invoke-static/range {p1 .. p6}, Lcom/google/android/gms/plus/service/PlusService;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/common/c;
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 55
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_1

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 57
    :goto_1
    if-nez v0, :cond_2

    .line 59
    new-instance v0, Lcom/google/android/gms/common/c;

    invoke-direct {v0, v7, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 73
    :goto_2
    return-object v0

    .line 55
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v2

    goto :goto_1

    .line 62
    :cond_2
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1, v0, p4, v2, v2}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 65
    :try_start_0
    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {p3}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    const-string v4, "com.google.android.gms.plus"

    invoke-virtual {v1, v4}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/provider/PlusProvider;->b(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    :cond_3
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    new-instance v0, Lcom/google/android/gms/plus/n;

    invoke-direct {v0, p1, p3}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/gms/plus/n;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p3}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x4

    invoke-static {p1, v1, v3, v0}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
    :try_end_2
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 72
    :catch_0
    move-exception v0

    .line 73
    :goto_3
    new-instance v0, Lcom/google/android/gms/common/c;

    invoke-direct {v0, v7, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    goto :goto_2

    .line 65
    :cond_5
    :try_start_3
    sget-object v0, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    goto :goto_3

    .line 72
    :catch_2
    move-exception v0

    goto :goto_3
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/c;
    .locals 3

    .prologue
    .line 106
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 107
    sget-object v1, Lcom/google/android/gms/auth/r;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v1, "suppressProgressScreen"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 109
    if-eqz p6, :cond_0

    .line 110
    const-string v1, "request_visible_actions"

    const-string v2, " "

    invoke-static {v2, p6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 116
    :cond_0
    sget-object v1, Lcom/google/android/gms/auth/r;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    if-eqz p4, :cond_1

    .line 120
    :try_start_0
    invoke-static {p1, p5, p4, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    .line 130
    :cond_1
    sget-object v0, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    return-object v0

    .line 121
    :catch_0
    move-exception v0

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1, p5, p2}, Lcom/google/android/gms/plus/provider/d;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    throw v0
.end method

.method public final a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 143
    invoke-static {p1, p2}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
