.class public final enum Lcom/google/android/gms/drive/ui/picker/a/a/j;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/picker/a/a/j;

.field public static final enum b:Lcom/google/android/gms/drive/ui/picker/a/a/j;

.field private static final synthetic c:[Lcom/google/android/gms/drive/ui/picker/a/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;

    const-string v1, "LEFT"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    .line 51
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;

    const-string v1, "RIGHT"

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/ui/picker/a/a/j;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;->b:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    .line 49
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/a/a/j;

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/j;->b:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;->c:[Lcom/google/android/gms/drive/ui/picker/a/a/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/a/a/j;
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/picker/a/a/j;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;->c:[Lcom/google/android/gms/drive/ui/picker/a/a/j;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/picker/a/a/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/a/j;

    return-object v0
.end method
