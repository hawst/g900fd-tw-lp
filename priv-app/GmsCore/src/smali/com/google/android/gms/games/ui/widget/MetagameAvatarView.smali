.class public final Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field private static final a:F

.field private static final b:F

.field private static c:F


# instance fields
.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field private g:Landroid/widget/FrameLayout;

.field private h:Lcom/google/android/gms/games/ui/widget/b;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/graphics/drawable/ShapeDrawable;

.field private k:I

.field private l:I

.field private m:F

.field private n:I

.field private o:I

.field private p:Landroid/graphics/Rect;

.field private q:Landroid/graphics/drawable/Drawable;

.field private r:Landroid/graphics/drawable/Drawable;

.field private s:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 46
    const-wide/high16 v0, 0x4014000000000000L    # 5.0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 47
    sput v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    sput v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b:F

    .line 48
    const/high16 v0, 0x3f000000    # 0.5f

    sput v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 82
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 85
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 88
    sget v1, Lcom/google/android/gms/g;->ah:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->k:I

    .line 90
    sget v1, Lcom/google/android/gms/g;->Z:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    .line 93
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    .line 95
    iput v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->n:I

    .line 96
    iput v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->o:I

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    .line 101
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setWillNotDraw(Z)V

    .line 102
    return-void
.end method

.method private g(I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 307
    if-lez p1, :cond_1

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 311
    new-instance v0, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v1, Lcom/google/android/gms/games/ui/widget/a;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/widget/a;-><init>()V

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->j:Landroid/graphics/drawable/ShapeDrawable;

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->j:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 313
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->j:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 321
    :goto_0
    return-void

    .line 316
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->j:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 319
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/images/internal/LoadingImageView;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    return-object v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b(I)V

    .line 128
    return-void
.end method

.method public final a(JZ)V
    .locals 1

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/widget/b;->a(JZ)V

    .line 334
    :cond_0
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 236
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;I)V

    .line 237
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;I)V
    .locals 1

    .prologue
    .line 251
    if-eqz p1, :cond_0

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 257
    :goto_0
    invoke-direct {p0, p3}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g(I)V

    .line 258
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;Z)V

    .line 192
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Player;Z)V
    .locals 4

    .prologue
    .line 209
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    .line 210
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->s:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->s:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 211
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    .line 213
    :cond_1
    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->s:Ljava/lang/String;

    .line 215
    const/4 v0, -0x1

    .line 216
    if-eqz p2, :cond_2

    .line 217
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v1

    .line 218
    if-eqz v1, :cond_2

    .line 219
    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    .line 222
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g(I)V

    .line 223
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/PlayerLevelInfo;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 268
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    sget v0, Lcom/google/android/gms/j;->pS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g:Landroid/widget/FrameLayout;

    .line 271
    new-instance v0, Lcom/google/android/gms/games/ui/widget/b;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/widget/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/b;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/b;->a(Lcom/google/android/gms/games/PlayerLevelInfo;)V

    .line 281
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setElevation(F)V

    .line 285
    :cond_0
    return-void
.end method

.method public final b()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    return-object v0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->k:I

    if-eq p1, v0, :cond_0

    .line 132
    iput p1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->k:I

    .line 133
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->invalidate()V

    .line 135
    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d(I)V

    .line 139
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    if-eq p1, v0, :cond_0

    .line 143
    iput p1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->invalidate()V

    .line 146
    :cond_0
    return-void
.end method

.method public final draw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 445
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 447
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->isPressed()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 448
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->q:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 449
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->ap:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->q:Landroid/graphics/drawable/Drawable;

    .line 452
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->q:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->q:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 456
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 457
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->r:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_2

    .line 458
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->ao:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->r:Landroid/graphics/drawable/Drawable;

    .line 461
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->r:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->r:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 465
    :cond_3
    return-void
.end method

.method protected final drawableStateChanged()V
    .locals 0

    .prologue
    .line 439
    invoke-super {p0}, Landroid/view/ViewGroup;->drawableStateChanged()V

    .line 440
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->invalidate()V

    .line 441
    return-void
.end method

.method public final e(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 155
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e:Landroid/view/View;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g:Landroid/widget/FrameLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g:Landroid/widget/FrameLayout;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setElevation(F)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setElevation(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->m:F

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setElevation(F)V

    .line 156
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setElevation(F)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->j:Landroid/graphics/drawable/ShapeDrawable;

    if-eqz v0, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->j:Landroid/graphics/drawable/ShapeDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    if-eqz v0, :cond_1

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/widget/b;->a(I)V

    .line 302
    :cond_1
    return-void
.end method

.method protected final onFinishInflate()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Landroid/view/ViewGroup;->onFinishInflate()V

    .line 108
    sget v0, Lcom/google/android/gms/j;->bK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d:Landroid/view/View;

    .line 109
    sget v0, Lcom/google/android/gms/j;->bI:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e:Landroid/view/View;

    .line 110
    sget v0, Lcom/google/android/gms/j;->bE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 111
    sget v0, Lcom/google/android/gms/j;->bF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    .line 116
    return-void
.end method

.method protected final onLayout(ZIIII)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 392
    sub-int v0, p5, p3

    iget v1, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->k:I

    sub-int/2addr v0, v1

    .line 393
    sub-int v1, p4, p2

    .line 398
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d:Landroid/view/View;

    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->k:I

    div-int/lit8 v5, v1, 0x2

    div-int/lit8 v6, v0, 0x2

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->k:I

    add-int/2addr v6, v0

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 407
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->left:I

    .line 408
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iput v7, v2, Landroid/graphics/Rect;->top:I

    .line 409
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    add-int/2addr v3, v4

    iput v3, v2, Landroid/graphics/Rect;->right:I

    .line 410
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 413
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 416
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    if-eqz v2, :cond_0

    .line 417
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g:Landroid/widget/FrameLayout;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    iget-object v5, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 419
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->bottom:I

    iget-object v5, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->p:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->top:I

    sub-int/2addr v4, v5

    invoke-virtual {v2, v7, v7, v3, v4}, Lcom/google/android/gms/games/ui/widget/b;->layout(IIII)V

    .line 426
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    div-int/lit8 v3, v1, 0x2

    div-int/lit8 v4, v0, 0x2

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    add-int/2addr v3, v4

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    div-int/lit8 v5, v1, 0x2

    div-int/lit8 v6, v0, 0x2

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    sub-int v6, v0, v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->layout(IIII)V

    .line 432
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    sub-int v3, v1, v3

    iget v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->n:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    sub-int v4, v0, v4

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->o:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->n:I

    sub-int/2addr v1, v5

    iget v5, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->o:I

    sub-int/2addr v0, v5

    invoke-virtual {v2, v3, v4, v1, v0}, Landroid/widget/TextView;->layout(IIII)V

    .line 435
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v7, 0x41100000    # 9.0f

    const/high16 v10, 0x40000000    # 2.0f

    const/high16 v9, 0x3f000000    # 0.5f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 338
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    .line 341
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getMeasuredHeight()I

    move-result v0

    .line 342
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 343
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getMeasuredWidth()I

    move-result v0

    .line 345
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->setMeasuredDimension(II)V

    .line 348
    :cond_0
    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 349
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->d:Landroid/view/View;

    invoke-virtual {v2, v1, v1}, Landroid/view/View;->measure(II)V

    .line 350
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e:Landroid/view/View;

    invoke-virtual {v2, v1, v1}, Landroid/view/View;->measure(II)V

    .line 352
    iget v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    mul-int/lit8 v2, v2, 0x2

    sub-int v2, v0, v2

    .line 353
    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v3, v2, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->measure(II)V

    .line 356
    int-to-float v2, v0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v2, v3

    .line 357
    const/high16 v3, 0x42700000    # 60.0f

    sub-float/2addr v2, v3

    const/high16 v3, 0x42b40000    # 90.0f

    div-float/2addr v2, v3

    .line 359
    const/4 v3, 0x0

    invoke-static {v2, v8}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    .line 360
    const/high16 v3, -0x41800000    # -0.25f

    mul-float/2addr v2, v3

    add-float/2addr v2, v8

    .line 366
    iget v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->l:I

    sub-int v3, v0, v3

    int-to-float v3, v3

    mul-float/2addr v3, v9

    int-to-float v4, v0

    div-float/2addr v3, v4

    .line 369
    int-to-float v0, v0

    div-float/2addr v0, v7

    const/high16 v4, 0x40000000    # 2.0f

    sget v5, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40800000    # 4.0f

    sget v6, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a:F

    mul-float/2addr v5, v6

    mul-float/2addr v3, v3

    sub-float/2addr v3, v8

    mul-float/2addr v3, v7

    add-float/2addr v3, v5

    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v3, v6

    sub-float v3, v4, v3

    mul-float/2addr v0, v3

    .line 371
    sget v3, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->b:F

    mul-float/2addr v3, v0

    .line 373
    mul-float v4, v0, v2

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 374
    mul-float v5, v3, v2

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 375
    sub-float v6, v8, v2

    mul-float/2addr v6, v9

    mul-float/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->n:I

    .line 376
    sub-float v0, v8, v2

    mul-float/2addr v0, v9

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->o:I

    .line 378
    mul-float v0, v3, v2

    sget v2, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c:F

    mul-float/2addr v0, v2

    .line 379
    iget-object v2, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 381
    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 382
    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 383
    iget-object v3, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->i:Landroid/widget/TextView;

    invoke-virtual {v3, v0, v2}, Landroid/widget/TextView;->measure(II)V

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->h:Lcom/google/android/gms/games/ui/widget/b;

    if-eqz v0, :cond_1

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->g:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v1, v1}, Landroid/widget/FrameLayout;->measure(II)V

    .line 388
    :cond_1
    return-void
.end method
