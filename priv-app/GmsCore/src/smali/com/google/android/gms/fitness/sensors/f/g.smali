.class final Lcom/google/android/gms/fitness/sensors/f/g;
.super Lcom/google/android/gms/fitness/internal/ac;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

.field final synthetic b:Lcom/google/k/k/a/aq;

.field final synthetic c:Lcom/google/android/gms/fitness/sensors/f/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/f/c;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Lcom/google/k/k/a/aq;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/f/g;->c:Lcom/google/android/gms/fitness/sensors/f/c;

    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/f/g;->a:Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/f/g;->b:Lcom/google/k/k/a/aq;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/internal/ac;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 6

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/g;->c:Lcom/google/android/gms/fitness/sensors/f/c;

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/f/g;->a:Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/f/c;->c:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    if-eqz v0, :cond_0

    const-string v2, "Updating the data source for listener %s from %s to %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    const/4 v0, 0x2

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/f/g;->b:Lcom/google/k/k/a/aq;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Object;)Z

    .line 180
    return-void
.end method
