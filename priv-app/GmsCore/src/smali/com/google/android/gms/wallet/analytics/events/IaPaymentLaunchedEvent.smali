.class public Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/f;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 34
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 35
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 26
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->j:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->a:Ljava/lang/String;

    .line 29
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    .line 30
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V

    .line 41
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 42
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 54
    return-void
.end method
