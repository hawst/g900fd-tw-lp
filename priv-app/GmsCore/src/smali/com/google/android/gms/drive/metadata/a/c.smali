.class final Lcom/google/android/gms/drive/metadata/a/c;
.super Lcom/google/android/gms/drive/metadata/a/al;
.source "SourceFile"


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 257
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/gms/drive/metadata/a/al;-><init>(Lcom/google/android/gms/drive/metadata/b;ZLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 257
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Lorg/json/JSONObject;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 257
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public final synthetic a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 257
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot query Drive API v2 for a collection of items."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final synthetic a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 257
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Cannot currently set collections."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final synthetic a(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 257
    check-cast p2, Ljava/util/Collection;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    const-string v3, "root"

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lcom/google/android/gms/drive/internal/model/ParentReference;

    invoke-direct {v3}, Lcom/google/android/gms/drive/internal/model/ParentReference;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "appdata"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v0, "appdata"

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/internal/model/ParentReference;->e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/ParentReference;

    :goto_1
    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/model/ParentReference;->d()Lcom/google/android/gms/drive/internal/model/ParentReference;

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/internal/model/ParentReference;->e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/ParentReference;

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v1}, Lcom/google/android/gms/drive/internal/model/File;->a(Ljava/util/List;)Lcom/google/android/gms/drive/internal/model/File;

    return-void
.end method

.method protected final synthetic a(Lorg/json/JSONObject;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 257
    check-cast p2, Ljava/util/Collection;

    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/a/ay;->c:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    check-cast p1, Lcom/google/android/gms/drive/DriveId;

    sget-object v0, Lcom/google/android/gms/drive/metadata/a/a;->a:Lcom/google/android/gms/drive/metadata/a/ay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
