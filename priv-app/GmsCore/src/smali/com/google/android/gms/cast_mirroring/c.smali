.class public final Lcom/google/android/gms/cast_mirroring/c;
.super Lcom/google/android/gms/cast_mirroring/e;
.source "SourceFile"


# instance fields
.field private final e:I


# direct methods
.method public constructor <init>(ILcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/cast_mirroring/e;-><init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V

    .line 167
    iput p1, p0, Lcom/google/android/gms/cast_mirroring/c;->e:I

    .line 168
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast_mirroring/c;)I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/google/android/gms/cast_mirroring/c;->e:I

    return v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 172
    sget-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast_mirroring/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast_mirroring/d;-><init>(Lcom/google/android/gms/cast_mirroring/c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/c;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 199
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 196
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/c;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 199
    :catch_1
    move-exception v0

    goto :goto_0
.end method
