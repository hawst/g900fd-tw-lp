.class final Lcom/google/android/gms/smart_profile/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 217
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/af;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/af;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 217
    check-cast p1, Lcom/google/android/gms/people/i;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/people/i;->c()Lcom/google/android/gms/people/model/k;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/af;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    new-instance v2, Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/google/android/gms/people/model/k;->c()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v0, v2}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/util/Map;)Ljava/util/Map;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/people/model/k;->c()I

    move-result v2

    if-ge v0, v2, :cond_0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/model/k;->b(I)Lcom/google/android/gms/people/model/j;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/af;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->n(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/util/Map;

    move-result-object v3

    invoke-interface {v2}, Lcom/google/android/gms/people/model/j;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2}, Lcom/google/android/gms/people/model/j;->d()[Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/people/model/k;->w_()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/af;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->o(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/af;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->p(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/people/model/k;->w_()V

    throw v0
.end method
