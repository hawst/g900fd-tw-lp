.class public final Lcom/google/android/gms/drive/api/a/aq;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x16

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/aq;->e:Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aq;->e:Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;

    const-string v1, "Invalid update request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aq;->e:Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid update request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aq;->e:Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    const-string v1, "Invalid update request."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/aq;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/aq;->e:Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/aq;->e:Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/aq;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/c/a;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/aq;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnMetadataResponse;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/internal/OnMetadataResponse;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnMetadataResponse;)V

    .line 42
    return-void
.end method
