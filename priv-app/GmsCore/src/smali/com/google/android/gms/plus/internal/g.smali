.class public abstract Lcom/google/android/gms/plus/internal/g;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/plus/internal/g;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/plus/internal/f;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/google/android/gms/plus/internal/f;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/internal/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/h;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 492
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v7

    .line 46
    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v1, v0

    .line 61
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 62
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 67
    :goto_2
    invoke-virtual {p0, v3, v1, v0}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 69
    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 58
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 65
    goto :goto_2

    .line 73
    :sswitch_2
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v1, v0

    .line 84
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 85
    sget-object v0, Landroid/os/ParcelFileDescriptor;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    .line 90
    :goto_4
    invoke-virtual {p0, v3, v1, v0}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    .line 91
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 92
    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 81
    goto :goto_3

    :cond_3
    move-object v0, v2

    .line 88
    goto :goto_4

    .line 96
    :sswitch_3
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/g;->a(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 101
    goto :goto_0

    .line 105
    :sswitch_4
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 109
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 110
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 116
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_4

    .line 117
    sget-object v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1whitelisted/models/ee;

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ee;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;

    move-result-object v2

    .line 122
    :cond_4
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 124
    goto/16 :goto_0

    :cond_5
    move-object v0, v2

    .line 113
    goto :goto_5

    .line 128
    :sswitch_5
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 131
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/p;

    invoke-static {p2}, Lcom/google/android/gms/common/data/p;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 137
    :cond_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 138
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/plus/internal/g;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    .line 139
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 140
    goto/16 :goto_0

    .line 144
    :sswitch_6
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 146
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 148
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 149
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v1, v0

    .line 155
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 156
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 161
    :goto_7
    invoke-virtual {p0, v3, v1, v0}, Lcom/google/android/gms/plus/internal/g;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 162
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 163
    goto/16 :goto_0

    :cond_7
    move-object v1, v2

    .line 152
    goto :goto_6

    :cond_8
    move-object v0, v2

    .line 159
    goto :goto_7

    .line 167
    :sswitch_7
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 172
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    move-object v2, v0

    .line 178
    :cond_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v4

    .line 182
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v5

    .line 184
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v6

    move-object v0, p0

    .line 185
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 186
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 187
    goto/16 :goto_0

    .line 191
    :sswitch_8
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 193
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 195
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 196
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 202
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 204
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a

    .line 205
    sget-object v2, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/apps/b;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/apps/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v2

    .line 210
    :cond_a
    invoke-virtual {p0, v1, v0, v3, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    .line 211
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 212
    goto/16 :goto_0

    :cond_b
    move-object v0, v2

    .line 199
    goto :goto_8

    .line 216
    :sswitch_9
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 218
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 220
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 221
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 226
    :goto_9
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;)V

    .line 227
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 228
    goto/16 :goto_0

    :cond_c
    move-object v0, v2

    .line 224
    goto :goto_9

    .line 232
    :sswitch_a
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 234
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 236
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_e

    .line 237
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 243
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_d

    .line 244
    sget-object v2, Lcom/google/android/gms/plus/model/posts/Post;->CREATOR:Lcom/google/android/gms/plus/model/posts/d;

    invoke-static {p2}, Lcom/google/android/gms/plus/model/posts/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v2

    .line 249
    :cond_d
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 250
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 251
    goto/16 :goto_0

    :cond_e
    move-object v0, v2

    .line 240
    goto :goto_a

    .line 255
    :sswitch_b
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 257
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 259
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    .line 260
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 266
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_f

    .line 267
    sget-object v2, Lcom/google/android/gms/plus/model/posts/Settings;->CREATOR:Lcom/google/android/gms/plus/model/posts/e;

    invoke-static {p2}, Lcom/google/android/gms/plus/model/posts/e;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v2

    .line 272
    :cond_f
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Settings;)V

    .line 273
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 274
    goto/16 :goto_0

    :cond_10
    move-object v0, v2

    .line 263
    goto :goto_b

    .line 278
    :sswitch_c
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 280
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 282
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_11

    .line 283
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 289
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 290
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    .line 291
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 292
    goto/16 :goto_0

    :cond_11
    move-object v0, v2

    .line 286
    goto :goto_c

    .line 296
    :sswitch_d
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 298
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 300
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    .line 301
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 307
    :goto_d
    sget-object v2, Lcom/google/android/gms/common/people/data/AudienceMember;->CREATOR:Lcom/google/android/gms/common/people/data/d;

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v2

    .line 308
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Ljava/util/List;)V

    .line 309
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 310
    goto/16 :goto_0

    :cond_12
    move-object v0, v2

    .line 304
    goto :goto_d

    .line 314
    :sswitch_e
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 316
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 318
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    .line 319
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 325
    :goto_e
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 326
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;I)V

    .line 327
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 328
    goto/16 :goto_0

    :cond_13
    move-object v0, v2

    .line 322
    goto :goto_e

    .line 332
    :sswitch_f
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 334
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 336
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_15

    .line 337
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 343
    :goto_f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_14

    .line 344
    sget-object v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->CREATOR:Lcom/google/android/gms/plus/service/v1whitelisted/models/fo;

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fo;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    move-result-object v2

    .line 349
    :cond_14
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 350
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 351
    goto/16 :goto_0

    :cond_15
    move-object v0, v2

    .line 340
    goto :goto_f

    .line 355
    :sswitch_10
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 357
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 359
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_17

    .line 360
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 366
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_16

    .line 367
    sget-object v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1whitelisted/models/el;

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/el;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v2

    .line 372
    :cond_16
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    .line 373
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 374
    goto/16 :goto_0

    :cond_17
    move-object v0, v2

    .line 363
    goto :goto_10

    .line 378
    :sswitch_11
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 380
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 382
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_19

    .line 383
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 389
    :goto_11
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_18

    .line 390
    sget-object v2, Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;->CREATOR:Lcom/google/android/gms/plus/internal/model/acls/c;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/acls/c;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;

    move-result-object v2

    .line 395
    :cond_18
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V

    .line 396
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 397
    goto/16 :goto_0

    :cond_19
    move-object v0, v2

    .line 386
    goto :goto_11

    .line 401
    :sswitch_12
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 403
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 405
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1b

    .line 406
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 412
    :goto_12
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1a

    .line 413
    sget-object v2, Lcom/google/android/gms/plus/model/posts/Comment;->CREATOR:Lcom/google/android/gms/plus/model/posts/b;

    invoke-static {p2}, Lcom/google/android/gms/plus/model/posts/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Comment;

    move-result-object v2

    .line 418
    :cond_1a
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    .line 419
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 420
    goto/16 :goto_0

    :cond_1b
    move-object v0, v2

    .line 409
    goto :goto_12

    .line 424
    :sswitch_13
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 426
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 428
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1d

    .line 429
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 435
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1c

    .line 436
    sget-object v2, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;->CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/b;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/smart_profile/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;

    move-result-object v2

    .line 441
    :cond_1c
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;)V

    .line 442
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 443
    goto/16 :goto_0

    :cond_1d
    move-object v0, v2

    .line 432
    goto :goto_13

    .line 447
    :sswitch_14
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 449
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 451
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1f

    .line 452
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 458
    :goto_14
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1e

    .line 459
    sget-object v2, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;->CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/d;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/smart_profile/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;

    move-result-object v2

    .line 464
    :cond_1e
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;)V

    .line 465
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 466
    goto/16 :goto_0

    :cond_1f
    move-object v0, v2

    .line 455
    goto :goto_14

    .line 470
    :sswitch_15
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalCallbacks"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 474
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_21

    .line 475
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 481
    :goto_15
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_20

    .line 482
    sget-object v2, Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/apps/b;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/apps/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    move-result-object v2

    .line 487
    :cond_20
    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/plus/internal/g;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    .line 488
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v7

    .line 489
    goto/16 :goto_0

    :cond_21
    move-object v0, v2

    .line 478
    goto :goto_15

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x6 -> :sswitch_4
        0x8 -> :sswitch_5
        0x9 -> :sswitch_6
        0xa -> :sswitch_7
        0xb -> :sswitch_8
        0xc -> :sswitch_9
        0xd -> :sswitch_a
        0xe -> :sswitch_b
        0xf -> :sswitch_c
        0x10 -> :sswitch_d
        0x11 -> :sswitch_e
        0x12 -> :sswitch_f
        0x13 -> :sswitch_10
        0x14 -> :sswitch_11
        0x15 -> :sswitch_12
        0x16 -> :sswitch_13
        0x17 -> :sswitch_14
        0x18 -> :sswitch_15
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
