.class final Lcom/google/android/gms/drive/realtime/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/a/d;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/realtime/j;

.field final synthetic b:Lcom/google/c/a/a/a/a/a;

.field final synthetic c:Lcom/google/android/gms/drive/realtime/e;

.field final synthetic d:Lcom/google/android/gms/drive/realtime/f;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/realtime/f;Lcom/google/android/gms/drive/realtime/j;Lcom/google/c/a/a/a/a/a;Lcom/google/android/gms/drive/realtime/e;)V
    .locals 0

    .prologue
    .line 235
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/i;->d:Lcom/google/android/gms/drive/realtime/f;

    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/i;->a:Lcom/google/android/gms/drive/realtime/j;

    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/i;->b:Lcom/google/c/a/a/a/a/a;

    iput-object p4, p0, Lcom/google/android/gms/drive/realtime/i;->c:Lcom/google/android/gms/drive/realtime/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/i;->a:Lcom/google/android/gms/drive/realtime/j;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/i;->b:Lcom/google/c/a/a/a/a/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/i;->c:Lcom/google/android/gms/drive/realtime/e;

    iget-object v2, v2, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/c/a/a/a/a/a;Lcom/google/android/gms/drive/DriveId;)V

    .line 239
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/i;->a:Lcom/google/android/gms/drive/realtime/j;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x7

    const-string v3, "Document is unavailable offline. Try loading again online."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 247
    return-void
.end method
