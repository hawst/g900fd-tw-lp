.class public final Lcom/google/android/gms/drive/b/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:D

.field public final d:I


# direct methods
.method public constructor <init>(IIDI)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 40
    if-lt p2, p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 41
    const-wide/16 v4, 0x0

    cmpl-double v0, p3, v4

    if-lez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 42
    if-lez p5, :cond_3

    :goto_3
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 43
    iput p1, p0, Lcom/google/android/gms/drive/b/a/e;->a:I

    .line 44
    iput p2, p0, Lcom/google/android/gms/drive/b/a/e;->b:I

    .line 45
    iput-wide p3, p0, Lcom/google/android/gms/drive/b/a/e;->c:D

    .line 46
    iput p5, p0, Lcom/google/android/gms/drive/b/a/e;->d:I

    .line 47
    return-void

    :cond_0
    move v0, v2

    .line 39
    goto :goto_0

    :cond_1
    move v0, v2

    .line 40
    goto :goto_1

    :cond_2
    move v0, v2

    .line 41
    goto :goto_2

    :cond_3
    move v1, v2

    .line 42
    goto :goto_3
.end method
