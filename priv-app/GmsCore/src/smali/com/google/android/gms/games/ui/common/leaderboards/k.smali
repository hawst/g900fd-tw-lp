.class public final Lcom/google/android/gms/games/ui/common/leaderboards/k;
.super Lcom/google/android/gms/games/ui/d;
.source "SourceFile"


# static fields
.field private static final j:I


# instance fields
.field protected h:J

.field protected i:Ljava/lang/String;

.field private final k:Lcom/google/android/gms/games/ui/common/leaderboards/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/google/android/gms/l;->aX:I

    sput v0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->j:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/leaderboards/l;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/d;-><init>(Landroid/content/Context;)V

    .line 52
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:J

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->k:Lcom/google/android/gms/games/ui/common/leaderboards/l;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/leaderboards/k;)Lcom/google/android/gms/games/ui/common/leaderboards/l;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->k:Lcom/google/android/gms/games/ui/common/leaderboards/l;

    return-object v0
.end method

.method private w()Z
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 97
    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 98
    iput-wide p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:J

    .line 99
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(II)V

    .line 102
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 97
    goto :goto_0
.end method

.method public final bridge synthetic a(Landroid/support/v7/widget/cs;I)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/gms/games/ui/f;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Lcom/google/android/gms/games/ui/f;I)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 2

    .prologue
    .line 58
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/games/e/f;

    if-eqz v0, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 62
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/d;->f:Z

    if-eqz v1, :cond_1

    .line 63
    add-int/lit8 v0, v0, -0x1

    .line 66
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/d;->a(Lcom/google/android/gms/common/data/d;)V

    .line 69
    if-ltz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 70
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->c(I)V

    .line 72
    :cond_2
    return-void

    .line 58
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/ui/f;I)V
    .locals 4

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->i:Ljava/lang/String;

    const-string v1, "Must set a player ID before binding views"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 123
    iget-wide v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Must set the number of scores before binding views"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 124
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/d;->a(Lcom/google/android/gms/games/ui/f;I)V

    .line 125
    return-void

    .line 123
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 84
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->i:Ljava/lang/String;

    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->w()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(II)V

    .line 88
    :cond_0
    return-void
.end method

.method protected final b(Landroid/view/ViewGroup;I)Lcom/google/android/gms/games/ui/f;
    .locals 4

    .prologue
    .line 130
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/m;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->e:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/leaderboards/k;->j:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/m;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 110
    sget v0, Lcom/google/android/gms/j;->gD:I

    return v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    return v0
.end method
