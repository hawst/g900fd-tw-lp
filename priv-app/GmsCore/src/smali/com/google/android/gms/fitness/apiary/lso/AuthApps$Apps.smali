.class public final Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/fitness/apiary/lso/c;

.field private static final i:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 406
    new-instance v0, Lcom/google/android/gms/fitness/apiary/lso/c;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/lso/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/c;

    .line 423
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 426
    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    const-string v1, "clients"

    const-string v2, "clients"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 428
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    const-string v1, "display_name"

    const-string v2, "display_name"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 429
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    const-string v1, "icon_url"

    const-string v2, "icon_url"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    const-string v1, "project_id"

    const-string v2, "project_id"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    const-string v1, "revocation_handle"

    const-string v2, "revocation_handle"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 432
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    const-string v1, "scope_ids"

    const-string v2, "scope_ids"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 471
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->b:I

    .line 472
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a:Ljava/util/Set;

    .line 473
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 485
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 486
    iput-object p1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a:Ljava/util/Set;

    .line 487
    iput p2, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->b:I

    .line 488
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->c:Ljava/util/List;

    .line 489
    iput-object p4, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->d:Ljava/lang/String;

    .line 490
    iput-object p5, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->e:Ljava/lang/String;

    .line 491
    iput-object p6, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->f:Ljava/lang/String;

    .line 492
    iput-object p7, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->g:Ljava/lang/String;

    .line 493
    iput-object p8, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->h:Ljava/util/List;

    .line 494
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 437
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 975
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 976
    packed-switch v0, :pswitch_data_0

    .line 990
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 978
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->d:Ljava/lang/String;

    .line 993
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 994
    return-void

    .line 981
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->e:Ljava/lang/String;

    goto :goto_0

    .line 984
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->f:Ljava/lang/String;

    goto :goto_0

    .line 987
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->g:Ljava/lang/String;

    goto :goto_0

    .line 976
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 999
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1000
    packed-switch v0, :pswitch_data_0

    .line 1005
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1002
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->c:Ljava/util/List;

    .line 1009
    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1010
    return-void

    .line 1000
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 921
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 926
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 940
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 928
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->c:Ljava/util/List;

    .line 938
    :goto_0
    return-object v0

    .line 930
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->d:Ljava/lang/String;

    goto :goto_0

    .line 932
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->e:Ljava/lang/String;

    goto :goto_0

    .line 934
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->f:Ljava/lang/String;

    goto :goto_0

    .line 936
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->g:Ljava/lang/String;

    goto :goto_0

    .line 938
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->h:Ljava/util/List;

    goto :goto_0

    .line 926
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->c:Ljava/util/List;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->g:Ljava/lang/String;

    return-object v0
.end method

.method protected final c(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 960
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 961
    packed-switch v0, :pswitch_data_0

    .line 966
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an array of ints."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 963
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->h:Ljava/util/List;

    .line 969
    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 970
    return-void

    .line 961
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
    .end packed-switch
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->h:Ljava/util/List;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 911
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/c;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1027
    instance-of v0, p1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;

    if-nez v0, :cond_0

    move v0, v1

    .line 1058
    :goto_0
    return v0

    .line 1032
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 1033
    goto :goto_0

    .line 1036
    :cond_1
    check-cast p1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;

    .line 1037
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1038
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1039
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1041
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1043
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1048
    goto :goto_0

    .line 1051
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1053
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1058
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1014
    const/4 v0, 0x0

    .line 1015
    sget-object v1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1016
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1017
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 1018
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 1020
    goto :goto_0

    .line 1021
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 916
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/c;

    invoke-static {p0, p1}, Lcom/google/android/gms/fitness/apiary/lso/c;->a(Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;Landroid/os/Parcel;)V

    .line 917
    return-void
.end method
