.class public final Lcom/google/android/gms/people/d/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/people/d/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 429
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 430
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/f;->cachedSize:I

    .line 431
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 479
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 480
    iget-object v1, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    if-eqz v1, :cond_0

    .line 481
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 441
    if-ne p1, p0, :cond_1

    .line 457
    :cond_0
    :goto_0
    return v0

    .line 444
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 445
    goto :goto_0

    .line 447
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/f;

    .line 448
    iget-object v2, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    if-nez v2, :cond_3

    .line 449
    iget-object v2, p1, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    if-eqz v2, :cond_0

    move v0, v1

    .line 450
    goto :goto_0

    .line 453
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    iget-object v3, p1, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 454
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 465
    return v0

    .line 462
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/g;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 409
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/g;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    if-eqz v0, :cond_0

    .line 472
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 474
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 475
    return-void
.end method
