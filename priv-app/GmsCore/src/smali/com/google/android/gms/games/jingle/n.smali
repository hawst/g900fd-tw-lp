.class public final Lcom/google/android/gms/games/jingle/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Z

.field private static final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 16
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 17
    const/16 v3, 0x13

    if-lt v0, v3, :cond_1

    move v0, v1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/games/jingle/n;->a:Z

    .line 23
    sget-boolean v0, Lcom/google/android/gms/games/jingle/n;->a:Z

    if-eqz v0, :cond_0

    .line 25
    :try_start_0
    const-class v0, Landroid/net/ConnectivityManager;

    const-string v3, "registerNetworkFactory"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    const-class v6, Landroid/os/Messenger;

    aput-object v6, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    if-eqz v0, :cond_2

    :goto_1
    move v2, v1

    .line 32
    :cond_0
    :goto_2
    sput-boolean v2, Lcom/google/android/gms/games/jingle/n;->b:Z

    .line 33
    return-void

    :cond_1
    move v0, v2

    .line 17
    goto :goto_0

    :cond_2
    move v1, v2

    .line 25
    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 40
    sget-boolean v0, Lcom/google/android/gms/games/jingle/n;->b:Z

    return v0
.end method
