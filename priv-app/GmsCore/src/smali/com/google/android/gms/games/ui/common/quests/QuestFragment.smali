.class public Lcom/google/android/gms/games/ui/common/quests/QuestFragment;
.super Lcom/google/android/gms/games/ui/common/quests/a;
.source "SourceFile"


# instance fields
.field private m:Lcom/google/android/gms/games/ui/common/quests/k;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/quests/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/bh;Z)V
    .locals 6

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestFragment;->m:Lcom/google/android/gms/games/ui/common/quests/k;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/quests/k;->V()[I

    move-result-object v4

    .line 37
    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 38
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-interface {v0, p1, v4, p3}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;[IZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 47
    :goto_0
    return-void

    .line 42
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v3

    move-object v1, p1

    move v5, p3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[IZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/quests/a;->onActivityCreated(Landroid/os/Bundle;)V

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestFragment;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/quests/k;

    const-string v1, "Parent activity did not implement QuestMetadataProvider"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestFragment;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/quests/k;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestFragment;->m:Lcom/google/android/gms/games/ui/common/quests/k;

    .line 31
    return-void
.end method
