.class public final Lcom/google/android/gms/appstate/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/gms/appstate/a/e;


# instance fields
.field public final a:Ljava/util/concurrent/locks/ReentrantLock;

.field public final b:Lcom/google/android/gms/appstate/a/a;

.field private final c:Lcom/google/android/gms/appstate/d/a;

.field private final d:Lcom/google/android/gms/appstate/d/a;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 62
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 63
    new-instance v0, Lcom/google/android/gms/appstate/d/a;

    sget-object v2, Lcom/google/android/gms/appstate/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/appstate/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/appstate/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/appstate/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appstate/d/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    iput-object v0, p0, Lcom/google/android/gms/appstate/a/e;->c:Lcom/google/android/gms/appstate/d/a;

    .line 64
    new-instance v0, Lcom/google/android/gms/appstate/d/a;

    sget-object v2, Lcom/google/android/gms/appstate/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/appstate/b/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/appstate/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/appstate/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appstate/d/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZZ)V

    iput-object v0, p0, Lcom/google/android/gms/appstate/a/e;->d:Lcom/google/android/gms/appstate/d/a;

    .line 65
    new-instance v0, Lcom/google/android/gms/appstate/a/a;

    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->c:Lcom/google/android/gms/appstate/d/a;

    iget-object v2, p0, Lcom/google/android/gms/appstate/a/e;->d:Lcom/google/android/gms/appstate/d/a;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/appstate/a/a;-><init>(Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    .line 66
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/appstate/a/e;
    .locals 1

    .prologue
    .line 45
    sget-object v0, Lcom/google/android/gms/appstate/a/e;->e:Lcom/google/android/gms/appstate/a/e;

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Lcom/google/android/gms/appstate/a/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appstate/a/e;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/appstate/a/e;->e:Lcom/google/android/gms/appstate/a/e;

    .line 48
    :cond_0
    sget-object v0, Lcom/google/android/gms/appstate/a/e;->e:Lcom/google/android/gms/appstate/a/e;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 175
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 179
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 181
    return v0

    .line 179
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 102
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 104
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    move-result v0

    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    invoke-static {v2, v3, p3}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 108
    return-object v0

    .line 106
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 81
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 83
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)I

    move-result v0

    invoke-static {p1, p2}, Lcom/google/android/gms/appstate/a/d;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    invoke-static {v2, v3, p3, p4}, Lcom/google/android/gms/appstate/provider/d;->a(JLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 87
    return-object v0

    .line 85
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 130
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;[B)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 137
    return-object v0

    .line 135
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I[BZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 199
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/appstate/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I[BZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 204
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 206
    return-object v0

    .line 204
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "DataBroker left locked!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 58
    return-void

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 154
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/appstate/a/a;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 160
    return v0

    .line 158
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method
