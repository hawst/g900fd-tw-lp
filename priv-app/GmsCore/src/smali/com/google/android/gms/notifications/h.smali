.class public final Lcom/google/android/gms/notifications/h;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# instance fields
.field private final j:Landroid/graphics/BitmapFactory$Options;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 36
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/notifications/h;->j:Landroid/graphics/BitmapFactory$Options;

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/notifications/h;->j:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 38
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/gms/notifications/h;
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/notifications/h;

    invoke-direct {v0}, Lcom/google/android/gms/notifications/h;-><init>()V

    .line 44
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 45
    const-string v2, "GunsDialogFrag.title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v2, "GunsDialogFrag.message"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const-string v2, "GunsDialogFrag.positiveButtonLabel"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v2, "GunsDialogFrag.negativeButtonLabel"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    const-string v2, "GunsDialogFrag.iconResourceId"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    const-string v2, "GunsDialogFrag.iconFilePath"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v0, v1}, Lcom/google/android/gms/notifications/h;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8

    .prologue
    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "GunsDialogFrag.title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "GunsDialogFrag.message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "GunsDialogFrag.positiveButtonLabel"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "GunsDialogFrag.negativeButtonLabel"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v5, "GunsDialogFrag.iconResourceId"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 63
    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getActivity()Landroid/support/v4/app/q;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 64
    const/4 v6, -0x1

    if-eq v0, v6, :cond_0

    .line 65
    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    .line 75
    :goto_0
    invoke-virtual {v5, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/notifications/j;

    invoke-direct {v1, p0}, Lcom/google/android/gms/notifications/j;-><init>(Lcom/google/android/gms/notifications/h;)V

    invoke-virtual {v0, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/notifications/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/notifications/i;-><init>(Lcom/google/android/gms/notifications/h;)V

    invoke-virtual {v0, v4, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 67
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v6, "GunsDialogFrag.iconFilePath"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    iget-object v6, p0, Lcom/google/android/gms/notifications/h;->j:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v6}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v6

    if-eqz v6, :cond_1

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/gms/notifications/h;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v0, v7, v6}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 68
    :goto_1
    if-eqz v0, :cond_2

    .line 69
    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setIcon(Landroid/graphics/drawable/Drawable;)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 71
    :cond_2
    const v0, 0x1080027

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 97
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onAttach(Landroid/app/Activity;)V

    .line 98
    instance-of v0, p1, Lcom/google/android/gms/notifications/GunsNotificationDialogActivity;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "GNDFragment can\'t be hosted by anything other than GNDActivity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    return-void
.end method
