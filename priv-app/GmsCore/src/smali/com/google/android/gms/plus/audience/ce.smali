.class public final Lcom/google/android/gms/plus/audience/ce;
.super Lcom/google/android/gms/plus/f/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 500
    invoke-direct {p0}, Lcom/google/android/gms/plus/f/d;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/plus/audience/ce;
    .locals 3

    .prologue
    .line 507
    invoke-static {p0}, Lcom/google/android/gms/plus/f/d;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    .line 508
    const-string v1, "resultCode"

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 509
    const-string v1, "dataIntent"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 510
    new-instance v1, Lcom/google/android/gms/plus/audience/ce;

    invoke-direct {v1}, Lcom/google/android/gms/plus/audience/ce;-><init>()V

    .line 511
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/ce;->setArguments(Landroid/os/Bundle;)V

    .line 512
    return-object v1
.end method


# virtual methods
.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 4

    .prologue
    .line 517
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/f/d;->onDismiss(Landroid/content/DialogInterface;)V

    .line 518
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ce;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;

    .line 519
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ce;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 520
    if-eqz v0, :cond_0

    .line 521
    const-string v1, "resultCode"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v1, "dataIntent"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->setResult(ILandroid/content/Intent;)V

    .line 523
    const-string v1, "resultCode"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    const-string v1, "dataIntent"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/plus/audience/UpdateCirclesActivity;->a(ILandroid/content/Intent;)V

    .line 526
    :cond_0
    return-void
.end method
