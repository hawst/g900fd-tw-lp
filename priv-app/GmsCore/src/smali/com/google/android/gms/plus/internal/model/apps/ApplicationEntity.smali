.class public Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/plus/model/a/a;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/internal/model/apps/d;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/pm/ApplicationInfo;

.field private final f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

.field private final g:Z

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Z

.field private final k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/d;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/model/apps/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/apps/d;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;ZLjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    iput p1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    .line 75
    iput-object p4, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    .line 76
    iput-object p5, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->e:Landroid/content/pm/ApplicationInfo;

    .line 77
    iput-object p6, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    .line 78
    iput-boolean p7, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    .line 79
    iput-object p8, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    .line 80
    iput-object p9, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    .line 81
    iput-boolean p10, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->j:Z

    .line 82
    iput-boolean p11, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->k:Z

    .line 83
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/plus/model/a/a;)V
    .locals 10

    .prologue
    .line 156
    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->i()Z

    move-result v5

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->k()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->l()Ljava/lang/String;

    move-result-object v7

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->m()Z

    move-result v8

    invoke-interface {p1}, Lcom/google/android/gms/plus/model/a/a;->j()Z

    move-result v9

    move-object v0, p0

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 159
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 136
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v4

    move-object v7, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 95
    const/4 v1, 0x2

    const/4 v6, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;ZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 98
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 109
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;ZLjava/lang/String;Ljava/lang/String;)V

    .line 111
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;ZZ)V
    .locals 12

    .prologue
    .line 124
    const/4 v1, 0x2

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v5, p4

    move/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;ZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 126
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;ZZB)V
    .locals 0

    .prologue
    .line 150
    invoke-direct/range {p0 .. p9}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/pm/ApplicationInfo;ZLjava/lang/String;Ljava/lang/String;ZZ)V

    .line 152
    return-void
.end method

.method public static a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;
    .locals 1

    .prologue
    .line 167
    if-nez p0, :cond_0

    .line 168
    const/4 p0, 0x0

    .line 175
    :goto_0
    return-object p0

    .line 171
    :cond_0
    instance-of v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-eqz v0, :cond_1

    .line 172
    check-cast p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    goto :goto_0

    .line 175
    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;-><init>(Lcom/google/android/gms/plus/model/a/a;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 190
    iget v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    return v0
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 18
    return-object p0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 264
    instance-of v1, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    if-nez v1, :cond_1

    .line 269
    :cond_0
    :goto_0
    return v0

    .line 268
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    .line 269
    iget v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->k:Z

    iget-boolean v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->k:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Landroid/content/pm/ApplicationInfo;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->e:Landroid/content/pm/ApplicationInfo;

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 258
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->f:Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->g:Z

    return v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 228
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->k:Z

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->j:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 253
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/internal/model/apps/d;->a(Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Landroid/os/Parcel;I)V

    .line 254
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 186
    const/4 v0, 0x1

    return v0
.end method
