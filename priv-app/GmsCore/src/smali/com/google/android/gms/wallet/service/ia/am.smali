.class final Lcom/google/android/gms/wallet/service/ia/am;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic d:Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ia/ad;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/am;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ia/am;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ia/am;->d:Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 9

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/am;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/service/ia/ad;)Lcom/google/android/gms/wallet/service/ia/o;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/am;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/am;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/am;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/wallet/service/ia/am;->d:Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/am;->a:Landroid/accounts/Account;

    iget-object v6, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/am;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v1, Lcom/google/android/gms/wallet/service/ia/o;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/y;

    move-object v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wallet/service/ia/y;-><init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "get_purchase_options"

    invoke-static {v8, v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 144
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/o;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 156
    :goto_0
    return v0

    .line 147
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v0

    const/4 v3, 0x3

    if-eq v0, v3, :cond_1

    move v0, v2

    .line 149
    goto :goto_0

    .line 152
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/al;

    .line 153
    if-nez v0, :cond_2

    move v0, v2

    .line 154
    goto :goto_0

    .line 156
    :cond_2
    iget v0, v0, Lcom/google/checkout/inapp/proto/al;->i:I

    const/4 v3, 0x6

    if-ne v0, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method
