.class public Lcom/google/android/gms/drive/api/DriveAsyncService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/common/service/d;

.field private static c:Lcom/google/android/gms/common/b;


# instance fields
.field private d:Lcom/google/android/gms/common/service/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 53
    const-string v0, "DriveAsyncService"

    sget-object v1, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 54
    return-void
.end method

.method static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 96
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    if-eqz v0, :cond_0

    .line 97
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    invoke-virtual {p0, v0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 98
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    .line 101
    :cond_0
    const-string v0, "com.google.android.gms.drive.EXECUTE"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 102
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/drive/api/ApiService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 103
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/service/d;->clear()V

    .line 104
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    .line 49
    const-string v0, "com.google.android.gms.drive.EXECUTE"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 50
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/drive/api/a/ao;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/api/a/ao;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 61
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/gms/common/service/e;
    .locals 3

    .prologue
    .line 75
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Lcom/google/android/gms/common/b;

    invoke-direct {v0}, Lcom/google/android/gms/common/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    .line 77
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/service/AccountService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 78
    sget-object v1, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/drive/api/DriveAsyncService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :try_start_1
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/service/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/common/service/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->d:Lcom/google/android/gms/common/service/e;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 86
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->d:Lcom/google/android/gms/common/service/e;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-super {p0}, Lcom/google/android/gms/common/service/c;->onDestroy()V

    .line 66
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    if-eqz v0, :cond_0

    .line 67
    sget-object v0, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->unbindService(Landroid/content/ServiceConnection;)V

    .line 68
    sput-object v1, Lcom/google/android/gms/drive/api/DriveAsyncService;->c:Lcom/google/android/gms/common/b;

    .line 69
    iput-object v1, p0, Lcom/google/android/gms/drive/api/DriveAsyncService;->d:Lcom/google/android/gms/common/service/e;

    .line 71
    :cond_0
    return-void
.end method
