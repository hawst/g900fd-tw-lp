.class public Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/wallet/common/ui/v;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/x;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 27
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/x;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p0, v1}, Lcom/google/android/gms/wallet/common/ui/x;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/x;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/common/ui/x;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/x;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/common/ui/x;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/w;)V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/android/gms/wallet/common/ui/w;)V

    .line 90
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 48
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->a(Z)V

    .line 63
    return-void
.end method

.method public final a([Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    .line 53
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/x;->a()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/x;->b(Z)V

    .line 100
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/x;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 111
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->a:Lcom/google/android/gms/wallet/common/ui/x;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/x;->onNothingSelected(Landroid/widget/AdapterView;)V

    .line 116
    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->isEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 74
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressSelectorSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/z;

    .line 76
    if-eqz v0, :cond_0

    .line 77
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/z;->notifyDataSetChanged()V

    .line 80
    :cond_0
    return-void
.end method
