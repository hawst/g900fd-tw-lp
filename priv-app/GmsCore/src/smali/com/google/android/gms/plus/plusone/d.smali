.class final Lcom/google/android/gms/plus/plusone/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/plusone/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/c;)V
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/b;)V
    .locals 3

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    iget-boolean v0, v0, Lcom/google/android/gms/plus/plusone/c;->a:Z

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/plus/plusone/c;->a:Z

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/plusone/c;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->refreshDrawableState()V

    .line 43
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz p2, :cond_1

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/plusone/c;->a(Lcom/google/android/gms/plus/data/a/b;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/c;->a()V

    .line 46
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    iget-object v0, p2, Lcom/google/android/gms/plus/data/a/b;->a:Landroid/os/Bundle;

    const-string v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/plusone/c;->setTag(Ljava/lang/Object;)V

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_1
    const-string v0, "PlusOneButtonView"

    const-string v1, "PlusOne failed to load"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/d;->a:Lcom/google/android/gms/plus/plusone/c;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/c;->e()V

    goto :goto_0
.end method
