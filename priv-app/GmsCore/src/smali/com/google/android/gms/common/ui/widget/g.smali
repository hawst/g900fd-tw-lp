.class public abstract Lcom/google/android/gms/common/ui/widget/g;
.super Lcom/google/android/gms/common/ui/widget/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/ui/widget/a;

.field private final c:Lcom/google/android/gms/common/ui/widget/c;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/ui/widget/a;ILcom/google/android/gms/common/ui/widget/c;)V
    .locals 2

    .prologue
    .line 664
    invoke-static {p1, p2}, Lcom/google/android/gms/common/ui/widget/a;->a(Lcom/google/android/gms/common/ui/widget/a;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/google/android/gms/common/ui/widget/a;->a(Lcom/google/android/gms/common/ui/widget/a;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/ui/widget/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1, p3}, Lcom/google/android/gms/common/ui/widget/g;-><init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    .line 667
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/ui/widget/a;ILjava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V
    .locals 1

    .prologue
    .line 674
    invoke-static {p1, p2}, Lcom/google/android/gms/common/ui/widget/a;->a(Lcom/google/android/gms/common/ui/widget/a;I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/gms/common/ui/widget/g;-><init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    .line 675
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V
    .locals 1

    .prologue
    .line 670
    invoke-static {p2}, Lcom/google/android/gms/common/ui/widget/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/gms/common/ui/widget/g;-><init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    .line 671
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V
    .locals 0

    .prologue
    .line 677
    iput-object p1, p0, Lcom/google/android/gms/common/ui/widget/g;->a:Lcom/google/android/gms/common/ui/widget/a;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/c;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    .line 678
    iput-object p4, p0, Lcom/google/android/gms/common/ui/widget/g;->c:Lcom/google/android/gms/common/ui/widget/c;

    .line 679
    iput-object p2, p0, Lcom/google/android/gms/common/ui/widget/g;->d:Ljava/lang/String;

    .line 680
    iput-object p3, p0, Lcom/google/android/gms/common/ui/widget/g;->e:Ljava/lang/String;

    .line 681
    return-void
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 720
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Z
    .locals 1

    .prologue
    .line 725
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/widget/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)I
    .locals 1

    .prologue
    .line 735
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/widget/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 736
    add-int/lit8 p1, p1, -0x1

    .line 738
    :cond_0
    return p1
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->c:Lcom/google/android/gms/common/ui/widget/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    .line 686
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/widget/g;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 701
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/g;->c(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 702
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->d:Ljava/lang/String;

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/common/ui/widget/g;->a(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 704
    :goto_0
    return-object v0

    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/common/ui/widget/g;->c:Lcom/google/android/gms/common/ui/widget/c;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/g;->d(I)I

    move-result v4

    if-eqz p4, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/common/ui/widget/g;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    if-ne p1, v0, :cond_1

    move v2, v0

    :goto_1
    if-nez v2, :cond_2

    :goto_2
    invoke-virtual {v3, v4, p2, p3, v0}, Lcom/google/android/gms/common/ui/widget/c;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method protected abstract a(Ljava/lang/String;Landroid/view/View;)Landroid/view/View;
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 691
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/g;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 692
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->d:Ljava/lang/String;

    .line 694
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->c:Lcom/google/android/gms/common/ui/widget/c;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/g;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/ui/widget/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/util/ArrayList;I)V
    .locals 2

    .prologue
    .line 744
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/widget/g;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 745
    new-instance v0, Lcom/google/android/gms/common/ui/widget/i;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/g;->e:Ljava/lang/String;

    invoke-direct {v0, v1, p2}, Lcom/google/android/gms/common/ui/widget/i;-><init>(Ljava/lang/String;I)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 748
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/g;->c:Lcom/google/android/gms/common/ui/widget/c;

    invoke-direct {p0}, Lcom/google/android/gms/common/ui/widget/g;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, p2

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/common/ui/widget/c;->a(Ljava/util/ArrayList;I)V

    .line 749
    return-void

    .line 748
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 711
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/g;->c(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->a:Lcom/google/android/gms/common/ui/widget/a;

    iget v0, v0, Lcom/google/android/gms/common/ui/widget/a;->c:I

    .line 714
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/g;->c:Lcom/google/android/gms/common/ui/widget/c;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/g;->d(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/ui/widget/c;->b(I)I

    move-result v0

    goto :goto_0
.end method
