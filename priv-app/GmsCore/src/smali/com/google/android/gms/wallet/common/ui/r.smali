.class final Lcom/google/android/gms/wallet/common/ui/r;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 457
    new-instance v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-direct {v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->createCharArray()[C

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    const-class v0, Lcom/google/t/a/b;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    const-class v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->i:Ljava/util/Collection;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->j:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    :goto_4
    iput-boolean v1, v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->k:Z

    return-object v3

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v1, v2

    goto :goto_4
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 457
    new-array v0, p1, [Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    return-object v0
.end method
