.class public final Lcom/google/android/gms/wallet/service/analytics/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/Class;Ljava/lang/String;)Lcom/google/android/gms/wallet/service/analytics/a/h;
    .locals 2

    .prologue
    .line 42
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/af;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;

    move-result-object v0

    .line 43
    const-class v1, Lcom/google/android/gms/wallet/service/analytics/a/b;

    if-ne p0, v1, :cond_0

    .line 44
    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/b;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/analytics/a/h;

    .line 56
    :goto_0
    return-object v0

    .line 45
    :cond_0
    const-class v1, Lcom/google/android/gms/wallet/service/analytics/a/c;

    if-ne p0, v1, :cond_1

    .line 46
    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/c;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/c;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/analytics/a/h;

    goto :goto_0

    .line 47
    :cond_1
    const-class v1, Lcom/google/android/gms/wallet/service/analytics/a/e;

    if-ne p0, v1, :cond_2

    .line 48
    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/e;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/e;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/analytics/a/h;

    goto :goto_0

    .line 49
    :cond_2
    const-class v1, Lcom/google/android/gms/wallet/service/analytics/a/f;

    if-ne p0, v1, :cond_3

    .line 50
    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/f;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/f;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/analytics/a/h;

    goto :goto_0

    .line 51
    :cond_3
    const-class v1, Lcom/google/android/gms/wallet/service/analytics/a/g;

    if-ne p0, v1, :cond_4

    .line 52
    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/g;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/g;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/analytics/a/h;

    goto :goto_0

    .line 53
    :cond_4
    const-class v1, Lcom/google/android/gms/wallet/service/analytics/a/d;

    if-ne p0, v1, :cond_5

    .line 54
    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/d;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/d;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/analytics/a/h;

    goto :goto_0

    .line 55
    :cond_5
    const-class v1, Lcom/google/android/gms/wallet/service/analytics/a/j;

    if-ne p0, v1, :cond_6

    .line 56
    new-instance v1, Lcom/google/android/gms/wallet/service/analytics/a/j;

    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/service/analytics/a/j;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    invoke-virtual {p0, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/analytics/a/h;

    goto :goto_0

    .line 58
    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unexpected SessionState type used"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)Ljava/lang/Class;
    .locals 1

    .prologue
    .line 93
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryActivityClosedEvent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryLaunchedEvent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;

    if-nez v0, :cond_0

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;

    if-eqz v0, :cond_1

    .line 100
    :cond_0
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/b;

    .line 123
    :goto_0
    return-object v0

    .line 101
    :cond_1
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;

    if-nez v0, :cond_2

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;

    if-nez v0, :cond_2

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;

    if-nez v0, :cond_2

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;

    if-nez v0, :cond_2

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;

    if-nez v0, :cond_2

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;

    if-eqz v0, :cond_3

    .line 107
    :cond_2
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/g;

    goto :goto_0

    .line 108
    :cond_3
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;

    if-nez v0, :cond_4

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;

    if-eqz v0, :cond_5

    .line 110
    :cond_4
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/c;

    goto :goto_0

    .line 111
    :cond_5
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/SignupActivityStartedEvent;

    if-nez v0, :cond_6

    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/SignupActivityClosedEvent;

    if-eqz v0, :cond_7

    .line 113
    :cond_6
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/j;

    goto :goto_0

    .line 114
    :cond_7
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;

    if-eqz v0, :cond_8

    .line 115
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/f;

    goto :goto_0

    .line 116
    :cond_8
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwFullWalletRequestedEvent;

    if-eqz v0, :cond_9

    .line 117
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/e;

    goto :goto_0

    .line 118
    :cond_9
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;

    if-eqz v0, :cond_a

    .line 119
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/e;

    goto :goto_0

    .line 120
    :cond_a
    instance-of v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;

    if-eqz v0, :cond_b

    .line 121
    const-class v0, Lcom/google/android/gms/wallet/service/analytics/a/d;

    goto :goto_0

    .line 123
    :cond_b
    const/4 v0, 0x0

    goto :goto_0
.end method
