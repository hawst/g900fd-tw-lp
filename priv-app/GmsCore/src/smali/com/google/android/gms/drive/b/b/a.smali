.class public final Lcom/google/android/gms/drive/b/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/b/o;


# static fields
.field static final a:Ljava/lang/String;

.field private static b:Lcom/google/android/gms/drive/b/a/n;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/drive/b/b/n;

.field private final e:Lcom/google/android/gms/drive/b/b;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final g:Lcom/google/android/gms/drive/b/b/d;

.field private final h:Lcom/google/android/gms/drive/b/b/p;

.field private final i:Lcom/google/android/gms/drive/b/a/e;

.field private final j:Lcom/google/android/gms/drive/b/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/drive/b/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/b/a/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/b/b/a;->b:Lcom/google/android/gms/drive/b/a/n;

    .line 75
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/upload/drive/v2beta/files/%s?setModifiedDate=true"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/b/b/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;Lcom/google/android/gms/drive/b/a/e;Lcom/google/android/gms/drive/b/f;)V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->c:Landroid/content/Context;

    .line 146
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/b/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    .line 147
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/b;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->e:Lcom/google/android/gms/drive/b/b;

    .line 148
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 149
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/b/d;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->g:Lcom/google/android/gms/drive/b/b/d;

    .line 150
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/b/p;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->h:Lcom/google/android/gms/drive/b/b/p;

    .line 151
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/a/e;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->i:Lcom/google/android/gms/drive/b/a/e;

    .line 152
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->j:Lcom/google/android/gms/drive/b/f;

    .line 153
    return-void
.end method

.method private static a(Ljava/io/InputStream;I)Lcom/google/android/gms/drive/b/a/i;
    .locals 4

    .prologue
    .line 272
    :try_start_0
    new-instance v0, Lcom/google/android/gms/drive/b/a/k;

    sget-object v1, Lcom/google/android/gms/drive/b/b/a;->b:Lcom/google/android/gms/drive/b/a/n;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/drive/b/a/k;-><init>(ILcom/google/android/gms/drive/b/a/n;)V

    .line 274
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/gms/drive/b/a/j;

    iget-object v2, v0, Lcom/google/android/gms/drive/b/a/k;->b:Lcom/google/android/gms/drive/b/a/n;

    invoke-interface {v2, p1}, Lcom/google/android/gms/drive/b/a/n;->a(I)Lcom/google/android/gms/drive/b/a/m;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/b/a/j;-><init>(Lcom/google/android/gms/drive/b/a/m;)V

    iget v0, v0, Lcom/google/android/gms/drive/b/a/k;->a:I

    new-array v0, v0, [B

    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/google/android/gms/drive/b/a/j;->a([BII)Lcom/google/android/gms/drive/b/a/j;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 276
    :catch_0
    move-exception v0

    .line 277
    const-string v1, "BinaryDiffUploader"

    const-string v2, "Unable to create hash summary"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 279
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 274
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/a/j;->a()Lcom/google/android/gms/drive/b/a/i;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/b;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/drive/b/a/e;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;)Lcom/google/android/gms/drive/b/b/a;
    .locals 9

    .prologue
    .line 102
    sget-object v0, Lcom/google/android/gms/drive/af;->a:Lcom/google/android/gms/drive/af;

    invoke-static {v0}, Lcom/google/android/gms/drive/ag;->a(Lcom/google/android/gms/drive/af;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    :cond_0
    const/4 v0, 0x0

    .line 135
    :goto_0
    return-object v0

    .line 108
    :cond_1
    iget-wide v2, p1, Lcom/google/android/gms/drive/b/b/n;->e:J

    sget-object v0, Lcom/google/android/gms/drive/ai;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-gez v0, :cond_2

    .line 109
    const/4 v0, 0x0

    goto :goto_0

    .line 113
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/drive/b/b/n;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 114
    const/4 v0, 0x0

    goto :goto_0

    .line 116
    :cond_3
    iget-object v0, p1, Lcom/google/android/gms/drive/b/b/n;->g:Ljava/lang/String;

    invoke-interface {p3, v0}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v0

    if-nez v0, :cond_4

    .line 117
    const-string v0, "BinaryDiffUploader"

    const-string v1, "No content found for base hash"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 118
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/drive/b/b/n;->h:Ljava/lang/String;

    if-eqz v0, :cond_5

    const-string v1, "__unknown_revision_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_6

    .line 122
    const-string v0, "BinaryDiffUploader"

    const-string v1, "No source revision found for base hash"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    const/4 v0, 0x0

    goto :goto_0

    .line 120
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 131
    :cond_6
    iget-wide v0, p1, Lcom/google/android/gms/drive/b/b/n;->e:J

    const-wide/32 v2, 0x400000

    cmp-long v0, v0, v2

    if-lez v0, :cond_7

    .line 132
    const-string v0, "BinaryDiffUploader"

    const-string v1, "Target file size is bigger than Multipart max size"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    const/4 v0, 0x0

    goto :goto_0

    .line 135
    :cond_7
    new-instance v0, Lcom/google/android/gms/drive/b/b/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p4

    move-object v4, p5

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object v7, p6

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/b/b/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/b/b;Ljava/util/concurrent/atomic/AtomicBoolean;Lcom/google/android/gms/drive/b/b/d;Lcom/google/android/gms/drive/b/b/p;Lcom/google/android/gms/drive/b/a/e;Lcom/google/android/gms/drive/b/f;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/b/b/l;)Lcom/google/android/gms/drive/d/e;
    .locals 13

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 163
    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v4, v3, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    .line 164
    invoke-interface {p1, v4}, Lcom/google/android/gms/drive/b/b/l;->a(Lcom/google/android/gms/drive/DriveId;)V

    .line 172
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-wide v6, v3, Lcom/google/android/gms/drive/b/b/n;->e:J

    .line 173
    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/a;->i:Lcom/google/android/gms/drive/b/a/e;

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-ltz v5, :cond_2

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    long-to-double v8, v6

    iget-wide v10, v3, Lcom/google/android/gms/drive/b/a/e;->c:D

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    iget v1, v3, Lcom/google/android/gms/drive/b/a/e;->d:I

    int-to-double v10, v1

    div-double/2addr v8, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v1, v8

    iget v2, v3, Lcom/google/android/gms/drive/b/a/e;->d:I

    mul-int/2addr v1, v2

    iget v2, v3, Lcom/google/android/gms/drive/b/a/e;->a:I

    iget v3, v3, Lcom/google/android/gms/drive/b/a/e;->b:I

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 174
    const-string v2, "BinaryDiffUploader"

    const-string v3, "File size = %d, chunk size: %d"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 176
    iget-object v2, p0, Lcom/google/android/gms/drive/b/b/a;->j:Lcom/google/android/gms/drive/b/f;

    iget-object v3, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v3, v3, Lcom/google/android/gms/drive/b/b/n;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/b/f;->b(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v3

    .line 177
    :try_start_1
    new-instance v2, Lcom/google/android/gms/drive/b/j;

    iget-object v5, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/b/b/n;->a()Ljava/io/InputStream;

    move-result-object v5

    const-string v6, "SHA-1"

    invoke-direct {v2, v5, v6}, Lcom/google/android/gms/drive/b/j;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 179
    :try_start_2
    invoke-static {v3, v1}, Lcom/google/android/gms/drive/b/b/a;->a(Ljava/io/InputStream;I)Lcom/google/android/gms/drive/b/a/i;

    move-result-object v1

    .line 180
    new-instance v5, Lcom/google/android/gms/drive/b/a/q;

    invoke-direct {v5}, Lcom/google/android/gms/drive/b/a/q;-><init>()V

    .line 181
    iput-object v1, v5, Lcom/google/android/gms/drive/b/a/q;->b:Lcom/google/android/gms/drive/b/a/i;

    .line 182
    iput-object v2, v5, Lcom/google/android/gms/drive/b/a/q;->c:Ljava/io/InputStream;

    .line 183
    iget-object v1, v5, Lcom/google/android/gms/drive/b/a/q;->b:Lcom/google/android/gms/drive/b/a/i;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, v5, Lcom/google/android/gms/drive/b/a/q;->c:Ljava/io/InputStream;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/google/android/gms/drive/b/a/f;

    new-instance v6, Lcom/google/android/gms/drive/b/a/s;

    iget v7, v5, Lcom/google/android/gms/drive/b/a/q;->d:I

    iget-object v8, v5, Lcom/google/android/gms/drive/b/a/q;->e:Lcom/google/android/gms/drive/b/a/l;

    invoke-direct {v6, v7, v8}, Lcom/google/android/gms/drive/b/a/s;-><init>(ILcom/google/android/gms/drive/b/a/l;)V

    iget-object v7, v5, Lcom/google/android/gms/drive/b/a/q;->a:Lcom/google/android/gms/drive/b/a/n;

    invoke-direct {v1, v6, v7}, Lcom/google/android/gms/drive/b/a/f;-><init>(Lcom/google/android/gms/drive/b/a/h;Lcom/google/android/gms/drive/b/a/n;)V

    new-instance v6, Lcom/google/android/gms/drive/b/a/p;

    iget-object v7, v5, Lcom/google/android/gms/drive/b/a/q;->c:Ljava/io/InputStream;

    iget-object v5, v5, Lcom/google/android/gms/drive/b/a/q;->b:Lcom/google/android/gms/drive/b/a/i;

    invoke-direct {v6, v1, v7, v5}, Lcom/google/android/gms/drive/b/a/p;-><init>(Lcom/google/android/gms/drive/b/a/f;Ljava/io/InputStream;Lcom/google/android/gms/drive/b/a/i;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 186
    :try_start_3
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    sget-object v5, Lcom/google/android/gms/drive/b/b/a;->a:Ljava/lang/String;

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v1, v5, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 188
    invoke-static {v1}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    .line 189
    iget-object v5, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v5, v5, Lcom/google/android/gms/drive/b/b/n;->a:Lcom/google/android/gms/drive/auth/g;

    .line 190
    iget-object v7, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v7, v7, Lcom/google/android/gms/drive/b/b/n;->h:Ljava/lang/String;

    .line 191
    iget-object v8, p0, Lcom/google/android/gms/drive/b/b/a;->g:Lcom/google/android/gms/drive/b/b/d;

    iget-object v8, p0, Lcom/google/android/gms/drive/b/b/a;->c:Landroid/content/Context;

    const-string v9, "PUT"

    iget-object v10, p0, Lcom/google/android/gms/drive/b/b/a;->e:Lcom/google/android/gms/drive/b/b;

    invoke-static {v8, v1, v9, v5, v10}, Lcom/google/android/gms/drive/b/b/d;->a(Landroid/content/Context;Ljava/net/URL;Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/b/b;)Lcom/google/android/gms/drive/b/b/c;
    :try_end_3
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_3 .. :try_end_3} :catch_b
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_3 .. :try_end_3} :catch_5
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    move-result-object v1

    .line 194
    :try_start_4
    const-string v0, "X-Goog-Upload-Protocol"

    const-string v5, "multipart"

    invoke-virtual {v1, v0, v5}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    const-string v0, "X-Goog-Diff-Content-Encoding"

    invoke-virtual {v1, v0, v7}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->i:Ljava/lang/String;

    .line 200
    if-eqz v0, :cond_0

    .line 201
    const-string v5, "If-Match"

    invoke-virtual {v1, v5, v0}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v0, v0, Lcom/google/android/gms/drive/b/b/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;)J

    .line 208
    const-string v0, "application/octet-stream"

    invoke-virtual {v1, v0, v6}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;Lcom/google/android/gms/drive/b/a/p;)V

    .line 212
    const-string v0, "X-Goog-Hash: sha1=%s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/drive/b/j;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 214
    const-string v5, "text/plain"

    const-string v6, "UTF-8"

    invoke-virtual {v0, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v1, v5, v0}, Lcom/google/android/gms/drive/b/b/c;->a(Ljava/lang/String;[B)J

    .line 217
    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/b/c;->a()I

    move-result v0

    .line 218
    const-string v5, "BinaryDiffUploader"

    const-string v6, "HTTP upload status %d"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 219
    const/16 v5, 0xc9

    if-eq v0, v5, :cond_4

    const/16 v5, 0xc8

    if-eq v0, v5, :cond_4

    .line 222
    const-string v4, "BinaryDiffUploader"

    const-string v5, "Upload ID: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "X-GUploader-UploadID"

    invoke-virtual {v1, v8}, Lcom/google/android/gms/drive/b/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 224
    const-string v4, "X-GUploader-Request-Result"

    invoke-virtual {v1, v4}, Lcom/google/android/gms/drive/b/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 226
    const/16 v5, 0x190

    if-ne v0, v5, :cond_1

    const-string v5, "invalid_protocol"

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 228
    const-string v4, "BinaryDiffUploader"

    const-string v5, "Binary diff upload disabled"

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v4, p0, Lcom/google/android/gms/drive/b/b/a;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 231
    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v4, v4, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    iget-object v4, v4, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-ne v0, v4, :cond_3

    .line 233
    new-instance v0, Lcom/google/android/gms/drive/b/b/j;

    const-string v4, "Conflict uploading"

    invoke-direct {v0, v4}, Lcom/google/android/gms/drive/b/b/j;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_4 .. :try_end_4} :catch_6
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    .line 246
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    :try_start_5
    new-instance v1, Ljava/lang/InterruptedException;

    invoke-direct {v1}, Ljava/lang/InterruptedException;-><init>()V

    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_5 .. :try_end_5} :catch_7
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    .line 248
    :catch_1
    move-exception v1

    move-object v12, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v12

    .line 249
    :goto_2
    :try_start_6
    new-instance v4, Lcom/google/android/gms/drive/b/b/k;

    const-string v5, "Upload exception"

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 256
    :catchall_0
    move-exception v0

    move-object v12, v1

    move-object v1, v3

    move-object v3, v2

    move-object v2, v12

    :goto_3
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 257
    invoke-static {v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 258
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    :cond_2
    move v1, v2

    .line 173
    goto/16 :goto_0

    .line 235
    :cond_3
    :try_start_7
    new-instance v4, Lcom/google/android/gms/drive/b/b/k;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Upload failed HTTP status "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/gms/drive/b/b/k;-><init>(Ljava/lang/String;)V

    throw v4

    .line 248
    :catch_2
    move-exception v0

    move-object v12, v2

    move-object v2, v3

    move-object v3, v1

    move-object v1, v12

    goto :goto_2

    .line 237
    :cond_4
    invoke-interface {p1, v4}, Lcom/google/android/gms/drive/b/b/l;->b(Lcom/google/android/gms/drive/DriveId;)V

    .line 238
    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->h:Lcom/google/android/gms/drive/b/b/p;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/b/c;->b()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/b/b/p;->a(Ljava/io/InputStream;)Lcom/google/android/gms/drive/internal/model/File;

    move-result-object v4

    .line 241
    const-string v0, "X-Server-Object-Version"

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/b/b/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 244
    new-instance v0, Lcom/google/android/gms/drive/d/e;

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/drive/d/e;-><init>(Lcom/google/android/gms/drive/internal/model/File;Ljava/lang/String;)V
    :try_end_7
    .catch Lcom/google/android/gms/drive/b/b/i; {:try_start_7 .. :try_end_7} :catch_0
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Lcom/google/android/gms/drive/b/b/k; {:try_start_7 .. :try_end_7} :catch_6
    .catchall {:try_start_7 .. :try_end_7} :catchall_4

    .line 256
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 257
    invoke-static {v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 258
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    :goto_4
    return-object v0

    .line 250
    :catch_3
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    .line 251
    :goto_5
    :try_start_8
    const-string v4, "BinaryDiffUploader"

    const-string v5, "Binary diff upload failed, trying alternate uploader"

    invoke-static {v4, v0, v5}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/a;->h:Lcom/google/android/gms/drive/b/b/p;

    iget-object v4, p0, Lcom/google/android/gms/drive/b/b/a;->d:Lcom/google/android/gms/drive/b/b/n;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v4, v5, v6}, Lcom/google/android/gms/drive/b/b/p;->a(Lcom/google/android/gms/drive/b/b/n;Lcom/google/android/gms/drive/g/aw;Z)Lcom/google/android/gms/drive/b/b/o;

    move-result-object v0

    .line 254
    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/b/b/o;->a(Lcom/google/android/gms/drive/b/b/l;)Lcom/google/android/gms/drive/d/e;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    move-result-object v0

    .line 256
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 257
    invoke-static {v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 258
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    goto :goto_4

    .line 256
    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_3

    :catchall_2
    move-exception v1

    move-object v2, v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_3

    :catchall_3
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_3

    :catchall_4
    move-exception v0

    goto/16 :goto_3

    :catchall_5
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_3

    .line 250
    :catch_4
    move-exception v1

    move-object v2, v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_5

    :catch_5
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_5

    :catch_6
    move-exception v0

    goto :goto_5

    :catch_7
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_5

    .line 248
    :catch_8
    move-exception v1

    move-object v2, v0

    move-object v3, v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_2

    :catch_9
    move-exception v1

    move-object v2, v3

    move-object v3, v0

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto/16 :goto_2

    :catch_a
    move-exception v1

    move-object v12, v1

    move-object v1, v2

    move-object v2, v3

    move-object v3, v0

    move-object v0, v12

    goto/16 :goto_2

    .line 246
    :catch_b
    move-exception v1

    goto/16 :goto_1
.end method
