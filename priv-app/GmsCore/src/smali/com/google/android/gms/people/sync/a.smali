.class public Lcom/google/android/gms/people/sync/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/gms/people/sync/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/sync/a;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static a()Lcom/google/android/gms/people/sync/a;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/people/sync/a;

    invoke-direct {v0}, Lcom/google/android/gms/people/sync/a;-><init>()V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/people/sync/a;
    .locals 1

    .prologue
    .line 28
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->l()Lcom/google/android/gms/people/sync/a;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/accounts/Account;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 57
    invoke-static {p0, p1, p2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 58
    return-void
.end method

.method public static a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 42
    return-void
.end method

.method public static a(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V
    .locals 1

    .prologue
    .line 37
    invoke-static {p0, p1, p2, p3, p4}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 38
    return-void
.end method

.method public static a(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 49
    invoke-static {p0, p1, p2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 50
    return-void
.end method

.method public static a(Landroid/accounts/Account;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 45
    invoke-static {p0, p1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static varargs a(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 71
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/gsf/n;->a(Landroid/content/ContentResolver;Landroid/accounts/Account;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/accounts/Account;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 53
    invoke-static {p0, p1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static b(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 61
    invoke-static {p0, p1, p2}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 62
    return-void
.end method

.method public static b()Z
    .locals 1

    .prologue
    .line 65
    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    return v0
.end method
