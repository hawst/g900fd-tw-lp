.class public final Lcom/google/android/gms/plus/audience/cf;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field private a:Lcom/google/android/gms/plus/internal/ad;

.field private b:Lcom/google/android/gms/common/api/v;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/ArrayList;

.field private g:Ljava/util/ArrayList;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Z

.field private k:Z

.field private l:Lcom/google/android/gms/common/api/Status;

.field private m:Ljava/util/ArrayList;

.field private n:Ljava/util/ArrayList;

.field private final o:Lcom/google/android/gms/common/api/aq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 99
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 198
    new-instance v0, Lcom/google/android/gms/plus/audience/cg;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/audience/cg;-><init>(Lcom/google/android/gms/plus/audience/cf;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->o:Lcom/google/android/gms/common/api/aq;

    .line 100
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->a:Lcom/google/android/gms/plus/internal/ad;

    .line 101
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/cf;Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/cf;->l:Lcom/google/android/gms/common/api/Status;

    return-object p1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/audience/cf;
    .locals 2

    .prologue
    .line 81
    const-string v0, "Account name must not be empty."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 82
    const-string v0, "Update person ID must not be empty"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 83
    invoke-virtual {p3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Circles to add and remove are empty, nothing to do."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 86
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 87
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    const-string v1, "plusPageId"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 89
    const-string v1, "updatePersonId"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    const-string v1, "circleIdsToAdd"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 91
    const-string v1, "circleIdsToRemove"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 92
    const-string v1, "callingPackageName"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    const-string v1, "clientApplicationId"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    new-instance v1, Lcom/google/android/gms/plus/audience/cf;

    invoke-direct {v1}, Lcom/google/android/gms/plus/audience/cf;-><init>()V

    .line 95
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/cf;->setArguments(Landroid/os/Bundle;)V

    .line 96
    return-object v1

    .line 83
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/cf;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/cf;->m:Ljava/util/ArrayList;

    return-object p1
.end method

.method private static a(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 147
    if-nez p0, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 156
    :goto_0
    return-object v0

    .line 151
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 152
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 153
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 154
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 156
    goto :goto_0
.end method

.method static synthetic a(Ljava/util/ArrayList;Ljava/util/List;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 31
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-object v3
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/cf;)Z
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->k:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/cf;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/cf;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/cf;->n:Ljava/util/ArrayList;

    return-object p1
.end method

.method private b()V
    .locals 8

    .prologue
    .line 137
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->k:Z

    .line 139
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/cf;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/cf;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/cf;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/cf;->f:Ljava/util/ArrayList;

    invoke-static {v5}, Lcom/google/android/gms/plus/audience/cf;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/audience/cf;->g:Ljava/util/ArrayList;

    invoke-static {v6}, Lcom/google/android/gms/plus/audience/cf;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/plus/a/n;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->o:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 144
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/cf;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method private c()V
    .locals 4

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/cf;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/ch;

    .line 243
    if-eqz v0, :cond_0

    .line 244
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->l:Lcom/google/android/gms/common/api/Status;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/cf;->m:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/cf;->n:Ljava/util/ArrayList;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/audience/ch;->a(Lcom/google/android/gms/common/api/Status;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    .line 247
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/plus/audience/cf;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/cf;->c()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->k:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->l:Lcom/google/android/gms/common/api/Status;

    if-eqz v0, :cond_1

    .line 116
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "UpdateCirclesFragment should only be used once."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 121
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->k:Z

    if-nez v0, :cond_2

    .line 122
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/cf;->b()V

    .line 133
    :cond_2
    :goto_0
    return-void

    .line 129
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->j:Z

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 237
    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->l:Lcom/google/android/gms/common/api/Status;

    .line 238
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/cf;->c()V

    .line 239
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->j:Z

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->j:Z

    .line 253
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/cf;->b()V

    .line 255
    :cond_0
    return-void
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 259
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->j:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->k:Z

    if-eqz v0, :cond_1

    .line 264
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/cf;->j:Z

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 267
    :cond_1
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 161
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 162
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/ch;

    if-nez v0, :cond_0

    .line 163
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Activity must implement UpdateCirclesFragmentHost."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 165
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 169
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 170
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/cf;->setRetainInstance(Z)V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/cf;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 172
    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->c:Ljava/lang/String;

    .line 173
    const-string v1, "plusPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->d:Ljava/lang/String;

    .line 174
    const-string v1, "updatePersonId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->e:Ljava/lang/String;

    .line 175
    const-string v1, "circleIdsToAdd"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->f:Ljava/util/ArrayList;

    .line 176
    const-string v1, "circleIdsToRemove"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->g:Ljava/util/ArrayList;

    .line 177
    const-string v1, "callingPackageName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/audience/cf;->h:Ljava/lang/String;

    .line 178
    const-string v1, "clientApplicationId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->i:Ljava/lang/String;

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/cf;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/cf;->i:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/analytics/a;->a(Ljava/lang/String;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/cf;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 190
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 194
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/cf;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 196
    return-void
.end method
