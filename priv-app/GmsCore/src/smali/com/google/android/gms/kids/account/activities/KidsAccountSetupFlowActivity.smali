.class public Lcom/google/android/gms/kids/account/activities/KidsAccountSetupFlowActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    sget v0, Lcom/google/android/gms/l;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/kids/account/activities/KidsAccountSetupFlowActivity;->setContentView(I)V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/gms/kids/account/activities/KidsAccountSetupFlowActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "kids_account_name_key"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 26
    sget v0, Lcom/google/android/gms/j;->kO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/kids/account/activities/KidsAccountSetupFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 27
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 29
    sget v0, Lcom/google/android/gms/j;->kP:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/kids/account/activities/KidsAccountSetupFlowActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 30
    new-instance v1, Lcom/google/android/gms/kids/account/activities/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/kids/account/activities/a;-><init>(Lcom/google/android/gms/kids/account/activities/KidsAccountSetupFlowActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 37
    return-void
.end method
