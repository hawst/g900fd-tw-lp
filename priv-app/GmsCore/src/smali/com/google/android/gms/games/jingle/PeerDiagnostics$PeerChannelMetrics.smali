.class public Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final mConnectionEstablishmentLatencyMs:I

.field private final mConnectionStartTimestampMs:I

.field private final mNumBytesReceived:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

.field private final mNumBytesSent:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

.field private final mNumMessagesLost:I

.field private final mNumMessagesReceived:I

.field private final mNumMessagesSent:I

.field private final mRoundTripLatencyMs:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesSent:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    .line 55
    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesReceived:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    .line 57
    new-instance v0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    invoke-direct {v0}, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mRoundTripLatencyMs:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    .line 62
    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesSent:I

    .line 63
    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesReceived:I

    .line 64
    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesLost:I

    .line 65
    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionStartTimestampMs:I

    .line 66
    iput v1, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionEstablishmentLatencyMs:I

    .line 67
    return-void
.end method


# virtual methods
.method public getConnectionEstablishmentLatencyMs()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionEstablishmentLatencyMs:I

    return v0
.end method

.method public getConnectionStartTimestampMs()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mConnectionStartTimestampMs:I

    return v0
.end method

.method public getNumBytesReceived()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesReceived:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    return-object v0
.end method

.method public getNumBytesSent()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumBytesSent:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    return-object v0
.end method

.method public getNumMessagesLost()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesLost:I

    return v0
.end method

.method public getNumMessagesReceived()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesReceived:I

    return v0
.end method

.method public getNumMessagesSent()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mNumMessagesSent:I

    return v0
.end method

.method public getRoundTripLatencyMs()Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$PeerChannelMetrics;->mRoundTripLatencyMs:Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;

    return-object v0
.end method
