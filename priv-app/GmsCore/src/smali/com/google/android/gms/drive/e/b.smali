.class public final Lcom/google/android/gms/drive/e/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/d/c;


# instance fields
.field final a:Lcom/google/android/gms/drive/g/n;

.field final b:Lcom/google/android/gms/drive/database/r;

.field final c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

.field d:Ljava/util/concurrent/ExecutorService;

.field final e:Ljava/util/Map;

.field final f:Ljava/util/Map;

.field final g:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

.field private final h:Lcom/google/android/gms/drive/g/ae;

.field private final i:Lcom/google/android/gms/drive/e/c;

.field private j:Ljava/util/concurrent/ExecutorService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/ae;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/gms/drive/e/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/drive/e/c;-><init>(Lcom/google/android/gms/drive/e/b;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->i:Lcom/google/android/gms/drive/e/c;

    .line 42
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->j:Ljava/util/concurrent/ExecutorService;

    .line 43
    sget-object v0, Lcom/google/android/gms/drive/ai;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->d:Ljava/util/concurrent/ExecutorService;

    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->f:Ljava/util/Map;

    .line 53
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->a:Lcom/google/android/gms/drive/g/n;

    .line 54
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->b:Lcom/google/android/gms/drive/database/r;

    .line 55
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/ae;

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->h:Lcom/google/android/gms/drive/g/ae;

    .line 56
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    .line 57
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->g:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    .line 58
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->a:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->j:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/drive/e/c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/drive/e/c;-><init>(Lcom/google/android/gms/drive/e/b;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    monitor-enter v1

    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/e/d;

    .line 111
    invoke-virtual {v0}, Lcom/google/android/gms/drive/e/d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method final a(Lcom/google/android/gms/drive/database/model/ah;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->a:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 182
    :goto_0
    return v0

    .line 161
    :cond_0
    if-nez p1, :cond_1

    move v0, v1

    .line 162
    goto :goto_0

    .line 164
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->aj()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 166
    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->ak()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 169
    goto :goto_0

    .line 171
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->f:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 172
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lcom/google/android/gms/drive/ai;->I:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v3, v0, :cond_4

    move v0, v1

    .line 173
    goto :goto_0

    .line 175
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->p()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    move v0, v2

    .line 177
    goto :goto_0

    .line 179
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->h:Lcom/google/android/gms/drive/g/ae;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->a:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_6
    move v0, v2

    .line 180
    goto :goto_0

    :cond_7
    move v0, v1

    .line 182
    goto :goto_0
.end method

.method public final declared-synchronized b()Ljava/util/concurrent/Future;
    .locals 3

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 72
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v2

    sget-object v0, Lcom/google/android/gms/drive/ai;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->e:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 77
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->j:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/google/android/gms/drive/e/b;->i:Lcom/google/android/gms/drive/e/c;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    monitor-exit p0

    return-object v0

    .line 77
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 71
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->d:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/drive/e/b;->j:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 87
    sget-object v0, Lcom/google/android/gms/drive/ai;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->d:Ljava/util/concurrent/ExecutorService;

    .line 88
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/e/b;->j:Ljava/util/concurrent/ExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
