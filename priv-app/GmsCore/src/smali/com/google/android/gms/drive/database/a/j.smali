.class public final Lcom/google/android/gms/drive/database/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/a/c;


# static fields
.field private static final a:Lcom/google/android/gms/drive/database/a/j;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/gms/drive/database/a/j;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/a/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/a/j;->a:Lcom/google/android/gms/drive/database/a/j;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    return-void
.end method

.method public static a()Lcom/google/android/gms/drive/database/a/j;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/gms/drive/database/a/j;->a:Lcom/google/android/gms/drive/database/a/j;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->a:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->b:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->d:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->e:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v5

    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->c:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/google/android/gms/drive/database/model/ce;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/ce;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/cf;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ce;->d(J)V

    return-object v0
.end method
