.class public final Lcom/google/android/gms/fitness/b/c/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/b/ad;


# instance fields
.field private final a:Z

.field private final b:Lcom/google/android/gms/fitness/b/b;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/fitness/b/c/s;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/fitness/b/b;Lcom/google/android/gms/fitness/b/c/s;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/ap;->c:Ljava/lang/String;

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/fitness/b/c/ap;->d:Ljava/lang/String;

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/ap;->a:Z

    .line 53
    iput-object p3, p0, Lcom/google/android/gms/fitness/b/c/ap;->b:Lcom/google/android/gms/fitness/b/b;

    .line 54
    iput-object p4, p0, Lcom/google/android/gms/fitness/b/c/ap;->e:Lcom/google/android/gms/fitness/b/c/s;

    .line 55
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/b/i;)I
    .locals 1

    .prologue
    .line 375
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/i;->a()I

    move-result v0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/c;JJLcom/google/android/gms/fitness/b/s;)Lcom/google/android/gms/fitness/b/c;
    .locals 7

    .prologue
    .line 243
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v0

    .line 244
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v1}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v1}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 245
    sub-long v4, p3, p1

    long-to-float v1, v4

    long-to-float v2, v2

    div-float/2addr v1, v2

    .line 247
    invoke-interface {p5}, Lcom/google/android/gms/fitness/b/s;->c()Lcom/google/android/gms/fitness/b/d;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, p1, p2, v3}, Lcom/google/android/gms/fitness/b/d;->b(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, p3, p4, v3}, Lcom/google/android/gms/fitness/b/d;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/c;->b()Lcom/google/android/gms/fitness/b/e;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/d;->a(Lcom/google/android/gms/fitness/b/e;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v2

    .line 251
    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/d;->a()Lcom/google/android/gms/fitness/b/k;

    move-result-object v3

    int-to-float v0, v0

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface {v3, v0}, Lcom/google/android/gms/fitness/b/k;->a(I)Lcom/google/android/gms/fitness/b/k;

    .line 252
    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/d;->b()Lcom/google/android/gms/fitness/b/c;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/Iterator;)Lcom/google/android/gms/fitness/b/c;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 391
    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 392
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/c;

    .line 393
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/b/j;

    .line 394
    invoke-interface {v1}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v3

    .line 395
    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/ap;->e:Lcom/google/android/gms/fitness/b/c/s;

    iget-object v1, v1, Lcom/google/android/gms/fitness/b/c/s;->a:[I

    aget v1, v1, v3

    const/4 v4, -0x1

    if-eq v1, v4, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/ap;->e:Lcom/google/android/gms/fitness/b/c/s;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/fitness/b/c/s;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 399
    :cond_1
    :goto_1
    return-object v0

    :cond_2
    move v1, v2

    .line 395
    goto :goto_0

    .line 399
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/fitness/b/f;)Lcom/google/android/gms/fitness/b/f;
    .locals 2

    .prologue
    .line 470
    const-string v0, "com.google.step_count.delta"

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/h;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    const-string v1, "estimated_steps"

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/ap;->b:Lcom/google/android/gms/fitness/b/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/c;)Lcom/google/android/gms/fitness/b/i;
    .locals 1

    .prologue
    .line 359
    invoke-static {p0}, Lcom/google/android/gms/fitness/b/c/ap;->b(Lcom/google/android/gms/fitness/b/c;)Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/t;Lcom/google/android/gms/fitness/b/h;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;
    .locals 2

    .prologue
    .line 95
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/t;->a()Lcom/google/android/gms/fitness/b/s;

    move-result-object v0

    .line 96
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/s;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/h;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/android/gms/fitness/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v1

    invoke-interface {v1, p3}, Lcom/google/android/gms/fitness/b/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    .line 100
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/s;->d()Lcom/google/android/gms/fitness/b/r;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;
    .locals 3

    .prologue
    .line 443
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/r;

    .line 444
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 448
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/u;
    .locals 4

    .prologue
    const/16 v3, 0xe10

    .line 494
    new-instance v0, Lcom/google/android/gms/fitness/b/v;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/b/v;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/fitness/b/v;->a:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/fitness/b/aa;

    invoke-direct {v1}, Lcom/google/android/gms/fitness/b/aa;-><init>()V

    invoke-virtual {v1, p2}, Lcom/google/android/gms/fitness/b/aa;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/fitness/b/c/ap;->b:Lcom/google/android/gms/fitness/b/b;

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/b/aa;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/aa;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/fitness/b/aa;->a:Lcom/google/android/gms/fitness/b/y;

    iput-object v1, v0, Lcom/google/android/gms/fitness/b/v;->b:Lcom/google/android/gms/fitness/b/o;

    iput v3, v0, Lcom/google/android/gms/fitness/b/v;->g:I

    iput v3, v0, Lcom/google/android/gms/fitness/b/v;->i:I

    iget-boolean v1, p0, Lcom/google/android/gms/fitness/b/c/ap;->a:Z

    iput-boolean v1, v0, Lcom/google/android/gms/fitness/b/v;->e:Z

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/b/v;->a()Lcom/google/android/gms/fitness/b/u;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;JJ)V
    .locals 4

    .prologue
    .line 429
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-ltz v0, :cond_0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    cmp-long v0, v0, p4

    if-lez v0, :cond_1

    .line 439
    :cond_0
    :goto_0
    return-void

    .line 434
    :cond_1
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/s;->b()Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/c;->b()Lcom/google/android/gms/fitness/b/e;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/d;->a(Lcom/google/android/gms/fitness/b/e;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v1}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/fitness/b/d;->b(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v1}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/fitness/b/d;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/d;->a()Lcom/google/android/gms/fitness/b/k;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/b/k;->a(I)Lcom/google/android/gms/fitness/b/k;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;Ljava/util/List;JJ)V
    .locals 8

    .prologue
    .line 265
    if-nez p2, :cond_1

    move-object v0, p1

    move-object v1, p3

    move-wide v2, p4

    move-wide v4, p6

    .line 266
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/List;JJ)V

    .line 270
    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/List;->clear()V

    .line 271
    return-void

    .line 268
    :cond_1
    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/ap;->e:Lcom/google/android/gms/fitness/b/c/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/b/c/s;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p2}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/c;)Lcom/google/android/gms/fitness/b/i;

    move-result-object v6

    if-nez v2, :cond_0

    invoke-static {p2}, Lcom/google/android/gms/fitness/b/c/ap;->b(Lcom/google/android/gms/fitness/b/c;)Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/b/c/w;->b(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    if-eqz v6, :cond_0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_2
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/b/c;

    invoke-static {v1}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/c;)Lcom/google/android/gms/fitness/b/i;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const/4 v2, 0x2

    invoke-static {v0}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/i;)I

    move-result v3

    if-eq v2, v3, :cond_2

    const/4 v2, 0x1

    invoke-static {v0}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/i;)I

    move-result v0

    if-eq v2, v0, :cond_2

    move-object v0, p1

    move-wide v2, p4

    move-wide v4, p6

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;JJ)V

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/c;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v0

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    move v1, v0

    goto :goto_3

    :cond_5
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v0}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v0}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_6

    const-string v0, "Empty segment"

    invoke-static {v0, p2}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    move-object v0, p1

    move-object v1, p3

    move-wide v2, p4

    move-wide v4, p6

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/List;JJ)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/ap;->e:Lcom/google/android/gms/fitness/b/c/s;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    long-to-float v6, v4

    invoke-static {v1, v6, v3}, Lcom/google/android/gms/fitness/b/c/s;->a(FFLjava/util/concurrent/TimeUnit;)F

    move-result v3

    iget-object v6, v0, Lcom/google/android/gms/fitness/b/c/s;->b:[I

    aget v6, v6, v2

    int-to-float v6, v6

    cmpl-float v6, v3, v6

    if-ltz v6, :cond_7

    iget-object v0, v0, Lcom/google/android/gms/fitness/b/c/s;->c:[I

    aget v0, v0, v2

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_7

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_8

    move-object v0, p1

    move-object v1, p3

    move-wide v2, p4

    move-wide v4, p6

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/List;JJ)V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_4

    :cond_8
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v0}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    cmp-long v0, v6, p4

    if-ltz v0, :cond_0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v0}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    cmp-long v0, v6, p6

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Cadence ["

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/fitness/b/c/ap;->e:Lcom/google/android/gms/fitness/b/c/s;

    long-to-float v3, v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {v1, v3, v6}, Lcom/google/android/gms/fitness/b/c/s;->a(FFLjava/util/concurrent/TimeUnit;)F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "], activity "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " outside of range.  Using default."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/ap;->e:Lcom/google/android/gms/fitness/b/c/s;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v0, v0, Lcom/google/android/gms/fitness/b/c/s;->a:[I

    aget v0, v0, v2

    int-to-float v0, v0

    long-to-float v2, v4

    invoke-static {v2, v1}, Lcom/google/android/gms/fitness/b/c/s;->a(FLjava/util/concurrent/TimeUnit;)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/s;->b()Lcom/google/android/gms/fitness/b/d;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v2}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/fitness/b/d;->b(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v2}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/fitness/b/d;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/fitness/b/d;->a()Lcom/google/android/gms/fitness/b/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/b/k;->a(I)Lcom/google/android/gms/fitness/b/k;

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/s;Ljava/util/Iterator;JJ)V
    .locals 6

    .prologue
    .line 417
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/b/c;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    .line 419
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;JJ)V

    goto :goto_0

    .line 421
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/b/s;Ljava/util/List;JJ)V
    .locals 8

    .prologue
    .line 407
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/b/c;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    .line 408
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;JJ)V

    goto :goto_0

    .line 410
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/b/s;Ljava/util/ListIterator;Lcom/google/android/gms/fitness/b/c;J)V
    .locals 7

    .prologue
    .line 231
    invoke-interface {p1}, Ljava/util/ListIterator;->remove()V

    .line 232
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v0}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    move-object v1, p2

    move-wide v4, p3

    move-object v6, p0

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/c;JJLcom/google/android/gms/fitness/b/s;)Lcom/google/android/gms/fitness/b/c;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 233
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p2, v0}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    move-object v1, p2

    move-wide v2, p3

    move-object v6, p0

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/c;JJLcom/google/android/gms/fitness/b/s;)Lcom/google/android/gms/fitness/b/c;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    .line 234
    return-void
.end method

.method private static b(Lcom/google/android/gms/fitness/b/c;)Lcom/google/android/gms/fitness/b/e;
    .locals 1

    .prologue
    .line 366
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/c;->b()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/c;->a()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/c;->b()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 453
    const-string v0, "com.google.step_count.delta"

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/b/g;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 460
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/g;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/f;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/f;->a()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;JJLcom/google/android/gms/fitness/b/t;)Ljava/util/List;
    .locals 16

    .prologue
    .line 63
    invoke-static {}, Lcom/google/android/gms/fitness/b/c/ae;->a()V

    .line 65
    const-string v2, "com.google.step_count.delta"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/b/c/ap;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v3

    .line 66
    const-string v2, "com.google.activity.segment"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/b/c/ap;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    .line 68
    if-nez v3, :cond_10

    .line 69
    const-string v3, "No steps data source found in input"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    .line 70
    sget-object v3, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    const-string v4, "com.google.step_count.delta"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/fitness/b/c/ap;->c:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/t;Lcom/google/android/gms/fitness/b/h;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v3

    move-object v4, v3

    .line 76
    :goto_0
    if-nez v2, :cond_f

    .line 77
    const-string v2, "No segments data source found in input"

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    .line 78
    sget-object v2, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    const-string v3, "com.google.activity.segment"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/fitness/b/c/ap;->d:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-static {v0, v2, v3, v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/t;Lcom/google/android/gms/fitness/b/h;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    move-object v3, v2

    .line 84
    :goto_1
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x2

    if-le v2, v5, :cond_0

    .line 85
    const-string v2, "Input contains more than two data streams"

    const/4 v5, 0x0

    invoke-static {v2, v5}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    .line 88
    :cond_0
    invoke-interface/range {p6 .. p6}, Lcom/google/android/gms/fitness/b/t;->a()Lcom/google/android/gms/fitness/b/s;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/s;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/f;)Lcom/google/android/gms/fitness/b/f;

    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/r;->a()Ljava/util/List;

    move-result-object v5

    invoke-interface {v4}, Lcom/google/android/gms/fitness/b/r;->a()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v7

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_1
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/b/c;

    :cond_2
    :goto_3
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/b/c;

    sget-object v9, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v9}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    sget-object v9, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v9}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-lez v9, :cond_2

    sget-object v9, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v9}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v14

    cmp-long v9, v14, v12

    if-gez v9, :cond_3

    invoke-static {v2, v7, v4, v12, v13}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/ListIterator;Lcom/google/android/gms/fitness/b/c;J)V

    invoke-interface {v7}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    goto :goto_3

    :cond_3
    sget-object v9, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v9}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v9, v10, v12

    if-lez v9, :cond_2

    cmp-long v3, v14, v12

    if-gez v3, :cond_4

    invoke-static {v2, v7, v4, v12, v13}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/ListIterator;Lcom/google/android/gms/fitness/b/c;J)V

    :cond_4
    invoke-interface {v7}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    goto :goto_2

    :cond_5
    invoke-interface {v6}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v3

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/gms/fitness/b/c/ap;->a(Ljava/util/Iterator;)Lcom/google/android/gms/fitness/b/c;

    move-result-object v8

    if-nez v8, :cond_6

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/Iterator;JJ)V

    :cond_6
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move-object v5, v8

    :goto_4
    invoke-interface {v3}, Ljava/util/ListIterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_e

    invoke-interface {v3}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Lcom/google/android/gms/fitness/b/c;

    move-object v6, v5

    :goto_5
    if-eqz v6, :cond_8

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v6, v4}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v8, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v12, v8}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    cmp-long v4, v4, v8

    if-ltz v4, :cond_8

    const/4 v4, 0x1

    :goto_6
    if-nez v4, :cond_d

    move-object/from16 v4, p0

    move-object v5, v2

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;Ljava/util/List;JJ)V

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/gms/fitness/b/c/ap;->a(Ljava/util/Iterator;)Lcom/google/android/gms/fitness/b/c;

    move-result-object v4

    if-nez v4, :cond_c

    :goto_7
    if-nez v4, :cond_9

    move-object v4, v2

    move-object v5, v12

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-static/range {v4 .. v9}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;JJ)V

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/Iterator;JJ)V

    :cond_7
    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/s;->d()Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    return-object v2

    :cond_8
    const/4 v4, 0x0

    goto :goto_6

    :cond_9
    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v12, v5}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    cmp-long v5, v8, v10

    if-gtz v5, :cond_a

    move-object v8, v2

    move-object v9, v12

    move-wide/from16 v10, p2

    move-wide/from16 v12, p4

    invoke-static/range {v8 .. v13}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;JJ)V

    move-object v5, v4

    goto :goto_4

    :cond_a
    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v5}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    cmp-long v5, v8, v10

    if-gtz v5, :cond_b

    invoke-interface {v7, v12}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    move-object v5, v4

    goto :goto_4

    :goto_8
    if-eqz v6, :cond_7

    move-object/from16 v4, p0

    move-object v5, v2

    move-wide/from16 v8, p2

    move-wide/from16 v10, p4

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/fitness/b/c/ap;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;Ljava/util/List;JJ)V

    move-object/from16 v0, p0

    invoke-direct {v0, v14}, Lcom/google/android/gms/fitness/b/c/ap;->a(Ljava/util/Iterator;)Lcom/google/android/gms/fitness/b/c;

    move-result-object v6

    goto :goto_8

    :cond_c
    move-object v6, v4

    goto/16 :goto_5

    :cond_d
    move-object v4, v6

    goto :goto_7

    :cond_e
    move-object v6, v5

    goto :goto_8

    :cond_f
    move-object v3, v2

    goto/16 :goto_1

    :cond_10
    move-object v4, v3

    goto/16 :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 3

    .prologue
    .line 479
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 480
    const-string v1, "com.google.step_count.delta"

    iget-object v2, p0, Lcom/google/android/gms/fitness/b/c/ap;->c:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/fitness/b/c/ap;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/u;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 484
    const-string v1, "com.google.activity.segment"

    iget-object v2, p0, Lcom/google/android/gms/fitness/b/c/ap;->d:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/fitness/b/c/ap;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/u;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 487
    return-object v0
.end method
