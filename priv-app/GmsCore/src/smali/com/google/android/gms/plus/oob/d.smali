.class public abstract Lcom/google/android/gms/plus/oob/d;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field protected a:Z

.field protected b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

.field protected c:Lcom/google/android/gms/plus/oob/e;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 179
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 180
    iput-boolean p2, p0, Lcom/google/android/gms/plus/oob/d;->a:Z

    .line 181
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/d;->a()I

    move-result v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 183
    return-void
.end method

.method static a(Landroid/content/Context;ILcom/google/android/gms/plus/service/v1whitelisted/models/b;Z)Lcom/google/android/gms/plus/oob/d;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 131
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    const/4 v0, 0x0

    .line 135
    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v4

    .line 137
    if-eqz p2, :cond_2

    const-string v1, "pronoun"

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v2

    :goto_0
    if-eqz v1, :cond_3

    .line 138
    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    .line 164
    :goto_1
    if-eqz v1, :cond_11

    .line 167
    add-int/lit16 v0, p1, 0x1388

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/oob/d;->setId(I)V

    .line 172
    :cond_0
    :goto_2
    sget-object v0, Lcom/google/android/gms/plus/c/a;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "FieldView"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 173
    const-string v0, "FieldView"

    const-string v4, "%s from %s"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v1, v5, v3

    aput-object p2, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_1
    return-object v1

    :cond_2
    move v1, v3

    .line 137
    goto :goto_0

    .line 139
    :cond_3
    if-eqz p2, :cond_4

    const-string v1, "customGender"

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    move v1, v2

    :goto_3
    if-eqz v1, :cond_5

    .line 140
    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewCustomGender;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto :goto_1

    :cond_4
    move v1, v3

    .line 139
    goto :goto_3

    .line 141
    :cond_5
    const-string v1, "button"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 142
    new-instance v0, Lcom/google/android/gms/plus/oob/k;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/k;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto :goto_1

    .line 143
    :cond_6
    const-string v1, "check"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 144
    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewCheck;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewCheck;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto :goto_1

    .line 145
    :cond_7
    if-eqz p2, :cond_8

    const-string v1, "birthday"

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v2

    :goto_4
    if-eqz v1, :cond_9

    .line 146
    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewHiddenBirthday;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto :goto_1

    :cond_8
    move v1, v3

    .line 145
    goto :goto_4

    .line 147
    :cond_9
    if-eqz p2, :cond_b

    const-string v1, "date"

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "dayInYear"

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    :cond_a
    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->o()Z

    move-result v1

    if-eqz v1, :cond_b

    move v1, v2

    :goto_5
    if-eqz v1, :cond_c

    .line 148
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Mandatory non-birthday date fields are not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_b
    move v1, v3

    .line 147
    goto :goto_5

    .line 150
    :cond_c
    const-string v1, "info"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 151
    new-instance v0, Lcom/google/android/gms/plus/oob/l;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/l;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_1

    .line 152
    :cond_d
    invoke-static {p2}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 153
    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewName;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewName;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_1

    .line 154
    :cond_e
    const-string v1, "option"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 155
    const-string v1, "gender"

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 156
    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewGender;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewGender;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_1

    .line 157
    :cond_f
    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->o()Z

    move-result v1

    if-eqz v1, :cond_12

    .line 158
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Mandatory non-gender option fields are not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_10
    const-string v1, "string"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 162
    new-instance v0, Lcom/google/android/gms/plus/oob/FieldViewString;

    invoke-direct {v0, p0, p3}, Lcom/google/android/gms/plus/oob/FieldViewString;-><init>(Landroid/content/Context;Z)V

    move-object v1, v0

    goto/16 :goto_1

    .line 168
    :cond_11
    const-string v0, "FieldView"

    const/4 v5, 0x5

    invoke-static {v0, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    const-string v0, "FieldView"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Unsupported field: type="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_12
    move-object v1, v0

    goto/16 :goto_1
.end method


# virtual methods
.method protected abstract a()I
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/d;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 0

    .prologue
    .line 261
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 262
    invoke-static {p2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 263
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    .line 264
    iput-object p2, p0, Lcom/google/android/gms/plus/oob/d;->c:Lcom/google/android/gms/plus/oob/e;

    .line 265
    return-void
.end method

.method public abstract b()Z
.end method

.method public abstract c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final f()Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 227
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/d;->d()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_0
    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/d;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method protected g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 235
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final h()Z
    .locals 1

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/d;->g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;
    .locals 3

    .prologue
    .line 251
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->d:Ljava/util/Set;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->b:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->d:Ljava/util/Set;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->v()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->c:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->d:Ljava/util/Set;

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method protected final j()Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->m()Lcom/google/android/gms/plus/service/v1whitelisted/models/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/f;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 274
    :cond_0
    :goto_0
    return-object v4

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->c:Lcom/google/android/gms/plus/oob/e;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->m()Lcom/google/android/gms/plus/service/v1whitelisted/models/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/f;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->m()Lcom/google/android/gms/plus/service/v1whitelisted/models/f;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/f;->d()Ljava/util/List;

    move-result-object v3

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/oob/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v4

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 0

    .prologue
    .line 309
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 310
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 303
    invoke-super {p0}, Landroid/widget/LinearLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 314
    const-string v1, "%s<id=\"%s\" type=\"%s\" hidden=\"%s\">"

    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    if-nez v0, :cond_0

    const-string v0, ""

    :goto_0
    aput-object v0, v2, v3

    const/4 v3, 0x2

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    if-nez v0, :cond_1

    const-string v0, ""

    :goto_1
    aput-object v0, v2, v3

    const/4 v3, 0x3

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    if-nez v0, :cond_2

    const-string v0, ""

    :goto_2
    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->s()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/d;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->f()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_2
.end method
