.class public final Lcom/google/android/gms/plus/circlesbutton/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/w;


# instance fields
.field a:Z

.field b:Z

.field private final c:Lcom/google/android/gms/common/api/v;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private f:Ljava/util/Map;

.field private g:Lcom/google/android/gms/plus/circlesbutton/h;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/circlesbutton/h;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcom/google/android/gms/plus/circlesbutton/e;->c:Lcom/google/android/gms/common/api/v;

    .line 48
    iput-object p2, p0, Lcom/google/android/gms/plus/circlesbutton/e;->d:Ljava/lang/String;

    .line 49
    iput-object p3, p0, Lcom/google/android/gms/plus/circlesbutton/e;->e:Ljava/lang/String;

    .line 50
    iput-object p4, p0, Lcom/google/android/gms/plus/circlesbutton/e;->g:Lcom/google/android/gms/plus/circlesbutton/h;

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/circlesbutton/e;Lcom/google/android/gms/people/model/e;)V
    .locals 5

    .prologue
    .line 18
    invoke-virtual {p1}, Lcom/google/android/gms/people/model/e;->c()I

    move-result v1

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->f:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->f:Ljava/util/Map;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->f:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/people/model/e;->b(I)Lcom/google/android/gms/people/model/d;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/e;->f:Ljava/util/Map;

    invoke-interface {v2}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->b:Z

    invoke-virtual {p1}, Lcom/google/android/gms/people/model/e;->d()V

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->g:Lcom/google/android/gms/plus/circlesbutton/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->g:Lcom/google/android/gms/plus/circlesbutton/h;

    invoke-interface {v0}, Lcom/google/android/gms/plus/circlesbutton/h;->a()V

    :cond_2
    return-void
.end method


# virtual methods
.method public final Z_()V
    .locals 5

    .prologue
    .line 117
    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->a:Z

    if-eqz v0, :cond_0

    .line 132
    :goto_0
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->a:Z

    .line 121
    new-instance v0, Lcom/google/android/gms/people/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d;-><init>()V

    const/16 v1, -0x3e7

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/d;->a(I)Lcom/google/android/gms/people/d;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/d;->a(Z)Lcom/google/android/gms/people/d;

    move-result-object v0

    .line 124
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/e;->c:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/e;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/e;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/circlesbutton/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/circlesbutton/g;-><init>(Lcom/google/android/gms/plus/circlesbutton/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->f:Ljava/util/Map;

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->c:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->b:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->a:Z

    if-nez v0, :cond_2

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/circlesbutton/e;->a:Z

    .line 67
    new-instance v0, Lcom/google/android/gms/people/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d;-><init>()V

    const/16 v1, -0x3e7

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/d;->a(I)Lcom/google/android/gms/people/d;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/d;->a(Z)Lcom/google/android/gms/people/d;

    move-result-object v0

    .line 70
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/circlesbutton/e;->c:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/e;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/e;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/circlesbutton/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/circlesbutton/f;-><init>(Lcom/google/android/gms/plus/circlesbutton/e;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 79
    :cond_2
    sget-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/circlesbutton/e;->c:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/plus/circlesbutton/e;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/circlesbutton/e;->e:Ljava/lang/String;

    const/4 v5, 0x6

    move-object v2, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/v;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;

    goto :goto_0
.end method
