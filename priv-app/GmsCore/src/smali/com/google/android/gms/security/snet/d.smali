.class public final Lcom/google/android/gms/security/snet/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Boolean;

.field public f:[Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 52
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 53
    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/snet/d;->cachedSize:I

    .line 54
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 206
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 208
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 211
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    if-eqz v2, :cond_1

    .line 212
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 215
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_2

    .line 216
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 219
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    if-eqz v2, :cond_3

    .line 220
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 223
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 224
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 227
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v1

    move v3, v1

    .line 230
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_6

    .line 231
    iget-object v4, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 232
    if-eqz v4, :cond_5

    .line 233
    add-int/lit8 v3, v3, 0x1

    .line 234
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 230
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 238
    :cond_6
    add-int/2addr v0, v2

    .line 239
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 241
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 242
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 246
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 250
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 72
    if-ne p1, p0, :cond_1

    .line 139
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/snet/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 76
    goto :goto_0

    .line 78
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/snet/d;

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    if-nez v2, :cond_3

    .line 80
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_4

    move v0, v1

    .line 81
    goto :goto_0

    .line 83
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    if-nez v2, :cond_5

    .line 87
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    if-eqz v2, :cond_6

    move v0, v1

    .line 88
    goto :goto_0

    .line 90
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    if-nez v2, :cond_7

    .line 94
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    if-eqz v2, :cond_8

    move v0, v1

    .line 95
    goto :goto_0

    .line 97
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    if-nez v2, :cond_9

    .line 101
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    if-eqz v2, :cond_a

    move v0, v1

    .line 102
    goto :goto_0

    .line 104
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 105
    goto :goto_0

    .line 107
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_b

    .line 108
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_c

    move v0, v1

    .line 109
    goto :goto_0

    .line 111
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 112
    goto :goto_0

    .line 114
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 116
    goto/16 :goto_0

    .line 118
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 119
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 120
    goto/16 :goto_0

    .line 122
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 123
    goto/16 :goto_0

    .line 125
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    if-nez v2, :cond_10

    .line 126
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    if-eqz v2, :cond_11

    move v0, v1

    .line 127
    goto/16 :goto_0

    .line 129
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 130
    goto/16 :goto_0

    .line 132
    :cond_11
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 133
    iget-object v2, p1, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 134
    goto/16 :goto_0

    .line 136
    :cond_12
    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 137
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 147
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 149
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 151
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 153
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 155
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 159
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 161
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 163
    return v0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_2

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 153
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_4

    .line 157
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 159
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_6

    .line 161
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 173
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 176
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 179
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 181
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 182
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 184
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 185
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_6

    .line 186
    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->f:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 187
    if-eqz v1, :cond_5

    .line 188
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 185
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 192
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 193
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 195
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 196
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 198
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 199
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/security/snet/d;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 201
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 202
    return-void
.end method
