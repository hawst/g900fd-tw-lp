.class public final Lcom/google/android/gms/icing/c/a/x;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/icing/c/a/ac;

.field public b:[Lcom/google/android/gms/icing/c/a/y;

.field public c:[Lcom/google/android/gms/icing/c/a/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3911
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3912
    invoke-static {}, Lcom/google/android/gms/icing/c/a/ac;->a()[Lcom/google/android/gms/icing/c/a/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/y;->a()[Lcom/google/android/gms/icing/c/a/y;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/ab;->a()[Lcom/google/android/gms/icing/c/a/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/x;->cachedSize:I

    .line 3913
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3993
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3994
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 3995
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 3996
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    aget-object v3, v3, v0

    .line 3997
    if-eqz v3, :cond_0

    .line 3998
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3995
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 4003
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 4004
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 4005
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    aget-object v3, v3, v0

    .line 4006
    if-eqz v3, :cond_3

    .line 4007
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4004
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 4012
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 4013
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 4014
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    aget-object v2, v2, v1

    .line 4015
    if-eqz v2, :cond_6

    .line 4016
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4013
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4021
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3926
    if-ne p1, p0, :cond_1

    .line 3927
    const/4 v0, 0x1

    .line 3945
    :cond_0
    :goto_0
    return v0

    .line 3929
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/x;

    if-eqz v1, :cond_0

    .line 3932
    check-cast p1, Lcom/google/android/gms/icing/c/a/x;

    .line 3933
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3937
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3941
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3945
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/x;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3950
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3953
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3955
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3957
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/x;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3958
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2876
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/x;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/ac;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/c/a/ac;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/ac;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/c/a/ac;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/ac;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/y;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/icing/c/a/y;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/icing/c/a/y;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/ab;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/android/gms/icing/c/a/ab;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/ab;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/android/gms/icing/c/a/ab;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/ab;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3964
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 3965
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3966
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    aget-object v2, v2, v0

    .line 3967
    if-eqz v2, :cond_0

    .line 3968
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3965
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3972
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 3973
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 3974
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/x;->b:[Lcom/google/android/gms/icing/c/a/y;

    aget-object v2, v2, v0

    .line 3975
    if-eqz v2, :cond_2

    .line 3976
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3973
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3980
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 3981
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 3982
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->c:[Lcom/google/android/gms/icing/c/a/ab;

    aget-object v0, v0, v1

    .line 3983
    if-eqz v0, :cond_4

    .line 3984
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3981
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3988
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3989
    return-void
.end method
