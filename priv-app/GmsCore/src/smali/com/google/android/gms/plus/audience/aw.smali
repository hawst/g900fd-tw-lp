.class public abstract Lcom/google/android/gms/plus/audience/aw;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/google/android/gms/plus/audience/ad;
.implements Lcom/google/android/gms/plus/audience/ae;
.implements Lcom/google/android/gms/plus/audience/as;
.implements Lcom/google/android/gms/plus/audience/at;
.implements Lcom/google/android/gms/plus/audience/au;
.implements Lcom/google/android/gms/plus/audience/av;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:Z

.field private f:Z

.field g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

.field h:Landroid/widget/FrameLayout;

.field i:I

.field j:Landroid/widget/FrameLayout;

.field k:Z

.field l:Z

.field m:Z

.field n:Z

.field o:I

.field p:I

.field q:I

.field r:I

.field s:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 36
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 80
    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    .line 84
    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->k:Z

    .line 85
    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->l:Z

    .line 86
    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    .line 88
    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->o:I

    .line 503
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/aw;)I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->o:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/aw;I)I
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/aw;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/aw;->k:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/aw;I)I
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/google/android/gms/plus/audience/aw;->p:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/aw;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->f:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/aw;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/aw;I)I
    .locals 0

    .prologue
    .line 36
    iput p1, p0, Lcom/google/android/gms/plus/audience/aw;->q:I

    return p1
.end method

.method static d(I)I
    .locals 2

    .prologue
    const/high16 v1, -0x1000000

    .line 588
    and-int v0, p0, v1

    if-nez v0, :cond_0

    .line 589
    or-int/2addr p0, v1

    .line 591
    :cond_0
    return p0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/audience/aw;)Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/audience/aw;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->s:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/audience/aw;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->d:I

    return v0
.end method


# virtual methods
.method public final a(IZ)V
    .locals 1

    .prologue
    .line 302
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->b:I

    if-lt p1, v0, :cond_0

    if-eqz p2, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    if-nez v0, :cond_0

    .line 303
    iput p1, p0, Lcom/google/android/gms/plus/audience/aw;->b:I

    .line 304
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    .line 306
    :cond_0
    return-void
.end method

.method final a(Landroid/animation/Animator$AnimatorListener;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    .line 562
    iput-boolean v3, p0, Lcom/google/android/gms/plus/audience/aw;->k:Z

    .line 564
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_1

    .line 565
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v0, v3, v3}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->smoothScrollTo(II)V

    .line 566
    if-eqz p1, :cond_0

    .line 567
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/animation/Animator$AnimatorListener;->onAnimationEnd(Landroid/animation/Animator;)V

    .line 585
    :cond_0
    :goto_0
    return-void

    .line 572
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    const-string v1, "scrollY"

    const/4 v2, 0x1

    new-array v2, v2, [I

    aput v3, v2, v3

    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->bu:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    add-float/2addr v1, v5

    float-to-int v1, v1

    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->r:I

    int-to-float v2, v2

    const v3, 0x3f19999a    # 0.6f

    add-float/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/g;->bw:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    mul-float/2addr v2, v3

    iget v3, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    int-to-float v3, v3

    add-float/2addr v2, v3

    int-to-float v1, v1

    add-float/2addr v1, v2

    add-float/2addr v1, v5

    float-to-int v1, v1

    const-wide/16 v2, 0xc8

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getScrollY()I

    move-result v4

    int-to-long v4, v4

    mul-long/2addr v2, v4

    int-to-long v4, v1

    div-long/2addr v2, v4

    const-wide/16 v4, 0x96

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 575
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 576
    if-eqz p1, :cond_2

    .line 577
    invoke-virtual {v0, p1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 580
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    new-instance v2, Lcom/google/android/gms/plus/audience/bd;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/plus/audience/bd;-><init>(Lcom/google/android/gms/plus/audience/aw;Landroid/animation/ObjectAnimator;)V

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/audience/AudienceSelectionListView;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 271
    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-eqz v1, :cond_0

    .line 277
    invoke-virtual {p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    if-ne v2, v0, :cond_1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 280
    :cond_0
    return-void

    .line 277
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/audience/AudienceSelectionListView;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 285
    if-eqz p2, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->getFirstVisiblePosition()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 294
    iput v2, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    .line 295
    invoke-virtual {p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 297
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 255
    invoke-virtual {p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->ba:I

    if-eq v0, v1, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->k:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->c:I

    if-ge p2, v0, :cond_2

    .line 262
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->k:Z

    .line 263
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->c()V

    goto :goto_0

    .line 264
    :cond_2
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->b:I

    if-ge p2, v0, :cond_0

    .line 265
    iput v2, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    goto :goto_0
.end method

.method protected abstract b()V
.end method

.method public final b(I)V
    .locals 11

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-nez v0, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->d:I

    if-eq v0, p1, :cond_2

    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->r:I

    if-gt p1, v0, :cond_2

    .line 323
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->l:Z

    .line 325
    :cond_2
    iput p1, p0, Lcom/google/android/gms/plus/audience/aw;->d:I

    .line 326
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->p:I

    if-nez v0, :cond_3

    .line 328
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->s:Z

    goto :goto_0

    .line 331
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->s:Z

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->j:Landroid/widget/FrameLayout;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/google/android/gms/plus/audience/aw;->p:I

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 337
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->p:I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->bt:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sub-int v4, v0, v1

    .line 339
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bu:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v5, v0

    .line 341
    const/4 v0, 0x0

    .line 342
    if-nez p1, :cond_b

    .line 343
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bz:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    move v1, v0

    .line 347
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/g;->bw:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v6, v0

    .line 349
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/g;->by:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v7, v0

    .line 351
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/g;->bE:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v8, v0

    .line 353
    mul-int v0, p1, v6

    add-int/2addr v0, v1

    mul-int/lit8 v2, v7, 0x2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/g;->bE:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    add-int/2addr v0, v2

    .line 355
    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->q:I

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    sget v10, Lcom/google/android/gms/g;->bt:I

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    mul-float/2addr v3, v9

    float-to-int v3, v3

    sub-int v3, v2, v3

    .line 358
    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    sub-int v2, v4, v2

    sub-int/2addr v2, v5

    if-le v0, v2, :cond_a

    .line 359
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    sub-int v0, v4, v0

    sub-int/2addr v0, v5

    move v2, v0

    .line 362
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v9, Lcom/google/android/gms/g;->bs:I

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    .line 364
    if-le v3, v0, :cond_4

    move v3, v0

    .line 368
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->h:Landroid/widget/FrameLayout;

    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x1

    invoke-direct {v9, v10, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v9}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 372
    if-eqz v0, :cond_5

    .line 373
    new-instance v9, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v10, -0x2

    invoke-direct {v9, v3, v10}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v9}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 377
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->l:Z

    if-nez v0, :cond_0

    .line 380
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->l:Z

    .line 382
    int-to-float v0, p1

    .line 383
    iget v3, p0, Lcom/google/android/gms/plus/audience/aw;->r:I

    if-le p1, v3, :cond_7

    iget-boolean v3, p0, Lcom/google/android/gms/plus/audience/aw;->e:Z

    if-nez v3, :cond_7

    .line 385
    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->r:I

    int-to-float v0, v0

    const v3, 0x3f19999a    # 0.6f

    add-float/2addr v0, v3

    .line 386
    int-to-float v3, v6

    mul-float/2addr v0, v3

    int-to-float v3, v7

    add-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    int-to-float v3, v5

    add-float/2addr v0, v3

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 392
    :goto_3
    if-lt v0, v4, :cond_6

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->f:Z

    move v0, v4

    .line 396
    :cond_6
    iget v1, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/gms/plus/audience/aw;->b:I

    .line 398
    iget v1, p0, Lcom/google/android/gms/plus/audience/aw;->d:I

    const/4 v2, 0x2

    if-le v1, v2, :cond_8

    .line 399
    iget v1, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    mul-int/lit8 v2, v6, 0x2

    add-int/2addr v1, v2

    add-int/2addr v1, v8

    iput v1, p0, Lcom/google/android/gms/plus/audience/aw;->c:I

    .line 405
    :goto_4
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_9

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    new-instance v2, Lcom/google/android/gms/plus/audience/ba;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/plus/audience/ba;-><init>(Lcom/google/android/gms/plus/audience/aw;I)V

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    new-instance v1, Lcom/google/android/gms/plus/audience/bb;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/bb;-><init>(Lcom/google/android/gms/plus/audience/aw;)V

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 407
    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    new-instance v1, Lcom/google/android/gms/plus/audience/ay;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/ay;-><init>(Lcom/google/android/gms/plus/audience/aw;)V

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 389
    :cond_7
    int-to-float v3, v6

    mul-float/2addr v0, v3

    mul-int/lit8 v3, v7, 0x2

    int-to-float v3, v3

    add-float/2addr v0, v3

    int-to-float v3, v8

    add-float/2addr v0, v3

    iget v3, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    int-to-float v3, v3

    add-float/2addr v0, v3

    int-to-float v3, v5

    add-float/2addr v0, v3

    int-to-float v1, v1

    add-float/2addr v0, v1

    float-to-int v0, v0

    goto :goto_3

    .line 402
    :cond_8
    iget v1, p0, Lcom/google/android/gms/plus/audience/aw;->i:I

    iput v1, p0, Lcom/google/android/gms/plus/audience/aw;->c:I

    goto :goto_4

    .line 405
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    const-string v2, "scrollY"

    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    aput v0, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    new-instance v1, Lcom/google/android/gms/plus/audience/bf;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/bf;-><init>(Lcom/google/android/gms/plus/audience/aw;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    new-instance v2, Lcom/google/android/gms/plus/audience/bc;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/plus/audience/bc;-><init>(Lcom/google/android/gms/plus/audience/aw;Landroid/animation/ObjectAnimator;)V

    const-wide/16 v4, 0x32

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_5

    :cond_a
    move v2, v0

    goto/16 :goto_2

    :cond_b
    move v1, v0

    goto/16 :goto_1
.end method

.method protected abstract c()V
.end method

.method protected final c(I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 206
    iget-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-nez v2, :cond_1

    .line 230
    :cond_0
    :goto_0
    return v0

    .line 210
    :cond_1
    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->o:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    .line 213
    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->o:I

    if-ne v2, v0, :cond_2

    move v0, v1

    .line 215
    goto :goto_0

    .line 218
    :cond_2
    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->o:I

    .line 219
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/aw;->a(Landroid/animation/Animator$AnimatorListener;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    new-instance v2, Lcom/google/android/gms/plus/audience/ax;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/plus/audience/ax;-><init>(Lcom/google/android/gms/plus/audience/aw;I)V

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v2, v4, v5}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v1

    .line 230
    goto :goto_0
.end method

.method public final finish()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 199
    invoke-super {p0}, Landroid/support/v4/app/q;->finish()V

    .line 200
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0, v1, v1}, Lcom/google/android/gms/plus/audience/aw;->overridePendingTransition(II)V

    .line 203
    :cond_0
    return-void
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 310
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    return v0
.end method

.method protected final j()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 429
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->e:Z

    .line 430
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->s:Z

    .line 431
    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->l:Z

    .line 432
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->f:Z

    .line 433
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->k()V

    .line 435
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->setVisibility(I)V

    .line 436
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->n:Z

    .line 437
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_2

    .line 438
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->setScrollY(I)V

    .line 442
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/aw;->l()Lcom/google/android/gms/plus/audience/an;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/an;->a()Landroid/widget/ListView;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/an;->a()Landroid/widget/ListView;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/plus/audience/aw;->e:Z

    if-eqz v2, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/audience/aw;->d:I

    :cond_0
    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    .line 443
    :cond_1
    return-void

    .line 440
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getScrollX()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->scrollTo(II)V

    goto :goto_0
.end method

.method final k()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 447
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 449
    if-eqz v0, :cond_0

    .line 450
    new-instance v1, Lcom/google/android/gms/plus/audience/az;

    invoke-direct {v1, p0}, Lcom/google/android/gms/plus/audience/az;-><init>(Lcom/google/android/gms/plus/audience/aw;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 472
    :cond_0
    return-void
.end method

.method protected l()Lcom/google/android/gms/plus/audience/an;
    .locals 1

    .prologue
    .line 475
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final m()V
    .locals 2

    .prologue
    .line 512
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->a(Lcom/google/android/gms/plus/audience/av;)V

    .line 513
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->setVisibility(I)V

    .line 514
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->n:Z

    .line 515
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 104
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 105
    if-nez p1, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->n:Z

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->e:Z

    .line 108
    return-void

    .line 105
    :cond_0
    const-string v1, "STATE_IS_LIST_CONTAINER_VISIBLE"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 112
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 113
    const-string v0, "STATE_IS_LIST_CONTAINER_VISIBLE"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/aw;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 114
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    .prologue
    .line 250
    return-void
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 236
    if-nez p2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getScrollY()I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->b:I

    if-lt v1, v2, :cond_0

    .line 239
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getScrollY()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/plus/audience/aw;->b:I

    .line 240
    iput v0, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    .line 241
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/plus/audience/aw;->a:I

    if-ne v2, v0, :cond_1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 244
    :cond_0
    return-void

    .line 241
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 187
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onWindowFocusChanged(Z)V

    .line 188
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/aw;->m:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->h:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aw;->g:Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    invoke-virtual {v0, v2, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->scrollTo(II)V

    .line 194
    :cond_0
    return-void
.end method
