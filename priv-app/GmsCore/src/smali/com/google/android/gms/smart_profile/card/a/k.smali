.class final Lcom/google/android/gms/smart_profile/card/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/bg;

.field final synthetic b:Lcom/google/android/gms/smart_profile/card/a/j;


# direct methods
.method constructor <init>(Lcom/google/android/gms/smart_profile/card/a/j;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/a/k;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iput-object p2, p0, Lcom/google/android/gms/smart_profile/card/a/k;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 7

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/k;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/smart_profile/c;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/k;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v1

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/k;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v2

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/k;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v3

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/k;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v5, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v5, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-static {v4, v1, v2, v3}, Lcom/google/android/gms/smart_profile/aj;->a(Landroid/content/Context;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/k;->b:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 123
    return-void
.end method
