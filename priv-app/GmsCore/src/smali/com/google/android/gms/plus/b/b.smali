.class public final Lcom/google/android/gms/plus/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static i:Lcom/google/android/gms/plus/b/b;


# instance fields
.field public final a:Lcom/google/android/gms/common/server/n;

.field public final b:Lcom/google/android/gms/plus/b/p;

.field public final c:Lcom/google/android/gms/plus/b/n;

.field public final d:Lcom/google/android/gms/plus/b/c;

.field public final e:Lcom/google/android/gms/plus/b/d;

.field private final f:Lcom/google/android/gms/plus/b/m;

.field private final g:Lcom/google/android/gms/plus/b/k;

.field private final h:Lcom/google/android/gms/plus/service/b/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 17

    .prologue
    .line 122
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 123
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 125
    new-instance v1, Lcom/google/android/gms/plus/e/c;

    sget-object v3, Lcom/google/android/gms/plus/c/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/c/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/plus/c/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/plus/c/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    sget-object v7, Lcom/google/android/gms/plus/c/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v7}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    sget-object v8, Lcom/google/android/gms/plus/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v8}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/plus/e/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 126
    new-instance v3, Lcom/google/android/gms/plus/e/b;

    sget-object v4, Lcom/google/android/gms/plus/c/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, ""

    sget-object v4, Lcom/google/android/gms/plus/c/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    sget-object v4, Lcom/google/android/gms/plus/c/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    sget-object v4, Lcom/google/android/gms/plus/c/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/plus/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    move-object v4, v2

    invoke-direct/range {v3 .. v10}, Lcom/google/android/gms/plus/e/b;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 127
    invoke-static {v2}, Lcom/google/android/gms/plus/e/d;->b(Landroid/content/Context;)Lcom/google/android/gms/common/server/n;

    move-result-object v13

    .line 128
    invoke-static {v2}, Lcom/google/android/gms/people/service/g;->d(Landroid/content/Context;)Lcom/google/android/gms/people/service/g;

    move-result-object v14

    .line 129
    invoke-static {v2}, Lcom/google/android/gms/plus/e/d;->a(Landroid/content/Context;)Lcom/google/android/gms/common/server/n;

    move-result-object v15

    .line 131
    new-instance v4, Lcom/google/android/gms/plus/e/c;

    sget-object v5, Lcom/google/android/gms/common/a/b;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/common/a/b;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/plus/c/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    sget-object v5, Lcom/google/android/gms/plus/c/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    sget-object v5, Lcom/google/android/gms/plus/c/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/common/a/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    move-object v5, v2

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/plus/e/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-static {v2}, Lcom/google/android/gms/plus/e/d;->c(Landroid/content/Context;)Lcom/google/android/gms/common/server/q;

    move-result-object v16

    .line 133
    new-instance v5, Lcom/google/android/gms/plus/e/c;

    sget-object v6, Lcom/google/android/gms/plus/c/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/plus/c/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/plus/c/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    sget-object v6, Lcom/google/android/gms/plus/c/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Boolean;

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v10

    sget-object v6, Lcom/google/android/gms/plus/c/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/plus/c/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/String;

    move-object v6, v2

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/plus/e/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/gms/plus/b/b;->a:Lcom/google/android/gms/common/server/n;

    .line 135
    new-instance v2, Lcom/google/android/gms/plus/b/p;

    new-instance v5, Lcom/google/android/gms/plus/service/pos/o;

    invoke-direct {v5, v1}, Lcom/google/android/gms/plus/service/pos/o;-><init>(Lcom/google/android/gms/common/server/n;)V

    new-instance v1, Lcom/google/android/gms/plus/service/c/d;

    invoke-direct {v1, v3}, Lcom/google/android/gms/plus/service/c/d;-><init>(Lcom/google/android/gms/plus/e/b;)V

    sget-object v3, Lcom/google/android/gms/plus/service/f;->a:Lcom/google/android/gms/plus/service/f;

    invoke-direct {v2, v5, v1, v3}, Lcom/google/android/gms/plus/b/p;-><init>(Lcom/google/android/gms/plus/service/pos/o;Lcom/google/android/gms/plus/service/c/d;Lcom/google/android/gms/plus/service/f;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/plus/b/b;->b:Lcom/google/android/gms/plus/b/p;

    .line 137
    new-instance v1, Lcom/google/android/gms/plus/b/n;

    invoke-direct {v1, v15}, Lcom/google/android/gms/plus/b/n;-><init>(Lcom/google/android/gms/common/server/n;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    .line 138
    new-instance v1, Lcom/google/android/gms/plus/b/m;

    invoke-direct {v1, v14}, Lcom/google/android/gms/plus/b/m;-><init>(Lcom/google/android/gms/common/server/n;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/b/b;->f:Lcom/google/android/gms/plus/b/m;

    .line 139
    new-instance v1, Lcom/google/android/gms/plus/b/c;

    new-instance v2, Lcom/google/android/gms/plus/service/v1/a;

    invoke-direct {v2, v13}, Lcom/google/android/gms/plus/service/v1/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    invoke-direct {v1, v13, v2}, Lcom/google/android/gms/plus/b/c;-><init>(Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/plus/service/v1/a;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/b/b;->d:Lcom/google/android/gms/plus/b/c;

    .line 140
    new-instance v1, Lcom/google/android/gms/plus/b/k;

    new-instance v2, Lcom/google/android/gms/plus/service/v1/c;

    invoke-direct {v2, v13}, Lcom/google/android/gms/plus/service/v1/c;-><init>(Lcom/google/android/gms/common/server/n;)V

    invoke-direct {v1, v13, v2}, Lcom/google/android/gms/plus/b/k;-><init>(Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/plus/service/v1/c;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/b/b;->g:Lcom/google/android/gms/plus/b/k;

    .line 141
    new-instance v1, Lcom/google/android/gms/plus/b/d;

    invoke-direct {v1, v15, v4}, Lcom/google/android/gms/plus/b/d;-><init>(Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/b/b;->e:Lcom/google/android/gms/plus/b/d;

    .line 142
    new-instance v1, Lcom/google/android/gms/plus/service/b/b;

    move-object/from16 v0, v16

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/service/b/b;-><init>(Lcom/google/android/gms/common/server/q;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/gms/plus/b/b;->h:Lcom/google/android/gms/plus/service/b/b;

    .line 143
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/plus/b/b;
    .locals 2

    .prologue
    .line 111
    const-class v1, Lcom/google/android/gms/plus/b/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/plus/b/b;->i:Lcom/google/android/gms/plus/b/b;

    if-nez v0, :cond_0

    .line 112
    new-instance v0, Lcom/google/android/gms/plus/b/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/b/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/plus/b/b;->i:Lcom/google/android/gms/plus/b/b;

    .line 114
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/b/b;->i:Lcom/google/android/gms/plus/b/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/android/volley/ac;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 707
    const-string v0, "GooglePlusPlatform"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget-object v0, v0, Lcom/android/volley/m;->b:[B

    if-eqz v0, :cond_0

    .line 709
    const-string v0, "GooglePlusPlatform"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response code ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v2, v2, Lcom/android/volley/m;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") when requesting: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 713
    const-string v0, "GooglePlusPlatform"

    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 717
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;)I
    .locals 2

    .prologue
    .line 566
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->f:Lcom/google/android/gms/plus/b/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/b/m;->a(Lcom/google/android/gms/common/server/ClientContext;)I
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 567
    :catch_0
    move-exception v0

    .line 568
    const-string v1, "getGenderInternal"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    .line 569
    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;)Landroid/util/Pair;
    .locals 12

    .prologue
    .line 490
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->g:Lcom/google/android/gms/plus/b/k;

    packed-switch p2, :pswitch_data_0

    const-string v3, "visible"

    :goto_0
    packed-switch p3, :pswitch_data_1

    const-string v6, "alphabetical"

    :goto_1
    iget-object v0, v0, Lcom/google/android/gms/plus/b/k;->a:Lcom/google/android/gms/plus/service/v1/c;

    const-string v2, "me"

    const/4 v4, 0x0

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object v1, p1

    move-object/from16 v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/service/v1/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v6

    sget-object v0, Lcom/google/android/gms/plus/internal/aa;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v7

    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v6, :cond_1

    invoke-interface {v5, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/c/a;

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->f()Lcom/google/android/gms/plus/model/c/f;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    move-object v2, v1

    :goto_3
    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->d()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->e()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->g()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown objectType state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 492
    :catch_0
    move-exception v0

    .line 494
    const-string v1, "listPeople"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    .line 495
    throw v0

    .line 490
    :pswitch_0
    :try_start_1
    const-string v3, "visible"

    goto :goto_0

    :pswitch_1
    const-string v3, "connected"

    goto :goto_0

    :pswitch_2
    const-string v6, "alphabetical"

    goto :goto_1

    :pswitch_3
    const-string v6, "best"

    goto :goto_1

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->f()Lcom/google/android/gms/plus/model/c/f;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/model/c/f;->d()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_3

    :pswitch_4
    const-string v1, "person"

    :goto_4
    invoke-interface {v0}, Lcom/google/android/gms/plus/model/c/a;->h()Ljava/lang/String;

    move-result-object v0

    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    const-string v11, "displayName"

    invoke-virtual {v10, v11, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "personId"

    invoke-virtual {v10, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "image"

    invoke-virtual {v10, v8, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "objectType"

    invoke-virtual {v10, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "url"

    invoke-virtual {v10, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v10}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :pswitch_5
    const-string v1, "page"

    goto :goto_4

    :cond_1
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x0

    invoke-virtual {v7, v1}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->g()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    .prologue
    const/4 v0, 0x0

    .line 586
    :try_start_0
    iget-object v10, p0, Lcom/google/android/gms/plus/b/b;->g:Lcom/google/android/gms/plus/b/k;

    invoke-static {}, Lcom/google/android/gms/common/data/j;->g()Lcom/google/android/gms/common/data/m;

    move-result-object v11

    new-instance v7, Lcom/google/android/gms/plus/b/l;

    invoke-direct {v7, v10, v11}, Lcom/google/android/gms/plus/b/l;-><init>(Lcom/google/android/gms/plus/b/k;Lcom/google/android/gms/common/data/m;)V

    const-class v1, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v1}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->a(Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FieldMappingDictionary;

    move-result-object v6

    iget-object v1, v10, Lcom/google/android/gms/plus/b/k;->b:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/n;->c()V

    move v9, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v9, v0, :cond_0

    iget-object v1, v10, Lcom/google/android/gms/plus/b/k;->a:Lcom/google/android/gms/plus/service/v1/c;

    invoke-interface {p2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/plus/service/v1/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    move-object v1, p1

    move-object v8, v7

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Lcom/android/volley/x;Lcom/android/volley/w;)V

    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_0

    :cond_0
    iget-object v0, v10, Lcom/google/android/gms/plus/b/k;->b:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/n;->e()V

    invoke-virtual {v7}, Lcom/google/android/gms/common/util/f;->b()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "com.google.android.gms.plus.IsSafeParcelable"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/google/android/gms/common/data/m;->a(Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 587
    :catch_0
    move-exception v0

    .line 589
    const-string v1, "getPeople"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    .line 590
    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/server/response/SafeParcelResponse;
    .locals 3

    .prologue
    .line 530
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->g:Lcom/google/android/gms/plus/b/k;

    iget-object v0, v0, Lcom/google/android/gms/plus/b/k;->a:Lcom/google/android/gms/plus/service/v1/c;

    const-class v1, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    const-class v2, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->a(Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FieldMappingDictionary;

    move-result-object v2

    invoke-virtual {v0, p1, p2, v1, v2}, Lcom/google/android/gms/plus/service/v1/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/SafeParcelResponse;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 531
    :catch_0
    move-exception v0

    .line 533
    const-string v1, "getPerson"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    .line 534
    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;I)Lcom/google/android/gms/plus/data/a/c;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 162
    if-eq p3, v0, :cond_6

    .line 163
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 164
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/plus/provider/d;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v3

    .line 167
    if-eqz v3, :cond_5

    .line 168
    :try_start_0
    invoke-virtual {v3}, Landroid/database/AbstractWindowedCursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 170
    const-string v2, "signedUp"

    invoke-virtual {v3, v2}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 172
    invoke-virtual {v3, v2}, Landroid/database/AbstractWindowedCursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_1

    move v2, v1

    .line 173
    :goto_0
    if-nez v2, :cond_2

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->b:Lcom/google/android/gms/plus/b/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/b/p;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/data/a/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 197
    if-eqz v3, :cond_0

    .line 198
    invoke-virtual {v3}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 204
    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move v2, v0

    .line 172
    goto :goto_0

    .line 178
    :cond_2
    :try_start_1
    const-string v2, "updated"

    invoke-virtual {v3, v2}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 179
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v3, v2}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 181
    const-wide/32 v6, 0x36ee80

    cmp-long v2, v4, v6

    if-lez v2, :cond_3

    .line 183
    new-instance v2, Lcom/google/android/gms/plus/service/a/o;

    const/4 v4, 0x0

    invoke-direct {v2, p2, v4}, Lcom/google/android/gms/plus/service/a/o;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/c;)V

    invoke-static {p1, v2}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 187
    :cond_3
    const-string v2, "display_name"

    invoke-virtual {v3, v2}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v3, v2}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 189
    const-string v4, "profile_image_url"

    invoke-virtual {v3, v4}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 191
    const-string v5, "signedUp"

    invoke-virtual {v3, v5}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-virtual {v3, v5}, Landroid/database/AbstractWindowedCursor;->getInt(I)I

    move-result v5

    if-eqz v5, :cond_4

    move v1, v0

    .line 193
    :cond_4
    new-instance v0, Lcom/google/android/gms/plus/data/a/c;

    invoke-direct {v0, v2, v4, v1}, Lcom/google/android/gms/plus/data/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 197
    if-eqz v3, :cond_0

    .line 198
    invoke-virtual {v3}, Landroid/database/AbstractWindowedCursor;->close()V

    goto :goto_1

    .line 197
    :cond_5
    if-eqz v3, :cond_6

    .line 198
    invoke-virtual {v3}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 204
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->b:Lcom/google/android/gms/plus/b/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/b/p;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/data/a/c;

    move-result-object v0

    goto :goto_1

    .line 197
    :catchall_0
    move-exception v0

    if-eqz v3, :cond_7

    .line 198
    invoke-virtual {v3}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_7
    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;
    .locals 6

    .prologue
    .line 802
    :try_start_0
    invoke-virtual {p3}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ac/c/a/a/a/q;->a([B)Lcom/google/ac/c/a/a/a/q;

    move-result-object v0

    .line 805
    const-string v1, "SmartProfile"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 806
    const-string v1, "SmartProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending payload:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 808
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/b/b;->h:Lcom/google/android/gms/plus/service/b/b;

    invoke-virtual {p3}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->d()I

    move-result v3

    new-instance v4, Lcom/google/c/e/b/a/a/d;

    invoke-direct {v4}, Lcom/google/c/e/b/a/a/d;-><init>()V

    iput-object v0, v4, Lcom/google/c/e/b/a/a/d;->a:Lcom/google/ac/c/a/a/a/q;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/plus/service/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/c/e/b/a/a/b;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/e/b/a/a/d;->apiHeader:Lcom/google/c/e/b/a/a/b;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/b/b;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    const-string v3, "/getsmartprofile?alt=proto"

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/c/e/b/a/a/e;

    invoke-direct {v5}, Lcom/google/c/e/b/a/a/e;-><init>()V

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/e/b/a/a/e;

    iget-object v1, v0, Lcom/google/c/e/b/a/a/e;->a:Lcom/google/ac/c/a/a/a/r;

    .line 811
    new-instance v0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;

    invoke-static {v1}, Lcom/google/ac/c/a/a/a/r;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;-><init>([B)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 814
    :goto_0
    return-object v0

    .line 812
    :catch_0
    move-exception v0

    .line 813
    const-string v1, "SmartProfile"

    const-string v2, "Failed to parse smart profile cards"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 814
    const/4 v0, 0x0

    goto :goto_0

    .line 815
    :catch_1
    move-exception v0

    .line 816
    const-string v1, "SmartProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get smart profile cards for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 817
    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;
    .locals 6

    .prologue
    .line 843
    :try_start_0
    invoke-virtual {p4}, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/c/f/c/a/a/b;->a([B)Lcom/google/c/f/c/a/a/b;

    move-result-object v0

    .line 847
    const-string v1, "SmartProfile"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 848
    const-string v1, "SmartProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending payload:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 850
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/b/b;->h:Lcom/google/android/gms/plus/service/b/b;

    invoke-virtual {p4}, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p4}, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;->d()I

    move-result v3

    new-instance v4, Lcom/google/c/e/b/a/a/j;

    invoke-direct {v4}, Lcom/google/c/e/b/a/a/j;-><init>()V

    iput-object v0, v4, Lcom/google/c/e/b/a/a/j;->a:Lcom/google/c/f/c/a/a/b;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/plus/service/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/c/e/b/a/a/b;

    move-result-object v0

    iput-object v0, v4, Lcom/google/c/e/b/a/a/j;->apiHeader:Lcom/google/c/e/b/a/a/b;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/b/b;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    packed-switch p3, :pswitch_data_0

    new-instance v0, Lcom/android/volley/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown relationship "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 857
    :catch_0
    move-exception v0

    .line 858
    const-string v1, "SmartProfile"

    const-string v2, "Failed to parse people for profiles response"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 859
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 850
    :pswitch_0
    :try_start_1
    const-string v3, "/loadpeopleincommon?alt=proto"

    :goto_1
    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/c/e/b/a/a/k;

    invoke-direct {v5}, Lcom/google/c/e/b/a/a/k;-><init>()V

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/e/b/a/a/k;

    iget-object v1, v0, Lcom/google/c/e/b/a/a/k;->a:Lcom/google/c/f/c/a/b/c;

    .line 855
    new-instance v0, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;

    invoke-static {v1}, Lcom/google/c/f/c/a/b/c;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;-><init>([B)V
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 860
    :catch_1
    move-exception v0

    .line 861
    const-string v1, "SmartProfile"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to load parse people for profiles for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 863
    throw v0

    .line 850
    :pswitch_1
    :try_start_2
    const-string v3, "/lookupownerincomingmembers?alt=proto"
    :try_end_2
    .catch Lcom/google/protobuf/nano/i; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;
    .locals 8

    .prologue
    .line 400
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->d:Lcom/google/android/gms/plus/b/c;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/b/c;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1/MomentsFeed;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 402
    :catch_0
    move-exception v0

    .line 404
    const-string v1, "listMoments"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    .line 405
    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;
    .locals 6

    .prologue
    .line 778
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    move-result-object v1

    .line 781
    invoke-static {v1}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fi;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 785
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    .line 787
    :try_start_0
    const-string v0, "cp"

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 788
    invoke-static {p1, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 789
    const-string v0, "cp"

    invoke-static {p1, v2, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 796
    :cond_0
    :goto_0
    return-object v1

    .line 790
    :catch_0
    move-exception v0

    .line 792
    const-string v3, "UpgradeAccount"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception updating service bits for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 218
    const/4 v0, 0x0

    .line 221
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/plus/b/b;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;I)Lcom/google/android/gms/plus/data/a/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/c;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;
    .locals 2

    .prologue
    .line 546
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/b/n;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 547
    :catch_0
    move-exception v0

    .line 549
    const-string v1, "getPerson"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    .line 550
    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    .locals 2

    .prologue
    .line 509
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/b/b;->g:Lcom/google/android/gms/plus/b/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/b/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 510
    :catch_0
    move-exception v0

    .line 512
    const-string v1, "getPerson"

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/b;->a(Lcom/android/volley/ac;Ljava/lang/String;)V

    .line 513
    throw v0
.end method
