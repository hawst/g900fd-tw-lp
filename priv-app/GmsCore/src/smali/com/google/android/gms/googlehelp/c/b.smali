.class public final Lcom/google/android/gms/googlehelp/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/Set;

.field public final b:Ljava/util/List;


# direct methods
.method private constructor <init>(Ljava/util/Set;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/b;->a:Ljava/util/Set;

    .line 46
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/b;->b:Ljava/util/List;

    .line 47
    return-void
.end method

.method public static a([Ljava/lang/String;Landroid/widget/LinearLayout;)Lcom/google/android/gms/googlehelp/c/b;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 65
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 66
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 68
    array-length v6, p0

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    aget-object v0, p0, v3

    .line 69
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 70
    const-string v1, "\\+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v8

    .line 71
    array-length v9, v8

    move v1, v2

    :goto_1
    if-ge v1, v9, :cond_1

    aget-object v0, v8, v1

    .line 72
    const-string v10, "\\:"

    invoke-virtual {v0, v10}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v10

    .line 73
    aget-object v0, v10, v2

    invoke-virtual {p1, v0}, Landroid/widget/LinearLayout;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/af;

    .line 75
    const/4 v11, 0x1

    aget-object v10, v10, v11

    invoke-static {v0, v10}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v10

    invoke-interface {v7, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    if-eqz v0, :cond_0

    .line 79
    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 82
    :cond_1
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 68
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 84
    :cond_2
    new-instance v0, Lcom/google/android/gms/googlehelp/c/b;

    invoke-direct {v0, v5, v4}, Lcom/google/android/gms/googlehelp/c/b;-><init>(Ljava/util/Set;Ljava/util/List;)V

    return-object v0
.end method
