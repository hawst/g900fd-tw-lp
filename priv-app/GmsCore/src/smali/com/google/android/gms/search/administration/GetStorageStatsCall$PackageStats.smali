.class public Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/administration/a;


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Z

.field public d:J

.field final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/gms/search/administration/a;

    invoke-direct {v0}, Lcom/google/android/gms/search/administration/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->CREATOR:Lcom/google/android/gms/search/administration/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->e:I

    .line 170
    return-void
.end method

.method constructor <init>(ILjava/lang/String;JZJ)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 188
    iput p1, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->e:I

    .line 189
    iput-object p2, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->a:Ljava/lang/String;

    .line 190
    iput-wide p3, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->b:J

    .line 191
    iput-boolean p5, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->c:Z

    .line 192
    iput-wide p6, p0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->d:J

    .line 193
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 199
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->CREATOR:Lcom/google/android/gms/search/administration/a;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 205
    sget-object v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->CREATOR:Lcom/google/android/gms/search/administration/a;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/administration/a;->a(Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;Landroid/os/Parcel;)V

    .line 206
    return-void
.end method
