.class public final Lcom/google/android/gms/games/g/aj;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/games/g/aj;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:[Lcom/google/android/gms/games/g/ak;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6644
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6645
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/aj;->b:I

    iput v1, p0, Lcom/google/android/gms/games/g/aj;->c:I

    invoke-static {}, Lcom/google/android/gms/games/g/ak;->a()[Lcom/google/android/gms/games/g/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/aj;->cachedSize:I

    .line 6646
    return-void
.end method

.method public static a()[Lcom/google/android/gms/games/g/aj;
    .locals 2

    .prologue
    .line 6621
    sget-object v0, Lcom/google/android/gms/games/g/aj;->e:[Lcom/google/android/gms/games/g/aj;

    if-nez v0, :cond_1

    .line 6622
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6624
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/g/aj;->e:[Lcom/google/android/gms/games/g/aj;

    if-nez v0, :cond_0

    .line 6625
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/games/g/aj;

    sput-object v0, Lcom/google/android/gms/games/g/aj;->e:[Lcom/google/android/gms/games/g/aj;

    .line 6627
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6629
    :cond_1
    sget-object v0, Lcom/google/android/gms/games/g/aj;->e:[Lcom/google/android/gms/games/g/aj;

    return-object v0

    .line 6627
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 6723
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6724
    iget-object v1, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6725
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6728
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/g/aj;->b:I

    if-eqz v1, :cond_1

    .line 6729
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/g/aj;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6732
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/aj;->c:I

    if-eqz v1, :cond_2

    .line 6733
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/aj;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6736
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 6737
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 6738
    iget-object v2, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    aget-object v2, v2, v0

    .line 6739
    if-eqz v2, :cond_3

    .line 6740
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 6737
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v1

    .line 6745
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6659
    if-ne p1, p0, :cond_1

    .line 6683
    :cond_0
    :goto_0
    return v0

    .line 6662
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/aj;

    if-nez v2, :cond_2

    move v0, v1

    .line 6663
    goto :goto_0

    .line 6665
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/aj;

    .line 6666
    iget-object v2, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 6667
    iget-object v2, p1, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 6668
    goto :goto_0

    .line 6670
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 6671
    goto :goto_0

    .line 6673
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/g/aj;->b:I

    iget v3, p1, Lcom/google/android/gms/games/g/aj;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 6674
    goto :goto_0

    .line 6676
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/g/aj;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/aj;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 6677
    goto :goto_0

    .line 6679
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    iget-object v3, p1, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 6681
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 6688
    iget-object v0, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 6691
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/aj;->b:I

    add-int/2addr v0, v1

    .line 6692
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/aj;->c:I

    add-int/2addr v0, v1

    .line 6693
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6695
    return v0

    .line 6688
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6469
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/aj;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/aj;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/g/ak;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/games/g/ak;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/games/g/ak;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 6701
    iget-object v0, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6702
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/aj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 6704
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/g/aj;->b:I

    if-eqz v0, :cond_1

    .line 6705
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/g/aj;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6707
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/aj;->c:I

    if-eqz v0, :cond_2

    .line 6708
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/aj;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6710
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 6711
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 6712
    iget-object v1, p0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    aget-object v1, v1, v0

    .line 6713
    if-eqz v1, :cond_3

    .line 6714
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6711
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6718
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6719
    return-void
.end method
