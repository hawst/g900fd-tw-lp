.class public final Lcom/google/android/gms/fitness/sensors/a/c;
.super Lcom/google/android/gms/fitness/sensors/c/c;
.source "SourceFile"


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/util/List;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    sget-boolean v0, Lcom/google/android/gms/fitness/sensors/a/c;->a:Z

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/fitness/sensors/c/c;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/util/List;Z)V

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/a/c;->d:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/data/Device;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/c/b;

    .line 38
    check-cast v0, Lcom/google/android/gms/fitness/c/a/a/b;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/c/a/a/b;->a(Lcom/google/android/gms/fitness/data/Device;)V

    goto :goto_0

    .line 40
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;)Z
    .locals 2

    .prologue
    .line 31
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/c;->d:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
