.class public Lcom/google/android/gms/wallet/common/ui/bq;
.super Landroid/widget/AdapterView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bj;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private a:Landroid/database/DataSetObserver;

.field public b:Landroid/widget/ArrayAdapter;

.field protected c:Lcom/google/android/gms/wallet/common/ui/bl;

.field protected d:I

.field protected e:Landroid/view/View;

.field protected f:Landroid/widget/LinearLayout;

.field private g:Z

.field private h:I

.field private i:I

.field private j:I

.field private k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/bq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/common/ui/bq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 66
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->g:Z

    .line 46
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->d:I

    .line 51
    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/bq;->h:I

    .line 52
    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/bq;->i:I

    .line 53
    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/bq;->j:I

    .line 55
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    .line 68
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v3, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v3, v1}, Lcom/google/android/gms/wallet/common/ui/bq;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 72
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 99
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 101
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/bq;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/bq;I)V
    .locals 4

    .prologue
    .line 31
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/bq;Z)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/bq;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 240
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    .line 241
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setClickable(Z)V

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 243
    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/bq;I)I
    .locals 0

    .prologue
    .line 31
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    return p1
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v4

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 211
    new-array v0, v1, [I

    const v3, 0x101030e

    aput v3, v0, v2

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v5

    move v3, v2

    .line 213
    :goto_0
    if-ge v3, v4, :cond_2

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3, v6, v7}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 216
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->g:Z

    if-eqz v0, :cond_0

    .line 217
    invoke-virtual {v5, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 218
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bs;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/wallet/common/ui/bs;-><init>(Lcom/google/android/gms/wallet/common/ui/bq;I)V

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 224
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v6, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 226
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 213
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 224
    goto :goto_1

    .line 228
    :cond_2
    invoke-virtual {v5}, Landroid/content/res/TypedArray;->recycle()V

    .line 229
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->c()V

    .line 231
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-nez v0, :cond_4

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    move v0, v2

    :goto_2
    if-ge v0, v1, :cond_3

    .line 233
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setAlpha(F)V

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 235
    :cond_3
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/common/ui/bq;->a(Z)V

    .line 237
    :cond_4
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/bq;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->b()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/bq;Z)V
    .locals 3

    .prologue
    .line 31
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setFocusable(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/bq;)Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/bq;)Z
    .locals 1

    .prologue
    .line 31
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->g:Z

    return v0
.end method

.method private o()Lcom/google/android/gms/wallet/common/ui/bk;
    .locals 1

    .prologue
    .line 492
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bv;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bv;-><init>(Lcom/google/android/gms/wallet/common/ui/bq;)V

    return-object v0
.end method


# virtual methods
.method protected final a(FI)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 336
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->h:I

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->i:I

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->h:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->j:I

    .line 338
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->j:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    sub-float v1, v3, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    int-to-float v1, p2

    mul-float/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 345
    sub-float v0, p1, v3

    int-to-float v1, p2

    mul-float/2addr v1, v0

    .line 346
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 347
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 348
    invoke-virtual {v3, p1}, Landroid/view/View;->setAlpha(F)V

    .line 349
    invoke-virtual {v3, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 346
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    :cond_0
    return-void
.end method

.method public a(Landroid/widget/ArrayAdapter;)V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/bq;->a(Landroid/widget/ArrayAdapter;Z)V

    .line 113
    return-void
.end method

.method public final a(Landroid/widget/ArrayAdapter;Z)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->a:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 120
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    .line 121
    iput-boolean p2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->g:Z

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_1

    .line 124
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/br;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/br;-><init>(Lcom/google/android/gms/wallet/common/ui/bq;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->a:Landroid/database/DataSetObserver;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->a:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 142
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->a()V

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->l()V

    .line 144
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->b()V

    .line 145
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/bl;)V
    .locals 0

    .prologue
    .line 149
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->c:Lcom/google/android/gms/wallet/common/ui/bl;

    .line 150
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 381
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-nez v0, :cond_0

    .line 382
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->m()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->o()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 386
    :cond_1
    return-void
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    .line 435
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 436
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->n()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 438
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->o()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    :cond_1
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 358
    if-eqz p1, :cond_0

    .line 359
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/bq;->a(FI)V

    .line 360
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->requestLayout()V

    .line 361
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    .line 367
    :goto_0
    return-void

    .line 363
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/bq;->a(FI)V

    .line 364
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->requestLayout()V

    .line 365
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    goto :goto_0
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 262
    return-void
.end method

.method public final c(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 483
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->o()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    :cond_0
    return-void
.end method

.method protected d()I
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x0

    return v0
.end method

.method protected e()I
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    return v0
.end method

.method protected f()I
    .locals 1

    .prologue
    .line 421
    sget v0, Lcom/google/android/gms/n;->A:I

    return v0
.end method

.method protected g()I
    .locals 1

    .prologue
    .line 471
    sget v0, Lcom/google/android/gms/p;->Ap:I

    return v0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 6

    .prologue
    .line 426
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->e()I

    move-result v0

    .line 427
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->f()I

    move-result v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 476
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 94
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    return v0
.end method

.method public final k()Landroid/widget/ArrayAdapter;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    return-object v0
.end method

.method protected final l()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/bq;->removeViewInLayout(Landroid/view/View;)V

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getSelectedItemPosition()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v3, v4, p0}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    .line 187
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->g:Z

    if-eqz v0, :cond_1

    .line 188
    new-array v0, v1, [I

    const v3, 0x101030e

    aput v3, v0, v2

    .line 189
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 190
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 191
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 193
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 197
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-ne v0, v1, :cond_2

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    .line 202
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bq;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    .line 203
    return-void

    :cond_3
    move v0, v2

    .line 193
    goto :goto_0
.end method

.method public final m()Lcom/google/android/gms/wallet/common/ui/bk;
    .locals 2

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->d()I

    move-result v0

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bt;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/wallet/common/ui/bt;-><init>(Lcom/google/android/gms/wallet/common/ui/bq;I)V

    return-object v1
.end method

.method public final n()Lcom/google/android/gms/wallet/common/ui/bk;
    .locals 2

    .prologue
    .line 443
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->d()I

    move-result v0

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/bu;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/wallet/common/ui/bu;-><init>(Lcom/google/android/gms/wallet/common/ui/bq;I)V

    return-object v1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->c:Lcom/google/android/gms/wallet/common/ui/bl;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->c:Lcom/google/android/gms/wallet/common/ui/bl;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bl;->a()V

    .line 176
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_1

    .line 290
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getChildCount()I

    move-result v1

    .line 291
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 292
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/bq;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 293
    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v3

    const/16 v4, 0x8

    if-eq v3, v4, :cond_0

    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getPaddingTop()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 291
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 301
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 270
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-ne v0, v3, :cond_1

    .line 276
    :cond_0
    invoke-static {v2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 277
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/bq;->measureChildren(II)V

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->h:I

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->i:I

    .line 281
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    if-ne v0, v3, :cond_2

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->i:I

    :goto_0
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->j:I

    .line 284
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->j:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/common/ui/bq;->setMeasuredDimension(II)V

    .line 285
    return-void

    .line 281
    :cond_2
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->h:I

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 76
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 77
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 83
    :goto_0
    return-void

    .line 80
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 81
    const-string v0, "superInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 82
    const-string v0, "state"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 87
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 88
    const-string v1, "superInstanceState"

    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 89
    const-string v1, "state"

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/bq;->k:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 90
    return-object v0
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 316
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 318
    if-eqz p1, :cond_0

    .line 319
    invoke-virtual {p1, v6}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 321
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/bq;->getOnItemClickListener()Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    move v0, v6

    .line 325
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 30
    check-cast p1, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/bq;->a(Landroid/widget/ArrayAdapter;)V

    return-void
.end method

.method public setSelection(I)V
    .locals 0

    .prologue
    .line 166
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    return v0
.end method
