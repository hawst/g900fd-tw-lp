.class public final Lcom/google/android/gms/analytics/bv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field final b:Ljava/util/Map;

.field public c:Z

.field public d:Lcom/google/android/gms/analytics/bw;

.field e:Lcom/google/android/gms/analytics/bx;

.field public f:Lcom/google/android/gms/analytics/z;

.field private final g:Lcom/google/android/gms/analytics/by;

.field private final h:Ljava/util/Map;

.field private i:Lcom/google/android/gms/analytics/bq;

.field private final j:Lcom/google/android/gms/analytics/o;

.field private final k:Lcom/google/android/gms/analytics/br;

.field private final l:Lcom/google/android/gms/analytics/j;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/analytics/by;Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 133
    invoke-static {}, Lcom/google/android/gms/analytics/o;->a()Lcom/google/android/gms/analytics/o;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/analytics/br;->a()Lcom/google/android/gms/analytics/br;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/analytics/j;->a()Lcom/google/android/gms/analytics/j;

    move-result-object v5

    new-instance v6, Lcom/google/android/gms/analytics/bk;

    const-string v0, "tracking"

    const/4 v1, 0x0

    invoke-direct {v6, v0, v1}, Lcom/google/android/gms/analytics/bk;-><init>(Ljava/lang/String;B)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/analytics/bv;-><init>(Ljava/lang/String;Lcom/google/android/gms/analytics/by;Lcom/google/android/gms/analytics/o;Lcom/google/android/gms/analytics/br;Lcom/google/android/gms/analytics/j;Lcom/google/android/gms/analytics/bq;Landroid/content/Context;)V

    .line 137
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/analytics/by;Lcom/google/android/gms/analytics/o;Lcom/google/android/gms/analytics/br;Lcom/google/android/gms/analytics/j;Lcom/google/android/gms/analytics/bq;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    .line 145
    iput-object p2, p0, Lcom/google/android/gms/analytics/bv;->g:Lcom/google/android/gms/analytics/by;

    .line 146
    if-eqz p7, :cond_0

    .line 147
    invoke-virtual {p7}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/bv;->a:Landroid/content/Context;

    .line 149
    :cond_0
    if-eqz p1, :cond_1

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    const-string v1, "&tid"

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    const-string v1, "useSecure"

    const-string v2, "1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    iput-object p3, p0, Lcom/google/android/gms/analytics/bv;->j:Lcom/google/android/gms/analytics/o;

    .line 157
    iput-object p4, p0, Lcom/google/android/gms/analytics/bv;->k:Lcom/google/android/gms/analytics/br;

    .line 158
    iput-object p5, p0, Lcom/google/android/gms/analytics/bv;->l:Lcom/google/android/gms/analytics/j;

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    const-string v1, "&a"

    new-instance v2, Ljava/util/Random;

    invoke-direct {v2}, Ljava/util/Random;-><init>()V

    const v3, 0x7fffffff

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 164
    iput-object p6, p0, Lcom/google/android/gms/analytics/bv;->i:Lcom/google/android/gms/analytics/bq;

    .line 166
    new-instance v0, Lcom/google/android/gms/analytics/bw;

    invoke-direct {v0, p0}, Lcom/google/android/gms/analytics/bw;-><init>(Lcom/google/android/gms/analytics/bv;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/bv;->d:Lcom/google/android/gms/analytics/bw;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    const-string v1, "&ate"

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    const-string v1, "&adid"

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 169
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 395
    const-string v0, "Key should be non-null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/analytics/at;->k:Lcom/google/android/gms/analytics/at;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/as;->a(Lcom/google/android/gms/analytics/at;)V

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 398
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 284
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/analytics/at;->l:Lcom/google/android/gms/analytics/at;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/as;->a(Lcom/google/android/gms/analytics/at;)V

    .line 285
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 287
    if-eqz p1, :cond_0

    .line 288
    invoke-interface {v2, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 293
    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 294
    iget-object v4, p0, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 297
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 299
    const-string v0, "&tid"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 300
    const-string v0, "Missing tracking id (%s) parameter."

    new-array v3, v1, [Ljava/lang/Object;

    const-string v4, "&tid"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->d(Ljava/lang/String;)V

    .line 303
    :cond_3
    const-string v0, "&t"

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 304
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 305
    const-string v0, "Missing hit type (%s) parameter."

    new-array v3, v1, [Ljava/lang/Object;

    const-string v4, "&t"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->d(Ljava/lang/String;)V

    .line 306
    const-string v0, ""

    .line 309
    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/analytics/bv;->d:Lcom/google/android/gms/analytics/bw;

    iget-boolean v4, v3, Lcom/google/android/gms/analytics/bw;->c:Z

    iput-boolean v5, v3, Lcom/google/android/gms/analytics/bw;->c:Z

    if-eqz v4, :cond_5

    .line 310
    const-string v3, "&sc"

    const-string v4, "start"

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    :cond_5
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    .line 314
    const-string v0, "screenview"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "pageview"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "appview"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 318
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    const-string v4, "&a"

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 319
    add-int/lit8 v0, v0, 0x1

    .line 320
    const v4, 0x7fffffff

    if-lt v0, v4, :cond_7

    move v0, v1

    .line 321
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/analytics/bv;->h:Ljava/util/Map;

    const-string v4, "&a"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    :cond_8
    const-string v0, "transaction"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    const-string v0, "item"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->i:Lcom/google/android/gms/analytics/bq;

    invoke-interface {v0}, Lcom/google/android/gms/analytics/bq;->a()Z

    move-result v0

    if-nez v0, :cond_9

    .line 328
    const-string v0, "Too many hits sent too quickly, rate limiting invoked."

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->d(Ljava/lang/String;)V

    .line 332
    :goto_1
    return-void

    .line 330
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/analytics/bv;->g:Lcom/google/android/gms/analytics/by;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/by;->a(Ljava/util/Map;)V

    goto :goto_1
.end method
