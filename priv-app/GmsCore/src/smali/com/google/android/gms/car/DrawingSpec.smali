.class public Lcom/google/android/gms/car/DrawingSpec;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:Landroid/view/Surface;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/gms/car/iw;

    invoke-direct {v0}, Lcom/google/android/gms/car/iw;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/DrawingSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIIILandroid/view/Surface;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/gms/car/DrawingSpec;->a:I

    .line 50
    iput p2, p0, Lcom/google/android/gms/car/DrawingSpec;->b:I

    .line 51
    iput p3, p0, Lcom/google/android/gms/car/DrawingSpec;->c:I

    .line 52
    iput p4, p0, Lcom/google/android/gms/car/DrawingSpec;->d:I

    .line 53
    iput-object p5, p0, Lcom/google/android/gms/car/DrawingSpec;->e:Landroid/view/Surface;

    .line 54
    return-void
.end method

.method public constructor <init>(IIILandroid/view/Surface;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/DrawingSpec;->a:I

    .line 36
    iput p1, p0, Lcom/google/android/gms/car/DrawingSpec;->b:I

    .line 37
    iput p2, p0, Lcom/google/android/gms/car/DrawingSpec;->c:I

    .line 38
    iput p3, p0, Lcom/google/android/gms/car/DrawingSpec;->d:I

    .line 39
    iput-object p4, p0, Lcom/google/android/gms/car/DrawingSpec;->e:Landroid/view/Surface;

    .line 40
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/gms/car/DrawingSpec;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 63
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/iw;->a(Lcom/google/android/gms/car/DrawingSpec;Landroid/os/Parcel;I)V

    .line 64
    return-void
.end method
