.class public final Lcom/google/android/gms/smart_profile/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/ak;->b:Ljava/lang/String;

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/ak;->c:Ljava/lang/String;

    .line 46
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/ak;->d:Ljava/lang/String;

    .line 47
    iput-object p4, p0, Lcom/google/android/gms/smart_profile/ak;->a:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;
    .locals 2

    .prologue
    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 173
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ak;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 174
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    return-object v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/gms/smart_profile/b;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, p2, v1}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 134
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 142
    return-void
.end method

.method final a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ak;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 148
    const-string v0, "SmartProfile"

    const-string v1, "Dropping anonymous smart profile analytics event"

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    :goto_0
    return-void

    .line 151
    :cond_0
    if-nez p2, :cond_1

    .line 152
    const-string v0, "SmartProfile"

    const-string v1, "Dropping smart profile analytics event, no action"

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :cond_1
    if-nez p3, :cond_2

    .line 156
    const-string v0, "SmartProfile"

    const-string v1, "Dropping smart profile analytics event, no startView"

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 159
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/server/y;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ak;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ak;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ak;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    .line 165
    if-eqz p4, :cond_3

    .line 166
    invoke-virtual {v0, p4}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)Lcom/google/android/gms/common/server/y;

    .line 168
    :cond_3
    invoke-static {p1, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    goto :goto_0
.end method
