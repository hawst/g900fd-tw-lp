.class final Lcom/google/android/gms/wallet/common/ui/bx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/FormEditText;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V
    .locals 0

    .prologue
    .line 525
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    .prologue
    .line 543
    return-void
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 539
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 528
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lcom/google/android/gms/wallet/common/ui/validator/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Landroid/widget/TextView;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lcom/google/android/gms/wallet/common/ui/validator/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/validator/d;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 530
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lcom/google/android/gms/wallet/common/ui/validator/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 535
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bx;->a:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    goto :goto_0
.end method
