.class final Lcom/google/android/gms/wearable/node/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/node/bb;

.field private final b:Ljava/io/InputStream;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/wearable/node/be;

.field private final e:Lcom/google/android/gms/wearable/node/bi;

.field private final f:Lcom/google/android/gms/wearable/node/bj;

.field private final g:Lcom/google/android/gms/wearable/node/u;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wearable/node/bb;Ljava/io/InputStream;Lcom/google/android/gms/wearable/c/i;Lcom/google/android/gms/wearable/node/be;)V
    .locals 2

    .prologue
    .line 526
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/bf;->a:Lcom/google/android/gms/wearable/node/bb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 517
    invoke-static {}, Lcom/google/android/gms/wearable/node/bh;->a()Lcom/google/android/gms/wearable/node/bi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bf;->e:Lcom/google/android/gms/wearable/node/bi;

    .line 520
    new-instance v0, Lcom/google/android/gms/wearable/node/bj;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/node/bj;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bf;->f:Lcom/google/android/gms/wearable/node/bj;

    .line 527
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/bf;->b:Ljava/io/InputStream;

    .line 528
    iget-object v0, p3, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bf;->c:Ljava/lang/String;

    .line 529
    iget v0, p3, Lcom/google/android/gms/wearable/c/i;->d:I

    iget-object v1, p1, Lcom/google/android/gms/wearable/node/bb;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wearable/node/v;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wearable/node/v;-><init>(Ljava/lang/String;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bf;->g:Lcom/google/android/gms/wearable/node/u;

    .line 531
    iput-object p4, p0, Lcom/google/android/gms/wearable/node/bf;->d:Lcom/google/android/gms/wearable/node/be;

    .line 532
    return-void

    .line 529
    :cond_0
    new-instance v0, Lcom/google/android/gms/wearable/node/w;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wearable/node/w;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a()Ljava/lang/Void;
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 536
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "WearableReader"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 537
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/node/ax;

    .line 538
    new-instance v3, Lcom/google/android/gms/wearable/c/n;

    invoke-direct {v3}, Lcom/google/android/gms/wearable/c/n;-><init>()V

    .line 540
    :cond_0
    :goto_0
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/ax;->a()Z

    move-result v1

    if-nez v1, :cond_d

    .line 541
    const-string v1, "wearable"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "WearableVerbose"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 543
    :cond_1
    const-string v1, "wearable"

    const-string v4, "reading from peer"

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 547
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/gms/wearable/c/n;->a()Lcom/google/android/gms/wearable/c/n;

    .line 548
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bf;->e:Lcom/google/android/gms/wearable/node/bi;

    iget-object v4, p0, Lcom/google/android/gms/wearable/node/bf;->b:Ljava/io/InputStream;

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/bf;->d:Lcom/google/android/gms/wearable/node/be;

    invoke-static {v1, v4, v3, v5}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/node/bi;Ljava/io/InputStream;Lcom/google/android/gms/wearable/c/n;Lcom/google/android/gms/wearable/node/be;)I

    .line 549
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bf;->f:Lcom/google/android/gms/wearable/node/bj;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wearable/node/bj;->a(Lcom/google/android/gms/wearable/c/n;)Lcom/google/android/gms/wearable/c/m;

    move-result-object v1

    .line 553
    if-nez v1, :cond_4

    .line 554
    const-string v1, "wearable"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "WearableVerbose"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 556
    :cond_3
    const-string v1, "wearable"

    const-string v4, "incoming message is null"

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 613
    :catch_0
    move-exception v0

    .line 614
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bf;->d:Lcom/google/android/gms/wearable/node/be;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reader threw IOException: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V

    .line 615
    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 617
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bf;->g:Lcom/google/android/gms/wearable/node/u;

    invoke-interface {v1}, Lcom/google/android/gms/wearable/node/u;->a()V

    throw v0

    .line 561
    :cond_4
    :try_start_2
    iget-object v4, p0, Lcom/google/android/gms/wearable/node/bf;->g:Lcom/google/android/gms/wearable/node/u;

    invoke-interface {v4, v1}, Lcom/google/android/gms/wearable/node/u;->a(Lcom/google/android/gms/wearable/c/m;)Lcom/google/android/gms/wearable/node/at;

    move-result-object v4

    .line 565
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bf;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v5, v1, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    monitor-enter v5
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 566
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bf;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/bb;->e:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/gms/wearable/node/bf;->c:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/bd;

    .line 567
    if-eqz v1, :cond_10

    .line 568
    iget-object v1, v1, Lcom/google/android/gms/wearable/node/bd;->b:Lcom/google/android/gms/wearable/node/ab;

    .line 570
    :goto_1
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 574
    if-nez v4, :cond_5

    .line 575
    if-eqz v1, :cond_0

    .line 576
    :try_start_4
    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/ab;->b()V

    goto/16 :goto_0

    .line 570
    :catchall_1
    move-exception v0

    monitor-exit v5

    throw v0

    .line 581
    :cond_5
    iget-object v5, v4, Lcom/google/android/gms/wearable/node/at;->a:Lcom/google/android/gms/wearable/c/m;

    iget-object v5, v5, Lcom/google/android/gms/wearable/c/m;->k:Lcom/google/android/gms/wearable/c/l;

    if-eqz v5, :cond_7

    .line 582
    const-string v1, "wearable"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "WearableVerbose"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 584
    :cond_6
    const-string v1, "wearable"

    const-string v4, "it\'s a heartbeat message"

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 589
    :cond_7
    if-eqz v1, :cond_8

    .line 590
    invoke-virtual {v1}, Lcom/google/android/gms/wearable/node/ab;->b()V

    .line 593
    :cond_8
    const-string v1, "wearable"

    const/4 v5, 0x2

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "WearableVerbose"

    const/4 v5, 0x2

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 595
    :cond_9
    const-string v1, "wearable"

    const-string v5, "notifying message transports"

    invoke-static {v1, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 597
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bf;->a:Lcom/google/android/gms/wearable/node/bb;

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/z;

    .line 598
    iget-object v6, p0, Lcom/google/android/gms/wearable/node/bf;->c:Ljava/lang/String;

    iget-object v7, v4, Lcom/google/android/gms/wearable/node/at;->a:Lcom/google/android/gms/wearable/c/m;

    iget-object v8, v4, Lcom/google/android/gms/wearable/node/at;->b:Lcom/google/android/gms/wearable/node/y;

    invoke-interface {v1, v6, v7, v8}, Lcom/google/android/gms/wearable/node/z;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/c/m;Lcom/google/android/gms/wearable/node/y;)V

    goto :goto_2

    .line 602
    :cond_b
    const-string v1, "wearable"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "WearableVerbose"

    const/4 v4, 0x2

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 604
    :cond_c
    const-string v1, "wearable"

    const-string v4, "reading from peer is done"

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 607
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bf;->d:Lcom/google/android/gms/wearable/node/be;

    const-string v1, "reader was stopped"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V

    .line 608
    const-string v0, "wearable"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_e

    const-string v0, "WearableVerbose"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 610
    :cond_e
    const-string v0, "wearable"

    const-string v1, "WearableReader is finished."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 612
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bf;->g:Lcom/google/android/gms/wearable/node/u;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/node/u;->a()V

    return-object v2

    :cond_10
    move-object v1, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 510
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bf;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
