.class final Lcom/google/android/gms/update/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->g(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/lang/String;

    move-result-object v0

    .line 384
    :goto_0
    const-string v1, "UpdateFromSdCardActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "calling verifyUpdate for path: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 385
    iget-object v1, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->i(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    .line 386
    iget-object v1, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->j(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Lcom/google/android/gms/update/UpdateFromSdCardService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->b(Ljava/lang/String;)V

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->k(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 389
    :cond_0
    return-void

    .line 380
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->h(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/update/aa;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->f(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
