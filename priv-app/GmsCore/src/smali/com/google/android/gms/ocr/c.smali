.class public final Lcom/google/android/gms/ocr/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/gms/ocr/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;

    new-instance v1, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;-><init>(Landroid/content/pm/PackageManager;)V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    sget-object v3, Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;->ALL_CPU_ABIS:Ljava/util/HashSet;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;-><init>(Lcom/google/android/libraries/commerce/ocr/pub/FeaturePrerequisiteCheckerUtil;Ljava/util/HashSet;Ljava/util/HashSet;)V

    invoke-virtual {v0}, Lcom/google/android/libraries/commerce/ocr/pub/CreditCardOcrPrerequisiteChecker;->check()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 33
    :goto_0
    const-string v1, "OcrUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Updating ocr activity enabled="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 34
    const-class v1, Lcom/google/android/gms/ocr/SecuredCreditCardOcrActivity;

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;Z)V

    .line 36
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
