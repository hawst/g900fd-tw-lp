.class final Lcom/google/android/gms/photos/autobackup/v;
.super Lcom/google/android/gms/photos/autobackup/a/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field final synthetic b:Lcom/google/android/gms/photos/autobackup/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/u;Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/v;->b:Lcom/google/android/gms/photos/autobackup/u;

    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/v;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/a/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILjava/util/List;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 33
    if-eqz p1, :cond_0

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/v;->b:Lcom/google/android/gms/photos/autobackup/u;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/autobackup/u;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    const-string v0, "LegacyLoadSettingsOp"

    const-string v1, "Failed trying to deliver failure"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 42
    :cond_0
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    .line 44
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/v;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->b()Z

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(IZLjava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 47
    :catch_1
    move-exception v0

    const-string v0, "LegacyLoadSettingsOp"

    const-string v1, "Failed trying to deliver success"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
