.class public final Lcom/google/android/gms/people/d/o;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/people/d/r;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:I

.field public k:J

.field public l:[Lcom/google/android/gms/people/d/m;

.field public m:J

.field public n:J

.field public o:Lcom/google/android/gms/people/d/p;

.field public p:[I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2096
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2097
    iput-object v4, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    iput v1, p0, Lcom/google/android/gms/people/d/o;->b:I

    iput v1, p0, Lcom/google/android/gms/people/d/o;->c:I

    iput v1, p0, Lcom/google/android/gms/people/d/o;->d:I

    iput v1, p0, Lcom/google/android/gms/people/d/o;->e:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/people/d/o;->f:I

    iput v1, p0, Lcom/google/android/gms/people/d/o;->g:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/people/d/o;->j:I

    iput-wide v2, p0, Lcom/google/android/gms/people/d/o;->k:J

    invoke-static {}, Lcom/google/android/gms/people/d/m;->a()[Lcom/google/android/gms/people/d/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    iput-wide v2, p0, Lcom/google/android/gms/people/d/o;->m:J

    iput-wide v2, p0, Lcom/google/android/gms/people/d/o;->n:J

    iput-object v4, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->p:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/o;->cachedSize:I

    .line 2098
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 8

    .prologue
    const/4 v4, 0x1

    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 2297
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2298
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v2, :cond_0

    .line 2299
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    invoke-static {v4, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2302
    :cond_0
    iget v2, p0, Lcom/google/android/gms/people/d/o;->b:I

    if-eqz v2, :cond_1

    .line 2303
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/gms/people/d/o;->b:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2306
    :cond_1
    iget v2, p0, Lcom/google/android/gms/people/d/o;->c:I

    if-eqz v2, :cond_2

    .line 2307
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/gms/people/d/o;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2310
    :cond_2
    iget v2, p0, Lcom/google/android/gms/people/d/o;->e:I

    if-eqz v2, :cond_3

    .line 2311
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/gms/people/d/o;->e:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2314
    :cond_3
    iget v2, p0, Lcom/google/android/gms/people/d/o;->f:I

    if-eq v2, v4, :cond_4

    .line 2315
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/android/gms/people/d/o;->f:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2318
    :cond_4
    iget v2, p0, Lcom/google/android/gms/people/d/o;->g:I

    if-eqz v2, :cond_5

    .line 2319
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/android/gms/people/d/o;->g:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2322
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 2323
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2326
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 2327
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2330
    :cond_7
    iget v2, p0, Lcom/google/android/gms/people/d/o;->j:I

    if-eqz v2, :cond_8

    .line 2331
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/android/gms/people/d/o;->j:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2334
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->k:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_9

    .line 2335
    const/16 v2, 0xa

    iget-wide v4, p0, Lcom/google/android/gms/people/d/o;->k:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2338
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v0

    move v0, v1

    .line 2339
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    array-length v3, v3

    if-ge v0, v3, :cond_b

    .line 2340
    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    aget-object v3, v3, v0

    .line 2341
    if-eqz v3, :cond_a

    .line 2342
    const/16 v4, 0xb

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2339
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_b
    move v0, v2

    .line 2347
    :cond_c
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->m:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_d

    .line 2348
    const/16 v2, 0xc

    iget-wide v4, p0, Lcom/google/android/gms/people/d/o;->m:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2351
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    if-eqz v2, :cond_e

    .line 2352
    const/16 v2, 0xd

    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2355
    :cond_e
    iget v2, p0, Lcom/google/android/gms/people/d/o;->d:I

    if-eqz v2, :cond_f

    .line 2356
    const/16 v2, 0xe

    iget v3, p0, Lcom/google/android/gms/people/d/o;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2359
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->p:[I

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->p:[I

    array-length v2, v2

    if-lez v2, :cond_11

    move v2, v1

    .line 2361
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->p:[I

    array-length v3, v3

    if-ge v1, v3, :cond_10

    .line 2362
    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->p:[I

    aget v3, v3, v1

    .line 2363
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 2361
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2366
    :cond_10
    add-int/2addr v0, v2

    .line 2367
    iget-object v1, p0, Lcom/google/android/gms/people/d/o;->p:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2369
    :cond_11
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->n:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_12

    .line 2370
    const/16 v1, 0x10

    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->n:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2373
    :cond_12
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2123
    if-ne p1, p0, :cond_1

    .line 2200
    :cond_0
    :goto_0
    return v0

    .line 2126
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/o;

    if-nez v2, :cond_2

    move v0, v1

    .line 2127
    goto :goto_0

    .line 2129
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/o;

    .line 2130
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    if-nez v2, :cond_3

    .line 2131
    iget-object v2, p1, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2132
    goto :goto_0

    .line 2135
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    iget-object v3, p1, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2136
    goto :goto_0

    .line 2139
    :cond_4
    iget v2, p0, Lcom/google/android/gms/people/d/o;->b:I

    iget v3, p1, Lcom/google/android/gms/people/d/o;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 2140
    goto :goto_0

    .line 2142
    :cond_5
    iget v2, p0, Lcom/google/android/gms/people/d/o;->c:I

    iget v3, p1, Lcom/google/android/gms/people/d/o;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2143
    goto :goto_0

    .line 2145
    :cond_6
    iget v2, p0, Lcom/google/android/gms/people/d/o;->d:I

    iget v3, p1, Lcom/google/android/gms/people/d/o;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2146
    goto :goto_0

    .line 2148
    :cond_7
    iget v2, p0, Lcom/google/android/gms/people/d/o;->e:I

    iget v3, p1, Lcom/google/android/gms/people/d/o;->e:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2149
    goto :goto_0

    .line 2151
    :cond_8
    iget v2, p0, Lcom/google/android/gms/people/d/o;->f:I

    iget v3, p1, Lcom/google/android/gms/people/d/o;->f:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 2152
    goto :goto_0

    .line 2154
    :cond_9
    iget v2, p0, Lcom/google/android/gms/people/d/o;->g:I

    iget v3, p1, Lcom/google/android/gms/people/d/o;->g:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 2155
    goto :goto_0

    .line 2157
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 2158
    iget-object v2, p1, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 2159
    goto :goto_0

    .line 2161
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 2162
    goto :goto_0

    .line 2164
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2165
    iget-object v2, p1, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    .line 2166
    goto :goto_0

    .line 2168
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 2169
    goto :goto_0

    .line 2171
    :cond_e
    iget v2, p0, Lcom/google/android/gms/people/d/o;->j:I

    iget v3, p1, Lcom/google/android/gms/people/d/o;->j:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 2172
    goto/16 :goto_0

    .line 2174
    :cond_f
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->k:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/o;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    move v0, v1

    .line 2175
    goto/16 :goto_0

    .line 2177
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    iget-object v3, p1, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 2179
    goto/16 :goto_0

    .line 2181
    :cond_11
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->m:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/o;->m:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    move v0, v1

    .line 2182
    goto/16 :goto_0

    .line 2184
    :cond_12
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->n:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/o;->n:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_13

    move v0, v1

    .line 2185
    goto/16 :goto_0

    .line 2187
    :cond_13
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    if-nez v2, :cond_14

    .line 2188
    iget-object v2, p1, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    if-eqz v2, :cond_15

    move v0, v1

    .line 2189
    goto/16 :goto_0

    .line 2192
    :cond_14
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    iget-object v3, p1, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 2193
    goto/16 :goto_0

    .line 2196
    :cond_15
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->p:[I

    iget-object v3, p1, Lcom/google/android/gms/people/d/o;->p:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2198
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 2205
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2208
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/people/d/o;->b:I

    add-int/2addr v0, v2

    .line 2209
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/people/d/o;->c:I

    add-int/2addr v0, v2

    .line 2210
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/people/d/o;->d:I

    add-int/2addr v0, v2

    .line 2211
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/people/d/o;->e:I

    add-int/2addr v0, v2

    .line 2212
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/people/d/o;->f:I

    add-int/2addr v0, v2

    .line 2213
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/people/d/o;->g:I

    add-int/2addr v0, v2

    .line 2214
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2216
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2218
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/people/d/o;->j:I

    add-int/2addr v0, v2

    .line 2219
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->k:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/o;->k:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 2221
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2223
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->m:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/o;->m:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 2225
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->n:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/o;->n:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 2227
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2229
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/people/d/o;->p:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2231
    return v0

    .line 2205
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/r;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2214
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2216
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2227
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    invoke-virtual {v1}, Lcom/google/android/gms/people/d/p;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1490
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/r;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/o;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/people/d/o;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/o;->e:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_6
    iput v0, p0, Lcom/google/android/gms/people/d/o;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/o;->g:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/o;->j:I

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/people/d/o;->k:J

    goto :goto_0

    :sswitch_c
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/people/d/m;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/gms/people/d/m;

    invoke-direct {v3}, Lcom/google/android/gms/people/d/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/gms/people/d/m;

    invoke-direct {v3}, Lcom/google/android/gms/people/d/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/people/d/o;->m:J

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/people/d/p;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/o;->d:I

    goto/16 :goto_0

    :sswitch_10
    const/16 v0, 0x78

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->p:[I

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/people/d/o;->p:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->p:[I

    array-length v0, v0

    goto :goto_3

    :cond_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/people/d/o;->p:[I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->p:[I

    if-nez v2, :cond_b

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_a

    iget-object v4, p0, Lcom/google/android/gms/people/d/o;->p:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->p:[I

    array-length v2, v2

    goto :goto_6

    :cond_c
    iput-object v0, p0, Lcom/google/android/gms/people/d/o;->p:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/people/d/o;->n:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x50 -> :sswitch_b
        0x5a -> :sswitch_c
        0x60 -> :sswitch_d
        0x6a -> :sswitch_e
        0x70 -> :sswitch_f
        0x78 -> :sswitch_10
        0x7a -> :sswitch_11
        0x80 -> :sswitch_12
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_6
        0x2 -> :sswitch_6
        0x3 -> :sswitch_6
        0x4 -> :sswitch_6
        0x5 -> :sswitch_6
        0x6 -> :sswitch_6
        0x64 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const-wide/16 v4, 0x0

    .line 2237
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v0, :cond_0

    .line 2238
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2240
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/d/o;->b:I

    if-eqz v0, :cond_1

    .line 2241
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/gms/people/d/o;->b:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2243
    :cond_1
    iget v0, p0, Lcom/google/android/gms/people/d/o;->c:I

    if-eqz v0, :cond_2

    .line 2244
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/android/gms/people/d/o;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2246
    :cond_2
    iget v0, p0, Lcom/google/android/gms/people/d/o;->e:I

    if-eqz v0, :cond_3

    .line 2247
    const/4 v0, 0x4

    iget v2, p0, Lcom/google/android/gms/people/d/o;->e:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2249
    :cond_3
    iget v0, p0, Lcom/google/android/gms/people/d/o;->f:I

    if-eq v0, v3, :cond_4

    .line 2250
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/android/gms/people/d/o;->f:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2252
    :cond_4
    iget v0, p0, Lcom/google/android/gms/people/d/o;->g:I

    if-eqz v0, :cond_5

    .line 2253
    const/4 v0, 0x6

    iget v2, p0, Lcom/google/android/gms/people/d/o;->g:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2255
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2256
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2258
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 2259
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2261
    :cond_7
    iget v0, p0, Lcom/google/android/gms/people/d/o;->j:I

    if-eqz v0, :cond_8

    .line 2262
    const/16 v0, 0x9

    iget v2, p0, Lcom/google/android/gms/people/d/o;->j:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2264
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->k:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_9

    .line 2265
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2267
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    array-length v0, v0

    if-lez v0, :cond_b

    move v0, v1

    .line 2268
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    array-length v2, v2

    if-ge v0, v2, :cond_b

    .line 2269
    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->l:[Lcom/google/android/gms/people/d/m;

    aget-object v2, v2, v0

    .line 2270
    if-eqz v2, :cond_a

    .line 2271
    const/16 v3, 0xb

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2268
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2275
    :cond_b
    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->m:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_c

    .line 2276
    const/16 v0, 0xc

    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->m:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2278
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    if-eqz v0, :cond_d

    .line 2279
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->o:Lcom/google/android/gms/people/d/p;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2281
    :cond_d
    iget v0, p0, Lcom/google/android/gms/people/d/o;->d:I

    if-eqz v0, :cond_e

    .line 2282
    const/16 v0, 0xe

    iget v2, p0, Lcom/google/android/gms/people/d/o;->d:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2284
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->p:[I

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->p:[I

    array-length v0, v0

    if-lez v0, :cond_f

    .line 2285
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/o;->p:[I

    array-length v0, v0

    if-ge v1, v0, :cond_f

    .line 2286
    const/16 v0, 0xf

    iget-object v2, p0, Lcom/google/android/gms/people/d/o;->p:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2285
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2289
    :cond_f
    iget-wide v0, p0, Lcom/google/android/gms/people/d/o;->n:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_10

    .line 2290
    const/16 v0, 0x10

    iget-wide v2, p0, Lcom/google/android/gms/people/d/o;->n:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2292
    :cond_10
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2293
    return-void
.end method
