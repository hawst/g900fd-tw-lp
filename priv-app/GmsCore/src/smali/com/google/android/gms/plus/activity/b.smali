.class final Lcom/google/android/gms/plus/activity/b;
.super Lcom/google/android/gms/plus/activity/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    .prologue
    .line 537
    iput-object p1, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/k;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V
    .locals 0

    .prologue
    .line 537
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/b;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 545
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const-class v2, Lcom/google/android/gms/plus/oob/PlusActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 546
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.OVERRIDE_THEME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 547
    const-string v1, "com.google.android.gms.plus.OVERRIDE_THEME"

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.plus.OVERRIDE_THEME"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 550
    :cond_0
    const-string v1, "com.google.android.gms.common.oob.EXTRA_BACK_BUTTON_NAME"

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->f(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 551
    const-string v1, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 552
    const-string v1, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 553
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->g(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/content/Intent;)V

    .line 554
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v2, 0x2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 557
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/common/analytics/m;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/b;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 561
    return-void
.end method
