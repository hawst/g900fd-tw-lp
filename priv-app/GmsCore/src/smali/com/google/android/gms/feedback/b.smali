.class public final Lcom/google/android/gms/feedback/b;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private b:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 0

    .prologue
    .line 142
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 143
    iput-object p1, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    .line 144
    invoke-direct {p0, p2}, Lcom/google/android/gms/feedback/b;->a(Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 145
    return-void
.end method

.method private a(D)Ljava/lang/String;
    .locals 13

    .prologue
    const/4 v11, 0x3

    const v4, 0x15180

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 469
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 470
    const-wide v2, 0x408f400000000000L    # 1000.0

    div-double v2, p1, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    .line 473
    if-le v2, v4, :cond_5

    .line 474
    div-int v0, v2, v4

    .line 475
    mul-int v3, v0, v4

    sub-int/2addr v2, v3

    move v4, v0

    .line 477
    :goto_0
    const/16 v0, 0xe10

    if-le v2, v0, :cond_4

    .line 478
    div-int/lit16 v0, v2, 0xe10

    .line 479
    mul-int/lit16 v3, v0, 0xe10

    sub-int/2addr v2, v3

    move v3, v0

    .line 481
    :goto_1
    const/16 v0, 0x3c

    if-le v2, v0, :cond_3

    .line 482
    div-int/lit8 v0, v2, 0x3c

    .line 483
    mul-int/lit8 v6, v0, 0x3c

    sub-int/2addr v2, v6

    .line 485
    :goto_2
    if-lez v4, :cond_0

    .line 486
    iget-object v6, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    sget v7, Lcom/google/android/gms/p;->mL:I

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v8, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v8, v9

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v8, v11

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 496
    :goto_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 488
    :cond_0
    if-lez v3, :cond_1

    .line 489
    iget-object v4, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    sget v6, Lcom/google/android/gms/p;->mM:I

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v7, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v10

    invoke-virtual {v4, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 491
    :cond_1
    if-lez v0, :cond_2

    .line 492
    iget-object v3, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->mN:I

    new-array v6, v10, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-virtual {v3, v4, v6}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    .line 494
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->mO:I

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-virtual {v0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v3, v1

    goto/16 :goto_1

    :cond_5
    move v4, v1

    goto/16 :goto_0
.end method

.method private static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 503
    const-class v1, Landroid/telephony/TelephonyManager;

    .line 504
    invoke-virtual {v1}, Ljava/lang/Class;->getDeclaredFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 506
    :try_start_0
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 508
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 516
    :cond_0
    :goto_1
    return-object v0

    .line 511
    :catch_0
    move-exception v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 513
    :catch_1
    move-exception v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 504
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a(ILjava/lang/Object;I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 428
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 429
    return-void
.end method

.method private a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 439
    iget-object v7, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/feedback/c;

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p4

    move v5, p3

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/feedback/c;-><init>(Lcom/google/android/gms/feedback/b;ILjava/lang/Object;Ljava/lang/Class;ILjava/lang/String;)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 441
    return-void
.end method

.method private a(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 13
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 288
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    .line 290
    sget v0, Lcom/google/android/gms/p;->nH:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v1, v1, Landroid/app/ApplicationErrorReport;->type:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 292
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 293
    sget v0, Lcom/google/android/gms/p;->ni:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 295
    :cond_0
    sget v0, Lcom/google/android/gms/p;->nt:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 296
    sget v0, Lcom/google/android/gms/p;->nu:I

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->d:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 297
    sget v0, Lcom/google/android/gms/p;->nv:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->e:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 298
    sget v0, Lcom/google/android/gms/p;->nr:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 300
    sget v0, Lcom/google/android/gms/p;->nw:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 301
    sget v0, Lcom/google/android/gms/p;->nG:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-wide v4, v1, Landroid/app/ApplicationErrorReport;->time:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 302
    sget v0, Lcom/google/android/gms/p;->nF:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-boolean v1, v1, Landroid/app/ApplicationErrorReport;->systemApp:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 303
    sget v0, Lcom/google/android/gms/p;->nO:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->D:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 305
    sget v0, Lcom/google/android/gms/p;->nE:I

    invoke-direct {p0, v0, v6, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 306
    sget v0, Lcom/google/android/gms/p;->nj:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->f:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 307
    sget v0, Lcom/google/android/gms/p;->ne:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->g:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 308
    sget v0, Lcom/google/android/gms/p;->nf:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->h:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 309
    sget v0, Lcom/google/android/gms/p;->nd:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->k:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 310
    sget v0, Lcom/google/android/gms/p;->ns:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->i:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 311
    sget v0, Lcom/google/android/gms/p;->nx:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->j:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 312
    sget v0, Lcom/google/android/gms/p;->nD:I

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->l:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 313
    sget v0, Lcom/google/android/gms/p;->ny:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->m:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 314
    sget v0, Lcom/google/android/gms/p;->nq:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->n:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 315
    sget v0, Lcom/google/android/gms/p;->ng:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->o:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 316
    sget v0, Lcom/google/android/gms/p;->nb:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 317
    sget v0, Lcom/google/android/gms/p;->nc:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->q:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 319
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 320
    sget v0, Lcom/google/android/gms/p;->ol:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 323
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_2

    .line 324
    sget v1, Lcom/google/android/gms/p;->nz:I

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "running applications"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 327
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 328
    sget v1, Lcom/google/android/gms/p;->of:I

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "system logs"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 331
    :cond_3
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 332
    sget v1, Lcom/google/android/gms/p;->nJ:I

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "event logs"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 335
    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    if-eqz v0, :cond_5

    .line 336
    sget v0, Lcom/google/android/gms/p;->nh:I

    invoke-direct {p0, v0, v6, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 337
    sget v0, Lcom/google/android/gms/p;->nk:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 339
    sget v0, Lcom/google/android/gms/p;->nn:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 341
    sget v0, Lcom/google/android/gms/p;->nm:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 343
    sget v0, Lcom/google/android/gms/p;->np:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 345
    sget v0, Lcom/google/android/gms/p;->no:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 347
    sget v1, Lcom/google/android/gms/p;->nl:I

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "stack trace"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 352
    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    if-eqz v0, :cond_7

    .line 353
    sget v0, Lcom/google/android/gms/p;->mR:I

    invoke-direct {p0, v0, v6, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 354
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 355
    sget v0, Lcom/google/android/gms/p;->mS:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 358
    :cond_6
    sget v0, Lcom/google/android/gms/p;->mT:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$AnrInfo;->cause:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 359
    sget v1, Lcom/google/android/gms/p;->mU:I

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->info:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "anr info"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 361
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 362
    sget v1, Lcom/google/android/gms/p;->mV:I

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "anr stack trace"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 367
    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    if-eqz v0, :cond_8

    .line 368
    sget v0, Lcom/google/android/gms/p;->mW:I

    invoke-direct {p0, v0, v6, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 369
    sget v0, Lcom/google/android/gms/p;->na:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget v1, v1, Landroid/app/ApplicationErrorReport$BatteryInfo;->usagePercent:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 371
    sget v0, Lcom/google/android/gms/p;->mZ:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-wide v4, v1, Landroid/app/ApplicationErrorReport$BatteryInfo;->durationMicros:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 373
    sget v1, Lcom/google/android/gms/p;->mY:I

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->usageDetails:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "battery usage details"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 376
    sget v1, Lcom/google/android/gms/p;->mX:I

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->checkinDetails:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "battery checkin details"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 381
    :cond_8
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    if-eqz v0, :cond_9

    .line 382
    sget v0, Lcom/google/android/gms/p;->nA:I

    invoke-direct {p0, v0, v6, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 383
    sget v0, Lcom/google/android/gms/p;->nC:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-wide v4, v1, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->durationMillis:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x4

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 386
    sget v1, Lcom/google/android/gms/p;->nB:I

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-object v2, v0, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    const-class v4, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v5, "running service details"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;ILjava/lang/Class;Ljava/lang/String;)V

    .line 391
    :cond_9
    sget v0, Lcom/google/android/gms/p;->nP:I

    invoke-direct {p0, v0, v6, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 392
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_a

    .line 393
    sget v0, Lcom/google/android/gms/p;->nS:I

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 395
    :cond_a
    sget v0, Lcom/google/android/gms/p;->nX:I

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->z:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x5

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 396
    sget v0, Lcom/google/android/gms/p;->nT:I

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->A:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x6

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 397
    sget v0, Lcom/google/android/gms/p;->nQ:I

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->G:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 398
    sget v0, Lcom/google/android/gms/p;->nR:I

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->H:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 399
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-eqz v0, :cond_b

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-gtz v0, :cond_c

    :cond_b
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    .line 401
    :cond_c
    sget v0, Lcom/google/android/gms/p;->ob:I

    invoke-direct {p0, v0, v6, v3}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/Object;I)V

    .line 402
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-eqz v0, :cond_d

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_d

    .line 403
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 404
    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    new-instance v5, Lcom/google/android/gms/feedback/c;

    invoke-direct {v5, p0, v0, v2}, Lcom/google/android/gms/feedback/c;-><init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 407
    :cond_d
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    if-eqz v0, :cond_e

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_e

    .line 408
    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v2, v1

    move v0, v3

    :goto_1
    if-ge v0, v2, :cond_e

    aget-object v6, v1, v0

    .line 409
    sget v5, Lcom/google/android/gms/p;->ob:I

    const-class v9, Lcom/google/android/gms/feedback/ShowTextActivity;

    const-string v11, "product specific binary data details"

    iget-object v12, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    new-instance v4, Lcom/google/android/gms/feedback/c;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v5, p0

    move-object v8, v6

    move v10, v3

    invoke-direct/range {v4 .. v11}, Lcom/google/android/gms/feedback/c;-><init>(Lcom/google/android/gms/feedback/b;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Object;Ljava/lang/Class;ILjava/lang/String;)V

    invoke-interface {v12, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 408
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 414
    :cond_e
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 451
    iget-object v0, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/c;

    .line 453
    invoke-virtual {v0}, Lcom/google/android/gms/feedback/c;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 462
    :goto_0
    return-void

    .line 457
    :cond_0
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    iget-object v3, v0, Lcom/google/android/gms/feedback/c;->b:Ljava/lang/Class;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 458
    const-string v2, "feedback.FIELD_NAME"

    iget-object v3, v0, Lcom/google/android/gms/feedback/c;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 459
    const-string v2, "feedback.FIELD_VALUE"

    iget-object v3, v0, Lcom/google/android/gms/feedback/c;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 460
    const-string v2, "feedback.OBJECT_VALUE"

    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 461
    iget-object v0, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 163
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/c;

    .line 186
    iget-object v1, p0, Lcom/google/android/gms/feedback/b;->b:Landroid/content/Context;

    const-string v4, "layout_inflater"

    invoke-virtual {v1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 191
    iget-object v4, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    if-nez v4, :cond_1

    move v4, v2

    :goto_0
    if-eqz v4, :cond_2

    .line 192
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v2

    sget v4, Lcom/google/android/gms/j;->qX:I

    if-eq v2, v4, :cond_b

    .line 193
    :cond_0
    sget v2, Lcom/google/android/gms/l;->fl:I

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    :goto_1
    move-object v1, v2

    .line 195
    check-cast v1, Landroid/widget/TextView;

    .line 196
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 280
    :goto_2
    return-object v2

    :cond_1
    move v4, v3

    .line 191
    goto :goto_0

    .line 201
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/feedback/c;->a()Z

    move-result v4

    if-eqz v4, :cond_7

    .line 202
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v4

    sget v5, Lcom/google/android/gms/j;->fP:I

    if-eq v4, v5, :cond_4

    .line 203
    :cond_3
    sget v4, Lcom/google/android/gms/l;->at:I

    invoke-virtual {v1, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 211
    :cond_4
    :goto_3
    iget v1, v0, Lcom/google/android/gms/feedback/c;->c:I

    const/4 v4, 0x7

    if-ne v1, v4, :cond_9

    move v1, v2

    :goto_4
    if-nez v1, :cond_5

    .line 212
    sget v1, Lcom/google/android/gms/j;->kS:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 213
    iget-object v2, v0, Lcom/google/android/gms/feedback/c;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 214
    iget-object v2, v0, Lcom/google/android/gms/feedback/c;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 222
    :cond_5
    :goto_5
    iget-object v1, v0, Lcom/google/android/gms/feedback/c;->b:Ljava/lang/Class;

    if-nez v1, :cond_6

    .line 223
    sget v1, Lcom/google/android/gms/j;->tM:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 224
    const v2, 0x800003

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 225
    iget v2, v0, Lcom/google/android/gms/feedback/c;->c:I

    packed-switch v2, :pswitch_data_0

    :cond_6
    :goto_6
    move-object v2, p2

    .line 280
    goto :goto_2

    .line 206
    :cond_7
    if-eqz p2, :cond_8

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v4

    sget v5, Lcom/google/android/gms/j;->kT:I

    if-eq v4, v5, :cond_4

    .line 207
    :cond_8
    sget v4, Lcom/google/android/gms/l;->cw:I

    invoke-virtual {v1, v4, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_3

    :cond_9
    move v1, v3

    .line 211
    goto :goto_4

    .line 216
    :cond_a
    iget-object v2, v0, Lcom/google/android/gms/feedback/c;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_5

    .line 227
    :pswitch_0
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 231
    :pswitch_1
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 232
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v4, v5}, Ljava/util/Date;-><init>(J)V

    .line 233
    invoke-static {v3}, Ljava/text/DateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v2

    .line 234
    invoke-virtual {v2, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 238
    :pswitch_2
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 239
    packed-switch v0, :pswitch_data_1

    :pswitch_3
    goto :goto_6

    .line 244
    :pswitch_4
    sget v0, Lcom/google/android/gms/p;->nh:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 241
    :pswitch_5
    sget v0, Lcom/google/android/gms/p;->mR:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 247
    :pswitch_6
    sget v0, Lcom/google/android/gms/p;->mW:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 250
    :pswitch_7
    sget v0, Lcom/google/android/gms/p;->nA:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 253
    :pswitch_8
    sget v0, Lcom/google/android/gms/p;->nI:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_6

    .line 259
    :pswitch_9
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 260
    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-double v2, v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/feedback/b;->a(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_6

    .line 264
    :pswitch_a
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 265
    long-to-double v2, v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/feedback/b;->a(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 269
    :pswitch_b
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 270
    const-string v2, "NETWORK_TYPE_"

    invoke-static {v0, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    .line 274
    :pswitch_c
    iget-object v0, v0, Lcom/google/android/gms/feedback/c;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 275
    const-string v2, "PHONE_TYPE_"

    invoke-static {v0, v2}, Lcom/google/android/gms/feedback/b;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_6

    :cond_b
    move-object v2, p2

    goto/16 :goto_1

    .line 225
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
    .end packed-switch

    .line 239
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_7
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 168
    const/4 v0, 0x0

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/gms/feedback/b;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/c;

    .line 179
    invoke-virtual {v0}, Lcom/google/android/gms/feedback/c;->a()Z

    move-result v0

    return v0
.end method
