.class public final enum Lcom/google/android/gms/drive/auth/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/auth/e;

.field public static final enum b:Lcom/google/android/gms/drive/auth/e;

.field public static final enum c:Lcom/google/android/gms/drive/auth/e;

.field public static final enum d:Lcom/google/android/gms/drive/auth/e;

.field private static final synthetic f:[Lcom/google/android/gms/drive/auth/e;


# instance fields
.field private final e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 68
    new-instance v0, Lcom/google/android/gms/drive/auth/e;

    const-string v1, "OK"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/auth/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/drive/auth/e;->a:Lcom/google/android/gms/drive/auth/e;

    .line 75
    new-instance v0, Lcom/google/android/gms/drive/auth/e;

    const-string v1, "OK_EXPIRED"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/gms/drive/auth/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/drive/auth/e;->b:Lcom/google/android/gms/drive/auth/e;

    .line 80
    new-instance v0, Lcom/google/android/gms/drive/auth/e;

    const-string v1, "FAIL_USER_CONSENT_REQUIRED"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/auth/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/drive/auth/e;->c:Lcom/google/android/gms/drive/auth/e;

    .line 85
    new-instance v0, Lcom/google/android/gms/drive/auth/e;

    const-string v1, "FAIL_PERMANENT"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/drive/auth/e;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    .line 64
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/auth/e;

    sget-object v1, Lcom/google/android/gms/drive/auth/e;->a:Lcom/google/android/gms/drive/auth/e;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/auth/e;->b:Lcom/google/android/gms/drive/auth/e;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/auth/e;->c:Lcom/google/android/gms/drive/auth/e;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/auth/e;->d:Lcom/google/android/gms/drive/auth/e;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/drive/auth/e;->f:[Lcom/google/android/gms/drive/auth/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 90
    iput-boolean p3, p0, Lcom/google/android/gms/drive/auth/e;->e:Z

    .line 91
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/auth/e;
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/google/android/gms/drive/auth/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/auth/e;
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/gms/drive/auth/e;->f:[Lcom/google/android/gms/drive/auth/e;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/auth/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/auth/e;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/gms/drive/auth/e;->e:Z

    return v0
.end method
