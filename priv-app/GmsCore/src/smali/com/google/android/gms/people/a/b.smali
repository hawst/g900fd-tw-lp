.class public final Lcom/google/android/gms/people/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final h:Ljava/util/Map;

.field private static final i:Ljava/util/Map;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/gms/common/a/d;

.field public final c:Lcom/google/android/gms/common/a/d;

.field public final d:Lcom/google/android/gms/common/a/d;

.field public final e:Lcom/google/android/gms/common/a/d;

.field public final f:Lcom/google/android/gms/common/a/d;

.field public final g:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/a/b;->h:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/a/b;->i:Ljava/util/Map;

    .line 72
    const-string v0, "get"

    sget-object v1, Lcom/google/android/gms/people/a/b;->h:Ljava/util/Map;

    const-string v2, "people/${gaia_id}?includeProfileWithState=disabled&onBehalfOf=${on_behalf_of}"

    const/4 v3, 0x2

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/people/a/b;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;

    .line 75
    const-string v0, "list"

    sget-object v1, Lcom/google/android/gms/people/a/b;->i:Ljava/util/Map;

    const-string v2, "people/me/people/all?onBehalfOf=${on_behalf_of}"

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/people/a/b;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;

    .line 78
    const-string v0, "list_by_email"

    sget-object v1, Lcom/google/android/gms/people/a/b;->i:Ljava/util/Map;

    const-string v2, "people/lookup?id=${email}&onBehalfOf=${on_behalf_of}&type=email&matchType=lenient"

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/people/a/b;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;

    .line 81
    const-string v0, "list_by_phone"

    sget-object v1, Lcom/google/android/gms/people/a/b;->i:Ljava/util/Map;

    const-string v2, "people/lookup?id=${phone}&onBehalfOf=${on_behalf_of}&type=phone&matchType=lenient"

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/people/a/b;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;

    .line 83
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "gms.people.endpoint."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 88
    iput-object v0, p0, Lcom/google/android/gms/people/a/b;->a:Ljava/lang/String;

    .line 89
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".server_url"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "https://www.googleapis.com"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/a/b;->b:Lcom/google/android/gms/common/a/d;

    .line 90
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".server_api_path"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "/plus/v2whitelisted/"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/a/b;->c:Lcom/google/android/gms/common/a/d;

    .line 91
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".server_method"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/a/b;->d:Lcom/google/android/gms/common/a/d;

    .line 92
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".backend_override"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    invoke-static {v1, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/a/b;->e:Lcom/google/android/gms/common/a/d;

    .line 93
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".format"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/a/b;->f:Lcom/google/android/gms/common/a/d;

    .line 94
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".batching"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/a/b;->g:Lcom/google/android/gms/common/a/d;

    .line 95
    return-void
.end method

.method public static declared-synchronized a(Ljava/lang/String;)Lcom/google/android/gms/people/a/b;
    .locals 4

    .prologue
    .line 116
    const-class v1, Lcom/google/android/gms/people/a/b;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "get"

    .line 117
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/a/b;->h:Ljava/util/Map;

    const-string v2, "people/${gaia_id}?includeProfileWithState=disabled&onBehalfOf=${on_behalf_of}"

    const/4 v3, 0x2

    invoke-static {p0, v0, v2, v3}, Lcom/google/android/gms/people/a/b;->b(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 116
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;
    .locals 2

    .prologue
    .line 99
    const-class v1, Lcom/google/android/gms/people/a/b;

    monitor-enter v1

    :try_start_0
    new-instance v0, Lcom/google/android/gms/people/a/b;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/gms/people/a/b;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    .line 101
    invoke-interface {p1, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    monitor-exit v1

    return-object v0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized b(Ljava/lang/String;)Lcom/google/android/gms/people/a/b;
    .locals 4

    .prologue
    .line 122
    const-class v1, Lcom/google/android/gms/people/a/b;

    monitor-enter v1

    :try_start_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p0, "list"

    .line 123
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/a/b;->i:Ljava/util/Map;

    const-string v2, "people/me/people/all?onBehalfOf=${on_behalf_of}"

    const/4 v3, 0x4

    invoke-static {p0, v0, v2, v3}, Lcom/google/android/gms/people/a/b;->b(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static declared-synchronized b(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;
    .locals 2

    .prologue
    .line 107
    const-class v1, Lcom/google/android/gms/people/a/b;

    monitor-enter v1

    :try_start_0
    invoke-interface {p1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/a/b;

    .line 108
    if-nez v0, :cond_0

    .line 109
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/people/a/b;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;I)Lcom/google/android/gms/people/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 111
    :cond_0
    monitor-exit v1

    return-object v0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/people/a/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/people/a/b;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/people/a/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
