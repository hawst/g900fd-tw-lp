.class public final Lcom/google/android/gms/droidguard/e/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/gms/droidguard/e/d;->a:Landroid/content/Context;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;[B)[B
    .locals 5

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/droidguard/e/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/p;->a(Landroid/content/Context;)Lcom/google/android/gms/http/GoogleHttpClient;

    move-result-object v1

    .line 37
    :try_start_0
    new-instance v0, Lorg/apache/http/client/methods/HttpPost;

    invoke-direct {v0, p1}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 38
    new-instance v2, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v2, p2}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v0, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 39
    invoke-virtual {v1, v0}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 40
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v2

    invoke-interface {v2}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v2

    .line 42
    const/16 v3, 0xc8

    if-eq v2, v3, :cond_0

    .line 43
    new-instance v0, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received status code ["

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] instead of [200]."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    throw v0

    .line 47
    :cond_0
    :try_start_1
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 48
    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 49
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 51
    const/4 v3, 0x0

    :try_start_2
    invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 53
    :try_start_3
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 54
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    .line 56
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 58
    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    return-object v0

    .line 53
    :catchall_1
    move-exception v3

    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 54
    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->close()V

    throw v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method
