.class final Lcom/google/android/gms/fitness/sensors/sample/a;
.super Lcom/google/android/gms/fitness/f/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

.field final synthetic c:Lcom/google/android/gms/fitness/sensors/sample/c;

.field final synthetic d:I

.field final synthetic e:Lcom/google/android/gms/fitness/sensors/sample/CollectSensorService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/sample/CollectSensorService;Landroid/content/Intent;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Lcom/google/android/gms/fitness/sensors/sample/c;I)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->e:Lcom/google/android/gms/fitness/sensors/sample/CollectSensorService;

    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->a:Landroid/content/Intent;

    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->b:Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    iput-object p4, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->c:Lcom/google/android/gms/fitness/sensors/sample/c;

    iput p5, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->d:I

    invoke-direct {p0}, Lcom/google/android/gms/fitness/f/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/fitness/sensors/sample/CollectSensorReceiver;->a(Landroid/content/Intent;)Z

    .line 66
    return-void
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 70
    const-string v0, "Unable to register for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->b:Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->c:Lcom/google/android/gms/fitness/sensors/sample/c;

    iget v1, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/sample/c;->b(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/a;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/fitness/sensors/sample/CollectSensorReceiver;->a(Landroid/content/Intent;)Z

    .line 73
    return-void
.end method
