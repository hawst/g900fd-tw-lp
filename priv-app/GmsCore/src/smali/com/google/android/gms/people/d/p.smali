.class public final Lcom/google/android/gms/people/d/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J

.field public d:J

.field public e:Z

.field public f:I

.field public g:I

.field public h:I

.field public i:I

.field public j:Lcom/google/android/gms/people/d/q;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 1790
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1791
    iput-wide v2, p0, Lcom/google/android/gms/people/d/p;->a:J

    iput-wide v2, p0, Lcom/google/android/gms/people/d/p;->b:J

    iput-wide v2, p0, Lcom/google/android/gms/people/d/p;->c:J

    iput-wide v2, p0, Lcom/google/android/gms/people/d/p;->d:J

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/p;->e:Z

    iput v0, p0, Lcom/google/android/gms/people/d/p;->f:I

    iput v0, p0, Lcom/google/android/gms/people/d/p;->g:I

    iput v0, p0, Lcom/google/android/gms/people/d/p;->h:I

    iput v0, p0, Lcom/google/android/gms/people/d/p;->i:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/p;->cachedSize:I

    .line 1792
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1916
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1917
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 1918
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1921
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 1922
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1925
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 1926
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1929
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/p;->e:Z

    if-eqz v1, :cond_3

    .line 1930
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/p;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1933
    :cond_3
    iget v1, p0, Lcom/google/android/gms/people/d/p;->f:I

    if-eqz v1, :cond_4

    .line 1934
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/people/d/p;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1937
    :cond_4
    iget v1, p0, Lcom/google/android/gms/people/d/p;->g:I

    if-eqz v1, :cond_5

    .line 1938
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/people/d/p;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1941
    :cond_5
    iget v1, p0, Lcom/google/android/gms/people/d/p;->h:I

    if-eqz v1, :cond_6

    .line 1942
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/people/d/p;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1945
    :cond_6
    iget v1, p0, Lcom/google/android/gms/people/d/p;->i:I

    if-eqz v1, :cond_7

    .line 1946
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/people/d/p;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1949
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-eqz v1, :cond_8

    .line 1950
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1953
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 1954
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1957
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1811
    if-ne p1, p0, :cond_1

    .line 1854
    :cond_0
    :goto_0
    return v0

    .line 1814
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 1815
    goto :goto_0

    .line 1817
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/p;

    .line 1818
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/p;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 1819
    goto :goto_0

    .line 1821
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/p;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 1822
    goto :goto_0

    .line 1824
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/p;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 1825
    goto :goto_0

    .line 1827
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/p;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 1828
    goto :goto_0

    .line 1830
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/p;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/p;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1831
    goto :goto_0

    .line 1833
    :cond_7
    iget v2, p0, Lcom/google/android/gms/people/d/p;->f:I

    iget v3, p1, Lcom/google/android/gms/people/d/p;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1834
    goto :goto_0

    .line 1836
    :cond_8
    iget v2, p0, Lcom/google/android/gms/people/d/p;->g:I

    iget v3, p1, Lcom/google/android/gms/people/d/p;->g:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1837
    goto :goto_0

    .line 1839
    :cond_9
    iget v2, p0, Lcom/google/android/gms/people/d/p;->h:I

    iget v3, p1, Lcom/google/android/gms/people/d/p;->h:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1840
    goto :goto_0

    .line 1842
    :cond_a
    iget v2, p0, Lcom/google/android/gms/people/d/p;->i:I

    iget v3, p1, Lcom/google/android/gms/people/d/p;->i:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1843
    goto :goto_0

    .line 1845
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-nez v2, :cond_c

    .line 1846
    iget-object v2, p1, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1847
    goto :goto_0

    .line 1850
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    iget-object v3, p1, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1851
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 1859
    iget-wide v0, p0, Lcom/google/android/gms/people/d/p;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 1862
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/p;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1864
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/p;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1866
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/p;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1868
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/p;->e:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 1869
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/p;->f:I

    add-int/2addr v0, v1

    .line 1870
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/p;->g:I

    add-int/2addr v0, v1

    .line 1871
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/p;->h:I

    add-int/2addr v0, v1

    .line 1872
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/p;->i:I

    add-int/2addr v0, v1

    .line 1873
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 1875
    return v0

    .line 1868
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 1873
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/q;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1502
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/p;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/p;->c:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/p;->d:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/p;->e:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/p;->f:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/p;->g:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/p;->h:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/p;->i:I

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/q;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/p;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 1881
    iget-wide v0, p0, Lcom/google/android/gms/people/d/p;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 1882
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1884
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/people/d/p;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 1885
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1887
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/people/d/p;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 1888
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1890
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/p;->e:Z

    if-eqz v0, :cond_3

    .line 1891
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/p;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1893
    :cond_3
    iget v0, p0, Lcom/google/android/gms/people/d/p;->f:I

    if-eqz v0, :cond_4

    .line 1894
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/people/d/p;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1896
    :cond_4
    iget v0, p0, Lcom/google/android/gms/people/d/p;->g:I

    if-eqz v0, :cond_5

    .line 1897
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/people/d/p;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1899
    :cond_5
    iget v0, p0, Lcom/google/android/gms/people/d/p;->h:I

    if-eqz v0, :cond_6

    .line 1900
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/people/d/p;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1902
    :cond_6
    iget v0, p0, Lcom/google/android/gms/people/d/p;->i:I

    if-eqz v0, :cond_7

    .line 1903
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/people/d/p;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1905
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    if-eqz v0, :cond_8

    .line 1906
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/people/d/p;->j:Lcom/google/android/gms/people/d/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1908
    :cond_8
    iget-wide v0, p0, Lcom/google/android/gms/people/d/p;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 1909
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/people/d/p;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1911
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1912
    return-void
.end method
