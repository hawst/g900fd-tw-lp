.class public final Lcom/google/android/gms/fitness/d/j;
.super Lcom/google/android/gms/fitness/d/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/d/a;-><init>(Landroid/content/Context;)V

    .line 40
    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/d/o;)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 97
    new-instance v1, Lcom/google/android/gms/common/b;

    invoke-direct {v1}, Lcom/google/android/gms/common/b;-><init>()V

    .line 98
    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/fitness/d/j;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/common/service/AccountService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/fitness/d/j;->a:Landroid/content/Context;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 102
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/service/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/common/service/e;

    move-result-object v0

    .line 104
    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/d/o;->a(Lcom/google/android/gms/common/service/e;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 111
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/fitness/d/j;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    return-object v0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    .line 107
    new-instance v2, Lcom/google/android/gms/fitness/d/i;

    invoke-direct {v2, v0}, Lcom/google/android/gms/fitness/d/i;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/fitness/d/j;->a:Landroid/content/Context;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0

    .line 108
    :catch_1
    move-exception v0

    .line 109
    :try_start_2
    new-instance v2, Lcom/google/android/gms/fitness/d/i;

    invoke-direct {v2, v0}, Lcom/google/android/gms/fitness/d/i;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 133
    const-string v0, "Saving account directory"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 134
    new-instance v0, Lcom/google/android/gms/common/api/w;

    iget-object v1, p0, Lcom/google/android/gms/fitness/d/j;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/wearable/z;->g:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 138
    new-instance v1, Lcom/google/android/gms/fitness/d/m;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/fitness/d/m;-><init>(Lcom/google/android/gms/fitness/d/j;Lcom/google/android/gms/common/api/v;)V

    .line 146
    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 147
    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 148
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 149
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/s;)V
    .locals 4

    .prologue
    .line 191
    const-string v0, "Peer %s connected"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/wearable/s;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 192
    invoke-direct {p0}, Lcom/google/android/gms/fitness/d/j;->c()V

    .line 193
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 45
    const-string v0, "Saving authenticated account %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 46
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/fitness/d/a;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    .line 47
    if-eqz v0, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/fitness/d/j;->c()V

    .line 50
    :cond_0
    return v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 68
    const-string v0, "Resolving account name %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 69
    new-instance v0, Lcom/google/android/gms/fitness/d/k;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/fitness/d/k;-><init>(Lcom/google/android/gms/fitness/d/j;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/d/j;->a(Lcom/google/android/gms/fitness/d/o;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final bridge synthetic b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 36
    invoke-super {p0}, Lcom/google/android/gms/fitness/d/a;->b()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/wearable/s;)V
    .locals 4

    .prologue
    .line 197
    const-string v0, "Peer %s disconnected"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/wearable/s;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 198
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 80
    const-string v0, "Clearing selected account %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 81
    new-instance v0, Lcom/google/android/gms/fitness/d/l;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/fitness/d/l;-><init>(Lcom/google/android/gms/fitness/d/j;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/d/j;->a(Lcom/google/android/gms/fitness/d/o;)Ljava/lang/Object;

    .line 88
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 55
    const-string v0, "Revoking scopes for account %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 56
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/fitness/d/a;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 57
    invoke-direct {p0}, Lcom/google/android/gms/fitness/d/j;->c()V

    .line 58
    return-void
.end method

.method public final c(Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 121
    const-string v0, "<<default account>>"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    const/4 v0, 0x0

    .line 126
    :goto_0
    return-object v0

    .line 125
    :cond_0
    invoke-static {}, Lcom/google/android/gms/common/internal/ay;->b()Landroid/content/Intent;

    move-result-object v0

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/fitness/d/j;->a:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    goto :goto_0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/fitness/d/d;
    .locals 2

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/fitness/d/p;

    iget-object v1, p0, Lcom/google/android/gms/fitness/d/j;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/fitness/d/p;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-object v0
.end method
