.class public final Lcom/google/android/gms/wallet/service/ow/aq;
.super Lcom/google/android/gms/wallet/service/ow/w;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ow/w;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/service/ow/w;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/w;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/aq;->a:Lcom/google/android/gms/wallet/service/ow/w;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 1

    .prologue
    .line 47
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aq;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aq;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 75
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 76
    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aq;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 69
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aq;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 38
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    const/4 v0, 0x0

    .line 41
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aq;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 31
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/aq;->a:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
