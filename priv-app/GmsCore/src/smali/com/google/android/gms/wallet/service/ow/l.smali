.class public final Lcom/google/android/gms/wallet/service/ow/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/aa/b/a/a/a/a/b;


# direct methods
.method public constructor <init>(Lcom/google/aa/b/a/a/a/a/i;)V
    .locals 6

    .prologue
    .line 74
    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/i;->a:Lcom/google/aa/b/a/a/a/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/i;->b:Ljava/lang/String;

    iget-boolean v4, p1, Lcom/google/aa/b/a/a/a/a/i;->y:Z

    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v5, p1, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/l;-><init>(Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;Ljava/lang/String;ZLjava/lang/String;)V

    .line 80
    return-void

    .line 74
    :cond_0
    const/4 v5, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 84
    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    move-object v0, p0

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ow/l;-><init>(Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;Ljava/lang/String;ZLjava/lang/String;)V

    .line 89
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/b;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 61
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 62
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u001f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u001f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\u001f"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p5, :cond_0

    :goto_0
    invoke-virtual {v0, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/l;->a:Ljava/lang/String;

    .line 70
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/l;->b:Lcom/google/aa/b/a/a/a/a/b;

    .line 71
    return-void

    .line 63
    :cond_0
    const-string p5, ""

    goto :goto_0
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 98
    if-ne p0, p1, :cond_1

    .line 105
    :cond_0
    :goto_0
    return v0

    .line 100
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 101
    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 103
    goto :goto_0

    .line 104
    :cond_3
    check-cast p1, Lcom/google/android/gms/wallet/service/ow/l;

    .line 105
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/l;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wallet/service/ow/l;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/l;->b:Lcom/google/aa/b/a/a/a/a/b;

    iget-object v3, p1, Lcom/google/android/gms/wallet/service/ow/l;->b:Lcom/google/aa/b/a/a/a/a/b;

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/l;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/l;->b:Lcom/google/aa/b/a/a/a/a/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/y;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
