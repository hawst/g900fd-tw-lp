.class public Lcom/google/android/gms/auth/be/proximity/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/auth/be/proximity/g;

.field private static final b:Ljava/lang/String;

.field private static final c:[B


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/auth/be/cryptauth/a/a;

.field private final f:Lcom/google/android/gms/auth/be/proximity/h;

.field private final g:Lcom/google/android/gms/auth/g/b;

.field private final h:Lcom/google/android/gms/auth/be/proximity/b/c;

.field private final i:Lcom/google/android/gms/auth/authzen/keyservice/g;

.field private final j:Lcom/google/android/gms/auth/authzen/b/d;

.field private final k:Lcom/google/android/gms/auth/be/proximity/authorization/a;

.field private final l:Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;

.field private final m:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const-class v0, Lcom/google/android/gms/auth/be/proximity/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    .line 73
    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/g;->c:[B

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 87
    new-instance v2, Lcom/google/android/gms/auth/be/cryptauth/a/a;

    invoke-direct {v2, p1}, Lcom/google/android/gms/auth/be/cryptauth/a/a;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/google/android/gms/auth/be/proximity/h;

    invoke-direct {v3}, Lcom/google/android/gms/auth/be/proximity/h;-><init>()V

    new-instance v4, Lcom/google/android/gms/auth/g/b;

    invoke-direct {v4, p1}, Lcom/google/android/gms/auth/g/b;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/auth/be/proximity/b/d;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/b/c;

    move-result-object v5

    invoke-static {}, Lcom/google/android/gms/auth/authzen/b/d;->a()Lcom/google/android/gms/auth/authzen/b/d;

    move-result-object v6

    new-instance v7, Lcom/google/android/gms/auth/authzen/keyservice/g;

    invoke-direct {v7, p1}, Lcom/google/android/gms/auth/authzen/keyservice/g;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/authorization/a;->a()Lcom/google/android/gms/auth/be/proximity/authorization/a;

    move-result-object v8

    invoke-static {p1}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/auth/be/proximity/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/cryptauth/a/a;Lcom/google/android/gms/auth/be/proximity/h;Lcom/google/android/gms/auth/g/b;Lcom/google/android/gms/auth/be/proximity/b/c;Lcom/google/android/gms/auth/authzen/b/d;Lcom/google/android/gms/auth/authzen/keyservice/g;Lcom/google/android/gms/auth/be/proximity/authorization/a;Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;)V

    .line 97
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/cryptauth/a/a;Lcom/google/android/gms/auth/be/proximity/h;Lcom/google/android/gms/auth/g/b;Lcom/google/android/gms/auth/be/proximity/b/c;Lcom/google/android/gms/auth/authzen/b/d;Lcom/google/android/gms/auth/authzen/keyservice/g;Lcom/google/android/gms/auth/be/proximity/authorization/a;Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;)V
    .locals 1

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    iput-object p1, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    .line 111
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/cryptauth/a/a;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->e:Lcom/google/android/gms/auth/be/cryptauth/a/a;

    .line 112
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/h;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->f:Lcom/google/android/gms/auth/be/proximity/h;

    .line 113
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/g/b;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->g:Lcom/google/android/gms/auth/g/b;

    .line 114
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/b/c;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->h:Lcom/google/android/gms/auth/be/proximity/b/c;

    .line 115
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/authzen/b/d;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->j:Lcom/google/android/gms/auth/authzen/b/d;

    .line 116
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/authzen/keyservice/g;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->i:Lcom/google/android/gms/auth/authzen/keyservice/g;

    .line 117
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/a;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->k:Lcom/google/android/gms/auth/be/proximity/authorization/a;

    .line 118
    invoke-static {p9}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->l:Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;

    .line 119
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->m:Ljava/lang/Object;

    .line 120
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/g;
    .locals 3

    .prologue
    .line 63
    const-class v1, Lcom/google/android/gms/auth/be/proximity/g;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/g;->a:Lcom/google/android/gms/auth/be/proximity/g;

    if-nez v0, :cond_0

    .line 65
    new-instance v0, Lcom/google/android/gms/auth/be/proximity/g;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/be/proximity/g;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/g;->a:Lcom/google/android/gms/auth/be/proximity/g;

    .line 67
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/g;->a:Lcom/google/android/gms/auth/be/proximity/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 350
    const-string v0, "permit://google.com/easyunlock/v1/%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/gms/auth/h/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    const-string v1, "com.google.android.gms.auth.be.proximity.ProximitySyncManager.proximity_features"

    invoke-virtual {v0, v1, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 132
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 133
    const-string v0, "last_proactive_sync_time"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 136
    sub-long v6, v2, v4

    sget-object v0, Lcom/google/android/gms/auth/b/a;->E:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-gez v0, :cond_1

    .line 138
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v1, "Sync bypassed. Last proactive sync was %dms ago."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v10

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    return-void

    .line 143
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_proactive_sync_time"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 147
    new-instance v1, Lcom/google/android/gms/auth/be/proximity/f;

    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/be/proximity/f;-><init>(Landroid/content/Context;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 150
    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/be/proximity/f;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 157
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/be/proximity/g;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 14

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 193
    iget-object v10, p0, Lcom/google/android/gms/auth/be/proximity/g;->m:Ljava/lang/Object;

    monitor-enter v10

    .line 194
    :try_start_0
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v2, "Syncing data for account: %s..."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :try_start_1
    invoke-static {p1}, Lcom/google/android/gms/auth/be/proximity/g;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v11

    .line 202
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/g;->e:Lcom/google/android/gms/auth/be/cryptauth/a/a;

    new-instance v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/ah;

    invoke-direct {v6}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ah;-><init>()V

    iget-object v12, v1, Lcom/google/android/gms/auth/be/cryptauth/a/a;->a:Lcom/google/android/gms/auth/gencode/authzen/server/api/f;

    new-instance v13, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, v1, Lcom/google/android/gms/auth/be/cryptauth/a/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v1, v1, Lcom/google/android/gms/auth/be/cryptauth/a/a;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v13, v2, p1, p1, v1}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/auth/b/a;->ai:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v13, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;

    iget-object v2, v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/ah;->e:Ljava/util/Set;

    iget-boolean v3, v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/ah;->a:Z

    iget-boolean v4, v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/ah;->b:Z

    iget v5, v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/ah;->c:I

    iget v6, v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/ah;->d:I

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;-><init>(Ljava/util/Set;ZZII)V

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;

    move-object v5, v0

    const-string v4, "deviceSync/getmydevices"

    iget-object v1, v12, Lcom/google/android/gms/auth/gencode/authzen/server/api/f;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x1

    const-class v6, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesResponseEntity;

    move-object v2, v13

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesResponseEntity;

    .line 203
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/g;->i:Lcom/google/android/gms/auth/authzen/keyservice/g;

    const-string v3, "device_key"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/authzen/keyservice/g;->b(Ljava/lang/String;)[B

    move-result-object v2

    .line 204
    iget-object v3, p0, Lcom/google/android/gms/auth/be/proximity/g;->f:Lcom/google/android/gms/auth/be/proximity/h;

    invoke-static {v1, v2}, Lcom/google/android/gms/auth/be/proximity/h;->a(Lcom/google/android/gms/auth/gencode/authzen/server/api/aj;[B)Lcom/google/android/gms/auth/gencode/authzen/server/api/h;

    move-result-object v3

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/g;->f:Lcom/google/android/gms/auth/be/proximity/h;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/aj;->e()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/aj;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/h;

    invoke-interface {v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/h;->h()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catch Lcom/google/android/gms/auth/ae; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 210
    :catch_0
    move-exception v1

    .line 215
    :try_start_3
    sget-object v2, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v3, "Failed to authenticate user: %s. Clearing data."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v2, v8

    move-object v3, v7

    .line 221
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/g;->h:Lcom/google/android/gms/auth/be/proximity/b/c;

    invoke-virtual {v1, v11}, Lcom/google/android/gms/auth/be/proximity/b/c;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    move-result-object v4

    .line 224
    invoke-static {v4, v7}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    if-nez v7, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/g;->h:Lcom/google/android/gms/auth/be/proximity/b/c;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/auth/be/proximity/b/c;->c(Ljava/lang/String;)Z

    sget-object v1, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v5, "Removed permit %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->c()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v12

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_1
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/g;->l:Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/b;->a()V

    .line 230
    invoke-static {v4, v7}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 232
    :cond_2
    :goto_3
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 233
    iget-object v4, p0, Lcom/google/android/gms/auth/be/proximity/g;->k:Lcom/google/android/gms/auth/be/proximity/authorization/a;

    invoke-virtual {v4, v11, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/a;->a(Ljava/lang/String;Ljava/util/Set;)V
    :try_end_3
    .catch Lcom/google/android/gms/auth/q; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 236
    :cond_3
    if-nez v2, :cond_c

    .line 237
    :try_start_4
    monitor-exit v10
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v1, v8

    .line 258
    :goto_4
    return v1

    .line 207
    :cond_4
    if-nez v3, :cond_5

    move-object v1, v7

    :goto_5
    move v2, v9

    move-object v7, v1

    .line 220
    goto :goto_1

    .line 207
    :cond_5
    :try_start_5
    invoke-interface {v3}, Lcom/google/android/gms/auth/gencode/authzen/server/api/h;->g()Z

    move-result v1

    if-nez v1, :cond_6

    move-object v1, v7

    goto :goto_5

    :cond_6
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/data/c;

    invoke-direct {v1}, Lcom/google/android/gms/auth/firstparty/proximity/data/c;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/auth/be/proximity/g;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/auth/firstparty/proximity/data/c;->a:Ljava/lang/String;

    iput-object p1, v1, Lcom/google/android/gms/auth/firstparty/proximity/data/c;->b:Ljava/lang/String;

    const-string v2, "unlock"

    iput-object v2, v1, Lcom/google/android/gms/auth/firstparty/proximity/data/c;->c:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    iget-object v5, p0, Lcom/google/android/gms/auth/be/proximity/g;->g:Lcom/google/android/gms/auth/g/b;

    iget-object v5, v5, Lcom/google/android/gms/auth/g/b;->a:Lcom/google/android/gms/common/c/a;

    invoke-virtual {v5}, Lcom/google/android/gms/common/c/a;->a()Ljava/lang/String;

    move-result-object v5

    const-string v6, "AUTHZEN_PUBLIC_KEY"

    sget-object v12, Lcom/google/android/gms/auth/be/proximity/g;->c:[B

    invoke-direct {v2, v5, v6, v12}, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    iput-object v2, v1, Lcom/google/android/gms/auth/firstparty/proximity/data/c;->d:Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    const-string v2, "bluetooth_classic"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/firstparty/proximity/data/c;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/proximity/data/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/proximity/data/c;->a()Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    move-result-object v2

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v5, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    const-string v6, "AUTHZEN_PUBLIC_KEY"

    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->c(Ljava/lang/String;)[B

    move-result-object v12

    invoke-direct {v5, v1, v6, v12}, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;-><init>(Ljava/lang/String;Ljava/lang/String;[B)V

    invoke-virtual {v2, v5}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->a(Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;)V
    :try_end_5
    .catch Lcom/google/android/gms/auth/ae; {:try_start_5 .. :try_end_5} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_6

    .line 255
    :catch_1
    move-exception v1

    .line 256
    :goto_7
    :try_start_6
    sget-object v2, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v3, "Failed to sync data with server for account: %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 258
    monitor-exit v10
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v1, v8

    goto :goto_4

    :cond_7
    move-object v1, v2

    .line 207
    goto :goto_5

    .line 224
    :cond_8
    :try_start_7
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/g;->h:Lcom/google/android/gms/auth/be/proximity/b/c;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/auth/be/proximity/b/c;->a(Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;)Z

    sget-object v1, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v5, "Saved permit %s, with %d requester access(es)."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual {v7}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->c()Ljava/lang/String;

    move-result-object v13

    aput-object v13, v6, v12

    const/4 v12, 0x1

    invoke-virtual {v7}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->e()Ljava/util/List;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v13

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v6, v12

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 255
    :catch_2
    move-exception v1

    goto :goto_7

    .line 230
    :cond_9
    if-nez v7, :cond_a

    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->e()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V
    :try_end_7
    .catch Lcom/google/android/gms/auth/q; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_3

    .line 260
    :catchall_0
    move-exception v1

    monitor-exit v10

    throw v1

    .line 230
    :cond_a
    if-nez v4, :cond_b

    :try_start_8
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v7}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->e()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    goto/16 :goto_3

    :cond_b
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->e()Ljava/util/List;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v7}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->e()Ljava/util/List;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->b()Ljava/util/List;

    move-result-object v5

    invoke-virtual {v7}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->b()Ljava/util/List;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->d()Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    move-result-object v5

    invoke-virtual {v7}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->d()Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance v5, Ljava/util/HashSet;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->e()Ljava/util/List;

    move-result-object v4

    invoke-direct {v5, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v7}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->e()Ljava/util/List;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    invoke-interface {v1, v5}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    goto/16 :goto_3

    .line 240
    :cond_c
    if-nez v3, :cond_d

    .line 241
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v2, "Device didn\'t appear in the device list after Authzen is registered."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Lcom/google/android/gms/auth/q; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 243
    :try_start_9
    monitor-exit v10
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move v1, v8

    goto/16 :goto_4

    .line 246
    :cond_d
    :try_start_a
    invoke-interface {v3}, Lcom/google/android/gms/auth/gencode/authzen/server/api/h;->g()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {v3}, Lcom/google/android/gms/auth/gencode/authzen/server/api/h;->d()Z

    move-result v1

    if-nez v1, :cond_e

    .line 247
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v2, "Forcing Authzen registration for account %s because EasyUnlock is enabled and bluetooth address is missing."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/g;->j:Lcom/google/android/gms/auth/authzen/b/d;

    sget-object v2, Lcom/google/android/gms/auth/authzen/b/e;->b:Lcom/google/android/gms/auth/authzen/b/e;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/gms/auth/authzen/b/d;->a(Lcom/google/android/gms/auth/authzen/b/e;Ljava/lang/String;)Z
    :try_end_a
    .catch Lcom/google/android/gms/auth/q; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    move-result v1

    :try_start_b
    monitor-exit v10
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto/16 :goto_4

    .line 254
    :cond_e
    monitor-exit v10

    move v1, v9

    goto/16 :goto_4
.end method

.method public final b()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    const-string v1, "com.google.android.gms.auth.be.proximity.ProximitySyncManager.proximity_features"

    invoke-virtual {v0, v1, v10}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 170
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 171
    const-string v0, "last_sync_on_authentication_failure_time_in_millis"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v0, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 174
    sub-long v6, v2, v4

    sget-object v0, Lcom/google/android/gms/auth/b/a;->F:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v0, v6, v8

    if-gez v0, :cond_1

    .line 176
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/g;->b:Ljava/lang/String;

    const-string v1, "Sync bypassed. Last sync (on authentication failure) was %dms ago."

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v6, v10

    invoke-static {v1, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_0
    return-void

    .line 181
    :cond_1
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_sync_on_authentication_failure_time_in_millis"

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/g;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 187
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/be/proximity/g;->a(Ljava/lang/String;)Z

    goto :goto_0
.end method
