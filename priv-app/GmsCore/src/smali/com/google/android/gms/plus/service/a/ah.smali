.class public final Lcom/google/android/gms/plus/service/a/ah;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/ah;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 38
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/ah;->b:Ljava/lang/String;

    .line 39
    iput p3, p0, Lcom/google/android/gms/plus/service/a/ah;->c:I

    .line 40
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/ah;->d:Ljava/lang/String;

    .line 41
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ah;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    .line 69
    :goto_0
    return-void

    .line 51
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/ah;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/ah;->b:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/plus/service/a/ah;->c:I

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/ah;->d:Ljava/lang/String;

    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ILjava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    .line 53
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 55
    :catch_0
    move-exception v0

    .line 56
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 57
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 58
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    invoke-static {v8, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 62
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ah;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    invoke-static {v8, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {v1, v0, v6}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0

    .line 66
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1, v6}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ah;->e:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    .line 76
    :cond_0
    return-void
.end method
