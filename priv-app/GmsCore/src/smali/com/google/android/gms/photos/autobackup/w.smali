.class public final Lcom/google/android/gms/photos/autobackup/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private a:Lcom/google/android/gms/photos/autobackup/a/ac;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/w;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/w;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->b(ILjava/util/List;)V

    .line 49
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 22
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    const-class v0, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/ac;

    invoke-static {p1, v2}, Lcom/google/android/libraries/social/autobackup/u;->a(Landroid/content/Context;I)[Lcom/google/android/libraries/social/autobackup/w;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    array-length v1, v3

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v5, v3

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v3, v1

    iget-object v7, v6, Lcom/google/android/libraries/social/autobackup/w;->a:Ljava/lang/Integer;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    iget-object v6, v6, Lcom/google/android/libraries/social/autobackup/w;->b:Ljava/lang/String;

    invoke-interface {v0, v7}, Lcom/google/android/libraries/social/autobackup/ac;->c(Ljava/lang/String;)Z

    move-result v9

    invoke-direct {v8, v6, v7, v9}, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/w;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    invoke-interface {v0, v2, v4}, Lcom/google/android/gms/photos/autobackup/a/ac;->b(ILjava/util/List;)V

    return-void
.end method
