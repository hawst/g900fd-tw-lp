.class public Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljava/util/concurrent/BlockingQueue;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Lcom/google/android/gms/drive/g/aw;

.field private final f:Lcom/google/android/gms/drive/database/r;

.field private final g:Lcom/google/android/gms/drive/g/at;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;-><init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/BlockingQueue;)V

    .line 58
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/BlockingQueue;)V
    .locals 4

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->d:Ljava/util/ArrayList;

    .line 61
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/aw;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->e:Lcom/google/android/gms/drive/g/aw;

    .line 62
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/BlockingQueue;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->b:Ljava/util/concurrent/BlockingQueue;

    .line 63
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->f:Lcom/google/android/gms/drive/database/r;

    .line 64
    new-instance v0, Lcom/google/android/gms/drive/g/e;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->b()Lcom/google/android/gms/drive/g/i;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/ai;->C:Lcom/google/android/gms/common/a/d;

    sget-object v3, Lcom/google/android/gms/drive/ai;->D:Lcom/google/android/gms/common/a/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/g/e;-><init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->g:Lcom/google/android/gms/drive/g/at;

    .line 67
    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;

    .line 195
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->interrupt()V

    goto :goto_0

    .line 199
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;

    .line 201
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 203
    :catch_0
    move-exception v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 204
    const-string v0, "FeedProcessorDriver"

    const-string v2, "Producer not cleaned up correctly."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 211
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;

    .line 214
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a()Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    if-eqz v0, :cond_2

    .line 223
    :cond_3
    :goto_2
    return-void

    .line 219
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 220
    const-string v0, "FeedProcessorDriver"

    const-string v1, "Producer not cleaned up correctly."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V
    .locals 12

    .prologue
    .line 227
    invoke-interface {p3, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->b(Lcom/google/android/gms/drive/d/g;)V

    .line 229
    invoke-interface {p2}, Lcom/google/android/gms/drive/d/g;->a()Ljava/util/List;

    move-result-object v1

    .line 230
    if-nez v1, :cond_0

    .line 254
    :goto_0
    return-void

    .line 234
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->f:Lcom/google/android/gms/drive/database/r;

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->g:Lcom/google/android/gms/drive/g/at;

    invoke-interface {v0, v4}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/g/at;)V

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->f:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 238
    :try_start_0
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/d/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 240
    :try_start_1
    invoke-interface {p3, p2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a(Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/d/d;)V
    :try_end_1
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 241
    :catch_0
    move-exception v5

    .line 242
    :try_start_2
    iget-object v6, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v6, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v6, Landroid/content/SyncStats;->numParseExceptions:J

    .line 243
    const-string v6, "FeedProcessorDriver"

    const-string v7, "Error parsing entry."

    invoke-static {v6, v5, v7}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 244
    const-string v6, "FeedProcessorDriver"

    const-string v7, "Error parsing entry %s."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v8, v9

    invoke-static {v6, v5, v7, v8}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 250
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->f:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    throw v0

    .line 247
    :cond_1
    :try_start_3
    invoke-interface {p3, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a(Lcom/google/android/gms/drive/d/g;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->f:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->f:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 252
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 253
    const-string v0, "FeedProcessorDriver"

    const-string v4, "Stored ResultsPage of %d entries (%d msec)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x1

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;

    const-string v1, "No feeds to process"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;-><init>(Ljava/lang/String;)V

    throw v0

    .line 101
    :cond_0
    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    .line 103
    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a()V

    .line 104
    const/4 v0, 0x0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/af/c/a/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/af/c/a/e; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/af/c/a/c; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/af/c/a/b; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catch Lcom/google/af/c/c/a; {:try_start_0 .. :try_end_0} :catch_6
    .catch Lcom/google/af/c/a/a; {:try_start_0 .. :try_end_0} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_a
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v0

    .line 150
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p1, Landroid/content/SyncResult;->databaseError:Z

    .line 153
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;

    const-string v2, "Database corrupted"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a()V

    throw v0

    :cond_1
    move v4, v1

    .line 107
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/drive/g/f;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "Interrupted during processing"

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/af/c/a/d; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/af/c/a/e; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/af/c/a/c; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/af/c/a/b; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Lcom/google/af/c/c/a; {:try_start_2 .. :try_end_2} :catch_6
    .catch Lcom/google/af/c/a/a; {:try_start_2 .. :try_end_2} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_b
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 154
    :catch_1
    move-exception v0

    .line 155
    :try_start_3
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    .line 156
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;

    const-string v2, "Error getting feed data"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 111
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->b:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;
    :try_end_4
    .catch Landroid/database/SQLException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Lcom/google/af/c/a/d; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/af/c/a/e; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/af/c/a/c; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/google/af/c/a/b; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catch Lcom/google/af/c/c/a; {:try_start_4 .. :try_end_4} :catch_6
    .catch Lcom/google/af/c/a/a; {:try_start_4 .. :try_end_4} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/text/ParseException; {:try_start_4 .. :try_end_4} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_a
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_b
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 113
    :try_start_5
    iget-object v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    if-eqz v1, :cond_3

    move v1, v3

    :goto_2
    if-eqz v1, :cond_4

    .line 114
    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->c:Ljava/lang/Exception;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 141
    :catchall_1
    move-exception v0

    :try_start_6
    throw v0
    :try_end_6
    .catch Landroid/database/SQLException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/google/af/c/a/d; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/af/c/a/e; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/google/af/c/a/c; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/google/af/c/a/b; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catch Lcom/google/af/c/c/a; {:try_start_6 .. :try_end_6} :catch_6
    .catch Lcom/google/af/c/a/a; {:try_start_6 .. :try_end_6} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_6 .. :try_end_6} :catch_8
    .catch Ljava/text/ParseException; {:try_start_6 .. :try_end_6} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_a
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 157
    :catch_2
    move-exception v0

    .line 158
    :try_start_7
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    .line 159
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;

    const-string v2, "Error getting feed data"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :cond_3
    move v1, v2

    .line 113
    goto :goto_2

    .line 118
    :cond_4
    :try_start_8
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 119
    add-int/lit8 v1, v4, 0x1

    .line 120
    iget v4, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    iget-object v6, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    if-eqz v6, :cond_5

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    invoke-interface {v0}, Lcom/google/android/gms/drive/d/g;->b()Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-virtual {v5, v4, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move-result v0

    if-ne v1, v0, :cond_1

    .line 145
    :goto_4
    :try_start_9
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_a

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    .line 147
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a(Ljava/lang/String;)V
    :try_end_9
    .catch Landroid/database/SQLException; {:try_start_9 .. :try_end_9} :catch_0
    .catch Lcom/google/af/c/a/d; {:try_start_9 .. :try_end_9} :catch_1
    .catch Lcom/google/af/c/a/e; {:try_start_9 .. :try_end_9} :catch_2
    .catch Lcom/google/af/c/a/c; {:try_start_9 .. :try_end_9} :catch_3
    .catch Lcom/google/af/c/a/b; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5
    .catch Lcom/google/af/c/c/a; {:try_start_9 .. :try_end_9} :catch_6
    .catch Lcom/google/af/c/a/a; {:try_start_9 .. :try_end_9} :catch_7
    .catch Landroid/accounts/AuthenticatorException; {:try_start_9 .. :try_end_9} :catch_8
    .catch Ljava/text/ParseException; {:try_start_9 .. :try_end_9} :catch_9
    .catch Ljava/lang/InterruptedException; {:try_start_9 .. :try_end_9} :catch_a
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_b
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 145
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 120
    :cond_5
    :try_start_a
    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->d:Ljava/lang/String;

    goto :goto_3

    .line 131
    :cond_6
    invoke-static {}, Lcom/google/android/gms/drive/g/f;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 132
    new-instance v1, Ljava/lang/InterruptedException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Interrupted while processing #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 136
    :cond_7
    sget-boolean v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a:Z

    if-nez v1, :cond_9

    iget v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    if-ltz v1, :cond_8

    iget v1, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    iget-object v6, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lt v1, v6, :cond_9

    :cond_8
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    .line 138
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    iget v6, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->a:I

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    .line 139
    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;->b:Lcom/google/android/gms/drive/d/g;

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/d/g;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    .line 188
    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a()V

    .line 189
    return-void

    .line 160
    :catch_3
    move-exception v0

    .line 161
    :try_start_b
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 162
    throw v0

    .line 163
    :catch_4
    move-exception v0

    .line 164
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 165
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;

    const-string v2, "Error getting feed data"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 166
    :catch_5
    move-exception v0

    .line 167
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 168
    throw v0

    .line 170
    :catch_6
    move-exception v0

    iget-object v0, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 171
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 172
    :catch_7
    move-exception v0

    .line 173
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 174
    throw v0

    .line 175
    :catch_8
    move-exception v0

    .line 176
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 177
    throw v0

    .line 178
    :catch_9
    move-exception v0

    .line 179
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    .line 180
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;

    const-string v2, "Feed data contains error"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 181
    :catch_a
    move-exception v0

    .line 182
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 183
    throw v0

    .line 184
    :catch_b
    move-exception v0

    .line 185
    iget-object v1, p1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    add-long/2addr v2, v8

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 186
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;

    const-string v2, "Runtime Exception"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/o;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_b
    move v4, v2

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;ILcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V
    .locals 7

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 79
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->e:Lcom/google/android/gms/drive/g/aw;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->b:Ljava/util/concurrent/BlockingQueue;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;-><init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;II)V

    .line 83
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->a()V

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    return-void

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
