.class public final Lcom/google/android/gms/photos/autobackup/service/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/autobackup/h;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/android/gms/photos/autobackup/service/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/d;->a:Landroid/content/Context;

    .line 24
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/service/d;)Lcom/google/android/gms/photos/autobackup/service/a/b;
    .locals 1

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/d;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/photos/autobackup/service/d;)Lcom/google/android/gms/photos/autobackup/service/a/b;
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/d;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 28
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/d;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 53
    :goto_0
    monitor-exit p0

    return-void

    .line 33
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/photos/autobackup/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/d;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/service/e;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/service/e;-><init>(Lcom/google/android/gms/photos/autobackup/service/d;)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/service/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/photos/autobackup/service/a/f;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/d;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
