.class public final Lcom/google/android/gms/wallet/common/ui/validator/n;
.super Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/HashMap;


# direct methods
.method protected constructor <init>(Landroid/widget/TextView;Ljava/util/HashMap;)V
    .locals 1

    .prologue
    .line 127
    invoke-virtual {p1}, Landroid/widget/TextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;-><init>(Landroid/content/Context;)V

    .line 128
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->a:Ljava/lang/ref/WeakReference;

    .line 129
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->c:Ljava/lang/String;

    .line 130
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->d:Ljava/util/HashMap;

    .line 131
    return-void
.end method


# virtual methods
.method protected final a()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->a:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 138
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 116
    new-instance v0, Landroid/util/Pair;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->c:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method

.method protected final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 116
    check-cast p1, Landroid/util/Pair;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/validator/n;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->d:Ljava/util/HashMap;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v2, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/ui/dn;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/n;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    :cond_0
    return-void
.end method
