.class public Lcom/google/android/gms/photos/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/d;


# static fields
.field private static a:Lcom/google/android/gms/photos/b;


# instance fields
.field private final b:Lcom/google/android/libraries/social/a/a;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lcom/google/android/libraries/social/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/a/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/b;->b:Lcom/google/android/libraries/social/a/a;

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/photos/b;->b:Lcom/google/android/libraries/social/a/a;

    new-instance v1, Lcom/google/android/libraries/social/a/g;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/a/g;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Lcom/google/android/libraries/social/a/f;)Lcom/google/android/libraries/social/a/a;

    .line 31
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/photos/b;
    .locals 2

    .prologue
    .line 16
    sget-object v0, Lcom/google/android/gms/photos/b;->a:Lcom/google/android/gms/photos/b;

    if-nez v0, :cond_1

    .line 17
    const-class v1, Lcom/google/android/gms/photos/b;

    monitor-enter v1

    .line 18
    :try_start_0
    sget-object v0, Lcom/google/android/gms/photos/b;->a:Lcom/google/android/gms/photos/b;

    if-nez v0, :cond_0

    .line 19
    new-instance v0, Lcom/google/android/gms/photos/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/b;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/photos/b;->a:Lcom/google/android/gms/photos/b;

    .line 21
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 23
    :cond_1
    sget-object v0, Lcom/google/android/gms/photos/b;->a:Lcom/google/android/gms/photos/b;

    return-object v0

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final U_()Lcom/google/android/libraries/social/a/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/photos/b;->b:Lcom/google/android/libraries/social/a/a;

    return-object v0
.end method
