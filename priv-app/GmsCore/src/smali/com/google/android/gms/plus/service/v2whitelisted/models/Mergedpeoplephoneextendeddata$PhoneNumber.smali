.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/j;

.field private static final e:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 174
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/j;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/j;

    .line 183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 186
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->e:Ljava/util/HashMap;

    const-string v1, "e164"

    const-string v2, "e164"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->e:Ljava/util/HashMap;

    const-string v1, "internationalizationData"

    const-string v2, "internationalizationData"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 215
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 216
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->b:I

    .line 217
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a:Ljava/util/Set;

    .line 218
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;)V
    .locals 0

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 227
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a:Ljava/util/Set;

    .line 228
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->b:I

    .line 229
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->c:Ljava/lang/String;

    .line 230
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;

    .line 231
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 672
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 673
    packed-switch v0, :pswitch_data_0

    .line 678
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 675
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;

    .line 682
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 683
    return-void

    .line 673
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 657
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 658
    packed-switch v0, :pswitch_data_0

    .line 663
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 660
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->c:Ljava/lang/String;

    .line 666
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 667
    return-void

    .line 658
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 631
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 637
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 633
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->c:Ljava/lang/String;

    .line 635
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;

    goto :goto_0

    .line 631
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 616
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/j;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 700
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

    if-nez v0, :cond_0

    move v0, v1

    .line 731
    :goto_0
    return v0

    .line 705
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 706
    goto :goto_0

    .line 709
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;

    .line 710
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 711
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 712
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 714
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 716
    goto :goto_0

    :cond_3
    move v0, v1

    .line 721
    goto :goto_0

    .line 724
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 726
    goto :goto_0

    :cond_5
    move v0, v2

    .line 731
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 687
    const/4 v0, 0x0

    .line 688
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 689
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 690
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 691
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 693
    goto :goto_0

    .line 694
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 621
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/j;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/j;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber;Landroid/os/Parcel;I)V

    .line 622
    return-void
.end method
