.class public final Lcom/google/android/gms/feedback/a;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/feedback/ErrorReport;)Lcom/google/android/gms/feedback/a;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/feedback/a;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/a;-><init>()V

    .line 35
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 36
    const-string v2, "error_report"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 37
    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/a;->setArguments(Landroid/os/Bundle;)V

    .line 38
    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 43
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/gms/p;->om:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->nV:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->og:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "error_report"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/ErrorReport;

    .line 57
    packed-switch p2, :pswitch_data_0

    .line 62
    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->I:Z

    .line 64
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/FeedbackAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->setResult(I)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 67
    return-void

    .line 59
    :pswitch_0
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->I:Z

    goto :goto_0

    .line 57
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
