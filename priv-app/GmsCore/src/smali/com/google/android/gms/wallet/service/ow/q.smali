.class final Lcom/google/android/gms/wallet/service/ow/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    new-instance v1, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;-><init>(JLcom/google/android/gms/wallet/shared/service/ServerResponse;)V

    return-object v1
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    new-array v0, p1, [Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    return-object v0
.end method
