.class public final Lcom/google/android/gms/games/g/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:J

.field public c:I

.field public d:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 5073
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5074
    iput v0, p0, Lcom/google/android/gms/games/g/d;->a:I

    iput-wide v2, p0, Lcom/google/android/gms/games/g/d;->b:J

    iput v0, p0, Lcom/google/android/gms/games/g/d;->c:I

    iput-wide v2, p0, Lcom/google/android/gms/games/g/d;->d:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/d;->cachedSize:I

    .line 5075
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 5142
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5143
    iget v1, p0, Lcom/google/android/gms/games/g/d;->a:I

    if-eqz v1, :cond_0

    .line 5144
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/d;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5147
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 5148
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5151
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/d;->c:I

    if-eqz v1, :cond_2

    .line 5152
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/d;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5155
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 5156
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 5159
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5088
    if-ne p1, p0, :cond_1

    .line 5107
    :cond_0
    :goto_0
    return v0

    .line 5091
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 5092
    goto :goto_0

    .line 5094
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/d;

    .line 5095
    iget v2, p0, Lcom/google/android/gms/games/g/d;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/d;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 5096
    goto :goto_0

    .line 5098
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/d;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 5099
    goto :goto_0

    .line 5101
    :cond_4
    iget v2, p0, Lcom/google/android/gms/games/g/d;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/d;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 5102
    goto :goto_0

    .line 5104
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/d;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 5105
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 5112
    iget v0, p0, Lcom/google/android/gms/games/g/d;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 5114
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/games/g/d;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 5116
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/d;->c:I

    add-int/2addr v0, v1

    .line 5117
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/games/g/d;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 5119
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5020
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/d;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/d;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/d;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/d;->d:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 5125
    iget v0, p0, Lcom/google/android/gms/games/g/d;->a:I

    if-eqz v0, :cond_0

    .line 5126
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/d;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5128
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/g/d;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 5129
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 5131
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/d;->c:I

    if-eqz v0, :cond_2

    .line 5132
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/d;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5134
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/games/g/d;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 5135
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/g/d;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 5137
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5138
    return-void
.end method
