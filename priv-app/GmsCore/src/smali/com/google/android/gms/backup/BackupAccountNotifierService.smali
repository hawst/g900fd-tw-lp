.class public Lcom/google/android/gms/backup/BackupAccountNotifierService;
.super Landroid/app/IntentService;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private b:Lcom/google/android/gms/backup/ag;

.field private c:Landroid/app/NotificationManager;

.field private d:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "BackupAccountNotifierService"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    const-string v0, "BackupAccountNotifierService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/backup/BackupAccountNotifierService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "notify"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->c:Landroid/app/NotificationManager;

    const-string v1, "com.google.android.backup.notification.tag"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "notified"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    sget-object v0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Fail to write notification cancellation preference."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 92
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a(Landroid/content/Context;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 64
    new-instance v0, Lcom/google/android/gms/backup/ag;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/ag;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->b:Lcom/google/android/gms/backup/ag;

    .line 65
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->c:Landroid/app/NotificationManager;

    .line 66
    const-string v0, "com.google.android.gms.backup.BackupAccountNotifierService"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->d:Landroid/content/SharedPreferences;

    .line 67
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 71
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    const-string v0, "notify"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 76
    invoke-static {}, Landroid/os/Process;->myUserHandle()Landroid/os/UserHandle;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/UserHandle;->isOwner()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->b:Lcom/google/android/gms/backup/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/ag;->isBackupEnabled()Z

    move-result v3

    new-instance v0, Lcom/google/android/gms/backup/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/backup/b;->a()Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v3, :cond_2

    if-eqz v0, :cond_4

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a()V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->d:Landroid/content/SharedPreferences;

    const-string v3, "notified"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v3, 0x3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Showing notification."

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    :cond_5
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v3, 0x8000000

    invoke-static {p0, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v2, Landroid/app/Notification;

    invoke-direct {v2}, Landroid/app/Notification;-><init>()V

    iget v3, v2, Landroid/app/Notification;->flags:I

    or-int/lit16 v3, v3, 0x80

    iput v3, v2, Landroid/app/Notification;->flags:I

    const v3, 0x108008a

    iput v3, v2, Landroid/app/Notification;->icon:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v2, Landroid/app/Notification;->when:J

    invoke-virtual {p0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->rt:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    invoke-virtual {p0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->rn:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/p;->rm:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, p0, v3, v4, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->c:Landroid/app/NotificationManager;

    const-string v3, "com.google.android.backup.notification.tag"

    invoke-virtual {v0, v3, v1, v2}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    iget-object v0, p0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "notified"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Fail to write notification preference"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 78
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a()V

    goto/16 :goto_0
.end method
