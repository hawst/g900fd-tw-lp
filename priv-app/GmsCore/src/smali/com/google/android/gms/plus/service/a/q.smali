.class public final Lcom/google/android/gms/plus/service/a/q;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/f;

.field private final c:Lcom/google/android/gms/plus/model/posts/Post;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/q;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/q;->b:Lcom/google/android/gms/plus/internal/f;

    .line 46
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    .line 47
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 13

    .prologue
    .line 52
    :try_start_0
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;-><init>()V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->g()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->b:Ljava/util/Set;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 58
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    const/4 v0, 0x0

    new-instance v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;

    invoke-direct {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/gms/plus/model/posts/Post;->n()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2}, Lcom/google/android/gms/plus/model/posts/Post;->m()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/a/b;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/b;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/an;

    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->d:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    const/4 v5, 0x6

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->b:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    const/4 v5, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->k()Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ar;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ar;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ar;->a:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ar;->b:Ljava/util/Set;

    const/4 v6, 0x4

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;

    iget-object v6, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ar;->b:Ljava/util/Set;

    iget-object v5, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ar;->a:Ljava/lang/String;

    invoke-direct {v0, v6, v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;-><init>(Ljava/util/Set;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;

    iput-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$ImageEntity;

    iget-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    const/16 v5, 0xa

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ap;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ap;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/a/b;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ap;->a:Ljava/lang/String;

    iget-object v0, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ap;->c:Ljava/util/Set;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$DeepLinkEntity;

    iget-object v4, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ap;->c:Ljava/util/Set;

    iget-object v6, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ap;->a:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ap;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v6, v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$DeepLinkEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$DeepLinkEntity;

    iput-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity$AttachmentsEntity$DeepLinkEntity;

    iget-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->g:Ljava/util/Set;

    const/4 v4, 0x5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    const/4 v0, 0x1

    :cond_6
    invoke-virtual {v2}, Lcom/google/android/gms/plus/model/posts/Post;->l()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v2}, Lcom/google/android/gms/plus/model/posts/Post;->k()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/plus/sharebox/a/a;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/sharebox/a/a;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/an;)Lcom/google/android/gms/plus/service/v1whitelisted/models/an;

    const/4 v0, 0x1

    :cond_7
    if-eqz v0, :cond_9

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/an;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ai;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/au;

    .line 60
    new-instance v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;

    invoke-direct {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;-><init>()V

    .line 61
    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/au;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;)Lcom/google/android/gms/plus/service/v1whitelisted/models/af;

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/t;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iput-object v0, v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v0, v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->b:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/q;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->q()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->o()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->p()Z

    move-result v5

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;

    move-result-object v7

    check-cast v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    move-object v1, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/plus/b/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    move-result-object v3

    .line 68
    const/4 v0, 0x0

    .line 69
    if-eqz v3, :cond_8

    .line 70
    new-instance v0, Lcom/google/android/gms/plus/model/posts/Post;

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/model/posts/Post;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/model/posts/Post;->g()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/model/posts/Post;->h()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/model/posts/Post;->i()Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v7}, Lcom/google/android/gms/plus/model/posts/Post;->k()Landroid/os/Bundle;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v8}, Lcom/google/android/gms/plus/model/posts/Post;->m()Landroid/os/Bundle;

    move-result-object v8

    iget-object v9, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v9}, Lcom/google/android/gms/plus/model/posts/Post;->o()Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v10}, Lcom/google/android/gms/plus/model/posts/Post;->p()Z

    move-result v10

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iget-object v11, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v11}, Lcom/google/android/gms/plus/model/posts/Post;->q()Ljava/lang/String;

    move-result-object v11

    iget-object v12, p0, Lcom/google/android/gms/plus/service/a/q;->c:Lcom/google/android/gms/plus/model/posts/Post;

    invoke-virtual {v12}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v12

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/plus/model/posts/Post;-><init>(Ljava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    .line 78
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/q;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 93
    :goto_1
    return-void

    .line 58
    :cond_9
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 81
    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    const/4 v3, 0x0

    invoke-static {p1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 82
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-interface {v0, v2, v1, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V

    goto :goto_1

    .line 86
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/q;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V

    goto :goto_1

    .line 89
    :catch_2
    move-exception v0

    .line 90
    const-string v1, "InsertActivityOperation"

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->b:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/q;->b:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 100
    :cond_0
    return-void
.end method
