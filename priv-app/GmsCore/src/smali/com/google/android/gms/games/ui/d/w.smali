.class public final Lcom/google/android/gms/games/ui/d/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:Z

.field c:[Ljava/lang/String;

.field d:Z

.field e:Z

.field final f:Lcom/google/android/gms/games/ui/d/z;

.field private g:Lcom/google/android/gms/games/Player;

.field private h:Ljava/util/ArrayList;

.field private final i:Landroid/support/v4/app/Fragment;

.field private final j:Landroid/app/Activity;

.field private final k:Ljava/lang/String;

.field private final l:Lcom/google/android/gms/common/api/v;

.field private final m:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/d/z;Landroid/app/Activity;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/w;->g:Lcom/google/android/gms/games/Player;

    .line 128
    iput-object p2, p0, Lcom/google/android/gms/games/ui/d/w;->f:Lcom/google/android/gms/games/ui/d/z;

    .line 129
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->i:Landroid/support/v4/app/Fragment;

    .line 130
    iput-object p3, p0, Lcom/google/android/gms/games/ui/d/w;->j:Landroid/app/Activity;

    .line 131
    iput-object p4, p0, Lcom/google/android/gms/games/ui/d/w;->l:Lcom/google/android/gms/common/api/v;

    .line 132
    iput-object p5, p0, Lcom/google/android/gms/games/ui/d/w;->k:Ljava/lang/String;

    .line 133
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/d/w;->m:I

    .line 134
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/d/z;Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/google/android/gms/games/ui/d/w;->g:Lcom/google/android/gms/games/Player;

    .line 115
    iput-object p2, p0, Lcom/google/android/gms/games/ui/d/w;->f:Lcom/google/android/gms/games/ui/d/z;

    .line 116
    iput-object p3, p0, Lcom/google/android/gms/games/ui/d/w;->i:Landroid/support/v4/app/Fragment;

    .line 117
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->j:Landroid/app/Activity;

    .line 118
    iput-object p4, p0, Lcom/google/android/gms/games/ui/d/w;->l:Lcom/google/android/gms/common/api/v;

    .line 119
    iput-object p5, p0, Lcom/google/android/gms/games/ui/d/w;->k:Ljava/lang/String;

    .line 120
    iput p6, p0, Lcom/google/android/gms/games/ui/d/w;->m:I

    .line 121
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/d/z;Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)Lcom/google/android/gms/games/ui/d/w;
    .locals 7

    .prologue
    .line 155
    new-instance v0, Lcom/google/android/gms/games/ui/d/w;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/w;-><init>(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/d/z;Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V

    .line 157
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/w;->a()V

    .line 158
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 196
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/w;->e:Z

    if-eqz v0, :cond_0

    .line 197
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Restarting a previously canceled ManageCirclesHelper instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 202
    :cond_0
    iput-object v4, p0, Lcom/google/android/gms/games/ui/d/w;->a:Ljava/util/ArrayList;

    .line 203
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/d/w;->b:Z

    .line 204
    iput-object v4, p0, Lcom/google/android/gms/games/ui/d/w;->c:[Ljava/lang/String;

    .line 205
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/d/w;->d:Z

    .line 206
    iput-object v4, p0, Lcom/google/android/gms/games/ui/d/w;->h:Ljava/util/ArrayList;

    .line 207
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/d/w;->e:Z

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->l:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 213
    const-string v0, "ManageCirclesHelper"

    const-string v1, "ManageCirclesHelper.start: PeopleClient not connected!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    :goto_0
    return-void

    .line 224
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/w;->l:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/w;->k:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v4, v4}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/d/x;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/d/x;-><init>(Lcom/google/android/gms/games/ui/d/w;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 235
    new-instance v0, Lcom/google/android/gms/people/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/h;-><init>()V

    .line 236
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 237
    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/w;->g:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/h;->a(Ljava/util/Collection;)Lcom/google/android/gms/people/h;

    .line 239
    sget-object v1, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/w;->l:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/w;->k:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/h;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/d/y;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/d/y;-><init>(Lcom/google/android/gms/games/ui/d/w;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 262
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/w;->e:Z

    if-eqz v0, :cond_0

    .line 265
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Canceling a previously canceled ManageCirclesHelper instance"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 268
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/d/w;->e:Z

    .line 269
    return-void
.end method

.method final c()V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 347
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/w;->e:Z

    if-eqz v0, :cond_1

    .line 348
    const-string v0, "ManageCirclesHelper"

    const-string v1, "computeBelongingCircles: Canceled! Bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 351
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/w;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/w;->d:Z

    if-eqz v0, :cond_0

    .line 360
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->h:Ljava/util/ArrayList;

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->c:[Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->c:[Ljava/lang/String;

    array-length v4, v0

    move v3, v1

    :goto_1
    if-ge v3, v4, :cond_4

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->c:[Ljava/lang/String;

    aget-object v5, v0, v3

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v1

    :goto_2
    if-ge v2, v6, :cond_3

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 368
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 369
    iget-object v7, p0, Lcom/google/android/gms/games/ui/d/w;->h:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 363
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 375
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/d/w;->e:Z

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->i:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->i:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/w;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/w;->g:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/w;->h:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/w;->i:Landroid/support/v4/app/Fragment;

    iget v2, p0, Lcom/google/android/gms/games/ui/d/w;->m:I

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->j:Landroid/app/Activity;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->j:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/w;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/d/w;->g:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/w;->h:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/w;->j:Landroid/app/Activity;

    iget v2, p0, Lcom/google/android/gms/games/ui/d/w;->m:I

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "mFragment and mActivity cannot both be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final d()V
    .locals 3

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->i:Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->i:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    :goto_0
    sget v1, Lcom/google/android/gms/p;->kf:I

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 406
    return-void

    .line 404
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/w;->j:Landroid/app/Activity;

    goto :goto_0
.end method
