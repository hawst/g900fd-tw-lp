.class public final Lcom/google/android/gms/people/service/a/ac;
.super Lcom/google/android/gms/people/service/a/b;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:I

.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 7

    .prologue
    .line 47
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/people/service/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;)V

    .line 49
    iput-object p3, p0, Lcom/google/android/gms/people/service/a/ac;->j:Ljava/lang/String;

    .line 50
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/ac;->g:Ljava/lang/String;

    .line 51
    iput-object p7, p0, Lcom/google/android/gms/people/service/a/ac;->h:Ljava/lang/String;

    .line 52
    iput-object p8, p0, Lcom/google/android/gms/people/service/a/ac;->i:Ljava/lang/String;

    .line 53
    move/from16 v0, p9

    iput v0, p0, Lcom/google/android/gms/people/service/a/ac;->k:I

    .line 54
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/ac;->l:Ljava/lang/String;

    .line 56
    return-void
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 20

    .prologue
    .line 61
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/e;

    move-result-object v3

    .line 62
    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/i;

    invoke-virtual {v3}, Lcom/google/android/gms/people/service/e;->a()Lcom/google/android/gms/people/service/g;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/i;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 64
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/people/service/a/ac;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/a/ac;->j:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/a/ac;->i:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/google/android/gms/people/service/a/ac;->k:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/people/service/a/ac;->h:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/people/service/a/ac;->l:Ljava/lang/String;

    invoke-virtual/range {v2 .. v8}, Lcom/google/android/gms/plus/service/v1whitelisted/i;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v17

    .line 73
    new-instance v18, Ljava/util/HashSet;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashSet;-><init>()V

    .line 74
    new-instance v19, Lcom/google/android/gms/people/model/k;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/a/ac;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/people/service/a/ac;->h:Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x1

    const/4 v9, 0x0

    const-wide/16 v10, 0x0

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/people/service/a/ac;->i:Ljava/lang/String;

    const/4 v13, 0x7

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v3, p1

    invoke-static/range {v3 .. v16}, Lcom/google/android/gms/people/service/a/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/people/internal/a/b;->a:Lcom/google/android/gms/people/internal/a/d;

    sget-object v4, Lcom/google/android/gms/people/internal/a/b;->b:Lcom/google/android/gms/people/internal/a/c;

    move-object/from16 v0, v19

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/people/model/k;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/people/internal/a/d;Lcom/google/android/gms/people/internal/a/c;)V

    .line 90
    const/4 v2, 0x0

    :goto_0
    :try_start_0
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/people/model/k;->c()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 91
    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/model/k;->b(I)Lcom/google/android/gms/people/model/j;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/people/model/j;->a()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 90
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 94
    :cond_0
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/people/model/k;->d()V

    .line 97
    sget-object v2, Lcom/google/android/gms/people/internal/bc;->c:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v9

    .line 99
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    .line 100
    const/4 v2, 0x0

    move v8, v2

    :goto_1
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v8, v2, :cond_3

    .line 101
    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v7, v2

    check-cast v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;

    .line 102
    invoke-interface {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 104
    move-object/from16 v0, v18

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 105
    invoke-interface {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v10, "person"

    invoke-interface {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->j()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v10, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    const/4 v7, 0x1

    :goto_2
    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/people/internal/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/util/HashMap;

    move-result-object v2

    invoke-virtual {v9, v2}, Lcom/google/android/gms/common/data/m;->a(Ljava/util/HashMap;)Lcom/google/android/gms/common/data/m;

    .line 100
    :cond_1
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_1

    .line 94
    :catchall_0
    move-exception v2

    invoke-virtual/range {v19 .. v19}, Lcom/google/android/gms/people/model/k;->d()V

    throw v2

    .line 105
    :cond_2
    const/4 v7, 0x2

    goto :goto_2

    .line 131
    :cond_3
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 133
    const-string v4, "pageToken"

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_4

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v2

    :goto_3
    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/people/service/a/ac;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/people/service/a/ac;->h:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2, v4, v3}, Lcom/google/android/gms/people/service/a/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 137
    invoke-virtual {v9, v3}, Lcom/google/android/gms/common/data/m;->a(Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    return-object v2

    .line 133
    :cond_4
    const/4 v2, 0x0

    goto :goto_3
.end method
