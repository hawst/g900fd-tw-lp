.class public final Lcom/google/android/gms/wearable/node/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/wearable/node/bi;Ljava/io/InputStream;Lcom/google/android/gms/wearable/c/n;Lcom/google/android/gms/wearable/node/be;)I
    .locals 3

    .prologue
    const/4 v1, 0x4

    const/4 v2, 0x0

    .line 93
    invoke-virtual {p3, v2}, Lcom/google/android/gms/wearable/node/be;->b(Z)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wearable/node/bh;->a(Ljava/io/InputStream;[BI)V

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 95
    invoke-static {p0, v0}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/node/bi;I)V

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/wearable/node/bh;->a(Ljava/io/InputStream;[BI)V

    .line 97
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    invoke-static {p2, v1, v2, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[BII)Lcom/google/protobuf/nano/j;

    .line 98
    add-int/lit8 v0, v0, 0x4

    .line 99
    const/4 v1, 0x1

    invoke-virtual {p3, v1}, Lcom/google/android/gms/wearable/node/be;->b(Z)V

    .line 100
    invoke-virtual {p3, v0}, Lcom/google/android/gms/wearable/node/be;->b(I)V

    .line 101
    return v0
.end method

.method public static a(Lcom/google/android/gms/wearable/node/bi;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/c/n;Lcom/google/android/gms/wearable/node/be;)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x2

    .line 118
    const/4 v0, 0x1

    invoke-virtual {p3, v0}, Lcom/google/android/gms/wearable/node/be;->a(Z)V

    .line 119
    invoke-virtual {p2}, Lcom/google/android/gms/wearable/c/n;->getSerializedSize()I

    move-result v0

    .line 120
    invoke-static {p0, v0}, Lcom/google/android/gms/wearable/node/bh;->a(Lcom/google/android/gms/wearable/node/bi;I)V

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    invoke-static {p2, v1, v5, v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;[BII)V

    .line 122
    const-string v1, "wearable"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "WearableVerbose"

    invoke-static {v1, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 124
    :cond_0
    const-string v1, "wearable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sending message of length "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bi;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v3

    invoke-virtual {p1, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    invoke-virtual {p1, v1, v5, v0}, Ljava/io/OutputStream;->write([BII)V

    .line 128
    add-int/lit8 v1, v0, 0x4

    .line 129
    invoke-virtual {p3, v5}, Lcom/google/android/gms/wearable/node/be;->a(Z)V

    .line 130
    invoke-virtual {p3, v1}, Lcom/google/android/gms/wearable/node/be;->a(I)V

    .line 131
    const-string v2, "wearable"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "WearableVerbose"

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 133
    :cond_2
    const-string v2, "wearable"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sending message of length "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " is done"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_3
    return v1
.end method

.method public static a(Lcom/google/android/gms/wearable/c/n;)Lcom/google/android/gms/wearable/c/m;
    .locals 4

    .prologue
    .line 141
    new-instance v0, Lcom/google/android/gms/wearable/c/m;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/c/m;-><init>()V

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/wearable/c/n;->a:[B

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/wearable/c/n;->a:[B

    array-length v3, v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wearable/c/m;->mergeFrom(Lcom/google/protobuf/nano/j;[BII)Lcom/google/protobuf/nano/j;

    .line 143
    return-object v0
.end method

.method public static a(Ljava/util/List;)Lcom/google/android/gms/wearable/c/m;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 160
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/c/n;

    iget-object v3, v0, Lcom/google/android/gms/wearable/c/n;->b:Ljava/lang/String;

    .line 162
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/c/n;

    .line 163
    iget-object v0, v0, Lcom/google/android/gms/wearable/c/n;->a:[B

    array-length v0, v0

    add-int/2addr v0, v1

    move v1, v0

    .line 164
    goto :goto_0

    .line 166
    :cond_0
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 167
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 168
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/c/n;

    .line 169
    iget-object v0, v0, Lcom/google/android/gms/wearable/c/n;->a:[B

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 167
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 171
    :cond_1
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/node/o;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 172
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 173
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "The computed message digest should match the received digest: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 176
    :cond_2
    new-instance v0, Lcom/google/android/gms/wearable/c/m;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/c/m;-><init>()V

    .line 177
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    .line 178
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wearable/c/m;)Lcom/google/android/gms/wearable/c/n;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 188
    new-instance v0, Lcom/google/android/gms/wearable/c/n;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/c/n;-><init>()V

    .line 189
    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/wearable/c/n;->a:[B

    .line 190
    iget-object v1, v0, Lcom/google/android/gms/wearable/c/n;->a:[B

    invoke-static {v1}, Lcom/google/android/gms/wearable/node/o;->a([B)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/wearable/c/n;->b:Ljava/lang/String;

    .line 191
    iput v2, v0, Lcom/google/android/gms/wearable/c/n;->c:I

    .line 192
    iput v2, v0, Lcom/google/android/gms/wearable/c/n;->d:I

    .line 193
    return-object v0
.end method

.method public static a()Lcom/google/android/gms/wearable/node/bi;
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/wearable/node/bi;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/wearable/node/bi;-><init>(B)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wearable/c/m;I)Ljava/util/List;
    .locals 8

    .prologue
    .line 206
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 207
    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v3

    .line 208
    invoke-static {v3}, Lcom/google/android/gms/wearable/node/o;->a([B)Ljava/lang/String;

    move-result-object v4

    .line 210
    const/4 v0, 0x0

    :goto_0
    mul-int v1, v0, p1

    array-length v5, v3

    if-ge v1, v5, :cond_1

    .line 211
    new-instance v5, Lcom/google/android/gms/wearable/c/n;

    invoke-direct {v5}, Lcom/google/android/gms/wearable/c/n;-><init>()V

    .line 212
    add-int/lit8 v1, v0, 0x1

    iput v1, v5, Lcom/google/android/gms/wearable/c/n;->c:I

    .line 213
    iput-object v4, v5, Lcom/google/android/gms/wearable/c/n;->b:Ljava/lang/String;

    .line 214
    mul-int v6, v0, p1

    .line 215
    add-int v1, v6, p1

    .line 216
    array-length v7, v3

    if-le v1, v7, :cond_0

    array-length v1, v3

    .line 217
    :cond_0
    invoke-static {v3, v6, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v1

    iput-object v1, v5, Lcom/google/android/gms/wearable/c/n;->a:[B

    .line 218
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/c/n;

    .line 222
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    iput v3, v0, Lcom/google/android/gms/wearable/c/n;->d:I

    goto :goto_1

    .line 224
    :cond_2
    return-object v2
.end method

.method private static a(Lcom/google/android/gms/wearable/node/bi;I)V
    .locals 4

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    array-length v0, v0

    if-le p1, v0, :cond_2

    .line 106
    :cond_0
    int-to-float v0, p1

    const v1, 0x3f8ccccd    # 1.1f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    if-eqz v1, :cond_1

    .line 108
    const-string v1, "wearable"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ensureBuffersHaveSpace: increasing size from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    array-length v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_1
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bi;->b:[B

    .line 113
    :cond_2
    return-void
.end method

.method private static a(Ljava/io/InputStream;[BI)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 76
    const/4 v0, 0x0

    .line 77
    :goto_0
    if-ge v0, p2, :cond_1

    .line 78
    sub-int v1, p2, v0

    invoke-virtual {p0, p1, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 79
    if-gtz v1, :cond_0

    .line 80
    new-instance v0, Ljava/io/IOException;

    const-string v1, "connection closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    add-int/2addr v0, v1

    .line 83
    goto :goto_0

    .line 84
    :cond_1
    const-string v0, "wearable"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "WearableVerbose"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 86
    :cond_2
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "reading message of length "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is done."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_3
    return-void
.end method

.method public static b(Lcom/google/android/gms/wearable/c/n;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 228
    const-string v0, "MessagePiece: queueId=%d digest=%s len=%d piece=%d of %d"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/gms/wearable/c/n;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/wearable/c/n;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/wearable/c/n;->a:[B

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/gms/wearable/c/n;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/google/android/gms/wearable/c/n;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
