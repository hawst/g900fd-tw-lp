.class final Lcom/google/android/gms/cast/c/b;
.super Lcom/google/android/gms/cast/e/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/c/a;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/c/a;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    .line 141
    invoke-static {p1}, Lcom/google/android/gms/cast/c/a;->a(Lcom/google/android/gms/cast/c/a;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "FilterRequest"

    invoke-static {}, Lcom/google/android/gms/cast/c/a;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gms/cast/c/a;->b(Lcom/google/android/gms/cast/c/a;)Landroid/os/Handler;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/cast/e/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 142
    iput-object p2, p0, Lcom/google/android/gms/cast/c/b;->d:Ljava/lang/String;

    .line 143
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/a;->d(Lcom/google/android/gms/cast/c/a;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1

    .line 182
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/a;->d(Lcom/google/android/gms/cast/c/a;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/a;->d(Lcom/google/android/gms/cast/c/a;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/a;->e(Lcom/google/android/gms/cast/c/a;)V

    .line 186
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final a()Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 147
    new-instance v0, Lcom/google/i/a/a/b/c;

    invoke-direct {v0}, Lcom/google/i/a/a/b/c;-><init>()V

    .line 148
    iget-object v1, p0, Lcom/google/android/gms/cast/c/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/i/a/a/b/c;->a(Ljava/lang/String;)Lcom/google/i/a/a/b/c;

    .line 149
    return-object v0
.end method

.method protected final a(I)V
    .locals 5

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/cast/c/b;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onError: %s %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/cast/c/b;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 177
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/b;->b()V

    .line 178
    return-void
.end method

.method protected final a([B)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/cast/c/b;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onSuccessResponse: %s"

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/b;->d:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 157
    :try_start_0
    new-instance v0, Lcom/google/i/a/a/b/d;

    invoke-direct {v0}, Lcom/google/i/a/a/b/d;-><init>()V

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/a/b/d;

    .line 158
    iget-boolean v0, v0, Lcom/google/i/a/a/b/d;->a:Z

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/cast/c/b;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "onSuccessResponse: %s %b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/cast/c/b;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/a;->c(Lcom/google/android/gms/cast/c/a;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 161
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    invoke-static {v2}, Lcom/google/android/gms/cast/c/a;->c(Lcom/google/android/gms/cast/c/a;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/c/b;->d:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/cast/c/c;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-direct {v4, v0, v6, v7}, Lcom/google/android/gms/cast/c/c;-><init>(ZJ)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 164
    if-eqz v0, :cond_0

    .line 165
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/b;->a:Lcom/google/android/gms/cast/c/a;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/b;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/cast/c/a;->a(Lcom/google/android/gms/cast/c/a;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/google/protobuf/a/e; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 170
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/b;->b()V

    .line 171
    :goto_0
    return-void

    .line 163
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catch Lcom/google/protobuf/a/e; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 167
    :catch_0
    move-exception v0

    .line 168
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/cast/c/b;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Unable to parse response data"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 170
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/b;->b()V

    goto :goto_0

    :catchall_1
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/cast/c/b;->b()V

    throw v0
.end method
