.class public final Lcom/google/android/gms/drive/database/model/am;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JJ)V
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 28
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/am;->a:J

    .line 29
    iput-wide p4, p0, Lcom/google/android/gms/drive/database/model/am;->b:J

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 51
    sget-object v0, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/am;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 52
    sget-object v0, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/am;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 53
    return-void
.end method
