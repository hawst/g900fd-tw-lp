.class final Lcom/google/android/gms/plus/sharebox/bd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/people/w;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/au;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/sharebox/au;)V
    .locals 0

    .prologue
    .line 376
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/sharebox/au;B)V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/bd;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->u(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    move-result-object v0

    if-nez v0, :cond_3

    .line 408
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/j;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->v(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 416
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/PlusPage;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/PlusPage;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/plus/sharebox/au;Ljava/lang/String;)Ljava/lang/String;

    .line 420
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->w(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 421
    sget-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->x(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 430
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 431
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h()Z

    move-result v0

    if-nez v0, :cond_5

    .line 432
    sget-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/people/d;

    invoke-direct {v4}, Lcom/google/android/gms/people/d;-><init>()V

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/d;->a(Ljava/lang/String;)Lcom/google/android/gms/people/d;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/d;->a(Z)Lcom/google/android/gms/people/d;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v5}, Lcom/google/android/gms/people/d;->a(I)Lcom/google/android/gms/people/d;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->A(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 444
    :cond_2
    :goto_2
    return-void

    .line 412
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->u(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V

    goto/16 :goto_0

    .line 424
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->y(Lcom/google/android/gms/plus/sharebox/au;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    .line 425
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->w(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, v4, v4}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->z(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto/16 :goto_1

    .line 439
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 440
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/bf;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    goto :goto_2
.end method


# virtual methods
.method public final Z_()V
    .locals 0

    .prologue
    .line 457
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/bd;->a()V

    .line 458
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 381
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 385
    sget-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    move-object v2, p0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/v;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;

    .line 388
    sget-object v0, Lcom/google/android/gms/people/x;->h:Lcom/google/android/gms/people/ai;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v4}, Lcom/google/android/gms/plus/sharebox/au;->m(Lcom/google/android/gms/plus/sharebox/au;)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/ai;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/gms/common/api/am;

    .line 391
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/bd;->a()V

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->n(Lcom/google/android/gms/plus/sharebox/au;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->o(Lcom/google/android/gms/plus/sharebox/au;)Z

    .line 395
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v4}, Lcom/google/android/gms/plus/sharebox/au;->q(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->p(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 400
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->r(Lcom/google/android/gms/plus/sharebox/au;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->s(Lcom/google/android/gms/plus/sharebox/au;)Z

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->t(Lcom/google/android/gms/plus/sharebox/au;)V

    .line 404
    :cond_1
    return-void
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->w(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->y(Lcom/google/android/gms/plus/sharebox/au;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/bd;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 453
    :cond_1
    return-void
.end method
