.class public final Lcom/google/android/gms/plus/audience/bg;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/common/people/data/Audience;

.field private final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bg;->b:Ljava/util/Set;

    .line 24
    const-string v0, "Audience must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 40
    const-string v0, "Audience must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bg;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/bh;

    invoke-interface {v0, p2}, Lcom/google/android/gms/plus/audience/bh;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 42
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/audience/bh;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bg;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 47
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/audience/bh;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/bg;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 52
    return-void
.end method
