.class public final Lcom/google/android/gms/fitness/settings/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/gms/fitness/d/h;

.field private c:Lcom/google/android/gms/fitness/apiary/lso/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/d/h;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lcom/google/android/gms/fitness/settings/i;->b:Lcom/google/android/gms/fitness/d/h;

    .line 43
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/i;->a:Landroid/content/Context;

    .line 44
    return-void
.end method

.method private declared-synchronized a()Lcom/google/android/gms/fitness/apiary/lso/f;
    .locals 9

    .prologue
    .line 152
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/i;->c:Lcom/google/android/gms/fitness/apiary/lso/f;

    if-nez v0, :cond_0

    .line 153
    new-instance v8, Lcom/google/android/gms/fitness/apiary/lso/f;

    new-instance v0, Lcom/google/android/gms/common/server/n;

    iget-object v1, p0, Lcom/google/android/gms/fitness/settings/i;->a:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/common/a/b;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/a/b;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->aO:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v5, Lcom/google/android/gms/fitness/g/c;->aP:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/fitness/g/c;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/common/a/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v7}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    invoke-direct {v8, v0}, Lcom/google/android/gms/fitness/apiary/lso/f;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v8, p0, Lcom/google/android/gms/fitness/settings/i;->c:Lcom/google/android/gms/fitness/apiary/lso/f;

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/i;->c:Lcom/google/android/gms/fitness/apiary/lso/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 152
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 52
    new-instance v1, Lcom/google/android/gms/fitness/settings/l;

    invoke-direct {v1}, Lcom/google/android/gms/fitness/settings/l;-><init>()V

    .line 53
    invoke-virtual {p0, p1}, Lcom/google/android/gms/fitness/settings/i;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/fitness/settings/l;->a:Ljava/lang/String;

    .line 54
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 58
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/i;->a()Lcom/google/android/gms/fitness/apiary/lso/f;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/fitness/settings/i;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/fitness/settings/i;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v5

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v5, p1, p1, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "https://www.googleapis.com/auth/grants.audit"

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/gms/fitness/apiary/lso/f;->a:Lcom/google/android/gms/fitness/apiary/lso/e;

    const-string v4, "third_party"

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    if-nez v3, :cond_4

    :cond_0
    :goto_0
    const-string v3, "authapps"

    const-string v5, "category"

    invoke-static {v4}, Lcom/google/android/gms/fitness/apiary/lso/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v5, v4}, Lcom/google/android/gms/fitness/apiary/lso/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_1

    const-string v4, "hl"

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/lso/e;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/fitness/apiary/lso/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/fitness/apiary/lso/e;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/fitness/apiary/lso/AuthApps;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps;->c()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps;->b()Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 64
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;

    .line 66
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 67
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 68
    const v0, 0xdea8

    if-gt v0, v5, :cond_7

    const v0, 0xdf0b

    if-gt v5, v0, :cond_7

    move v0, v7

    :goto_3
    if-eqz v0, :cond_3

    .line 69
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 58
    :cond_4
    :try_start_1
    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, ""

    :goto_4
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    const-string v0, "-"

    goto :goto_4

    :cond_6
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_1

    .line 60
    :catch_0
    move-exception v0

    .line 61
    :goto_5
    new-instance v1, Lcom/google/android/gms/fitness/settings/d;

    invoke-direct {v1, v0}, Lcom/google/android/gms/fitness/settings/d;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_7
    move v0, v6

    .line 68
    goto :goto_3

    .line 72
    :cond_8
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 73
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v5, v0, [I

    move v2, v6

    .line 76
    :goto_6
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_9

    .line 77
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v5, v2

    .line 76
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 80
    :cond_9
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_a
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;

    .line 81
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->c()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 82
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps$Clients;->b()Ljava/lang/String;

    move-result-object v0

    .line 83
    const-string v4, "Connected app: %s"

    new-array v9, v7, [Ljava/lang/Object;

    aput-object v0, v9, v6

    invoke-static {v4, v9}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 84
    new-instance v4, Lcom/google/android/gms/fitness/settings/ConnectedApp;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/apiary/lso/AuthApps$Apps;->c()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v4, v0, v9, v5}, Lcom/google/android/gms/fitness/settings/ConnectedApp;-><init>(Ljava/lang/String;Ljava/lang/String;[I)V

    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    .line 88
    :cond_b
    return-object v8

    .line 60
    :catch_1
    move-exception v0

    goto :goto_5
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 169
    const-string v1, "Querying Auth2 token for %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 172
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/settings/i;->a:Landroid/content/Context;

    const-string v2, "oauth2:https://www.googleapis.com/auth/grants.audit"

    const/4 v3, 0x0

    invoke-static {v1, p1, v2, v3}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 187
    const-string v1, "token = %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 188
    :goto_0
    return-object v0

    .line 177
    :catch_0
    move-exception v1

    .line 178
    const-string v2, "UserRecoverableAuthException: %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 180
    :catch_1
    move-exception v1

    .line 181
    const-string v2, "UserRecoverableAuthException: %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 183
    :catch_2
    move-exception v1

    .line 184
    const-string v2, "UserRecoverableAuthException: %s"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {v1}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
