.class public final Lcom/google/android/gms/smart_profile/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/smart_profile/ad;


# instance fields
.field a:Lcom/google/android/gms/common/api/v;

.field b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

.field c:Lcom/google/android/gms/plus/internal/ab;

.field private final d:Landroid/support/v4/app/Fragment;

.field private final e:Lcom/google/android/gms/smart_profile/e;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:I

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/smart_profile/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/d;->d:Landroid/support/v4/app/Fragment;

    .line 81
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/d;->e:Lcom/google/android/gms/smart_profile/e;

    .line 82
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/d;->f:Ljava/lang/String;

    .line 83
    iput-object p4, p0, Lcom/google/android/gms/smart_profile/d;->g:Ljava/lang/String;

    .line 84
    iput-object p5, p0, Lcom/google/android/gms/smart_profile/d;->h:Ljava/lang/String;

    .line 85
    iput p6, p0, Lcom/google/android/gms/smart_profile/d;->i:I

    .line 87
    if-eqz p7, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/os/Bundle;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/internal/cn;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/d;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v2}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/d;->f:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/plus.native"

    aput-object v3, v2, v4

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/plus/internal/cn;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/smart_profile/g;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/smart_profile/g;-><init>(Lcom/google/android/gms/smart_profile/d;B)V

    sget-object v2, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/d;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-interface {v2, v3, v0, v1, v1}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    .line 90
    :cond_0
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    iget v1, p0, Lcom/google/android/gms/smart_profile/d;->i:I

    iput v1, v0, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/smart_profile/f;

    invoke-direct {v1, p0, v4}, Lcom/google/android/gms/smart_profile/f;-><init>(Lcom/google/android/gms/smart_profile/d;B)V

    new-instance v2, Lcom/google/android/gms/common/api/w;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/d;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/d;->f:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/d;->a:Lcom/google/android/gms/common/api/v;

    .line 91
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/d;->a:Lcom/google/android/gms/common/api/v;

    new-instance v0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/d;->d:Landroid/support/v4/app/Fragment;

    invoke-virtual {v1}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/d;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/d;->g:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/d;->h:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/gms/smart_profile/d;->i:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/ad;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/d;->b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    .line 92
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 1

    .prologue
    .line 130
    iput-boolean p1, p0, Lcom/google/android/gms/smart_profile/d;->j:Z

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/d;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/d;->j:Z

    if-eqz v0, :cond_2

    .line 136
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/d;->d()V

    .line 138
    :cond_2
    return-void
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/d;->j:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 167
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/d;->j:Z

    if-nez v0, :cond_1

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a()V

    .line 170
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 177
    :cond_0
    return-void
.end method

.method final d()V
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->e:Lcom/google/android/gms/smart_profile/e;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/d;->e:Lcom/google/android/gms/smart_profile/e;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/e;->ai_()V

    .line 183
    :cond_0
    return-void
.end method
