.class public final Lcom/google/android/gms/audiomodem/aj;
.super Lcom/google/android/gms/audiomodem/ah;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;

.field private b:Ljava/io/BufferedOutputStream;

.field private c:I


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/io/File;I)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/audiomodem/ah;-><init>(Ljava/io/File;I)V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/aj;->a:Landroid/content/Context;

    .line 25
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/audiomodem/bo;Ljava/lang/String;)Ljava/io/File;
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    const/4 v8, 0x6

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    if-nez v1, :cond_1

    .line 78
    const-string v1, "audioModem"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "DiagnosticWavFileWriterwriteSavedBytes called before tmp file initialized"

    const-string v2, "audioModem"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    :cond_0
    :goto_0
    return-object v0

    .line 83
    :cond_1
    iget v1, p0, Lcom/google/android/gms/audiomodem/aj;->c:I

    invoke-virtual {p0, p2, v1}, Lcom/google/android/gms/audiomodem/aj;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 84
    if-eqz v1, :cond_0

    .line 92
    const/16 v3, 0x2000

    new-array v5, v3, [B

    .line 94
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :try_start_1
    iget v3, p0, Lcom/google/android/gms/audiomodem/aj;->c:I

    const-string v6, "RIFF"

    invoke-static {v4, v6}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    add-int/lit8 v6, v3, 0x24

    const/4 v7, 0x4

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    const-string v6, "WAVE"

    invoke-static {v4, v6}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    const-string v6, "fmt "

    invoke-static {v4, v6}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    const/16 v6, 0x10

    const/4 v7, 0x4

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    const/4 v6, 0x1

    const/4 v7, 0x2

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    iget v6, p1, Lcom/google/android/gms/audiomodem/bo;->b:I

    const/4 v7, 0x2

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    iget v6, p1, Lcom/google/android/gms/audiomodem/bo;->a:I

    const/4 v7, 0x4

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    iget v6, p1, Lcom/google/android/gms/audiomodem/bo;->b:I

    iget v7, p1, Lcom/google/android/gms/audiomodem/bo;->a:I

    mul-int/2addr v6, v7

    iget v7, p1, Lcom/google/android/gms/audiomodem/bo;->c:I

    div-int/lit8 v7, v7, 0x8

    mul-int/2addr v6, v7

    const/4 v7, 0x4

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    iget v6, p1, Lcom/google/android/gms/audiomodem/bo;->b:I

    iget v7, p1, Lcom/google/android/gms/audiomodem/bo;->c:I

    div-int/lit8 v7, v7, 0x8

    mul-int/2addr v6, v7

    const/4 v7, 0x2

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    iget v6, p1, Lcom/google/android/gms/audiomodem/bo;->c:I

    const/4 v7, 0x2

    invoke-static {v4, v6, v7}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    const-string v6, "data"

    invoke-static {v4, v6}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;Ljava/lang/String;)V

    const/4 v6, 0x4

    invoke-static {v4, v3, v6}, Lcom/google/android/gms/audiomodem/bo;->a(Ljava/io/OutputStream;II)V

    .line 96
    iget-object v3, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    invoke-virtual {v3}, Ljava/io/BufferedOutputStream;->flush()V

    .line 97
    new-instance v3, Ljava/io/BufferedInputStream;

    iget-object v6, p0, Lcom/google/android/gms/audiomodem/aj;->a:Landroid/content/Context;

    const-string v7, "tmpData"

    invoke-virtual {v6, v7}, Landroid/content/Context;->openFileInput(Ljava/lang/String;)Ljava/io/FileInputStream;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_8
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 99
    :goto_1
    :try_start_2
    invoke-virtual {v3, v5}, Ljava/io/BufferedInputStream;->read([B)I

    move-result v6

    .line 100
    const/4 v7, -0x1

    if-eq v6, v7, :cond_5

    .line 101
    add-int/2addr v2, v6

    .line 104
    const/4 v7, 0x0

    invoke-virtual {v4, v5, v7, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    .line 112
    :catch_0
    move-exception v2

    .line 113
    :goto_2
    :try_start_3
    const-string v5, "audioModem"

    const/4 v6, 0x6

    invoke-static {v5, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 114
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "DiagnosticWavFileWriterFailed to write "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "audioModem"

    invoke-static {v6, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 118
    :cond_2
    if-eqz v4, :cond_3

    .line 119
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 121
    :cond_3
    const-string v2, "audioModem"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "DiagnosticWavFileWriterWrote "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audioModem"

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 130
    :cond_4
    :goto_3
    if-eqz v3, :cond_0

    .line 131
    :try_start_5
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_0

    .line 133
    :catch_1
    move-exception v1

    .line 134
    const-string v2, "audioModem"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    const-string v2, "DiagnosticWavFileWriterUnable to close tmpDataInputStream"

    const-string v3, "audioModem"

    invoke-static {v3, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 106
    :cond_5
    :try_start_6
    iget v5, p0, Lcom/google/android/gms/audiomodem/aj;->c:I

    if-eq v2, v5, :cond_6

    .line 107
    const-string v2, "audioModem"

    const/4 v5, 0x5

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 108
    const-string v2, "DiagnosticWavFileWriterMismatch in no. of bytes for wav file."

    const-string v5, "audioModem"

    invoke-static {v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 118
    :cond_6
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 121
    const-string v0, "audioModem"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "DiagnosticWavFileWriterWrote "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "audioModem"

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 130
    :cond_7
    :goto_4
    :try_start_8
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_8
    :goto_5
    move-object v0, v1

    .line 137
    goto/16 :goto_0

    .line 124
    :catch_2
    move-exception v0

    .line 125
    const-string v2, "audioModem"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 126
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "DiagnosticWavFileWriterUnable to close "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audioModem"

    invoke-static {v4, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 133
    :catch_3
    move-exception v0

    .line 134
    const-string v2, "audioModem"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 135
    const-string v2, "DiagnosticWavFileWriterUnable to close tmpDataInputStream"

    const-string v3, "audioModem"

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_5

    .line 124
    :catch_4
    move-exception v2

    .line 125
    const-string v4, "audioModem"

    invoke-static {v4, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 126
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DiagnosticWavFileWriterUnable to close "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "audioModem"

    invoke-static {v4, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_3

    .line 117
    :catchall_0
    move-exception v2

    move-object v3, v0

    move-object v4, v0

    move-object v0, v2

    .line 118
    :goto_6
    if-eqz v4, :cond_9

    .line 119
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V

    .line 121
    :cond_9
    const-string v2, "audioModem"

    const/4 v4, 0x4

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 122
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "DiagnosticWavFileWriterWrote "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v4, "audioModem"

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    .line 130
    :cond_a
    :goto_7
    if-eqz v3, :cond_b

    .line 131
    :try_start_a
    invoke-virtual {v3}, Ljava/io/BufferedInputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_6

    .line 137
    :cond_b
    :goto_8
    throw v0

    .line 124
    :catch_5
    move-exception v2

    .line 125
    const-string v4, "audioModem"

    invoke-static {v4, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_a

    .line 126
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "DiagnosticWavFileWriterUnable to close "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "audioModem"

    invoke-static {v4, v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_7

    .line 133
    :catch_6
    move-exception v1

    .line 134
    const-string v2, "audioModem"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 135
    const-string v2, "DiagnosticWavFileWriterUnable to close tmpDataInputStream"

    const-string v3, "audioModem"

    invoke-static {v3, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    .line 117
    :catchall_1
    move-exception v2

    move-object v3, v0

    move-object v0, v2

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    .line 112
    :catch_7
    move-exception v2

    move-object v3, v0

    move-object v4, v0

    goto/16 :goto_2

    :catch_8
    move-exception v2

    move-object v3, v0

    goto/16 :goto_2
.end method

.method final a()V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    if-eqz v0, :cond_1

    .line 171
    const-string v0, "audioModem"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "DiagnosticWavFileWriterstartSavingBytes called after already initialized; reintializing"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/audiomodem/aj;->b()V

    .line 178
    :cond_1
    :try_start_0
    new-instance v0, Ljava/io/BufferedOutputStream;

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/aj;->a:Landroid/content/Context;

    const-string v2, "tmpData"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    .line 180
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/audiomodem/aj;->c:I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 186
    :cond_2
    :goto_0
    return-void

    .line 181
    :catch_0
    move-exception v0

    .line 182
    const-string v1, "audioModem"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 183
    const-string v1, "DiagnosticWavFileWriterTmp file not found"

    const-string v2, "audioModem"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method final a([BI)V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    if-nez v0, :cond_1

    .line 147
    const-string v0, "audioModem"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const-string v0, "DiagnosticWavFileWritersaveBytes called before tmp file initialized"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 153
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Ljava/io/BufferedOutputStream;->write([BII)V

    .line 154
    iget v0, p0, Lcom/google/android/gms/audiomodem/aj;->c:I

    add-int/2addr v0, p2

    iput v0, p0, Lcom/google/android/gms/audiomodem/aj;->c:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    const-string v1, "audioModem"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 157
    const-string v1, "DiagnosticWavFileWriterUnable to write to output stream"

    const-string v2, "audioModem"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method final b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    if-nez v0, :cond_1

    .line 193
    const-string v0, "audioModem"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 194
    const-string v0, "DiagnosticWavFileWriterstopSavingBytes called before tmp file initialized"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    :cond_0
    :goto_0
    return-void

    .line 199
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    iput-object v4, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    .line 206
    iput v3, p0, Lcom/google/android/gms/audiomodem/aj;->c:I

    goto :goto_0

    .line 200
    :catch_0
    move-exception v0

    .line 201
    :try_start_1
    const-string v1, "audioModem"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 202
    const-string v1, "DiagnosticWavFileWriterCould not close tmp output stream"

    const-string v2, "audioModem"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    :cond_2
    iput-object v4, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    .line 206
    iput v3, p0, Lcom/google/android/gms/audiomodem/aj;->c:I

    goto :goto_0

    .line 205
    :catchall_0
    move-exception v0

    iput-object v4, p0, Lcom/google/android/gms/audiomodem/aj;->b:Ljava/io/BufferedOutputStream;

    .line 206
    iput v3, p0, Lcom/google/android/gms/audiomodem/aj;->c:I

    throw v0
.end method
