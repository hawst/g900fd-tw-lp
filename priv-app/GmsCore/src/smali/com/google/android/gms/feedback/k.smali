.class final Lcom/google/android/gms/feedback/k;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/feedback/ErrorReport;

.field final synthetic b:Lcom/google/android/gms/feedback/Screenshot;

.field final synthetic c:I

.field final synthetic d:Landroid/widget/ProgressBar;

.field final synthetic e:Landroid/widget/LinearLayout;

.field final synthetic f:Landroid/widget/ImageView;

.field final synthetic g:Landroid/view/View;

.field final synthetic h:Landroid/widget/TextView;

.field final synthetic i:Lcom/google/android/gms/feedback/FeedbackActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/feedback/FeedbackActivity;Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;ILandroid/widget/ProgressBar;Landroid/widget/LinearLayout;Landroid/widget/ImageView;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/gms/feedback/k;->i:Lcom/google/android/gms/feedback/FeedbackActivity;

    iput-object p2, p0, Lcom/google/android/gms/feedback/k;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iput-object p3, p0, Lcom/google/android/gms/feedback/k;->b:Lcom/google/android/gms/feedback/Screenshot;

    iput p4, p0, Lcom/google/android/gms/feedback/k;->c:I

    iput-object p5, p0, Lcom/google/android/gms/feedback/k;->d:Landroid/widget/ProgressBar;

    iput-object p6, p0, Lcom/google/android/gms/feedback/k;->e:Landroid/widget/LinearLayout;

    iput-object p7, p0, Lcom/google/android/gms/feedback/k;->f:Landroid/widget/ImageView;

    iput-object p8, p0, Lcom/google/android/gms/feedback/k;->g:Landroid/view/View;

    iput-object p9, p0, Lcom/google/android/gms/feedback/k;->h:Landroid/widget/TextView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 278
    iget-object v2, p0, Lcom/google/android/gms/feedback/k;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/feedback/k;->b:Lcom/google/android/gms/feedback/Screenshot;

    iget v2, v2, Lcom/google/android/gms/feedback/Screenshot;->a:I

    iget v3, p0, Lcom/google/android/gms/feedback/k;->c:I

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_5

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/feedback/k;->b:Lcom/google/android/gms/feedback/Screenshot;

    iget-object v2, v2, Lcom/google/android/gms/feedback/Screenshot;->c:Ljava/lang/String;

    invoke-static {v2, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    :goto_1
    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->h()Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->h()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/Screenshot;->a([BZ)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/google/android/gms/feedback/k;->i:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/feedback/k;->i:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v2}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/feedback/k;->i:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/Screenshot;->a([BZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackActivity;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/feedback/k;->i:Lcom/google/android/gms/feedback/FeedbackActivity;

    invoke-static {v0}, Lcom/google/android/gms/feedback/FeedbackActivity;->a(Lcom/google/android/gms/feedback/FeedbackActivity;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/feedback/k;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget v2, v2, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    iget v3, p0, Lcom/google/android/gms/feedback/k;->c:I

    div-int/lit8 v3, v3, 0x2

    if-lt v2, v3, :cond_4

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/feedback/k;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, v1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 278
    check-cast p1, Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/google/android/gms/feedback/k;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/k;->e:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/k;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/k;->e:Landroid/widget/LinearLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/k;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/k;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method
