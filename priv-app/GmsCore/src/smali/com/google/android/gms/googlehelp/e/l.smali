.class public final Lcom/google/android/gms/googlehelp/e/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 206
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/l;->cachedSize:I

    .line 207
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 266
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 267
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 268
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 271
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 272
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 218
    if-ne p1, p0, :cond_1

    .line 239
    :cond_0
    :goto_0
    return v0

    .line 221
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 222
    goto :goto_0

    .line 224
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/l;

    .line 225
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 226
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 227
    goto :goto_0

    .line 229
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 230
    goto :goto_0

    .line 232
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 233
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 234
    goto :goto_0

    .line 236
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 237
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 247
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 249
    return v0

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 247
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 182
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/l;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 259
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/l;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 261
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 262
    return-void
.end method
