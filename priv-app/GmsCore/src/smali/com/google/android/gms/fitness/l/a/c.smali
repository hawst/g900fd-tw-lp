.class final Lcom/google/android/gms/fitness/l/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/l/a/a;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/l/a/b;

.field private final b:Lcom/google/android/gms/fitness/l/a/d;

.field private final c:Ljava/lang/String;

.field private d:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/l/a/b;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p2, p0, Lcom/google/android/gms/fitness/l/a/c;->c:Ljava/lang/String;

    .line 92
    new-instance v0, Lcom/google/android/gms/fitness/l/a/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/l/a/d;-><init>(Lcom/google/android/gms/fitness/l/a/c;B)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->b:Lcom/google/android/gms/fitness/l/a/d;

    .line 93
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->d:Ljava/util/concurrent/atomic/AtomicLong;

    .line 94
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/l/a/b;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/l/a/c;-><init>(Lcom/google/android/gms/fitness/l/a/b;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v1, v0, Lcom/google/android/gms/fitness/l/a/b;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->a:Ljava/util/concurrent/ConcurrentMap;

    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/c;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    if-eqz v0, :cond_0

    .line 127
    :try_start_1
    invoke-interface {v0}, Lcom/google/android/gms/fitness/l/a/a;->a()V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->d:Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->c:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/c;->b:Lcom/google/android/gms/fitness/l/a/d;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 132
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->a:Ljava/util/concurrent/ConcurrentMap;

    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/c;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_0
    :goto_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    .line 129
    :catch_0
    move-exception v0

    :goto_1
    :try_start_3
    const-string v2, "Cannot flush data points buffer"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 132
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->a:Ljava/util/concurrent/ConcurrentMap;

    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/c;->c:Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 132
    :catchall_1
    move-exception v0

    :try_start_5
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v2, v2, Lcom/google/android/gms/fitness/l/a/b;->a:Ljava/util/concurrent/ConcurrentMap;

    iget-object v3, p0, Lcom/google/android/gms/fitness/l/a/c;->c:Ljava/lang/String;

    invoke-interface {v2, v3}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 129
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v2, 0x0

    .line 98
    iget-object v3, p0, Lcom/google/android/gms/fitness/l/a/c;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v3}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/a/a;

    if-nez v0, :cond_0

    const-string v0, "Creating new data points buffer"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v1, Lcom/google/android/gms/fitness/l/a/e;

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->b:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/a;->a()Lcom/google/android/gms/fitness/l/z;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/fitness/l/a/e;-><init>(Lcom/google/android/gms/fitness/l/z;)V

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v3, v1}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->Z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v3, v3, Lcom/google/android/gms/fitness/l/a/b;->c:Landroid/os/Handler;

    iget-object v4, p0, Lcom/google/android/gms/fitness/l/a/c;->b:Lcom/google/android/gms/fitness/l/a/d;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    move-object v0, v1

    :cond_0
    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/l/a/a;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 99
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/c;->d:Ljava/util/concurrent/atomic/AtomicLong;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->Z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    add-long/2addr v6, v4

    invoke-virtual {v1, v8, v9, v6, v7}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-nez v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    cmp-long v3, v0, v8

    if-eqz v3, :cond_2

    cmp-long v0, v0, v4

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a/c;->a()V

    .line 105
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 101
    goto :goto_0
.end method

.method public final a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/c;->a:Lcom/google/android/gms/fitness/l/a/b;

    iget-object v0, v0, Lcom/google/android/gms/fitness/l/a/b;->a:Ljava/util/concurrent/ConcurrentMap;

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/c;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/a/a;

    .line 141
    if-eqz v0, :cond_0

    .line 142
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/fitness/l/a/a;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 144
    :cond_0
    return-void
.end method
