.class public final Lcom/google/android/gms/people/sync/ap;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final m:[Ljava/lang/String;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/util/p;

.field private final c:Lcom/google/android/gms/people/c/f;

.field private final d:Lcom/google/android/gms/people/c/e;

.field private final e:Lcom/google/android/gms/people/service/h;

.field private final f:J

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Landroid/content/ContentValues;

.field private k:Ljava/util/Set;

.field private l:Ljava/util/Set;

.field private final n:Lcom/google/android/gms/people/sync/ba;

.field private final o:Lcom/google/android/gms/people/sync/bc;

.field private final p:Lcom/google/android/gms/people/sync/bd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 2495
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "display_name"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "formatted_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "given_name"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "family_name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "middle_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "honorific_suffix"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "honorific_prefix"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "yomi_given_name"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "yomi_family_name"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "yomi_honorific_suffix"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "yomi_honorific_prefix"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/sync/ap;->m:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->j:Landroid/content/ContentValues;

    .line 1667
    iput-object v2, p0, Lcom/google/android/gms/people/sync/ap;->k:Ljava/util/Set;

    .line 1668
    iput-object v2, p0, Lcom/google/android/gms/people/sync/ap;->l:Ljava/util/Set;

    .line 2863
    new-instance v0, Lcom/google/android/gms/people/sync/ba;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/people/sync/ba;-><init>(Lcom/google/android/gms/people/sync/ap;B)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->n:Lcom/google/android/gms/people/sync/ba;

    .line 2866
    new-instance v0, Lcom/google/android/gms/people/sync/bc;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/people/sync/bc;-><init>(Lcom/google/android/gms/people/sync/ap;B)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->o:Lcom/google/android/gms/people/sync/bc;

    .line 2869
    new-instance v0, Lcom/google/android/gms/people/sync/bd;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/people/sync/bd;-><init>(Lcom/google/android/gms/people/sync/ap;B)V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->p:Lcom/google/android/gms/people/sync/bd;

    .line 148
    iput-object p1, p0, Lcom/google/android/gms/people/sync/ap;->a:Landroid/content/Context;

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->b:Lcom/google/android/gms/common/util/p;

    .line 150
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->c:Lcom/google/android/gms/people/c/f;

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->c:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    .line 152
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    .line 153
    iget-wide v0, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    .line 154
    iput-object p2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    .line 155
    iput-object p3, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    .line 156
    invoke-static {p1}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->e:Lcom/google/android/gms/people/service/h;

    .line 163
    return-void
.end method

.method private A()Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->j:Landroid/content/ContentValues;

    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->j:Landroid/content/ContentValues;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;I)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 1822
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 1824
    const-string v1, "last_modified"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1825
    const-string v1, "owner_id"

    iget-wide v2, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1826
    const-string v1, "circle_id"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1827
    const-string v1, "name"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1828
    const-string v1, "people_count"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1829
    const-string v1, "sort_key"

    const-string v2, "s%06d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1830
    const-string v1, "type"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1831
    const-string v1, "client_policies"

    const/4 v2, 0x7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1839
    return-object v0
.end method

.method private a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 1367
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 1368
    invoke-static {v0, p1}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)V

    .line 1369
    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 1351
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 1353
    const-string v1, "owner_id"

    iget-wide v2, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1354
    const-string v1, "qualified_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1355
    const-string v1, "custom_label"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1356
    const-string v1, "email"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1357
    const-string v1, "type"

    invoke-static {p2}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1359
    invoke-static {v0, p2}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)V

    .line 1363
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/people/sync/ap;)Lcom/google/android/gms/people/sync/ba;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->n:Lcom/google/android/gms/people/sync/ba;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 931
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "e"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)V
    .locals 4

    .prologue
    .line 1373
    const-string v0, "affinity1"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v1

    const-string v2, "emailAutocomplete"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1375
    const-string v0, "logging_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v1

    const-string v2, "emailAutocomplete"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1378
    const-string v0, "affinity2"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1379
    const-string v0, "affinity3"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1380
    const-string v0, "affinity4"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1381
    const-string v0, "affinity5"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1383
    const-string v0, "logging_id2"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1384
    const-string v0, "logging_id3"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1385
    const-string v0, "logging_id4"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1386
    const-string v0, "logging_id5"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1387
    return-void
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;)V
    .locals 4

    .prologue
    .line 2397
    const-string v0, "affinity1"

    const-string v1, "gplusAutocomplete"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2399
    const-string v0, "logging_id"

    const-string v1, "gplusAutocomplete"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2402
    const-string v0, "affinity2"

    const-string v1, "chatAutocomplete"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2404
    const-string v0, "logging_id2"

    const-string v1, "chatAutocomplete"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2407
    const-string v0, "affinity3"

    const-string v1, "peopleAutocompleteSocial"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2409
    const-string v0, "logging_id3"

    const-string v1, "peopleAutocompleteSocial"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412
    const-string v0, "affinity4"

    const-string v1, "fieldAutocompleteSocial"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 2414
    const-string v0, "logging_id4"

    const-string v1, "fieldAutocompleteSocial"

    invoke-static {p1, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2417
    const-string v0, "affinity5"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2418
    const-string v0, "logging_id5"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2419
    return-void
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 555
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;

    move-result-object v3

    .line 556
    const-string v4, "cover_photo_url"

    if-nez v3, :cond_0

    move-object v0, v1

    :goto_0
    invoke-virtual {p0, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 558
    const-string v4, "cover_photo_height"

    if-nez v3, :cond_1

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 560
    const-string v0, "cover_photo_width"

    if-nez v3, :cond_2

    :goto_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p0, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 561
    const-string v0, "cover_photo_id"

    if-nez v3, :cond_3

    :goto_3
    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    return-void

    .line 556
    :cond_0
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 558
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->b()I

    move-result v0

    goto :goto_1

    .line 560
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->e()I

    move-result v2

    goto :goto_2

    .line 561
    :cond_3
    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$CoverPhotos;->c()Ljava/lang/String;

    move-result-object v1

    goto :goto_3
.end method

.method private static a(Landroid/content/Context;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 1022
    const-string v0, "PeopleSync"

    const-string v1, "FK Error, ignoring."

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1024
    const-string v0, "PeopleSync"

    const-string v1, "FK Error, ignoring."

    invoke-static {p0, v0, v1, p1}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1025
    return-void
.end method

.method private static a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 1591
    sget-object v0, Lcom/google/android/gms/people/a/a;->ay:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1592
    const-string v0, "affinity1"

    const-string v1, "gplusAutocomplete"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1594
    const-string v0, "logging_id"

    const-string v1, "gplusAutocomplete"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1597
    const-string v0, "affinity2"

    const-string v1, "chatAutocomplete"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1599
    const-string v0, "logging_id2"

    const-string v1, "chatAutocomplete"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1602
    const-string v0, "affinity3"

    const-string v1, "peopleAutocompleteSocial"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1604
    const-string v0, "logging_id3"

    const-string v1, "peopleAutocompleteSocial"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1607
    const-string v0, "affinity4"

    const-string v1, "fieldAutocompleteSocial"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    .line 1609
    const-string v0, "logging_id4"

    const-string v1, "fieldAutocompleteSocial"

    invoke-static {p0, v1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1627
    :goto_0
    return-void

    .line 1612
    :cond_0
    const-string v0, "affinity1"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1613
    const-string v0, "logging_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1615
    const-string v0, "affinity2"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1616
    const-string v0, "logging_id2"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1618
    const-string v0, "affinity3"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1619
    const-string v0, "logging_id3"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1621
    const-string v0, "affinity4"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1622
    const-string v0, "logging_id4"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1624
    const-string v0, "affinity5"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1625
    const-string v0, "logging_id5"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 1583
    const-string v0, "last_modified"

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1584
    const-string v0, "owner_id"

    iget-wide v2, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1585
    const-string v0, "qualified_id"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->n(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1586
    const-string v0, "v2_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1587
    const-string v0, "sync_is_alive"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1588
    return-void
.end method

.method private a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;ZLcom/google/android/gms/people/sync/y;)V
    .locals 12

    .prologue
    .line 2172
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v0

    .line 2173
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v1

    .line 2175
    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2176
    const-string v2, "PeopleSync"

    const-string v3, "  insertAutocompletePerson: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2180
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "ac_people"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v4

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "owner_id"

    iget-wide v8, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "people_v2_id"

    invoke-virtual {v5, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "qualified_id"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->p(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "sync_is_alive"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {v5, v4}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;)V

    invoke-virtual {v0, v2, v5}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v8

    .line 2182
    iget v0, p3, Lcom/google/android/gms/people/sync/y;->D:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p3, Lcom/google/android/gms/people/sync/y;->D:I

    .line 2187
    new-instance v4, Landroid/support/v4/g/a;

    invoke-direct {v4}, Landroid/support/v4/g/a;-><init>()V

    .line 2190
    if-nez p2, :cond_2

    .line 2191
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->h(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2194
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2195
    sget-object v2, Lcom/google/android/gms/people/sync/e;->d:Ljava/lang/String;

    invoke-interface {v4, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2197
    const-string v2, "people_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2198
    const-string v2, "container_type"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2199
    const-string v2, "gaia_id"

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2200
    const-string v1, "profile_type"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2201
    const-string v1, "in_circle"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->r(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2203
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->k(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2206
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 2207
    sget-object v1, Lcom/google/android/gms/people/sync/e;->e:Ljava/lang/String;

    invoke-interface {v4, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2209
    const-string v1, "people_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2210
    const-string v1, "container_type"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2211
    const-string v1, "profile_type"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2212
    const-string v1, "in_circle"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->r(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2215
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->j(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2218
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2219
    invoke-interface {v4, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2221
    const-string v3, "people_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2222
    const-string v3, "container_type"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2223
    const-string v3, "contact_id"

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2224
    const-string v0, "profile_type"

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2225
    const-string v0, "in_circle"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->r(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 2229
    :cond_3
    new-instance v0, Lcom/google/android/gms/people/sync/aq;

    sget-object v2, Lcom/google/android/gms/people/sync/m;->a:Lcom/google/android/gms/people/sync/m;

    move-object v1, p0

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/sync/aq;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/aq;->a()V

    .line 2230
    new-instance v0, Lcom/google/android/gms/people/sync/ar;

    sget-object v2, Lcom/google/android/gms/people/sync/n;->a:Lcom/google/android/gms/people/sync/n;

    move-object v1, p0

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/sync/ar;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ar;->a()V

    .line 2231
    new-instance v0, Lcom/google/android/gms/people/sync/as;

    sget-object v2, Lcom/google/android/gms/people/sync/l;->a:Lcom/google/android/gms/people/sync/l;

    move-object v1, p0

    move-object v3, p1

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/sync/as;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/as;->a()V

    .line 2233
    if-nez p2, :cond_4

    .line 2237
    invoke-static {v4}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/util/Map;)V

    .line 2243
    :cond_4
    new-instance v7, Landroid/support/v4/g/a;

    invoke-direct {v7}, Landroid/support/v4/g/a;-><init>()V

    .line 2244
    invoke-interface {v4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2245
    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    .line 2246
    const/4 v3, 0x2

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2247
    const-string v3, "PeopleSync"

    const-string v5, "    container: %s (%s %s)"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v6, v10

    const/4 v10, 0x1

    const-string v11, "gaia_id"

    invoke-virtual {v1, v11}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v6, v10

    const/4 v10, 0x2

    const-string v11, "contact_id"

    invoke-virtual {v1, v11}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v11

    aput-object v11, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2250
    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v5, "ac_container"

    invoke-virtual {v3, v5, v1}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v10

    .line 2251
    iget v1, p3, Lcom/google/android/gms/people/sync/y;->G:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p3, Lcom/google/android/gms/people/sync/y;->G:I

    .line 2253
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2256
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "container_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "item_type"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "value"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v3, "ac_item"

    invoke-virtual {v1, v3, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1

    .line 2260
    :cond_6
    new-instance v0, Lcom/google/android/gms/people/sync/at;

    sget-object v2, Lcom/google/android/gms/people/sync/i;->a:Lcom/google/android/gms/people/sync/i;

    move-object v1, p0

    move-object v3, p1

    move-object v4, v7

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/sync/at;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;ZLcom/google/android/gms/people/sync/y;)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/at;->a()V

    new-instance v0, Lcom/google/android/gms/people/sync/au;

    sget-object v2, Lcom/google/android/gms/people/sync/o;->a:Lcom/google/android/gms/people/sync/o;

    move-object v1, p0

    move-object v3, p1

    move-object v4, v7

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/sync/au;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;ZLcom/google/android/gms/people/sync/y;)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/au;->a()V

    new-instance v0, Lcom/google/android/gms/people/sync/av;

    sget-object v2, Lcom/google/android/gms/people/sync/q;->a:Lcom/google/android/gms/people/sync/q;

    move-object v1, p0

    move-object v3, p1

    move-object v4, v7

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/sync/av;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;ZLcom/google/android/gms/people/sync/y;)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/av;->a()V

    .line 2263
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->c:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->h()Lcom/google/android/gms/people/c/a/f;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/people/c/a/f;->a(J)V

    .line 2264
    return-void
.end method

.method private static a(Ljava/util/Map;)V
    .locals 8

    .prologue
    .line 2514
    sget-object v0, Lcom/google/android/gms/people/sync/e;->d:Ljava/lang/String;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 2515
    if-nez v0, :cond_1

    .line 2531
    :cond_0
    return-void

    .line 2518
    :cond_1
    invoke-interface {p0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    .line 2519
    const-string v2, "container_type"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v4, 0x1

    if-ne v2, v4, :cond_2

    .line 2520
    const-string v2, "display_name"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 2523
    sget-object v4, Lcom/google/android/gms/people/sync/ap;->m:[Ljava/lang/String;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 2528
    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2527
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/people/sync/ap;)Lcom/google/android/gms/people/sync/bc;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->o:Lcom/google/android/gms/people/sync/bc;

    return-object v0
.end method

.method private static b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 1630
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1631
    const-string v0, "sort_key"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1633
    const-string v0, "sort_key_irank"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->s()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$SortKeys;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1639
    :goto_0
    return-void

    .line 1636
    :cond_0
    const-string v0, "sort_key"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1637
    const-string v0, "sort_key_irank"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 1275
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "circle_members"

    const-string v3, "owner_id = ? AND qualified_id = ?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v4, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1283
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1284
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v3

    .line 1285
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    .line 1286
    :goto_0
    if-ge v1, v4, :cond_2

    .line 1287
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v11}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "PeopleSync"

    const-string v6, "    %s    --> %s: (Create)"

    new-array v7, v11, [Ljava/lang/Object;

    aput-object p1, v7, v2

    aput-object v0, v7, v10

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/sync/ap;->i(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "PeopleSync"

    const-string v6, "    Circle %s doesn\'t exist"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v0, v7, v2

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1286
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1287
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v5

    const-string v6, "owner_id"

    iget-wide v8, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "circle_id"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "qualified_id"

    invoke-virtual {v5, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v6, "circle_members"

    invoke-virtual {v0, v6, v5}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1

    .line 1290
    :cond_2
    return-void
.end method

.method private c(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 683
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v1

    .line 685
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 686
    new-instance v0, Lcom/google/android/gms/people/sync/t;

    const-string v1, "Missing gaia-id for +page"

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/t;-><init>(Ljava/lang/String;)V

    throw v0

    .line 689
    :cond_0
    const-string v0, "gaia_id"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 691
    const-string v0, "account_name"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    const-string v0, "display_name"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    const-string v0, "page_gaia_id"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    const-string v0, "etag"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    const-string v2, "avatar"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    return-object v1

    .line 697
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/plus/service/v2whitelisted/models/a;I)Landroid/content/ContentValues;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1918
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 1920
    const-string v1, "last_modified"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1921
    const-string v1, "owner_id"

    iget-wide v2, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1922
    const-string v1, "circle_id"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1923
    const-string v1, "name"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1925
    const-string v1, "sort_key"

    const-string v2, "p%06d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1926
    const-string v1, "type"

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1927
    const-string v1, "client_policies"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1928
    const-string v1, "for_sharing"

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->d()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1931
    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/people/sync/ap;)Lcom/google/android/gms/people/sync/bd;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->p:Lcom/google/android/gms/people/sync/bd;

    return-object v0
.end method

.method private c(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 4

    .prologue
    .line 1293
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "emails"

    const-string v2, "owner_id = ? AND qualified_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1301
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1318
    :cond_0
    :goto_0
    return-void

    .line 1305
    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/people/sync/e;->l(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    move-result-object v0

    .line 1306
    if-eqz v0, :cond_2

    .line 1307
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "emails"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    .line 1311
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    .line 1312
    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1313
    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v3, "emails"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/gms/people/sync/ap;)Landroid/content/ContentValues;
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 10

    .prologue
    .line 1390
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "phones"

    const-string v2, "owner_id = ? AND qualified_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1398
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1412
    :cond_0
    return-void

    .line 1401
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r()Ljava/util/List;

    move-result-object v2

    .line 1402
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 1403
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1405
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    .line 1406
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->c()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1408
    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v5, "phones"

    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v6

    const-string v7, "owner_id"

    iget-wide v8, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "qualified_id"

    invoke-virtual {v6, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "phone"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->e()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "type"

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1403
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/people/sync/ap;)Lcom/google/android/gms/people/c/e;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    return-object v0
.end method

.method private e(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 8

    .prologue
    .line 974
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "application_packages"

    const-string v2, "owner_id = ? AND dev_console_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 982
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v2

    .line 983
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 984
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;

    .line 985
    const-string v3, "android"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 987
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v3

    .line 989
    const-string v4, "dev_console_id"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 991
    const-string v4, "owner_id"

    iget-wide v6, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 992
    const-string v4, "package_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 993
    const-string v4, "certificate_hash"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PlusAppClient;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 996
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v4, "application_packages"

    invoke-virtual {v0, v4, v3}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 983
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 999
    :cond_1
    return-void
.end method

.method private e(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 10

    .prologue
    .line 1429
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "postal_address"

    const-string v2, "owner_id = ? AND qualified_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1436
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1451
    :cond_0
    return-void

    .line 1440
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b()Ljava/util/List;

    move-result-object v2

    .line 1441
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 1442
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 1443
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    .line 1444
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->d()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1446
    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v5, "postal_address"

    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v6

    const-string v7, "owner_id"

    iget-wide v8, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "qualified_id"

    invoke-virtual {v6, v7, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "postal_address"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "type"

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1442
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public static e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z
    .locals 1

    .prologue
    .line 1674
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Landroid/content/ContentValues;
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1492
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v4

    .line 1494
    invoke-direct {p0, p1, v4}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/content/ContentValues;)V

    .line 1495
    const-string v0, "etag"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1496
    const-string v0, "mobile_owner_id"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->s(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1498
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1499
    new-instance v0, Lcom/google/android/gms/people/sync/t;

    const-string v1, "This person has no metadata. Each synced person must have a gaia-id or belong to a circle."

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/sync/t;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1503
    :cond_0
    const-string v5, "invisible_3p"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->o()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1507
    const-string v5, "blocked"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1509
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 1510
    const-string v0, "gaia_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1514
    :goto_2
    const-string v5, "in_viewer_domain"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->n()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x2

    :goto_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1517
    const-string v0, "in_circle"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->r(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1518
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1519
    const-string v0, "in_contacts"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1524
    :goto_4
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-static {v0, v4}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Landroid/content/ContentValues;)V

    .line 1527
    invoke-static {p1, v4}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/content/ContentValues;)V

    .line 1529
    const-string v0, "profile_type"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1534
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;

    move-result-object v0

    .line 1535
    const-string v1, "tagline"

    if-nez v0, :cond_7

    move-object v0, v3

    :goto_5
    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1541
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    .line 1542
    if-eqz v0, :cond_9

    .line 1543
    const-string v1, "name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1544
    const-string v1, "family_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1545
    const-string v1, "given_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    const-string v1, "middle_name"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1547
    const-string v1, "name_verified"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->h()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v5

    if-nez v5, :cond_8

    :goto_6
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1558
    :goto_7
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    move-result-object v0

    .line 1559
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->b()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1560
    :cond_1
    const-string v0, "avatar"

    invoke-virtual {v4, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1568
    :goto_8
    return-object v4

    :cond_2
    move v0, v2

    .line 1503
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 1507
    goto/16 :goto_1

    .line 1512
    :cond_4
    const-string v0, "gaia_id"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    :cond_5
    move v0, v1

    .line 1514
    goto/16 :goto_3

    .line 1521
    :cond_6
    const-string v0, "in_contacts"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_4

    .line 1535
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Taglines;->c()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    .line 1547
    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->h()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;->g()Z

    move-result v2

    goto :goto_6

    .line 1550
    :cond_9
    const-string v0, "name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1551
    const-string v0, "family_name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1552
    const-string v0, "given_name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1553
    const-string v0, "middle_name"

    invoke-virtual {v4, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 1554
    const-string v0, "name_verified"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_7

    .line 1563
    :cond_a
    const-string v1, "avatar"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_8
.end method

.method private r(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->c:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->h()Lcom/google/android/gms/people/c/a/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gms/people/c/a/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1471
    return-void
.end method

.method private s(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 2877
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2878
    const-string v0, "PeopleSync"

    const-string v1, "  deleteAutocompletePerson: id=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2882
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 2884
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "ac_people"

    const-string v2, "owner_id=? AND people_v2_id=?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT value FROM sync_tokens WHERE owner_id=? AND name=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 8

    .prologue
    .line 174
    const-string v0, "PeopleSync"

    const-string v1, "New sync Tokens are:"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT name, value FROM sync_tokens WHERE owner_id = ? ORDER BY name"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 187
    :try_start_0
    const-string v0, "name"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 188
    const-string v2, "value"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 190
    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 191
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 192
    const-string v3, "PeopleSync"

    const-string v4, "    %s: %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 197
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 198
    return-void
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 447
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 448
    const-string v1, "last_full_people_sync_time"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 450
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "owners"

    const-string v3, "_id = ?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 454
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    .line 1029
    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v0

    .line 1030
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1031
    const-string v1, "PeopleSync"

    const-string v2, "    Application: %s - Person: %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1035
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v1

    .line 1037
    const-string v2, "dev_console_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    const-string v0, "owner_id"

    iget-wide v2, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1039
    const-string v0, "qualified_id"

    invoke-virtual {v1, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "facl_people"

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1043
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 1770
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1772
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v0

    .line 1774
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1775
    const-string v1, "PeopleSync"

    const-string v2, "    %s: %s (Create)"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1779
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "circles"

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;I)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1780
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 387
    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Create)"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "owners"

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ap;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 393
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x2

    const/4 v2, 0x0

    .line 509
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 511
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    .line 512
    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 513
    const-string v3, "PeopleSync"

    const-string v4, "    %s: %s (Update)"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v6, 0x1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 518
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v3

    const-string v0, "gaia_id"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->q()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    const-string v4, "display_name"

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "etag"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->b()Z

    move-result v4

    if-eqz v4, :cond_5

    :cond_1
    const-string v0, "avatar"

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    invoke-static {v3, p1}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 522
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "owners"

    const-string v4, "_id = ?"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v5}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "owner_emails"

    const-string v3, "owner_id=?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v4

    move v1, v2

    :goto_3
    if-ge v1, v4, :cond_6

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v6, "owner_emails"

    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v7

    const-string v8, "owner_id"

    iget-wide v10, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->c()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "email"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "type"

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 513
    :cond_3
    const-string v0, "- Unknown -"

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 518
    goto/16 :goto_1

    :cond_5
    const-string v1, "avatar"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Images;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 526
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "owner_phones"

    const-string v3, "owner_id=?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v4

    move v1, v2

    :goto_4
    if-ge v1, v4, :cond_8

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->c()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v6, "owner_phones"

    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v7

    const-string v8, "owner_id"

    iget-wide v10, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v8, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->d()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "phone"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->e()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "type"

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v8, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 527
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "owner_postal_address"

    const-string v3, "owner_id=?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v4

    move v1, v2

    :goto_5
    if-ge v1, v4, :cond_a

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v5, "owner_postal_address"

    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v6

    const-string v7, "owner_id"

    iget-wide v8, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v7, "custom_label"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "postal_address"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v7, "type"

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v6, v7, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v2, v5, v6}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 528
    :cond_a
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Lcom/google/android/gms/people/sync/y;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1685
    invoke-static {v8}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1686
    const-string v0, "PeopleSync"

    const-string v2, "    %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1689
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->o(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v2

    .line 1691
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->g(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1755
    :cond_1
    :goto_0
    return-void

    .line 1697
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1698
    iget v0, p2, Lcom/google/android/gms/people/sync/y;->X:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/gms/people/sync/y;->X:I

    .line 1703
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->k:Ljava/util/Set;

    if-nez v0, :cond_3

    .line 1704
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->k:Ljava/util/Set;

    .line 1706
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->l:Ljava/util/Set;

    if-nez v0, :cond_4

    .line 1707
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/ap;->l:Ljava/util/Set;

    .line 1709
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1710
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->l:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1713
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    if-eqz v0, :cond_6

    .line 1714
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;->d()Ljava/util/List;

    move-result-object v3

    .line 1715
    invoke-static {v3}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v4

    move v0, v1

    .line 1716
    :goto_2
    if-ge v0, v4, :cond_6

    .line 1717
    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->k:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1716
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1700
    :cond_5
    iget v0, p2, Lcom/google/android/gms/people/sync/y;->Y:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/gms/people/sync/y;->Y:I

    goto :goto_1

    .line 1722
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v3, "SELECT circle_id FROM circle_members WHERE owner_id=?  AND qualified_id=?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 1730
    :goto_3
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1731
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->l:Ljava/util/Set;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 1734
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_7
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1737
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->n(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v0

    .line 1739
    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->k:Ljava/util/Set;

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->l:Ljava/util/Set;

    invoke-interface {v3, v4}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->c:Lcom/google/android/gms/people/c/f;

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v0}, Lcom/google/android/gms/people/c/f;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1741
    :cond_8
    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->e(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1742
    iget v0, p2, Lcom/google/android/gms/people/sync/y;->Z:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/gms/people/sync/y;->Z:I

    .line 1746
    :goto_4
    invoke-static {v9}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1747
    const-string v0, "PeopleSync"

    const-string v3, "Inconsistency found: gaia=%s  expected=%s  actual=%s"

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v2, v4, v1

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->k:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v7

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->l:Ljava/util/Set;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1744
    :cond_9
    iget v0, p2, Lcom/google/android/gms/people/sync/y;->aa:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p2, Lcom/google/android/gms/people/sync/y;->aa:I

    goto :goto_4
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;ZZLcom/google/android/gms/people/sync/y;)V
    .locals 10

    .prologue
    .line 2129
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 2132
    if-eqz p2, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->q(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    .line 2133
    iget v0, p4, Lcom/google/android/gms/people/sync/y;->E:I

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/people/sync/ap;->s(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p4, Lcom/google/android/gms/people/sync/y;->E:I

    .line 2144
    :cond_0
    :goto_1
    return-void

    .line 2132
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->r(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->h(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v1

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->i(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v2

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    if-nez v2, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 2137
    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->m(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 2138
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "PeopleSync"

    const-string v2, "  updateAutocompletePerson: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "ac_people"

    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "sync_is_alive"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;)V

    const-string v4, "owner_id = ? AND people_v2_id = ?"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v5, v0}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "SELECT _id FROM ac_people WHERE owner_id=? AND people_v2_id=?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v2, v0, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v2

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_5

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleSync"

    const-string v1, "    person not found"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    iget v0, p4, Lcom/google/android/gms/people/sync/y;->F:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p4, Lcom/google/android/gms/people/sync/y;->F:I

    new-instance v4, Landroid/support/v4/g/a;

    invoke-direct {v4}, Landroid/support/v4/g/a;-><init>()V

    if-nez p3, :cond_7

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->h(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT _id FROM ac_container WHERE people_id=? AND container_type=?"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, -0x1

    invoke-virtual {v0, v1, v5, v6, v7}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-gez v5, :cond_9

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "PeopleSync"

    const-string v1, "    container: %d not found"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    :goto_2
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->k(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT _id FROM ac_container WHERE people_id=? AND container_type=?"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const-wide/16 v6, -0x1

    invoke-virtual {v0, v1, v5, v6, v7}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v5, v0, v6

    if-gez v5, :cond_a

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "PeopleSync"

    const-string v1, "    container: %d not found"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v1, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    :goto_3
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->j(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_8
    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v6, "SELECT _id FROM ac_container WHERE people_id=? AND container_type=? AND contact_id=?"

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8, v0}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    const-wide/16 v8, -0x1

    invoke-virtual {v5, v6, v7, v8, v9}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v5, v6, v8

    if-gez v5, :cond_b

    const/4 v5, 0x2

    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v5

    if-eqz v5, :cond_8

    const-string v5, "PeopleSync"

    const-string v6, "    container: %d (null %s) not found"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    const/4 v8, 0x1

    aput-object v0, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    sget-object v5, Lcom/google/android/gms/people/sync/e;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_a
    sget-object v5, Lcom/google/android/gms/people/sync/e;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_b
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_c
    new-instance v0, Lcom/google/android/gms/people/sync/aw;

    sget-object v2, Lcom/google/android/gms/people/sync/i;->a:Lcom/google/android/gms/people/sync/i;

    move-object v1, p0

    move-object v3, p1

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/sync/aw;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;ZLcom/google/android/gms/people/sync/y;)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/aw;->a()V

    new-instance v0, Lcom/google/android/gms/people/sync/ax;

    sget-object v2, Lcom/google/android/gms/people/sync/o;->a:Lcom/google/android/gms/people/sync/o;

    move-object v1, p0

    move-object v3, p1

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/sync/ax;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;ZLcom/google/android/gms/people/sync/y;)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ax;->a()V

    new-instance v0, Lcom/google/android/gms/people/sync/ay;

    sget-object v2, Lcom/google/android/gms/people/sync/q;->a:Lcom/google/android/gms/people/sync/q;

    move-object v1, p0

    move-object v3, p1

    move v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/sync/ay;-><init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;ZLcom/google/android/gms/people/sync/y;)V

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ay;->a()V

    goto/16 :goto_1

    .line 2141
    :cond_d
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->h()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/sync/ap;->s(Ljava/lang/String;)I

    .line 2142
    invoke-direct {p0, p1, p3, p4}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;ZLcom/google/android/gms/people/sync/y;)V

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/a;I)V
    .locals 5

    .prologue
    const/4 v2, 0x2

    .line 1869
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1871
    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1872
    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Create)"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->f()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1876
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "circles"

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/sync/ap;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/a;I)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1877
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 6

    .prologue
    .line 718
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 721
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 722
    invoke-static {v0, p2}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/ContentValues;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 724
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "owners"

    const-string v3, "account_name = ? AND page_gaia_id = ?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 727
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 220
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->a()V

    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM sync_tokens WHERE owner_id = ? AND name = ?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 230
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "INSERT INTO sync_tokens(owner_id,name,value) VALUES (?,?,?)"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1, p2}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 243
    return-void

    .line 242
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 2039
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 2041
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2042
    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2046
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM gaia_id_map WHERE owner_id=? AND contact_id=?  AND value=? "

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1, p2}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2054
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "INSERT INTO gaia_id_map(owner_id,contact_id,value,gaia_id,type) VALUES (?,?,?,?,?)"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, p1, p2, p3, v3}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2062
    return-void
.end method

.method public final a(ZJLjava/lang/Long;)V
    .locals 6

    .prologue
    const/4 v3, 0x3

    const/4 v4, 0x1

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->e()V

    .line 414
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sync stats: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " page="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  start="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  end="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 419
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 423
    const-string v1, "last_sync_start_time"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 425
    if-nez p4, :cond_1

    .line 426
    const-string v1, "last_sync_status"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 427
    const-string v1, "last_sync_finish_time"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 440
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->e:Lcom/google/android/gms/people/service/h;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 441
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "owners"

    const-string v3, "_id = ?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->e:Lcom/google/android/gms/people/service/h;

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/h;->b()V

    .line 444
    return-void

    .line 429
    :cond_1
    const-string v1, "last_sync_finish_time"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 430
    if-eqz p1, :cond_2

    .line 431
    const-string v1, "last_sync_status"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 432
    const-string v1, "last_successful_sync_time"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 434
    :cond_2
    const-string v1, "last_sync_status"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x2

    const/4 v1, 0x1

    .line 836
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 837
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 839
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 840
    const-string v0, "PeopleSync"

    const-string v3, "Owner domain: %s %s"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v2

    aput-object p2, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 844
    if-eqz p1, :cond_2

    .line 845
    const-string v1, "is_dasher"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 846
    const-string v1, "dasher_domain"

    invoke-virtual {v0, v1, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "owners"

    const-string v3, "account_name=?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 856
    return-void

    :cond_1
    move v0, v2

    .line 836
    goto :goto_0

    .line 848
    :cond_2
    const-string v2, "is_dasher"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 849
    const-string v1, "dasher_domain"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 254
    const-string v0, "people"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    const-string v0, "gaiamap"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    const-string v0, "autocomplete"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 5

    .prologue
    .line 942
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 943
    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Create)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 947
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v0

    .line 949
    const-string v1, "dev_console_id"

    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 950
    const-string v1, "owner_id"

    iget-wide v2, p0, Lcom/google/android/gms/people/sync/ap;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 952
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "applications"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 954
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ap;->e(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    .line 955
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x2

    .line 1047
    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v0

    .line 1048
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1049
    const-string v1, "PeopleSync"

    const-string v2, "    Application: %s - circle: %s"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1054
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "INSERT INTO facl_people(dev_console_id, owner_id, qualified_id) SELECT ?, owner_id, qualified_id FROM circle_members WHERE owner_id = ? AND circle_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v0, v3, p2}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1068
    :goto_0
    return-void

    .line 1065
    :catch_0
    move-exception v0

    .line 1066
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/Context;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;I)V
    .locals 6

    .prologue
    const/4 v3, 0x2

    .line 1795
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1797
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v0

    .line 1799
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1800
    const-string v1, "PeopleSync"

    const-string v2, "    %s: %s (Update)"

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1804
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "circles"

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;I)Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "owner_id = ? AND circle_id = ?"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1806
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 660
    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Update)"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 665
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "owners"

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ap;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "account_name = ? AND page_gaia_id = ?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 668
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 1167
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1169
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->n(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    .line 1171
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1172
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    .line 1173
    const-string v2, "PeopleSync"

    const-string v3, "    %s: %s [%d Phones, %d Emails, %d Addresses] (Create)"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v5, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v4, v5

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v6

    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1183
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "people"

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ap;->f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1185
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->b(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1186
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->c(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1187
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->d(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1188
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->e(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1190
    invoke-direct {p0, v1}, Lcom/google/android/gms/people/sync/ap;->r(Ljava/lang/String;)V

    .line 1191
    return-void

    .line 1173
    :cond_1
    const-string v0, "- Unknown -"

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/plus/service/v2whitelisted/models/a;I)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 1892
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1894
    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1895
    const-string v0, "PeopleSync"

    const-string v1, "    %s: %s (Update)"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->f()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1899
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "circles"

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/sync/ap;->c(Lcom/google/android/gms/plus/service/v2whitelisted/models/a;I)Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "owner_id = ? AND circle_id = ?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/a;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1901
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 247
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    return-void
.end method

.method public final c()Ljava/util/Set;
    .locals 4

    .prologue
    .line 295
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 296
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "SELECT circle_id, type FROM circles WHERE owner_id = ? AND type != -1"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 307
    :try_start_0
    const-string v2, "circle_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 308
    const-string v3, "type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 310
    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 311
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 312
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 315
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 317
    return-object v0
.end method

.method public final c(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 5

    .prologue
    .line 965
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 966
    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Update)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ap;->e(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    .line 971
    return-void
.end method

.method public final c(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1195
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->e()V

    .line 1196
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->a()V

    .line 1198
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1201
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/h;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    .line 1203
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1205
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/h;->b()V

    .line 1207
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1209
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 1210
    return-void

    .line 1209
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method

.method public final c(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->e()V

    .line 268
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM sync_tokens WHERE owner_id = ? AND name LIKE ?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    return-void
.end method

.method public final d()Ljava/util/Set;
    .locals 4

    .prologue
    .line 322
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 323
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "SELECT circle_id, type FROM circles WHERE owner_id = ? AND type = -1"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 334
    :try_start_0
    const-string v2, "circle_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 335
    const-string v3, "type"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    .line 337
    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 338
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 339
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 342
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 344
    return-object v0
.end method

.method public final d(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V
    .locals 5

    .prologue
    .line 1003
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1004
    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Clear fACL)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "facl_people"

    const-string v2, "owner_id = ? AND dev_console_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1015
    return-void
.end method

.method public final d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 1231
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1233
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->n(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Ljava/lang/String;

    move-result-object v1

    .line 1235
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->m(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1236
    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237
    const-string v0, "PeopleSync"

    const-string v2, "    %s: [%d Emails] (Incremental Update)"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "people"

    invoke-direct {p0}, Lcom/google/android/gms/people/sync/ap;->A()Landroid/content/ContentValues;

    move-result-object v3

    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/content/ContentValues;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->n()Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Metadata;Landroid/content/ContentValues;)V

    invoke-static {p1, v3}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Landroid/content/ContentValues;)V

    const-string v4, "owner_id = ? AND qualified_id = ?"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v5, v1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1248
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->l(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v3, "emails"

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;

    move-result-object v4

    const-string v5, "owner_id = ? AND qualified_id = ? AND email = ?"

    iget-object v6, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v1, v0}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v3, v4, v5, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1272
    :cond_1
    :goto_0
    return-void

    .line 1248
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Ljava/lang/Iterable;)Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v4, "emails"

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;)Landroid/content/ContentValues;

    move-result-object v5

    const-string v6, "owner_id = ? AND qualified_id = ? AND email = ?"

    iget-object v7, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Emails;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v1, v0}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v5, v6, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 1250
    :cond_4
    invoke-static {v5}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1251
    invoke-static {p1}, Lcom/google/android/gms/people/sync/e;->b(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    move-result-object v0

    .line 1252
    const-string v2, "PeopleSync"

    const-string v3, "    %s: %s [%d Phones, %d Emails, %d Addresses] (Update)"

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b()Ljava/lang/String;

    move-result-object v0

    :goto_2
    aput-object v0, v4, v7

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->r()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x3

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->d()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x4

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;->b()Ljava/util/List;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1262
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "people"

    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/ap;->f(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "owner_id = ? AND qualified_id = ?"

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v5, v1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1265
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->b(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1266
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->c(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1267
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->d(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1268
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/people/sync/ap;->e(Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;)V

    .line 1270
    invoke-direct {p0, v1}, Lcom/google/android/gms/people/sync/ap;->r(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1252
    :cond_6
    const-string v0, "- Unknown -"

    goto :goto_2
.end method

.method public final d(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->e()V

    .line 284
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT COUNT(1) FROM sync_tokens WHERE owner_id = ? AND name LIKE ? "

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v6, v7}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v6

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Ljava/util/Set;
    .locals 4

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 350
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 351
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "SELECT page_gaia_id FROM owners WHERE page_gaia_id IS NOT NULL AND account_name = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 361
    :try_start_0
    const-string v2, "page_gaia_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 363
    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 364
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 365
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 368
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 349
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 368
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 370
    return-object v0
.end method

.method public final e(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 731
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 733
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    const-string v0, "PeopleSync"

    const-string v1, "    %s: ??? (Delete)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 737
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "owners"

    const-string v2, "account_name = ? AND page_gaia_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 740
    return-void
.end method

.method final f()J
    .locals 6

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT last_successful_sync_time FROM owners WHERE _id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 782
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 783
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 784
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v3, "SELECT COUNT(_id) FROM owners WHERE account_name=? AND page_gaia_id=?"

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v4, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3, v4, v6, v7}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v4

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 782
    goto :goto_0

    :cond_1
    move v1, v2

    .line 784
    goto :goto_1
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 793
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 794
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT avatar FROM owners WHERE account_name=? AND page_gaia_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 792
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 488
    sget-object v0, Lcom/google/android/gms/people/a/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 492
    :goto_0
    return v0

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "SELECT last_full_people_sync_time FROM owners WHERE _id=?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v2

    .line 492
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    sget-object v0, Lcom/google/android/gms/people/a/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final h(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 811
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 812
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 813
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT cover_photo_url FROM owners WHERE account_name=? AND page_gaia_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 811
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 753
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE    FROM owner_sync_requests WHERE account_name=?1 AND page_gaia_id IS NOT NULL  AND page_gaia_id NOT IN  (SELECT page_gaia_id FROM owners WHERE page_gaia_id IS NOT NULL  AND account_name=?1)"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 758
    return-void
.end method

.method public final i()Ljava/util/List;
    .locals 3

    .prologue
    .line 761
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 762
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT page_gaia_id   FROM owner_sync_requests WHERE account_name=?1 AND page_gaia_id IS NOT NULL  AND page_gaia_id NOT IN  (SELECT page_gaia_id FROM owners WHERE page_gaia_id IS NOT NULL  AND account_name=?1)"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 767
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 769
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 770
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 773
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 775
    return-object v0
.end method

.method public final i(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 862
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 863
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT COUNT(_id) FROM circles WHERE owner_id=? AND circle_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 4

    .prologue
    .line 802
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT avatar FROM owners WHERE account_name=? AND page_gaia_id IS NULL"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final j(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 874
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 875
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT COUNT(_id) FROM people WHERE owner_id=? AND qualified_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 4

    .prologue
    .line 821
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT cover_photo_url FROM owners WHERE account_name=? AND page_gaia_id IS NULL"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final k(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1073
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1074
    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s - everyone"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1079
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "INSERT INTO facl_people(dev_console_id, owner_id, qualified_id) SELECT ?, owner_id, qualified_id FROM people WHERE owner_id =? AND in_circle=1"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1092
    :goto_0
    return-void

    .line 1089
    :catch_0
    move-exception v0

    .line 1090
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/people/sync/ap;->a(Landroid/content/Context;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method public final l()Ljava/util/Set;
    .locals 4

    .prologue
    .line 897
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 898
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v2, "SELECT dev_console_id FROM applications WHERE owner_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 907
    :try_start_0
    const-string v2, "dev_console_id"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 909
    const/4 v3, -0x1

    invoke-interface {v1, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 910
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 911
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 914
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 916
    return-object v0
.end method

.method public final l(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1096
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1097
    const-string v0, "PeopleSync"

    const-string v1, "    Application: %s (Delete)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1101
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "applications"

    const-string v2, "owner_id = ? AND dev_console_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1108
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 1120
    const-string v0, "PeopleSync"

    const-string v1, "clearPeopleAliveFlag"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1122
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "UPDATE people SET sync_is_alive=0 WHERE owner_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1126
    return-void
.end method

.method public final m(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1643
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1644
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645
    const-string v0, "PeopleSync"

    const-string v1, "    %s: ??? (Delete)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1648
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "people"

    const-string v2, "owner_id = ? AND qualified_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1650
    return-void
.end method

.method public final n()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1132
    const-string v0, "PeopleSync"

    const-string v1, "deleteDeadPeople"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1134
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->a()V

    .line 1136
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM people WHERE owner_id=? AND sync_is_alive=0"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1141
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/ap;->v()V

    .line 1143
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1145
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 1146
    return-void

    .line 1145
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1654
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1655
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1656
    const-string v0, "PeopleSync"

    const-string v1, "    %s(v2): ??? (Delete)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1659
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM people WHERE owner_id = ? AND v2_id = ?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1664
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 1977
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1978
    const-string v0, "PeopleSync"

    const-string v1, "Clearing gaia-id map."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1980
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM gaia_id_map WHERE owner_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1983
    return-void
.end method

.method public final o(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1936
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1938
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1939
    const-string v0, "PeopleSync"

    const-string v1, "    %s: ??? (Delete)"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1942
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "circles"

    const-string v2, "owner_id = ? AND circle_id = ?"

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1944
    return-void
.end method

.method public final p()V
    .locals 3

    .prologue
    .line 2076
    const-string v0, "PeopleSync"

    const-string v1, "clearAutocompleteAliveFlag"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2078
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "UPDATE ac_people SET sync_is_alive=0 WHERE owner_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2082
    return-void
.end method

.method public final p(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1989
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 1990
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1991
    const-string v0, "PeopleSync"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Clear for :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1994
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM gaia_id_map WHERE owner_id=? AND contact_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1998
    return-void
.end method

.method public final q(Ljava/lang/String;)Ljava/util/List;
    .locals 3

    .prologue
    .line 2007
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->d()V

    .line 2009
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "   SELECT gaia_id FROM gaia_id_map m1  WHERE owner_id=?1 AND contact_id=?2  AND NOT EXISTS(SELECT * FROM gaia_id_map m2  WHERE contact_id<>?2 AND owner_id=?1  AND m1.gaia_id=m2.gaia_id)  AND NOT EXISTS(SELECT * FROM people p  WHERE p.gaia_id=m1.gaia_id AND p.owner_id=?1 AND in_circle=1)"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2, p1}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2024
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2026
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2027
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2030
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2032
    return-object v0
.end method

.method public final q()V
    .locals 3

    .prologue
    .line 2092
    const-string v0, "PeopleSync"

    const-string v1, "deleteAutocompleteDeadPeople"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2094
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM ac_people WHERE owner_id=? AND sync_is_alive=0"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2098
    return-void
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 2909
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "DELETE FROM ac_people WHERE owner_id=? AND NOT EXISTS (SELECT 1 FROM ac_container AS c WHERE ac_people._id =c.people_id AND NOT (c.container_type=0 AND c.in_circle=0))"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2923
    return-void
.end method

.method public final s()I
    .locals 6

    .prologue
    .line 2938
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT count(1) FROM people WHERE owner_id=?"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final t()I
    .locals 6

    .prologue
    .line 2949
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT count(1) FROM people WHERE owner_id=? AND gaia_id IS NOT NULL AND in_circle=1"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final u()I
    .locals 6

    .prologue
    .line 2962
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const-string v1, "SELECT count(1) FROM people WHERE owner_id=? AND in_circle=0"

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->g:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public final v()V
    .locals 3

    .prologue
    .line 2970
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->c:Lcom/google/android/gms/people/c/f;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ap;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/sync/ap;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 2971
    return-void
.end method

.method public final w()V
    .locals 1

    .prologue
    .line 2974
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->a()V

    .line 2975
    return-void
.end method

.method public final x()V
    .locals 1

    .prologue
    .line 2978
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->b()Z

    .line 2979
    return-void
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 2982
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/c/e;->a(Z)V

    .line 2983
    return-void
.end method

.method public final z()V
    .locals 1

    .prologue
    .line 2986
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ap;->d:Lcom/google/android/gms/people/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/e;->c()V

    .line 2987
    return-void
.end method
