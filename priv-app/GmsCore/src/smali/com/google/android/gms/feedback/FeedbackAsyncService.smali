.class public Lcom/google/android/gms/feedback/FeedbackAsyncService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/common/service/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/FeedbackAsyncService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    const-string v0, "GFEEDBACK_SendFeedbackAsyncService"

    sget-object v1, Lcom/google/android/gms/feedback/FeedbackAsyncService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 20
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 2

    .prologue
    .line 26
    iget-boolean v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    if-eqz v0, :cond_0

    .line 27
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackAsyncService;->b:Lcom/google/android/gms/common/service/d;

    new-instance v1, Lcom/google/android/gms/feedback/al;

    invoke-direct {v1, p1}, Lcom/google/android/gms/feedback/al;-><init>(Lcom/google/android/gms/feedback/ErrorReport;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    .line 31
    :goto_0
    const-string v0, "com.google.android.gms.feedback.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 32
    return-void

    .line 29
    :cond_0
    sget-object v0, Lcom/google/android/gms/feedback/FeedbackAsyncService;->b:Lcom/google/android/gms/common/service/d;

    new-instance v1, Lcom/google/android/gms/feedback/ai;

    invoke-direct {v1, p1}, Lcom/google/android/gms/feedback/ai;-><init>(Lcom/google/android/gms/feedback/ErrorReport;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
