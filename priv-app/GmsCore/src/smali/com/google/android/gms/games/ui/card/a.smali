.class public abstract Lcom/google/android/gms/games/ui/card/a;
.super Landroid/support/v7/widget/CardView;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected a:Lcom/google/android/gms/games/ui/br;

.field protected b:Landroid/view/View;

.field protected c:Lcom/google/android/gms/games/ui/card/s;

.field protected d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field protected e:Landroid/widget/TextView;

.field protected f:Landroid/widget/TextView;

.field protected g:Landroid/widget/TextView;

.field private h:Lcom/google/android/gms/games/ui/card/p;

.field private i:Landroid/database/CharArrayBuffer;

.field private j:Landroid/database/CharArrayBuffer;

.field private k:Landroid/database/CharArrayBuffer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method


# virtual methods
.method public final a(F)V
    .locals 1

    .prologue
    .line 104
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 107
    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 141
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setImageAlpha(I)V

    .line 146
    :goto_0
    return-void

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setAlpha(I)V

    goto :goto_0
.end method

.method public final a(IF)V
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(IF)V

    .line 176
    return-void
.end method

.method public final a(Landroid/database/CharArrayBuffer;)V
    .locals 4

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->e:Landroid/widget/TextView;

    iget-object v1, p1, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v2, 0x0

    iget v3, p1, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->setText([CII)V

    .line 205
    return-void
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->a:Lcom/google/android/gms/games/ui/br;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {v0, v1, p1, p2}, Lcom/google/android/gms/games/ui/br;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    .line 136
    return-void
.end method

.method protected final a(Landroid/widget/TextView;I)V
    .locals 1

    .prologue
    .line 295
    if-lez p2, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/card/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 301
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/br;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/a;->a:Lcom/google/android/gms/games/ui/br;

    .line 77
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/card/p;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/a;->h:Lcom/google/android/gms/games/ui/card/p;

    .line 156
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/ui/card/s;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/gms/games/ui/card/a;->c:Lcom/google/android/gms/games/ui/card/s;

    .line 112
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->b:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 127
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 116
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/card/a;->setClickable(Z)V

    .line 117
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 190
    return-void
.end method

.method public final b(Landroid/database/CharArrayBuffer;)V
    .locals 4

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    iget-object v1, p1, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v2, 0x0

    iget v3, p1, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TextView;->setText([CII)V

    .line 234
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 171
    return-void
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 151
    return-void
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/card/a;->a(Landroid/widget/TextView;I)V

    .line 210
    return-void
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setClickable(Z)V

    .line 161
    return-void
.end method

.method public final d(I)V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 219
    return-void
.end method

.method public final d_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    return-void
.end method

.method public e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->a(F)V

    .line 81
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->setClickable(Z)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 85
    const/16 v0, 0xff

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->a(I)V

    .line 86
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/card/a;->c(Z)V

    .line 88
    sget v0, Lcom/google/android/gms/f;->s:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->c(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    sget v0, Lcom/google/android/gms/f;->r:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->e(I)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->g:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 95
    sget v0, Lcom/google/android/gms/f;->q:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->f(I)V

    .line 96
    return-void
.end method

.method public final e(I)V
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/card/a;->a(Landroid/widget/TextView;I)V

    .line 240
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->g:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    return-void
.end method

.method public final e_(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    return-void
.end method

.method public final f()Landroid/database/CharArrayBuffer;
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->i:Landroid/database/CharArrayBuffer;

    return-object v0
.end method

.method public final f(I)V
    .locals 1

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/card/a;->a(Landroid/widget/TextView;I)V

    .line 276
    return-void
.end method

.method public final g()Landroid/database/CharArrayBuffer;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->j:Landroid/database/CharArrayBuffer;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->b:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->c:Lcom/google/android/gms/games/ui/card/s;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/s;->u()V

    .line 290
    :cond_0
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-ne p1, v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->h:Lcom/google/android/gms/games/ui/card/p;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/card/p;->v()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/16 v1, 0x40

    .line 57
    invoke-super {p0}, Landroid/support/v7/widget/CardView;->onFinishInflate()V

    .line 59
    iput-object p0, p0, Lcom/google/android/gms/games/ui/card/a;->b:Landroid/view/View;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->b:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 62
    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 65
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->e:Landroid/widget/TextView;

    .line 66
    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->i:Landroid/database/CharArrayBuffer;

    .line 68
    sget v0, Lcom/google/android/gms/j;->si:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->f:Landroid/widget/TextView;

    .line 69
    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->j:Landroid/database/CharArrayBuffer;

    .line 71
    sget v0, Lcom/google/android/gms/j;->pF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/card/a;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->g:Landroid/widget/TextView;

    .line 72
    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v1}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/card/a;->k:Landroid/database/CharArrayBuffer;

    .line 73
    return-void
.end method
