.class public Lcom/google/android/gms/location/copresence/People;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:Lcom/google/android/gms/location/copresence/People;

.field public static final b:Lcom/google/android/gms/location/copresence/People;


# instance fields
.field private final c:I

.field private final d:Ljava/util/List;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/location/copresence/t;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/t;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/People;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 59
    new-instance v0, Lcom/google/android/gms/location/copresence/People;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/location/copresence/People;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/location/copresence/People;->a:Lcom/google/android/gms/location/copresence/People;

    .line 64
    new-instance v0, Lcom/google/android/gms/location/copresence/People;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/location/copresence/People;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/location/copresence/People;->b:Lcom/google/android/gms/location/copresence/People;

    return-void
.end method

.method private constructor <init>(I)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/location/copresence/People;-><init>(ILjava/util/List;I)V

    .line 54
    return-void
.end method

.method constructor <init>(ILjava/util/List;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/gms/location/copresence/People;->c:I

    .line 44
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/location/copresence/People;->d:Ljava/util/List;

    .line 45
    iput p3, p0, Lcom/google/android/gms/location/copresence/People;->e:I

    .line 46
    return-void

    .line 44
    :cond_0
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/gms/location/copresence/People;->c:I

    return v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/People;->d:Ljava/util/List;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/google/android/gms/location/copresence/People;->e:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 135
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/t;->a(Lcom/google/android/gms/location/copresence/People;Landroid/os/Parcel;)V

    .line 136
    return-void
.end method
