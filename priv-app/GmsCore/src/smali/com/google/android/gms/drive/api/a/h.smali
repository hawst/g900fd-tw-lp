.class public final Lcom/google/android/gms/drive/api/a/h;
.super Lcom/google/android/gms/drive/api/a/d;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/api/a/d;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/h;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->k()Z

    move-result v0

    .line 30
    if-eqz v0, :cond_0

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/h;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 37
    :goto_0
    return-void

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/h;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    const-string v3, "There was an error removing some Drive apps data. Please try again later."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
