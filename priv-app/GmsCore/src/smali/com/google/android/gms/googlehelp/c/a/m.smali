.class final Lcom/google/android/gms/googlehelp/c/a/m;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Z

.field final synthetic c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/gms/googlehelp/c/a/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/googlehelp/c/a/d;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 575
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/a/m;->a:Ljava/lang/String;

    iput-boolean p3, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    iput-object p4, p0, Lcom/google/android/gms/googlehelp/c/a/m;->c:Ljava/lang/String;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 578
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->e(Z)V

    .line 595
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 581
    const-string v0, "EXTRA_IS_HANGOUT_SUPPORT_STATUS_UPDATE"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 582
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->j()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->c(Z)V

    const-string v2, "EXTRA_HANGOUT_SUPPORT_QUEUE_POSITION"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-lez v2, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/c/a/d;->g:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/c/a/d;->b:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/gms/n;->p:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v0, v3, v2, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    if-nez v2, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->e(Z)V

    goto :goto_0

    .line 583
    :cond_3
    const-string v0, "EXTRA_IS_HANGOUT_SUPPORT_UNAVAILABLE"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 584
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->g(Z)V

    goto :goto_0

    .line 585
    :cond_4
    const-string v0, "EXTRA_IS_HANGOUT_REQUEST_CANCELLED"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 586
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->d(Z)V

    goto :goto_0

    .line 587
    :cond_5
    const-string v0, "EXTRA_IS_HANGOUT_ENDED"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 588
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->i(Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->h(Z)V

    goto :goto_0

    .line 589
    :cond_6
    const-string v0, "EXTRA_IS_HANGOUT_DATA_STALE"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->g(Z)V

    goto/16 :goto_0

    .line 591
    :cond_7
    const-string v0, "EXTRA_NO_PENDING_REQUEST"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 592
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/m;->d:Lcom/google/android/gms/googlehelp/c/a/d;

    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/c/a/m;->b:Z

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->k()Lcom/google/android/gms/googlehelp/common/a;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/c/a/d;->b(Lcom/google/android/gms/googlehelp/common/a;)V

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/a/d;->i(Z)V

    goto/16 :goto_0

    :cond_8
    iget-object v2, v0, Lcom/google/android/gms/googlehelp/c/a/d;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    new-instance v3, Lcom/google/android/gms/googlehelp/c/a/l;

    invoke-direct {v3, v0}, Lcom/google/android/gms/googlehelp/c/a/l;-><init>(Lcom/google/android/gms/googlehelp/c/a/d;)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/b;)V

    goto :goto_1
.end method
