.class public final enum Lcom/google/android/gms/drive/database/model/ba;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/ba;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/ba;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/ba;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/ba;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/ba;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/ba;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/ba;

.field public static final enum h:Lcom/google/android/gms/drive/database/model/ba;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final synthetic j:[Lcom/google/android/gms/drive/database/model/ba;


# instance fields
.field private final i:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x4

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    .line 45
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "CONTENT_HASH"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "md5Hash"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v10, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "hash"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v10, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v11, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    .line 54
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "LAST_ACCESSED"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "lastAccessed"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v10, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->b:Lcom/google/android/gms/drive/database/model/ba;

    .line 60
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "SIZE_BYTES"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "size"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->c:Lcom/google/android/gms/drive/database/model/ba;

    .line 69
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "INTERNAL_FILE_NAME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "internalFileName"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->d:Lcom/google/android/gms/drive/database/model/ba;

    .line 79
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "SHARED_FILE_NAME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "sharedFileName"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    .line 86
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "ENCRYPTION_KEY"

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "encryptionKey"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->d:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->f:Lcom/google/android/gms/drive/database/model/ba;

    .line 93
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "ENCRYPTION_ALGORITHM"

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "encryptionAlgorithm"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->g:Lcom/google/android/gms/drive/database/model/ba;

    .line 105
    new-instance v0, Lcom/google/android/gms/drive/database/model/ba;

    const-string v1, "REVISION_ID"

    const/4 v2, 0x7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->d()Lcom/google/android/gms/drive/database/model/az;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x11

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "revisionId"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    const/16 v4, 0x27

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ba;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->h:Lcom/google/android/gms/drive/database/model/ba;

    .line 41
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/ba;

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->b:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->c:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->d:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v1, v0, v12

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v1, v0, v11

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/database/model/ba;->f:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/database/model/ba;->g:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/drive/database/model/ba;->h:Lcom/google/android/gms/drive/database/model/ba;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/ba;->j:[Lcom/google/android/gms/drive/database/model/ba;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 114
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ba;->i:Lcom/google/android/gms/drive/database/model/ab;

    .line 115
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ba;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/gms/drive/database/model/ba;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ba;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/ba;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->j:[Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/ba;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/ba;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ba;->i:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ba;->i:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
