.class public Lcom/google/android/gms/wallet/service/analytics/a/f;
.super Lcom/google/android/gms/wallet/service/analytics/a/h;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/k/f/a/a/r;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>()V

    .line 29
    new-instance v0, Lcom/google/k/f/a/a/r;

    invoke-direct {v0}, Lcom/google/k/f/a/a/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    .line 30
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    .line 34
    const-class v0, Lcom/google/k/f/a/a/r;

    new-instance v1, Lcom/google/k/f/a/a/r;

    invoke-direct {v1}, Lcom/google/k/f/a/a/r;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/a/a/r;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V
    .locals 3

    .prologue
    .line 56
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;

    if-eqz v0, :cond_0

    .line 57
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;

    .line 58
    iget-object v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/f;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->d:I

    iput v1, v0, Lcom/google/k/f/a/a/r;->a:I

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->e:I

    iput v1, v0, Lcom/google/k/f/a/a/r;->b:I

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->f:I

    iput v1, v0, Lcom/google/k/f/a/a/r;->c:I

    .line 62
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/analytics/events/OwInitializedEvent;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->d:Ljava/lang/String;

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    const-string v0, "OwInitializedSessionState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fill data for event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/wallet/common/ah;)V
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/android/gms/wallet/common/ah;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    .line 42
    return-void
.end method

.method protected final a(Lcom/google/k/f/a/a/j;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    iput-object v0, p1, Lcom/google/k/f/a/a/j;->j:Lcom/google/k/f/a/a/r;

    .line 47
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/f;->a:Lcom/google/k/f/a/a/r;

    iget v0, v0, Lcom/google/k/f/a/a/r;->b:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
