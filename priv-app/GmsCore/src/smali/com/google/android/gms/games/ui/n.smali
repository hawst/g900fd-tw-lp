.class public final Lcom/google/android/gms/games/ui/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;

.field public static l:Lcom/google/android/gms/common/a/d;

.field public static m:Lcom/google/android/gms/common/a/d;

.field public static n:Lcom/google/android/gms/common/a/d;

.field public static o:Lcom/google/android/gms/common/a/d;

.field public static p:Lcom/google/android/gms/common/a/d;

.field public static q:Lcom/google/android/gms/common/a/d;

.field public static r:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 20
    const-string v0, "games.activate_cheat_code"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->a:Lcom/google/android/gms/common/a/d;

    .line 24
    const-string v0, "games.google_settings_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->b:Lcom/google/android/gms/common/a/d;

    .line 28
    const-string v0, "games.play_games_help_webpage_url"

    const-string v1, "https://support.google.com/googleplay/topic/6026775"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->c:Lcom/google/android/gms/common/a/d;

    .line 33
    const-string v0, "games.play_sign_in_problems_webpage_url"

    const-string v1, "https://support.google.com/googleplay/?p=account_password"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->d:Lcom/google/android/gms/common/a/d;

    .line 38
    const-string v0, "games.play_games_dogfood"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->e:Lcom/google/android/gms/common/a/d;

    .line 42
    const-string v0, "games.play_games_id_sharing_webpage_url"

    const-string v1, "https://support.google.com/googleplay/?p=games_visibility"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->f:Lcom/google/android/gms/common/a/d;

    .line 47
    const-string v0, "games.learn_more_notifications_url"

    const-string v1, "https://support.google.com/googleplay/?p=games_notifications"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->g:Lcom/google/android/gms/common/a/d;

    .line 53
    const-string v0, "games.verbose_games_playlog_logging"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->h:Lcom/google/android/gms/common/a/d;

    .line 57
    const-string v0, "games.enable_metagame"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->i:Lcom/google/android/gms/common/a/d;

    .line 62
    const-string v0, "games.level_up_congrats_time"

    const-wide/32 v2, 0xf731400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->j:Lcom/google/android/gms/common/a/d;

    .line 67
    const-string v0, "games.allow_nearby_player_search"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->k:Lcom/google/android/gms/common/a/d;

    .line 71
    const-string v0, "games.play_games_profile_visibility_webpage_url"

    const-string v1, "https://support.google.com/googleplay/?p=game_profile_visibility"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->l:Lcom/google/android/gms/common/a/d;

    .line 76
    const-string v0, "games.play_games_nearby_players_webpage_url"

    const-string v1, "https://support.google.com/googleplay/?p=play_games_nearby"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->m:Lcom/google/android/gms/common/a/d;

    .line 82
    const-string v0, "games.use_server_game_theme_color"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->n:Lcom/google/android/gms/common/a/d;

    .line 87
    const-string v0, "games.play_now_ordering"

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->o:Lcom/google/android/gms/common/a/d;

    .line 93
    const-string v0, "games.use_in_app_purchase_flow"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->p:Lcom/google/android/gms/common/a/d;

    .line 99
    const-string v0, "games.play_now_card_density"

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->q:Lcom/google/android/gms/common/a/d;

    .line 105
    const-string v0, "games.use_warm_welcome_flow"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/n;->r:Lcom/google/android/gms/common/a/d;

    return-void
.end method
