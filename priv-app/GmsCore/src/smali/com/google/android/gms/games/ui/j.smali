.class public final Lcom/google/android/gms/games/ui/j;
.super Lcom/google/android/gms/games/ui/cw;
.source "SourceFile"


# static fields
.field private static final f:I


# instance fields
.field protected c:Lcom/google/android/gms/games/ui/k;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/google/android/gms/l;->aL:I

    sput v0, Lcom/google/android/gms/games/ui/j;->f:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cw;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/j;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/gms/games/ui/j;->h:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/cx;
    .locals 4

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/games/ui/l;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/j;->e:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/j;->f:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/l;-><init>(Lcom/google/android/gms/games/ui/j;Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/ui/k;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/games/ui/j;->c:Lcom/google/android/gms/games/ui/k;

    .line 37
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 41
    sget v0, Lcom/google/android/gms/games/ui/j;->f:I

    return v0
.end method

.method public final f(I)V
    .locals 0

    .prologue
    .line 32
    iput p1, p0, Lcom/google/android/gms/games/ui/j;->h:I

    .line 33
    return-void
.end method
