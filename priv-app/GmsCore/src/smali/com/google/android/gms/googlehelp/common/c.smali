.class final Lcom/google/android/gms/googlehelp/common/c;
.super Lcom/google/android/gms/googlehelp/f/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/googlehelp/common/a;

.field private final b:Lcom/google/android/gms/googlehelp/common/b;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/googlehelp/common/HelpConfig;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/common/b;Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/f/c;-><init>()V

    .line 148
    new-instance v0, Lcom/google/android/gms/googlehelp/common/a;

    invoke-direct {v0, p2, p3}, Lcom/google/android/gms/googlehelp/common/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->a:Lcom/google/android/gms/googlehelp/common/a;

    .line 150
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/c;->b:Lcom/google/android/gms/googlehelp/common/b;

    .line 151
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/c;->c:Landroid/content/Context;

    .line 152
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/common/c;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 153
    return-void
.end method

.method private varargs a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v0

    .line 158
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/c;->c:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 159
    :cond_0
    const-string v0, ""

    .line 177
    :goto_0
    return-object v0

    .line 162
    :cond_1
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 169
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/common/c;->a(Ljava/lang/String;)V

    .line 172
    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    .line 174
    :goto_1
    const-string v1, "GoogleHelp_SetAccountSpecificPrefsAccountIdTask"

    const-string v2, "Failed to get account ID."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 177
    const-string v0, ""

    goto :goto_0

    .line 173
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 12

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->c:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/AccountChangeEvent;

    .line 184
    invoke-virtual {v0}, Lcom/google/android/gms/auth/AccountChangeEvent;->a()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_0

    .line 186
    invoke-virtual {v0}, Lcom/google/android/gms/auth/AccountChangeEvent;->c()Ljava/lang/String;

    move-result-object v1

    .line 187
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 188
    new-instance v3, Lcom/google/android/gms/googlehelp/common/a;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/common/c;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/common/c;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/googlehelp/common/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 193
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/googlehelp/common/a;->a:Ljava/lang/String;

    .line 194
    const-string v4, "last_seen_account_change_index"

    const/4 v5, -0x1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;I)I

    move-result v3

    .line 197
    invoke-virtual {v0}, Lcom/google/android/gms/auth/AccountChangeEvent;->b()I

    move-result v0

    if-ge v3, v0, :cond_0

    .line 198
    invoke-direct {p0, v1}, Lcom/google/android/gms/googlehelp/common/c;->a(Ljava/lang/String;)V

    .line 206
    new-instance v0, Lcom/google/android/gms/googlehelp/common/a;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/common/c;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/common/c;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/googlehelp/common/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/gms/googlehelp/common/a;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/googlehelp/common/a;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/common/c;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-direct {v4, v0, v5}, Lcom/google/android/gms/googlehelp/common/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/googlehelp/common/a;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    const/16 v1, 0xe

    new-array v1, v1, [Landroid/util/Pair;

    const/4 v6, 0x0

    new-instance v7, Landroid/util/Pair;

    const-string v8, "last_seen_account_change_index"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/4 v6, 0x1

    new-instance v7, Landroid/util/Pair;

    const-string v8, "ongoing_session_last_stopped_ms"

    const-wide/16 v10, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/4 v6, 0x2

    new-instance v7, Landroid/util/Pair;

    const-string v8, "ongoing_session_id"

    const-string v9, ""

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/4 v6, 0x3

    new-instance v7, Landroid/util/Pair;

    const-string v8, "ongoing_chat_request_pool_id"

    const-string v9, ""

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/4 v6, 0x4

    new-instance v7, Landroid/util/Pair;

    const-string v8, "ongoing_video_request_pool_id"

    const-string v9, ""

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/4 v6, 0x5

    new-instance v7, Landroid/util/Pair;

    const-string v8, "hangout_was_opened"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/4 v6, 0x6

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v8

    const-wide/16 v10, -0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/4 v6, 0x7

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v8

    const-wide/16 v10, -0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/16 v6, 0x8

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/16 v6, 0x9

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->d(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v8

    const-string v9, ""

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/16 v6, 0xa

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->e(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/16 v6, 0xb

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->f(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, -0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/16 v6, 0xc

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->g(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v7, v8, v9}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    const/16 v6, 0xd

    new-instance v7, Landroid/util/Pair;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/u;->h(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v0

    const/4 v8, 0x0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-direct {v7, v0, v8}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    aput-object v7, v1, v6

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/googlehelp/common/a;->b(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    instance-of v7, v0, Ljava/lang/Boolean;

    if-eqz v7, :cond_3

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v4, v1, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Z)Lcom/google/android/gms/googlehelp/common/h;

    :cond_2
    :goto_2
    invoke-virtual {v5, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    goto :goto_1

    :cond_3
    instance-of v7, v0, Ljava/lang/String;

    if-eqz v7, :cond_4

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v1, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    goto :goto_2

    :cond_4
    instance-of v7, v0, Ljava/lang/Integer;

    if-eqz v7, :cond_5

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v1, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {v3, v1, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/common/h;

    goto :goto_2

    :cond_5
    instance-of v7, v0, Ljava/lang/Long;

    if-eqz v7, :cond_6

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v4, v1, v8, v9}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v8

    invoke-virtual {v3, v1, v8, v9}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;J)Lcom/google/android/gms/googlehelp/common/h;

    goto :goto_2

    :cond_6
    instance-of v0, v0, Ljava/lang/Float;

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Float not supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    const-string v0, "is_account_in_prefs"

    invoke-virtual {v5, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    iget-object v0, v5, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    .line 209
    :cond_8
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 131
    check-cast p1, Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->a:Lcom/google/android/gms/googlehelp/common/a;

    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/a;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->a:Lcom/google/android/gms/googlehelp/common/a;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "is_account_in_prefs"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Z)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/c;->b:Lcom/google/android/gms/googlehelp/common/b;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/c;->a:Lcom/google/android/gms/googlehelp/common/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/googlehelp/common/b;->a(Lcom/google/android/gms/googlehelp/common/a;)V

    return-void
.end method
