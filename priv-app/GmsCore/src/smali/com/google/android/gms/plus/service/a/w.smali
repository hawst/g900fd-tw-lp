.class public final Lcom/google/android/gms/plus/service/a/w;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/f;

.field private final c:I

.field private final d:I

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/w;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 37
    iput p2, p0, Lcom/google/android/gms/plus/service/a/w;->c:I

    .line 38
    iput p3, p0, Lcom/google/android/gms/plus/service/a/w;->d:I

    .line 39
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/w;->e:Ljava/lang/String;

    .line 40
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/w;->b:Lcom/google/android/gms/plus/internal/f;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/w;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lcom/google/android/gms/plus/service/a/w;->c:I

    iget v2, p0, Lcom/google/android/gms/plus/service/a/w;->d:I

    iget-object v3, p0, Lcom/google/android/gms/plus/service/a/w;->e:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v4, v0, v1, v2, v3}, Lcom/google/android/gms/plus/b/n;->a(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/w;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 60
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 50
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 51
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 52
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/w;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v7, v1, v5}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    goto :goto_0

    .line 55
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/w;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/w;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v7, v0, v5}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    goto :goto_0

    .line 58
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/w;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v5, v5}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/w;->b:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/w;->b:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    .line 67
    :cond_0
    return-void
.end method
