.class public final Lcom/google/android/gms/games/ui/common/leaderboards/n;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 654
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 661
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/n;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 662
    sget v1, Lcom/google/android/gms/p;->jr:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 664
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 665
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 666
    sget v0, Lcom/google/android/gms/p;->jp:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 667
    const v0, 0x104000a

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 668
    const/high16 v0, 0x1040000

    const/4 v1, 0x0

    invoke-virtual {v2, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 669
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/n;->a(Z)V

    .line 670
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 675
    packed-switch p2, :pswitch_data_0

    .line 683
    const-string v0, "LeaderboardScoreFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled dialog action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 686
    :goto_0
    return-void

    .line 677
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/n;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;

    .line 679
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;->a(Lcom/google/android/gms/games/ui/common/leaderboards/LeaderboardScoreFragment;)Lcom/google/android/gms/games/ui/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->t()V

    goto :goto_0

    .line 675
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch
.end method
