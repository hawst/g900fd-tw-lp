.class public final Lcom/google/android/gms/plus/audience/a;
.super Lcom/google/android/gms/plus/audience/ac;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/common/audience/widgets/d;
.implements Lcom/google/android/gms/plus/audience/bh;


# instance fields
.field private o:I

.field private p:Landroid/widget/LinearLayout;

.field private q:Landroid/widget/TextView;

.field private r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

.field private s:Landroid/widget/CompoundButton;

.field private t:Landroid/view/View;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:I

.field private x:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/ac;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/util/List;)Lcom/google/android/gms/plus/audience/a;
    .locals 18

    .prologue
    .line 88
    new-instance v17, Lcom/google/android/gms/plus/audience/a;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/gms/plus/audience/a;-><init>()V

    .line 89
    const/4 v7, 0x0

    const/4 v11, 0x1

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    move/from16 v3, p2

    move/from16 v4, p3

    move/from16 v5, p4

    move/from16 v6, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v14, p12

    move/from16 v15, p13

    move-object/from16 v16, p14

    invoke-static/range {v1 .. v16}, Lcom/google/android/gms/plus/audience/ac;->a(Ljava/lang/String;Ljava/lang/String;ZZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/util/List;)Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "domainRestricted"

    move/from16 v0, p9

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a;->setArguments(Landroid/os/Bundle;)V

    .line 93
    return-object v17
.end method

.method private a(Ljava/lang/String;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pa:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 254
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->oZ:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 256
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->tz:I

    new-array v4, v6, [Ljava/lang/Object;

    aput-object p1, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->ty:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p1, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setVisibility(I)V

    .line 263
    return-void
.end method

.method private p()V
    .locals 4

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->q:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_1

    .line 267
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 269
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 270
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->q:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/ac;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-static {v3, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 275
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 2

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, p1}, Lcom/google/android/gms/common/people/data/g;->b(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 292
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/model/g;)V
    .locals 2

    .prologue
    .line 243
    invoke-interface {p1}, Lcom/google/android/gms/people/model/g;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 245
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->tx:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    .line 247
    :cond_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    if-eqz v0, :cond_1

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/a;->a(Ljava/lang/String;I)V

    .line 250
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 296
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 297
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/a;->p()V

    .line 298
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 232
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/a;->u:Z

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    if-eqz v0, :cond_0

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Z)V

    .line 236
    :cond_0
    return-void
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 285
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/a;->x:Z

    .line 286
    return-void
.end method

.method public final c()Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    .line 167
    new-instance v0, Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/google/android/gms/common/audience/widgets/AudienceView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/audience/widgets/d;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->b(Z)V

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    iget-boolean v4, p0, Lcom/google/android/gms/plus/audience/a;->u:Z

    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/audience/widgets/AudienceView;->a(Z)V

    .line 173
    sget v0, Lcom/google/android/gms/l;->dy:I

    invoke-virtual {v3, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->p:Landroid/widget/LinearLayout;

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->p:Landroid/widget/LinearLayout;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/a;->r:Lcom/google/android/gms/common/audience/widgets/AudienceView;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->p:Landroid/widget/LinearLayout;

    sget v4, Lcom/google/android/gms/j;->qQ:I

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->p:Landroid/widget/LinearLayout;

    sget v4, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->q:Landroid/widget/TextView;

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ac;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->q:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    :goto_0
    iget v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    if-eqz v0, :cond_0

    .line 187
    iget v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    if-ne v0, v1, :cond_3

    .line 188
    :goto_1
    sget v0, Lcom/google/android/gms/l;->do:I

    invoke-virtual {v3, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->pq:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->s:Landroid/widget/CompoundButton;

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/plus/audience/a;->w:I

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/plus/audience/a;->a(Ljava/lang/String;I)V

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->s:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->s:Landroid/widget/CompoundButton;

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->p:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 202
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/a;->x:Z

    if-eqz v0, :cond_1

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->p:Landroid/widget/LinearLayout;

    sget v1, Lcom/google/android/gms/j;->qQ:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 206
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->p:Landroid/widget/LinearLayout;

    return-object v0

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->q:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/ac;->i:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/a;->p()V

    goto :goto_0

    :cond_3
    move v1, v2

    .line 187
    goto :goto_1
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 210
    iget v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    return v0
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->s:Landroid/widget/CompoundButton;

    if-ne p1, v0, :cond_0

    .line 216
    if-eqz p2, :cond_1

    move v0, v6

    :goto_0
    iput v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    .line 217
    if-eqz p2, :cond_2

    sget-object v3, Lcom/google/android/gms/common/analytics/u;->C:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 220
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/al;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/al;->k:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/al;->m:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 224
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    .line 225
    new-instance v1, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/people/data/a;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    if-eqz p2, :cond_3

    :goto_2
    invoke-virtual {v1, v6}, Lcom/google/android/gms/common/people/data/a;->a(I)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    .line 227
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 229
    :cond_0
    return-void

    :cond_1
    move v0, v7

    .line 216
    goto :goto_0

    .line 217
    :cond_2
    sget-object v3, Lcom/google/android/gms/common/analytics/u;->D:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1

    :cond_3
    move v6, v7

    .line 225
    goto :goto_2
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 279
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->qQ:I

    if-ne v0, v1, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->g()Lcom/google/android/gms/plus/audience/ae;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->onSearchRequested()Z

    .line 282
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x4

    .line 114
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ac;->onCreate(Landroid/os/Bundle;)V

    .line 116
    if-eqz p1, :cond_1

    .line 117
    const-string v0, "domainRestricted"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    .line 118
    const-string v0, "domain"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    .line 119
    const-string v0, "domainRestrictedVisibility"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/a;->w:I

    .line 121
    const-string v0, "hideSearchIcon"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a;->x:Z

    .line 122
    const-string v0, "underage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/a;->u:Z

    .line 130
    :goto_0
    if-nez p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/al;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/al;->k:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/common/analytics/u;->B:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/al;->m:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 136
    :cond_0
    return-void

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 125
    const-string v1, "domainRestricted"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    .line 126
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    .line 127
    iput v2, p0, Lcom/google/android/gms/plus/audience/a;->w:I

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/ac;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 153
    const-string v0, "domainRestricted"

    iget v1, p0, Lcom/google/android/gms/plus/audience/a;->o:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 154
    const-string v0, "domain"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a;->v:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 156
    const-string v0, "domainRestrictedVisibility"

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a;->t:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 159
    :cond_0
    const-string v0, "hideSearchIcon"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/a;->x:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    const-string v0, "underage"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/a;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 161
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->onStart()V

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 142
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/a;->j()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->b(Lcom/google/android/gms/plus/audience/bh;)V

    .line 147
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/ac;->onStop()V

    .line 148
    return-void
.end method
