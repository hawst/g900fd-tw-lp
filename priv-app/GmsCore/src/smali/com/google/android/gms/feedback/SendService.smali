.class public Lcom/google/android/gms/feedback/SendService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Handler;

.field private b:Lcom/google/android/gms/feedback/aj;

.field private c:Z

.field private d:Z

.field private e:Lcom/google/android/gms/feedback/aa;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 44
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/feedback/SendService;->a:Landroid/os/Handler;

    .line 128
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/SendService;)Lcom/google/android/gms/feedback/aa;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->e:Lcom/google/android/gms/feedback/aa;

    return-object v0
.end method

.method private a()V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcom/google/android/gms/feedback/aj;

    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->c:Z

    .line 89
    const-string v0, "GoogleFeedbackSendService"

    const-string v1, "starting report scan"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SendService;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "reports"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 91
    new-instance v1, Lcom/google/android/gms/feedback/aj;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/feedback/aj;-><init>(Lcom/google/android/gms/feedback/SendService;Ljava/io/File;)V

    iput-object v1, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcom/google/android/gms/feedback/aj;

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcom/google/android/gms/feedback/aj;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/aj;->start()V

    .line 97
    :goto_0
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->c:Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/SendService;ZZ)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/feedback/SendService;->b:Lcom/google/android/gms/feedback/aj;

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->c:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/feedback/SendService;->a()V

    :goto_0
    return-void

    :cond_0
    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {p0, v0}, Lcom/google/android/gms/feedback/FeedbackConnectivityReceiver;->a(Landroid/content/Context;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/feedback/SendService;->stopSelf()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/feedback/SendService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/feedback/SendService;->a:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 66
    new-instance v0, Lcom/google/android/gms/feedback/aa;

    invoke-direct {v0, p0}, Lcom/google/android/gms/feedback/aa;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/SendService;->e:Lcom/google/android/gms/feedback/aa;

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->d:Z

    .line 68
    invoke-direct {p0}, Lcom/google/android/gms/feedback/SendService;->a()V

    .line 69
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Landroid/app/Service;->onStart(Landroid/content/Intent;I)V

    .line 75
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->d:Z

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/SendService;->d:Z

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/feedback/SendService;->a()V

    goto :goto_0
.end method
