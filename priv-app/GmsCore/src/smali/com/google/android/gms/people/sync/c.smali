.class public final Lcom/google/android/gms/people/sync/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/gms/people/f/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/f/d;->a(Landroid/content/Context;)Lcom/google/android/gms/people/f/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    .line 32
    return-void
.end method

.method public static a(JZ)Ljava/lang/String;
    .locals 2

    .prologue
    .line 236
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DEFAULT-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v0, "-PAGE"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method


# virtual methods
.method public final a(IZ)Lcom/google/android/gms/people/f/a;
    .locals 12

    .prologue
    const/4 v9, 0x0

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v3

    .line 210
    invoke-virtual {v3, p2}, Lcom/google/android/gms/people/a/c;->c(Z)J

    move-result-wide v4

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->a(Landroid/content/Context;)Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    const-wide/16 v6, 0x3e8

    div-long v6, v0, v6

    .line 212
    if-nez p2, :cond_1

    sget-object v0, Lcom/google/android/gms/people/a/a;->ah:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    .line 213
    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 214
    invoke-static {v4, v5, p2}, Lcom/google/android/gms/people/sync/c;->a(JZ)Ljava/lang/String;

    move-result-object v2

    .line 215
    sub-long v4, v6, v4

    sget-object v0, Lcom/google/android/gms/people/a/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v4, v10

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-virtual {v0, v2, v9, v8}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 219
    :cond_0
    invoke-static {v6, v7, p2}, Lcom/google/android/gms/people/sync/c;->a(JZ)Ljava/lang/String;

    move-result-object v0

    .line 220
    const/4 v4, 0x1

    invoke-virtual {p0, v0, v9, v1, v4}, Lcom/google/android/gms/people/sync/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 222
    iget-object v4, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-virtual {v4, v0, v9, v8}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 226
    iget-object v4, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-virtual {v4, v2, v9}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    invoke-static {v9}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    iget-object v2, v3, Lcom/google/android/gms/people/a/c;->a:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {p2}, Lcom/google/android/gms/people/a/c;->b(Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 231
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-static {v1}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v9, v1, p1}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/people/f/a;

    move-result-object v0

    return-object v0

    .line 212
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/a/a;->ai:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    :cond_2
    move-object v0, v2

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-static {p3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 190
    :cond_0
    :goto_0
    return-void

    .line 183
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/service/m;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/people/service/m;->c(Ljava/lang/String;Z)[B

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    if-eq v0, v1, :cond_0

    sget-object v1, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    if-eq v0, v1, :cond_0

    .line 187
    iget-object v1, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-static {p3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2, v0}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Z

    goto :goto_0
.end method

.method final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-static {p3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, p1, p2, v2}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v0

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/sync/c;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/service/m;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/m;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/google/android/gms/people/service/m;->b(Ljava/lang/String;)[B

    move-result-object v1

    .line 161
    if-eqz v1, :cond_2

    sget-object v2, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    if-eq v1, v2, :cond_2

    sget-object v2, Lcom/google/android/gms/people/service/a/a/e;->j:[B

    if-eq v1, v2, :cond_2

    .line 163
    iget-object v2, p0, Lcom/google/android/gms/people/sync/c;->b:Lcom/google/android/gms/people/f/d;

    invoke-static {p3}, Lcom/google/android/gms/people/f/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, p2, v3, v1}, Lcom/google/android/gms/people/f/d;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)Z

    goto :goto_0

    .line 166
    :cond_2
    sget-object v2, Lcom/google/android/gms/people/service/a/a/e;->i:[B

    if-ne v1, v2, :cond_0

    .line 170
    const/4 v0, 0x0

    goto :goto_0
.end method
