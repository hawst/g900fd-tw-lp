.class final Lcom/google/android/gms/smart_profile/i;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v4/app/Fragment;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Landroid/content/res/Resources;

.field private d:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/smart_profile/SmartProfilePerson;)V
    .locals 1

    .prologue
    .line 108
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 109
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/i;->a:Landroid/support/v4/app/Fragment;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/i;->b:Landroid/view/LayoutInflater;

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/i;->c:Landroid/content/res/Resources;

    .line 112
    invoke-direct {p0, p2}, Lcom/google/android/gms/smart_profile/i;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;)V

    .line 113
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/i;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->a:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;)V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v3, 0x0

    .line 174
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->r()Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    :cond_0
    :goto_0
    return-void

    .line 177
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/i;->d:Ljava/util/List;

    .line 178
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->q()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;

    .line 179
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 180
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;->b()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "cp2"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 182
    new-instance v7, Lcom/google/android/gms/smart_profile/k;

    invoke-direct {v7, v10}, Lcom/google/android/gms/smart_profile/k;-><init>(B)V

    .line 183
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->d()Ljava/lang/String;

    move-result-object v0

    .line 184
    iput-object v0, v7, Lcom/google/android/gms/smart_profile/k;->b:Ljava/lang/String;

    .line 185
    iget-object v4, p0, Lcom/google/android/gms/smart_profile/i;->a:Landroid/support/v4/app/Fragment;

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "data15"

    aput-object v1, v2, v10

    sget-object v1, Landroid/provider/ContactsContract$Contacts;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v1, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "photo"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v4}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "data15"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    :goto_2
    iput-object v0, v7, Lcom/google/android/gms/smart_profile/k;->c:[B

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->d:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v3

    .line 185
    goto :goto_2

    .line 191
    :cond_4
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/i;->b(Lcom/google/android/gms/smart_profile/SmartProfilePerson;)V

    goto/16 :goto_0
.end method

.method private b(Lcom/google/android/gms/smart_profile/SmartProfilePerson;)V
    .locals 6

    .prologue
    .line 195
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 216
    :cond_0
    return-void

    .line 198
    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 199
    invoke-virtual {p1}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->u()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;

    .line 200
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->c()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 201
    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->d()Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;

    move-result-object v3

    .line 202
    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->c()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->b()Ljava/lang/String;

    move-result-object v4

    const-string v5, "cp2"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 206
    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/models/DefaultMetadataImpl;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 210
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/k;

    .line 211
    iget-object v1, v0, Lcom/google/android/gms/smart_profile/k;->b:Ljava/lang/String;

    .line 212
    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 213
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/smart_profile/k;->a:Ljava/lang/String;

    goto :goto_1
.end method


# virtual methods
.method public final getCount()I
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 127
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 133
    if-nez p2, :cond_0

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->fj:I

    invoke-virtual {v0, v1, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 138
    new-instance v1, Lcom/google/android/gms/smart_profile/l;

    invoke-direct {v1, v7}, Lcom/google/android/gms/smart_profile/l;-><init>(B)V

    .line 139
    sget v0, Lcom/google/android/gms/j;->by:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/smart_profile/l;->a:Landroid/widget/ImageView;

    .line 140
    sget v0, Lcom/google/android/gms/j;->ex:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/smart_profile/l;->b:Landroid/widget/TextView;

    .line 141
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 146
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/i;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/k;

    .line 149
    iget-object v2, v0, Lcom/google/android/gms/smart_profile/k;->c:[B

    .line 150
    if-eqz v2, :cond_1

    .line 151
    iget-object v3, v1, Lcom/google/android/gms/smart_profile/l;->a:Landroid/widget/ImageView;

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/i;->c:Landroid/content/res/Resources;

    array-length v6, v2

    invoke-static {v2, v7, v6}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 161
    :goto_1
    iget-object v1, v1, Lcom/google/android/gms/smart_profile/l;->b:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/google/android/gms/smart_profile/k;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    new-instance v1, Lcom/google/android/gms/smart_profile/j;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/smart_profile/j;-><init>(Lcom/google/android/gms/smart_profile/i;Lcom/google/android/gms/smart_profile/k;)V

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 170
    return-object p2

    .line 143
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/l;

    move-object v1, v0

    goto :goto_0

    .line 156
    :cond_1
    iget-object v2, v1, Lcom/google/android/gms/smart_profile/l;->a:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/i;->c:Landroid/content/res/Resources;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/i;->c:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/h;->cY:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method
