.class public final Lcom/google/android/gms/people/d/g;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 548
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 549
    iput v0, p0, Lcom/google/android/gms/people/d/g;->a:I

    iput v0, p0, Lcom/google/android/gms/people/d/g;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/g;->cachedSize:I

    .line 550
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 599
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 600
    iget v1, p0, Lcom/google/android/gms/people/d/g;->a:I

    if-eqz v1, :cond_0

    .line 601
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/people/d/g;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 604
    :cond_0
    iget v1, p0, Lcom/google/android/gms/people/d/g;->b:I

    if-eqz v1, :cond_1

    .line 605
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/people/d/g;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 608
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 561
    if-ne p1, p0, :cond_1

    .line 574
    :cond_0
    :goto_0
    return v0

    .line 564
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 565
    goto :goto_0

    .line 567
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/g;

    .line 568
    iget v2, p0, Lcom/google/android/gms/people/d/g;->a:I

    iget v3, p1, Lcom/google/android/gms/people/d/g;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 569
    goto :goto_0

    .line 571
    :cond_3
    iget v2, p0, Lcom/google/android/gms/people/d/g;->b:I

    iget v3, p1, Lcom/google/android/gms/people/d/g;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 572
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 579
    iget v0, p0, Lcom/google/android/gms/people/d/g;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 581
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/g;->b:I

    add-int/2addr v0, v1

    .line 582
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 525
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/g;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/g;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 588
    iget v0, p0, Lcom/google/android/gms/people/d/g;->a:I

    if-eqz v0, :cond_0

    .line 589
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/people/d/g;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 591
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/d/g;->b:I

    if-eqz v0, :cond_1

    .line 592
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/people/d/g;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 594
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 595
    return-void
.end method
