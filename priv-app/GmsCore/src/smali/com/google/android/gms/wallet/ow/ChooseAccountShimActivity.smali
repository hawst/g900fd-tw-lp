.class public Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method private a()V
    .locals 4

    .prologue
    .line 318
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 319
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_GET_MASKED_WALLET"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xc8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 332
    :goto_0
    return-void

    .line 321
    :cond_0
    const-string v1, "com.google.android.gms.wallet.payform.ACTION_REQUEST_AUTO_COMPLETE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 322
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x190

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 323
    :cond_1
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_AUTHENTICATE_INSTRUMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 324
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->b()V

    goto :goto_0

    .line 325
    :cond_2
    const-string v1, "com.google.android.gms.wallet.ACTION_REVIEW_PURCHASE_OPTIONS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.google.android.gms.wallet.ACTION_START_BILLING_ENROLLMENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 327
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 328
    :cond_4
    const-string v1, "com.google.android.gms.identity.REQUEST_USER_ADDRESS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 330
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.identity.intents.EXTRA_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/identity/intents/UserAddressRequest;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v1, v2, v0}, Lcom/google/android/gms/wallet/address/RequestUserAddressActivity;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/identity/intents/UserAddressRequest;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x258

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 331
    :cond_5
    const-string v1, "com.google.android.gms.wallet.onlinewallet.ACTION_CREATE_WALLET_OBJECTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 332
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x2bc

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 334
    :cond_6
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected action="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/RootActivity;->a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    .line 340
    const/16 v1, 0x12c

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 341
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 192
    sparse-switch p1, :sswitch_data_0

    .line 224
    const-string v0, "ChooseAccountShimActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown request code "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", result code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->onActivityResult(IILandroid/content/Intent;)V

    .line 228
    :goto_0
    return-void

    .line 194
    :sswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 195
    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    const-string v1, "accountType"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    new-instance v2, Landroid/accounts/Account;

    invoke-direct {v2, v0, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    .line 201
    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/a;->a(Z)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/shared/a;->a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    .line 206
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/shared/d;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/d;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 209
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a()V

    goto :goto_0

    .line 211
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/google/android/gms/wallet/MaskedWallet;->a()Lcom/google/android/gms/wallet/o;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    const-string v0, "com.google.android.gms.wallet.EXTRA_REQUEST"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/MaskedWalletRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/o;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    :cond_1
    const-string v0, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/o;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/o;

    :cond_2
    const-string v0, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    iget-object v2, v2, Lcom/google/android/gms/wallet/o;->a:Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->finish()V

    goto :goto_0

    .line 220
    :sswitch_1
    invoke-virtual {p0, p2, p3}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->setResult(ILandroid/content/Intent;)V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->finish()V

    goto :goto_0

    .line 192
    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x12c -> :sswitch_1
        0x190 -> :sswitch_1
        0x1f4 -> :sswitch_1
        0x1f5 -> :sswitch_0
        0x258 -> :sswitch_1
        0x2bc -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 85
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    .line 87
    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    .line 88
    const-string v1, "com.google.android.gms.wallet.payform.ACTION_REQUEST_AUTO_COMPLETE"

    invoke-static {v1, v6}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.wallet.ACTION_REVIEW_PURCHASE_OPTIONS"

    invoke-static {v1, v6}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "com.google.android.gms.wallet.ACTION_START_BILLING_ENROLLMENT"

    invoke-static {v1, v6}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 91
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getIntent()Landroid/content/Intent;

    move-result-object v7

    invoke-virtual {v7}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.wallet.payform.ACTION_REQUEST_AUTO_COMPLETE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "com.google.android.gms.wallet.EXTRA_PARAMETERS"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    if-nez v1, :cond_11

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object v2, v1

    :goto_0
    const-string v1, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v7, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/aa;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    move-result-object v1

    const-string v8, "com.google.android.gms.wallet.EXTRA_ENVIRONMENT"

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->b()I

    move-result v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v8, "com.google.android.gms.wallet.EXTRA_ALLOW_ACCOUNT_SELECTION"

    const/4 v9, 0x1

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v8, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->c()Landroid/accounts/Account;

    move-result-object v9

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v8, "com.google.android.gms.wallet.EXTRA_THEME"

    const/4 v9, 0x1

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v8, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v7, v8, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.EXTRA_PARAMETERS"

    invoke-virtual {v7, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p0, v7}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->setIntent(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :cond_2
    new-instance v1, Lcom/google/android/gms/wallet/ow/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/ow/f;-><init>(Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;)V

    invoke-static {v1}, Ljava/lang/Thread;->setDefaultUncaughtExceptionHandler(Ljava/lang/Thread$UncaughtExceptionHandler;)V

    .line 122
    if-eqz p1, :cond_3

    .line 124
    const-string v0, "buyFlowConfig"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 181
    :goto_1
    return-void

    .line 92
    :catch_0
    move-exception v0

    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->setResult(I)V

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->finish()V

    goto :goto_1

    .line 127
    :cond_3
    const-string v1, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 129
    const-string v1, "com.google.android.gms.identity.REQUEST_USER_ADDRESS"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 130
    const-string v1, "com.google.android.gms.identity.intents.EXTA_CONFIG"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 133
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    if-nez v1, :cond_a

    .line 134
    invoke-virtual {p0}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.wallet.payform.ACTION_REQUEST_AUTO_COMPLETE"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v2, "payform"

    const-string v1, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v5, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    :goto_2
    const-string v7, "com.google.android.gms.wallet.EXTRA_PARAMETERS"

    invoke-virtual {v5, v7}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/wallet/common/c;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v5

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v7

    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a()Lcom/google/android/gms/wallet/shared/d;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/google/android/gms/wallet/shared/d;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v8

    invoke-virtual {v8, v2}, Lcom/google/android/gms/wallet/shared/d;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/wallet/shared/d;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v2

    invoke-static {v7, v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/shared/d;->d(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/d;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 141
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v1

    .line 142
    sget-object v2, Lcom/google/android/gms/wallet/w;->a:Landroid/accounts/Account;

    invoke-virtual {v2, v1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v2

    if-nez v2, :cond_6

    move-object v1, v0

    .line 148
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v2}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v5}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v5

    const-string v7, "onlinewallet_account_shim_activity"

    invoke-static {v2, v5, v7}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    const-string v2, "com.google.android.gms.wallet.onlinewallet.ACTION_AUTHENTICATE_INSTRUMENT"

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 153
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->b()V

    goto/16 :goto_1

    .line 134
    :cond_7
    const-string v2, "com.google.android.gms.wallet.onlinewallet.ACTION_GET_MASKED_WALLET"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "com.google.android.gms.wallet.onlinewallet.ACTION_AUTHENTICATE_INSTRUMENT"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    :cond_8
    const-string v1, "onlinewallet"

    move-object v2, v1

    move-object v1, v0

    goto :goto_2

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected action="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_a
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 138
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "package name mismatch"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_b
    if-nez v1, :cond_d

    .line 159
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-static {p0}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    .line 160
    if-eqz v2, :cond_d

    array-length v5, v2

    if-lez v5, :cond_d

    array-length v5, v2

    if-eq v5, v4, :cond_c

    const-string v5, "com.google.android.gms.wallet.onlinewallet.ACTION_CREATE_WALLET_OBJECTS"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    .line 162
    :cond_c
    aget-object v1, v2, v3

    .line 163
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    .line 164
    invoke-static {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/google/android/gms/wallet/shared/a;->a(Z)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/shared/a;->a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    .line 169
    iget-object v5, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v5}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/google/android/gms/wallet/shared/d;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/d;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 174
    :cond_d
    if-nez v1, :cond_f

    .line 175
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-ge v1, v2, :cond_e

    move v9, v4

    :goto_3
    new-array v2, v4, [Ljava/lang/String;

    const-string v1, "com.google"

    aput-object v1, v2, v3

    move-object v1, v0

    move-object v4, v0

    move-object v5, v0

    move-object v6, v0

    move-object v7, v0

    move v8, v3

    move v10, v3

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/common/a;->a(Landroid/accounts/Account;Ljava/util/ArrayList;[Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;ZII)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "account_chooser"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e()I

    move-result v1

    if-ne v1, v4, :cond_10

    move v9, v4

    goto :goto_3

    .line 177
    :cond_f
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a()V

    goto/16 :goto_1

    :cond_10
    move v9, v3

    goto :goto_3

    :cond_11
    move-object v2, v1

    goto/16 :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 185
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 187
    const-string v0, "buyFlowConfig"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 188
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 391
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/ChooseAccountShimActivity;->b:Ljava/lang/Integer;

    .line 392
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 393
    return-void
.end method
