.class final Lcom/google/android/gms/cast/media/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public b:J

.field public c:J

.field public d:Landroid/app/PendingIntent;

.field final synthetic e:Lcom/google/android/gms/cast/media/o;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/media/o;)V
    .locals 2

    .prologue
    .line 1176
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/cast/media/w;-><init>(Lcom/google/android/gms/cast/media/o;J)V

    .line 1177
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/cast/media/o;J)V
    .locals 2

    .prologue
    .line 1169
    iput-object p1, p0, Lcom/google/android/gms/cast/media/w;->e:Lcom/google/android/gms/cast/media/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1170
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "media-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    .line 1171
    iput-wide p2, p0, Lcom/google/android/gms/cast/media/w;->b:J

    .line 1172
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/cast/media/w;->c:J

    .line 1173
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 1181
    const-string v0, "TrackedItem(itemId=%s, loadRequestId=%d, mediaSessionId=%d)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lcom/google/android/gms/cast/media/w;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/cast/media/w;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
