.class public final Lcom/google/android/gms/fitness/settings/e;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/fitness/settings/e;
    .locals 3

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/fitness/settings/e;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/settings/e;-><init>()V

    .line 27
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 28
    const-string v2, "account"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 29
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/settings/e;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v0
.end method

.method static synthetic a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 22
    const-string v0, "Failed to delete user\'s data"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    sget v0, Lcom/google/android/gms/p;->ho:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/e;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 37
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->hi:I

    sget v4, Lcom/google/android/gms/p;->hj:I

    new-instance v5, Lcom/google/android/gms/fitness/settings/f;

    invoke-direct {v5, p0, v0, v1}, Lcom/google/android/gms/fitness/settings/f;-><init>(Lcom/google/android/gms/fitness/settings/e;Ljava/lang/String;Landroid/content/Context;)V

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gms/common/ui/d;->a(Landroid/app/Activity;IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
