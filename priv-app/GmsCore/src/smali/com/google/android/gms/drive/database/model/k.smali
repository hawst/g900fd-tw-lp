.class public final enum Lcom/google/android/gms/drive/database/model/k;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/k;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/k;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/k;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/k;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/k;

.field private static final synthetic g:[Lcom/google/android/gms/drive/database/model/k;


# instance fields
.field private final f:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x6

    const/4 v7, 0x1

    .line 41
    new-instance v0, Lcom/google/android/gms/drive/database/model/k;

    const-string v1, "APP_PACKAGING_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->d()Lcom/google/android/gms/drive/database/model/j;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "appPackagingId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/k;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    .line 45
    new-instance v0, Lcom/google/android/gms/drive/database/model/k;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->d()Lcom/google/android/gms/drive/database/model/j;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "accountId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v4

    invoke-virtual {v3, v4, v10}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x9

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x9

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "accountId"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v5

    invoke-virtual {v4, v5, v10}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    new-array v5, v7, [Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    iget-object v6, v6, Lcom/google/android/gms/drive/database/model/k;->f:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/k;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/database/model/k;

    const-string v1, "HAS_APPDATA_SYNCED"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->d()Lcom/google/android/gms/drive/database/model/j;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "hasAppDataSynced"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/k;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/k;->c:Lcom/google/android/gms/drive/database/model/k;

    .line 66
    new-instance v0, Lcom/google/android/gms/drive/database/model/k;

    const-string v1, "APPDATA_FOLDER_ENTRY_ID"

    const/4 v2, 0x3

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->d()Lcom/google/android/gms/drive/database/model/j;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "appDataFolderEntryId"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v5

    invoke-virtual {v4, v5, v10}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/k;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/k;->d:Lcom/google/android/gms/drive/database/model/k;

    .line 75
    new-instance v0, Lcom/google/android/gms/drive/database/model/k;

    const-string v1, "IS_APPDATA_FOLDER_PLACEHOLDER"

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->d()Lcom/google/android/gms/drive/database/model/j;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "isAppDataFolderPlaceholder"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/k;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/k;->e:Lcom/google/android/gms/drive/database/model/k;

    .line 40
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/k;

    sget-object v1, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/k;->c:Lcom/google/android/gms/drive/database/model/k;

    aput-object v1, v0, v11

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/gms/drive/database/model/k;->d:Lcom/google/android/gms/drive/database/model/k;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/drive/database/model/k;->e:Lcom/google/android/gms/drive/database/model/k;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/k;->g:[Lcom/google/android/gms/drive/database/model/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 83
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/k;->f:Lcom/google/android/gms/drive/database/model/ab;

    .line 84
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/k;
    .locals 1

    .prologue
    .line 40
    const-class v0, Lcom/google/android/gms/drive/database/model/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/k;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/k;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->g:[Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/k;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/k;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/k;->f:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
