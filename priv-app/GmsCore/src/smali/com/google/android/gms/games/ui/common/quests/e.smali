.class public final Lcom/google/android/gms/games/ui/common/quests/e;
.super Lcom/google/android/gms/games/ui/f;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final k:Lcom/google/android/gms/games/ui/common/quests/b;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/f;-><init>(Landroid/view/View;)V

    .line 55
    new-instance v0, Lcom/google/android/gms/games/ui/common/quests/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/quests/b;-><init>(Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/e;->k:Lcom/google/android/gms/games/ui/common/quests/b;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/e;->k:Lcom/google/android/gms/games/ui/common/quests/b;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/quests/b;->n:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/common/quests/b;->o:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, v0, Lcom/google/android/gms/games/ui/common/quests/b;->q:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 57
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 21

    .prologue
    .line 48
    check-cast p3, Lcom/google/android/gms/games/quest/Quest;

    invoke-super/range {p0 .. p3}, Lcom/google/android/gms/games/ui/f;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/quests/e;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v2, Lcom/google/android/gms/games/ui/common/quests/d;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/games/ui/common/quests/e;->k:Lcom/google/android/gms/games/ui/common/quests/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/quests/e;->l:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/quests/d;->a(Lcom/google/android/gms/games/ui/common/quests/d;)Lcom/google/android/gms/games/ui/q;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v4

    iput-boolean v4, v13, Lcom/google/android/gms/games/ui/common/quests/b;->a:Z

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v2, v2, v4

    if-lez v2, :cond_3

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_0

    iget-object v2, v13, Lcom/google/android/gms/games/ui/common/quests/b;->i:Landroid/widget/TextView;

    const/4 v4, 0x2

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setMaxLines(I)V

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->m()I

    move-result v2

    const/4 v12, 0x0

    const/4 v11, 0x0

    const/4 v4, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x0

    packed-switch v2, :pswitch_data_0

    const-string v5, "InternalQuestVH"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "populateViews: unexpected state: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    const-string v5, ""

    const-string v2, ""

    move v6, v11

    move v7, v12

    move-object v8, v2

    move v2, v9

    move-object v9, v5

    move v5, v4

    move v4, v10

    :goto_1
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->f()Landroid/net/Uri;

    move-result-object v10

    if-nez v10, :cond_1

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v10

    invoke-interface {v10}, Lcom/google/android/gms/games/Game;->m()Landroid/net/Uri;

    move-result-object v10

    :cond_1
    iget-object v11, v13, Lcom/google/android/gms/games/ui/common/quests/b;->c:Landroid/view/View;

    const/16 v12, 0x8

    invoke-virtual {v11, v12}, Landroid/view/View;->setVisibility(I)V

    iget-object v11, v13, Lcom/google/android/gms/games/ui/common/quests/b;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v12, Lcom/google/android/gms/h;->aD:I

    new-instance v14, Lcom/google/android/gms/games/ui/common/quests/c;

    invoke-direct {v14, v13}, Lcom/google/android/gms/games/ui/common/quests/c;-><init>(Lcom/google/android/gms/games/ui/common/quests/b;)V

    move-object/from16 v0, p0

    invoke-interface {v0, v11, v10, v12, v14}, Lcom/google/android/gms/games/ui/br;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;ILcom/google/android/gms/common/images/g;)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->d:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->h()Landroid/net/Uri;

    move-result-object v11

    sget v12, Lcom/google/android/gms/h;->af:I

    move-object/from16 v0, p0

    invoke-interface {v0, v10, v11, v12}, Lcom/google/android/gms/games/ui/br;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->j()Lcom/google/android/gms/games/quest/Milestone;

    move-result-object v10

    invoke-interface {v10}, Lcom/google/android/gms/games/quest/Milestone;->d()J

    move-result-wide v14

    invoke-interface {v10}, Lcom/google/android/gms/games/quest/Milestone;->g()J

    move-result-wide v16

    sget v10, Lcom/google/android/gms/p;->lU:I

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v14, v15}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v11, v12

    const/4 v12, 0x1

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v11, v12

    invoke-virtual {v3, v10, v11}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    iget-object v11, v13, Lcom/google/android/gms/games/ui/common/quests/b;->e:Landroid/widget/TextView;

    invoke-static {v10}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v10

    invoke-virtual {v11, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    if-eqz v4, :cond_7

    const/16 v11, 0x64

    sget v10, Lcom/google/android/gms/h;->aF:I

    :goto_2
    iget-object v12, v13, Lcom/google/android/gms/games/ui/common/quests/b;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v12, v11}, Landroid/widget/ProgressBar;->setProgress(I)V

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    iget-object v12, v13, Lcom/google/android/gms/games/ui/common/quests/b;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v11, v10}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v10

    invoke-virtual {v12, v10}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->h:Landroid/database/CharArrayBuffer;

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Lcom/google/android/gms/games/quest/Quest;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->g:Landroid/widget/TextView;

    iget-object v12, v13, Lcom/google/android/gms/games/ui/common/quests/b;->h:Landroid/database/CharArrayBuffer;

    iget-object v12, v12, Landroid/database/CharArrayBuffer;->data:[C

    const/16 v18, 0x0

    iget-object v0, v13, Lcom/google/android/gms/games/ui/common/quests/b;->h:Landroid/database/CharArrayBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/database/CharArrayBuffer;->sizeCopied:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v12, v0, v1}, Landroid/widget/TextView;->setText([CII)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->j:Landroid/database/CharArrayBuffer;

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Lcom/google/android/gms/games/quest/Quest;->b(Landroid/database/CharArrayBuffer;)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->i:Landroid/widget/TextView;

    iget-object v12, v13, Lcom/google/android/gms/games/ui/common/quests/b;->j:Landroid/database/CharArrayBuffer;

    iget-object v12, v12, Landroid/database/CharArrayBuffer;->data:[C

    const/16 v18, 0x0

    iget-object v0, v13, Lcom/google/android/gms/games/ui/common/quests/b;->j:Landroid/database/CharArrayBuffer;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget v0, v0, Landroid/database/CharArrayBuffer;->sizeCopied:I

    move/from16 v19, v0

    move/from16 v0, v18

    move/from16 v1, v19

    invoke-virtual {v10, v12, v0, v1}, Landroid/widget/TextView;->setText([CII)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->k:Landroid/widget/TextView;

    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->l:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->m:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->a:Z

    if-eqz v10, :cond_8

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->l:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v10, 0x8

    const/4 v12, 0x1

    new-array v12, v12, [Landroid/view/View;

    const/16 v18, 0x0

    iget-object v0, v13, Lcom/google/android/gms/games/ui/common/quests/b;->m:Landroid/widget/TextView;

    move-object/from16 v19, v0

    aput-object v19, v12, v18

    invoke-static {v2, v10, v12}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    :goto_3
    if-eqz v4, :cond_9

    sget v4, Lcom/google/android/gms/f;->C:I

    :goto_4
    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->k:Landroid/widget/TextView;

    invoke-virtual {v11, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v10, v4}, Landroid/widget/TextView;->setTextColor(I)V

    if-nez v7, :cond_2

    if-nez v6, :cond_2

    if-eqz v5, :cond_a

    :cond_2
    const/4 v4, 0x1

    :goto_5
    const/16 v10, 0x8

    const/4 v11, 0x1

    new-array v11, v11, [Landroid/view/View;

    const/4 v12, 0x0

    iget-object v0, v13, Lcom/google/android/gms/games/ui/common/quests/b;->p:Landroid/view/View;

    move-object/from16 v18, v0

    aput-object v18, v11, v12

    invoke-static {v4, v10, v11}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    const/16 v4, 0x8

    const/4 v10, 0x1

    new-array v10, v10, [Landroid/view/View;

    const/4 v11, 0x0

    iget-object v12, v13, Lcom/google/android/gms/games/ui/common/quests/b;->n:Landroid/widget/Button;

    aput-object v12, v10, v11

    invoke-static {v7, v4, v10}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    iget-object v4, v13, Lcom/google/android/gms/games/ui/common/quests/b;->n:Landroid/widget/Button;

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    const/16 v4, 0x8

    const/4 v7, 0x1

    new-array v7, v7, [Landroid/view/View;

    const/4 v10, 0x0

    iget-object v11, v13, Lcom/google/android/gms/games/ui/common/quests/b;->o:Landroid/widget/Button;

    aput-object v11, v7, v10

    invoke-static {v6, v4, v7}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    iget-object v4, v13, Lcom/google/android/gms/games/ui/common/quests/b;->o:Landroid/widget/Button;

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    const/16 v4, 0x8

    const/4 v6, 0x1

    new-array v6, v6, [Landroid/view/View;

    const/4 v7, 0x0

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->q:Landroid/widget/Button;

    aput-object v10, v6, v7

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    iget-object v4, v13, Lcom/google/android/gms/games/ui/common/quests/b;->q:Landroid/widget/Button;

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    if-eqz v2, :cond_b

    sget v2, Lcom/google/android/gms/p;->lL:I

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object v8, v4, v5

    const/4 v5, 0x5

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    :goto_6
    iget-object v3, v13, Lcom/google/android/gms/games/ui/common/quests/b;->r:Landroid/view/View;

    invoke-virtual {v3, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_3
    const/4 v2, 0x0

    goto/16 :goto_0

    :pswitch_0
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->s()J

    move-result-wide v4

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/games/ui/common/quests/m;->a(Landroid/content/Context;JJZ)Ljava/lang/String;

    move-result-object v5

    const-string v4, ""

    const/4 v2, 0x0

    move v6, v11

    move v7, v12

    move-object v8, v4

    move v4, v10

    move/from16 v20, v9

    move-object v9, v5

    move v5, v2

    move/from16 v2, v20

    goto/16 :goto_1

    :pswitch_1
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->p()J

    move-result-wide v4

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/games/ui/common/quests/m;->a(Landroid/content/Context;JJZ)Ljava/lang/String;

    move-result-object v6

    const-string v5, ""

    iget-boolean v4, v13, Lcom/google/android/gms/games/ui/common/quests/b;->a:Z

    if-nez v4, :cond_4

    const/4 v2, 0x1

    :goto_7
    move v7, v4

    move-object v8, v5

    move v5, v2

    move v4, v10

    move v2, v9

    move-object v9, v6

    move v6, v11

    goto/16 :goto_1

    :cond_4
    const/4 v2, 0x0

    goto :goto_7

    :pswitch_2
    sget v2, Lcom/google/android/gms/p;->lV:I

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->p()J

    move-result-wide v4

    const/4 v8, 0x1

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/games/ui/common/quests/m;->a(Landroid/content/Context;JJZ)Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x1

    iget-boolean v2, v13, Lcom/google/android/gms/games/ui/common/quests/b;->a:Z

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_8
    move v6, v11

    move v7, v12

    move-object v8, v5

    move v5, v2

    move v2, v4

    move v4, v10

    goto/16 :goto_1

    :cond_5
    const/4 v2, 0x0

    goto :goto_8

    :pswitch_3
    const/4 v2, 0x3

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->j()Lcom/google/android/gms/games/quest/Milestone;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/quest/Milestone;->f()I

    move-result v5

    if-ne v2, v5, :cond_6

    sget v2, Lcom/google/android/gms/p;->lY:I

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v5, ""

    const/4 v2, 0x1

    move v7, v12

    move-object v8, v5

    move v5, v4

    move v4, v10

    move/from16 v20, v9

    move-object v9, v6

    move v6, v2

    move/from16 v2, v20

    goto/16 :goto_1

    :cond_6
    sget v2, Lcom/google/android/gms/p;->lY:I

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v2, ""

    move v6, v11

    move v7, v12

    move-object v8, v2

    move v2, v9

    move-object v9, v5

    move v5, v4

    move v4, v10

    goto/16 :goto_1

    :pswitch_4
    sget v2, Lcom/google/android/gms/p;->lZ:I

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v5, ""

    const/4 v2, 0x1

    move v7, v12

    move-object v8, v5

    move v5, v4

    move v4, v2

    move v2, v9

    move-object v9, v6

    move v6, v11

    goto/16 :goto_1

    :pswitch_5
    sget v2, Lcom/google/android/gms/p;->lZ:I

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v5, ""

    const/4 v2, 0x1

    move v7, v12

    move-object v8, v5

    move v5, v4

    move v4, v2

    move v2, v9

    move-object v9, v6

    move v6, v11

    goto/16 :goto_1

    :cond_7
    const-wide/16 v10, 0x64

    mul-long/2addr v10, v14

    div-long v10, v10, v16

    long-to-int v11, v10

    sget v10, Lcom/google/android/gms/h;->aE:I

    goto/16 :goto_2

    :cond_8
    const/16 v10, 0x8

    const/4 v12, 0x1

    new-array v12, v12, [Landroid/view/View;

    const/16 v18, 0x0

    iget-object v0, v13, Lcom/google/android/gms/games/ui/common/quests/b;->l:Landroid/widget/TextView;

    move-object/from16 v19, v0

    aput-object v19, v12, v18

    invoke-static {v2, v10, v12}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    iget-object v10, v13, Lcom/google/android/gms/games/ui/common/quests/b;->m:Landroid/widget/TextView;

    const/16 v12, 0x8

    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_9
    sget v4, Lcom/google/android/gms/f;->W:I

    goto/16 :goto_4

    :cond_a
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_b
    sget v2, Lcom/google/android/gms/p;->lT:I

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    aput-object v9, v4, v5

    const/4 v5, 0x5

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/quest/Quest;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v2, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_6

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/e;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/quests/d;

    .line 71
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/e;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/quest/Quest;

    .line 72
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/quests/d;->b(Lcom/google/android/gms/games/ui/common/quests/d;)Lcom/google/android/gms/games/ui/common/quests/g;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/quests/g;->a(Lcom/google/android/gms/games/quest/Quest;)V

    .line 73
    return-void
.end method
