.class public final Lcom/google/android/gms/people/service/a/aa;
.super Lcom/google/android/gms/people/service/a/b;
.source "SourceFile"


# instance fields
.field private final g:Z

.field private final h:Z

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;ZZLjava/lang/String;Ljava/lang/String;I)V
    .locals 6

    .prologue
    .line 40
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;)V

    .line 42
    iput-boolean p5, p0, Lcom/google/android/gms/people/service/a/aa;->g:Z

    .line 43
    iput-boolean p6, p0, Lcom/google/android/gms/people/service/a/aa;->h:Z

    .line 44
    iput-object p7, p0, Lcom/google/android/gms/people/service/a/aa;->i:Ljava/lang/String;

    .line 45
    iput-object p8, p0, Lcom/google/android/gms/people/service/a/aa;->j:Ljava/lang/String;

    .line 46
    iput p9, p0, Lcom/google/android/gms/people/service/a/aa;->k:I

    .line 47
    return-void
.end method

.method private static a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 62
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v3

    .line 67
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    .line 68
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 69
    invoke-static {p0}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v5

    array-length v6, v5

    move v0, v1

    :goto_0
    if-ge v0, v6, :cond_0

    aget-object v7, v5, v0

    .line 70
    new-instance v8, Lcom/google/android/gms/people/model/AccountMetadata;

    invoke-direct {v8}, Lcom/google/android/gms/people/model/AccountMetadata;-><init>()V

    .line 72
    invoke-static {p0, v7}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v9

    iput-boolean v9, v8, Lcom/google/android/gms/people/model/AccountMetadata;->b:Z

    .line 75
    iget-object v9, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Lcom/google/android/gms/people/sync/u;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, v8, Lcom/google/android/gms/people/model/AccountMetadata;->c:Z

    .line 79
    iget-object v9, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v10, "pageid"

    invoke-virtual {v3, v9, v10}, Lcom/google/android/gms/people/sync/u;->c(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, v8, Lcom/google/android/gms/people/model/AccountMetadata;->d:Z

    .line 81
    iget-object v9, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v10, "pageid"

    invoke-virtual {v3, v9, v10}, Lcom/google/android/gms/people/sync/u;->b(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, v8, Lcom/google/android/gms/people/model/AccountMetadata;->e:Z

    .line 84
    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v7, v8}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 88
    const-string v0, "account_metadata"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 95
    if-nez p4, :cond_1

    .line 98
    const-string v0, "account_name,(page_gaia_id IS NOT NULL),display_name COLLATE LOCALIZED"

    .line 116
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SELECT _id,account_name,display_name,gaia_id,page_gaia_id,avatar,cover_photo_url,cover_photo_height,cover_photo_width,cover_photo_id,last_sync_start_time,last_sync_finish_time,last_sync_status,last_successful_sync_time,sync_to_contacts,sync_circles_to_contacts,sync_evergreen_to_contacts,is_dasher,dasher_domain FROM owners leftOwners  WHERE ((?2=\'\') OR (account_name=?2 AND (((?3 = \'\') AND (page_gaia_id IS NULL)) OR (?3=page_gaia_id)))) AND ( ?1 OR (page_gaia_id IS NULL)) ORDER BY "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    if-eqz p1, :cond_3

    const-string v0, "1"

    :goto_2
    invoke-static {p2}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {p3}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v5, v6}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    .line 152
    new-instance v2, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v2

    .line 101
    :cond_1
    const/4 v0, 0x1

    if-ne p4, v0, :cond_2

    .line 103
    const-string v0, "(SELECT _id FROM owners WHERE account_name=leftOwners.account_name AND page_gaia_id IS NULL) ,(page_gaia_id IS NOT NULL),display_name COLLATE LOCALIZED"

    goto :goto_1

    .line 112
    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Value of sortOrder isn\'t valid.sortOrder= "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_3
    const-string v0, "0"

    goto :goto_2
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a/aa;->g:Z

    if-eqz v0, :cond_0

    .line 52
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a/aa;->h:Z

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/aa;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/aa;->j:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/people/service/a/aa;->k:I

    invoke-static {p1, v0, v1, v2, v3}, Lcom/google/android/gms/people/service/a/aa;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 54
    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a/aa;->h:Z

    iget v1, p0, Lcom/google/android/gms/people/service/a/aa;->k:I

    invoke-static {p1, v0, v2, v2, v1}, Lcom/google/android/gms/people/service/a/aa;->a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
