.class public Lcom/google/android/gms/common/server/b;
.super Lcom/android/volley/f;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/android/volley/f;-><init>()V

    .line 201
    iput-object p1, p0, Lcom/google/android/gms/common/server/b;->a:Landroid/content/Context;

    .line 202
    iput-object p2, p0, Lcom/google/android/gms/common/server/b;->b:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;IIF)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0, p3, p4, p5}, Lcom/android/volley/f;-><init>(IIF)V

    .line 208
    iput-object p1, p0, Lcom/google/android/gms/common/server/b;->a:Landroid/content/Context;

    .line 209
    iput-object p2, p0, Lcom/google/android/gms/common/server/b;->b:Ljava/lang/String;

    .line 210
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/ac;)V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    const/16 v1, 0x190

    if-lt v0, v1, :cond_1

    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    const/16 v1, 0x1f4

    if-ge v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 220
    invoke-static {p1}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;)Ljava/lang/String;

    move-result-object v0

    .line 221
    const-string v1, "userRateLimitExceeded"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/common/server/b;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/common/server/a;->a(Lcom/android/volley/ac;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/common/server/b;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/server/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 225
    :cond_0
    throw p1

    .line 219
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 233
    :cond_2
    invoke-super {p0, p1}, Lcom/android/volley/f;->a(Lcom/android/volley/ac;)V

    .line 234
    return-void
.end method
