.class public final Lcom/google/android/gms/security/verifier/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile h:[Lcom/google/android/gms/security/verifier/q;


# instance fields
.field public a:[B

.field public b:Ljava/lang/Boolean;

.field public c:[B

.field public d:[B

.field public e:[B

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 384
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 385
    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/q;->cachedSize:I

    .line 386
    return-void
.end method

.method public static a()[Lcom/google/android/gms/security/verifier/q;
    .locals 2

    .prologue
    .line 352
    sget-object v0, Lcom/google/android/gms/security/verifier/q;->h:[Lcom/google/android/gms/security/verifier/q;

    if-nez v0, :cond_1

    .line 353
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 355
    :try_start_0
    sget-object v0, Lcom/google/android/gms/security/verifier/q;->h:[Lcom/google/android/gms/security/verifier/q;

    if-nez v0, :cond_0

    .line 356
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/security/verifier/q;

    sput-object v0, Lcom/google/android/gms/security/verifier/q;->h:[Lcom/google/android/gms/security/verifier/q;

    .line 358
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 360
    :cond_1
    sget-object v0, Lcom/google/android/gms/security/verifier/q;->h:[Lcom/google/android/gms/security/verifier/q;

    return-object v0

    .line 358
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 490
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 491
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    if-eqz v1, :cond_0

    .line 492
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 495
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 496
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 499
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    if-eqz v1, :cond_2

    .line 500
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 503
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    if-eqz v1, :cond_3

    .line 504
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 507
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    if-eqz v1, :cond_4

    .line 508
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 511
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 512
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 515
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 516
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 519
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 402
    if-ne p1, p0, :cond_1

    .line 442
    :cond_0
    :goto_0
    return v0

    .line 405
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 406
    goto :goto_0

    .line 408
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/q;

    .line 409
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/q;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 410
    goto :goto_0

    .line 412
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    .line 413
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_5

    move v0, v1

    .line 414
    goto :goto_0

    .line 416
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    invoke-virtual {v2, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 417
    goto :goto_0

    .line 419
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/q;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 420
    goto :goto_0

    .line 422
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/q;->d:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 423
    goto :goto_0

    .line 425
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/q;->e:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 426
    goto :goto_0

    .line 428
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    if-nez v2, :cond_9

    .line 429
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    if-eqz v2, :cond_a

    move v0, v1

    .line 430
    goto :goto_0

    .line 432
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 433
    goto :goto_0

    .line 435
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    if-nez v2, :cond_b

    .line 436
    iget-object v2, p1, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    if-eqz v2, :cond_0

    move v0, v1

    .line 437
    goto :goto_0

    .line 439
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    invoke-virtual {v2, v3}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 440
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 449
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 451
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 452
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 453
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 454
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 456
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 458
    return v0

    .line 449
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 454
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 456
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 346
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    if-eqz v0, :cond_0

    .line 465
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 468
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 470
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    if-eqz v0, :cond_2

    .line 471
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 473
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    if-eqz v0, :cond_3

    .line 474
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->d:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 476
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    if-eqz v0, :cond_4

    .line 477
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->e:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 479
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 480
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 482
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 483
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/q;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 485
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 486
    return-void
.end method
