.class public final Lcom/google/android/gms/audiomodem/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/audiomodem/s;


# instance fields
.field private final b:Landroid/content/SharedPreferences;

.field private c:Lcom/google/whispernet/b;


# direct methods
.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const-string v0, "audioModem"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "AudioModemOverrideManager:  Created override manager"

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/s;->b:Landroid/content/SharedPreferences;

    .line 60
    const-string v0, "overrides"

    invoke-direct {p0, v0}, Lcom/google/android/gms/audiomodem/s;->a(Ljava/lang/String;)Lcom/google/whispernet/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/s;->c:Lcom/google/whispernet/b;

    .line 61
    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/gms/audiomodem/s;
    .locals 4

    .prologue
    .line 36
    const-class v1, Lcom/google/android/gms/audiomodem/s;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/audiomodem/s;->a:Lcom/google/android/gms/audiomodem/s;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Lcom/google/android/gms/audiomodem/s;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    const-string v3, "audiomodem_configuration"

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/audiomodem/s;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/audiomodem/s;->a:Lcom/google/android/gms/audiomodem/s;

    .line 39
    :cond_0
    sget-object v0, Lcom/google/android/gms/audiomodem/s;->a:Lcom/google/android/gms/audiomodem/s;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/whispernet/b;
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/s;->b:Landroid/content/SharedPreferences;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 65
    if-nez v0, :cond_0

    .line 66
    new-instance v0, Lcom/google/whispernet/b;

    invoke-direct {v0}, Lcom/google/whispernet/b;-><init>()V

    .line 83
    :goto_0
    return-object v0

    .line 70
    :cond_0
    const/4 v1, 0x0

    :try_start_0
    invoke-static {v0, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    .line 71
    new-instance v0, Lcom/google/whispernet/b;

    invoke-direct {v0}, Lcom/google/whispernet/b;-><init>()V

    .line 72
    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 74
    :catch_0
    move-exception v0

    .line 75
    const-string v1, "audioModem"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioModemOverrideManager:  reading stored configuration error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/s;->c()Lcom/google/whispernet/b;

    move-result-object v0

    goto :goto_0

    .line 79
    :catch_1
    move-exception v0

    .line 80
    const-string v1, "audioModem"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 81
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioModemOverrideManager:  reading stored configuration error: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/audiomodem/s;->c()Lcom/google/whispernet/b;

    move-result-object v0

    goto :goto_0
.end method

.method private c()Lcom/google/whispernet/b;
    .locals 3

    .prologue
    .line 88
    new-instance v0, Lcom/google/whispernet/b;

    invoke-direct {v0}, Lcom/google/whispernet/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/s;->c:Lcom/google/whispernet/b;

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/s;->c:Lcom/google/whispernet/b;

    invoke-static {v0}, Lcom/google/whispernet/b;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/s;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "overrides"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/s;->c:Lcom/google/whispernet/b;

    return-object v0
.end method


# virtual methods
.method final b()Lcom/google/whispernet/b;
    .locals 4

    .prologue
    .line 43
    new-instance v1, Lcom/google/whispernet/b;

    invoke-direct {v1}, Lcom/google/whispernet/b;-><init>()V

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/s;->c:Lcom/google/whispernet/b;

    invoke-static {v0}, Lcom/google/whispernet/b;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :cond_0
    :goto_0
    return-object v1

    .line 46
    :catch_0
    move-exception v0

    .line 47
    const-string v2, "audioModem"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AudioModemOverrideManager: Error copying overrides "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "audioModem"

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
