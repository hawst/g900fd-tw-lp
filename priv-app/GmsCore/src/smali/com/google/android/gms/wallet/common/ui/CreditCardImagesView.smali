.class public Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/ak;


# static fields
.field private static final e:[I

.field private static final f:[I


# instance fields
.field a:[Landroid/widget/ImageView;

.field b:Landroid/widget/ImageView;

.field c:Lcom/google/android/gms/wallet/common/ui/at;

.field d:I

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 31
    new-array v0, v3, [I

    const/4 v1, 0x0

    sget v2, Lcom/google/android/gms/j;->tT:I

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/google/android/gms/j;->lM:I

    aput v2, v0, v1

    const/4 v1, 0x2

    sget v2, Lcom/google/android/gms/j;->aA:I

    aput v2, v0, v1

    const/4 v1, 0x3

    sget v2, Lcom/google/android/gms/j;->ew:I

    aput v2, v0, v1

    const/4 v1, 0x4

    sget v2, Lcom/google/android/gms/j;->kN:I

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->e:[I

    .line 37
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->f:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
        0x5
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 51
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    .line 64
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->c:Lcom/google/android/gms/wallet/common/ui/at;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/at;->b(I)V

    .line 118
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 131
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->c:Lcom/google/android/gms/wallet/common/ui/at;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/at;->a(I)V

    .line 133
    return-void
.end method

.method public onFinishInflate()V
    .locals 5

    .prologue
    .line 68
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 69
    sget v0, Lcom/google/android/gms/j;->gZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->b:Landroid/widget/ImageView;

    .line 70
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 71
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->e:[I

    array-length v3, v1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 72
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->e:[I

    aget v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 73
    if-eqz v0, :cond_0

    .line 74
    sget-object v4, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->f:[I

    aget v4, v4, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 75
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->a:[Landroid/widget/ImageView;

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->a:[Landroid/widget/ImageView;

    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/wallet/common/ui/av;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->b:Landroid/widget/ImageView;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/av;-><init>([Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->c:Lcom/google/android/gms/wallet/common/ui/at;

    .line 80
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->b(I)V

    .line 81
    return-void

    .line 79
    :cond_2
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/au;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->b:Landroid/widget/ImageView;

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/gms/wallet/common/ui/au;-><init>(Landroid/content/Context;[Landroid/widget/ImageView;Landroid/widget/ImageView;)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 109
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 110
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->g:Z

    if-eqz v0, :cond_0

    .line 111
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->g:Z

    .line 112
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->b(I)V

    .line 114
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 93
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 94
    check-cast p1, Landroid/os/Bundle;

    .line 99
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->g:Z

    .line 100
    const-string v0, "cardType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    .line 101
    const-string v0, "parentState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 105
    :goto_0
    return-void

    .line 104
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 85
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 86
    const-string v1, "parentState"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 87
    const-string v1, "cardType"

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 88
    return-object v0
.end method
