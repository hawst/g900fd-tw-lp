.class public Lcom/google/android/gms/photos/autobackup/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/account/a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/libraries/social/account/b;

.field private final d:Lcom/google/android/libraries/social/account/refresh/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/a;->a:Landroid/content/Context;

    .line 37
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "autobackup-accounts-wrapper"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 38
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 39
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/photos/autobackup/a;->b:Landroid/os/Handler;

    .line 40
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/account/refresh/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/refresh/a;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->d:Lcom/google/android/libraries/social/account/refresh/a;

    .line 42
    return-void
.end method

.method private b(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/auth/r;->c(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 148
    :goto_0
    return-object v0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    const-string v1, "AccountsWrapper"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    const-string v1, "AccountsWrapper"

    const-string v2, "Got GoogleAuthException trying to log in account"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 148
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 143
    :catch_1
    move-exception v0

    .line 144
    const-string v1, "AccountsWrapper"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 145
    const-string v1, "AccountsWrapper"

    const-string v2, "Got IOException trying to log in account"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/a;->d:Lcom/google/android/libraries/social/account/refresh/a;

    invoke-interface {v1, p1}, Lcom/google/android/libraries/social/account/refresh/a;->a(I)V

    .line 157
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/a;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/a;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 159
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 50
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 61
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v0

    .line 55
    if-ne v0, v1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    const/4 v1, 0x5

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/d;->a(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/c;)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/photos/autobackup/b;-><init>(Lcom/google/android/gms/photos/autobackup/a;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 86
    return-void
.end method

.method public final a(I)Z
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 94
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v2, p1}, Lcom/google/android/libraries/social/account/b;->c(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 127
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v2, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/libraries/social/account/c;->a()I

    move-result v3

    if-ne v3, v4, :cond_2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v1

    :goto_1
    if-eqz v2, :cond_3

    .line 99
    invoke-direct {p0, p1}, Lcom/google/android/gms/photos/autobackup/a;->c(I)V

    move v0, v1

    .line 100
    goto :goto_0

    :cond_2
    move v2, v0

    .line 98
    goto :goto_1

    .line 103
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v2, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 108
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v2, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v2

    const-string v3, "account_name"

    invoke-interface {v2, v3}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 112
    invoke-direct {p0, v2}, Lcom/google/android/gms/photos/autobackup/a;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 113
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3, v2}, Lcom/google/android/libraries/social/account/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    .line 122
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->c:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->b(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0, v4}, Lcom/google/android/libraries/social/account/d;->a(I)Lcom/google/android/libraries/social/account/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/libraries/social/account/d;->c()I

    .line 126
    invoke-direct {p0, p1}, Lcom/google/android/gms/photos/autobackup/a;->c(I)V

    move v0, v1

    .line 127
    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;I)V

    .line 167
    return-void
.end method
