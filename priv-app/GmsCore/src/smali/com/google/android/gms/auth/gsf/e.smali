.class final Lcom/google/android/gms/auth/gsf/e;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/frp/a;

.field final synthetic b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;Lcom/google/android/gms/auth/frp/a;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/gms/auth/gsf/e;->b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;

    iput-object p2, p0, Lcom/google/android/gms/auth/gsf/e;->a:Lcom/google/android/gms/auth/frp/a;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 257
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x16

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/e;->b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;

    iget-object v0, v0, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->m:Lcom/google/android/gms/auth/gsf/h;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/gsf/h;->l:Z

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/e;->b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->a(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/e;->b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/e;->b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/e;->a:Lcom/google/android/gms/auth/frp/a;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/frp/a;->a()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 257
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.app.action.CONFIRM_DEVICE_CREDENTIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/gsf/e;->b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;

    const v2, 0xa413

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/gsf/e;->b:Lcom/google/android/gms/auth/gsf/AccountIntroActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/gsf/AccountIntroActivity;->b(Lcom/google/android/gms/auth/gsf/AccountIntroActivity;)V

    goto :goto_0
.end method
