.class public final Lcom/google/android/gms/drive/api/f;
.super Lcom/google/android/gms/drive/internal/by;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/drive/api/d;

.field private final c:Lcom/google/android/gms/drive/api/k;

.field private final d:Lcom/google/android/gms/drive/api/b;

.field private final e:Landroid/os/IBinder;

.field private final f:Ljava/util/List;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/b;Lcom/google/android/gms/drive/api/k;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/by;-><init>()V

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/f;->f:Ljava/util/List;

    .line 127
    iput-object p1, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    .line 128
    iput-object p2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    .line 129
    iput-object p4, p0, Lcom/google/android/gms/drive/api/f;->c:Lcom/google/android/gms/drive/api/k;

    .line 130
    iput-object p5, p0, Lcom/google/android/gms/drive/api/f;->e:Landroid/os/IBinder;

    .line 131
    iput-object p3, p0, Lcom/google/android/gms/drive/api/f;->d:Lcom/google/android/gms/drive/api/b;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->d:Lcom/google/android/gms/drive/api/b;

    iget-object v1, v0, Lcom/google/android/gms/drive/api/b;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/drive/api/b;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/b;Lcom/google/android/gms/drive/api/k;Landroid/os/IBinder;B)V
    .locals 0

    .prologue
    .line 92
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/drive/api/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/b;Lcom/google/android/gms/drive/api/k;Landroid/os/IBinder;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;)Landroid/content/IntentSender;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->a()Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->b()Lcom/google/android/gms/drive/c/b;

    move-result-object v7

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->e()I

    move-result v6

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->b()I

    move-result v3

    if-nez v6, :cond_0

    if-ltz v3, :cond_1

    const/4 v4, 0x1

    :goto_0
    const-string v8, "The request id must be provided."

    invoke-static {v4, v8}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    :cond_0
    invoke-interface {v7}, Lcom/google/android/gms/drive/c/b;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/drive/c/a;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v4

    const/4 v7, 0x5

    invoke-interface {v4, v5, v7}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/c/a;

    move-result-object v4

    invoke-interface {v4, v6}, Lcom/google/android/gms/drive/c/a;->d(I)Lcom/google/android/gms/drive/c/a;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->d()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CreateFileIntentSenderRequest;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;ILcom/google/android/gms/drive/DriveId;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v0

    invoke-interface {v7}, Lcom/google/android/gms/drive/c/a;->d()Lcom/google/android/gms/drive/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/drive/c/a;->a()V

    return-object v0

    :cond_1
    move v4, v5

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;)Landroid/content/IntentSender;
    .locals 11

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->a()Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 331
    iget-object v1, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v1}, Lcom/google/android/gms/drive/api/d;->b()Lcom/google/android/gms/drive/c/b;

    move-result-object v1

    .line 332
    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;->c()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OpenFileIntentSenderRequest;->b()[Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v6, v6, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    iget-wide v8, v0, Lcom/google/android/gms/drive/auth/g;->b:J

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/gms/drive/c/b;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/drive/c/a;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v1

    const/4 v7, 0x0

    const/16 v10, 0xf

    invoke-interface {v1, v7, v10}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v1

    invoke-static {v2, v6, v8, v9, v0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/ui/picker/i;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/drive/ui/picker/i;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/ui/picker/i;->a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/i;->a()Lcom/google/android/gms/drive/ui/picker/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/i;->a:Landroid/content/Intent;

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v0

    invoke-interface {v1}, Lcom/google/android/gms/drive/c/a;->d()Lcom/google/android/gms/drive/c/a;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/drive/c/a;->a()V

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/api/d;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    return-object v0
.end method

.method public final a(Lcom/google/c/a/a/a/a/a;Landroid/os/IBinder;Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/api/l;
    .locals 6

    .prologue
    .line 408
    new-instance v0, Lcom/google/android/gms/drive/api/l;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v5

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/api/l;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/c/a/a/a/a/a;Landroid/os/IBinder;Lcom/google/android/gms/drive/realtime/e;Lcom/google/android/gms/common/util/p;)V

    .line 410
    iget-object v1, p0, Lcom/google/android/gms/drive/api/f;->f:Ljava/util/List;

    monitor-enter v1

    .line 411
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->f:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    return-object v0

    .line 412
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/am;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/am;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 436
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Lcom/google/android/gms/drive/internal/cd;Ljava/lang/String;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2, p4}, Lcom/google/android/gms/drive/api/a/a;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Lcom/google/android/gms/drive/internal/cd;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 357
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/e;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/e;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 348
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/f;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/f;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/CancelPendingActionsRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 457
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/g;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 399
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 8

    .prologue
    .line 273
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v4

    .line 274
    iget-object v7, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/drive/api/a/k;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->c:Lcom/google/android/gms/drive/api/k;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/g/aw;->v()Lcom/google/android/gms/drive/b/d;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/g/aw;->u()Lcom/google/android/gms/drive/e/b;

    move-result-object v4

    move-object v5, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/api/a/k;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/e/b;Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v7, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 278
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CloseContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 5

    .prologue
    .line 254
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->b()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 256
    new-instance v0, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/Contents;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/CloseContentsRequest;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/drive/ae;

    invoke-direct {v4}, Lcom/google/android/gms/drive/ae;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/ae;->b()Lcom/google/android/gms/drive/ad;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/ad;)V

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/drive/api/f;->a(Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V

    .line 268
    :goto_0
    return-void

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/f;->c:Lcom/google/android/gms/drive/api/k;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/google/android/gms/drive/api/a/q;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/CloseContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CreateContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 4

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/m;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/f;->c:Lcom/google/android/gms/drive/api/k;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/google/android/gms/drive/api/a/m;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/CreateContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 234
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CreateFileRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 4

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/n;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/f;->c:Lcom/google/android/gms/drive/api/k;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/google/android/gms/drive/api/a/n;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/CreateFileRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 219
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/o;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/o;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 227
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/DeleteResourceRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/p;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/p;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/DeleteResourceRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 292
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/DisconnectRequest;)V
    .locals 4

    .prologue
    .line 165
    new-instance v0, Lcom/google/android/gms/drive/api/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/api/g;-><init>(Lcom/google/android/gms/drive/api/f;)V

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/drive/api/a/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v2, v3, p1, v0, p0}, Lcom/google/android/gms/drive/api/a/r;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/DisconnectRequest;Lcom/google/android/gms/drive/internal/ca;Lcom/google/android/gms/drive/api/f;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 176
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/s;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/s;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/GetDriveIdFromUniqueIdentifierRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 392
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/GetMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/v;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/v;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/GetMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 306
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/ListParentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/x;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/x;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ListParentsRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 312
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 7

    .prologue
    .line 297
    iget-object v6, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/drive/api/a/y;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v2}, Lcom/google/android/gms/drive/api/d;->p()I

    move-result v5

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/api/a/y;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/f;Lcom/google/android/gms/drive/internal/LoadRealtimeRequest;Lcom/google/android/gms/drive/internal/ca;I)V

    invoke-static {v6, v0}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 299
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 4

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/ab;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/f;->c:Lcom/google/android/gms/drive/api/k;

    invoke-direct {v1, v2, v3, p1, p2}, Lcom/google/android/gms/drive/api/a/ab;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/OpenContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 242
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/QueryRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/ad;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/ad;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/QueryRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 212
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;Lcom/google/android/gms/drive/internal/cd;Ljava/lang/String;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/ae;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2, p4}, Lcom/google/android/gms/drive/api/a/ae;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/RemoveEventListenerRequest;Lcom/google/android/gms/drive/internal/cd;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 366
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/SetDrivePreferencesRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/ah;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/ah;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/SetDrivePreferencesRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 428
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/ai;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/ai;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/SetFileUploadPreferencesRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 450
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/aj;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/aj;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/SetResourceParentsRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 385
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/TrashResourceRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/an;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/an;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/TrashResourceRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 285
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/aq;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/aq;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/UpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 319
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/h;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/h;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 182
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->j()V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->d:Lcom/google/android/gms/drive/api/b;

    iget-object v1, v0, Lcom/google/android/gms/drive/api/b;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/drive/api/b;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 151
    iget-object v1, p0, Lcom/google/android/gms/drive/api/f;->f:Ljava/util/List;

    monitor-enter v1

    .line 152
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/api/l;

    .line 153
    invoke-virtual {v0}, Lcom/google/android/gms/drive/api/l;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 150
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 155
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 156
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 157
    if-eqz p1, :cond_1

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->e:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 160
    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/internal/QueryRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/ak;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/drive/api/a/ak;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/QueryRequest;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 378
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/l;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/l;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 188
    return-void
.end method

.method public final binderDied()V
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/api/f;->a(Z)V

    .line 140
    return-void
.end method

.method public final c(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/i;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 194
    return-void
.end method

.method public final d(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/ap;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/ap;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 200
    return-void
.end method

.method public final e(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/w;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/w;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 206
    return-void
.end method

.method public final f(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/af;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/af;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 325
    return-void
.end method

.method public final g(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/t;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/t;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 421
    return-void
.end method

.method public final h(Lcom/google/android/gms/drive/internal/ca;)V
    .locals 3

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/gms/drive/api/f;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/drive/api/a/u;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/f;->b:Lcom/google/android/gms/drive/api/d;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/drive/api/a/u;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 443
    return-void
.end method
