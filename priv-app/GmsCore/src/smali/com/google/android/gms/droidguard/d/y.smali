.class final Lcom/google/android/gms/droidguard/d/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    .line 19
    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/y;->b:Ljava/lang/String;

    .line 20
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 29
    if-ne p0, p1, :cond_0

    .line 30
    const/4 v0, 0x1

    .line 35
    :goto_0
    return v0

    .line 32
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/droidguard/d/y;

    if-nez v0, :cond_1

    .line 33
    const/4 v0, 0x0

    goto :goto_0

    .line 35
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    check-cast p1, Lcom/google/android/gms/droidguard/d/y;

    iget-object v1, p1, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
