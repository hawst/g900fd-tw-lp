.class public Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 124
    new-instance v0, Lcom/google/android/gms/drive/ui/legacy/navigation/c;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    .line 37
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;)Z
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;

    .line 52
    invoke-interface {p1, v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 56
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/Query;
    .locals 3

    .prologue
    .line 61
    new-instance v1, Lcom/google/android/gms/drive/query/f;

    invoke-direct {v1}, Lcom/google/android/gms/drive/query/f;-><init>()V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;

    .line 63
    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;->a()Lcom/google/android/gms/drive/query/Filter;

    move-result-object v0

    .line 64
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/query/f;->a(Lcom/google/android/gms/drive/query/Filter;)Lcom/google/android/gms/drive/query/f;

    goto :goto_0

    .line 66
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/query/f;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/google/android/gms/drive/ui/picker/a/q;
    .locals 6

    .prologue
    .line 88
    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;

    .line 91
    instance-of v3, v0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;

    if-eqz v3, :cond_3

    .line 92
    check-cast v0, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;

    .line 93
    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 94
    if-eqz v1, :cond_1

    .line 98
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "More than one main filter : %s, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c()Lcom/google/android/gms/drive/ui/picker/a/q;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 102
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;->c()Lcom/google/android/gms/drive/ui/picker/a/q;

    move-result-object v0

    :goto_1
    move-object v1, v0

    .line 104
    goto :goto_0

    .line 105
    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 76
    if-ne p0, p1, :cond_1

    .line 82
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;

    if-eqz v2, :cond_3

    .line 79
    check-cast p1, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;

    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p1, p0}, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 82
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-class v2, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Ljava/util/HashSet;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 110
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CriterionSet %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 120
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 122
    return-void
.end method
