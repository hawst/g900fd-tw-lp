.class public final Lcom/google/android/gms/wallet/ow/a;
.super Lcom/google/android/gms/common/ui/a;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnKeyListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field j:Lcom/google/android/gms/wallet/service/k;

.field k:Landroid/view/View;

.field l:Landroid/app/AlertDialog;

.field m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field o:Landroid/widget/Button;

.field p:Landroid/widget/ProgressBar;

.field final q:Lcom/google/android/gms/wallet/service/l;

.field private r:I

.field private s:Lcom/google/android/gms/wallet/ow/c;

.field private t:Z

.field private u:Lcom/google/checkout/inapp/proto/j;

.field private v:Lcom/google/checkout/inapp/proto/a/b;

.field private w:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/a;-><init>()V

    .line 76
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/a;->r:I

    .line 78
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/a;->t:Z

    .line 79
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->u:Lcom/google/checkout/inapp/proto/j;

    .line 80
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->v:Lcom/google/checkout/inapp/proto/a/b;

    .line 81
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 83
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 87
    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->w:Ljava/util/ArrayList;

    .line 447
    new-instance v0, Lcom/google/android/gms/wallet/ow/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/b;-><init>(Lcom/google/android/gms/wallet/ow/a;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->q:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method public static a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/ow/a;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 115
    const-string v1, "address"

    invoke-static {v0, v1, p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 116
    const-string v1, "config"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 117
    const-string v1, "account"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 118
    const-string v1, "addressHints"

    invoke-static {v0, v1, p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    .line 119
    new-instance v1, Lcom/google/android/gms/wallet/ow/a;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/ow/a;-><init>()V

    .line 120
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ow/a;->setArguments(Landroid/os/Bundle;)V

    .line 121
    return-object v1
.end method

.method public static a(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/ow/a;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    const-string v1, "instrument"

    invoke-static {v0, v1, p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 103
    const-string v1, "config"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 104
    const-string v1, "account"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 105
    const-string v1, "addressHints"

    invoke-static {v0, v1, p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    .line 106
    new-instance v1, Lcom/google/android/gms/wallet/ow/a;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/ow/a;-><init>()V

    .line 107
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ow/a;->setArguments(Landroid/os/Bundle;)V

    .line 108
    return-object v1
.end method

.method public static a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/ow/a;
    .locals 2

    .prologue
    .line 127
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 128
    const-string v1, "address"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 129
    const-string v1, "instrument"

    invoke-static {v0, v1, p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 130
    const-string v1, "config"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 131
    const-string v1, "account"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 132
    const-string v1, "addressHints"

    invoke-static {v0, v1, p4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    .line 133
    new-instance v1, Lcom/google/android/gms/wallet/ow/a;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/ow/a;-><init>()V

    .line 134
    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/ow/a;->setArguments(Landroid/os/Bundle;)V

    .line 135
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/a;->v:Lcom/google/checkout/inapp/proto/a/b;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/a;->u:Lcom/google/checkout/inapp/proto/j;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V
    .locals 4

    .prologue
    .line 338
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/o;

    sget v1, Lcom/google/android/gms/p;->Bv:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ow/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/common/ui/validator/o;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 340
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->w:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 342
    new-instance v0, Lcom/google/android/gms/wallet/common/a/u;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/a;->w:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Lcom/google/android/gms/wallet/common/a/u;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/a/q;

    if-eqz v0, :cond_2

    .line 345
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/q;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/a/q;->b()Lcom/google/android/gms/wallet/common/a/n;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 350
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 351
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    .line 352
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ct;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/l;->hj:I

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/wallet/common/ui/ct;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 355
    :cond_1
    return-void

    .line 348
    :cond_2
    new-instance v0, Lcom/google/android/gms/wallet/common/a/n;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/wallet/common/a/n;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/a;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Bv:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/a;)Z
    .locals 1

    .prologue
    .line 55
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/a;->t:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/android/gms/wallet/ow/c;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->s:Lcom/google/android/gms/wallet/ow/c;

    return-object v0
.end method

.method private b()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 296
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/a;->c()I

    move-result v4

    .line 297
    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    move v3, v2

    .line 299
    :goto_0
    and-int/lit8 v0, v4, 0x2

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    .line 301
    :goto_1
    if-eqz v3, :cond_3

    if-eqz v0, :cond_3

    .line 302
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/ow/a;->b(Z)V

    .line 303
    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_2

    .line 304
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "instrument"

    const-class v5, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v3, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 306
    new-instance v3, Lcom/google/checkout/inapp/proto/ap;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/ap;-><init>()V

    .line 307
    new-instance v5, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v5}, Lcom/google/checkout/a/a/a/d;-><init>()V

    iput-object v5, v3, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    .line 308
    iget-object v5, v3, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iput v2, v5, Lcom/google/checkout/a/a/a/d;->a:I

    .line 309
    iget-object v5, v3, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    new-instance v6, Lcom/google/checkout/a/a/a/b;

    invoke-direct {v6}, Lcom/google/checkout/a/a/a/b;-><init>()V

    iput-object v6, v5, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 310
    iget-object v5, v3, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v5, v5, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v6, v6, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iput-object v6, v5, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    .line 312
    iget-object v5, v3, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v5, v5, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iput v2, v5, Lcom/google/checkout/a/a/a/b;->f:I

    .line 313
    iget-object v5, v3, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v5, v5, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    .line 315
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->j:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0, v3, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ap;Z)V

    .line 318
    :cond_2
    and-int/lit8 v0, v4, 0x2

    if-eqz v0, :cond_3

    .line 319
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "address"

    const-class v5, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, v3, v5}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 321
    new-instance v3, Lcom/google/checkout/inapp/proto/ao;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/ao;-><init>()V

    .line 322
    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iput-object v5, v3, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    .line 323
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iput-object v0, v3, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->j:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0, v3, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ao;Z)V

    .line 326
    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_3

    .line 327
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ow/a;->t:Z

    .line 331
    :cond_3
    return-void

    :cond_4
    move v3, v1

    .line 297
    goto/16 :goto_0

    :cond_5
    move v0, v1

    .line 299
    goto/16 :goto_1
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 427
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/a;->p:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 428
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/a;->o:Landroid/widget/Button;

    if-nez p1, :cond_3

    move v0, v2

    :goto_1
    invoke-virtual {v3, v0}, Landroid/widget/Button;->setEnabled(Z)V

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    .line 430
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_4

    move v0, v2

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 433
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_1

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_5

    :goto_3
    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 436
    :cond_1
    return-void

    .line 427
    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    :cond_3
    move v0, v1

    .line 428
    goto :goto_1

    :cond_4
    move v0, v1

    .line 430
    goto :goto_2

    :cond_5
    move v2, v1

    .line 434
    goto :goto_3
.end method

.method private c()I
    .locals 3

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 359
    const/4 v0, 0x0

    .line 360
    const-string v2, "instrument"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 361
    const-string v0, "address"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    const/4 v0, 0x3

    .line 369
    :cond_0
    :goto_0
    return v0

    .line 364
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 366
    :cond_2
    const-string v2, "address"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 367
    const/4 v0, 0x2

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->v:Lcom/google/checkout/inapp/proto/a/b;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ow/a;)Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/a;->t:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ow/a;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/a;->b(Z)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ow/a;)Lcom/google/checkout/inapp/proto/j;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->u:Lcom/google/checkout/inapp/proto/j;

    return-object v0
.end method


# virtual methods
.method final a(Lcom/google/android/gms/wallet/ow/c;)V
    .locals 0

    .prologue
    .line 334
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/a;->s:Lcom/google/android/gms/wallet/ow/c;

    .line 335
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 143
    const-string v0, "config"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 144
    const-string v1, "account"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 146
    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/a;->j:Lcom/google/android/gms/wallet/service/k;

    if-nez v4, :cond_0

    .line 147
    new-instance v4, Lcom/google/android/gms/wallet/service/e;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-direct {v4, v3, v0, v1, v6}, Lcom/google/android/gms/wallet/service/e;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/google/android/gms/wallet/ow/a;->j:Lcom/google/android/gms/wallet/service/k;

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->j:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/service/k;->a()V

    .line 154
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 155
    sget v1, Lcom/google/android/gms/l;->hn:I

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    .line 156
    new-instance v6, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 158
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    move v4, v3

    .line 159
    :goto_0
    if-eqz v4, :cond_7

    .line 160
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 162
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/a;->c()I

    move-result v7

    .line 163
    packed-switch v7, :pswitch_data_0

    move v0, v2

    :goto_2
    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->Q:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 166
    packed-switch v7, :pswitch_data_1

    move v3, v2

    :goto_3
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    .line 167
    const-string v0, "addressHints"

    const-class v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->w:Ljava/util/ArrayList;

    .line 169
    and-int/lit8 v0, v7, 0x1

    if-eqz v0, :cond_1

    .line 170
    const-string v0, "instrument"

    const-class v3, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v5, v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 172
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;)Ljava/lang/String;

    move-result-object v3

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    sget v8, Lcom/google/android/gms/j;->bW:I

    invoke-virtual {v0, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->bY:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 181
    if-eqz v4, :cond_1

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->m:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 186
    :cond_1
    and-int/lit8 v0, v7, 0x2

    if-eqz v0, :cond_2

    .line 187
    const-string v0, "address"

    const-class v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 189
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;)Ljava/lang/String;

    move-result-object v3

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    sget v5, Lcom/google/android/gms/j;->rB:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 192
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 193
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->rH:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/a;->a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 198
    if-eqz v4, :cond_2

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->n:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 203
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pO:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->p:Landroid/widget/ProgressBar;

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->k:Landroid/view/View;

    invoke-virtual {v6, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 205
    sget v0, Lcom/google/android/gms/p;->Au:I

    invoke-virtual {v6, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 206
    sget v0, Lcom/google/android/gms/p;->Af:I

    invoke-virtual {v6, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 207
    invoke-virtual {v6, p0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 209
    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->l:Landroid/app/AlertDialog;

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->l:Landroid/app/AlertDialog;

    return-object v0

    :cond_3
    move v4, v2

    .line 158
    goto/16 :goto_0

    .line 163
    :pswitch_0
    if-eqz v4, :cond_4

    sget v0, Lcom/google/android/gms/p;->Db:I

    goto/16 :goto_2

    :cond_4
    sget v0, Lcom/google/android/gms/p;->zx:I

    goto/16 :goto_2

    :pswitch_1
    if-eqz v4, :cond_5

    sget v0, Lcom/google/android/gms/p;->Dc:I

    goto/16 :goto_2

    :cond_5
    sget v0, Lcom/google/android/gms/p;->zy:I

    goto/16 :goto_2

    :pswitch_2
    if-eqz v4, :cond_6

    sget v0, Lcom/google/android/gms/p;->Da:I

    goto/16 :goto_2

    :cond_6
    sget v0, Lcom/google/android/gms/p;->zw:I

    goto/16 :goto_2

    .line 166
    :pswitch_3
    sget v3, Lcom/google/android/gms/p;->BJ:I

    goto/16 :goto_3

    :pswitch_4
    sget v3, Lcom/google/android/gms/p;->BL:I

    goto/16 :goto_3

    :pswitch_5
    sget v3, Lcom/google/android/gms/p;->BK:I

    goto/16 :goto_3

    :cond_7
    move-object v1, v0

    goto/16 :goto_1

    .line 163
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 166
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 268
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 270
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/a;->b()V

    .line 276
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->s:Lcom/google/android/gms/wallet/ow/c;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->s:Lcom/google/android/gms/wallet/ow/c;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/c;->a(I)V

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 280
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->l:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 281
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/a;->b()V

    .line 282
    return-void

    .line 280
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 215
    invoke-super {p0, p1}, Lcom/google/android/gms/common/ui/a;->onCreate(Landroid/os/Bundle;)V

    .line 217
    if-eqz p1, :cond_2

    .line 218
    const-string v0, "savePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/a;->r:I

    .line 221
    const-string v0, "pendingAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    const-string v0, "pendingAddress"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->v:Lcom/google/checkout/inapp/proto/a/b;

    .line 226
    :cond_0
    const-string v0, "pendingInstrument"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    const-string v0, "pendingInstrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->u:Lcom/google/checkout/inapp/proto/j;

    .line 231
    :cond_1
    const-string v0, "pendingUpdate"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/a;->t:Z

    .line 233
    :cond_2
    return-void
.end method

.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 286
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->s:Lcom/google/android/gms/wallet/ow/c;

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    if-ne p2, v1, :cond_0

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->s:Lcom/google/android/gms/wallet/ow/c;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/ow/c;->a(I)V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/a;->dismiss()V

    .line 289
    const/4 v0, 0x1

    .line 291
    :cond_0
    return v0
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 246
    invoke-super {p0}, Lcom/google/android/gms/common/ui/a;->onResume()V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->j:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->q:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/a;->r:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/a;->r:I

    .line 249
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 253
    invoke-super {p0, p1}, Lcom/google/android/gms/common/ui/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 255
    iget v0, p0, Lcom/google/android/gms/wallet/ow/a;->r:I

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->j:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->q:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/a;->r:I

    .line 256
    :cond_0
    const-string v0, "savePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/a;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    const-string v0, "pendingUpdate"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ow/a;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->v:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_1

    .line 259
    const-string v0, "pendingAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->v:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->u:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_2

    .line 262
    const-string v0, "pendingInstrument"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/a;->u:Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 264
    :cond_2
    return-void
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 237
    invoke-super {p0}, Lcom/google/android/gms/common/ui/a;->onStart()V

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->l:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->o:Landroid/widget/Button;

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/a;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 242
    return-void
.end method
