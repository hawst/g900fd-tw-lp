.class abstract Lcom/google/android/gms/fitness/j/f;
.super Lcom/google/android/gms/fitness/data/m;
.source "SourceFile"

# interfaces
.implements Landroid/app/PendingIntent$OnFinished;


# instance fields
.field private final a:Lcom/google/android/gms/fitness/j/a;

.field private final b:Landroid/app/PendingIntent;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/fitness/d/d;

.field private final e:Landroid/os/Handler;

.field private final f:Landroid/os/PowerManager$WakeLock;

.field private final g:Lcom/google/android/gms/fitness/j/e;

.field private final h:Lcom/google/android/gms/fitness/internal/a;

.field private final i:Lcom/google/android/location/n/ae;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/PendingIntent;Lcom/google/android/gms/fitness/j/e;Lcom/google/android/gms/fitness/internal/a;Lcom/google/android/gms/fitness/d/d;Landroid/os/Handler;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/fitness/data/m;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/f;->a:Lcom/google/android/gms/fitness/j/a;

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/fitness/j/f;->b:Landroid/app/PendingIntent;

    .line 74
    iput-object p1, p0, Lcom/google/android/gms/fitness/j/f;->c:Landroid/content/Context;

    .line 75
    iput-object p4, p0, Lcom/google/android/gms/fitness/j/f;->h:Lcom/google/android/gms/fitness/internal/a;

    .line 76
    iput-object p5, p0, Lcom/google/android/gms/fitness/j/f;->d:Lcom/google/android/gms/fitness/d/d;

    .line 77
    iput-object p6, p0, Lcom/google/android/gms/fitness/j/f;->e:Landroid/os/Handler;

    .line 78
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 80
    const-string v1, "Fitness"

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/f;->f:Landroid/os/PowerManager$WakeLock;

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v2}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 82
    invoke-static {p1}, Lcom/google/android/gms/common/util/be;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p4, Lcom/google/android/gms/fitness/internal/a;->c:Z

    if-eqz v0, :cond_0

    .line 84
    new-instance v0, Lcom/google/android/gms/location/internal/ClientIdentity;

    iget-object v1, p0, Lcom/google/android/gms/fitness/j/f;->h:Lcom/google/android/gms/fitness/internal/a;

    iget v1, v1, Lcom/google/android/gms/fitness/internal/a;->a:I

    iget-object v2, p0, Lcom/google/android/gms/fitness/j/f;->h:Lcom/google/android/gms/fitness/internal/a;

    iget-object v2, v2, Lcom/google/android/gms/fitness/internal/a;->b:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/location/internal/ClientIdentity;-><init>(ILjava/lang/String;)V

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/fitness/j/f;->f:Landroid/os/PowerManager$WakeLock;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/location/n/j;->a(Ljava/util/Collection;)Landroid/os/WorkSource;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/PowerManager$WakeLock;->setWorkSource(Landroid/os/WorkSource;)V

    .line 90
    :cond_0
    iput-object p3, p0, Lcom/google/android/gms/fitness/j/f;->g:Lcom/google/android/gms/fitness/j/e;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/n/ae;->a(Landroid/content/Context;)Lcom/google/android/location/n/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/f;->i:Lcom/google/android/location/n/ae;

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/j/a;Lcom/google/android/gms/fitness/j/e;Lcom/google/android/gms/fitness/internal/a;Lcom/google/android/gms/fitness/d/d;Landroid/os/Handler;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/fitness/data/m;-><init>()V

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/fitness/j/f;->a:Lcom/google/android/gms/fitness/j/a;

    .line 56
    iput-object p3, p0, Lcom/google/android/gms/fitness/j/f;->g:Lcom/google/android/gms/fitness/j/e;

    .line 57
    iput-object p4, p0, Lcom/google/android/gms/fitness/j/f;->h:Lcom/google/android/gms/fitness/internal/a;

    .line 58
    iput-object p1, p0, Lcom/google/android/gms/fitness/j/f;->c:Landroid/content/Context;

    .line 59
    iput-object p5, p0, Lcom/google/android/gms/fitness/j/f;->d:Lcom/google/android/gms/fitness/d/d;

    .line 60
    iput-object p6, p0, Lcom/google/android/gms/fitness/j/f;->e:Landroid/os/Handler;

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/location/n/ae;->a(Landroid/content/Context;)Lcom/google/android/location/n/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/j/f;->i:Lcom/google/android/location/n/ae;

    .line 62
    iput-object v1, p0, Lcom/google/android/gms/fitness/j/f;->b:Landroid/app/PendingIntent;

    .line 63
    iput-object v1, p0, Lcom/google/android/gms/fitness/j/f;->f:Landroid/os/PowerManager$WakeLock;

    .line 64
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/j/e;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->g:Lcom/google/android/gms/fitness/j/e;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/j/f;Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 6

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->a:Lcom/google/android/gms/fitness/j/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->a:Lcom/google/android/gms/fitness/j/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/j/a;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    :goto_0
    return-void

    :cond_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p1, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->b:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/google/android/gms/fitness/j/f;->c:Landroid/content/Context;

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v4, p0

    invoke-virtual/range {v0 .. v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;Landroid/app/PendingIntent$OnFinished;Landroid/os/Handler;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/internal/a;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->h:Lcom/google/android/gms/fitness/internal/a;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/d/d;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->d:Lcom/google/android/gms/fitness/d/d;

    return-object v0
.end method

.method private c()Z
    .locals 3

    .prologue
    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/j/f;->h:Lcom/google/android/gms/fitness/internal/a;

    iget-object v1, v1, Lcom/google/android/gms/fitness/internal/a;->b:Ljava/lang/String;

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 215
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 213
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/fitness/j/f;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->b:Landroid/app/PendingIntent;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/fitness/j/f;)Z
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/fitness/j/f;->c()Z

    move-result v0

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/location/n/ae;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->i:Lcom/google/android/location/n/ae;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/fitness/j/f;)Lcom/google/android/gms/fitness/j/a;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->a:Lcom/google/android/gms/fitness/j/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/fitness/j/f;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->f:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method


# virtual methods
.method abstract a()V
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/j/g;

    invoke-direct {v1, p0, p1, v2}, Lcom/google/android/gms/fitness/j/g;-><init>(Lcom/google/android/gms/fitness/j/f;Lcom/google/android/gms/fitness/data/DataPoint;B)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-result v0

    .line 109
    if-nez v0, :cond_0

    .line 110
    const-string v0, "Could not schedule data point handler"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 112
    :cond_0
    return-void
.end method

.method abstract b()V
.end method

.method public onSendFinished(Landroid/app/PendingIntent;Landroid/content/Intent;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/fitness/j/f;->f:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 206
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 220
    const-string v0, "UnderlyingListener{%s} Timeout: %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/fitness/j/f;->a:Lcom/google/android/gms/fitness/j/a;

    iget-object v4, p0, Lcom/google/android/gms/fitness/j/f;->b:Landroid/app/PendingIntent;

    invoke-static {v3, v4}, Lcom/google/k/a/ac;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/fitness/j/f;->g:Lcom/google/android/gms/fitness/j/e;

    iget-wide v4, v3, Lcom/google/android/gms/fitness/j/e;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
