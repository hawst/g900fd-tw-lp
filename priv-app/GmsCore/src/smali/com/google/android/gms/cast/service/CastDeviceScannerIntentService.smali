.class public Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const-string v0, "CastDeviceScannerIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/c/q;)V
    .locals 1

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/gms/cast/service/a/s;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/service/a/s;-><init>(Lcom/google/android/gms/cast/c/q;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/a;)V

    .line 106
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/c/q;Z)V
    .locals 1

    .prologue
    .line 101
    new-instance v0, Lcom/google/android/gms/cast/service/a/q;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/q;-><init>(Lcom/google/android/gms/cast/c/q;Z)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/a;)V

    .line 102
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/cast/service/a;)V
    .locals 1

    .prologue
    .line 36
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 37
    sget-object v0, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 38
    const-string v0, "com.google.android.gms.cast.service.DEVICE_SCANNER_INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 39
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/cast/c/q;Z)V
    .locals 1

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/cast/service/a/l;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/l;-><init>(Lcom/google/android/gms/cast/c/q;Z)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/a;)V

    .line 111
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/service/a;

    .line 66
    if-nez v0, :cond_0

    .line 67
    const-string v0, "CastDeviceScannerIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :goto_0
    return-void

    .line 78
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/a;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 84
    :catch_0
    move-exception v0

    goto :goto_0

    :catchall_0
    move-exception v0

    throw v0
.end method
