.class public final enum Lcom/google/android/gms/drive/ui/picker/ab;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/picker/ab;

.field public static final enum b:Lcom/google/android/gms/drive/ui/picker/ab;

.field public static final enum c:Lcom/google/android/gms/drive/ui/picker/ab;

.field public static final enum d:Lcom/google/android/gms/drive/ui/picker/ab;

.field private static final i:[Lcom/google/android/gms/drive/ui/picker/ab;

.field private static final synthetic j:[Lcom/google/android/gms/drive/ui/picker/ab;


# instance fields
.field final e:Ljava/lang/String;

.field final f:Lcom/google/android/gms/drive/ui/picker/a/q;

.field final g:I

.field final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v2, 0x0

    .line 120
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/ab;

    const-string v1, "MY_DRIVE"

    const-string v3, "myDrive"

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/a/q;->e:Lcom/google/android/gms/drive/ui/picker/a/q;

    sget v5, Lcom/google/android/gms/h;->bD:I

    sget v6, Lcom/google/android/gms/p;->gY:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/ui/picker/ab;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/ui/picker/a/q;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    .line 122
    new-instance v3, Lcom/google/android/gms/drive/ui/picker/ab;

    const-string v4, "INCOMING"

    const-string v6, "incoming"

    sget-object v7, Lcom/google/android/gms/drive/ui/picker/a/q;->d:Lcom/google/android/gms/drive/ui/picker/a/q;

    sget v8, Lcom/google/android/gms/h;->bC:I

    sget v9, Lcom/google/android/gms/p;->gX:I

    move v5, v10

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/drive/ui/picker/ab;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/ui/picker/a/q;II)V

    sput-object v3, Lcom/google/android/gms/drive/ui/picker/ab;->b:Lcom/google/android/gms/drive/ui/picker/ab;

    .line 124
    new-instance v3, Lcom/google/android/gms/drive/ui/picker/ab;

    const-string v4, "RECENT"

    const-string v6, "recent"

    sget-object v7, Lcom/google/android/gms/drive/ui/picker/a/q;->f:Lcom/google/android/gms/drive/ui/picker/a/q;

    sget v8, Lcom/google/android/gms/h;->bE:I

    sget v9, Lcom/google/android/gms/p;->gZ:I

    move v5, v11

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/drive/ui/picker/ab;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/ui/picker/a/q;II)V

    sput-object v3, Lcom/google/android/gms/drive/ui/picker/ab;->c:Lcom/google/android/gms/drive/ui/picker/ab;

    .line 126
    new-instance v3, Lcom/google/android/gms/drive/ui/picker/ab;

    const-string v4, "STARRED"

    const-string v6, "starred"

    sget-object v7, Lcom/google/android/gms/drive/ui/picker/a/q;->b:Lcom/google/android/gms/drive/ui/picker/a/q;

    sget v8, Lcom/google/android/gms/h;->bF:I

    sget v9, Lcom/google/android/gms/p;->ha:I

    move v5, v12

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/drive/ui/picker/ab;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/ui/picker/a/q;II)V

    sput-object v3, Lcom/google/android/gms/drive/ui/picker/ab;->d:Lcom/google/android/gms/drive/ui/picker/ab;

    .line 119
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/ab;

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/ab;->b:Lcom/google/android/gms/drive/ui/picker/ab;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/ab;->c:Lcom/google/android/gms/drive/ui/picker/ab;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/ab;->d:Lcom/google/android/gms/drive/ui/picker/ab;

    aput-object v1, v0, v12

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->j:[Lcom/google/android/gms/drive/ui/picker/ab;

    .line 129
    invoke-static {}, Lcom/google/android/gms/drive/ui/picker/ab;->values()[Lcom/google/android/gms/drive/ui/picker/ab;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->i:[Lcom/google/android/gms/drive/ui/picker/ab;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/ui/picker/a/q;II)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 138
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/ab;->e:Ljava/lang/String;

    .line 139
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/ab;->f:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 140
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/ab;->g:I

    .line 141
    iput p6, p0, Lcom/google/android/gms/drive/ui/picker/ab;->h:I

    .line 142
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/ab;
    .locals 5

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/gms/drive/ui/picker/ab;->values()[Lcom/google/android/gms/drive/ui/picker/ab;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/google/android/gms/drive/ui/picker/ab;->e:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid TopCollection name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic c()[Lcom/google/android/gms/drive/ui/picker/ab;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->i:[Lcom/google/android/gms/drive/ui/picker/ab;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/ab;
    .locals 1

    .prologue
    .line 119
    const-class v0, Lcom/google/android/gms/drive/ui/picker/ab;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/ab;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/picker/ab;
    .locals 1

    .prologue
    .line 119
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->j:[Lcom/google/android/gms/drive/ui/picker/ab;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/picker/ab;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/ab;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/ab;->g:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/ab;->h:I

    return v0
.end method
