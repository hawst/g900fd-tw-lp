.class public final Lcom/google/android/gms/googlehelp/e/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/googlehelp/e/b;


# instance fields
.field public a:Lcom/google/android/gms/googlehelp/e/c;

.field public b:Lcom/google/android/gms/googlehelp/e/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 272
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 273
    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/b;->cachedSize:I

    .line 274
    return-void
.end method

.method public static a()[Lcom/google/android/gms/googlehelp/e/b;
    .locals 2

    .prologue
    .line 255
    sget-object v0, Lcom/google/android/gms/googlehelp/e/b;->c:[Lcom/google/android/gms/googlehelp/e/b;

    if-nez v0, :cond_1

    .line 256
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 258
    :try_start_0
    sget-object v0, Lcom/google/android/gms/googlehelp/e/b;->c:[Lcom/google/android/gms/googlehelp/e/b;

    if-nez v0, :cond_0

    .line 259
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/googlehelp/e/b;

    sput-object v0, Lcom/google/android/gms/googlehelp/e/b;->c:[Lcom/google/android/gms/googlehelp/e/b;

    .line 261
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 263
    :cond_1
    sget-object v0, Lcom/google/android/gms/googlehelp/e/b;->c:[Lcom/google/android/gms/googlehelp/e/b;

    return-object v0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 337
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 338
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v1, :cond_0

    .line 339
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 342
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    if-eqz v1, :cond_1

    .line 343
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 346
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 285
    if-ne p1, p0, :cond_1

    .line 310
    :cond_0
    :goto_0
    return v0

    .line 288
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 289
    goto :goto_0

    .line 291
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/b;

    .line 292
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    if-nez v2, :cond_3

    .line 293
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v2, :cond_4

    move v0, v1

    .line 294
    goto :goto_0

    .line 297
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/googlehelp/e/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 298
    goto :goto_0

    .line 301
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    if-nez v2, :cond_5

    .line 302
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    if-eqz v2, :cond_0

    move v0, v1

    .line 303
    goto :goto_0

    .line 306
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/googlehelp/e/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 307
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 318
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 320
    return v0

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/e/c;->hashCode()I

    move-result v0

    goto :goto_0

    .line 318
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/e/d;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 249
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/googlehelp/e/c;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/googlehelp/e/d;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    if-eqz v0, :cond_0

    .line 327
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 329
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    if-eqz v0, :cond_1

    .line 330
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 332
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 333
    return-void
.end method
