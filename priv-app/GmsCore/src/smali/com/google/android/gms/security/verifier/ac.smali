.class public final Lcom/google/android/gms/security/verifier/ac;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/net/ConnectivityManager;

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/ac;->a:Landroid/net/ConnectivityManager;

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/net/ConnectivityManager;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/security/verifier/ac;->a:Landroid/net/ConnectivityManager;

    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/ac;->b()V

    .line 30
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/ac;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 39
    if-nez v1, :cond_0

    .line 40
    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/ac;->b:Z

    .line 47
    :goto_0
    return-void

    .line 42
    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/security/verifier/ac;->b:Z

    .line 43
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v1, v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/ac;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    :cond_1
    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/ac;->c:Z

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/ac;->a:Landroid/net/ConnectivityManager;

    invoke-static {v0}, Landroid/support/v4/d/a;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/ac;->d:Z

    goto :goto_0

    .line 43
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/ac;->a:Landroid/net/ConnectivityManager;

    invoke-virtual {v1}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/security/verifier/ac;->a()Z

    move-result v0

    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/ac;->b()V

    .line 64
    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/security/verifier/ac;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Landroid/content/Context;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 67
    :cond_0
    monitor-exit p0

    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/ac;->b:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/ac;->c:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/ac;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
