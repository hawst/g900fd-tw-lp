.class final Lcom/google/android/gms/drive/metadata/sync/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/model/a;

.field private final b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

.field private final c:I

.field private d:Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;

.field private e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

.field private final f:Lcom/google/android/gms/drive/database/model/bb;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;Lcom/google/android/gms/drive/database/model/bb;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->d:Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;

    .line 38
    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    .line 53
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    if-ltz p4, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    .line 58
    iput p4, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->c:I

    .line 59
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->f:Lcom/google/android/gms/drive/database/model/bb;

    .line 60
    return-void

    .line 55
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 78
    if-eqz p2, :cond_0

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->d:Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;

    if-eqz v1, :cond_2

    .line 80
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->d:Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;

    iget-boolean v2, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;->c:Z

    const-string v3, "Must not call this method before finish()"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;->b:Ljava/lang/String;

    .line 81
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b()Ljava/util/Date;

    move-result-object v2

    .line 82
    if-nez v2, :cond_1

    .line 83
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->f:Lcom/google/android/gms/drive/database/model/bb;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/drive/database/model/bb;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->f:Lcom/google/android/gms/drive/database/model/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bb;->i()V

    .line 92
    :cond_0
    :goto_1
    return-void

    .line 82
    :cond_1
    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 88
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-interface {v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a()V

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V
    .locals 4

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->f:Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->f:Lcom/google/android/gms/drive/database/model/bb;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 67
    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;J)V

    iput-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    .line 68
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->d:Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->f:Lcom/google/android/gms/drive/database/model/bb;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget v2, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->c:I

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->d:Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;

    invoke-virtual {p1, v1, v0, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;ILcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 74
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 96
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "SyncMoreAlgorithm[delegate=%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/b/i;->b:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
