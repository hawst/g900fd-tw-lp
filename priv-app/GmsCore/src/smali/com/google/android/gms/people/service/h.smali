.class public final Lcom/google/android/gms/people/service/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final g:Lcom/google/android/gms/people/service/j;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private final e:Landroid/database/ContentObserver;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 237
    new-instance v0, Lcom/google/android/gms/people/service/j;

    const/16 v1, 0x8

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/gms/people/service/j;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/people/service/h;->g:Lcom/google/android/gms/people/service/j;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/service/h;->a:Ljava/lang/Object;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/service/h;->c:Ljava/util/ArrayList;

    .line 69
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    .line 72
    new-instance v0, Lcom/google/android/gms/people/service/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/service/i;-><init>(Lcom/google/android/gms/people/service/h;)V

    iput-object v0, p0, Lcom/google/android/gms/people/service/h;->e:Landroid/database/ContentObserver;

    .line 84
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/service/h;->b:Landroid/content/Context;

    .line 85
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;
    .locals 1

    .prologue
    .line 88
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->f()Lcom/google/android/gms/people/service/h;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/people/service/j;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 181
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    const-string v0, "PeopleNotification"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Data changed: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/android/gms/people/service/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v4, p1, Lcom/google/android/gms/people/service/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "  scope="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v4, p1, Lcom/google/android/gms/people/service/j;->c:I

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/people/service/h;->a:Ljava/lang/Object;

    monitor-enter v4

    move v2, v1

    .line 188
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/k;

    .line 191
    iget v5, v0, Lcom/google/android/gms/people/service/j;->c:I

    iget v6, p1, Lcom/google/android/gms/people/service/j;->c:I

    and-int/2addr v5, v6

    if-eqz v5, :cond_5

    .line 193
    iget-object v5, v0, Lcom/google/android/gms/people/service/k;->a:Ljava/lang/String;

    if-eqz v5, :cond_1

    iget-object v5, p1, Lcom/google/android/gms/people/service/j;->a:Ljava/lang/String;

    if-eqz v5, :cond_1

    .line 194
    iget-object v5, v0, Lcom/google/android/gms/people/service/k;->a:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/android/gms/people/service/j;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 195
    iget-object v5, v0, Lcom/google/android/gms/people/service/k;->b:Ljava/lang/String;

    iget-object v6, p1, Lcom/google/android/gms/people/service/j;->b:Ljava/lang/String;

    invoke-static {v5, v6}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 198
    :cond_1
    if-nez v3, :cond_2

    .line 199
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 200
    const-string v5, "scope"

    iget v6, p1, Lcom/google/android/gms/people/service/j;->c:I

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 201
    const-string v5, "account"

    iget-object v6, p1, Lcom/google/android/gms/people/service/j;->a:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    const-string v5, "pagegaiaid"

    iget-object v6, p1, Lcom/google/android/gms/people/service/j;->b:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    :cond_2
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/people/service/k;->d:Lcom/google/android/gms/people/internal/f;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-interface {v0, v5, v6, v3}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    move v1, v2

    move-object v2, v3

    .line 188
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move-object v3, v2

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 209
    :catch_0
    move-exception v0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 210
    add-int/lit8 v0, v1, -0x1

    .line 211
    const/4 v1, 0x1

    move-object v2, v3

    goto :goto_1

    .line 214
    :cond_3
    if-eqz v2, :cond_4

    .line 215
    invoke-direct {p0}, Lcom/google/android/gms/people/service/h;->c()V

    .line 217
    :cond_4
    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_5
    move v0, v1

    move v1, v2

    move-object v2, v3

    goto :goto_1
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/gms/people/service/h;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lcom/google/android/gms/people/service/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/service/h;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/h;->f:Z

    if-nez v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 154
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/people/service/h;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    const-string v0, "PeopleNotification"

    const-string v1, "Unregistering CP2 observer..."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/h;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/h;->f:Z

    goto :goto_0
.end method

.method private d()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 169
    iget-object v3, p0, Lcom/google/android/gms/people/service/h;->a:Ljava/lang/Object;

    monitor-enter v3

    move v2, v1

    .line 170
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/k;

    iget v0, v0, Lcom/google/android/gms/people/service/j;->c:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    .line 172
    const/4 v0, 0x1

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :goto_1
    return v0

    .line 170
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 175
    :cond_1
    monitor-exit v3

    move v0, v1

    goto :goto_1

    .line 176
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lcom/google/android/gms/people/service/h;->g:Lcom/google/android/gms/people/service/j;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/service/h;->a(Lcom/google/android/gms/people/service/j;)V

    .line 249
    return-void
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 122
    const-string v0, "PeopleNotification"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unregister: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/people/internal/f;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v3, p0, Lcom/google/android/gms/people/service/h;->a:Ljava/lang/Object;

    monitor-enter v3

    move v2, v1

    .line 126
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/k;

    iget-object v0, v0, Lcom/google/android/gms/people/service/k;->d:Lcom/google/android/gms/people/internal/f;

    invoke-interface {v0}, Lcom/google/android/gms/people/internal/f;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/internal/f;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    if-ne v0, v4, :cond_2

    .line 128
    const-string v0, "PeopleNotification"

    const-string v2, "  Removed."

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 130
    add-int/lit8 v0, v1, -0x1

    .line 131
    const/4 v1, 0x1

    .line 126
    :goto_1
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 134
    :cond_0
    if-eqz v2, :cond_1

    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/people/service/h;->c()V

    .line 137
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 105
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const-string v0, "PeopleNotification"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Register: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  scopes="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/people/internal/f;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/service/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->d:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/gms/people/service/k;

    invoke-direct {v2, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/k;-><init>(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 114
    and-int/lit8 v0, p4, 0x8

    if-eqz v0, :cond_1

    .line 115
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/h;->f:Z

    if-nez v0, :cond_1

    const-string v0, "PeopleNotification"

    const-string v2, "Registering CP2 observer..."

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Landroid/provider/ContactsContract;->AUTHORITY_URI:Landroid/net/Uri;

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/people/service/h;->e:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/h;->f:Z

    .line 117
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 224
    iget-object v1, p0, Lcom/google/android/gms/people/service/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 226
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/j;

    .line 227
    iget-object v3, v0, Lcom/google/android/gms/people/service/j;->a:Ljava/lang/String;

    invoke-static {v3, p1}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/gms/people/service/j;->b:Ljava/lang/String;

    invoke-static {v3, p2}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 229
    iget v2, v0, Lcom/google/android/gms/people/service/j;->c:I

    or-int/2addr v2, p3

    iput v2, v0, Lcom/google/android/gms/people/service/j;->c:I

    .line 230
    monitor-exit v1

    .line 234
    :goto_0
    return-void

    .line 233
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->c:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/gms/people/service/j;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/gms/people/service/j;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 255
    iget-object v1, p0, Lcom/google/android/gms/people/service/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/j;

    .line 258
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/service/h;->a(Lcom/google/android/gms/people/service/j;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/people/service/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 263
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 261
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/people/service/h;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 263
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    return-void
.end method
