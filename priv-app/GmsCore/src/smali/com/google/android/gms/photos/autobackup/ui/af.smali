.class public final Lcom/google/android/gms/photos/autobackup/ui/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/a/b;
.implements Lcom/google/android/libraries/social/i/ad;
.implements Lcom/google/android/libraries/social/i/ag;
.implements Lcom/google/android/libraries/social/i/ah;
.implements Lcom/google/android/libraries/social/i/ak;


# instance fields
.field final a:Landroid/app/Activity;

.field final b:I

.field c:Z

.field private d:Lcom/google/android/gms/photos/autobackup/l;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/libraries/social/i/w;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->a:Landroid/app/Activity;

    .line 42
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->b:I

    .line 43
    invoke-virtual {p2, p0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;

    .line 45
    new-instance v0, Lcom/google/android/gms/photos/autobackup/m;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/ag;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/ag;-><init>(Lcom/google/android/gms/photos/autobackup/ui/af;)V

    invoke-direct {v0, p2, v1}, Lcom/google/android/gms/photos/autobackup/m;-><init>(Lcom/google/android/libraries/social/i/w;Lcom/google/android/gms/photos/autobackup/n;)V

    .line 53
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->d:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->d:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/l;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->d:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v0

    .line 85
    invoke-static {}, Lcom/google/android/libraries/social/autobackup/s;->a()Z

    move-result v1

    .line 86
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->a:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->d:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v3}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    .line 88
    if-eqz v1, :cond_2

    if-nez v2, :cond_0

    .line 89
    :cond_2
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 90
    const-string v4, "PromptEnableSyncMixin.account_name"

    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 91
    const-string v0, "PromptEnableSyncMixin.master_sync_enabled"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 92
    const-string v0, "PromptEnableSyncMixin.account_sync_enabled"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->a:Landroid/app/Activity;

    iget v1, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->b:I

    invoke-virtual {v0, v1, v3}, Landroid/app/Activity;->showDialog(ILandroid/os/Bundle;)Z

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 62
    if-eqz p1, :cond_0

    .line 63
    const-string v0, "PromptEnableSyncMixin.sync_disabled_dialog_shown"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->c:Z

    .line 65
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/libraries/social/a/a;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/l;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->d:Lcom/google/android/gms/photos/autobackup/l;

    .line 58
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/af;->a()V

    .line 75
    return-void
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 69
    const-string v0, "PromptEnableSyncMixin.sync_disabled_dialog_shown"

    iget-boolean v1, p0, Lcom/google/android/gms/photos/autobackup/ui/af;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 70
    return-void
.end method
