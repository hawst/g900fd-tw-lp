.class public final Lcom/google/android/gms/photos/InitializePhotosIntentReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 26
    const-string v0, "com.google.android.gms.INITIALIZE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 30
    :cond_1
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    .line 31
    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {p1, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/autobackup/o;

    .line 35
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/s;->e(Landroid/content/Context;)V

    .line 37
    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/s;->f(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 38
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-static {p1, v2, v3, v4}, Lcom/google/android/libraries/social/autobackup/c/b;->a(Landroid/content/Context;JZ)V

    .line 43
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v1

    .line 44
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 46
    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    invoke-static {p1, v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 49
    invoke-static {p1, v0}, Lcom/google/android/libraries/social/autobackup/AutoBackupSyncService;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
