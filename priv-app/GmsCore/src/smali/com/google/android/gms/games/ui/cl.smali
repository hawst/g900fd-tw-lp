.class final Lcom/google/android/gms/games/ui/cl;
.super Lcom/google/android/play/headerlist/j;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/games/ui/cn;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/cn;)V
    .locals 1

    .prologue
    .line 265
    invoke-interface {p1}, Lcom/google/android/gms/games/ui/cn;->m()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/play/headerlist/j;-><init>(Landroid/content/Context;)V

    .line 266
    iput-object p1, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    .line 267
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/cn;->b(Landroid/content/Context;)Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;

    move-result-object v0

    return-object v0
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    .line 272
    return-void
.end method

.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/ui/cn;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    .line 277
    return-void
.end method

.method protected final a(Landroid/view/ViewGroup;)V
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/cn;->a(Landroid/view/ViewGroup;)V

    .line 282
    return-void
.end method

.method protected final b()F
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->h()F

    move-result v0

    return v0
.end method

.method protected final c()I
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/cn;->m()Landroid/app/Activity;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/cn;->a(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected final d()I
    .locals 1

    .prologue
    .line 297
    sget v0, Lcom/google/android/gms/j;->nn:I

    return v0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 302
    const v0, 0x102000a

    return v0
.end method

.method protected final f()Z
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->g()Z

    move-result v0

    return v0
.end method

.method protected final g()Z
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x1

    return v0
.end method

.method protected final h()I
    .locals 1

    .prologue
    .line 317
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->k()I

    move-result v0

    return v0
.end method

.method protected final i()I
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->l()I

    move-result v0

    return v0
.end method

.method protected final j()I
    .locals 1

    .prologue
    .line 327
    sget v0, Lcom/google/android/gms/j;->oJ:I

    return v0
.end method

.method protected final k()I
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    const/4 v0, 0x3

    return v0
.end method

.method protected final l()I
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    const/4 v0, 0x1

    return v0
.end method

.method protected final m()Z
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->i()Z

    move-result v0

    return v0
.end method

.method protected final n()I
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    const/4 v0, 0x0

    return v0
.end method

.method protected final o()I
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cn;->j()I

    move-result v0

    return v0
.end method

.method protected final p()I
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    const/4 v0, 0x0

    return v0
.end method

.method protected final q()I
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cl;->a:Lcom/google/android/gms/games/ui/cn;

    const/4 v0, 0x0

    return v0
.end method
