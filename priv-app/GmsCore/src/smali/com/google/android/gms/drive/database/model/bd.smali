.class public final enum Lcom/google/android/gms/drive/database/model/bd;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/bd;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/bd;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/bd;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/bd;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/bd;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/bd;

.field private static final synthetic h:[Lcom/google/android/gms/drive/database/model/bd;


# instance fields
.field private final g:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 39
    new-instance v0, Lcom/google/android/gms/drive/database/model/bd;

    const-string v1, "ACCOUNT_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->d()Lcom/google/android/gms/drive/database/model/bc;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "accountId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/bd;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bd;->a:Lcom/google/android/gms/drive/database/model/bd;

    .line 43
    new-instance v0, Lcom/google/android/gms/drive/database/model/bd;

    const-string v1, "FEED_TYPE"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->d()Lcom/google/android/gms/drive/database/model/bc;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "feedType"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    sget-object v4, Lcom/google/android/gms/drive/metadata/sync/a/e;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/metadata/sync/a/e;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/bd;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bd;->b:Lcom/google/android/gms/drive/database/model/bd;

    .line 46
    new-instance v0, Lcom/google/android/gms/drive/database/model/bd;

    const-string v1, "FEED_PARAMETERS"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->d()Lcom/google/android/gms/drive/database/model/bc;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "feedParameters"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/bd;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bd;->c:Lcom/google/android/gms/drive/database/model/bd;

    .line 49
    new-instance v0, Lcom/google/android/gms/drive/database/model/bd;

    const-string v1, "NEXT_PAGE_TOKEN"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->d()Lcom/google/android/gms/drive/database/model/bc;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "nextPageToken"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/bd;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bd;->d:Lcom/google/android/gms/drive/database/model/bd;

    .line 52
    new-instance v0, Lcom/google/android/gms/drive/database/model/bd;

    const-string v1, "CLIP_TIME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->d()Lcom/google/android/gms/drive/database/model/bc;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "clipTime"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    const-wide v4, 0x7fffffffffffffffL

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v7, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/bd;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bd;->e:Lcom/google/android/gms/drive/database/model/bd;

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/database/model/bd;

    const-string v1, "NUM_PAGES_RETRIEVED"

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->d()Lcom/google/android/gms/drive/database/model/bc;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "numPagesRetrieved"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3, v7, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bd;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bd;->f:Lcom/google/android/gms/drive/database/model/bd;

    .line 38
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/bd;

    sget-object v1, Lcom/google/android/gms/drive/database/model/bd;->a:Lcom/google/android/gms/drive/database/model/bd;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/bd;->b:Lcom/google/android/gms/drive/database/model/bd;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/bd;->c:Lcom/google/android/gms/drive/database/model/bd;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/bd;->d:Lcom/google/android/gms/drive/database/model/bd;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/bd;->e:Lcom/google/android/gms/drive/database/model/bd;

    aput-object v1, v0, v11

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/database/model/bd;->f:Lcom/google/android/gms/drive/database/model/bd;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/bd;->h:[Lcom/google/android/gms/drive/database/model/bd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bd;->g:Lcom/google/android/gms/drive/database/model/ab;

    .line 64
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/bd;
    .locals 1

    .prologue
    .line 38
    const-class v0, Lcom/google/android/gms/drive/database/model/bd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bd;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/bd;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/gms/drive/database/model/bd;->h:[Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/bd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/bd;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bd;->g:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bd;->g:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
