.class public final Lcom/google/android/gms/wallet/common/ui/ce;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# static fields
.field private static b:Landroid/graphics/ColorMatrixColorFilter;


# instance fields
.field final a:Lcom/google/android/gms/wallet/common/ui/cf;

.field private final c:Lcom/google/android/gms/wallet/common/ui/cf;

.field private d:Landroid/content/Context;

.field private e:Landroid/widget/AdapterView;

.field private f:Lcom/google/android/gms/wallet/common/ui/cd;

.field private g:Lcom/google/checkout/inapp/proto/j;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:[I

.field private m:[I

.field private n:[Ljava/lang/String;

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/ce;->j:Z

    .line 63
    iput-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/ce;->k:Z

    .line 67
    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/ce;->o:I

    .line 68
    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/ce;->p:I

    .line 69
    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/ce;->q:I

    .line 70
    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/ce;->r:I

    .line 71
    iput v4, p0, Lcom/google/android/gms/wallet/common/ui/ce;->s:I

    .line 85
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->d:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    .line 88
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cf;

    sget v1, Lcom/google/android/gms/p;->zu:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    sget v3, Lcom/google/android/gms/d;->c:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/cf;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->c:Lcom/google/android/gms/wallet/common/ui/cf;

    .line 92
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cf;

    sget v1, Lcom/google/android/gms/p;->Cz:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v4, v4}, Lcom/google/android/gms/wallet/common/ui/cf;-><init>(Ljava/lang/String;ZI)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->a:Lcom/google/android/gms/wallet/common/ui/cf;

    .line 95
    sget-object v0, Lcom/google/android/gms/r;->bh:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/google/android/gms/r;->bi:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->h:Z

    sget v1, Lcom/google/android/gms/r;->bo:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->i:Z

    sget v1, Lcom/google/android/gms/r;->bn:I

    invoke-virtual {v0, v1, v4}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->o:I

    sget v1, Lcom/google/android/gms/r;->bm:I

    sget v2, Lcom/google/android/gms/l;->gW:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->p:I

    sget v1, Lcom/google/android/gms/r;->bl:I

    sget v2, Lcom/google/android/gms/l;->gX:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->q:I

    sget v1, Lcom/google/android/gms/r;->bk:I

    sget v2, Lcom/google/android/gms/l;->gO:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->r:I

    sget v1, Lcom/google/android/gms/r;->bj:I

    sget v2, Lcom/google/android/gms/l;->gP:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->s:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 96
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ce;)Landroid/widget/AdapterView;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ch;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ch;->a:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(IZ)V
    .locals 1

    .prologue
    .line 366
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 367
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ce;->d()V

    .line 368
    const/4 p1, 0x0

    .line 370
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;

    if-eqz v0, :cond_1

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/InstrumentSelectorExpander;->a(IZ)V

    .line 375
    :goto_0
    return-void

    .line 373
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    goto :goto_0
.end method

.method static synthetic b()Landroid/graphics/ColorMatrixColorFilter;
    .locals 3

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/ce;->b:Landroid/graphics/ColorMatrixColorFilter;

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v1, Landroid/graphics/ColorMatrix;

    const/16 v2, 0x14

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v1, v2}, Landroid/graphics/ColorMatrix;-><init>([F)V

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->postConcat(Landroid/graphics/ColorMatrix;)V

    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    sput-object v1, Lcom/google/android/gms/wallet/common/ui/ce;->b:Landroid/graphics/ColorMatrixColorFilter;

    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/ce;->b:Landroid/graphics/ColorMatrixColorFilter;

    return-object v0

    :array_0
    .array-data 4
        0x3f333333    # 0.7f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f333333    # 0.7f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f333333    # 0.7f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f333333    # 0.7f
        0x0
    .end array-data
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/ce;)[I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->l:[I

    return-object v0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    .line 380
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cg;->a(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->a:Lcom/google/android/gms/wallet/common/ui/cf;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 382
    :goto_0
    return v0

    :cond_0
    move v0, v1

    .line 380
    goto :goto_0

    :cond_1
    move v0, v1

    .line 382
    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/ce;)[I
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->m:[I

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/ce;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->q:I

    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 387
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ce;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    :cond_0
    :goto_0
    return-void

    .line 390
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    .line 392
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/ch;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->a:Lcom/google/android/gms/wallet/common/ui/cf;

    invoke-direct {v1, v2}, Lcom/google/android/gms/wallet/common/ui/ch;-><init>(Lcom/google/android/gms/wallet/common/ui/cf;)V

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/cg;->insert(Ljava/lang/Object;I)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/ce;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->p:I

    return v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ce;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 404
    :goto_0
    return-void

    .line 401
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    .line 402
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cg;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/ch;

    .line 403
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cg;->remove(Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/common/ui/ce;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/common/ui/ce;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->k:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/common/ui/ce;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->h:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/common/ui/ce;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->i:Z

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/common/ui/ce;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->j:Z

    return v0
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/common/ui/ce;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->n:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/wallet/common/ui/ce;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->s:I

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/wallet/common/ui/ce;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->r:I

    return v0
.end method

.method static synthetic n(Lcom/google/android/gms/wallet/common/ui/ce;)I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->o:I

    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/cd;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    .line 299
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/checkout/inapp/proto/j;Z)V

    .line 209
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;Z)V
    .locals 2

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    const-string v1, "Set payment instruments before setting the selected payment instrument"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 216
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ce;->e()V

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    .line 219
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/cg;->a(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    .line 220
    if-ltz v0, :cond_0

    .line 221
    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/wallet/common/ui/ce;->a(IZ)V

    .line 225
    :goto_0
    return-void

    .line 223
    :cond_0
    const/4 v0, -0x1

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/wallet/common/ui/ce;->a(IZ)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 161
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->i:Z

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 167
    :cond_0
    return-void
.end method

.method public final a([I)V
    .locals 1

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->l:[I

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 189
    :cond_0
    return-void
.end method

.method public final a([Lcom/google/checkout/inapp/proto/j;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 228
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 229
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ch;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ce;->a:Lcom/google/android/gms/wallet/common/ui/cf;

    invoke-direct {v0, v3}, Lcom/google/android/gms/wallet/common/ui/ch;-><init>(Lcom/google/android/gms/wallet/common/ui/cf;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    if-eqz p1, :cond_0

    .line 231
    array-length v3, p1

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, p1, v0

    .line 232
    new-instance v5, Lcom/google/android/gms/wallet/common/ui/ch;

    invoke-direct {v5, v4}, Lcom/google/android/gms/wallet/common/ui/ch;-><init>(Lcom/google/checkout/inapp/proto/j;)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 235
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ch;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ce;->c:Lcom/google/android/gms/wallet/common/ui/cf;

    invoke-direct {v0, v3}, Lcom/google/android/gms/wallet/common/ui/ch;-><init>(Lcom/google/android/gms/wallet/common/ui/cf;)V

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 237
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ce;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ce;->d()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 247
    :goto_1
    return-void

    .line 243
    :cond_1
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->t:Ljava/util/ArrayList;

    .line 244
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cg;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->d:Landroid/content/Context;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/cg;-><init>(Lcom/google/android/gms/wallet/common/ui/ce;Landroid/content/Context;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_1
.end method

.method public final a([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 200
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->n:[Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 205
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 170
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->j:Z

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 174
    :cond_0
    return-void
.end method

.method public final b([I)V
    .locals 1

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->m:[I

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 197
    :cond_0
    return-void
.end method

.method final b(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 435
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->m:[I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->l:I

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 454
    :cond_0
    :goto_0
    return v0

    .line 438
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->l:[I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->c:I

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 441
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->n:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->n:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_2

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->n:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, v3, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v3, v3, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 448
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    .line 449
    goto :goto_0

    .line 451
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->h:Z

    if-eqz v2, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->d(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 452
    goto :goto_0
.end method

.method public final c(Z)V
    .locals 1

    .prologue
    .line 177
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->k:Z

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->e:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cg;->notifyDataSetChanged()V

    .line 181
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 460
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 461
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    if-eqz v1, :cond_0

    .line 462
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cd;->b(Lcom/google/checkout/inapp/proto/j;)V

    .line 464
    :cond_0
    return-void
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 289
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 257
    instance-of v0, v1, Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_4

    move-object v0, v1

    .line 258
    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 261
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->g:Lcom/google/checkout/inapp/proto/j;

    if-eq v1, v2, :cond_0

    .line 263
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->l:[I

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ce;->m:[I

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;[I[I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 266
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cd;->b(Lcom/google/checkout/inapp/proto/j;)V

    .line 269
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->g:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/checkout/inapp/proto/j;Z)V

    goto :goto_0

    .line 270
    :cond_2
    check-cast v1, Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->g:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/checkout/inapp/proto/j;Z)V

    goto :goto_0

    .line 275
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cd;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 276
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->g:Lcom/google/checkout/inapp/proto/j;

    .line 277
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ce;->e()V

    goto :goto_0

    .line 280
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->c:Lcom/google/android/gms/wallet/common/ui/cf;

    if-ne v1, v0, :cond_5

    .line 282
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->g:Lcom/google/checkout/inapp/proto/j;

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cd;->a()V

    goto :goto_0

    .line 286
    :cond_5
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ce;->g:Lcom/google/checkout/inapp/proto/j;

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/cd;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 293
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ce;->g:Lcom/google/checkout/inapp/proto/j;

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ce;->f:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cd;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 295
    return-void
.end method
