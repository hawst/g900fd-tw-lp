.class final Lcom/google/android/gms/wallet/ow/y;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/SignupActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/SignupActivity;)V
    .locals 0

    .prologue
    .line 1164
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/16 v1, 0x19b

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    .line 1408
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/j;J)V
    .locals 6

    .prologue
    .line 1388
    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lcom/google/android/gms/wallet/f;

    move-result-object v0

    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/wallet/ProxyCard;

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    iget v4, p1, Lcom/google/aa/b/a/a/a/a/j;->c:I

    iget v5, p1, Lcom/google/aa/b/a/a/a/a/j;->d:I

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/wallet/ProxyCard;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/wallet/ProxyCard;)Lcom/google/android/gms/wallet/f;

    move-result-object v0

    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->b(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;

    move-result-object v0

    iget-object v1, p1, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v1, v1, Lcom/google/t/a/b;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/f;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    .line 1398
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1399
    const-string v2, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1400
    const-string v0, "com.google.android.gms.wallet.EXTRA_BUYER_ACCOUNT"

    sget-object v2, Lcom/google/android/gms/wallet/w;->a:Landroid/accounts/Account;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 1402
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 1403
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/l;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1170
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    .line 1171
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 1174
    iget-object v4, p1, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    array-length v5, v4

    move v0, v3

    :goto_0
    if-ge v3, v5, :cond_7

    aget-object v6, v4, v3

    .line 1175
    iget v7, v6, Lcom/google/aa/b/a/a/a/a/r;->a:I

    .line 1176
    iget v6, v6, Lcom/google/aa/b/a/a/a/a/r;->b:I

    .line 1177
    const/16 v8, 0xfa

    if-eq v7, v8, :cond_0

    const/16 v8, 0x3ca

    if-ne v6, v8, :cond_2

    .line 1179
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 1382
    :cond_1
    :goto_1
    return-void

    .line 1182
    :cond_2
    packed-switch v7, :pswitch_data_0

    .line 1333
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto :goto_1

    .line 1184
    :pswitch_0
    packed-switch v6, :pswitch_data_1

    .line 1215
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto :goto_1

    .line 1186
    :pswitch_1
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_4

    .line 1189
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    .line 1191
    if-nez v0, :cond_3

    .line 1192
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    move v0, v1

    .line 1174
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1197
    :cond_4
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/f;->b()V

    .line 1198
    if-nez v0, :cond_3

    .line 1199
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move v0, v1

    .line 1201
    goto :goto_2

    .line 1207
    :pswitch_2
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/f;->b()V

    .line 1208
    if-nez v0, :cond_3

    .line 1209
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move v0, v1

    .line 1211
    goto :goto_2

    .line 1220
    :pswitch_3
    packed-switch v6, :pswitch_data_2

    .line 1251
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto :goto_1

    .line 1222
    :pswitch_4
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_5

    .line 1225
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d()V

    .line 1227
    if-nez v0, :cond_3

    .line 1228
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    move v0, v1

    .line 1230
    goto :goto_2

    .line 1233
    :cond_5
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/f;->d()V

    .line 1234
    if-nez v0, :cond_3

    .line 1235
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move v0, v1

    .line 1237
    goto :goto_2

    .line 1243
    :pswitch_5
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/f;->d()V

    .line 1244
    if-nez v0, :cond_3

    .line 1245
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move v0, v1

    .line 1247
    goto :goto_2

    .line 1256
    :pswitch_6
    packed-switch v6, :pswitch_data_3

    .line 1268
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 1259
    :pswitch_8
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/f;->h()V

    .line 1260
    if-nez v0, :cond_3

    .line 1261
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move v0, v1

    .line 1263
    goto/16 :goto_2

    .line 1273
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    sget v6, Lcom/google/android/gms/p;->Bo:I

    invoke-static {v0, v6}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    move v0, v1

    .line 1276
    goto/16 :goto_2

    .line 1278
    :pswitch_a
    packed-switch v6, :pswitch_data_4

    .line 1308
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 1280
    :pswitch_b
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v6, :cond_6

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getVisibility()I

    move-result v6

    if-nez v6, :cond_6

    .line 1283
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c()V

    .line 1284
    if-nez v0, :cond_3

    .line 1285
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->i:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    move v0, v1

    .line 1287
    goto/16 :goto_2

    .line 1290
    :cond_6
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/f;->c()V

    .line 1291
    if-nez v0, :cond_3

    .line 1292
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move v0, v1

    .line 1294
    goto/16 :goto_2

    .line 1300
    :pswitch_c
    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v6, v6, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/f;->c()V

    .line 1301
    if-nez v0, :cond_3

    .line 1302
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    move v0, v1

    .line 1304
    goto/16 :goto_2

    .line 1321
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    sget v6, Lcom/google/android/gms/p;->Bh:I

    invoke-static {v0, v6}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    move v0, v1

    .line 1324
    goto/16 :goto_2

    .line 1328
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    sget v6, Lcom/google/android/gms/p;->Bh:I

    invoke-static {v0, v6}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    move v0, v1

    .line 1331
    goto/16 :goto_2

    .line 1337
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    goto/16 :goto_1

    .line 1339
    :cond_8
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v0, v0

    if-ne v0, v1, :cond_9

    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    aget v0, v0, v3

    const/4 v4, 0x6

    if-ne v0, v4, :cond_9

    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    array-length v0, v0

    if-ne v0, v1, :cond_9

    .line 1344
    iget-object v4, p1, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    .line 1345
    iget-object v0, v4, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    aget-object v0, v0, v3

    .line 1347
    iget v0, v0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    packed-switch v0, :pswitch_data_5

    move-object v0, v2

    .line 1357
    :goto_3
    if-eqz v0, :cond_9

    .line 1358
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v2, v2, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/f;->a(Ljava/lang/String;)V

    .line 1360
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/SignupActivity;->g:Lcom/google/android/gms/wallet/common/ui/f;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/f;->g()Z

    .line 1361
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->c(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    .line 1362
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->d(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v4, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-ne v0, v1, :cond_1

    .line 1364
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v1, v4, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v1, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    goto/16 :goto_1

    .line 1349
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    sget v5, Lcom/google/android/gms/p;->Aj:I

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1353
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    sget v5, Lcom/google/android/gms/p;->Ai:I

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/ow/SignupActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 1370
    :cond_9
    iget-object v0, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v0, v0

    if-lez v0, :cond_a

    .line 1371
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1372
    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    invoke-static {v0, v3}, Lcom/google/android/gms/common/util/h;->a(Ljava/lang/StringBuilder;[I)V

    .line 1373
    const-string v3, "SignupActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unhandled required action(s): "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1374
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 1378
    :cond_a
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1379
    const-string v1, "EXTRA_SIGN_UP_RESULT"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 1380
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto/16 :goto_1

    .line 1182
    nop

    :pswitch_data_0
    .packed-switch 0xfb
        :pswitch_0
        :pswitch_3
        :pswitch_e
        :pswitch_6
        :pswitch_a
        :pswitch_9
        :pswitch_d
    .end packed-switch

    .line 1184
    :pswitch_data_1
    .packed-switch 0x3cb
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1220
    :pswitch_data_2
    .packed-switch 0x3cb
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 1256
    :pswitch_data_3
    .packed-switch 0x3cb
        :pswitch_8
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 1278
    :pswitch_data_4
    .packed-switch 0x3cb
        :pswitch_c
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 1347
    :pswitch_data_5
    .packed-switch 0xa
        :pswitch_f
        :pswitch_10
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/b/a/c;)V
    .locals 4

    .prologue
    .line 1422
    invoke-static {p1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;)I

    move-result v0

    .line 1423
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/SignupActivity;->f(Lcom/google/android/gms/wallet/ow/SignupActivity;)Z

    move-result v1

    invoke-static {p1, v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/b/a/c;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1424
    iget-object v1, p1, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    iget-object v1, v1, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    .line 1426
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    iget-object v3, v1, Lcom/google/aa/b/a/k;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/aa/b/a/k;->b:Ljava/lang/String;

    invoke-static {v2, v3, v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/ow/SignupActivity;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1435
    :goto_0
    return-void

    .line 1429
    :cond_0
    const/16 v1, 0x198

    if-ne v0, v1, :cond_1

    .line 1430
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 1432
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->b(Lcom/google/android/gms/wallet/ow/SignupActivity;I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 1413
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1417
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/SignupActivity;->e(Lcom/google/android/gms/wallet/ow/SignupActivity;)V

    .line 1418
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1439
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/y;->a:Lcom/google/android/gms/wallet/ow/SignupActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(ILandroid/content/Intent;)V

    .line 1440
    return-void
.end method
