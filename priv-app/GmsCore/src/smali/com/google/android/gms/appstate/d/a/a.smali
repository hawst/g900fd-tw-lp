.class public final Lcom/google/android/gms/appstate/d/a/a;
.super Lcom/google/android/gms/common/server/response/a;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 48
    sput-object v0, Lcom/google/android/gms/appstate/d/a/a;->b:Ljava/util/HashMap;

    const-string v1, "currentStateVersion"

    const-string v2, "local_version"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    sget-object v0, Lcom/google/android/gms/appstate/d/a/a;->b:Ljava/util/HashMap;

    const-string v1, "data"

    const-string v2, "local_data"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->i(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/android/gms/appstate/d/a/a;->b:Ljava/util/HashMap;

    const-string v1, "stateKey"

    const-string v2, "key"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/gms/appstate/d/a/a;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v1, "key"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method
