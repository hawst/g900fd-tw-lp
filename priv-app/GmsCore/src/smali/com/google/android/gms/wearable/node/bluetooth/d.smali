.class public final Lcom/google/android/gms/wearable/node/bluetooth/d;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wearable/node/bluetooth/h;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

.field private final d:Landroid/bluetooth/BluetoothDevice;

.field private final e:Lcom/google/android/gms/wearable/node/a/c;

.field private final f:Ljava/util/concurrent/locks/Lock;

.field private final g:Ljava/util/concurrent/locks/Condition;

.field private final h:Landroid/os/PowerManager$WakeLock;

.field private volatile i:Z

.field private volatile j:Z

.field private volatile k:Z

.field private l:Z

.field private final m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private volatile n:Lcom/google/android/gms/wearable/node/be;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/google/android/gms/wearable/ConnectionConfiguration;Lcom/google/android/gms/wearable/node/bluetooth/h;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 56
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->g:Ljava/util/concurrent/locks/Condition;

    .line 60
    iput-boolean v8, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z

    .line 61
    iput-boolean v8, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->j:Z

    .line 62
    iput-boolean v8, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->k:Z

    .line 64
    iput-boolean v8, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->l:Z

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->b:Landroid/content/Context;

    .line 78
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->d:Landroid/bluetooth/BluetoothDevice;

    .line 79
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/ConnectionConfiguration;

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    .line 80
    iput-object p4, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WearableBtClientThread"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-super {p0, v0}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 84
    new-instance v1, Lcom/google/android/gms/wearable/node/a/c;

    const/16 v2, 0x3e8

    sget-object v0, Lcom/google/android/gms/wearable/a/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lcom/google/android/gms/wearable/a/b;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/wearable/a/b;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/wearable/node/a/c;-><init>(IIJJ)V

    iput-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->e:Lcom/google/android/gms/wearable/node/a/c;

    .line 89
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "WearableBluetooth["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v9, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->h:Landroid/os/PowerManager$WakeLock;

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0, v8}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 93
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v9}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 94
    return-void
.end method

.method private static a(ILjava/lang/String;Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 324
    if-eqz p2, :cond_0

    .line 325
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    .line 329
    :goto_0
    return-void

    .line 327
    :cond_0
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/android/gms/wearable/node/a/b;->a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 137
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 138
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 140
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 142
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 144
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private d()Landroid/bluetooth/BluetoothSocket;
    .locals 3

    .prologue
    .line 234
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->d:Landroid/bluetooth/BluetoothDevice;

    sget-object v1, Lcom/google/android/gms/wearable/node/bluetooth/e;->a:Ljava/util/UUID;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothDevice;->createRfcommSocketToServiceRecord(Ljava/util/UUID;)Landroid/bluetooth/BluetoothSocket;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Caught RuntimeException when creating the RFComm socket."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private e()V
    .locals 13

    .prologue
    const/high16 v8, 0x8000000

    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->e:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->b()J

    move-result-wide v2

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->e:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/a/c;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->l:Z

    if-nez v0, :cond_0

    .line 248
    iput-boolean v10, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->l:Z

    .line 249
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->b:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->f()Landroid/content/Intent;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->a()Ljava/lang/String;

    move-result-object v7

    sget-object v0, Lcom/google/android/gms/wearable/a/b;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v4, v10, v5, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iget-object v4, v1, Lcom/google/android/gms/wearable/node/a/b;->b:Landroid/app/NotificationManager;

    invoke-static {v6}, Lcom/google/android/gms/wearable/node/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/support/v4/app/bk;

    iget-object v8, v1, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    invoke-direct {v6, v8}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v6

    iget-object v8, v1, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v9, Lcom/google/android/gms/p;->Di:I

    invoke-virtual {v8, v9}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v6

    iput-object v0, v6, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v0, Lcom/google/android/gms/h;->cc:I

    invoke-virtual {v6, v0}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, v12, v11}, Landroid/support/v4/app/bk;->a(IZ)V

    iput v11, v0, Landroid/support/v4/app/bk;->j:I

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v1, v1, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v7, Lcom/google/android/gms/p;->Dh:I

    invoke-virtual {v1, v7}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v4, v5, v11, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 253
    :cond_0
    const-string v0, "Waiting %.1f seconds to retry connection"

    new-array v1, v10, [Ljava/lang/Object;

    long-to-float v4, v2

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v1, v11

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v10, v0, v1}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->b:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 256
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 257
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 260
    const/4 v1, 0x2

    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->b:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->f()Landroid/content/Intent;

    move-result-object v6

    const/high16 v7, 0x8000000

    invoke-static {v4, v5, v6, v7}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 263
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const-string v0, "WearableBluetooth"

    const-string v1, "Releasing the WakeLock after setting retry alarm."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 267
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->j:Z

    if-nez v0, :cond_2

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->g:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 271
    :catch_0
    move-exception v0

    .line 273
    const/4 v1, 0x1

    :try_start_1
    const-string v2, "Retry interrupted."

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 274
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 277
    :goto_1
    return-void

    .line 270
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->j:Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method private f()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 287
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wearable.node.bluetooth.RETRY_CONNECTION"

    new-instance v2, Landroid/net/Uri$Builder;

    invoke-direct {v2}, Landroid/net/Uri$Builder;-><init>()V

    const-string v3, "content"

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v3}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 293
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v1}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/wearable/node/a/b;->b:Landroid/app/NotificationManager;

    invoke-static {v1}, Lcom/google/android/gms/wearable/node/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 295
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->n:Lcom/google/android/gms/wearable/node/be;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    const-string v1, "user requested"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/be;->a(Ljava/lang/String;)V

    .line 101
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->k:Z

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->interrupt()V

    .line 103
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 298
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z

    if-eqz v0, :cond_1

    .line 299
    const-string v0, "WearableBluetooth"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 300
    const-string v0, "WearableBluetooth"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring connection retry; already connected for this device: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 307
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->j:Z

    .line 308
    const-string v0, "WearableBluetooth"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    const-string v0, "WearableBluetooth"

    const-string v1, "Acquiring the WakeLock to signal a connection retry."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->g:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->f:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final run()V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 109
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->g()V

    .line 110
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->k:Z

    if-nez v0, :cond_10

    .line 111
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Connecting to \""

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->d()Landroid/bluetooth/BluetoothSocket;
    :try_end_1
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_1 .. :try_end_1} :catch_10
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    :try_start_2
    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->connect()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_2 .. :try_end_2} :catch_e
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_a
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    const/4 v2, 0x1

    :try_start_3
    iput-boolean v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->e:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/a/c;->a()V

    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->g()V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->l:Z

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a()V

    const-string v2, "Connected, running sync loop"

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    const-string v2, "WearableBluetooth"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "WearableBluetooth"

    const-string v3, "Releasing WakeLock before calling handleConnection."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->a()Lcom/google/android/gms/wearable/node/bb;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/bb;->a()Lcom/google/android/gms/wearable/node/be;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->n:Lcom/google/android/gms/wearable/node/be;

    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->a()Lcom/google/android/gms/wearable/node/bb;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/io/InputStream;)Ljava/io/InputStream;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/io/OutputStream;)Ljava/io/OutputStream;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->n:Lcom/google/android/gms/wearable/node/be;

    iget-object v6, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/gms/wearable/node/bb;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/wearable/node/be;Lcom/google/android/gms/wearable/ConnectionConfiguration;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    const-string v3, "Socket closed."

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/lang/String;)V

    const-string v2, "Socket closed"

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_3 .. :try_end_3} :catch_e
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_c
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_a
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_8
    .catchall {:try_start_3 .. :try_end_3} :catchall_4

    const/4 v2, 0x0

    :try_start_4
    iput-boolean v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_2

    :try_start_5
    const-string v2, "WearableBluetooth"

    const-string v3, "onSocketReady returned, closing socket"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_11
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :goto_1
    :try_start_6
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "WearableBluetooth"

    const-string v2, "Acquiring the WakeLock so that an alarm can be set."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z

    .line 116
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->k:Z

    if-nez v0, :cond_0

    .line 118
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->e()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_0

    .line 128
    :catchall_0
    move-exception v0

    const-string v2, "Android Wear BluetoothThread finished"

    invoke-static {v7, v2, v1}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 124
    :try_start_7
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 126
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 127
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    throw v0

    .line 114
    :catch_0
    move-exception v2

    :try_start_8
    new-instance v3, Lcom/google/k/f/n;

    invoke-direct {v3}, Lcom/google/k/f/n;-><init>()V

    new-instance v4, Lcom/google/k/f/m;

    invoke-direct {v4}, Lcom/google/k/f/m;-><init>()V

    iput-object v4, v3, Lcom/google/k/f/n;->f:Lcom/google/k/f/m;

    iget-object v4, v3, Lcom/google/k/f/n;->f:Lcom/google/k/f/m;

    const/4 v5, 0x4

    iput v5, v4, Lcom/google/k/f/m;->a:I

    sget-object v4, Lcom/google/android/gms/wearable/b/a;->a:Lcom/google/android/gms/wearable/b/a;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/wearable/b/a;->a(Lcom/google/k/f/n;)V

    throw v2
    :try_end_8
    .catch Lcom/google/android/gms/wearable/node/bk; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/google/android/gms/wearable/node/i; {:try_start_8 .. :try_end_8} :catch_e
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_c
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_a
    .catch Ljava/lang/RuntimeException; {:try_start_8 .. :try_end_8} :catch_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_4

    :catch_1
    move-exception v2

    :goto_3
    :try_start_9
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    const-string v3, "Wire protocol version mismatch!"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/lang/String;)V

    const-string v2, "Error: wire protocol version mismatch"

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->e:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/a/c;->d()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_4

    const/4 v2, 0x0

    :try_start_a
    iput-boolean v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    if-eqz v0, :cond_4

    :try_start_b
    const-string v2, "WearableBluetooth"

    const-string v3, "onSocketReady returned, closing socket"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_f
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_4
    :goto_4
    :try_start_c
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string v0, "WearableBluetooth"

    const-string v2, "Acquiring the WakeLock so that an alarm can be set."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v0, v1

    :goto_5
    :try_start_d
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    const-string v3, "Connection attempted from incorrect client"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/lang/String;)V

    const-string v2, "Error: Connection attempted from incorrect client"

    const/4 v3, 0x4

    const/4 v4, 0x0

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->e:Lcom/google/android/gms/wearable/node/a/c;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/node/a/c;->d()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_4

    const/4 v2, 0x0

    :try_start_e
    iput-boolean v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    if-eqz v0, :cond_6

    :try_start_f
    const-string v2, "WearableBluetooth"

    const-string v3, "onSocketReady returned, closing socket"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_d
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    :cond_6
    :goto_6
    :try_start_10
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "WearableBluetooth"

    const-string v2, "Acquiring the WakeLock so that an alarm can be set."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0

    goto/16 :goto_2

    :catch_3
    move-exception v0

    move-object v2, v1

    :goto_7
    :try_start_11
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->a:Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wearable/node/bluetooth/h;->a(Ljava/io/IOException;)Ljava/io/IOException;

    move-result-object v0

    const/4 v3, 0x1

    const-string v4, "Error writing to device"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_5

    const/4 v0, 0x0

    :try_start_12
    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_0

    if-eqz v2, :cond_8

    :try_start_13
    const-string v0, "WearableBluetooth"

    const-string v3, "onSocketReady returned, closing socket"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_13
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_b
    .catchall {:try_start_13 .. :try_end_13} :catchall_0

    :cond_8
    :goto_8
    :try_start_14
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "WearableBluetooth"

    const-string v2, "Acquiring the WakeLock so that an alarm can be set."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_0

    goto/16 :goto_2

    :catch_4
    move-exception v0

    move-object v2, v1

    :goto_9
    const/4 v3, 0x1

    :try_start_15
    const-string v4, "Connection interrupted."

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_5

    const/4 v0, 0x0

    :try_start_16
    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z
    :try_end_16
    .catchall {:try_start_16 .. :try_end_16} :catchall_0

    if-eqz v2, :cond_a

    :try_start_17
    const-string v0, "WearableBluetooth"

    const-string v3, "onSocketReady returned, closing socket"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_17
    .catch Ljava/io/IOException; {:try_start_17 .. :try_end_17} :catch_9
    .catchall {:try_start_17 .. :try_end_17} :catchall_0

    :cond_a
    :goto_a
    :try_start_18
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "WearableBluetooth"

    const-string v2, "Acquiring the WakeLock so that an alarm can be set."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z
    :try_end_18
    .catchall {:try_start_18 .. :try_end_18} :catchall_0

    goto/16 :goto_2

    :catch_5
    move-exception v0

    move-object v2, v1

    :goto_b
    const/4 v3, 0x1

    :try_start_19
    const-string v4, "Unexpected runtime exception"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_5

    const/4 v0, 0x0

    :try_start_1a
    iput-boolean v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z
    :try_end_1a
    .catchall {:try_start_1a .. :try_end_1a} :catchall_0

    if-eqz v2, :cond_c

    :try_start_1b
    const-string v0, "WearableBluetooth"

    const-string v3, "onSocketReady returned, closing socket"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_7
    .catchall {:try_start_1b .. :try_end_1b} :catchall_0

    :cond_c
    :goto_c
    :try_start_1c
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v0, "WearableBluetooth"

    const-string v2, "Acquiring the WakeLock so that an alarm can be set."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    move-object v2, v1

    :goto_d
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->i:Z
    :try_end_1c
    .catchall {:try_start_1c .. :try_end_1c} :catchall_0

    if-eqz v2, :cond_e

    :try_start_1d
    const-string v3, "WearableBluetooth"

    const-string v4, "onSocketReady returned, closing socket"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_1d
    .catch Ljava/io/IOException; {:try_start_1d .. :try_end_1d} :catch_6
    .catchall {:try_start_1d .. :try_end_1d} :catchall_0

    :cond_e
    :goto_e
    :try_start_1e
    const-string v2, "WearableBluetooth"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_f

    const-string v2, "WearableBluetooth"

    const-string v3, "Acquiring the WakeLock so that an alarm can be set."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_f
    invoke-direct {p0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->c()Z

    throw v0
    :try_end_1e
    .catchall {:try_start_1e .. :try_end_1e} :catchall_0

    .line 122
    :cond_10
    const-string v0, "Android Wear BluetoothThread finished"

    invoke-static {v7, v0, v1}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a(ILjava/lang/String;Ljava/lang/Throwable;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    monitor-enter v1

    .line 124
    :try_start_1f
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/d;->h:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 127
    monitor-exit v1
    :try_end_1f
    .catchall {:try_start_1f .. :try_end_1f} :catchall_2

    return-void

    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_6
    move-exception v2

    goto :goto_e

    .line 114
    :catchall_4
    move-exception v2

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    goto :goto_d

    :catchall_5
    move-exception v0

    goto :goto_d

    :catch_7
    move-exception v0

    goto :goto_c

    :catch_8
    move-exception v2

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    goto :goto_b

    :catch_9
    move-exception v0

    goto/16 :goto_a

    :catch_a
    move-exception v2

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    goto/16 :goto_9

    :catch_b
    move-exception v0

    goto/16 :goto_8

    :catch_c
    move-exception v2

    move-object v8, v2

    move-object v2, v0

    move-object v0, v8

    goto/16 :goto_7

    :catch_d
    move-exception v0

    goto/16 :goto_6

    :catch_e
    move-exception v2

    goto/16 :goto_5

    :catch_f
    move-exception v0

    goto/16 :goto_4

    :catch_10
    move-exception v0

    move-object v0, v1

    goto/16 :goto_3

    :catch_11
    move-exception v0

    goto/16 :goto_1
.end method
