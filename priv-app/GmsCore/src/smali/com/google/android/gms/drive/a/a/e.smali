.class public final Lcom/google/android/gms/drive/a/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/drive/g/aw;

.field private final b:Lcom/google/android/gms/drive/database/r;

.field private final c:Lcom/google/android/gms/drive/g/n;

.field private final d:Lcom/google/android/gms/drive/a/a/j;

.field private final e:Lcom/google/android/gms/drive/b/d;

.field private final f:Lcom/google/android/gms/drive/a/a/m;

.field private final g:Lcom/google/android/gms/drive/a/a/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/m;Lcom/google/android/gms/drive/a/a/k;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/aw;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->a:Lcom/google/android/gms/drive/g/aw;

    .line 65
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->b:Lcom/google/android/gms/drive/database/r;

    .line 66
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->c:Lcom/google/android/gms/drive/g/n;

    .line 67
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/j;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->d:Lcom/google/android/gms/drive/a/a/j;

    .line 68
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/d;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->e:Lcom/google/android/gms/drive/b/d;

    .line 69
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/m;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->f:Lcom/google/android/gms/drive/a/a/m;

    .line 70
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/k;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    .line 71
    return-void
.end method

.method private a(Z)V
    .locals 7

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/e;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/e;->e:Lcom/google/android/gms/drive/b/d;

    iget-object v6, p0, Lcom/google/android/gms/drive/a/a/e;->d:Lcom/google/android/gms/drive/a/a/j;

    move v1, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;ZIZLcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/j;)V

    .line 188
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 76
    const-string v0, "ApplyOnServerRunnable"

    const-string v1, "Running"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->c:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->c()Lcom/google/android/gms/drive/g/o;
    :try_end_0
    .catch Lcom/google/android/gms/drive/a/a/g; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/drive/a/a/f; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_4

    move-result-object v7

    :try_start_1
    const-string v0, "ApplyOnServerRunnable"

    const-string v1, "Applying action on server: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/a/c;->a(Lcom/google/android/gms/drive/g/aw;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/e;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/e;->e:Lcom/google/android/gms/drive/b/d;

    iget-object v6, p0, Lcom/google/android/gms/drive/a/a/e;->d:Lcom/google/android/gms/drive/a/a/j;

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;ZIZLcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/j;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/a/c;->b(Lcom/google/android/gms/drive/g/aw;)V
    :try_end_1
    .catch Lcom/google/android/gms/drive/a/i; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/gms/drive/a/v; {:try_start_1 .. :try_end_1} :catch_5
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_1 .. :try_end_1} :catch_6
    .catch Lcom/google/android/gms/drive/a/ab; {:try_start_1 .. :try_end_1} :catch_7
    .catch Lcom/google/android/gms/drive/a/f; {:try_start_1 .. :try_end_1} :catch_8
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_9
    .catch Lcom/google/android/gms/drive/a/a/g; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/drive/a/a/f; {:try_start_1 .. :try_end_1} :catch_3

    .line 79
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->f:Lcom/google/android/gms/drive/a/a/m;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;)V

    .line 99
    :goto_1
    return-void

    .line 78
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/e;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/e;->e:Lcom/google/android/gms/drive/b/d;

    iget-object v6, p0, Lcom/google/android/gms/drive/a/a/e;->d:Lcom/google/android/gms/drive/a/a/j;

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;ZIZLcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/j;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/a/c;->b(Lcom/google/android/gms/drive/g/aw;)V
    :try_end_2
    .catch Lcom/google/android/gms/drive/a/a/g; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/drive/a/a/f; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_0

    .line 81
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->f:Lcom/google/android/gms/drive/a/a/m;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    invoke-virtual {v0, v1, v9}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;Z)V

    goto :goto_1

    .line 78
    :catch_2
    move-exception v0

    move-object v1, v0

    :try_start_3
    const-string v0, "ApplyOnServerRunnable"

    const-string v2, "IOException during apply on server: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->c:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->c()Lcom/google/android/gms/drive/g/o;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/drive/g/o;->a:Lcom/google/android/gms/drive/g/o;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/g/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/a/a/f;

    const-string v1, "Applying action on server failed (Disconnect)."

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/a/a/f;-><init>(Ljava/lang/String;B)V

    throw v0
    :try_end_3
    .catch Lcom/google/android/gms/drive/a/a/g; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/drive/a/a/f; {:try_start_3 .. :try_end_3} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_4

    .line 82
    :catch_3
    move-exception v0

    .line 83
    const-string v1, "ApplyOnServerRunnable"

    const-string v2, "Action skipped because device was offline"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->f:Lcom/google/android/gms/drive/a/a/m;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    invoke-virtual {v0, v1, v8}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;Z)V

    goto :goto_1

    .line 78
    :cond_0
    :try_start_4
    invoke-virtual {v0, v7}, Lcom/google/android/gms/drive/g/o;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/drive/a/a/g;

    const-string v1, "Applying action on server failed (Connection changed)"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/a/a/g;-><init>(Ljava/lang/String;B)V

    throw v0
    :try_end_4
    .catch Lcom/google/android/gms/drive/a/a/g; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/gms/drive/a/a/f; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_4

    .line 85
    :catch_4
    move-exception v0

    .line 91
    const-string v1, "ApplyOnServerRunnable"

    const-string v2, "Unchecked exception handling action : %s"

    new-array v3, v9, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    aput-object v4, v3, v8

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 94
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->f:Lcom/google/android/gms/drive/a/a/m;

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/e;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v1

    const-string v2, "ApplyOnServerRunnable"

    const-string v3, "Unchecked exception handling action"

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_1

    .line 78
    :cond_1
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/e;->g:Lcom/google/android/gms/drive/a/a/k;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/a/k;->a:Lcom/google/android/gms/drive/database/model/be;

    iget v2, v0, Lcom/google/android/gms/drive/database/model/be;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/android/gms/drive/database/model/be;->c:I

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/be;->i()V

    iget v2, v0, Lcom/google/android/gms/drive/database/model/be;->c:I

    sget-object v0, Lcom/google/android/gms/drive/ai;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v2, v0, :cond_2

    new-instance v0, Lcom/google/android/gms/drive/a/a/g;

    const-string v1, "Applying action on server failed (Increased attempts)"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/a/a/g;-><init>(Ljava/lang/String;B)V

    throw v0

    :cond_2
    const-string v0, "ApplyOnServerRunnable"

    const-string v2, "Too many retries for action. Undoing."

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/e;->a(Z)V

    goto/16 :goto_0

    :catch_5
    move-exception v0

    const-string v0, "ApplyOnServerRunnable"

    const-string v1, "Action is not yet ready to apply on server. Should never happen."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/e;->a(Z)V

    goto/16 :goto_0

    :catch_6
    move-exception v0

    const-string v1, "ApplyOnServerRunnable"

    const-string v2, "Dropped action locally because its entry was deleted."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/e;->a(Z)V

    goto/16 :goto_0

    :catch_7
    move-exception v0

    const-string v1, "ApplyOnServerRunnable"

    const-string v2, "Dropped action locally because its parent entry was deleted."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/e;->a(Z)V

    goto/16 :goto_0

    :catch_8
    move-exception v0

    const-string v1, "ApplyOnServerRunnable"

    const-string v2, "Undoing action because the app is no longer authorized."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/e;->a(Z)V

    goto/16 :goto_0

    :catch_9
    move-exception v0

    const-string v1, "ApplyOnServerRunnable"

    const-string v2, "Unchecked exception while applying action. Undoing."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/e;->a(Z)V
    :try_end_5
    .catch Lcom/google/android/gms/drive/a/a/g; {:try_start_5 .. :try_end_5} :catch_1
    .catch Lcom/google/android/gms/drive/a/a/f; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_0
.end method
