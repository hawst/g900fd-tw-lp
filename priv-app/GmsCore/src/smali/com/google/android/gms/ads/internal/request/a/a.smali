.class public final Lcom/google/android/gms/ads/internal/request/a/a;
.super Lcom/google/android/gms/ads/internal/request/aa;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/ads/internal/request/a/a;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/ads/internal/n/a;

.field private final e:Lcom/google/android/gms/ads/internal/j/a;

.field private final f:Lcom/google/android/gms/ads/internal/d/a;

.field private final g:Lcom/google/android/gms/ads/internal/i/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/request/a/a;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/j/a;Lcom/google/android/gms/ads/internal/n/a;)V
    .locals 5

    .prologue
    .line 425
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/request/aa;-><init>()V

    .line 426
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/a/a;->c:Landroid/content/Context;

    .line 427
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/request/a/a;->d:Lcom/google/android/gms/ads/internal/n/a;

    .line 428
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/a/a;->e:Lcom/google/android/gms/ads/internal/j/a;

    .line 429
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/a/a;->f:Lcom/google/android/gms/ads/internal/d/a;

    .line 430
    new-instance v0, Lcom/google/android/gms/ads/internal/i/a;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    const v3, 0x6768a8

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;-><init>(IZ)V

    iget-object v3, p2, Lcom/google/android/gms/ads/internal/d/a;->a:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/ads/internal/request/a/i;

    invoke-direct {v4, p0}, Lcom/google/android/gms/ads/internal/request/a/i;-><init>(Lcom/google/android/gms/ads/internal/request/a/a;)V

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/ads/internal/i/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/a/e;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/a;->g:Lcom/google/android/gms/ads/internal/i/a;

    .line 443
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/n/a;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 101
    const-string v0, "Starting ad request from service."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 104
    new-instance v1, Lcom/google/android/gms/ads/internal/request/a/p;

    invoke-direct {v1, p0}, Lcom/google/android/gms/ads/internal/request/a/p;-><init>(Landroid/content/Context;)V

    .line 108
    iget v0, v1, Lcom/google/android/gms/ads/internal/request/a/p;->l:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 109
    const-string v0, "Device is offline."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 110
    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    .line 244
    :goto_0
    return-object v0

    .line 114
    :cond_0
    iget-object v2, p4, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->t:Ljava/lang/String;

    .line 115
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 116
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v2

    .line 118
    :cond_1
    new-instance v8, Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v0, p4, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->f:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-direct {v8, v2, v0}, Lcom/google/android/gms/ads/internal/request/a/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    iget-object v0, p4, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    .line 125
    iget-object v0, p4, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/client/AdRequestParcel;->c:Landroid/os/Bundle;

    const-string v3, "_ad"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_2

    .line 127
    invoke-static {p0, p4, v0}, Lcom/google/android/gms/ads/internal/request/a/j;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Ljava/lang/String;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-result-object v0

    goto :goto_0

    .line 132
    :cond_2
    iget-object v9, p2, Lcom/google/android/gms/ads/internal/d/a;->a:Ljava/lang/String;

    .line 135
    iget-object v3, p2, Lcom/google/android/gms/ads/internal/d/a;->b:Ljava/lang/String;

    .line 136
    iget-object v4, p2, Lcom/google/android/gms/ads/internal/d/a;->c:Ljava/lang/String;

    .line 137
    iget-object v5, p2, Lcom/google/android/gms/ads/internal/d/a;->d:Ljava/lang/String;

    .line 139
    iget-object v6, p2, Lcom/google/android/gms/ads/internal/d/a;->g:Ljava/lang/String;

    .line 142
    iget v7, p2, Lcom/google/android/gms/ads/internal/d/a;->e:I

    move-object v0, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/request/a/j;->a(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/a/p;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 146
    if-nez v0, :cond_3

    .line 147
    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    invoke-direct {v0, v10}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    goto :goto_0

    .line 150
    :cond_3
    iget-boolean v1, p2, Lcom/google/android/gms/ads/internal/d/a;->f:Z

    if-eqz v1, :cond_4

    .line 151
    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/ads/internal/request/a/b;

    invoke-direct {v2, p1, v8, v0}, Lcom/google/android/gms/ads/internal/request/a/b;-><init>(Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/request/a/k;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 218
    :goto_1
    :try_start_0
    iget-object v0, v8, Lcom/google/android/gms/ads/internal/request/a/k;->d:Lcom/google/android/gms/ads/internal/util/j;

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/request/a/o;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    if-nez v0, :cond_5

    .line 224
    :try_start_1
    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 244
    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/ads/internal/request/a/f;

    invoke-direct {v2, v8, p2, p1}, Lcom/google/android/gms/ads/internal/request/a/f;-><init>(Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/i/a;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 179
    :cond_4
    new-instance v4, Lcom/google/android/gms/ads/internal/request/a/h;

    invoke-direct {v4, v0}, Lcom/google/android/gms/ads/internal/request/a/h;-><init>(Ljava/lang/String;)V

    .line 181
    sget-object v6, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/ads/internal/request/a/e;

    move-object v1, p0

    move-object v2, p4

    move-object v3, v8

    move-object v5, v9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/request/a/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/p/e;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    .line 221
    :catch_0
    move-exception v0

    :try_start_2
    new-instance v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 244
    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/ads/internal/request/a/f;

    invoke-direct {v2, v8, p2, p1}, Lcom/google/android/gms/ads/internal/request/a/f;-><init>(Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/i/a;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    .line 226
    :cond_5
    :try_start_3
    iget v1, v0, Lcom/google/android/gms/ads/internal/request/a/o;->g:I

    const/4 v2, -0x2

    if-eq v1, v2, :cond_6

    .line 227
    new-instance v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/a/o;->g:I

    invoke-direct {v1, v0}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 244
    sget-object v0, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/ads/internal/request/a/f;

    invoke-direct {v2, v8, p2, p1}, Lcom/google/android/gms/ads/internal/request/a/f;-><init>(Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/i/a;)V

    invoke-virtual {v0, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object v0, v1

    goto/16 :goto_0

    .line 230
    :cond_6
    const/4 v1, 0x0

    .line 231
    :try_start_4
    iget-boolean v2, v0, Lcom/google/android/gms/ads/internal/request/a/o;->d:Z

    if-eqz v2, :cond_7

    .line 232
    iget-object v1, p4, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->g:Landroid/content/pm/PackageInfo;

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {p3, v1}, Lcom/google/android/gms/ads/internal/n/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 236
    :cond_7
    iget-object v2, p4, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/a/o;->f:Ljava/lang/String;

    invoke-static {p0, v2, v3, v1, v0}, Lcom/google/android/gms/ads/internal/request/a/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/request/a/o;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    .line 244
    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/ads/internal/request/a/f;

    invoke-direct {v2, v8, p2, p1}, Lcom/google/android/gms/ads/internal/request/a/f;-><init>(Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/i/a;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/ads/internal/request/a/f;

    invoke-direct {v2, v8, p2, p1}, Lcom/google/android/gms/ads/internal/request/a/f;-><init>(Lcom/google/android/gms/ads/internal/request/a/k;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/i/a;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    throw v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/ads/internal/request/a/o;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    .locals 24

    .prologue
    .line 326
    :try_start_0
    new-instance v23, Lcom/google/android/gms/ads/internal/request/a/n;

    invoke-direct/range {v23 .. v23}, Lcom/google/android/gms/ads/internal/request/a/n;-><init>()V

    .line 330
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "AdRequestServiceImpl: Sending request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 331
    new-instance v3, Ljava/net/URL;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 335
    const/4 v2, 0x0

    .line 336
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v16

    move-object v4, v3

    move v3, v2

    .line 338
    :goto_0
    invoke-virtual {v4}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v2

    check-cast v2, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 340
    const/4 v5, 0x0

    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5, v2}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/net/HttpURLConnection;)V

    .line 343
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 344
    const-string v5, "x-afma-drt-cookie"

    move-object/from16 v0, p3

    invoke-virtual {v2, v5, v0}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_0
    if-eqz p4, :cond_1

    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/request/a/o;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 349
    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 350
    move-object/from16 v0, p4

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/request/a/o;->b:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B

    move-result-object v5

    .line 351
    array-length v6, v5

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 352
    new-instance v6, Ljava/io/BufferedOutputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 354
    invoke-virtual {v6, v5}, Ljava/io/BufferedOutputStream;->write([B)V

    .line 355
    invoke-virtual {v6}, Ljava/io/BufferedOutputStream;->close()V

    .line 362
    :cond_1
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    .line 363
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v6

    .line 366
    const/16 v7, 0xc8

    if-lt v5, v7, :cond_2

    const/16 v7, 0x12c

    if-ge v5, v7, :cond_2

    .line 367
    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v3

    .line 368
    new-instance v4, Ljava/io/InputStreamReader;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v7

    invoke-direct {v4, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-static {v4}, Lcom/google/android/gms/ads/internal/util/g;->a(Ljava/lang/Readable;)Ljava/lang/String;

    move-result-object v4

    .line 370
    invoke-static {v6, v4, v5}, Lcom/google/android/gms/ads/internal/request/a/a;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 371
    move-object/from16 v0, v23

    iput-object v3, v0, Lcom/google/android/gms/ads/internal/request/a/n;->b:Ljava/lang/String;

    move-object/from16 v0, v23

    iput-object v4, v0, Lcom/google/android/gms/ads/internal/request/a/n;->c:Ljava/lang/String;

    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/google/android/gms/ads/internal/request/a/n;->a(Ljava/util/Map;)V

    .line 372
    new-instance v3, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v0, v23

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/request/a/n;->b:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/request/a/n;->c:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v6, v0, Lcom/google/android/gms/ads/internal/request/a/n;->d:Ljava/util/List;

    move-object/from16 v0, v23

    iget-object v7, v0, Lcom/google/android/gms/ads/internal/request/a/n;->g:Ljava/util/List;

    move-object/from16 v0, v23

    iget-wide v8, v0, Lcom/google/android/gms/ads/internal/request/a/n;->h:J

    move-object/from16 v0, v23

    iget-boolean v10, v0, Lcom/google/android/gms/ads/internal/request/a/n;->i:Z

    move-object/from16 v0, v23

    iget-object v11, v0, Lcom/google/android/gms/ads/internal/request/a/n;->j:Ljava/util/List;

    move-object/from16 v0, v23

    iget-wide v12, v0, Lcom/google/android/gms/ads/internal/request/a/n;->k:J

    move-object/from16 v0, v23

    iget v14, v0, Lcom/google/android/gms/ads/internal/request/a/n;->l:I

    move-object/from16 v0, v23

    iget-object v15, v0, Lcom/google/android/gms/ads/internal/request/a/n;->a:Ljava/lang/String;

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/a/n;->e:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v23

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/a/n;->f:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/a/n;->m:Z

    move/from16 v20, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/a/n;->n:Z

    move/from16 v21, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/a/n;->o:Z

    move/from16 v22, v0

    move-object/from16 v0, v23

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/request/a/n;->p:Z

    move/from16 v23, v0

    invoke-direct/range {v3 .. v23}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;JZLjava/util/List;JILjava/lang/String;JLjava/lang/String;Ljava/lang/String;ZZZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 402
    :try_start_2
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 407
    :goto_1
    return-object v3

    .line 376
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/net/URL;->toString()Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v6, v4, v5}, Lcom/google/android/gms/ads/internal/request/a/a;->a(Ljava/util/Map;Ljava/lang/String;I)V

    .line 379
    const/16 v4, 0x12c

    if-lt v5, v4, :cond_4

    const/16 v4, 0x190

    if-ge v5, v4, :cond_4

    .line 380
    const-string v4, "Location"

    invoke-virtual {v2, v4}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 381
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 382
    const-string v3, "No location header to follow redirect."

    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 383
    new-instance v3, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 402
    :try_start_4
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1

    .line 404
    :catch_0
    move-exception v2

    .line 406
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while connecting to ad server: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 407
    new-instance v3, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v2, 0x2

    invoke-direct {v3, v2}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    goto :goto_1

    .line 386
    :cond_3
    :try_start_5
    new-instance v4, Ljava/net/URL;

    invoke-direct {v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 387
    add-int/lit8 v3, v3, 0x1

    .line 394
    const/4 v5, 0x5

    if-le v3, v5, :cond_5

    .line 395
    const-string v3, "Too many redirects."

    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 396
    new-instance v3, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 402
    :try_start_6
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_1

    .line 389
    :cond_4
    :try_start_7
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received error HTTP response code: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 390
    new-instance v3, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 402
    :try_start_8
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_1

    .line 400
    :cond_5
    :try_start_9
    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Lcom/google/android/gms/ads/internal/request/a/n;->a(Ljava/util/Map;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 402
    :try_start_a
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    goto/16 :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v3
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/j/a;Lcom/google/android/gms/ads/internal/n/a;)Lcom/google/android/gms/ads/internal/request/a/a;
    .locals 3

    .prologue
    .line 79
    sget-object v1, Lcom/google/android/gms/ads/internal/request/a/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/internal/request/a/a;->b:Lcom/google/android/gms/ads/internal/request/a/a;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/google/android/gms/ads/internal/request/a/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2, p1, p2, p3}, Lcom/google/android/gms/ads/internal/request/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/j/a;Lcom/google/android/gms/ads/internal/n/a;)V

    sput-object v0, Lcom/google/android/gms/ads/internal/request/a/a;->b:Lcom/google/android/gms/ads/internal/request/a/a;

    .line 89
    :cond_0
    sget-object v0, Lcom/google/android/gms/ads/internal/request/a/a;->b:Lcom/google/android/gms/ads/internal/request/a/a;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Ljava/util/Map;Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 289
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 290
    if-eqz p0, :cond_1

    .line 291
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 292
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "    "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 293
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 294
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "      "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    goto :goto_0

    .line 298
    :cond_1
    if-eqz p1, :cond_2

    .line 300
    const/4 v0, 0x0

    :goto_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const v2, 0x186a0

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 301
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit16 v2, v0, 0x3e8

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 300
    add-int/lit16 v0, v0, 0x3e8

    goto :goto_1

    .line 304
    :cond_2
    const-string v0, "    null"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 306
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  Response Code:\n    "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 308
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;
    .locals 4

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/a;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/a/a;->g:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/a/a;->f:Lcom/google/android/gms/ads/internal/d/a;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/request/a/a;->e:Lcom/google/android/gms/ads/internal/j/a;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/request/a/a;->d:Lcom/google/android/gms/ads/internal/n/a;

    invoke-static {v0, v1, v2, v3, p1}, Lcom/google/android/gms/ads/internal/request/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/d/a;Lcom/google/android/gms/ads/internal/n/a;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;)Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-result-object v0

    return-object v0
.end method
