.class public final enum Lcom/google/android/gms/drive/a/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/a/e;

.field public static final enum b:Lcom/google/android/gms/drive/a/e;

.field public static final enum c:Lcom/google/android/gms/drive/a/e;

.field public static final enum d:Lcom/google/android/gms/drive/a/e;

.field public static final enum e:Lcom/google/android/gms/drive/a/e;

.field public static final enum f:Lcom/google/android/gms/drive/a/e;

.field public static final enum g:Lcom/google/android/gms/drive/a/e;

.field public static final enum h:Lcom/google/android/gms/drive/a/e;

.field public static final enum i:Lcom/google/android/gms/drive/a/e;

.field public static final enum j:Lcom/google/android/gms/drive/a/e;

.field public static final enum k:Lcom/google/android/gms/drive/a/e;

.field public static final enum l:Lcom/google/android/gms/drive/a/e;

.field public static final enum m:Lcom/google/android/gms/drive/a/e;

.field public static final enum n:Lcom/google/android/gms/drive/a/e;

.field private static final o:Ljava/util/Map;

.field private static final synthetic r:[Lcom/google/android/gms/drive/a/e;


# instance fields
.field private final p:Ljava/lang/String;

.field private final q:Lcom/google/android/gms/drive/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 14
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "NULL"

    const-string v3, "null"

    new-instance v4, Lcom/google/android/gms/drive/a/aa;

    invoke-direct {v4}, Lcom/google/android/gms/drive/a/aa;-><init>()V

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->a:Lcom/google/android/gms/drive/a/e;

    .line 15
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "SET_APP_AUTH_STATE"

    const-string v3, "setAuthState"

    new-instance v4, Lcom/google/android/gms/drive/a/af;

    invoke-direct {v4}, Lcom/google/android/gms/drive/a/af;-><init>()V

    invoke-direct {v1, v2, v6, v3, v4}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->b:Lcom/google/android/gms/drive/a/e;

    .line 16
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "METADATA"

    const-string v3, "metadata"

    new-instance v4, Lcom/google/android/gms/drive/a/y;

    invoke-direct {v4}, Lcom/google/android/gms/drive/a/y;-><init>()V

    invoke-direct {v1, v2, v7, v3, v4}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->c:Lcom/google/android/gms/drive/a/e;

    .line 17
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "CONTENT_AND_METADATA"

    const-string v3, "contentAndMetadata"

    new-instance v4, Lcom/google/android/gms/drive/a/k;

    invoke-direct {v4}, Lcom/google/android/gms/drive/a/k;-><init>()V

    invoke-direct {v1, v2, v8, v3, v4}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->d:Lcom/google/android/gms/drive/a/e;

    .line 18
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "UNDO_CONTENT_AND_METADATA"

    const-string v3, "undoContentAndMetadata"

    new-instance v4, Lcom/google/android/gms/drive/a/al;

    invoke-direct {v4}, Lcom/google/android/gms/drive/a/al;-><init>()V

    invoke-direct {v1, v2, v9, v3, v4}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->e:Lcom/google/android/gms/drive/a/e;

    .line 20
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "CREATE_FILE"

    const/4 v3, 0x5

    const-string v4, "createFile"

    new-instance v5, Lcom/google/android/gms/drive/a/m;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/m;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->f:Lcom/google/android/gms/drive/a/e;

    .line 21
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "UNDO_CREATE_FILE"

    const/4 v3, 0x6

    const-string v4, "undoCreateFile"

    new-instance v5, Lcom/google/android/gms/drive/a/an;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/an;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->g:Lcom/google/android/gms/drive/a/e;

    .line 22
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "CREATE_SHORTCUT_FILE"

    const/4 v3, 0x7

    const-string v4, "createShortcutFile"

    new-instance v5, Lcom/google/android/gms/drive/a/q;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/q;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->h:Lcom/google/android/gms/drive/a/e;

    .line 23
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "UNDO_CREATE_SHORTCUT_FILE"

    const/16 v3, 0x8

    const-string v4, "undoCreateShortcutFile"

    new-instance v5, Lcom/google/android/gms/drive/a/ar;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/ar;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->i:Lcom/google/android/gms/drive/a/e;

    .line 25
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "CREATE_FOLDER"

    const/16 v3, 0x9

    const-string v4, "createFolder"

    new-instance v5, Lcom/google/android/gms/drive/a/o;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/o;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->j:Lcom/google/android/gms/drive/a/e;

    .line 26
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "UNDO_CREATE_FOLDER"

    const/16 v3, 0xa

    const-string v4, "undoCreateFolder"

    new-instance v5, Lcom/google/android/gms/drive/a/ap;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/ap;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->k:Lcom/google/android/gms/drive/a/e;

    .line 27
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "TRASH"

    const/16 v3, 0xb

    const-string v4, "trash"

    new-instance v5, Lcom/google/android/gms/drive/a/aj;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/aj;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->l:Lcom/google/android/gms/drive/a/e;

    .line 28
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "DELETE_FILE"

    const/16 v3, 0xc

    const-string v4, "deleteFile"

    new-instance v5, Lcom/google/android/gms/drive/a/s;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/s;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->m:Lcom/google/android/gms/drive/a/e;

    .line 29
    new-instance v1, Lcom/google/android/gms/drive/a/e;

    const-string v2, "SET_RESOURCE_PARENTS"

    const/16 v3, 0xd

    const-string v4, "setResourceParents"

    new-instance v5, Lcom/google/android/gms/drive/a/ah;

    invoke-direct {v5}, Lcom/google/android/gms/drive/a/ah;-><init>()V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->n:Lcom/google/android/gms/drive/a/e;

    .line 13
    const/16 v1, 0xe

    new-array v1, v1, [Lcom/google/android/gms/drive/a/e;

    sget-object v2, Lcom/google/android/gms/drive/a/e;->a:Lcom/google/android/gms/drive/a/e;

    aput-object v2, v1, v0

    sget-object v2, Lcom/google/android/gms/drive/a/e;->b:Lcom/google/android/gms/drive/a/e;

    aput-object v2, v1, v6

    sget-object v2, Lcom/google/android/gms/drive/a/e;->c:Lcom/google/android/gms/drive/a/e;

    aput-object v2, v1, v7

    sget-object v2, Lcom/google/android/gms/drive/a/e;->d:Lcom/google/android/gms/drive/a/e;

    aput-object v2, v1, v8

    sget-object v2, Lcom/google/android/gms/drive/a/e;->e:Lcom/google/android/gms/drive/a/e;

    aput-object v2, v1, v9

    const/4 v2, 0x5

    sget-object v3, Lcom/google/android/gms/drive/a/e;->f:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    sget-object v3, Lcom/google/android/gms/drive/a/e;->g:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    sget-object v3, Lcom/google/android/gms/drive/a/e;->h:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    sget-object v3, Lcom/google/android/gms/drive/a/e;->i:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    sget-object v3, Lcom/google/android/gms/drive/a/e;->j:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    sget-object v3, Lcom/google/android/gms/drive/a/e;->k:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0xb

    sget-object v3, Lcom/google/android/gms/drive/a/e;->l:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0xc

    sget-object v3, Lcom/google/android/gms/drive/a/e;->m:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/16 v2, 0xd

    sget-object v3, Lcom/google/android/gms/drive/a/e;->n:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    sput-object v1, Lcom/google/android/gms/drive/a/e;->r:[Lcom/google/android/gms/drive/a/e;

    .line 31
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/drive/a/e;->o:Ljava/util/Map;

    .line 34
    invoke-static {}, Lcom/google/android/gms/drive/a/e;->values()[Lcom/google/android/gms/drive/a/e;

    move-result-object v1

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 35
    sget-object v4, Lcom/google/android/gms/drive/a/e;->o:Ljava/util/Map;

    iget-object v5, v3, Lcom/google/android/gms/drive/a/e;->p:Ljava/lang/String;

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/drive/a/d;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    iput-object p3, p0, Lcom/google/android/gms/drive/a/e;->p:Ljava/lang/String;

    .line 44
    iput-object p4, p0, Lcom/google/android/gms/drive/a/e;->q:Lcom/google/android/gms/drive/a/d;

    .line 45
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/a/e;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/gms/drive/a/e;->o:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/e;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/a/e;
    .locals 1

    .prologue
    .line 13
    const-class v0, Lcom/google/android/gms/drive/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/a/e;
    .locals 1

    .prologue
    .line 13
    sget-object v0, Lcom/google/android/gms/drive/a/e;->r:[Lcom/google/android/gms/drive/a/e;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/a/e;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/a/e;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/a/d;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/drive/a/e;->q:Lcom/google/android/gms/drive/a/d;

    return-object v0
.end method
