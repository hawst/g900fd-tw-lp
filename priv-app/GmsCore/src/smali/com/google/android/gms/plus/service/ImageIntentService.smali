.class public final Lcom/google/android/gms/plus/service/ImageIntentService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static b:Lcom/google/android/gms/common/service/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "ImageIntentService"

    sget-object v1, Lcom/google/android/gms/plus/service/ImageIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 25
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    .locals 2

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/plus/service/ImageIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    .line 31
    const-string v0, "com.google.android.gms.plus.service.image.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/plus/service/ImageIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 33
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 34
    return-void
.end method
