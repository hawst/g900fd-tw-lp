.class public final Lcom/google/android/gms/location/places/al;
.super Lcom/google/android/gms/location/places/internal/e;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/location/places/x;

.field private final b:Lcom/google/android/gms/location/places/ai;

.field private final c:Lcom/google/android/gms/location/places/ag;

.field private final d:Lcom/google/android/gms/location/places/aj;

.field private final e:Lcom/google/android/gms/location/places/ak;

.field private final f:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ag;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    iput-object p1, p0, Lcom/google/android/gms/location/places/al;->a:Lcom/google/android/gms/location/places/x;

    invoke-direct {p0}, Lcom/google/android/gms/location/places/internal/e;-><init>()V

    .line 63
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    .line 64
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->d:Lcom/google/android/gms/location/places/aj;

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/location/places/al;->c:Lcom/google/android/gms/location/places/ag;

    .line 66
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->e:Lcom/google/android/gms/location/places/ak;

    .line 67
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->f:Landroid/content/Context;

    .line 68
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ag;B)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/location/places/al;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ag;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ai;Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/location/places/al;->a:Lcom/google/android/gms/location/places/x;

    invoke-direct {p0}, Lcom/google/android/gms/location/places/internal/e;-><init>()V

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    .line 48
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->d:Lcom/google/android/gms/location/places/aj;

    .line 49
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->c:Lcom/google/android/gms/location/places/ag;

    .line 50
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->e:Lcom/google/android/gms/location/places/ak;

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/location/places/al;->f:Landroid/content/Context;

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ai;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/location/places/al;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ai;Landroid/content/Context;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/aj;Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 54
    iput-object p1, p0, Lcom/google/android/gms/location/places/al;->a:Lcom/google/android/gms/location/places/x;

    invoke-direct {p0}, Lcom/google/android/gms/location/places/internal/e;-><init>()V

    .line 55
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/location/places/al;->d:Lcom/google/android/gms/location/places/aj;

    .line 57
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->c:Lcom/google/android/gms/location/places/ag;

    .line 58
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->e:Lcom/google/android/gms/location/places/ak;

    .line 59
    iput-object p3, p0, Lcom/google/android/gms/location/places/al;->f:Landroid/content/Context;

    .line 60
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/aj;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/location/places/al;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/aj;Landroid/content/Context;)V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ak;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 70
    iput-object p1, p0, Lcom/google/android/gms/location/places/al;->a:Lcom/google/android/gms/location/places/x;

    invoke-direct {p0}, Lcom/google/android/gms/location/places/internal/e;-><init>()V

    .line 71
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    .line 72
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->d:Lcom/google/android/gms/location/places/aj;

    .line 73
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->c:Lcom/google/android/gms/location/places/ag;

    .line 74
    iput-object p2, p0, Lcom/google/android/gms/location/places/al;->e:Lcom/google/android/gms/location/places/ak;

    .line 75
    iput-object v0, p0, Lcom/google/android/gms/location/places/al;->f:Landroid/content/Context;

    .line 76
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ak;B)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/location/places/al;-><init>(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/ak;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/location/places/al;->d:Lcom/google/android/gms/location/places/aj;

    if-eqz v3, :cond_2

    move v3, v1

    :goto_1
    if-eq v0, v3, :cond_3

    move v0, v1

    :goto_2
    const-string v3, "Only one of placeEstimator or placeReturner can be null"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    if-eqz v0, :cond_4

    .line 85
    :goto_3
    if-nez p1, :cond_6

    .line 86
    invoke-static {}, Lcom/google/android/gms/location/places/x;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x6

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-static {}, Lcom/google/android/gms/location/places/x;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onPlaceEstimated received null DataHolder: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/common/util/ar;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    if-eqz v1, :cond_5

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/ai;->b(Lcom/google/android/gms/common/api/Status;)V

    .line 113
    :goto_4
    return-void

    :cond_1
    move v0, v2

    .line 80
    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    .line 83
    goto :goto_3

    .line 93
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->d:Lcom/google/android/gms/location/places/aj;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/aj;->b(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_4

    .line 98
    :cond_6
    new-instance v2, Lcom/google/android/gms/location/places/i;

    const/16 v0, 0x64

    iget-object v3, p0, Lcom/google/android/gms/location/places/al;->f:Landroid/content/Context;

    invoke-direct {v2, p1, v0, v3}, Lcom/google/android/gms/location/places/i;-><init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/content/Context;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->a:Lcom/google/android/gms/location/places/x;

    invoke-static {v0, v2}, Lcom/google/android/gms/location/places/x;->a(Lcom/google/android/gms/location/places/x;Lcom/google/android/gms/location/places/i;)V

    .line 101
    if-eqz v1, :cond_7

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->b:Lcom/google/android/gms/location/places/ai;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/location/places/ai;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_4

    .line 104
    :cond_7
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 105
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/i;->c()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 106
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/i;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceLikelihood;

    .line 107
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceLikelihood;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    .line 109
    :cond_8
    invoke-virtual {v2}, Lcom/google/android/gms/location/places/i;->w_()V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->d:Lcom/google/android/gms/location/places/aj;

    new-instance v2, Lcom/google/android/gms/location/places/w;

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/location/places/w;-><init>(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/location/places/aj;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_4
.end method

.method public final b(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 117
    if-nez p1, :cond_1

    .line 118
    invoke-static {}, Lcom/google/android/gms/location/places/x;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-static {}, Lcom/google/android/gms/location/places/x;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAutocompletePrediction received null DataHolder: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/common/util/ar;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->c:Lcom/google/android/gms/location/places/ag;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/ag;->b(Lcom/google/android/gms/common/api/Status;)V

    .line 126
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->c:Lcom/google/android/gms/location/places/ag;

    new-instance v1, Lcom/google/android/gms/location/places/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/location/places/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/ag;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 131
    if-nez p1, :cond_1

    .line 132
    invoke-static {}, Lcom/google/android/gms/location/places/x;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    invoke-static {}, Lcom/google/android/gms/location/places/x;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPlaceUserDataFetched received null DataHolder: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/common/util/ar;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->e:Lcom/google/android/gms/location/places/ak;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/ak;->b(Lcom/google/android/gms/common/api/Status;)V

    .line 140
    :goto_0
    return-void

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/location/places/al;->e:Lcom/google/android/gms/location/places/ak;

    new-instance v1, Lcom/google/android/gms/location/places/personalized/d;

    invoke-direct {v1, p1}, Lcom/google/android/gms/location/places/personalized/d;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/places/ak;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0
.end method
