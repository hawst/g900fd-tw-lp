.class public final Lcom/google/android/gms/drive/database/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/a/f;


# static fields
.field private static final a:Lcom/google/android/gms/drive/database/a/g;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/drive/database/a/g;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/a/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/a/g;->a:Lcom/google/android/gms/drive/database/a/g;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    return-void
.end method

.method public static a()Lcom/google/android/gms/drive/database/a/g;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/gms/drive/database/a/g;->a:Lcom/google/android/gms/drive/database/a/g;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 15
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    return-object v0
.end method
