.class final Lcom/google/android/gms/droidguard/d/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/droidguard/d/q;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/droidguard/d/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/droidguard/d/o;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/af;->a:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/af;->b:Lcom/google/android/gms/droidguard/d/o;

    .line 44
    return-void
.end method

.method private a(Lcom/google/android/gms/droidguard/d/ag;)Lcom/google/android/gms/droidguard/d/k;
    .locals 8

    .prologue
    .line 47
    new-instance v1, Lcom/google/android/gms/droidguard/d/k;

    invoke-direct {v1}, Lcom/google/android/gms/droidguard/d/k;-><init>()V

    .line 50
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/af;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/droidguard/p;->a(Landroid/content/Context;)Lcom/google/android/gms/http/GoogleHttpClient;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 52
    :try_start_1
    new-instance v0, Lorg/apache/http/client/methods/HttpGet;

    iget-object v3, p1, Lcom/google/android/gms/droidguard/d/ag;->b:Ljava/lang/String;

    invoke-direct {v0, v3}, Lorg/apache/http/client/methods/HttpGet;-><init>(Ljava/lang/String;)V

    .line 54
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-wide v4

    .line 56
    :try_start_2
    invoke-virtual {v2, v0}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 58
    :try_start_3
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 60
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v3

    invoke-interface {v3}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v3

    .line 61
    const/16 v4, 0xc8

    if-eq v3, v4, :cond_0

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Received status code ["

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "] instead of [200]."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 84
    :try_start_4
    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 90
    :goto_0
    return-object v0

    .line 58
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-static {v4, v5}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 84
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    throw v0
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 88
    :catch_0
    move-exception v0

    .line 87
    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    :goto_1
    move-object v0, v1

    .line 90
    goto :goto_0

    .line 65
    :cond_0
    :try_start_7
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    .line 66
    if-nez v0, :cond_1

    .line 67
    const-string v0, "Failed to get HTTP entity."

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v0

    .line 84
    :try_start_8
    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0

    goto :goto_0

    .line 69
    :cond_1
    :try_start_9
    invoke-interface {v0}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v0

    .line 70
    iget-object v3, p0, Lcom/google/android/gms/droidguard/d/af;->a:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ".apk"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/io/File;

    const-string v6, "dg_cache"

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-direct {v5, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 71
    invoke-static {v0, v5}, Lcom/google/android/gms/droidguard/d/af;->a(Ljava/io/InputStream;Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    .line 72
    iget-object v3, p1, Lcom/google/android/gms/droidguard/d/ag;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 73
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 74
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "SHA1 verification failed for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ". Got: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v0

    .line 84
    :try_start_a
    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_0

    .line 77
    :cond_2
    :try_start_b
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/af;->b:Lcom/google/android/gms/droidguard/d/o;

    invoke-interface {v0, v5}, Lcom/google/android/gms/droidguard/d/o;->a(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 78
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Signature verification failed for "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v0

    .line 84
    :try_start_c
    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0

    goto/16 :goto_0

    .line 82
    :cond_3
    :try_start_d
    new-instance v0, Lcom/google/android/gms/droidguard/d/ab;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    invoke-direct {v0, v5, v3, v4, v6}, Lcom/google/android/gms/droidguard/d/ab;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 84
    :try_start_e
    invoke-virtual {v2}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0

    goto/16 :goto_1
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/File;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 96
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 98
    :try_start_1
    const-string v0, "SHA1"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    .line 100
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 111
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 102
    :cond_0
    :try_start_3
    new-instance v2, Ljava/security/DigestOutputStream;

    invoke-direct {v2, v1, v0}, Ljava/security/DigestOutputStream;-><init>(Ljava/io/OutputStream;Ljava/security/MessageDigest;)V

    .line 104
    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J

    .line 105
    invoke-virtual {v2}, Ljava/security/DigestOutputStream;->flush()V

    .line 106
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    .line 108
    :try_start_4
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 111
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    goto :goto_0

    .line 108
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 111
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;
    .locals 1

    .prologue
    .line 32
    check-cast p1, Lcom/google/android/gms/droidguard/d/ag;

    invoke-direct {p0, p1}, Lcom/google/android/gms/droidguard/d/af;->a(Lcom/google/android/gms/droidguard/d/ag;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v0

    return-object v0
.end method
