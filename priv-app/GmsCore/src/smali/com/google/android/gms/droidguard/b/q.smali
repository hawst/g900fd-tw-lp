.class final Lcom/google/android/gms/droidguard/b/q;
.super Ljava/lang/Exception;
.source "SourceFile"


# instance fields
.field private final a:I


# direct methods
.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    .line 19
    iput p1, p0, Lcom/google/android/gms/droidguard/b/q;->a:I

    .line 20
    return-void
.end method

.method constructor <init>(ILjava/lang/Exception;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    .line 24
    iput p1, p0, Lcom/google/android/gms/droidguard/b/q;->a:I

    .line 25
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/gms/droidguard/b/q;->a:I

    return v0
.end method

.method final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/droidguard/b/q;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {v0}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v0

    .line 36
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/droidguard/b/q;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    .line 41
    if-eqz v0, :cond_0

    .line 42
    invoke-static {v0}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
