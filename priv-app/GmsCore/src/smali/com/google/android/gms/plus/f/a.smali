.class public final Lcom/google/android/gms/plus/f/a;
.super Lcom/google/android/gms/plus/f/e;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static j:Landroid/animation/PropertyValuesHolder;


# instance fields
.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/plus/f/e;-><init>()V

    .line 62
    return-void
.end method

.method private static a(Landroid/view/View;J)Landroid/animation/Animator;
    .locals 11

    .prologue
    const-wide/16 v8, 0x1f4

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v5, 0x2

    .line 113
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleX(F)V

    .line 114
    invoke-virtual {p0, v0}, Landroid/view/View;->setScaleY(F)V

    .line 116
    new-array v0, v7, [Landroid/animation/PropertyValuesHolder;

    sget-object v1, Lcom/google/android/gms/plus/f/a;->j:Landroid/animation/PropertyValuesHolder;

    aput-object v1, v0, v6

    invoke-static {p0, v0}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 118
    const-wide/16 v2, 0x1d3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 120
    sget-object v1, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v2, v5, [F

    fill-array-data v2, :array_0

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 121
    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 123
    sget-object v2, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v3, v5, [F

    fill-array-data v3, :array_1

    invoke-static {p0, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 124
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 126
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 127
    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 128
    invoke-virtual {v3, p1, p2}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    .line 129
    return-object v3

    .line 120
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 123
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/plus/f/e;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 41
    invoke-static {}, Lcom/google/android/gms/plus/f/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    invoke-static {p0, v6}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;Z)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    .line 45
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/f/a;->j:Landroid/animation/PropertyValuesHolder;

    if-nez v0, :cond_1

    .line 46
    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v1, 0x4

    new-array v1, v1, [Landroid/animation/Keyframe;

    const/4 v2, 0x0

    invoke-static {v4, v4}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    aput-object v3, v1, v2

    const v2, 0x3eb851ec    # 0.36f

    invoke-static {v2, v5}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v2

    aput-object v2, v1, v6

    const/4 v2, 0x2

    const v3, 0x3f11eb85    # 0.57f

    invoke-static {v3, v5}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {v5, v4}, Landroid/animation/Keyframe;->ofFloat(FF)Landroid/animation/Keyframe;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofKeyframe(Landroid/util/Property;[Landroid/animation/Keyframe;)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/f/a;->j:Landroid/animation/PropertyValuesHolder;

    .line 53
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 54
    const-string v0, "message"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "cancelable"

    invoke-virtual {v1, v0, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 56
    new-instance v0, Lcom/google/android/gms/plus/f/a;

    invoke-direct {v0}, Lcom/google/android/gms/plus/f/a;-><init>()V

    .line 57
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/f/a;->setArguments(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/f/a;)Z
    .locals 1

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/google/android/gms/plus/f/a;->k:Z

    return v0
.end method

.method private static b()Z
    .locals 2

    .prologue
    .line 133
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/f/a;)Z
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/f/a;->k:Z

    return v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 105
    invoke-static {}, Lcom/google/android/gms/plus/f/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/f/e;->a_(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/f/e;->b(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/f/e;->onCreate(Landroid/os/Bundle;)V

    .line 67
    invoke-static {}, Lcom/google/android/gms/plus/f/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    :goto_0
    return-void

    .line 71
    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Landroid/support/v4/app/m;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/f/a;->a(II)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 77
    invoke-static {}, Lcom/google/android/gms/plus/f/a;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/f/e;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 81
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/f/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->f:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 84
    sget v0, Lcom/google/android/gms/j;->aF:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/plus/f/a;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "message"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    sget v0, Lcom/google/android/gms/j;->aC:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 89
    sget v2, Lcom/google/android/gms/j;->aD:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 90
    sget v3, Lcom/google/android/gms/j;->aE:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 91
    new-instance v4, Landroid/animation/AnimatorSet;

    invoke-direct {v4}, Landroid/animation/AnimatorSet;-><init>()V

    .line 92
    const/4 v5, 0x3

    new-array v5, v5, [Landroid/animation/Animator;

    const-wide/16 v6, 0x0

    invoke-static {v0, v6, v7}, Lcom/google/android/gms/plus/f/a;->a(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v0

    aput-object v0, v5, v8

    const/4 v0, 0x1

    const-wide/16 v6, 0x10b

    invoke-static {v2, v6, v7}, Lcom/google/android/gms/plus/f/a;->a(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v2

    aput-object v2, v5, v0

    const/4 v0, 0x2

    const-wide/16 v6, 0x216

    invoke-static {v3, v6, v7}, Lcom/google/android/gms/plus/f/a;->a(Landroid/view/View;J)Landroid/animation/Animator;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-virtual {v4, v5}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 96
    new-instance v0, Lcom/google/android/gms/plus/f/b;

    invoke-direct {v0, p0, v8}, Lcom/google/android/gms/plus/f/b;-><init>(Lcom/google/android/gms/plus/f/a;B)V

    invoke-virtual {v4, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 97
    invoke-virtual {v4}, Landroid/animation/AnimatorSet;->start()V

    move-object v0, v1

    .line 99
    goto :goto_0
.end method
