.class public Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/queries/d;


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/gms/search/queries/d;

    invoke-direct {v0}, Lcom/google/android/gms/search/queries/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->c:I

    .line 63
    return-void
.end method

.method constructor <init>(I[Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput p1, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->c:I

    .line 80
    iput-object p2, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->a:[Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    .line 82
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/d;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->CREATOR:Lcom/google/android/gms/search/queries/d;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/queries/d;->a(Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;Landroid/os/Parcel;I)V

    .line 94
    return-void
.end method
