.class public final Lcom/google/android/gms/googlehelp/f/e;
.super Lcom/google/android/gms/googlehelp/f/a;
.source "SourceFile"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private final d:Lcom/google/android/gms/googlehelp/common/HelpConfig;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/f/a;-><init>(Lcom/google/android/gms/googlehelp/f/n;)V

    .line 50
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->b:Landroid/content/Context;

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/f/e;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 52
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 53
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 36
    check-cast p1, Lcom/google/ad/a/a/u;

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/e;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Landroid/content/Context;Lcom/google/ad/a/a/u;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    const-string v2, "escalation_options"

    :try_start_0
    new-instance v3, Lcom/google/android/gms/googlehelp/common/f;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/googlehelp/common/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    new-instance v0, Ljava/io/File;

    iget-object v1, v3, Lcom/google/android/gms/googlehelp/common/f;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v3, v2}, Lcom/google/android/gms/googlehelp/common/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x0

    :try_start_1
    new-instance v1, Ljava/io/BufferedOutputStream;

    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v1, v3}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/BufferedOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :try_start_3
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->i()V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->p()I

    move-result v0

    if-ne v0, v5, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v1, "CHAT_AVAILABLE_BUT_HANGOUTS_NOT_SUPPORTED"

    sget-object v2, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->r()I

    move-result v0

    if-ne v0, v6, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v1, "VIDEO_AVAILABLE_BUT_VIDEO_NOT_SUPPORTED"

    sget-object v2, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a:[Ljava/lang/String;

    aget-object v2, v2, v7

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;I)V

    :cond_1
    :goto_2
    return-void

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->close()V

    :cond_2
    throw v0
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    const-string v1, "GOOGLEHELP_GoogleHelpFiles"

    const-string v2, "Writing nano proto to file failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->p()I

    move-result v0

    if-ne v0, v6, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v1, "CHAT_AVAILABLE_BUT_CHAT_NOT_SUPPORTED"

    sget-object v2, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a:[Ljava/lang/String;

    aget-object v2, v2, v5

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->r()I

    move-result v0

    if-ne v0, v5, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v1, "VIDEO_AVAILABLE_BUT_HANGOUTS_NOT_SUPPORTED"

    sget-object v2, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->a:[Ljava/lang/String;

    aget-object v2, v2, v7

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_2

    :catchall_1
    move-exception v0

    goto :goto_3
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/e;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/e;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/a/g;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/ad/a/a/u;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
