.class public final Lcom/google/android/gms/ads/internal/o/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

.field public final b:Lcom/google/android/gms/ads/internal/p/a;

.field public final c:Ljava/util/List;

.field public final d:I

.field public final e:Ljava/util/List;

.field public final f:Ljava/util/List;

.field public final g:I

.field public final h:J

.field public final i:Ljava/lang/String;

.field public final j:Lorg/json/JSONObject;

.field public final k:Z

.field public final l:Lcom/google/android/gms/ads/internal/k/c;

.field public final m:Lcom/google/android/gms/ads/internal/k/a/d;

.field public final n:Ljava/lang/String;

.field public final o:Lcom/google/android/gms/ads/internal/k/d;

.field public final p:Lcom/google/android/gms/ads/internal/k/f;

.field public final q:J

.field public final r:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field public final s:J

.field public final t:J

.field public final u:J

.field public final v:Ljava/lang/String;

.field public final w:Lcom/google/android/gms/ads/internal/f/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/p/a;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/k/a/d;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/k/f;JLcom/google/android/gms/ads/internal/client/AdSizeParcel;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/ads/internal/f/d;)V
    .locals 4

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/o/a;->a:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    .line 120
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    .line 121
    if-eqz p3, :cond_0

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_0
    iput-object v2, p0, Lcom/google/android/gms/ads/internal/o/a;->c:Ljava/util/List;

    .line 122
    iput p4, p0, Lcom/google/android/gms/ads/internal/o/a;->d:I

    .line 123
    if-eqz p5, :cond_1

    invoke-static {p5}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_1
    iput-object v2, p0, Lcom/google/android/gms/ads/internal/o/a;->e:Ljava/util/List;

    .line 125
    if-eqz p6, :cond_2

    invoke-static {p6}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    :goto_2
    iput-object v2, p0, Lcom/google/android/gms/ads/internal/o/a;->f:Ljava/util/List;

    .line 129
    iput p7, p0, Lcom/google/android/gms/ads/internal/o/a;->g:I

    .line 130
    iput-wide p8, p0, Lcom/google/android/gms/ads/internal/o/a;->h:J

    .line 131
    iput-object p10, p0, Lcom/google/android/gms/ads/internal/o/a;->i:Ljava/lang/String;

    .line 132
    iput-boolean p11, p0, Lcom/google/android/gms/ads/internal/o/a;->k:Z

    .line 133
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->l:Lcom/google/android/gms/ads/internal/k/c;

    .line 134
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->m:Lcom/google/android/gms/ads/internal/k/a/d;

    .line 135
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->n:Ljava/lang/String;

    .line 136
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->o:Lcom/google/android/gms/ads/internal/k/d;

    .line 137
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->p:Lcom/google/android/gms/ads/internal/k/f;

    .line 138
    move-wide/from16 v0, p17

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/o/a;->q:J

    .line 139
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->r:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 140
    move-wide/from16 v0, p20

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/o/a;->s:J

    .line 141
    move-wide/from16 v0, p22

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/o/a;->t:J

    .line 142
    move-wide/from16 v0, p24

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/o/a;->u:J

    .line 143
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->v:Ljava/lang/String;

    .line 144
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->j:Lorg/json/JSONObject;

    .line 145
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->w:Lcom/google/android/gms/ads/internal/f/d;

    .line 146
    return-void

    .line 121
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 123
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 125
    :cond_2
    const/4 v2, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/p/a;)V
    .locals 31

    .prologue
    .line 156
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v3, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v5, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->d:Ljava/util/List;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/gms/ads/internal/o/b;->e:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v7, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->f:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v8, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->j:Ljava/util/List;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v9, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->l:I

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v10, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->k:J

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v12, v2, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->i:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v13, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->h:Z

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->c:Lcom/google/android/gms/ads/internal/k/d;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v0, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->i:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v21, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v0, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/o/b;->f:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/o/b;->g:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v0, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->o:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->h:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p2

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/ads/internal/o/a;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/p/a;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/k/a/d;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/k/f;JLcom/google/android/gms/ads/internal/client/AdSizeParcel;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/ads/internal/f/d;)V

    .line 179
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    if-nez v0, :cond_1

    .line 183
    :cond_0
    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/a;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/c;->b()Z

    move-result v0

    goto :goto_0
.end method
