.class public final Lcom/google/android/gms/location/copresence/internal/BatchImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/location/copresence/h;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/util/ArrayList;

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/b;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->b:I

    .line 74
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a:Ljava/util/ArrayList;

    .line 75
    return-void
.end method

.method constructor <init>(ILjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->b:I

    .line 68
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a:Ljava/util/ArrayList;

    .line 69
    return-void
.end method

.method private a(Lcom/google/android/gms/location/copresence/internal/Operation;)Lcom/google/android/gms/location/copresence/internal/BatchImpl;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 83
    return-object p0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->b:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/a;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/location/copresence/internal/a;-><init>(Lcom/google/android/gms/location/copresence/internal/BatchImpl;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/h;
    .locals 1

    .prologue
    .line 137
    invoke-static {p1}, Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;->a(Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/UnsubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a(Lcom/google/android/gms/location/copresence/internal/Operation;)Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/location/copresence/u;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/h;
    .locals 4

    .prologue
    .line 124
    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    invoke-virtual {p2}, Lcom/google/android/gms/location/copresence/MessageFilter;->d()J

    move-result-wide v0

    const-wide/32 v2, 0x927c0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The \"messageFilter\" must have a time to live of at most 10 minutes in no-opt-in mode."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 130
    :cond_0
    check-cast p1, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;->a(Lcom/google/android/gms/location/copresence/internal/StrategyImpl;Lcom/google/android/gms/location/copresence/MessageFilter;Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/SubscribeOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a(Lcom/google/android/gms/location/copresence/internal/Operation;)Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    move-result-object v0

    return-object v0

    .line 125
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/h;
    .locals 2

    .prologue
    .line 107
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "p#"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a(Lcom/google/android/gms/location/copresence/internal/Operation;)Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/location/copresence/u;Lcom/google/android/gms/location/copresence/Message;Lcom/google/android/gms/location/copresence/AccessPolicy;)Lcom/google/android/gms/location/copresence/h;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v3, 0x30

    if-gt v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "The \"publishId\" parameter can be at most 48 characters."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    move-object v0, p2

    .line 94
    check-cast v0, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p4}, Lcom/google/android/gms/location/copresence/AccessPolicy;->f()J

    move-result-wide v4

    const-wide/32 v6, 0x927c0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_2

    :goto_1
    const-string v0, "The \"accessPolicy\" must have a time to live of at most 10 minutes in no-opt-in mode."

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 100
    :cond_0
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/PublishOperation;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "p#"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    check-cast p2, Lcom/google/android/gms/location/copresence/internal/StrategyImpl;

    invoke-direct {v0, v1, p2, p3, p4}, Lcom/google/android/gms/location/copresence/internal/PublishOperation;-><init>(Ljava/lang/String;Lcom/google/android/gms/location/copresence/internal/StrategyImpl;Lcom/google/android/gms/location/copresence/Message;Lcom/google/android/gms/location/copresence/AccessPolicy;)V

    invoke-static {v0}, Lcom/google/android/gms/location/copresence/internal/Operation;->a(Lcom/google/android/gms/location/copresence/internal/PublishOperation;)Lcom/google/android/gms/location/copresence/internal/Operation;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/location/copresence/internal/BatchImpl;->a(Lcom/google/android/gms/location/copresence/internal/Operation;)Lcom/google/android/gms/location/copresence/internal/BatchImpl;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v2

    .line 90
    goto :goto_0

    :cond_2
    move v1, v2

    .line 95
    goto :goto_1
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 208
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 213
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/internal/b;->a(Lcom/google/android/gms/location/copresence/internal/BatchImpl;Landroid/os/Parcel;)V

    .line 214
    return-void
.end method
