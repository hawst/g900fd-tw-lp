.class public final Lcom/google/android/gms/wallet/service/ia/ad;
.super Lcom/google/android/gms/wallet/service/ia/m;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ia/o;

.field private final b:Lcom/google/android/gms/wallet/service/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ia/o;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ia/m;-><init>()V

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/ad;->a:Lcom/google/android/gms/wallet/service/ia/o;

    .line 35
    new-instance v0, Lcom/google/android/gms/wallet/service/p;

    const-string v1, "NetworkIaService"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/wallet/service/p;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    .line 36
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/service/ia/ad;)Lcom/google/android/gms/wallet/service/ia/o;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/ad;->a:Lcom/google/android/gms/wallet/service/ia/o;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sparse-switch v1, :sswitch_data_0

    const-string v0, "https://checkout.google.com"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sparse-switch v1, :sswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected environment type = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const-string v0, "https://sandbox.google.com"

    goto :goto_0

    :sswitch_1
    const-string v0, "https://starlightdemo.corp.google.com"

    goto :goto_0

    :sswitch_2
    const-string v0, "/checkout"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :sswitch_3
    const-string v0, ""

    goto :goto_1

    :sswitch_4
    const-string v0, ""

    goto :goto_1

    :sswitch_5
    const-string v0, "/purchases"

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_0
        0x14 -> :sswitch_1
        0x15 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_3
        0x2 -> :sswitch_2
        0x14 -> :sswitch_4
        0x15 -> :sswitch_2
        0x16 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 242
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/af;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/af;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 272
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ah;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/ah;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 257
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ag;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/ag;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 102
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ak;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/ak;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 41
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ae;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/ae;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 178
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ao;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/ao;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 227
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/aq;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/aq;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 212
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ap;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/ap;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 286
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/ai;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/ai;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 132
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/am;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/am;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 163
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/an;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/an;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 117
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/al;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/al;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 71
    iget-object v6, p0, Lcom/google/android/gms/wallet/service/ia/ad;->b:Lcom/google/android/gms/wallet/service/p;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/aj;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/service/ia/aj;-><init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/wallet/service/p;->a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method
