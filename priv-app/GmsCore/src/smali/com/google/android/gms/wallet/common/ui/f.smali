.class public final Lcom/google/android/gms/wallet/common/ui/f;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/ah;
.implements Lcom/google/android/gms/wallet/common/ui/cb;
.implements Lcom/google/android/gms/wallet/common/ui/dp;
.implements Ljava/util/Comparator;


# instance fields
.field a:Z

.field b:Z

.field c:Ljava/util/ArrayList;

.field d:Lcom/google/android/gms/wallet/common/ui/an;

.field e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

.field f:Landroid/view/View;

.field g:Landroid/view/View;

.field h:Landroid/view/View;

.field i:Landroid/widget/RadioGroup;

.field private j:Z

.field private k:Lcom/google/android/gms/wallet/common/ui/cr;

.field private l:Z

.field private m:Z

.field private n:Lcom/google/android/gms/wallet/common/ui/ah;

.field private o:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 48
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 76
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->j:Z

    .line 78
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->l:Z

    .line 79
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    .line 81
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->b:Z

    .line 83
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->m:Z

    return-void
.end method

.method private static a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)I
    .locals 3

    .prologue
    .line 533
    if-ne p0, p1, :cond_0

    .line 534
    const/4 v0, 0x0

    .line 543
    :goto_0
    return v0

    .line 535
    :cond_0
    if-nez p0, :cond_1

    .line 536
    const/4 v0, 0x1

    goto :goto_0

    .line 537
    :cond_1
    if-nez p1, :cond_2

    .line 538
    const/4 v0, -0x1

    goto :goto_0

    .line 541
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 542
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    .line 543
    sget-object v2, Lcom/google/android/gms/wallet/common/a/e;->a:Ljava/util/Comparator;

    invoke-interface {v2, v0, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;ILjava/util/ArrayList;Ljava/lang/String;ZZ[I[ILjava/util/Collection;Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/f;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 138
    if-eqz p0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "buyFlowConfig must not be null"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 139
    if-eqz p1, :cond_1

    :goto_1
    const-string v0, "account must not be null"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 141
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/f;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/f;-><init>()V

    .line 143
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 144
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 145
    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    const-string v2, "cardEntryContext"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 147
    const-string v2, "allowedCountryCodes"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 148
    const-string v2, "defaultCountryCode"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v2, "requiresFullAddress"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 151
    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 152
    const-string v2, "phoneNumberRequired"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 153
    const-string v2, "addressHints"

    invoke-static {v1, v2, p9}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    .line 154
    const-string v2, "analyticsSessionId"

    invoke-virtual {v1, v2, p10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/f;->setArguments(Landroid/os/Bundle;)V

    .line 157
    return-object v0

    :cond_0
    move v0, v2

    .line 138
    goto :goto_0

    :cond_1
    move v1, v2

    .line 139
    goto :goto_1
.end method

.method private a(ZLjava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 548
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 549
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    .line 550
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 551
    const/4 v1, 0x0

    move v6, v4

    .line 552
    :goto_0
    if-ge v6, v7, :cond_3

    .line 553
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 554
    iget-boolean v2, v0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    move v2, v3

    .line 555
    :goto_1
    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    .line 556
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v5, v5, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-virtual {p2, v5}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    move v5, v3

    .line 561
    :goto_2
    if-nez v2, :cond_4

    if-nez v9, :cond_4

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/f;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v2

    if-eqz v2, :cond_4

    if-eqz v5, :cond_4

    .line 562
    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    :goto_3
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move-object v1, v0

    goto :goto_0

    :cond_1
    move v2, v4

    .line 554
    goto :goto_1

    :cond_2
    move v5, v4

    .line 556
    goto :goto_2

    .line 568
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 569
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 570
    return-void

    :cond_4
    move-object v0, v1

    goto :goto_3
.end method

.method private b(I)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 476
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    .line 477
    :goto_0
    if-ge v2, v4, :cond_2

    .line 478
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 479
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    sget v5, Lcom/google/android/gms/l;->gK:I

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/f;->i:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v5, v6, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 482
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    .line 483
    add-int/lit8 v0, v2, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setId(I)V

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v5, 0x1

    if-ne v0, v5, :cond_0

    .line 485
    const v0, 0x106000d

    invoke-virtual {v1, v0}, Landroid/widget/RadioButton;->setButtonDrawable(I)V

    .line 487
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->i:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->addView(Landroid/view/View;)V

    .line 489
    add-int/lit8 v0, v4, -0x1

    if-ge v2, v0, :cond_1

    .line 490
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->gL:I

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/f;->i:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 477
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 495
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->i:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p1}, Landroid/widget/RadioGroup;->check(I)V

    .line 496
    return-void
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 171
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/f;->j()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    if-eqz v0, :cond_3

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    aput-object v1, v0, v2

    .line 173
    :goto_1
    array-length v5, v0

    move v4, v3

    move v1, v2

    :goto_2
    if-ge v4, v5, :cond_6

    aget-object v6, v0, v4

    .line 174
    if-eqz p1, :cond_5

    .line 175
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_4

    if-eqz v1, :cond_4

    move v1, v2

    .line 173
    :cond_1
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    move v0, v3

    .line 171
    goto :goto_0

    :cond_3
    new-array v0, v2, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    aput-object v1, v0, v3

    goto :goto_1

    :cond_4
    move v1, v3

    .line 175
    goto :goto_3

    .line 176
    :cond_5
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_1

    .line 180
    :goto_4
    return v3

    :cond_6
    move v3, v1

    goto :goto_4
.end method

.method private i()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->j:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/an;->a(Z)V

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->j:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Z)V

    .line 222
    :cond_0
    return-void
.end method

.method private j()Z
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/checkout/a/a/a/d;
    .locals 4

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/an;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/a/a/a/d;

    .line 237
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/f;->j()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iget-object v1, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h()Lcom/google/t/a/b;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->l:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    .line 238
    :cond_1
    :goto_0
    return-object v0

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->i:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iput-object v1, v2, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/an;->getId()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->h:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 404
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->m:Z

    .line 407
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/f;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 408
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 409
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->b:Z

    .line 410
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    .line 411
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 422
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->n:Lcom/google/android/gms/wallet/common/ui/ah;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->n:Lcom/google/android/gms/wallet/common/ui/ah;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getId()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ah;->a(I)V

    .line 426
    :cond_0
    return-void

    .line 413
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 414
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->b:Z

    .line 415
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 416
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/ah;)V
    .locals 0

    .prologue
    .line 443
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/f;->n:Lcom/google/android/gms/wallet/common/ui/ah;

    .line 444
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/cr;)V
    .locals 1

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/f;->k:Lcom/google/android/gms/wallet/common/ui/cr;

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/cr;)V

    .line 194
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/an;->a(Ljava/lang/String;)V

    .line 262
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 213
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/f;->j:Z

    .line 214
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/f;->i()V

    .line 215
    return-void
.end method

.method public final a([I)V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/an;->a([I)V

    .line 440
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b()V

    .line 246
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c()V

    .line 250
    return-void
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 48
    check-cast p1, Lcom/google/checkout/inapp/proto/a/b;

    check-cast p2, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, p2}, Lcom/google/android/gms/wallet/common/ui/f;->a(Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d()V

    .line 254
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/f;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/f;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/an;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/an;->c()V

    .line 258
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->kv:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 395
    if-eqz v0, :cond_0

    .line 396
    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 398
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 453
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->f:Landroid/view/View;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->g:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 455
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->b:Z

    .line 456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    .line 457
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 266
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 267
    if-nez p1, :cond_0

    .line 277
    :goto_0
    return-void

    .line 271
    :cond_0
    const-string v0, "enabled"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->j:Z

    .line 273
    const-string v0, "addressAndPhoneNumberShowing"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    .line 274
    const-string v0, "addressContainerShowing"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->m:Z

    .line 275
    const-string v0, "addressSelectorShowing"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->b:Z

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    .line 282
    sget v0, Lcom/google/android/gms/l;->gr:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 284
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 285
    const-string v0, "defaultCountryCode"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v1

    .line 287
    const-string v0, "allowedCountryCodes"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v7

    .line 289
    invoke-static {v7}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/List;)[I

    move-result-object v2

    .line 291
    const-string v0, "phoneNumberRequired"

    const/4 v3, 0x1

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->l:Z

    .line 292
    sget v0, Lcom/google/android/gms/j;->as:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->f:Landroid/view/View;

    .line 293
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    if-nez v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->f:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 298
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->as:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 300
    const-string v0, "addressHints"

    const-class v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v0, v3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    .line 302
    :cond_1
    const-string v0, "requiresFullAddress"

    const/4 v3, 0x1

    invoke-virtual {v5, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    if-nez v0, :cond_2

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;)Landroid/util/Pair;

    move-result-object v3

    .line 307
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a()Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v4

    if-nez v8, :cond_5

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v4, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v4, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput-object v2, v4, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    iput v1, v2, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    sget v1, Lcom/google/android/gms/p;->Ak:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(I)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v1

    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->a(Ljava/util/Collection;)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->l:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/s;->b(Z)Lcom/google/android/gms/wallet/common/ui/s;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/s;->a:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 316
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    .line 318
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->as:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->e:Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->k:Lcom/google/android/gms/wallet/common/ui/cr;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/android/gms/wallet/common/ui/cr;)V

    .line 324
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->kv:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/an;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    if-nez v0, :cond_3

    .line 327
    const-string v0, "buyFlowConfig"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 330
    const-string v1, "cardEntryContext"

    const/4 v2, 0x0

    invoke-virtual {v5, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    .line 332
    const-string v1, "account"

    invoke-virtual {v5, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/accounts/Account;

    .line 333
    const-string v3, "disallowedCreditCardTypes"

    invoke-virtual {v5, v3}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v3

    .line 335
    const-string v4, "disallowedCardCategories"

    invoke-virtual {v5, v4}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v4

    .line 337
    const-string v9, "analyticsSessionId"

    invoke-virtual {v5, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 338
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/ui/an;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;I[I[ILjava/lang/String;)Lcom/google/android/gms/wallet/common/ui/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    .line 341
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/f;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->kv:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 350
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->d:Lcom/google/android/gms/wallet/common/ui/an;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/an;->a(Lcom/google/android/gms/wallet/common/ui/ah;)V

    .line 352
    sget v0, Lcom/google/android/gms/j;->av:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->g:Landroid/view/View;

    .line 353
    sget v0, Lcom/google/android/gms/j;->tZ:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->o:Landroid/widget/Button;

    .line 354
    sget v0, Lcom/google/android/gms/j;->tY:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->i:Landroid/widget/RadioGroup;

    .line 356
    if-eqz p3, :cond_6

    const-string v0, "addressSelectorSelectedId"

    const/4 v1, 0x1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 359
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/f;->j()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-direct {p0, v8, v7}, Lcom/google/android/gms/wallet/common/ui/f;->a(ZLjava/util/ArrayList;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/f;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->o:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 362
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->g:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->b:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 363
    sget v0, Lcom/google/android/gms/j;->tX:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->h:Landroid/view/View;

    .line 364
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->h:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/f;->m:Z

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 366
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/f;->i()V

    .line 368
    return-object v6

    .line 307
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 356
    :cond_6
    const/4 v0, 0x1

    goto :goto_1

    .line 362
    :cond_7
    const/16 v0, 0x8

    goto :goto_2

    .line 364
    :cond_8
    const/16 v0, 0x8

    goto :goto_3
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 381
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 383
    const-string v0, "enabled"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 384
    const-string v0, "addressAndPhoneNumberShowing"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 385
    const-string v0, "addressSelectorShowing"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 386
    const-string v0, "addressContainerShowing"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 387
    const-string v0, "addressSelectorSelectedId"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/f;->i:Landroid/widget/RadioGroup;

    invoke-virtual {v1}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 388
    return-void
.end method
