.class public abstract Lcom/google/android/gms/auth/setup/d2d/a/e;
.super Lcom/google/android/gms/auth/setup/d2d/a/a;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private final b:Landroid/bluetooth/BluetoothAdapter;

.field private final c:Ljava/util/UUID;

.field private final d:Ljava/lang/String;

.field private e:Landroid/bluetooth/BluetoothServerSocket;

.field private f:Landroid/bluetooth/BluetoothSocket;

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "BluetoothConnectionAcceptanceTask"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/a/e;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>(Landroid/bluetooth/BluetoothAdapter;Ljava/util/UUID;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/a/a;-><init>()V

    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->g:Z

    .line 34
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothAdapter;

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->b:Landroid/bluetooth/BluetoothAdapter;

    .line 35
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/UUID;

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->c:Ljava/util/UUID;

    .line 36
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->d:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->g:Z

    if-eqz v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->f:Landroid/bluetooth/BluetoothSocket;

    if-eqz v0, :cond_2

    .line 64
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothSocket;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 68
    :goto_1
    iput-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->f:Landroid/bluetooth/BluetoothSocket;

    .line 70
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->e:Landroid/bluetooth/BluetoothServerSocket;

    if-eqz v0, :cond_0

    .line 72
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->e:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothServerSocket;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 76
    :goto_2
    iput-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->e:Landroid/bluetooth/BluetoothServerSocket;

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/a/e;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_1

    .line 73
    :catch_1
    move-exception v0

    .line 74
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/a/e;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/Throwable;)V

    goto :goto_2
.end method

.method public run()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xa
    .end annotation

    .prologue
    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->b:Landroid/bluetooth/BluetoothAdapter;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->c:Ljava/util/UUID;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->listenUsingInsecureRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->e:Landroid/bluetooth/BluetoothServerSocket;

    .line 46
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a/e;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Accepting Bluetooth connection..."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->e:Landroid/bluetooth/BluetoothServerSocket;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothServerSocket;->accept()Landroid/bluetooth/BluetoothSocket;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->f:Landroid/bluetooth/BluetoothSocket;

    .line 48
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a/e;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Accepted Bluetooth connection."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->g:Z

    .line 50
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/a/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->e:Landroid/bluetooth/BluetoothServerSocket;

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/a/e;->f:Landroid/bluetooth/BluetoothSocket;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/setup/d2d/a/b;-><init>(Landroid/bluetooth/BluetoothServerSocket;Landroid/bluetooth/BluetoothSocket;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/a/e;->a(Lcom/google/android/gms/auth/setup/d2d/a/b;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/a/e;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Failed to accept Bluetooth connection."

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/a/e;->b()V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/a/e;->a()V

    goto :goto_0
.end method
