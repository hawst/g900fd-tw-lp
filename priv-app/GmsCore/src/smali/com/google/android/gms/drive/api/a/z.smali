.class final Lcom/google/android/gms/drive/api/a/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/realtime/j;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/api/a/y;

.field private b:Lcom/google/android/gms/drive/api/l;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/api/a/y;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/gms/drive/api/a/z;->a:Lcom/google/android/gms/drive/api/a/y;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/z;->b:Lcom/google/android/gms/drive/api/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/api/l;->b()V

    .line 104
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/z;->b:Lcom/google/android/gms/drive/api/l;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/z;->b:Lcom/google/android/gms/drive/api/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/api/l;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 117
    :goto_0
    return-void

    .line 112
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/z;->a:Lcom/google/android/gms/drive/api/a/y;

    iget-object v0, v0, Lcom/google/android/gms/drive/api/a/y;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 117
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/c/a/a/a/a/a;Lcom/google/android/gms/drive/DriveId;)V
    .locals 4

    .prologue
    .line 84
    new-instance v0, Lcom/google/android/gms/drive/internal/OnLoadRealtimeResponse;

    iget-object v1, p1, Lcom/google/c/a/a/a/a/a;->f:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v1}, Lcom/google/c/a/a/b/b/a/b;->e()Z

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/internal/OnLoadRealtimeResponse;-><init>(Z)V

    .line 87
    new-instance v1, Lcom/google/android/gms/drive/realtime/e;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/z;->a:Lcom/google/android/gms/drive/api/a/y;

    iget-object v2, v2, Lcom/google/android/gms/drive/api/a/y;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v2}, Lcom/google/android/gms/drive/api/d;->a()Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    invoke-direct {v1, p2, v2}, Lcom/google/android/gms/drive/realtime/e;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;)V

    .line 90
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/z;->a:Lcom/google/android/gms/drive/api/a/y;

    invoke-static {v2}, Lcom/google/android/gms/drive/api/a/y;->a(Lcom/google/android/gms/drive/api/a/y;)Lcom/google/android/gms/drive/realtime/b/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/realtime/b/a;->d()Lcom/google/android/gms/drive/realtime/b;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/drive/realtime/b;->a(Lcom/google/android/gms/drive/realtime/e;)V

    .line 93
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/z;->a:Lcom/google/android/gms/drive/api/a/y;

    invoke-static {v2}, Lcom/google/android/gms/drive/api/a/y;->b(Lcom/google/android/gms/drive/api/a/y;)Lcom/google/android/gms/drive/api/f;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/z;->a:Lcom/google/android/gms/drive/api/a/y;

    iget-object v3, v3, Lcom/google/android/gms/drive/api/a/y;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v3}, Lcom/google/android/gms/drive/internal/ca;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, p1, v3, v1}, Lcom/google/android/gms/drive/api/f;->a(Lcom/google/c/a/a/a/a/a;Landroid/os/IBinder;Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/api/l;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/api/a/z;->b:Lcom/google/android/gms/drive/api/l;

    .line 95
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/z;->a:Lcom/google/android/gms/drive/api/a/y;

    iget-object v1, v1, Lcom/google/android/gms/drive/api/a/y;->b:Lcom/google/android/gms/drive/internal/ca;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/z;->b:Lcom/google/android/gms/drive/api/l;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnLoadRealtimeResponse;Lcom/google/android/gms/drive/realtime/internal/ag;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
