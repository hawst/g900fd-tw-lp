.class public Lcom/google/android/gms/feedback/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field protected a:Lcom/google/android/gms/feedback/ErrorReport;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/feedback/ai;->a:Lcom/google/android/gms/feedback/ErrorReport;

    .line 28
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 74
    const-string v0, "GFEEDBACK_SendErrorReportOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error sending feedback report: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    return-void
.end method

.method public bridge synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 0

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/gms/feedback/FeedbackAsyncService;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/feedback/ai;->a(Lcom/google/android/gms/feedback/FeedbackAsyncService;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/feedback/FeedbackAsyncService;)V
    .locals 5

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/feedback/aa;

    invoke-direct {v0, p1}, Lcom/google/android/gms/feedback/aa;-><init>(Landroid/content/Context;)V

    .line 33
    const/4 v1, 0x0

    .line 35
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/feedback/ai;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, p0, Lcom/google/android/gms/feedback/ai;->a:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v3, v3, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/feedback/aa;->a(Lcom/google/android/gms/feedback/ErrorReport;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 39
    :goto_0
    if-nez v0, :cond_0

    .line 40
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/ai;->a:Lcom/google/android/gms/feedback/ErrorReport;

    invoke-static {v0, p1}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;Landroid/content/Context;)Li/i;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/feedback/ac;->a(Landroid/content/Context;Li/i;)Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/gms/feedback/ai;->a:Lcom/google/android/gms/feedback/ErrorReport;

    sget v1, Lcom/google/android/gms/p;->oc:I

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;ILandroid/content/Context;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/feedback/SendService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    .line 45
    :goto_1
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    const-string v2, "GFEEDBACK_SendErrorReportOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error doing instant send: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    goto :goto_0

    .line 40
    :catch_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/feedback/ai;->a:Lcom/google/android/gms/feedback/ErrorReport;

    sget v2, Lcom/google/android/gms/p;->mP:I

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;ILandroid/content/Context;)V

    const-string v1, "GFEEDBACK_SendErrorReportOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error saving report: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :catch_2
    move-exception v0

    const-string v1, "GFEEDBACK_SendErrorReportOperation"

    const-string v2, "invalid report"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 43
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/ai;->a:Lcom/google/android/gms/feedback/ErrorReport;

    sget v1, Lcom/google/android/gms/p;->oc:I

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;ILandroid/content/Context;)V

    goto :goto_1
.end method
