.class public final Lcom/google/android/gms/drive/ui/picker/a/o;
.super Lcom/google/android/gms/drive/ui/picker/a/a;
.source "SourceFile"


# instance fields
.field private final c:I

.field private final d:Lcom/google/android/gms/drive/ui/picker/a/d;

.field private e:Lcom/google/android/gms/drive/ui/picker/a/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/view/DocListView;Landroid/widget/ListView;Lcom/google/android/gms/drive/ui/picker/a/s;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/drive/ui/picker/a/a;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Landroid/widget/ListView;)V

    .line 33
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/d;

    invoke-direct {v0, p1, p4}, Lcom/google/android/gms/drive/ui/picker/a/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/a/s;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->d:Lcom/google/android/gms/drive/ui/picker/a/d;

    .line 34
    invoke-virtual {p3}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->c:I

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/ui/picker/a/l;Lcom/google/android/gms/drive/ui/picker/a/a/g;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->b:Landroid/widget/ListView;

    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDividerHeight(I)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/b;

    if-nez v0, :cond_2

    .line 44
    new-instance v6, Lcom/google/android/gms/drive/ui/picker/a/p;

    invoke-direct {v6, p0}, Lcom/google/android/gms/drive/ui/picker/a/p;-><init>(Lcom/google/android/gms/drive/ui/picker/a/o;)V

    .line 51
    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->d:Lcom/google/android/gms/drive/ui/picker/a/d;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->a:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->a:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/b;

    iget-object v1, v5, Lcom/google/android/gms/drive/ui/picker/a/d;->a:Landroid/content/Context;

    iget-object v3, v5, Lcom/google/android/gms/drive/ui/picker/a/d;->b:Lcom/google/android/gms/drive/g/av;

    iget-object v7, v5, Lcom/google/android/gms/drive/ui/picker/a/d;->c:Lcom/google/android/gms/drive/ui/picker/a/s;

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/ui/picker/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/view/i;Lcom/google/android/gms/drive/g/av;Lcom/google/android/gms/drive/ui/picker/a/t;Lcom/google/android/gms/drive/ui/picker/a/l;Landroid/view/View$OnClickListener;Lcom/google/android/gms/drive/ui/picker/a/s;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/b;

    .line 57
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->a:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->f()Lcom/google/android/gms/drive/ui/picker/a/q;

    move-result-object v0

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/b;

    invoke-virtual {v1, p2, v0}, Lcom/google/android/gms/drive/ui/picker/a/b;->a(Lcom/google/android/gms/drive/ui/picker/a/a/g;Lcom/google/android/gms/drive/ui/picker/a/q;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/b;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/b;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 65
    :cond_0
    if-eqz v8, :cond_1

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->a:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i()V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/b;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)V

    .line 69
    :cond_1
    return-void

    .line 54
    :cond_2
    const/4 v8, 0x1

    goto :goto_0
.end method

.method protected final e()Lcom/google/android/gms/drive/ui/picker/a/r;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/b;

    return-object v0
.end method
