.class public Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;

.field private static b:Ljava/lang/String;


# instance fields
.field private c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "GLSActivity"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "FactoryResetProtectionActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->a:Lcom/google/android/gms/auth/d/a;

    .line 30
    const-string v0, "url"

    sput-object v0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 33
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 34
    sget-object v1, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    return-object v0
.end method

.method private a()V
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->c:Ljava/lang/String;

    const/4 v3, 0x0

    move-object v2, v0

    move v5, v4

    move-object v6, v0

    move-object v7, v0

    move-object v8, v0

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/auth/login/BrowserActivity;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZLjava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 69
    const-string v1, "firstRun"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    const-string v1, "useImmersiveMode"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 71
    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 72
    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v0, 0x0

    .line 81
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 82
    if-nez p2, :cond_0

    .line 87
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->setResult(I)V

    .line 88
    invoke-virtual {p0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->finish()V

    .line 114
    :goto_0
    return-void

    .line 90
    :cond_0
    if-eq p2, v2, :cond_1

    .line 93
    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->a()V

    goto :goto_0

    .line 97
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 107
    :pswitch_0
    if-nez p3, :cond_2

    .line 108
    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->a()V

    goto :goto_0

    .line 103
    :pswitch_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->setResult(I)V

    .line 104
    invoke-virtual {p0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->finish()V

    goto :goto_0

    .line 110
    :cond_2
    invoke-static {p3}, Lcom/google/android/gms/auth/login/aa;->a(Landroid/content/Intent;)Lcom/google/android/gms/auth/login/aa;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/gms/auth/login/aa;->a:Landroid/os/Bundle;

    const-string v4, "obfuscated_gaia_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/auth/login/aa;->b()Ljava/lang/String;

    move-result-object v2

    if-eqz v3, :cond_3

    if-nez v2, :cond_5

    :cond_3
    const-string v4, "gaiaid (%s) or authCode (%s) is null."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v0

    if-nez v2, :cond_4

    move v0, v1

    :cond_4
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->a:Lcom/google/android/gms/auth/d/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->a()V

    goto :goto_0

    :cond_5
    invoke-static {p0, v3}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3ea

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    if-nez p1, :cond_0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->c:Ljava/lang/String;

    .line 48
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->a()V

    .line 49
    return-void

    .line 46
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 54
    sget-object v0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/frp/FactoryResetProtectionActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method
