.class final Lcom/google/android/gms/people/sync/ak;
.super Lcom/google/android/gms/people/sync/al;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/sync/z;

.field private final e:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/sync/z;Z)V
    .locals 0

    .prologue
    .line 1800
    iput-object p1, p0, Lcom/google/android/gms/people/sync/ak;->a:Lcom/google/android/gms/people/sync/z;

    .line 1801
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/al;-><init>(Lcom/google/android/gms/people/sync/z;)V

    .line 1802
    iput-boolean p2, p0, Lcom/google/android/gms/people/sync/ak;->e:Z

    .line 1803
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 1812
    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/ak;->e:Z

    if-nez v0, :cond_0

    .line 1813
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ak;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1815
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ak;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->o()V

    .line 1816
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ak;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1818
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ak;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1821
    :cond_0
    return-void

    .line 1818
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ak;->a:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1844
    return-void
.end method

.method protected final b(Ljava/lang/String;)Ljava/util/List;
    .locals 1

    .prologue
    .line 1851
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final b()V
    .locals 1

    .prologue
    .line 1830
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/ak;->f()V

    .line 1833
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ak;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 1834
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ak;->b:Lcom/google/android/gms/people/internal/bf;

    iget-object v0, v0, Lcom/google/android/gms/people/internal/bf;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 1835
    return-void
.end method

.method protected final c()V
    .locals 0

    .prologue
    .line 1840
    return-void
.end method
