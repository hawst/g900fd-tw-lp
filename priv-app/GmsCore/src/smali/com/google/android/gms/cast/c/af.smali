.class public final Lcom/google/android/gms/cast/c/af;
.super Lcom/google/android/gms/cast/c/q;
.source "SourceFile"


# static fields
.field private static final g:I

.field private static final h:I

.field private static final i:[I

.field private static final j:[I


# instance fields
.field private final k:Ljava/util/List;

.field private final l:Ljava/util/Map;

.field private m:Ljava/lang/Thread;

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 36
    sget-object v0, Lcom/google/android/gms/cast/a/c;->b:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/af;->g:I

    .line 38
    sget-object v0, Lcom/google/android/gms/cast/a/c;->h:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/af;->h:I

    .line 41
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/cast/c/af;->i:[I

    .line 43
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/gms/cast/c/af;->j:[I

    return-void

    .line 41
    :array_0
    .array-data 4
        0x21
        0x10
    .end array-data

    .line 43
    :array_1
    .array-data 4
        0x1
        0x1c
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 52
    const-string v0, "MDNS"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/c/q;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    .line 55
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/af;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 29
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/af;->n:Z

    if-nez v0, :cond_2

    :try_start_0
    sget v0, Lcom/google/android/gms/cast/c/af;->g:I

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-object v1, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aj;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    sget v7, Lcom/google/android/gms/cast/c/af;->h:I

    int-to-long v8, v7

    invoke-virtual {v0, v4, v5, v8, v9}, Lcom/google/android/gms/cast/c/aq;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v7, "removing expired pending response"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v0, v7, v8}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v6}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/af;->n:Z

    if-eqz v0, :cond_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "refreshLoop exiting"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void

    :cond_3
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v6, p0, Lcom/google/android/gms/cast/c/af;->e:Ljava/util/Map;

    monitor-enter v6

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/y;

    iget-wide v8, v0, Lcom/google/android/gms/cast/c/y;->b:J

    sub-long v8, v4, v8

    sget v1, Lcom/google/android/gms/cast/c/q;->a:I

    int-to-long v10, v1

    cmp-long v1, v8, v10

    if-ltz v1, :cond_5

    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    new-instance v8, Lcom/google/android/gms/cast/c/ai;

    invoke-direct {v8, p0, v0}, Lcom/google/android/gms/cast/c/ai;-><init>(Lcom/google/android/gms/cast/c/af;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v1, v8}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v8, "Expired record for %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-virtual {v1, v8, v9}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v7}, Ljava/util/Iterator;->remove()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_5
    move v1, v3

    goto :goto_3

    :cond_6
    :try_start_4
    monitor-exit v6
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/af;Lcom/google/android/gms/cast/c/aq;)V
    .locals 14

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "got a response: PTR? %b, SRV? %b, TXT? %b, A? %b"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/aq;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/aq;->b()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/aq;->c()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/aq;->d()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/aq;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "response has no PTR record"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    iget-object v1, v0, Lcom/google/android/gms/cast/c/ao;->a:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "service instance: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Lcom/google/android/gms/cast/c/ap;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/google/android/gms/cast/c/aq;->e()Z

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "response is incomplete"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aj;

    if-eqz v0, :cond_7

    iget-object v3, v0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    if-nez v4, :cond_2

    iget-object v4, p1, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    iput-object v4, v3, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    :cond_2
    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    if-nez v4, :cond_3

    iget-object v4, p1, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    iput-object v4, v3, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    :cond_3
    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    if-nez v4, :cond_4

    iget-object v4, p1, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    iput-object v4, v3, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    :cond_4
    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    if-nez v4, :cond_5

    iget-object v4, p1, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    iput-object v4, v3, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    :cond_5
    iget-object v4, v3, Lcom/google/android/gms/cast/c/aq;->e:Lcom/google/android/gms/cast/c/al;

    if-nez v4, :cond_6

    iget-object v4, p1, Lcom/google/android/gms/cast/c/aq;->e:Lcom/google/android/gms/cast/c/al;

    iput-object v4, v3, Lcom/google/android/gms/cast/c/aq;->e:Lcom/google/android/gms/cast/c/al;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/aq;->e:Lcom/google/android/gms/cast/c/al;

    :cond_6
    :goto_1
    iget-object v3, v0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/c/aq;->e()Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "response is now complete"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object p1, v0, Lcom/google/android/gms/cast/c/aj;->a:Lcom/google/android/gms/cast/c/aq;

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v2

    :goto_2
    const-string v0, "_googlecast"

    iget-object v1, p1, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/c/at;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "wrong service name: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p1, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/c/at;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_7
    :try_start_1
    new-instance v0, Lcom/google/android/gms/cast/c/aj;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/cast/c/aj;-><init>(Lcom/google/android/gms/cast/c/af;Lcom/google/android/gms/cast/c/aq;)V

    iget-object v3, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_8
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "response is still not complete."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/aj;->a()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_b
    iget-object v0, p1, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/au;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "no text strings in response"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_c
    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, -0x1

    const/4 v0, 0x0

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v6, v5

    move-object v5, v4

    move-object v4, v3

    move-object v3, v2

    move v2, v1

    move v1, v0

    :cond_d
    :goto_3
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/16 v8, 0x3d

    invoke-virtual {v0, v8}, Ljava/lang/String;->indexOf(I)I

    move-result v8

    if-lez v8, :cond_d

    const/4 v9, 0x0

    invoke-virtual {v0, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v9

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v0, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    const-string v8, "id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_e

    move-object v6, v0

    goto :goto_3

    :cond_e
    const-string v8, "md"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_f

    const-string v5, "(Eureka|Chromekey)( Dongle)?"

    const-string v8, "Chromecast"

    invoke-virtual {v0, v5, v8}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v5, v0

    goto :goto_3

    :cond_f
    const-string v8, "ve"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_10

    move-object v4, v0

    goto :goto_3

    :cond_10
    const-string v8, "ic"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_11

    move-object v3, v0

    goto :goto_3

    :cond_11
    const-string v8, "ca"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_12

    :try_start_4
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_0

    move-result v0

    move v1, v0

    goto :goto_3

    :cond_12
    const-string v8, "st"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_d

    :try_start_5
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_1

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, -0x1

    move v2, v0

    goto :goto_3

    :pswitch_0
    const/4 v0, 0x0

    move v2, v0

    goto :goto_3

    :pswitch_1
    const/4 v0, 0x1

    move v2, v0

    goto :goto_3

    :cond_13
    if-eqz v6, :cond_0

    if-nez v5, :cond_14

    iget-object v5, p0, Lcom/google/android/gms/cast/c/af;->f:Ljava/lang/String;

    :cond_14
    const/4 v7, 0x0

    iget-object v9, p0, Lcom/google/android/gms/cast/c/af;->e:Ljava/util/Map;

    monitor-enter v9

    const/4 v0, 0x0

    :try_start_6
    iget-object v8, p1, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    if-eqz v8, :cond_20

    iget-object v0, p1, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/ak;->a:Ljava/net/Inet4Address;

    move-object v8, v0

    :goto_4
    if-nez v8, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->e:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/y;

    if-eqz v0, :cond_15

    iget-object v1, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "No address. Removing: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_15
    monitor-exit v9
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto/16 :goto_0

    :catchall_2
    move-exception v0

    monitor-exit v9

    throw v0

    :cond_16
    :try_start_7
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    if-eqz v3, :cond_18

    invoke-virtual {v8}, Ljava/net/Inet4Address;->toString()Ljava/lang/String;

    move-result-object v0

    const/16 v11, 0x2f

    invoke-virtual {v0, v11}, Ljava/lang/String;->indexOf(I)I

    move-result v11

    if-ltz v11, :cond_17

    add-int/lit8 v11, v11, 0x1

    invoke-virtual {v0, v11}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_17
    const-string v11, "http://%s:8008%s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v0, v12, v13

    const/4 v0, 0x1

    aput-object v3, v12, v0

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/common/images/WebImage;

    invoke-direct {v3, v0}, Lcom/google/android/gms/common/images/WebImage;-><init>(Landroid/net/Uri;)V

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_18
    invoke-static {v6, v8}, Lcom/google/android/gms/cast/CastDevice;->a(Ljava/lang/String;Ljava/net/Inet4Address;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v3, p1, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/c/at;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cast/c;->a(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/c;->b(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/cast/c;->c(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v3, p1, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    iget v3, v3, Lcom/google/android/gms/cast/c/at;->a:I

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cast/c;->a(I)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/c;->b(I)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/android/gms/cast/c;->a(Ljava/util/List;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c;->c(I)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->e:Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/y;

    if-eqz v0, :cond_1f

    iget-boolean v2, v0, Lcom/google/android/gms/cast/c/y;->d:Z

    if-nez v2, :cond_1e

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/cast/c/y;->b:J

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/net/Inet4Address;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v2

    iget-object v3, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v3

    if-ne v2, v3, :cond_19

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->i()I

    move-result v2

    iget-object v3, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->i()I

    move-result v3

    if-eq v2, v3, :cond_1c

    :cond_19
    iget-object v0, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    :cond_1a
    :goto_5
    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->e:Ljava/util/Map;

    new-instance v3, Lcom/google/android/gms/cast/c/y;

    iget-wide v4, p1, Lcom/google/android/gms/cast/c/aq;->f:J

    invoke-direct {v3, v1, v4, v5}, Lcom/google/android/gms/cast/c/y;-><init>(Lcom/google/android/gms/cast/CastDevice;J)V

    invoke-interface {v2, v6, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v9
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    if-eqz v0, :cond_1b

    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "notifyDeviceOffline: because it has changed; newDevice=%s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/af;->d(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_1b
    if-eqz v1, :cond_0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/c/af;->b(Lcom/google/android/gms/cast/CastDevice;)V

    goto/16 :goto_0

    :cond_1c
    :try_start_8
    iget-object v2, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/CastDevice;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    iput-object v1, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/c/af;->c(Lcom/google/android/gms/cast/CastDevice;)V

    monitor-exit v9
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto/16 :goto_0

    :cond_1d
    monitor-exit v9

    goto/16 :goto_0

    :cond_1e
    :try_start_9
    iget-object v0, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->e:Ljava/util/Map;

    invoke-interface {v2, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_1a

    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Removing: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    goto :goto_5

    :catch_0
    move-exception v0

    goto/16 :goto_3

    :catch_1
    move-exception v0

    goto/16 :goto_3

    :cond_1f
    move-object v0, v7

    goto :goto_5

    :cond_20
    move-object v8, v0

    goto/16 :goto_4

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/af;[Ljava/lang/String;[I)V
    .locals 5

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/ab;

    :try_start_0
    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/cast/c/ab;->a([Ljava/lang/String;[I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "can\'t send resolve query"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c()[I
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/cast/c/af;->i:[I

    return-object v0
.end method

.method static synthetic d()[I
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/cast/c/af;->j:[I

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/ab;

    .line 119
    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/ab;->a()V

    goto :goto_0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 124
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/c/af;->n:Z

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->m:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    .line 128
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/af;->m:Ljava/lang/Thread;

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    monitor-enter v1

    .line 138
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 139
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 131
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 157
    return-void
.end method

.method protected final a(Ljava/util/List;Z)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "startScanInternal; passive scan? %b"

    new-array v2, v8, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 83
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "No network interfaces to scan on!"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    :goto_0
    return-void

    .line 88
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 89
    new-instance v2, Lcom/google/android/gms/cast/c/ag;

    sget-object v3, Lcom/google/android/gms/cast/c/ae;->b:[Ljava/lang/String;

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/gms/cast/c/ag;-><init>(Lcom/google/android/gms/cast/c/af;[Ljava/lang/String;Ljava/net/NetworkInterface;)V

    .line 97
    :try_start_0
    invoke-virtual {v2, p2}, Lcom/google/android/gms/cast/c/ab;->a(Z)V

    .line 98
    iget-object v3, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "added mdns client; count is now %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 101
    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Couldn\'t start MDNS client for %s"

    new-array v4, v8, [Ljava/lang/Object;

    aput-object v0, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 105
    :cond_1
    iput-boolean v7, p0, Lcom/google/android/gms/cast/c/af;->n:Z

    .line 106
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/gms/cast/c/ah;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/c/ah;-><init>(Lcom/google/android/gms/cast/c/af;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/af;->m:Ljava/lang/Thread;

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto :goto_0
.end method

.method protected final a(Z)V
    .locals 6

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/cast/c/af;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/ab;

    .line 145
    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/ab;->a()V

    .line 147
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/c/ab;->a(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 149
    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/google/android/gms/cast/c/af;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Couldn\'t start MDNS client for %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/ab;->b()Ljava/net/NetworkInterface;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 152
    :cond_0
    return-void
.end method

.method protected final a(Ljava/net/NetworkInterface;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 60
    :try_start_0
    invoke-virtual {p1}, Ljava/net/NetworkInterface;->isLoopback()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/net/NetworkInterface;->isPointToPoint()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljava/net/NetworkInterface;->supportsMulticast()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 75
    :goto_0
    return v0

    .line 68
    :cond_1
    invoke-virtual {p1}, Ljava/net/NetworkInterface;->getInterfaceAddresses()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/InterfaceAddress;

    .line 69
    invoke-virtual {v0}, Ljava/net/InterfaceAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v0

    instance-of v0, v0, Ljava/net/Inet4Address;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_2

    .line 70
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_3
    move v0, v1

    .line 75
    goto :goto_0
.end method
