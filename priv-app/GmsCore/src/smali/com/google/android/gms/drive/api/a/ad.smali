.class public final Lcom/google/android/gms/drive/api/a/ad;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/QueryRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/QueryRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x10

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/ad;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ad;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    const-string v1, "Invalid query request: no request"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ad;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/QueryRequest;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v0

    const-string v1, "Invalid query request: no query"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/ad;->e:Lcom/google/android/gms/drive/internal/QueryRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/QueryRequest;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v0

    .line 42
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/ad;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 43
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/ad;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v2, v0}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/query/Query;)Z

    move-result v2

    .line 44
    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/ad;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v3, v0}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/drive/c/a;

    .line 46
    new-instance v0, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;-><init>(Lcom/google/android/gms/common/data/DataHolder;Z)V

    .line 49
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/ad;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v2, v0}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnListEntriesResponse;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 58
    :cond_0
    return-void

    .line 54
    :catchall_0
    move-exception v0

    .line 55
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method
