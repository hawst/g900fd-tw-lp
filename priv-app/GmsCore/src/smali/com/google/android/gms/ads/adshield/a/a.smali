.class public final Lcom/google/android/gms/ads/adshield/a/a;
.super Lcom/google/android/gms/ads/adshield/a/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/a/p;

.field private final b:Lcom/google/android/a/w;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/ads/adshield/a/c;-><init>()V

    .line 24
    invoke-static {p1, p2}, Lcom/google/android/a/t;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/a/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/adshield/a/a;->a:Lcom/google/android/a/p;

    .line 25
    new-instance v0, Lcom/google/android/a/w;

    iget-object v1, p0, Lcom/google/android/gms/ads/adshield/a/a;->a:Lcom/google/android/a/p;

    invoke-direct {v0, v1}, Lcom/google/android/a/w;-><init>(Lcom/google/android/a/p;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/adshield/a/a;->b:Lcom/google/android/a/w;

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/b/l;)Lcom/google/android/gms/b/l;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 59
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 60
    invoke-static {p2}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    .line 61
    iget-object v3, p0, Lcom/google/android/gms/ads/adshield/a/a;->b:Lcom/google/android/a/w;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v1, v4, v5}, Lcom/google/android/a/w;->a(Landroid/net/Uri;Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    .line 64
    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;
    :try_end_0
    .catch Lcom/google/android/a/x; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    move-object v0, v2

    goto :goto_0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    const-string v0, "ms"

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/ads/adshield/a/a;->b:Lcom/google/android/a/w;

    const-string v1, ","

    invoke-virtual {p1, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/a/w;->c:[Ljava/lang/String;

    .line 53
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/ads/adshield/a/a;->b:Lcom/google/android/a/w;

    iput-object p1, v0, Lcom/google/android/a/w;->a:Ljava/lang/String;

    iput-object p2, v0, Lcom/google/android/a/w;->b:Ljava/lang/String;

    .line 36
    return-void
.end method

.method public final a(Lcom/google/android/gms/b/l;)Z
    .locals 2

    .prologue
    .line 40
    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/ads/adshield/a/a;->b:Lcom/google/android/a/w;

    invoke-virtual {v1, v0}, Lcom/google/android/a/w;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/b/l;)Z
    .locals 2

    .prologue
    .line 46
    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 47
    iget-object v1, p0, Lcom/google/android/gms/ads/adshield/a/a;->b:Lcom/google/android/a/w;

    invoke-virtual {v1, v0}, Lcom/google/android/a/w;->b(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/gms/b/l;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    invoke-static {p1}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/ads/adshield/a/a;->a:Lcom/google/android/a/p;

    invoke-interface {v1, v0}, Lcom/google/android/a/p;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
