.class public final Lcom/google/android/gms/cast/service/a/j;
.super Lcom/google/android/gms/cast/service/b;
.source "SourceFile"


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/service/b;-><init>(Lcom/google/android/gms/cast/b/e;)V

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/cast/service/a/j;->c:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/google/android/gms/cast/service/a/j;->d:Ljava/lang/String;

    .line 23
    iput-wide p4, p0, Lcom/google/android/gms/cast/service/a/j;->e:J

    .line 24
    iput-object p6, p0, Lcom/google/android/gms/cast/service/a/j;->f:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    .line 30
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/service/a/j;->b:Lcom/google/android/gms/cast/b/e;

    iget-object v2, p0, Lcom/google/android/gms/cast/service/a/j;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/cast/service/a/j;->d:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/gms/cast/service/a/j;->e:J

    iget-object v6, p0, Lcom/google/android/gms/cast/service/a/j;->f:Ljava/lang/String;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    sget-object v1, Lcom/google/android/gms/cast/service/a/j;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
