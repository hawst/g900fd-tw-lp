.class public final Lcom/google/android/gms/people/service/a/a/l;
.super Lcom/google/android/gms/people/service/a/a/c;
.source "SourceFile"


# instance fields
.field private final g:J

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;JZ)V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/a/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V

    .line 23
    iput-wide p5, p0, Lcom/google/android/gms/people/service/a/a/l;->g:J

    .line 24
    iput-boolean p7, p0, Lcom/google/android/gms/people/service/a/a/l;->h:Z

    .line 26
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "loadContactImage: cid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " thumbnail="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    :cond_0
    return-void
.end method


# virtual methods
.method protected final f()Ljava/lang/String;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[contact-id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, Lcom/google/android/gms/people/service/a/a/l;->g:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/people/service/a/a/l;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final g()[B
    .locals 4

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/a/l;->d()Landroid/content/Context;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/people/service/a/a/l;->g:J

    iget-boolean v1, p0, Lcom/google/android/gms/people/service/a/a/l;->h:Z

    invoke-static {v0, v2, v3, v1}, Lcom/google/android/gms/people/b/a;->b(Landroid/content/Context;JZ)[B

    move-result-object v0

    return-object v0
.end method
