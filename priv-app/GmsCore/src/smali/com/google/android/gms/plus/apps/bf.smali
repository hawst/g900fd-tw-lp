.class public abstract Lcom/google/android/gms/plus/apps/bf;
.super Landroid/support/v4/app/ar;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/apps/i;


# instance fields
.field private i:Landroid/support/v7/app/a;

.field protected j:Lcom/google/android/gms/plus/apps/c;

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/ar;-><init>()V

    return-void
.end method


# virtual methods
.method abstract a(Landroid/support/v7/app/a;)V
.end method

.method final b(Landroid/support/v7/app/a;)V
    .locals 1

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bf;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->a(Landroid/support/v7/app/a;)V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/bf;->k:Z

    .line 94
    :goto_0
    return-void

    .line 91
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/bf;->k:Z

    .line 92
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/bf;->i:Landroid/support/v7/app/a;

    goto :goto_0
.end method

.method protected abstract c()I
.end method

.method protected abstract d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
.end method

.method protected final i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 74
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bf;->a(Ljava/lang/CharSequence;)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bf;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 78
    const/high16 v1, 0x41900000    # 18.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextSize(F)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bf;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->bF:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 80
    invoke-virtual {v0, v3, v1, v3, v1}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 81
    const/16 v1, 0x30

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 82
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 83
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 84
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 30
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onActivityCreated(Landroid/os/Bundle;)V

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bf;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "apps_util"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/c;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bf;->j:Lcom/google/android/gms/plus/apps/c;

    .line 33
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bf;->j:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/apps/c;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bf;->j:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/apps/c;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 62
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/support/v4/app/ar;->onResume()V

    .line 45
    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/bf;->k:Z

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bf;->i:Landroid/support/v7/app/a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/bf;->a(Landroid/support/v7/app/a;)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/bf;->i:Landroid/support/v7/app/a;

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/bf;->k:Z

    .line 50
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStart()V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bf;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/c;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/bf;->V_()V

    .line 40
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 54
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStop()V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/bf;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/c;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 56
    return-void
.end method
