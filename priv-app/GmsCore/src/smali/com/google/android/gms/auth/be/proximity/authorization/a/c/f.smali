.class public final Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/auth/be/proximity/a/c;

.field private final b:Landroid/app/KeyguardManager;

.field private final c:Lcom/google/android/gms/auth/trustagent/at;

.field private final d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

.field private final e:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;

.field private final f:Lcom/google/android/gms/auth/g/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/a/c;Lcom/google/android/gms/auth/trustagent/at;Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;)V
    .locals 7

    .prologue
    .line 529
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/KeyguardManager;

    new-instance v5, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;

    invoke-direct {v5}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;-><init>()V

    new-instance v6, Lcom/google/android/gms/auth/g/a;

    invoke-direct {v6}, Lcom/google/android/gms/auth/g/a;-><init>()V

    move-object v0, p0

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;-><init>(Lcom/google/android/gms/auth/be/proximity/a/c;Landroid/app/KeyguardManager;Lcom/google/android/gms/auth/trustagent/at;Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;Lcom/google/android/gms/auth/g/a;)V

    .line 536
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/auth/be/proximity/a/c;Landroid/app/KeyguardManager;Lcom/google/android/gms/auth/trustagent/at;Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;Lcom/google/android/gms/auth/g/a;)V
    .locals 1

    .prologue
    .line 545
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 546
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/a/c;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->a:Lcom/google/android/gms/auth/be/proximity/a/c;

    .line 547
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->b:Landroid/app/KeyguardManager;

    .line 548
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/at;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->c:Lcom/google/android/gms/auth/trustagent/at;

    .line 549
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    .line 550
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;

    .line 551
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/g/a;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->f:Lcom/google/android/gms/auth/g/a;

    .line 552
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/ab/b/a/d/a;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 556
    .line 558
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->c:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/at;->b()Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 565
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->c:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/trustagent/at;->c()Z
    :try_end_1
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 571
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->a()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    move v4, v3

    .line 573
    :goto_2
    if-nez v4, :cond_0

    if-eqz v2, :cond_3

    .line 579
    :cond_0
    :goto_3
    const/16 v2, 0xc

    .line 580
    iget-object v3, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->f:Lcom/google/android/gms/auth/g/a;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-lt v3, v4, :cond_1

    .line 581
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->b:Landroid/app/KeyguardManager;

    invoke-virtual {v2}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v2

    if-eqz v2, :cond_4

    const/16 v2, 0xa

    .line 586
    :cond_1
    :goto_4
    const/16 v3, 0x16

    .line 587
    iget-object v4, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->c:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v4}, Lcom/google/android/gms/auth/trustagent/at;->a()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 588
    if-eqz v0, :cond_5

    const/16 v0, 0x14

    .line 594
    :goto_5
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->a:Lcom/google/android/gms/auth/be/proximity/a/c;

    new-instance v4, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;

    iget-object v5, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/n;

    new-instance v5, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/m;

    const/4 v6, 0x0

    invoke-direct {v5, v1, v2, v0, v6}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/m;-><init>(IIIB)V

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/m;->a()[B

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/ab/b/a/d/a;->c([B)[B

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;-><init>([B)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/auth/be/proximity/a/c;->a(Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    .line 601
    return-void

    .line 559
    :catch_0
    move-exception v0

    .line 560
    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a()Ljava/lang/String;

    move-result-object v2

    const-string v4, "Cannot get coffee enabled state."

    invoke-static {v2, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_0

    .line 566
    :catch_1
    move-exception v2

    .line 567
    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Cannot get coffee trusted state."

    invoke-static {v4, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v2, v1

    goto :goto_1

    :cond_2
    move v4, v1

    .line 571
    goto :goto_2

    :cond_3
    move v1, v3

    .line 576
    goto :goto_3

    .line 581
    :cond_4
    const/16 v2, 0xb

    goto :goto_4

    .line 588
    :cond_5
    const/16 v0, 0x15

    goto :goto_5

    .line 599
    :catch_2
    move-exception v0

    .line 600
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Failed to encode json."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_6
    move v0, v3

    goto :goto_5
.end method
