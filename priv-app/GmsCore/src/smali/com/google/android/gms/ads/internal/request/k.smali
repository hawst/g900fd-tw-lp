.class public final Lcom/google/android/gms/ads/internal/request/k;
.super Lcom/google/android/gms/ads/internal/util/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/p/e;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/ads/internal/request/j;

.field final b:Lcom/google/android/gms/ads/internal/p/a;

.field final c:Ljava/lang/Object;

.field d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

.field private final f:Lcom/google/android/gms/ads/internal/k/a/a;

.field private final g:Ljava/lang/Object;

.field private final h:Landroid/content/Context;

.field private final i:Lcom/google/android/gms/ads/internal/o/b;

.field private j:Z

.field private k:Lcom/google/android/gms/ads/internal/k/a;

.field private l:Lcom/google/android/gms/ads/internal/k/d;

.field private m:Lcom/google/android/gms/ads/internal/k/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/p/a;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/request/j;)V
    .locals 1

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/util/a;-><init>()V

    .line 54
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->g:Ljava/lang/Object;

    .line 56
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->c:Ljava/lang/Object;

    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/k;->j:Z

    .line 71
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/k;->h:Landroid/content/Context;

    .line 72
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/k;->i:Lcom/google/android/gms/ads/internal/o/b;

    .line 73
    iget-object v0, p2, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    .line 74
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    .line 75
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/request/k;->f:Lcom/google/android/gms/ads/internal/k/a/a;

    .line 76
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/request/k;->a:Lcom/google/android/gms/ads/internal/request/j;

    .line 77
    iget-object v0, p2, Lcom/google/android/gms/ads/internal/o/b;->c:Lcom/google/android/gms/ads/internal/k/d;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->l:Lcom/google/android/gms/ads/internal/k/d;

    .line 78
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 336
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/ads/internal/request/k;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 337
    new-instance v0, Lcom/google/android/gms/ads/internal/request/p;

    const-string v1, "Timed out waiting for WebView to finish loading."

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/ads/internal/request/p;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 344
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/k;->j:Z

    if-eqz v0, :cond_0

    .line 346
    return-void
.end method

.method private b(J)Z
    .locals 5

    .prologue
    .line 356
    const-wide/32 v0, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    sub-long/2addr v0, v2

    .line 357
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 358
    const/4 v0, 0x0

    .line 368
    :goto_0
    return v0

    .line 362
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/k;->c:Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 368
    const/4 v0, 0x1

    goto :goto_0

    .line 364
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/ads/internal/request/p;

    const-string v1, "Ad request cancelled."

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/ads/internal/request/p;-><init>(Ljava/lang/String;I)V

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 32

    .prologue
    .line 93
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->c:Ljava/lang/Object;

    move-object/from16 v31, v0

    monitor-enter v31

    .line 94
    :try_start_0
    const-string v2, "AdRendererBackgroundTask started."

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 95
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->i:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v12, v2, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    .line 96
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->i:Lcom/google/android/gms/ads/internal/o/b;

    iget v8, v2, Lcom/google/android/gms/ads/internal/o/b;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 99
    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->h:Z

    if-eqz v2, :cond_1

    .line 105
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/k;->g:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catch Lcom/google/android/gms/ads/internal/request/p; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v2, Lcom/google/android/gms/ads/internal/k/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/request/k;->h:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/request/k;->f:Lcom/google/android/gms/ads/internal/k/a/a;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/ads/internal/request/k;->l:Lcom/google/android/gms/ads/internal/k/d;

    invoke-direct {v2, v4, v12, v5, v6}, Lcom/google/android/gms/ads/internal/k/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/k/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->k:Lcom/google/android/gms/ads/internal/k/a;

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->k:Lcom/google/android/gms/ads/internal/k/a;

    invoke-virtual {v2, v10, v11}, Lcom/google/android/gms/ads/internal/k/a;->a(J)Lcom/google/android/gms/ads/internal/k/i;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    iget v2, v2, Lcom/google/android/gms/ads/internal/k/i;->a:I

    packed-switch v2, :pswitch_data_0

    new-instance v2, Lcom/google/android/gms/ads/internal/request/p;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected mediation result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    iget v4, v4, Lcom/google/android/gms/ads/internal/k/i;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/ads/internal/request/p;-><init>(Ljava/lang/String;I)V

    throw v2
    :try_end_3
    .catch Lcom/google/android/gms/ads/internal/request/p; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 113
    :catch_0
    move-exception v2

    .line 115
    :try_start_4
    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/request/p;->a()I

    move-result v6

    .line 116
    const/4 v3, 0x3

    if-eq v6, v3, :cond_0

    const/4 v3, -0x1

    if-ne v6, v3, :cond_6

    .line 118
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/request/p;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 125
    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    if-nez v2, :cond_7

    .line 126
    new-instance v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    invoke-direct {v2, v6}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    .line 132
    :goto_1
    sget-object v2, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/ads/internal/request/l;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/gms/ads/internal/request/l;-><init>(Lcom/google/android/gms/ads/internal/request/k;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 141
    :goto_2
    new-instance v2, Lcom/google/android/gms/ads/internal/o/a;

    iget-object v3, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v5, v5, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v7, v7, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->f:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v8, v8, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v9, v9, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->l:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v10, v10, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->k:J

    iget-object v12, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->i:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v13, v13, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->h:Z

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    if-eqz v14, :cond_8

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    iget-object v14, v14, Lcom/google/android/gms/ads/internal/k/i;->b:Lcom/google/android/gms/ads/internal/k/c;

    :goto_3
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    if-eqz v15, :cond_9

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    iget-object v15, v15, Lcom/google/android/gms/ads/internal/k/i;->c:Lcom/google/android/gms/ads/internal/k/a/d;

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    move-object/from16 v16, v0

    if-eqz v16, :cond_a

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/i;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->l:Lcom/google/android/gms/ads/internal/k/d;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    move-object/from16 v18, v0

    if-eqz v18, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->m:Lcom/google/android/gms/ads/internal/k/i;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/i;->e:Lcom/google/android/gms/ads/internal/k/f;

    move-object/from16 v18, v0

    :goto_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->i:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->i:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->i:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/o/b;->f:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->n:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->o:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->i:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->h:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/ads/internal/o/a;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/p/a;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/k/a/d;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/k/f;JLcom/google/android/gms/ads/internal/client/AdSizeParcel;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/ads/internal/f/d;)V

    .line 169
    sget-object v3, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/ads/internal/request/m;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/gms/ads/internal/request/m;-><init>(Lcom/google/android/gms/ads/internal/request/k;Lcom/google/android/gms/ads/internal/o/a;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 177
    monitor-exit v31
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void

    .line 105
    :catchall_0
    move-exception v2

    :try_start_5
    monitor-exit v3

    throw v2
    :try_end_5
    .catch Lcom/google/android/gms/ads/internal/request/p; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 177
    :catchall_1
    move-exception v2

    monitor-exit v31

    throw v2

    :pswitch_0
    move v6, v8

    .line 105
    goto/16 :goto_2

    :pswitch_1
    :try_start_6
    new-instance v2, Lcom/google/android/gms/ads/internal/request/p;

    const-string v3, "No fill from any mediation ad networks."

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/ads/internal/request/p;-><init>(Ljava/lang/String;I)V

    throw v2

    .line 106
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-boolean v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->p:Z

    if-eqz v2, :cond_5

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/p/a;->e()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-result-object v2

    iget-boolean v3, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->h:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v6, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    :goto_7
    new-instance v2, Lcom/google/android/gms/ads/internal/request/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    const/4 v7, 0x0

    move-object/from16 v3, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/ads/internal/request/g;-><init>(Lcom/google/android/gms/ads/internal/p/e;Lcom/google/android/gms/ads/internal/p/a;IIB)V

    sget-object v3, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/ads/internal/request/o;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/gms/ads/internal/request/o;-><init>(Lcom/google/android/gms/ads/internal/request/k;Lcom/google/android/gms/ads/internal/request/g;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/google/android/gms/ads/internal/request/k;->a(J)V

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/request/g;->c()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v2, "Ad-Network indicated no fill with passback URL."

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/ads/internal/request/p;

    const-string v3, "AdNetwork sent passback url"

    const/4 v4, 0x3

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/ads/internal/request/p;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_2
    iget v5, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    iget v6, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    goto :goto_7

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/request/g;->d()Z

    move-result v2

    if-nez v2, :cond_4

    new-instance v2, Lcom/google/android/gms/ads/internal/request/p;

    const-string v3, "AdNetwork timed out"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/ads/internal/request/p;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_4
    move v6, v8

    goto/16 :goto_2

    .line 111
    :cond_5
    sget-object v2, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/gms/ads/internal/request/n;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/gms/ads/internal/request/n;-><init>(Lcom/google/android/gms/ads/internal/request/k;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v11}, Lcom/google/android/gms/ads/internal/request/k;->a(J)V
    :try_end_6
    .catch Lcom/google/android/gms/ads/internal/request/p; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move v6, v8

    .line 138
    goto/16 :goto_2

    .line 120
    :cond_6
    :try_start_7
    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/request/p;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 128
    :cond_7
    new-instance v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v4, v3, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->k:J

    invoke-direct {v2, v6, v4, v5}, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;-><init>(IJ)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    goto/16 :goto_1

    .line 141
    :cond_8
    const/4 v14, 0x0

    goto/16 :goto_3

    :cond_9
    const/4 v15, 0x0

    goto/16 :goto_4

    :cond_a
    const-class v16, Lcom/google/a/a/a/a;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Class;->getName()Ljava/lang/String;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v16

    goto/16 :goto_5

    :cond_b
    const/16 v18, 0x0

    goto/16 :goto_6

    .line 105
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/ads/internal/p/a;)V
    .locals 2

    .prologue
    .line 83
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/k;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 84
    :try_start_0
    const-string v0, "WebView finished loading."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 85
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/k;->j:Z

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 87
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 317
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/k;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 319
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->stopLoading()V

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/webkit/WebView;)V

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->k:Lcom/google/android/gms/ads/internal/k/a;

    if-eqz v0, :cond_2

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/k;->k:Lcom/google/android/gms/ads/internal/k/a;

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/k/a;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    const/4 v3, 0x1

    :try_start_1
    iput-boolean v3, v0, Lcom/google/android/gms/ads/internal/k/a;->b:Z

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/k/a;->c:Lcom/google/android/gms/ads/internal/k/g;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/k/a;->c:Lcom/google/android/gms/ads/internal/k/g;

    iget-object v4, v3, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    monitor-enter v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, v3, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/ads/internal/k/g;->g:Lcom/google/android/gms/ads/internal/k/a/d;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/a/d;->c()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :try_start_3
    iput v0, v3, Lcom/google/android/gms/ads/internal/k/g;->h:I

    iget-object v0, v3, Lcom/google/android/gms/ads/internal/k/g;->e:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_1
    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 326
    :cond_2
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    return-void

    .line 324
    :catch_0
    move-exception v0

    :try_start_6
    const-string v5, "Could not destroy mediation adapter."

    invoke-static {v5, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_7
    monitor-exit v4

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v2

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 326
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0
.end method
