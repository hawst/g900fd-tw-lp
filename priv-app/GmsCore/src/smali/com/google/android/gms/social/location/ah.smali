.class public final Lcom/google/android/gms/social/location/ah;
.super Lcom/google/android/gms/social/location/ad;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/social/location/ad;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method private static a(Lcom/google/android/gms/social/location/af;Lcom/google/c/f/b/f;)Lcom/google/ac/e/a/a/f;
    .locals 3

    .prologue
    .line 112
    new-instance v1, Lcom/google/ac/e/a/a/f;

    invoke-direct {v1}, Lcom/google/ac/e/a/a/f;-><init>()V

    .line 113
    new-instance v0, Lcom/google/ac/e/a/a/e;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/e;-><init>()V

    iput-object v0, v1, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    .line 114
    iget-object v2, v1, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    sget-object v0, Lcom/google/android/gms/social/location/af;->a:Lcom/google/android/gms/social/location/af;

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lcom/google/ac/e/a/a/e;->a:Ljava/lang/Integer;

    .line 117
    iget-object v0, v1, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    iput-object p1, v0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    .line 118
    return-object v1

    .line 114
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Lcom/google/c/f/b/f;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 130
    invoke-static {p0}, Lcom/google/android/gms/common/people/data/c;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    .line 131
    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->c(Ljava/lang/String;)[B

    move-result-object v0

    .line 132
    invoke-static {v0}, Lcom/google/c/f/b/f;->a([B)Lcom/google/c/f/b/f;

    move-result-object v2

    .line 133
    if-nez v2, :cond_0

    .line 134
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Given list of AudienceMembers cannot be converted into RenderedSharingRosters."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 141
    :cond_0
    new-instance v3, Lcom/google/c/f/b/g;

    invoke-direct {v3}, Lcom/google/c/f/b/g;-><init>()V

    .line 142
    new-instance v0, Lcom/google/c/f/b/k;

    invoke-direct {v0}, Lcom/google/c/f/b/k;-><init>()V

    iput-object v0, v3, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    .line 143
    iget-object v0, v2, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    .line 144
    array-length v4, v0

    .line 146
    iget-object v0, v3, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    new-array v5, v4, [Lcom/google/c/f/b/m;

    iput-object v5, v0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    .line 147
    iget-object v0, v3, Lcom/google/c/f/b/g;->b:Lcom/google/c/f/b/k;

    iget-object v5, v0, Lcom/google/c/f/b/k;->a:[Lcom/google/c/f/b/m;

    move v0, v1

    .line 148
    :goto_0
    if-ge v0, v4, :cond_1

    .line 149
    iget-object v6, v2, Lcom/google/c/f/b/f;->c:[Lcom/google/c/f/b/l;

    aget-object v6, v6, v0

    iget-object v6, v6, Lcom/google/c/f/b/l;->a:Lcom/google/c/f/b/m;

    aput-object v6, v5, v0

    .line 148
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 152
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/c/f/b/g;

    aput-object v3, v0, v1

    iput-object v0, v2, Lcom/google/c/f/b/f;->a:[Lcom/google/c/f/b/g;

    .line 153
    return-object v2
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/social/location/DeviceLocationSettings;)Z
    .locals 8

    .prologue
    const/4 v0, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 57
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/social/location/ah;->a(Ljava/util/List;)Lcom/google/c/f/b/f;

    move-result-object v3

    .line 59
    invoke-virtual {p1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/social/location/ah;->a(Ljava/util/List;)Lcom/google/c/f/b/f;

    move-result-object v4

    .line 61
    new-instance v5, Lcom/google/ac/e/a/a/j;

    invoke-direct {v5}, Lcom/google/ac/e/a/a/j;-><init>()V

    .line 62
    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    iput-object v6, v5, Lcom/google/ac/e/a/a/j;->a:Ljava/lang/Integer;

    .line 63
    new-instance v6, Lcom/google/ac/e/a/a/d;

    invoke-direct {v6}, Lcom/google/ac/e/a/a/d;-><init>()V

    iput-object v6, v5, Lcom/google/ac/e/a/a/j;->b:Lcom/google/ac/e/a/a/d;

    .line 64
    iget-object v6, v5, Lcom/google/ac/e/a/a/j;->b:Lcom/google/ac/e/a/a/d;

    new-instance v7, Lcom/google/ac/e/a/a/c;

    invoke-direct {v7}, Lcom/google/ac/e/a/a/c;-><init>()V

    iput-object v7, v6, Lcom/google/ac/e/a/a/d;->a:Lcom/google/ac/e/a/a/c;

    .line 65
    iget-object v6, v5, Lcom/google/ac/e/a/a/j;->b:Lcom/google/ac/e/a/a/d;

    iget-object v6, v6, Lcom/google/ac/e/a/a/d;->a:Lcom/google/ac/e/a/a/c;

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/DeviceLocationSettings;->b()Ljava/lang/Boolean;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Lcom/google/ac/e/a/a/c;->a:Ljava/lang/Integer;

    .line 68
    sget-object v0, Lcom/google/android/gms/social/location/af;->a:Lcom/google/android/gms/social/location/af;

    invoke-static {v0, v3}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/android/gms/social/location/af;Lcom/google/c/f/b/f;)Lcom/google/ac/e/a/a/f;

    move-result-object v0

    .line 70
    sget-object v3, Lcom/google/android/gms/social/location/af;->b:Lcom/google/android/gms/social/location/af;

    invoke-static {v3, v4}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/android/gms/social/location/af;Lcom/google/c/f/b/f;)Lcom/google/ac/e/a/a/f;

    move-result-object v3

    .line 73
    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/ac/e/a/a/f;

    const/4 v6, 0x0

    aput-object v0, v4, v6

    const/4 v0, 0x1

    aput-object v3, v4, v0

    iput-object v4, v5, Lcom/google/ac/e/a/a/j;->c:[Lcom/google/ac/e/a/a/f;

    .line 76
    new-instance v0, Lcom/google/ac/e/a/a/k;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/k;-><init>()V

    const-string v3, "mutateacl"

    invoke-virtual {p0, v5, v0, v3}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;Ljava/lang/String;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/e/a/a/k;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    if-eqz v0, :cond_1

    move v0, v1

    .line 80
    :goto_0
    return v0

    :cond_1
    move v0, v2

    .line 77
    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    const-string v1, "SaveLocationSharingSettingsOperation"

    const-string v3, "Error converting AudienceMember array into RenderedSharingRosters."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 80
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/social/location/af;Lcom/google/android/gms/common/people/data/Audience;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 89
    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/social/location/ah;->a(Ljava/util/List;)Lcom/google/c/f/b/f;

    move-result-object v3

    .line 91
    new-instance v4, Lcom/google/ac/e/a/a/j;

    invoke-direct {v4}, Lcom/google/ac/e/a/a/j;-><init>()V

    .line 92
    sget-object v0, Lcom/google/android/gms/social/location/af;->a:Lcom/google/android/gms/social/location/af;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x3

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/ac/e/a/a/j;->a:Ljava/lang/Integer;

    .line 93
    new-instance v0, Lcom/google/ac/e/a/a/d;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/d;-><init>()V

    iput-object v0, v4, Lcom/google/ac/e/a/a/j;->b:Lcom/google/ac/e/a/a/d;

    .line 94
    iget-object v0, v4, Lcom/google/ac/e/a/a/j;->b:Lcom/google/ac/e/a/a/d;

    new-instance v5, Lcom/google/ac/e/a/a/c;

    invoke-direct {v5}, Lcom/google/ac/e/a/a/c;-><init>()V

    iput-object v5, v0, Lcom/google/ac/e/a/a/d;->a:Lcom/google/ac/e/a/a/c;

    .line 95
    iget-object v0, v4, Lcom/google/ac/e/a/a/j;->b:Lcom/google/ac/e/a/a/d;

    iget-object v0, v0, Lcom/google/ac/e/a/a/d;->a:Lcom/google/ac/e/a/a/c;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    iput-object v5, v0, Lcom/google/ac/e/a/a/c;->a:Ljava/lang/Integer;

    .line 97
    invoke-static {p1, v3}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/android/gms/social/location/af;Lcom/google/c/f/b/f;)Lcom/google/ac/e/a/a/f;

    move-result-object v0

    .line 98
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/ac/e/a/a/f;

    const/4 v5, 0x0

    aput-object v0, v3, v5

    iput-object v3, v4, Lcom/google/ac/e/a/a/j;->c:[Lcom/google/ac/e/a/a/f;

    .line 99
    new-instance v0, Lcom/google/ac/e/a/a/k;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/k;-><init>()V

    const-string v3, "mutateacl"

    invoke-virtual {p0, v4, v0, v3}, Lcom/google/android/gms/social/location/ah;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;Ljava/lang/String;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/e/a/a/k;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    if-eqz v0, :cond_1

    move v0, v1

    .line 103
    :goto_1
    return v0

    .line 92
    :cond_0
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    move v0, v2

    .line 100
    goto :goto_1

    .line 101
    :catch_0
    move-exception v0

    .line 102
    const-string v1, "SaveLocationSharingSettingsOperation"

    const-string v3, "Error converting AudienceMember array into RenderedSharingRosters."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 103
    goto :goto_1
.end method
