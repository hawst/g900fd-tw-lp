.class final Lcom/google/android/gms/fitness/service/aw;
.super Lcom/google/android/gms/fitness/f/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/c/c;

.field final synthetic b:Lcom/google/android/gms/fitness/data/l;

.field final synthetic c:Lcom/google/android/gms/fitness/service/av;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/service/av;Lcom/google/android/gms/fitness/c/c;Lcom/google/android/gms/fitness/data/l;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/aw;->c:Lcom/google/android/gms/fitness/service/av;

    iput-object p2, p0, Lcom/google/android/gms/fitness/service/aw;->a:Lcom/google/android/gms/fitness/c/c;

    iput-object p3, p0, Lcom/google/android/gms/fitness/service/aw;->b:Lcom/google/android/gms/fitness/data/l;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/f/b;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/aw;->c:Lcom/google/android/gms/fitness/service/av;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/aw;->a:Lcom/google/android/gms/fitness/c/c;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/aw;->b:Lcom/google/android/gms/fitness/data/l;

    const-string v3, "Adding registration for :%s"

    new-array v4, v7, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v3, v1, Lcom/google/android/gms/fitness/c/c;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    new-instance v5, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    invoke-interface {v4, v3, v5}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, v3}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-instance v3, Lcom/google/android/gms/fitness/sensors/e/h;

    invoke-direct {v3}, Lcom/google/android/gms/fitness/sensors/e/h;-><init>()V

    iput-object v2, v3, Lcom/google/android/gms/fitness/sensors/e/h;->a:Lcom/google/android/gms/fitness/data/l;

    iget-object v1, v1, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    iput-object v1, v3, Lcom/google/android/gms/fitness/sensors/e/h;->c:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/sensors/e/h;->a()Lcom/google/android/gms/fitness/sensors/e/g;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 97
    const-string v0, "Successfully added recording listener for %s"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/aw;->a:Lcom/google/android/gms/fitness/c/c;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 98
    return-void
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 102
    const-string v0, "Failed to add recording listener for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/aw;->a:Lcom/google/android/gms/fitness/c/c;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 103
    return-void
.end method
