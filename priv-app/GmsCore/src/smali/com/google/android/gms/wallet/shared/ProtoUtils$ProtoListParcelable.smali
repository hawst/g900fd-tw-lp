.class Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:[[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 428
    new-instance v0, Lcom/google/android/gms/wallet/shared/i;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>([[B)V
    .locals 0

    .prologue
    .line 393
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 394
    iput-object p1, p0, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;->a:[[B

    .line 395
    return-void
.end method

.method synthetic constructor <init>([[BB)V
    .locals 0

    .prologue
    .line 374
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;-><init>([[B)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;->a:[[B

    if-nez v0, :cond_1

    .line 418
    const/4 v0, -0x1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 425
    :cond_0
    return-void

    .line 420
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;->a:[[B

    array-length v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 421
    iget-object v1, p0, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;->a:[[B

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 422
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 421
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
