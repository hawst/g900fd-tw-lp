.class public final Lcom/google/android/gms/games/g/t;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/g/b;

.field public c:Lcom/google/android/gms/games/g/z;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4199
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4200
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/t;->a:I

    iput-object v1, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    iput-object v1, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/t;->cachedSize:I

    .line 4201
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4272
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4273
    iget v1, p0, Lcom/google/android/gms/games/g/t;->a:I

    if-eqz v1, :cond_0

    .line 4274
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/t;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4277
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    if-eqz v1, :cond_1

    .line 4278
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4281
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    if-eqz v1, :cond_2

    .line 4282
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4285
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4213
    if-ne p1, p0, :cond_1

    .line 4241
    :cond_0
    :goto_0
    return v0

    .line 4216
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/t;

    if-nez v2, :cond_2

    move v0, v1

    .line 4217
    goto :goto_0

    .line 4219
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/t;

    .line 4220
    iget v2, p0, Lcom/google/android/gms/games/g/t;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/t;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4221
    goto :goto_0

    .line 4223
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    if-nez v2, :cond_4

    .line 4224
    iget-object v2, p1, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    if-eqz v2, :cond_5

    move v0, v1

    .line 4225
    goto :goto_0

    .line 4228
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    iget-object v3, p1, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4229
    goto :goto_0

    .line 4232
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    if-nez v2, :cond_6

    .line 4233
    iget-object v2, p1, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    if-eqz v2, :cond_0

    move v0, v1

    .line 4234
    goto :goto_0

    .line 4237
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    iget-object v3, p1, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/z;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4238
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4246
    iget v0, p0, Lcom/google/android/gms/games/g/t;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4248
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 4250
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 4252
    return v0

    .line 4248
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4250
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    invoke-virtual {v1}, Lcom/google/android/gms/games/g/z;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4168
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/t;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/b;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/g/z;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/z;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4258
    iget v0, p0, Lcom/google/android/gms/games/g/t;->a:I

    if-eqz v0, :cond_0

    .line 4259
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/t;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4261
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    if-eqz v0, :cond_1

    .line 4262
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/t;->b:Lcom/google/android/gms/games/g/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4264
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    if-eqz v0, :cond_2

    .line 4265
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/t;->c:Lcom/google/android/gms/games/g/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4267
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4268
    return-void
.end method
