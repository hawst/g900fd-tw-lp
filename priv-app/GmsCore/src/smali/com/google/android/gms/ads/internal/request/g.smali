.class public final Lcom/google/android/gms/ads/internal/request/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field protected final a:Lcom/google/android/gms/ads/internal/p/a;

.field protected b:Z

.field protected c:Z

.field private final d:Landroid/os/Handler;

.field private final e:J

.field private f:J

.field private g:Lcom/google/android/gms/ads/internal/p/e;

.field private final h:I

.field private final i:I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/ads/internal/p/e;Lcom/google/android/gms/ads/internal/p/a;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/request/g;->e:J

    .line 151
    const-wide/16 v0, 0x32

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/request/g;->f:J

    .line 152
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/g;->d:Landroid/os/Handler;

    .line 153
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/g;->a:Lcom/google/android/gms/ads/internal/p/a;

    .line 154
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/g;->g:Lcom/google/android/gms/ads/internal/p/e;

    .line 155
    iput-boolean v2, p0, Lcom/google/android/gms/ads/internal/request/g;->b:Z

    .line 156
    iput-boolean v2, p0, Lcom/google/android/gms/ads/internal/request/g;->c:Z

    .line 157
    iput p4, p0, Lcom/google/android/gms/ads/internal/request/g;->h:I

    .line 158
    iput p3, p0, Lcom/google/android/gms/ads/internal/request/g;->i:I

    .line 159
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/internal/p/e;Lcom/google/android/gms/ads/internal/p/a;IIB)V
    .locals 0

    .prologue
    .line 166
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/ads/internal/request/g;-><init>(Lcom/google/android/gms/ads/internal/p/e;Lcom/google/android/gms/ads/internal/p/a;II)V

    .line 167
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/ads/internal/request/g;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/gms/ads/internal/request/g;->i:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/ads/internal/request/g;)I
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/gms/ads/internal/request/g;->h:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/ads/internal/request/g;)J
    .locals 4

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/google/android/gms/ads/internal/request/g;->f:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/request/g;->f:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/gms/ads/internal/request/g;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/google/android/gms/ads/internal/request/g;->f:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/gms/ads/internal/request/g;)Lcom/google/android/gms/ads/internal/p/e;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/g;->g:Lcom/google/android/gms/ads/internal/p/e;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/ads/internal/request/g;)J
    .locals 2

    .prologue
    .line 31
    iget-wide v0, p0, Lcom/google/android/gms/ads/internal/request/g;->e:J

    return-wide v0
.end method

.method static synthetic g(Lcom/google/android/gms/ads/internal/request/g;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/g;->d:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/g;->d:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/gms/ads/internal/request/g;->e:J

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 183
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/request/AdResponseParcel;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 203
    new-instance v0, Lcom/google/android/gms/ads/internal/p/p;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/g;->a:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v2, p1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->q:Ljava/lang/String;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/ads/internal/p/p;-><init>(Lcom/google/android/gms/ads/internal/request/g;Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;)V

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/g;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/p/a;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/g;->a:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, v5

    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->c:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/p/a;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    return-void

    .line 205
    :cond_0
    iget-object v1, p1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 209
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/g;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    monitor-exit p0

    return-void

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Z
    .locals 1

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/g;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 217
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/request/g;->c:Z

    return v0
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/g;->a:Lcom/google/android/gms/ads/internal/p/a;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/request/g;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/g;->g:Lcom/google/android/gms/ads/internal/p/e;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/g;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/ads/internal/p/e;->a(Lcom/google/android/gms/ads/internal/p/a;)V

    .line 179
    :goto_0
    return-void

    .line 178
    :cond_1
    new-instance v0, Lcom/google/android/gms/ads/internal/request/h;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/g;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/ads/internal/request/h;-><init>(Lcom/google/android/gms/ads/internal/request/g;Landroid/webkit/WebView;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/request/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
