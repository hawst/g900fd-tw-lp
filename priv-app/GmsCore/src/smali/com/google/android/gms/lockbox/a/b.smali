.class public final Lcom/google/android/gms/lockbox/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:Lcom/google/android/gms/lockbox/a/c;

.field public e:I

.field public f:[I

.field public g:[I

.field public h:I

.field public i:[Ljava/lang/String;

.field public j:I

.field public k:J

.field public l:J

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 223
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 224
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    iput v1, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    iput v1, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    iput v1, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    iput-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    iput-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    iput-boolean v1, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/lockbox/a/b;->cachedSize:I

    .line 225
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    .line 391
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 392
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 393
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 396
    :cond_0
    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    if-eqz v1, :cond_1

    .line 397
    const/4 v1, 0x2

    iget v3, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_1
    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    if-eqz v1, :cond_2

    .line 401
    const/4 v1, 0x3

    iget v3, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v1, :cond_3

    .line 405
    const/4 v1, 0x4

    iget-object v3, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 408
    :cond_3
    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    if-eqz v1, :cond_4

    .line 409
    const/4 v1, 0x5

    iget v3, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 412
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    .line 414
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 415
    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    aget v4, v4, v1

    .line 416
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 414
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 419
    :cond_5
    add-int/2addr v0, v3

    .line 420
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 422
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    .line 424
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    array-length v4, v4

    if-ge v1, v4, :cond_7

    .line 425
    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    aget v4, v4, v1

    .line 426
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 424
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 429
    :cond_7
    add-int/2addr v0, v3

    .line 430
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 432
    :cond_8
    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    if-eqz v1, :cond_9

    .line 433
    const/16 v1, 0x8

    iget v3, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 436
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_c

    move v1, v2

    move v3, v2

    .line 439
    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_b

    .line 440
    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 441
    if-eqz v4, :cond_a

    .line 442
    add-int/lit8 v3, v3, 0x1

    .line 443
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 439
    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 447
    :cond_b
    add-int/2addr v0, v1

    .line 448
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 450
    :cond_c
    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    if-eqz v1, :cond_d

    .line 451
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 454
    :cond_d
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_e

    .line 455
    const/16 v1, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    :cond_e
    iget-boolean v1, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    if-eqz v1, :cond_f

    .line 459
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 462
    :cond_f
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    cmp-long v1, v2, v6

    if-eqz v1, :cond_10

    .line 463
    const/16 v1, 0xd

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 466
    :cond_10
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 247
    if-ne p1, p0, :cond_1

    .line 306
    :cond_0
    :goto_0
    return v0

    .line 250
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/lockbox/a/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 251
    goto :goto_0

    .line 253
    :cond_2
    check-cast p1, Lcom/google/android/gms/lockbox/a/b;

    .line 254
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 255
    iget-object v2, p1, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 256
    goto :goto_0

    .line 258
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 259
    goto :goto_0

    .line 261
    :cond_4
    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/b;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 262
    goto :goto_0

    .line 264
    :cond_5
    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/b;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 265
    goto :goto_0

    .line 267
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    if-nez v2, :cond_7

    .line 268
    iget-object v2, p1, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v2, :cond_8

    move v0, v1

    .line 269
    goto :goto_0

    .line 272
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/lockbox/a/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 273
    goto :goto_0

    .line 276
    :cond_8
    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/b;->e:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 277
    goto :goto_0

    .line 279
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/b;->f:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 281
    goto :goto_0

    .line 283
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/b;->g:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 285
    goto :goto_0

    .line 287
    :cond_b
    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/b;->h:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 288
    goto :goto_0

    .line 290
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 292
    goto :goto_0

    .line 294
    :cond_d
    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    iget v3, p1, Lcom/google/android/gms/lockbox/a/b;->j:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 295
    goto :goto_0

    .line 297
    :cond_e
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    iget-wide v4, p1, Lcom/google/android/gms/lockbox/a/b;->k:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_f

    move v0, v1

    .line 298
    goto/16 :goto_0

    .line 300
    :cond_f
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    iget-wide v4, p1, Lcom/google/android/gms/lockbox/a/b;->l:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_10

    move v0, v1

    .line 301
    goto/16 :goto_0

    .line 303
    :cond_10
    iget-boolean v2, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    iget-boolean v3, p1, Lcom/google/android/gms/lockbox/a/b;->m:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 304
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 314
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    add-int/2addr v0, v2

    .line 315
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    add-int/2addr v0, v2

    .line 316
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 318
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    add-int/2addr v0, v1

    .line 319
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    add-int/2addr v0, v1

    .line 324
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    add-int/2addr v0, v1

    .line 327
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    iget-wide v4, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 329
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    iget-wide v4, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 331
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v1

    .line 332
    return v0

    .line 311
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 316
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/lockbox/a/c;->hashCode()I

    move-result v1

    goto :goto_1

    .line 331
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 143
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    iput v0, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/lockbox/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    sparse-switch v6, :sswitch_data_2

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :sswitch_8
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    sparse-switch v4, :sswitch_data_3

    goto :goto_4

    :sswitch_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    sparse-switch v5, :sswitch_data_4

    goto :goto_6

    :sswitch_b
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    :cond_b
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x38

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    if-nez v0, :cond_d

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [I

    if-eqz v0, :cond_c

    iget-object v3, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_e

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    array-length v0, v0

    goto :goto_7

    :cond_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v1, v0

    iput-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_f

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_f
    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    if-nez v1, :cond_11

    move v1, v2

    :goto_a
    add-int/2addr v0, v1

    new-array v0, v0, [I

    if-eqz v1, :cond_10

    iget-object v4, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    invoke-static {v4, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_10
    :goto_b
    array-length v4, v0

    if-ge v1, v4, :cond_12

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    array-length v1, v1

    goto :goto_a

    :cond_12
    iput-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    goto/16 :goto_0

    :sswitch_f
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    if-nez v0, :cond_14

    move v0, v2

    :goto_c
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    if-eqz v0, :cond_13

    iget-object v3, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_13
    :goto_d
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_15

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_c

    :cond_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v0

    iput-object v1, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
        0x32 -> :sswitch_9
        0x38 -> :sswitch_c
        0x3a -> :sswitch_d
        0x40 -> :sswitch_e
        0x4a -> :sswitch_f
        0x50 -> :sswitch_10
        0x58 -> :sswitch_11
        0x60 -> :sswitch_12
        0x68 -> :sswitch_13
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x64 -> :sswitch_3
        0x82 -> :sswitch_3
        0xc8 -> :sswitch_3
        0x12c -> :sswitch_3
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_8
        0x5 -> :sswitch_8
        0xa -> :sswitch_8
        0xf -> :sswitch_8
        0x14 -> :sswitch_8
        0x28 -> :sswitch_8
        0x3c -> :sswitch_8
        0x50 -> :sswitch_8
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x0 -> :sswitch_a
        0x5 -> :sswitch_a
        0xa -> :sswitch_a
        0xf -> :sswitch_a
        0x14 -> :sswitch_a
        0x28 -> :sswitch_a
        0x3c -> :sswitch_a
        0x50 -> :sswitch_a
    .end sparse-switch

    :sswitch_data_4
    .sparse-switch
        0x0 -> :sswitch_b
        0x5 -> :sswitch_b
        0xa -> :sswitch_b
        0xf -> :sswitch_b
        0x14 -> :sswitch_b
        0x28 -> :sswitch_b
        0x3c -> :sswitch_b
        0x50 -> :sswitch_b
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 341
    :cond_0
    iget v0, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    if-eqz v0, :cond_1

    .line 342
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->b:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 344
    :cond_1
    iget v0, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    if-eqz v0, :cond_2

    .line 345
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 347
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    if-eqz v0, :cond_3

    .line 348
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->d:Lcom/google/android/gms/lockbox/a/c;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 350
    :cond_3
    iget v0, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    if-eqz v0, :cond_4

    .line 351
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->e:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 353
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 354
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 355
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/lockbox/a/b;->f:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 358
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 359
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 360
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/lockbox/a/b;->g:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 359
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 363
    :cond_6
    iget v0, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    if-eqz v0, :cond_7

    .line 364
    const/16 v0, 0x8

    iget v2, p0, Lcom/google/android/gms/lockbox/a/b;->h:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 366
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_9

    .line 367
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_9

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/lockbox/a/b;->i:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 369
    if-eqz v0, :cond_8

    .line 370
    const/16 v2, 0x9

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 367
    :cond_8
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 374
    :cond_9
    iget v0, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    if-eqz v0, :cond_a

    .line 375
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/gms/lockbox/a/b;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 377
    :cond_a
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_b

    .line 378
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->l:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 380
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    if-eqz v0, :cond_c

    .line 381
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/android/gms/lockbox/a/b;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 383
    :cond_c
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_d

    .line 384
    const/16 v0, 0xd

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/a/b;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 386
    :cond_d
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 387
    return-void
.end method
