.class abstract Lcom/google/android/gms/drive/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/a/c;


# instance fields
.field final a:Lcom/google/android/gms/drive/database/model/a;

.field final b:Lcom/google/android/gms/drive/a/a/l;

.field private final c:Lcom/google/android/gms/drive/a/e;

.field private final d:Lcom/google/android/gms/drive/auth/AppIdentity;

.field private final e:Lcom/google/android/gms/drive/a/ac;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V
    .locals 6

    .prologue
    .line 66
    sget-object v5, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V

    .line 68
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, "type must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/e;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a;->c:Lcom/google/android/gms/drive/a/e;

    .line 55
    const-string v0, "account must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 56
    const-string v0, "app identity must not be null"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 58
    const-string v0, "enforcement mode must not be null"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/ac;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a;->e:Lcom/google/android/gms/drive/a/ac;

    .line 60
    const-string v0, "execution context must not be null"

    invoke-static {p5, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/l;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    .line 62
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 6

    .prologue
    .line 72
    const-string v0, "requestingAppIdentity"

    invoke-virtual {p3, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v3

    const-string v0, "permissionEnforcement"

    invoke-virtual {p3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/a/ac;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/a/ac;

    move-result-object v4

    invoke-static {p3}, Lcom/google/android/gms/drive/a/a/l;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/a/a/l;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V

    .line 77
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/a/a/i;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/l;->a()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v9

    .line 110
    :goto_0
    return-object v0

    .line 98
    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/a;->b(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    .line 99
    if-nez v2, :cond_1

    .line 100
    const-string v0, "AbstractAction"

    const-string v1, "Cannot notify on action completion: null DriveId!"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v9

    .line 101
    goto :goto_0

    .line 103
    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v5

    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v6

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/drive/a/a;->c:Lcom/google/android/gms/drive/a/e;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/a/a/i;-><init>(Lcom/google/android/gms/drive/a/a/l;Lcom/google/android/gms/drive/DriveId;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/e;)V
    :try_end_0
    .catch Lcom/google/android/gms/drive/a/f; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 108
    const-string v1, "AbstractAction"

    const-string v2, "App is no longer authorized to receive events; will not deliver completion event."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    move-object v0, v9

    .line 110
    goto :goto_0
.end method

.method protected abstract a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
.end method

.method public final a()Lcom/google/android/gms/drive/database/model/a;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    return-object v0
.end method

.method final a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 4

    .prologue
    .line 312
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    .line 315
    const-string v1, "root"

    invoke-virtual {p2}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 316
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 329
    :cond_0
    return-object v0

    .line 320
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    .line 321
    invoke-virtual {p2}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    .line 322
    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 323
    if-nez v0, :cond_2

    .line 324
    new-instance v0, Lcom/google/android/gms/drive/a/ab;

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/a/ab;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0

    .line 326
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 327
    new-instance v0, Lcom/google/android/gms/drive/a/v;

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/a/v;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0
.end method

.method protected abstract a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)V
.end method

.method public final a(Lcom/google/android/gms/drive/g/aw;)V
    .locals 8

    .prologue
    .line 185
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    .line 186
    const-string v0, "AbstractAction"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "ApplyOnServer start: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->h()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 189
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/a;->c(Lcom/google/android/gms/drive/g/aw;)V

    .line 190
    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 193
    const-string v4, "AbstractAction"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ApplyOnServer done:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v0, v2

    invoke-virtual {v5, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms. "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    return-void

    .line 192
    :catchall_0
    move-exception v0

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 193
    const-string v1, "AbstractAction"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "ApplyOnServer done:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v2, v4, v2

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms. "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    throw v0
.end method

.method protected a(Lcom/google/android/gms/drive/a/a;)Z
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->c:Lcom/google/android/gms/drive/a/e;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/a;->c:Lcom/google/android/gms/drive/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/a/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/a;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->e:Lcom/google/android/gms/drive/a/ac;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/a;->e:Lcom/google/android/gms/drive/a/ac;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/a/ac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    iget-object v1, p1, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/a/a/l;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/a/c;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    .line 255
    invoke-interface {p1}, Lcom/google/android/gms/drive/a/c;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    .line 258
    if-nez v1, :cond_0

    .line 259
    const-string v1, "AbstractAction"

    const-string v2, "Action provided to shouldBlock has null EntrySpec"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    const-string v1, "AbstractAction"

    const-string v2, "Provided action with null EntrySpec: %s"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 267
    :goto_0
    return v0

    .line 262
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    if-nez v2, :cond_1

    .line 263
    const-string v1, "AbstractAction"

    const-string v2, "Executing shouldBlock on an action with null EntrySpec"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    const-string v1, "AbstractAction"

    const-string v2, "This action with null EntrySpec: %s"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 267
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x1

    return v0
.end method

.method protected abstract b(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/DriveId;
.end method

.method public final b()Lcom/google/android/gms/drive/a/a/l;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    return-object v0
.end method

.method public b(Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method protected final b(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 338
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    invoke-interface {p1, v2}, Lcom/google/android/gms/drive/database/r;->e(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/l;

    move-result-object v2

    .line 341
    iget v3, v2, Lcom/google/android/gms/drive/database/model/l;->a:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/drive/g/n;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 354
    :cond_0
    :goto_0
    return v0

    .line 345
    :cond_1
    invoke-interface {p2}, Lcom/google/android/gms/drive/g/n;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-boolean v3, v2, Lcom/google/android/gms/drive/database/model/l;->c:Z

    if-eqz v3, :cond_0

    .line 348
    :cond_2
    iget v2, v2, Lcom/google/android/gms/drive/database/model/l;->b:I

    const/16 v3, 0x101

    if-ne v2, v3, :cond_3

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x0

    invoke-virtual {p3, v3, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "plugged"

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    if-eqz v2, :cond_4

    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    .line 354
    goto :goto_0

    :cond_4
    move v2, v0

    .line 348
    goto :goto_1
.end method

.method public final c(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/a/g;
    .locals 4

    .prologue
    .line 161
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 162
    new-instance v1, Lcom/google/android/gms/drive/a/g;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/drive/a/g;-><init>(Lcom/google/android/gms/drive/a/c;Lcom/google/android/gms/drive/a/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    const-string v0, "AbstractAction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ApplyLocally: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :catchall_0
    move-exception v0

    const-string v1, "AbstractAction"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ApplyLocally: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    throw v0
.end method

.method protected c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return-object v0
.end method

.method protected c(Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 208
    return-void
.end method

.method public d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method protected d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    return-object v0
.end method

.method public e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->e:Lcom/google/android/gms/drive/a/ac;

    sget-object v1, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    if-ne v0, v1, :cond_1

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 239
    :cond_0
    return-object v0

    .line 235
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 236
    if-nez v0, :cond_0

    .line 237
    new-instance v0, Lcom/google/android/gms/drive/a/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/a/f;-><init>(Lcom/google/android/gms/drive/auth/AppIdentity;)V

    throw v0
.end method

.method protected e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x0

    return-object v0
.end method

.method public f()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 215
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 216
    const-string v1, "requestingAppIdentity"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/auth/AppIdentity;->d()Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 217
    const-string v1, "operationType"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->c:Lcom/google/android/gms/drive/a/e;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/a/e;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 218
    const-string v1, "permissionEnforcement"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->e:Lcom/google/android/gms/drive/a/ac;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/a/ac;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/a/a/l;->b(Lorg/json/JSONObject;)V

    .line 220
    return-object v0
.end method

.method public g()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    return-object v0
.end method

.method protected final h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 281
    const-string v0, "Action type=%s, account=%s, requestingAppIdentity=%s, enforcementMode=%s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a;->c:Lcom/google/android/gms/drive/a/e;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a;->e:Lcom/google/android/gms/drive/a/ac;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected i()I
    .locals 3

    .prologue
    .line 298
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->c:Lcom/google/android/gms/drive/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->d:Lcom/google/android/gms/drive/auth/AppIdentity;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->e:Lcom/google/android/gms/drive/a/ac;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->b:Lcom/google/android/gms/drive/a/a/l;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
