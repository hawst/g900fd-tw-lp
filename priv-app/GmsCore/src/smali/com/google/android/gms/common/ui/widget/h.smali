.class public abstract Lcom/google/android/gms/common/ui/widget/h;
.super Lcom/google/android/gms/common/ui/widget/a;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field private e:[Lcom/google/android/gms/common/ui/widget/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 164
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/a;-><init>(Landroid/content/Context;)V

    .line 161
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/common/ui/widget/i;

    iput-object v0, p0, Lcom/google/android/gms/common/ui/widget/h;->e:[Lcom/google/android/gms/common/ui/widget/i;

    .line 165
    return-void
.end method


# virtual methods
.method protected final b()V
    .locals 3

    .prologue
    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/a;->d:Lcom/google/android/gms/common/ui/widget/c;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/ui/widget/c;->a(Ljava/util/ArrayList;I)V

    .line 199
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/gms/common/ui/widget/i;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/common/ui/widget/i;

    iput-object v0, p0, Lcom/google/android/gms/common/ui/widget/h;->e:[Lcom/google/android/gms/common/ui/widget/i;

    .line 200
    return-void
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/h;->e:[Lcom/google/android/gms/common/ui/widget/i;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/h;->e:[Lcom/google/android/gms/common/ui/widget/i;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/gms/common/ui/widget/i;->a:I

    .line 173
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSectionForPosition(I)I
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/h;->e:[Lcom/google/android/gms/common/ui/widget/i;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/h;->e:[Lcom/google/android/gms/common/ui/widget/i;

    aget-object v1, v1, v0

    iget v1, v1, Lcom/google/android/gms/common/ui/widget/i;->a:I

    if-le v1, p1, :cond_0

    .line 182
    add-int/lit8 v0, v0, -0x1

    .line 185
    :goto_1
    return v0

    .line 180
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/h;->e:[Lcom/google/android/gms/common/ui/widget/i;

    return-object v0
.end method
