.class public final Lcom/google/android/gms/fitness/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/Map;

.field private static final b:Lcom/google/android/gms/fitness/a/a;

.field private static final c:Lcom/google/android/gms/fitness/a/s;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/fitness/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/a/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/a/j;->b:Lcom/google/android/gms/fitness/a/a;

    .line 50
    new-instance v0, Lcom/google/android/gms/fitness/a/s;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/a/s;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/a/j;->c:Lcom/google/android/gms/fitness/a/s;

    .line 53
    invoke-static {}, Lcom/google/k/c/bo;->h()Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->a:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/r;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->a:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3}, Lcom/google/android/gms/fitness/a/r;-><init>(Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->n:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/r;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->n:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3}, Lcom/google/android/gms/fitness/a/r;-><init>(Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->d:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v2, Lcom/google/android/gms/fitness/a/j;->b:Lcom/google/android/gms/fitness/a/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->x:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v2, Lcom/google/android/gms/fitness/a/j;->b:Lcom/google/android/gms/fitness/a/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->p:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/v;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->p:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->F:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/fitness/a/v;-><init>(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->F:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/v;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->F:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->F:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/fitness/a/v;-><init>(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->k:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/v;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->k:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->C:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/fitness/a/v;-><init>(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->C:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/v;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->C:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->C:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/fitness/a/v;-><init>(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->v:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/v;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->v:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->G:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/fitness/a/v;-><init>(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->G:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/v;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->G:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v4, Lcom/google/android/gms/fitness/data/DataType;->G:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/fitness/a/v;-><init>(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->l:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v2, Lcom/google/android/gms/fitness/a/j;->c:Lcom/google/android/gms/fitness/a/s;

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->D:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v2, Lcom/google/android/gms/fitness/a/j;->c:Lcom/google/android/gms/fitness/a/s;

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->e:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/r;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->e:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3}, Lcom/google/android/gms/fitness/a/r;-><init>(Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->f:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v2, Lcom/google/android/gms/fitness/a/r;

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->f:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v2, v3}, Lcom/google/android/gms/fitness/a/r;-><init>(Lcom/google/android/gms/fitness/data/DataType;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/k/c/bp;->a(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/k/c/bp;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/k/c/bp;->a()Lcom/google/k/c/bo;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/a/j;->a:Ljava/util/Map;

    return-void
.end method

.method public static a(Ljava/util/List;I)Lcom/google/android/gms/fitness/data/Bucket;
    .locals 12

    .prologue
    const/4 v10, 0x1

    const/4 v11, 0x0

    .line 277
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v10

    :goto_0
    const-string v1, "No unbaked segment buckets specified."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 279
    invoke-interface {p0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Bucket;

    .line 280
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v4, v1

    check-cast v4, Lcom/google/android/gms/fitness/data/Bucket;

    .line 281
    new-instance v1, Lcom/google/android/gms/fitness/data/Bucket;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/Bucket;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/data/Bucket;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->b()I

    move-result v7

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->c()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    move v9, p1

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/fitness/data/Bucket;-><init>(JJLcom/google/android/gms/fitness/data/Session;III)V

    .line 289
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->e()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/data/Bucket;->a(Z)V

    .line 291
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 292
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSet;

    .line 293
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v3

    invoke-interface {v5, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    move v0, v11

    .line 277
    goto :goto_0

    :cond_1
    move v3, v10

    .line 296
    :goto_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    .line 297
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Bucket;

    .line 298
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->c()Ljava/util/List;

    move-result-object v2

    .line 299
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v4

    if-ne v0, v4, :cond_2

    move v0, v10

    :goto_3
    const-string v4, "DataSets in bucket %d don\'t match the original.  %s vs %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v11

    aput-object v2, v6, v10

    const/4 v7, 0x2

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v0, v4, v6}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 303
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSet;

    .line 304
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataSet;

    .line 305
    if-eqz v2, :cond_3

    move v4, v10

    :goto_5
    const-string v7, "Unmatched data type: %s"

    new-array v8, v10, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v4, v7, v8}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 307
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v7

    invoke-virtual {v4, v7}, Lcom/google/android/gms/fitness/data/DataSource;->equals(Ljava/lang/Object;)Z

    .line 308
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/data/DataSet;->a(Ljava/lang/Iterable;)V

    .line 312
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    invoke-interface {v5, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_4

    :cond_2
    move v0, v11

    .line 299
    goto :goto_3

    :cond_3
    move v4, v11

    .line 305
    goto :goto_5

    .line 296
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 316
    :cond_5
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSet;

    .line 317
    invoke-static {v0}, Lcom/google/android/gms/fitness/a/j;->a(Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/Bucket;->a(Lcom/google/android/gms/fitness/data/DataSet;)V

    goto :goto_6

    .line 319
    :cond_6
    return-object v1
.end method

.method private static a(Lcom/google/android/gms/fitness/a/n;Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/DataPoint;)Lcom/google/android/gms/fitness/data/DataPoint;
    .locals 6

    .prologue
    .line 197
    if-nez p1, :cond_1

    .line 203
    :cond_0
    :goto_0
    return-object p2

    .line 200
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/fitness/a/n;->a:J

    .line 201
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sub-long/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 202
    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p2, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sub-long v0, v4, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 203
    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    move-object p2, p1

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/fitness/a/n;Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/fitness/data/DataSet;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 146
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    .line 147
    invoke-static {v0}, Lcom/google/android/gms/fitness/a/e;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    .line 148
    invoke-static {p1, v4}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v2

    .line 150
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v1

    .line 151
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v0, v2

    .line 164
    :goto_0
    return-object v0

    .line 155
    :cond_0
    sget-object v5, Lcom/google/android/gms/fitness/data/DataType;->x:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 156
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->x:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->a(I)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Value;->c()I

    move-result v7

    invoke-virtual {v5, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/fitness/a/j;->a(Lcom/google/android/gms/fitness/a/n;Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/DataPoint;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v5, v7, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v3

    :goto_2
    if-ge v1, v6, :cond_3

    invoke-virtual {v5, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_3
    sget-object v0, Lcom/google/android/gms/fitness/a/j;->b:Lcom/google/android/gms/fitness/a/a;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/fitness/a/a;->a(Ljava/util/List;)V

    invoke-virtual {v2, v7}, Lcom/google/android/gms/fitness/data/DataSet;->a(Ljava/lang/Iterable;)V

    move-object v0, v2

    goto :goto_0

    .line 159
    :cond_4
    const/4 v0, 0x0

    .line 160
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v1, v0

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 161
    invoke-static {p0, v1, v0}, Lcom/google/android/gms/fitness/a/j;->a(Lcom/google/android/gms/fitness/a/n;Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/DataPoint;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    move-object v1, v0

    .line 162
    goto :goto_3

    .line 163
    :cond_5
    invoke-virtual {v1, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    move-object v0, v2

    .line 164
    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/fitness/data/DataSet;
    .locals 5

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    .line 127
    sget-object v0, Lcom/google/android/gms/fitness/a/j;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/a/g;

    .line 128
    if-nez v0, :cond_0

    .line 129
    const-string v0, "Aggregation requested for un-supported data type: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 130
    invoke-static {p0, v2}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    .line 132
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0, p0}, Lcom/google/android/gms/fitness/a/g;->a(Lcom/google/android/gms/fitness/data/DataSet;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Ljava/util/List;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 222
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 242
    :goto_0
    return-object v0

    .line 226
    :cond_0
    new-instance v4, Landroid/util/SparseArray;

    invoke-direct {v4}, Landroid/util/SparseArray;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Bucket;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->d()I

    move-result v1

    const/4 v6, 0x4

    if-ne v1, v6, :cond_1

    move v1, v2

    :goto_2
    const-string v6, "Unexpected bucket type: %s"

    new-array v7, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->d()I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->b()I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v4, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    :goto_3
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4, v6, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_2

    :cond_2
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_3

    .line 227
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 229
    :goto_4
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v3, v0, :cond_4

    .line 230
    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/a/j;->a(Ljava/util/List;I)Lcom/google/android/gms/fitness/data/Bucket;

    move-result-object v0

    .line 232
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 229
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 234
    :cond_4
    new-instance v0, Lcom/google/android/gms/fitness/a/k;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/a/k;-><init>()V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v0, v1

    .line 242
    goto :goto_0
.end method
