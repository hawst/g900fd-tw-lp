.class public final enum Lcom/google/android/gms/drive/database/model/e;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/e;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/e;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/e;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/e;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/e;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/e;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/e;

.field public static final enum h:Lcom/google/android/gms/drive/database/model/e;

.field private static final synthetic j:[Lcom/google/android/gms/drive/database/model/e;


# instance fields
.field private final i:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x4

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 44
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "ACCOUNT_HOLDER_NAME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "accountHolderName"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v8, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->a:Lcom/google/android/gms/drive/database/model/e;

    .line 51
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "FORCE_FULL_SYNC"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "forceFullSync"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0xa

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0xb

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "forceFullSync"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x17

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x18

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "forceFullSync"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->b:Lcom/google/android/gms/drive/database/model/e;

    .line 59
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "LAST_CONTENT_SYNC_TIME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "lastContentSyncTime"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->c:Lcom/google/android/gms/drive/database/model/e;

    .line 64
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "LAST_SYNC_TIME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "lastSyncTime"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v11, v2}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->d:Lcom/google/android/gms/drive/database/model/e;

    .line 68
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "FOLDER_SYNC_CLIP_TIME"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "folderSyncClipTime"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->e:Lcom/google/android/gms/drive/database/model/e;

    .line 71
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "DOCUMENT_SYNC_CLIP_TIME"

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "documentSyncClipTime"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->f:Lcom/google/android/gms/drive/database/model/e;

    .line 74
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "LAST_SYNC_CHANGE_STAMP"

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "lastSyncChangeStamp"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->g:Lcom/google/android/gms/drive/database/model/e;

    .line 82
    new-instance v0, Lcom/google/android/gms/drive/database/model/e;

    const-string v1, "LAST_SYNC_SEQUENCE_NUMBER"

    const/4 v2, 0x7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->d()Lcom/google/android/gms/drive/database/model/d;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "lastSyncSequenceNumber"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/e;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->h:Lcom/google/android/gms/drive/database/model/e;

    .line 43
    const/16 v0, 0x8

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/e;

    sget-object v1, Lcom/google/android/gms/drive/database/model/e;->a:Lcom/google/android/gms/drive/database/model/e;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/e;->b:Lcom/google/android/gms/drive/database/model/e;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/e;->c:Lcom/google/android/gms/drive/database/model/e;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/e;->d:Lcom/google/android/gms/drive/database/model/e;

    aput-object v1, v0, v11

    sget-object v1, Lcom/google/android/gms/drive/database/model/e;->e:Lcom/google/android/gms/drive/database/model/e;

    aput-object v1, v0, v12

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/database/model/e;->f:Lcom/google/android/gms/drive/database/model/e;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/database/model/e;->g:Lcom/google/android/gms/drive/database/model/e;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/drive/database/model/e;->h:Lcom/google/android/gms/drive/database/model/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/e;->j:[Lcom/google/android/gms/drive/database/model/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 89
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 90
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/e;->i:Lcom/google/android/gms/drive/database/model/ab;

    .line 91
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/e;
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/google/android/gms/drive/database/model/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/e;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/gms/drive/database/model/e;->j:[Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/e;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/e;->i:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/e;->i:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
