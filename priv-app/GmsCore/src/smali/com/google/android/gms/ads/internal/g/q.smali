.class public final Lcom/google/android/gms/ads/internal/g/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# static fields
.field static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 32
    sput-object v0, Lcom/google/android/gms/ads/internal/g/q;->a:Ljava/util/Map;

    const-string v1, "resize"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    sget-object v0, Lcom/google/android/gms/ads/internal/g/q;->a:Ljava/util/Map;

    const-string v1, "playVideo"

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 34
    sget-object v0, Lcom/google/android/gms/ads/internal/g/q;->a:Ljava/util/Map;

    const-string v1, "storePicture"

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    sget-object v0, Lcom/google/android/gms/ads/internal/g/q;->a:Ljava/util/Map;

    const-string v1, "createCalendarEvent"

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 36
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 6

    .prologue
    .line 40
    const-string v0, "a"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 42
    sget-object v1, Lcom/google/android/gms/ads/internal/g/q;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 53
    :pswitch_0
    const-string v0, "Unknown MRAID command called."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 56
    :goto_0
    return-void

    .line 44
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/ads/internal/l/d;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/ads/internal/l/d;-><init>(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/l/d;->a()V

    goto :goto_0

    .line 47
    :pswitch_2
    new-instance v0, Lcom/google/android/gms/ads/internal/l/a;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/ads/internal/l/a;-><init>(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V

    new-instance v1, Lcom/google/android/gms/ads/internal/c/a;

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/l/a;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/ads/internal/c/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/c/a;->b()Z

    move-result v1

    if-nez v1, :cond_0

    const-string v0, "This feature is not available on this version of the device."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/l/a;->b:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v2, Lcom/google/android/gms/p;->ft:I

    const-string v3, "Create calendar event"

    invoke-static {v2, v3}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/gms/p;->fs:I

    const-string v3, "Allow Ad to create a calendar event?"

    invoke-static {v2, v3}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/gms/p;->a:I

    const-string v3, "Accept"

    invoke-static {v2, v3}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/ads/internal/l/b;

    invoke-direct {v3, v0}, Lcom/google/android/gms/ads/internal/l/b;-><init>(Lcom/google/android/gms/ads/internal/l/a;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/gms/p;->fu:I

    const-string v3, "Decline"

    invoke-static {v2, v3}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/ads/internal/l/c;

    invoke-direct {v3, v0}, Lcom/google/android/gms/ads/internal/l/c;-><init>(Lcom/google/android/gms/ads/internal/l/a;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    .line 50
    :pswitch_3
    new-instance v1, Lcom/google/android/gms/ads/internal/l/e;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/ads/internal/l/e;-><init>(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V

    new-instance v0, Lcom/google/android/gms/ads/internal/c/a;

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/l/e;->c:Landroid/content/Context;

    invoke-direct {v0, v2}, Lcom/google/android/gms/ads/internal/c/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/c/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "Store picture feature is not supported on this device."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/ads/internal/l/e;->b:Ljava/util/Map;

    const-string v2, "iurl"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Image url cannot be empty."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/ads/internal/l/e;->b:Ljava/util/Map;

    const-string v2, "iurl"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid image url:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_3
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/g;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string v0, "Image type not recognized:"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    new-instance v3, Landroid/app/AlertDialog$Builder;

    iget-object v4, v1, Lcom/google/android/gms/ads/internal/l/e;->c:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v4, Lcom/google/android/gms/p;->xM:I

    const-string v5, "Save image"

    invoke-static {v4, v5}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/gms/p;->xL:I

    const-string v5, "Allow Ad to store image in Picture gallery?"

    invoke-static {v4, v5}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    sget v4, Lcom/google/android/gms/p;->a:I

    const-string v5, "Accept"

    invoke-static {v4, v5}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/ads/internal/l/f;

    invoke-direct {v5, v1, v0, v2}, Lcom/google/android/gms/ads/internal/l/f;-><init>(Lcom/google/android/gms/ads/internal/l/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v0, Lcom/google/android/gms/p;->fu:I

    const-string v2, "Decline"

    invoke-static {v0, v2}, Lcom/google/android/gms/ads/internal/o/e;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/ads/internal/l/g;

    invoke-direct {v2, v1}, Lcom/google/android/gms/ads/internal/l/g;-><init>(Lcom/google/android/gms/ads/internal/l/e;)V

    invoke-virtual {v3, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto/16 :goto_0

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method
