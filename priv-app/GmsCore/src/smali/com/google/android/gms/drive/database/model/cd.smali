.class public final enum Lcom/google/android/gms/drive/database/model/cd;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/cd;

.field private static final synthetic c:[Lcom/google/android/gms/drive/database/model/cd;


# instance fields
.field private final b:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 38
    new-instance v0, Lcom/google/android/gms/drive/database/model/cd;

    const-string v1, "NULL_HOLDER"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cc;->d()Lcom/google/android/gms/drive/database/model/cc;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "nullHolder"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/model/cd;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/cd;->a:Lcom/google/android/gms/drive/database/model/cd;

    .line 28
    new-array v0, v6, [Lcom/google/android/gms/drive/database/model/cd;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/database/model/cd;->a:Lcom/google/android/gms/drive/database/model/cd;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/cd;->c:[Lcom/google/android/gms/drive/database/model/cd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/cd;->b:Lcom/google/android/gms/drive/database/model/ab;

    .line 46
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/cd;
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/google/android/gms/drive/database/model/cd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/cd;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/cd;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/drive/database/model/cd;->c:[Lcom/google/android/gms/drive/database/model/cd;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/cd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/cd;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/cd;->b:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/cd;->b:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
