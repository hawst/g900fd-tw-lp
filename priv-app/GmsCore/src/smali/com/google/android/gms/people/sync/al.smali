.class abstract Lcom/google/android/gms/people/sync/al;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b:Lcom/google/android/gms/people/internal/bf;

.field protected final c:Ljava/util/Set;

.field final synthetic d:Lcom/google/android/gms/people/sync/z;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/sync/z;)V
    .locals 1

    .prologue
    .line 1644
    iput-object p1, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1636
    new-instance v0, Lcom/google/android/gms/people/internal/bf;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/bf;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/al;->b:Lcom/google/android/gms/people/internal/bf;

    .line 1642
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/sync/al;->c:Ljava/util/Set;

    .line 1645
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;)V
    .locals 7

    .prologue
    .line 1665
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->h(Lcom/google/android/gms/people/sync/z;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1673
    :goto_0
    return-void

    .line 1669
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleList;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;

    iget-object v5, p0, Lcom/google/android/gms/people/sync/al;->c:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gms/people/sync/al;->b:Lcom/google/android/gms/people/internal/bf;

    invoke-virtual {v4, v0, v5, v6}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Set;Lcom/google/android/gms/people/internal/bf;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1672
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/al;->b()V

    goto :goto_0
.end method

.method protected abstract a(Ljava/lang/String;)V
.end method

.method protected abstract b(Ljava/lang/String;)Ljava/util/List;
.end method

.method protected abstract b()V
.end method

.method protected abstract c()V
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1648
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->h(Lcom/google/android/gms/people/sync/z;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1649
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/al;->a()V

    .line 1651
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1676
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->h(Lcom/google/android/gms/people/sync/z;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1685
    :goto_0
    return-void

    .line 1680
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/al;->c()V

    .line 1682
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->f(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v2, v1}, Lcom/google/android/gms/people/service/h;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 1684
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->f(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/service/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/service/h;->b()V

    goto :goto_0
.end method

.method protected final f()V
    .locals 14

    .prologue
    const-wide/16 v12, 0x1

    const/4 v3, 0x0

    .line 1699
    .line 1700
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->w()V

    .line 1702
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v4, v3

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1704
    iget-object v7, p0, Lcom/google/android/gms/people/sync/al;->b:Lcom/google/android/gms/people/internal/bf;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/people/internal/bf;->a(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/sync/al;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_1
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move v5, v3

    :goto_2
    if-ge v5, v8, :cond_2

    invoke-virtual {v7, v0, v5}, Lcom/google/android/gms/people/internal/bf;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/people/sync/b;

    iget-object v2, v2, Lcom/google/android/gms/people/sync/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    :cond_2
    invoke-static {v1}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v2}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/people/sync/ap;->m(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->c(Lcom/google/android/gms/people/sync/z;)Landroid/content/SyncResult;

    move-result-object v1

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v10, v1, Landroid/content/SyncStats;->numDeletes:J

    add-long/2addr v10, v12

    iput-wide v10, v1, Landroid/content/SyncStats;->numDeletes:J

    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v1

    iget v2, v1, Lcom/google/android/gms/people/sync/y;->v:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/gms/people/sync/y;->v:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1734
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    .line 1707
    :cond_3
    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/sync/al;->a(Ljava/lang/String;)V

    .line 1710
    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->b:Lcom/google/android/gms/people/internal/bf;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/people/internal/bf;->a(Ljava/lang/String;)I

    move-result v5

    .line 1711
    if-nez v5, :cond_4

    .line 1714
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/people/sync/y;->B:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/people/sync/y;->B:I

    goto/16 :goto_0

    :cond_4
    move v2, v3

    .line 1716
    :goto_3
    if-ge v2, v5, :cond_0

    .line 1717
    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->b:Lcom/google/android/gms/people/internal/bf;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/people/internal/bf;->a(Ljava/lang/String;I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/sync/b;

    .line 1719
    iget-object v7, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v7}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v7

    iget-object v8, v1, Lcom/google/android/gms/people/sync/b;->b:Ljava/lang/String;

    iget-object v9, v1, Lcom/google/android/gms/people/sync/b;->a:Ljava/lang/String;

    iget v1, v1, Lcom/google/android/gms/people/sync/b;->c:I

    invoke-virtual {v7, v0, v8, v9, v1}, Lcom/google/android/gms/people/sync/ap;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1721
    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->c(Lcom/google/android/gms/people/sync/z;)Landroid/content/SyncResult;

    move-result-object v1

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v1, Landroid/content/SyncStats;->numInserts:J

    add-long/2addr v8, v12

    iput-wide v8, v1, Landroid/content/SyncStats;->numInserts:J

    .line 1723
    add-int/lit8 v1, v4, 0x1

    .line 1724
    const/16 v4, 0x32

    if-le v1, v4, :cond_5

    .line 1726
    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->x()V

    .line 1727
    iget-object v1, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->g(Lcom/google/android/gms/people/sync/z;)V

    move v1, v3

    .line 1716
    :cond_5
    add-int/lit8 v2, v2, 0x1

    move v4, v1

    goto :goto_3

    .line 1732
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1734
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    .line 1736
    iget-object v0, p0, Lcom/google/android/gms/people/sync/al;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->g(Lcom/google/android/gms/people/sync/z;)V

    .line 1737
    return-void
.end method
