.class public Lcom/google/android/gms/social/location/a;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private j:Lcom/google/android/gms/social/location/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 36
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/social/location/a;
    .locals 3

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/gms/social/location/a;

    invoke-direct {v0}, Lcom/google/android/gms/social/location/a;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    if-eqz p0, :cond_0

    const-string v2, "title"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    if-eqz p1, :cond_1

    const-string v2, "message"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    if-eqz p2, :cond_2

    const-string v2, "positive"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    if-eqz p3, :cond_3

    const-string v2, "negative"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/a;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method

.method private c()Lcom/google/android/gms/social/location/b;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/social/location/a;->j:Lcom/google/android/gms/social/location/b;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/social/location/a;->j:Lcom/google/android/gms/social/location/b;

    .line 200
    :goto_0
    return-object v0

    .line 192
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/social/location/b;

    if-eqz v0, :cond_1

    .line 193
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/b;

    goto :goto_0

    .line 196
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/social/location/b;

    if-eqz v0, :cond_2

    .line 197
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/b;

    goto :goto_0

    .line 200
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/social/location/b;)V
    .locals 0

    .prologue
    .line 118
    iput-object p1, p0, Lcom/google/android/gms/social/location/a;->j:Lcom/google/android/gms/social/location/b;

    .line 119
    return-void
.end method

.method public a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->b()Landroid/content/Context;

    move-result-object v0

    .line 126
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 127
    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 128
    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 131
    :cond_0
    const-string v3, "message"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 132
    const-string v3, "message"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 133
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lcom/google/android/gms/l;->cF:I

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 135
    sget v0, Lcom/google/android/gms/j;->mb:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    if-eqz v0, :cond_1

    .line 137
    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 138
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 140
    :cond_1
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 143
    :cond_2
    const-string v0, "positive"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 144
    const-string v0, "positive"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 147
    :cond_3
    const-string v0, "negative"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 148
    const-string v0, "negative"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 151
    :cond_4
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final b()Landroid/content/Context;
    .locals 3

    .prologue
    .line 212
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 213
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 218
    :goto_0
    return-object v0

    .line 215
    :cond_0
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const v2, 0x103000b

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/gms/social/location/a;->c()Lcom/google/android/gms/social/location/b;

    move-result-object v0

    .line 179
    if-eqz v0, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getArguments()Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getTag()Ljava/lang/String;

    .line 182
    :cond_0
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/android/gms/social/location/a;->c()Lcom/google/android/gms/social/location/b;

    move-result-object v0

    .line 157
    if-eqz v0, :cond_0

    .line 158
    packed-switch p2, :pswitch_data_0

    .line 166
    :cond_0
    :goto_0
    return-void

    .line 160
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getArguments()Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/social/location/b;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getArguments()Landroid/os/Bundle;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/a;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/social/location/b;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 158
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
