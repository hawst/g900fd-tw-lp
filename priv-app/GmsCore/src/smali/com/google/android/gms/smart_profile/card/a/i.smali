.class final Lcom/google/android/gms/smart_profile/card/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/bg;

.field final synthetic b:Lcom/google/android/gms/smart_profile/card/a/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/smart_profile/card/a/g;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iput-object p2, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 10

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v9, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->c()Lcom/google/android/gms/smart_profile/ak;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/smart_profile/c;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/smart_profile/ak;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 152
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v1, v1, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v5}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v6}, Lcom/google/android/gms/smart_profile/bg;->g()I

    move-result v6

    new-instance v7, Landroid/content/Intent;

    const-class v8, Lcom/google/android/gms/smart_profile/PeopleListActivity;

    invoke-direct {v7, v0, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v0, "relationship"

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v0, "target_person_id"

    invoke-virtual {v7, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.people.smart_profile.QUALIFIED_ID"

    invoke-virtual {v7, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_ACCOUNT_NAME"

    invoke-virtual {v7, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_PAGE_ID"

    invoke-virtual {v7, v0, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    invoke-virtual {v7, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->a:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0, v7, v9}, Lcom/google/android/gms/smart_profile/bg;->startActivityForResult(Landroid/content/Intent;I)V

    .line 187
    :goto_0
    return-void

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v5

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/g;->e:Lcom/google/ac/c/a/a/a/al;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    new-instance v7, Landroid/content/Intent;

    const-string v8, "android.intent.action.VIEW"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-ne v0, v2, :cond_2

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v9

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v8, Lcom/google/android/gms/p;->xh:I

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    :goto_2
    invoke-static {v7, v4, v5, v6}, Lcom/google/android/gms/smart_profile/aj;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 171
    const-string v0, "com.google.android.apps.plus"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, v1, v9}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 174
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 175
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 163
    goto :goto_1

    :cond_2
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v9

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v8, Lcom/google/android/gms/p;->xg:I

    invoke-virtual {v3, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    goto :goto_2

    .line 179
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->h()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/SmartProfilePerson;->k()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/i;->b:Lcom/google/android/gms/smart_profile/card/a/g;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/bg;->f()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {v6, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v1, v2, v9

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-static {v6, v3, v4, v5}, Lcom/google/android/gms/smart_profile/aj;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 184
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method
