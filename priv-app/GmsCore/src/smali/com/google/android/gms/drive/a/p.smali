.class public final Lcom/google/android/gms/drive/a/p;
.super Lcom/google/android/gms/drive/a/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/a/w;


# instance fields
.field protected c:Lcom/google/android/gms/drive/DriveId;

.field public d:Lcom/google/android/gms/drive/DriveId;

.field private final e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field private final f:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;)V
    .locals 8

    .prologue
    .line 58
    sget-object v7, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/a/p;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/ac;)V

    .line 60
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/ac;)V
    .locals 4

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/gms/drive/a/e;->h:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2, p7}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    .line 68
    iput-object p3, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v2, "application/vnd.google-apps.drive-sdk"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 72
    :cond_0
    const-string v0, "application/vnd.google-apps.drive-sdk"

    iget-object v1, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 75
    iput-wide p4, p0, Lcom/google/android/gms/drive/a/p;->f:J

    .line 76
    iput-object p6, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    .line 77
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/gms/drive/a/e;->h:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 82
    const-string v0, "metadata"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 84
    const-string v0, "writeOpenKey"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/a/p;->f:J

    .line 85
    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    const-string v0, "parent"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    .line 90
    :goto_0
    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "newDriveId"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    .line 93
    :cond_0
    return-void

    .line 88
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    goto :goto_0
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/p;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 5

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 116
    iget-wide v2, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "application/vnd.google-apps.drive-sdk."

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 117
    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-interface {p1}, Lcom/google/android/gms/drive/database/r;->u()Lcom/google/android/gms/drive/database/t;

    move-result-object v3

    invoke-interface {p1, v2, v0, v1, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/t;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 122
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/drive/metadata/a/ax;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 124
    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->l(Ljava/lang/String;)V

    .line 125
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 129
    iget-object v1, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    .line 130
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    invoke-interface {p1, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v1

    .line 134
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    invoke-interface {p1, v0, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/am;

    move-result-object v2

    .line 136
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/am;->i()V

    .line 139
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->C()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/Long;)V

    .line 143
    :cond_0
    iget-wide v2, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/ah;Ljava/util/Set;)V

    .line 146
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 148
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    .line 149
    new-instance v0, Lcom/google/android/gms/drive/a/aq;

    iget-object v1, p2, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p2, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/p;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/aq;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)V
    .locals 4

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v1, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    .line 173
    new-instance v1, Ljava/util/HashSet;

    if-nez v0, :cond_0

    sget-object v0, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    :cond_0
    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 179
    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    .line 180
    invoke-super {p0, v0}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    .line 181
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-interface {v2, p1, v3, v1}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/d/d;

    move-result-object v2

    .line 185
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/p;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 186
    invoke-interface {v2}, Lcom/google/android/gms/drive/d/d;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->c(Ljava/lang/String;)V

    .line 187
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 188
    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/a/c;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/a/c;)Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->b(Lcom/google/android/gms/drive/g/aw;)V

    return-void
.end method

.method protected final c(Lcom/google/android/gms/drive/g/aw;)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/a/p;->a(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    .line 163
    :cond_0
    return-void
.end method

.method public final bridge synthetic d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 224
    if-ne p0, p1, :cond_1

    .line 236
    :cond_0
    :goto_0
    return v0

    .line 225
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    .line 227
    :cond_3
    check-cast p1, Lcom/google/android/gms/drive/a/p;

    .line 229
    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    .line 230
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    .line 231
    goto :goto_0

    .line 230
    :cond_6
    iget-object v2, p1, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    if-nez v2, :cond_5

    .line 232
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 234
    goto :goto_0

    .line 232
    :cond_8
    iget-object v2, p1, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 210
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 211
    const-string v1, "metadata"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/a/ax;->b(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lorg/json/JSONObject;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 212
    const-string v1, "writeOpenKey"

    iget-wide v2, p0, Lcom/google/android/gms/drive/a/p;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 213
    iget-object v1, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    .line 214
    const-string v1, "parent"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_1

    .line 217
    const-string v1, "newDriveId"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 219
    :cond_1
    return-object v0
.end method

.method public final bridge synthetic g()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    .prologue
    .line 40
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->g()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    return-object v0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->hashCode()I

    move-result v0

    .line 242
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    .line 243
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 244
    return v0

    :cond_1
    move v0, v1

    .line 242
    goto :goto_0
.end method

.method public final j()Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 2

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 195
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/util/Set;
    .locals 4

    .prologue
    .line 200
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 201
    iget-object v1, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    if-eqz v1, :cond_0

    .line 203
    iget-object v1, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    :cond_0
    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 249
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CreateFileAction[%s, initialMetadata=%s, parent=%s, newDriveId=%s]"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/p;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/a/p;->e:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/drive/a/p;->c:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
