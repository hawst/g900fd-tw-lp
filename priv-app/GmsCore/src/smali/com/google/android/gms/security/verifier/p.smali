.class public final Lcom/google/android/gms/security/verifier/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/gms/security/verifier/p;


# instance fields
.field public a:[Lcom/google/android/gms/security/verifier/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 599
    invoke-static {}, Lcom/google/android/gms/security/verifier/q;->a()[Lcom/google/android/gms/security/verifier/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/security/verifier/p;->cachedSize:I

    .line 600
    return-void
.end method

.method public static a()[Lcom/google/android/gms/security/verifier/p;
    .locals 2

    .prologue
    .line 584
    sget-object v0, Lcom/google/android/gms/security/verifier/p;->b:[Lcom/google/android/gms/security/verifier/p;

    if-nez v0, :cond_1

    .line 585
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 587
    :try_start_0
    sget-object v0, Lcom/google/android/gms/security/verifier/p;->b:[Lcom/google/android/gms/security/verifier/p;

    if-nez v0, :cond_0

    .line 588
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/security/verifier/p;

    sput-object v0, Lcom/google/android/gms/security/verifier/p;->b:[Lcom/google/android/gms/security/verifier/p;

    .line 590
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 592
    :cond_1
    sget-object v0, Lcom/google/android/gms/security/verifier/p;->b:[Lcom/google/android/gms/security/verifier/p;

    return-object v0

    .line 590
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 648
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 649
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 650
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 651
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    aget-object v2, v2, v0

    .line 652
    if-eqz v2, :cond_0

    .line 653
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 650
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 658
    :cond_1
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 610
    if-ne p1, p0, :cond_1

    .line 621
    :cond_0
    :goto_0
    return v0

    .line 613
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/security/verifier/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 614
    goto :goto_0

    .line 616
    :cond_2
    check-cast p1, Lcom/google/android/gms/security/verifier/p;

    .line 617
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    iget-object v3, p1, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 619
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 626
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 629
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 343
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/security/verifier/q;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/security/verifier/q;

    invoke-direct {v3}, Lcom/google/android/gms/security/verifier/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/security/verifier/q;

    invoke-direct {v3}, Lcom/google/android/gms/security/verifier/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 636
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 637
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/p;->a:[Lcom/google/android/gms/security/verifier/q;

    aget-object v1, v1, v0

    .line 638
    if-eqz v1, :cond_0

    .line 639
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 636
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 643
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 644
    return-void
.end method
