.class final Lcom/google/android/gms/audiomodem/u;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/audiomodem/t;


# direct methods
.method constructor <init>(Lcom/google/android/gms/audiomodem/t;)V
    .locals 0

    .prologue
    .line 65
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/u;->a:Lcom/google/android/gms/audiomodem/t;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 69
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 71
    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 73
    const-string v1, "android.media.EXTRA_VOLUME_STREAM_VALUE"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 74
    const-string v2, "android.media.EXTRA_PREV_VOLUME_STREAM_VALUE"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 76
    if-eq v0, v3, :cond_0

    if-eq v1, v3, :cond_0

    if-ne v2, v3, :cond_1

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    if-eq v1, v2, :cond_0

    .line 84
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/u;->a:Lcom/google/android/gms/audiomodem/t;

    iput v1, v0, Lcom/google/android/gms/audiomodem/t;->j:I

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/u;->a:Lcom/google/android/gms/audiomodem/t;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/t;->b()V

    goto :goto_0
.end method
