.class public final Lcom/google/android/gms/appdatasearch/at;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field protected a:I

.field final synthetic b:Lcom/google/android/gms/appdatasearch/SearchResults;

.field private c:Lcom/google/android/gms/appdatasearch/ar;

.field private final d:[Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V
    .locals 1

    .prologue
    .line 316
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/at;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 317
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/at;->d:[Ljava/util/Map;

    .line 318
    return-void

    .line 317
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/SearchResults;->j:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Ljava/util/Map;

    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/gms/appdatasearch/at;)Lcom/google/android/gms/appdatasearch/ar;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/at;->c:Lcom/google/android/gms/appdatasearch/ar;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/gms/appdatasearch/at;Lcom/google/android/gms/appdatasearch/ar;)Lcom/google/android/gms/appdatasearch/ar;
    .locals 0

    .prologue
    .line 310
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/at;->c:Lcom/google/android/gms/appdatasearch/ar;

    return-object p1
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/appdatasearch/as;
    .locals 3

    .prologue
    .line 327
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/at;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "No more results."

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/as;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/at;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget v2, p0, Lcom/google/android/gms/appdatasearch/at;->a:I

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/appdatasearch/as;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;ILcom/google/android/gms/appdatasearch/at;)V

    .line 331
    iget v1, p0, Lcom/google/android/gms/appdatasearch/at;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/appdatasearch/at;->a:I

    .line 332
    return-object v0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/at;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/appdatasearch/at;->a:I

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/at;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->c()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/at;->a()Lcom/google/android/gms/appdatasearch/as;

    move-result-object v0

    return-object v0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 341
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "remove not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
