.class public final Lcom/google/android/gms/googlehelp/helpactivities/l;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# instance fields
.field private j:Lcom/google/android/gms/googlehelp/helpactivities/n;

.field private k:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 35
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/helpactivities/l;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/l;->k:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/helpactivities/l;)Lcom/google/android/gms/googlehelp/helpactivities/n;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/l;->j:Lcom/google/android/gms/googlehelp/helpactivities/n;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/helpactivities/n;)Lcom/google/android/gms/googlehelp/helpactivities/l;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/l;->j:Lcom/google/android/gms/googlehelp/helpactivities/n;

    .line 45
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/googlehelp/helpactivities/l;
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/l;->k:[Ljava/lang/String;

    .line 50
    return-object p0
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 55
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/l;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 56
    sget v1, Lcom/google/android/gms/p;->pc:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/l;->k:[Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/googlehelp/helpactivities/m;

    invoke-direct {v3, p0}, Lcom/google/android/gms/googlehelp/helpactivities/m;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/l;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 64
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
