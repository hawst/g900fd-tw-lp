.class public Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/media/ab;


# instance fields
.field private a:Lcom/google/android/gms/cast/media/CastMirroringProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    const-string v0, "com.google.android.gms.cast_mirroring.service.FOREGROUND"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 28
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    const-string v0, "com.google.android.gms.cast_mirroring.service.FOREGROUND"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 33
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;->stopForeground(Z)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;->a:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->b()Lcom/google/android/gms/cast/CastDevice;

    move-result-object v0

    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;->stopSelf()V

    .line 57
    :cond_0
    return-void
.end method

.method public final a(ILandroid/app/Notification;)V
    .locals 0

    .prologue
    .line 46
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;->startForeground(ILandroid/app/Notification;)V

    .line 47
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 39
    invoke-static {p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/CastMirroringProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;->a:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;->a:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a(Lcom/google/android/gms/cast/media/ab;)V

    .line 41
    return-void
.end method
