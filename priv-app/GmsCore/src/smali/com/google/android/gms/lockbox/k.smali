.class public final Lcom/google/android/gms/lockbox/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/lockbox/n;
.implements Lcom/google/android/gms/playlog/c;


# instance fields
.field private final a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/playlog/b;

.field private c:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;I)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/lockbox/k;->a:Ljava/lang/String;

    .line 25
    new-instance v0, Lcom/google/android/gms/playlog/b;

    invoke-direct {v0, p2, p3, p0}, Lcom/google/android/gms/playlog/b;-><init>(Landroid/content/Context;ILcom/google/android/gms/playlog/c;)V

    iput-object v0, p0, Lcom/google/android/gms/lockbox/k;->b:Lcom/google/android/gms/playlog/b;

    .line 26
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 40
    const-string v0, "Could not log data in main thread!"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->b:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->l()I

    move-result v0

    if-nez v0, :cond_0

    .line 52
    :goto_0
    return-void

    .line 44
    :cond_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/lockbox/k;->c:Ljava/util/concurrent/CountDownLatch;

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->b:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->f()V

    .line 47
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->b:Lcom/google/android/gms/playlog/b;

    iget-object v0, v0, Lcom/google/android/gms/playlog/b;->a:Lcom/google/android/gms/playlog/internal/i;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/internal/i;->g()V

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/lockbox/k;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Interrupted: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final a(Landroid/app/PendingIntent;)V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not connect to PlayLogger: PendingIntent="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 70
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 30
    iget-object v7, p0, Lcom/google/android/gms/lockbox/k;->b:Lcom/google/android/gms/playlog/b;

    iget-object v0, v7, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v0, v0, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v1, v7, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v1, v1, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->b:Ljava/lang/String;

    iget-object v2, v7, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget v2, v2, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->c:I

    iget-object v3, v7, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget v3, v3, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->d:I

    iget-object v4, v7, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-object v5, v4, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->f:Ljava/lang/String;

    iget-object v4, v7, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    iget-boolean v6, v4, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;->g:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/playlog/internal/PlayLoggerContext;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, v7, Lcom/google/android/gms/playlog/b;->b:Lcom/google/android/gms/playlog/internal/PlayLoggerContext;

    .line 31
    :cond_0
    return-void
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->b:Lcom/google/android/gms/playlog/b;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/gms/playlog/b;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 58
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->a:Ljava/lang/String;

    const-string v1, "Could not connect to PlayLogger"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/lockbox/k;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 64
    return-void
.end method
