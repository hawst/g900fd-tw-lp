.class public final Lcom/google/android/gms/drive/internal/model/Property;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/x;

.field private static final g:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/drive/internal/model/x;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/Property;->CREATOR:Lcom/google/android/gms/drive/internal/model/x;

    .line 69
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 72
    sput-object v0, Lcom/google/android/gms/drive/internal/model/Property;->g:Ljava/util/HashMap;

    const-string v1, "appId"

    const-string v2, "appId"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/android/gms/drive/internal/model/Property;->g:Ljava/util/HashMap;

    const-string v1, "key"

    const-string v2, "key"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/gms/drive/internal/model/Property;->g:Ljava/util/HashMap;

    const-string v1, "value"

    const-string v2, "value"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/google/android/gms/drive/internal/model/Property;->g:Ljava/util/HashMap;

    const-string v1, "visibility"

    const-string v2, "visibility"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 119
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 120
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->b:I

    .line 121
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    .line 122
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 133
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    .line 134
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/Property;->b:I

    .line 135
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/Property;->c:Ljava/lang/String;

    .line 136
    iput-object p4, p0, Lcom/google/android/gms/drive/internal/model/Property;->d:Ljava/lang/String;

    .line 137
    iput-object p5, p0, Lcom/google/android/gms/drive/internal/model/Property;->e:Ljava/lang/String;

    .line 138
    iput-object p6, p0, Lcom/google/android/gms/drive/internal/model/Property;->f:Ljava/lang/String;

    .line 139
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/gms/drive/internal/model/Property;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 325
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 326
    packed-switch v0, :pswitch_data_0

    .line 340
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 328
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/Property;->c:Ljava/lang/String;

    .line 343
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 344
    return-void

    .line 331
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/Property;->d:Ljava/lang/String;

    goto :goto_0

    .line 334
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/Property;->e:Ljava/lang/String;

    goto :goto_0

    .line 337
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/Property;->f:Ljava/lang/String;

    goto :goto_0

    .line 326
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 284
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 294
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 286
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->c:Ljava/lang/String;

    .line 292
    :goto_0
    return-object v0

    .line 288
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->d:Ljava/lang/String;

    goto :goto_0

    .line 290
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->e:Ljava/lang/String;

    goto :goto_0

    .line 292
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->f:Ljava/lang/String;

    goto :goto_0

    .line 284
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 269
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 274
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 258
    sget-object v0, Lcom/google/android/gms/drive/internal/model/Property;->CREATOR:Lcom/google/android/gms/drive/internal/model/x;

    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/Property;
    .locals 2

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/Property;->d:Ljava/lang/String;

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 234
    return-object p0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 361
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/Property;

    if-nez v0, :cond_0

    move v0, v1

    .line 392
    :goto_0
    return v0

    .line 366
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 367
    goto :goto_0

    .line 370
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/Property;

    .line 371
    sget-object v0, Lcom/google/android/gms/drive/internal/model/Property;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 372
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/Property;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 373
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/Property;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 375
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/Property;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/Property;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 377
    goto :goto_0

    :cond_3
    move v0, v1

    .line 382
    goto :goto_0

    .line 385
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/Property;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 387
    goto :goto_0

    :cond_5
    move v0, v2

    .line 392
    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/Property;
    .locals 2

    .prologue
    .line 241
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/Property;->e:Ljava/lang/String;

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 243
    return-object p0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/Property;
    .locals 2

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/Property;->f:Ljava/lang/String;

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/Property;->a:Ljava/util/Set;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 252
    return-object p0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 348
    const/4 v0, 0x0

    .line 349
    sget-object v1, Lcom/google/android/gms/drive/internal/model/Property;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 350
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/Property;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 351
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 352
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/Property;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 354
    goto :goto_0

    .line 355
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/gms/drive/internal/model/Property;->CREATOR:Lcom/google/android/gms/drive/internal/model/x;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/model/x;->a(Lcom/google/android/gms/drive/internal/model/Property;Landroid/os/Parcel;)V

    .line 264
    return-void
.end method
