.class final Lcom/google/android/gms/smart_profile/card/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/card/a/j;

.field private b:Landroid/widget/ImageView;

.field private c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/card/a/j;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/a/m;->a:Lcom/google/android/gms/smart_profile/card/a/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/card/a/m;->b:Landroid/widget/ImageView;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/card/a/m;->c:Ljava/lang/String;

    .line 51
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/card/a/j;Landroid/widget/ImageView;Ljava/lang/String;B)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/a/m;-><init>(Lcom/google/android/gms/smart_profile/card/a/j;Landroid/widget/ImageView;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 55
    new-instance v1, Lcom/google/android/gms/smart_profile/b/g;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/m;->a:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/m;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/a/m;->a:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v3, v3, Lcom/google/android/gms/smart_profile/card/a/b;->d:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/bg;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/smart_profile/b/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/api/v;)V

    return-object v1
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 66
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 43
    check-cast p2, Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/m;->b:Landroid/widget/ImageView;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/m;->a:Lcom/google/android/gms/smart_profile/card/a/j;

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    invoke-virtual {v0}, Landroid/support/v7/widget/CardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {v2, v0, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method
