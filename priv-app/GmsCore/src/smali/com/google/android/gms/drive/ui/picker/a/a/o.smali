.class final enum Lcom/google/android/gms/drive/ui/picker/a/a/o;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/picker/a/a/o;

.field public static final enum b:Lcom/google/android/gms/drive/ui/picker/a/a/o;

.field private static final synthetic g:[Lcom/google/android/gms/drive/ui/picker/a/a/o;


# instance fields
.field private final c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

.field private final d:Lcom/google/android/gms/drive/ui/picker/a/a/r;

.field private final e:Lcom/google/android/gms/drive/ui/picker/a/a/r;

.field private final f:Lcom/google/android/gms/drive/ui/picker/a/a/r;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 31
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;

    const-string v1, "FOLDERS"

    sget v2, Lcom/google/android/gms/p;->gx:I

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->a:Lcom/google/android/gms/drive/ui/picker/a/a/o;

    .line 32
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;

    const-string v1, "FILES"

    sget v2, Lcom/google/android/gms/p;->gW:I

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/o;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->b:Lcom/google/android/gms/drive/ui/picker/a/a/o;

    .line 30
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/a/a/o;

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/o;->a:Lcom/google/android/gms/drive/ui/picker/a/a/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/a/o;->b:Lcom/google/android/gms/drive/ui/picker/a/a/o;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->g:[Lcom/google/android/gms/drive/ui/picker/a/a/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v2, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 41
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v1, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->d:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 42
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v1, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 43
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/a/s;

    invoke-direct {v0, p3, v2, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/s;-><init>(IZZ)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->f:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 44
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/a/a/o;
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/picker/a/a/o;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->g:[Lcom/google/android/gms/drive/ui/picker/a/a/o;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/picker/a/a/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/a/o;

    return-object v0
.end method


# virtual methods
.method public final a(ZZ)Lcom/google/android/gms/drive/ui/picker/a/a/r;
    .locals 1

    .prologue
    .line 47
    if-nez p1, :cond_0

    if-nez p2, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->d:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    .line 54
    :goto_0
    return-object v0

    .line 49
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    goto :goto_0

    .line 51
    :cond_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_2

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->e:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    goto :goto_0

    .line 54
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/o;->f:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    goto :goto_0
.end method
