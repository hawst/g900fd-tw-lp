.class public final Lcom/google/android/gms/drive/query/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/query/internal/f;


# instance fields
.field private final a:Lcom/google/android/gms/drive/auth/g;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/auth/g;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/drive/query/a;->a:Lcom/google/android/gms/drive/auth/g;

    .line 26
    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/b;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/b;)Lcom/google/android/gms/drive/metadata/a/al;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/drive/metadata/a/ay;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/drive/query/a;->a:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v0, p2, v2}, Lcom/google/android/gms/drive/metadata/a/al;->b(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/drive/query/i;->e:Lcom/google/android/gms/drive/metadata/j;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/b/c;->d()Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 20
    invoke-static {p1}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/drive/metadata/a/ay;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/drive/query/a;->a:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v0, p2, v2}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 20
    invoke-static {p2}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/drive/metadata/a/ay;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/drive/query/a;->a:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v0, p3, v2}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Ljava/lang/Object;Lcom/google/android/gms/drive/auth/g;)Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/drive/query/internal/Operator;->a:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/google/android/gms/drive/query/internal/Operator;->b:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v2, Lcom/google/android/gms/drive/query/internal/Operator;->c:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->f(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/google/android/gms/drive/query/internal/Operator;->d:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->g(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/google/android/gms/drive/query/internal/Operator;->e:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->h(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/google/android/gms/drive/query/internal/Operator;->i:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    :cond_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported comparison operator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/internal/Operator;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Lcom/google/android/gms/drive/query/internal/Operator;Ljava/util/List;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/drive/query/internal/Operator;->f:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/query/internal/Operator;->g:Lcom/google/android/gms/drive/query/internal/Operator;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/query/internal/Operator;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->b(Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported logical operator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/internal/Operator;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/gms/drive/metadata/sync/b/c;

    invoke-static {p1}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Lcom/google/android/gms/drive/metadata/sync/b/c;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method
