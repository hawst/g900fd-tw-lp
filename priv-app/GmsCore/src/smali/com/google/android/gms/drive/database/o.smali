.class public final enum Lcom/google/android/gms/drive/database/o;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/o;

.field public static final enum b:Lcom/google/android/gms/drive/database/o;

.field public static final enum c:Lcom/google/android/gms/drive/database/o;

.field public static final enum d:Lcom/google/android/gms/drive/database/o;

.field public static final enum e:Lcom/google/android/gms/drive/database/o;

.field public static final enum f:Lcom/google/android/gms/drive/database/o;

.field public static final enum g:Lcom/google/android/gms/drive/database/o;

.field public static final enum h:Lcom/google/android/gms/drive/database/o;

.field public static final enum i:Lcom/google/android/gms/drive/database/o;

.field public static final enum j:Lcom/google/android/gms/drive/database/o;

.field public static final enum k:Lcom/google/android/gms/drive/database/o;

.field public static final enum l:Lcom/google/android/gms/drive/database/o;

.field public static final enum m:Lcom/google/android/gms/drive/database/o;

.field public static final enum n:Lcom/google/android/gms/drive/database/o;

.field public static final enum o:Lcom/google/android/gms/drive/database/o;

.field public static final enum p:Lcom/google/android/gms/drive/database/o;

.field public static final enum q:Lcom/google/android/gms/drive/database/o;

.field public static final enum r:Lcom/google/android/gms/drive/database/o;

.field public static final enum s:Lcom/google/android/gms/drive/database/o;

.field public static final enum t:Lcom/google/android/gms/drive/database/o;

.field public static final enum u:Lcom/google/android/gms/drive/database/o;

.field public static final enum v:Lcom/google/android/gms/drive/database/o;

.field private static final synthetic x:[Lcom/google/android/gms/drive/database/o;


# instance fields
.field private final w:Lcom/google/android/gms/drive/database/model/ae;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 136
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "ACCOUNT"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v2

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->a:Lcom/google/android/gms/drive/database/o;

    .line 137
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "APP_AUTH_METADATA"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v2

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->b:Lcom/google/android/gms/drive/database/o;

    .line 138
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "APP_PREFERENCES"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->a()Lcom/google/android/gms/drive/database/model/m;

    move-result-object v2

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->c:Lcom/google/android/gms/drive/database/o;

    .line 139
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "APPDATA_SYNC_STATUS"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->d:Lcom/google/android/gms/drive/database/o;

    .line 140
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "APP_SCOPE"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/p;->a()Lcom/google/android/gms/drive/database/model/p;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->e:Lcom/google/android/gms/drive/database/o;

    .line 141
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "CUSTOM_PROPERTIES"

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->a()Lcom/google/android/gms/drive/database/model/z;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->f:Lcom/google/android/gms/drive/database/o;

    .line 142
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "DOCUMENT_CONTENT"

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->a()Lcom/google/android/gms/drive/database/model/af;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->g:Lcom/google/android/gms/drive/database/o;

    .line 143
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "ENTRY"

    const/4 v2, 0x7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->h:Lcom/google/android/gms/drive/database/o;

    .line 144
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "CONTAINS_ID"

    const/16 v2, 0x8

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->i:Lcom/google/android/gms/drive/database/o;

    .line 145
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "PARTIAL_FEED"

    const/16 v2, 0x9

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->a()Lcom/google/android/gms/drive/database/model/bc;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->j:Lcom/google/android/gms/drive/database/o;

    .line 146
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "SYNC_REQUEST"

    const/16 v2, 0xa

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bz;->a()Lcom/google/android/gms/drive/database/model/bz;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->k:Lcom/google/android/gms/drive/database/o;

    .line 147
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "UNIQUE_ID"

    const/16 v2, 0xb

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cc;->a()Lcom/google/android/gms/drive/database/model/cc;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->l:Lcom/google/android/gms/drive/database/o;

    .line 148
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "ENTRY_AUTHORIZED_APP"

    const/16 v2, 0xc

    invoke-static {}, Lcom/google/android/gms/drive/database/model/aj;->a()Lcom/google/android/gms/drive/database/model/aj;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->m:Lcom/google/android/gms/drive/database/o;

    .line 149
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "PENDING_ACTION"

    const/16 v2, 0xd

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->n:Lcom/google/android/gms/drive/database/o;

    .line 150
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "FILE_CONTENT"

    const/16 v2, 0xe

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->o:Lcom/google/android/gms/drive/database/o;

    .line 151
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "PENDING_UPLOADS"

    const/16 v2, 0xf

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->p:Lcom/google/android/gms/drive/database/o;

    .line 152
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "DELETION_LOCK"

    const/16 v2, 0x10

    invoke-static {}, Lcom/google/android/gms/drive/database/model/u;->a()Lcom/google/android/gms/drive/database/model/u;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->q:Lcom/google/android/gms/drive/database/o;

    .line 153
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "SUBSCRIPTION"

    const/16 v2, 0x11

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->r:Lcom/google/android/gms/drive/database/o;

    .line 154
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "USER_PERMISSIONS"

    const/16 v2, 0x12

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->s:Lcom/google/android/gms/drive/database/o;

    .line 155
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "REALTIME_DOCUMENT_CONTENT"

    const/16 v2, 0x13

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->a()Lcom/google/android/gms/drive/database/model/bt;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->t:Lcom/google/android/gms/drive/database/o;

    .line 156
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "PERSISTED_EVENT"

    const/16 v2, 0x14

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bp;->a()Lcom/google/android/gms/drive/database/model/bp;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->u:Lcom/google/android/gms/drive/database/o;

    .line 157
    new-instance v0, Lcom/google/android/gms/drive/database/o;

    const-string v1, "PERSISTED_EVENT_CONTENT"

    const/16 v2, 0x15

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bn;->a()Lcom/google/android/gms/drive/database/model/bn;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/o;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V

    sput-object v0, Lcom/google/android/gms/drive/database/o;->v:Lcom/google/android/gms/drive/database/o;

    .line 135
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/google/android/gms/drive/database/o;

    sget-object v1, Lcom/google/android/gms/drive/database/o;->a:Lcom/google/android/gms/drive/database/o;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/database/o;->b:Lcom/google/android/gms/drive/database/o;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/drive/database/o;->c:Lcom/google/android/gms/drive/database/o;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/android/gms/drive/database/o;->d:Lcom/google/android/gms/drive/database/o;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/o;->e:Lcom/google/android/gms/drive/database/o;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/database/o;->f:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/database/o;->g:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/drive/database/o;->h:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/gms/drive/database/o;->i:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/gms/drive/database/o;->j:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/gms/drive/database/o;->k:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/google/android/gms/drive/database/o;->l:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/google/android/gms/drive/database/o;->m:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/google/android/gms/drive/database/o;->n:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/google/android/gms/drive/database/o;->o:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/google/android/gms/drive/database/o;->p:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/google/android/gms/drive/database/o;->q:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/google/android/gms/drive/database/o;->r:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/google/android/gms/drive/database/o;->s:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/google/android/gms/drive/database/o;->t:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/google/android/gms/drive/database/o;->u:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/google/android/gms/drive/database/o;->v:Lcom/google/android/gms/drive/database/o;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/o;->x:[Lcom/google/android/gms/drive/database/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ae;)V
    .locals 0

    .prologue
    .line 161
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 162
    iput-object p3, p0, Lcom/google/android/gms/drive/database/o;->w:Lcom/google/android/gms/drive/database/model/ae;

    .line 163
    return-void
.end method

.method public static synthetic a(Lcom/google/android/gms/drive/database/o;)Lcom/google/android/gms/drive/database/model/ae;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/drive/database/o;->w:Lcom/google/android/gms/drive/database/model/ae;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/o;
    .locals 1

    .prologue
    .line 135
    const-class v0, Lcom/google/android/gms/drive/database/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/o;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/o;
    .locals 1

    .prologue
    .line 135
    sget-object v0, Lcom/google/android/gms/drive/database/o;->x:[Lcom/google/android/gms/drive/database/o;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/o;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/drive/database/o;->w:Lcom/google/android/gms/drive/database/model/ae;

    return-object v0
.end method
