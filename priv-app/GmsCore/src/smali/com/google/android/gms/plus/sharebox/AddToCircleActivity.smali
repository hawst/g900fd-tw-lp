.class public Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;
.super Landroid/app/ListActivity;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private final b:Lcom/google/android/gms/common/internal/a/a;

.field private c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/ListActivity;-><init>()V

    .line 62
    new-instance v0, Lcom/google/android/gms/common/internal/a/a;

    const/high16 v1, 0x200000

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/a/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b:Lcom/google/android/gms/common/internal/a/a;

    .line 203
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    return-object v0
.end method

.method private a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 332
    const-string v0, "ShareBox"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setResult(I)V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->finish()V

    .line 335
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/common/internal/a/a;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b:Lcom/google/android/gms/common/internal/a/a;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 311
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->eH:I

    if-ne v0, v1, :cond_1

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;)V

    .line 316
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 317
    const-string v1, "add_to_circle_data"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 318
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setResult(ILandroid/content/Intent;)V

    .line 319
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->finish()V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 324
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/plus/sharebox/f;

    if-eqz v1, :cond_0

    .line 326
    check-cast v0, Lcom/google/android/gms/plus/sharebox/f;

    .line 327
    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/f;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 240
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onCreate(Landroid/os/Bundle;)V

    .line 243
    if-nez p1, :cond_0

    .line 244
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "add_to_circle_data"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    .line 248
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    if-nez v0, :cond_1

    .line 249
    const-string v0, "Add to circle data not specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Ljava/lang/String;)V

    .line 276
    :goto_1
    return-void

    .line 246
    :cond_0
    const-string v0, "add_to_circle_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    goto :goto_0

    .line 252
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 254
    :cond_2
    const-string v0, "No un-circled audience members specified"

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 259
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "calling_package_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 260
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "client_application_id"

    invoke-virtual {v1, v2, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 262
    new-instance v2, Lcom/google/android/gms/common/api/w;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    iput-object v0, v2, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v3, Lcom/google/android/gms/people/ad;

    invoke-direct {v3}, Lcom/google/android/gms/people/ad;-><init>()V

    iput v1, v3, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v3}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 270
    sget v0, Lcom/google/android/gms/p;->vE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setTitle(I)V

    .line 271
    sget v0, Lcom/google/android/gms/l;->dk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setContentView(I)V

    .line 272
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setClickable(Z)V

    .line 273
    sget v0, Lcom/google/android/gms/j;->eH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 275
    new-instance v0, Lcom/google/android/gms/plus/sharebox/c;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/plus/sharebox/c;-><init>(Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;B)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto/16 :goto_1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 305
    invoke-super {p0}, Landroid/app/ListActivity;->onDestroy()V

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->b:Lcom/google/android/gms/common/internal/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/a/a;->b()V

    .line 307
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 280
    invoke-super {p0, p1}, Landroid/app/ListActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 281
    const-string v0, "add_to_circle_data"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->c:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 282
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 286
    invoke-super {p0}, Landroid/app/ListActivity;->onStart()V

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 292
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 296
    invoke-super {p0}, Landroid/app/ListActivity;->onStop()V

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 301
    :cond_1
    return-void
.end method
