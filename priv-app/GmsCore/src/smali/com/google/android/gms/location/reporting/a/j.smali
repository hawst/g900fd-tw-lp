.class final Lcom/google/android/gms/location/reporting/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/location/reporting/h;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Lcom/google/android/gms/location/reporting/ReportingState;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/location/reporting/ReportingState;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/location/reporting/a/j;->a:Lcom/google/android/gms/common/api/Status;

    .line 40
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    if-nez v0, :cond_0

    .line 41
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 43
    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/location/reporting/a/j;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    .line 44
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/a/j;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/a/j;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->c()Z

    move-result v0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/a/j;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->e()Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/a/j;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->f()Z

    move-result v0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/location/reporting/a/j;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->h()Z

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ReportingStateResultImpl{mStatus="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/reporting/a/j;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mReportingState="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/location/reporting/a/j;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
