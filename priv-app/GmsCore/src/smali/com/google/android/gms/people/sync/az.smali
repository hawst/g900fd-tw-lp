.class abstract Lcom/google/android/gms/people/sync/az;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/sync/ap;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/sync/ap;)V
    .locals 0

    .prologue
    .line 2674
    iput-object p1, p0, Lcom/google/android/gms/people/sync/az;->a:Lcom/google/android/gms/people/sync/ap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/sync/ap;B)V
    .locals 0

    .prologue
    .line 2674
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/az;-><init>(Lcom/google/android/gms/people/sync/ap;)V

    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method protected abstract a(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method public final a(JLjava/lang/Object;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/people/sync/y;)V
    .locals 3

    .prologue
    .line 2683
    iget-object v0, p0, Lcom/google/android/gms/people/sync/az;->a:Lcom/google/android/gms/people/sync/ap;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/ap;->d(Lcom/google/android/gms/people/sync/ap;)Landroid/content/ContentValues;

    move-result-object v0

    .line 2685
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 2686
    const-string v1, "container_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2688
    const-string v1, "item_type"

    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/az;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2689
    const-string v1, "is_edge_key"

    invoke-interface {p4, p3}, Lcom/google/android/gms/people/sync/j;->a(Ljava/lang/Object;)Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/people/sync/e;->d(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 2692
    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/people/sync/az;->a(Landroid/content/ContentValues;Ljava/lang/Object;)V

    .line 2694
    const-string v1, "value"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2699
    :goto_0
    return-void

    .line 2697
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/az;->a:Lcom/google/android/gms/people/sync/ap;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/ap;->e(Lcom/google/android/gms/people/sync/ap;)Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "ac_item"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/people/c/e;->b(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2698
    invoke-virtual {p0, p5}, Lcom/google/android/gms/people/sync/az;->a(Lcom/google/android/gms/people/sync/y;)V

    goto :goto_0
.end method

.method public final a(JLjava/lang/Object;Lcom/google/android/gms/people/sync/y;)V
    .locals 7

    .prologue
    .line 2704
    iget-object v0, p0, Lcom/google/android/gms/people/sync/az;->a:Lcom/google/android/gms/people/sync/ap;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/ap;->d(Lcom/google/android/gms/people/sync/ap;)Landroid/content/ContentValues;

    move-result-object v0

    .line 2706
    invoke-virtual {v0}, Landroid/content/ContentValues;->clear()V

    .line 2707
    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/people/sync/az;->b(Landroid/content/ContentValues;Ljava/lang/Object;)V

    .line 2708
    invoke-virtual {v0}, Landroid/content/ContentValues;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 2709
    iget-object v1, p0, Lcom/google/android/gms/people/sync/az;->a:Lcom/google/android/gms/people/sync/ap;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/ap;->e(Lcom/google/android/gms/people/sync/ap;)Lcom/google/android/gms/people/c/e;

    move-result-object v1

    const-string v2, "ac_item"

    const-string v3, "container_id = ? AND item_type = ? AND value = ?"

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/az;->a()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p3}, Lcom/google/android/gms/people/sync/az;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2712
    invoke-virtual {p0, p4}, Lcom/google/android/gms/people/sync/az;->b(Lcom/google/android/gms/people/sync/y;)V

    .line 2714
    :cond_0
    return-void
.end method

.method protected abstract a(Landroid/content/ContentValues;Ljava/lang/Object;)V
.end method

.method protected abstract a(Lcom/google/android/gms/people/sync/y;)V
.end method

.method protected abstract b(Landroid/content/ContentValues;Ljava/lang/Object;)V
.end method

.method protected abstract b(Lcom/google/android/gms/people/sync/y;)V
.end method
