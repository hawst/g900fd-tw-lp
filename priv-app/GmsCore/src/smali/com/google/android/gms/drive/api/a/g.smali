.class public final Lcom/google/android/gms/drive/api/a/g;
.super Lcom/google/android/gms/drive/api/a/c;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/c;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/g;->e:Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;)V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/g;->e:Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;

    const-string v1, "checkResourceIds request may not be null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/g;->e:Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;->b()Ljava/util/List;

    move-result-object v0

    const-string v1, "Invalid checkResourceIds request: no resource ids provided."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/g;->e:Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x32

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Number of resource ids must be less than or equal to 50"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/g;->a:Lcom/google/android/gms/drive/api/d;

    new-instance v1, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/g;->e:Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CheckResourceIdsExistRequest;->b()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/api/d;->a(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/g;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnResourceIdSetResponse;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/internal/OnResourceIdSetResponse;-><init>(Ljava/util/Set;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnResourceIdSetResponse;)V

    .line 48
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
