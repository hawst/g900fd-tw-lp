.class public final Lcom/google/android/gms/usagereporting/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/usagereporting/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/usagereporting/a/l;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/usagereporting/a/l;-><init>(Lcom/google/android/gms/usagereporting/a/k;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/usagereporting/a/m;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/usagereporting/a/m;-><init>(Lcom/google/android/gms/usagereporting/a/k;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/usagereporting/f;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 57
    new-instance v0, Lcom/google/android/gms/usagereporting/a/n;

    invoke-direct {v0, p0, p1, p2, p1}, Lcom/google/android/gms/usagereporting/a/n;-><init>(Lcom/google/android/gms/usagereporting/a/k;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/usagereporting/f;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
