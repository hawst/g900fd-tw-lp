.class final Lcom/google/android/gms/auth/be/proximity/authorization/bt/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/a;->a:Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 149
    .line 151
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/a;->a:Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)Lcom/google/android/gms/auth/trustagent/at;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/at;->c()Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 156
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/a;->a:Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;

    invoke-static {v2}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->c(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)Lcom/google/android/gms/auth/be/proximity/authorization/a;

    move-result-object v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/a;->a:Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->b(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->a()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    :cond_0
    :goto_1
    iget-object v3, v2, Lcom/google/android/gms/auth/be/proximity/authorization/a;->a:Ljava/util/Set;

    monitor-enter v3

    :try_start_1
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, v2, Lcom/google/android/gms/auth/be/proximity/authorization/a;->a:Ljava/util/Set;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/b;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 152
    :catch_0
    move-exception v0

    .line 153
    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->e()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Cannot get trusted state."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_0

    .line 156
    :cond_1
    const/4 v1, 0x1

    goto :goto_1

    :cond_2
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void
.end method
