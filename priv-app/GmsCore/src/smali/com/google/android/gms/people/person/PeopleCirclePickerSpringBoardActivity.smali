.class public Lcom/google/android/gms/people/person/PeopleCirclePickerSpringBoardActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 49
    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/people/person/PeopleCirclePickerSpringBoardActivity;->finish()V

    .line 51
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 26
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 31
    if-eqz p1, :cond_0

    .line 45
    :goto_0
    return-void

    .line 35
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/person/PeopleCirclePickerSpringBoardActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 36
    const-string v0, "account"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 37
    const-string v1, "qualified_id"

    invoke-virtual {v2, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 38
    const-string v4, "circle_ids"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 40
    invoke-static {}, Lcom/google/android/gms/common/audience/a/i;->a()Lcom/google/android/gms/common/audience/a/k;

    move-result-object v4

    .line 41
    invoke-interface {v4, v0}, Lcom/google/android/gms/common/audience/a/k;->m(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    .line 42
    invoke-interface {v4, v1}, Lcom/google/android/gms/common/audience/a/k;->j(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/k;

    .line 43
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, v2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v5, v2

    move v0, v3

    :goto_1
    if-ge v0, v5, :cond_1

    aget-object v6, v2, v0

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v6

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    invoke-interface {v4, v1}, Lcom/google/android/gms/common/audience/a/k;->d(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/k;

    .line 44
    invoke-interface {v4}, Lcom/google/android/gms/common/audience/a/k;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/google/android/gms/people/person/PeopleCirclePickerSpringBoardActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method
