.class final Lcom/google/android/gms/fitness/d/m;
.super Lcom/google/android/gms/fitness/d/q;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/v;

.field final synthetic b:Lcom/google/android/gms/fitness/d/j;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/d/j;Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/gms/fitness/d/m;->b:Lcom/google/android/gms/fitness/d/j;

    iput-object p2, p0, Lcom/google/android/gms/fitness/d/m;->a:Lcom/google/android/gms/common/api/v;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/d/q;-><init>()V

    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    .line 141
    invoke-super {p0, p1}, Lcom/google/android/gms/fitness/d/q;->b_(Landroid/os/Bundle;)V

    .line 142
    iget-object v2, p0, Lcom/google/android/gms/fitness/d/m;->b:Lcom/google/android/gms/fitness/d/j;

    iget-object v3, p0, Lcom/google/android/gms/fitness/d/m;->a:Lcom/google/android/gms/common/api/v;

    const-string v0, "Sending accounts"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    const-string v0, "/fitness/WearableAccountManager/all_accounts"

    invoke-static {v0}, Lcom/google/android/gms/wearable/w;->a(Ljava/lang/String;)Lcom/google/android/gms/wearable/w;

    move-result-object v4

    iget-object v5, v4, Lcom/google/android/gms/wearable/w;->a:Lcom/google/android/gms/wearable/m;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/d/a;->b()Ljava/util/Set;

    move-result-object v0

    new-instance v6, Lcom/google/android/gms/wearable/m;

    invoke-direct {v6}, Lcom/google/android/gms/wearable/m;-><init>()V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/fitness/d/a;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    new-instance v8, Lcom/google/android/gms/wearable/m;

    invoke-direct {v8}, Lcom/google/android/gms/wearable/m;-><init>()V

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/fitness/d/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v10

    const-string v11, "Serializing %s scopes for package %s"

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v10, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    invoke-static {v11, v12}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11, v10}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v8, v1, v11}, Lcom/google/android/gms/wearable/m;->b(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v6, v0, v8}, Lcom/google/android/gms/wearable/m;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/m;)V

    goto :goto_0

    :cond_2
    const-string v0, "Serialized accounts: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v6, v1, v7

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v5, v6}, Lcom/google/android/gms/wearable/m;->a(Lcom/google/android/gms/wearable/m;)V

    sget-object v0, Lcom/google/android/gms/wearable/z;->a:Lcom/google/android/gms/wearable/d;

    invoke-virtual {v4}, Lcom/google/android/gms/wearable/w;->a()Lcom/google/android/gms/wearable/PutDataRequest;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Lcom/google/android/gms/wearable/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/d/n;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/fitness/d/n;-><init>(Lcom/google/android/gms/fitness/d/j;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 143
    return-void
.end method
