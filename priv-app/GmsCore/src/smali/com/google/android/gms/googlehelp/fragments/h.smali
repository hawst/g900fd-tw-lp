.class public final Lcom/google/android/gms/googlehelp/fragments/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/f/k;


# instance fields
.field public final a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field public final b:Lcom/google/android/gms/googlehelp/search/c;

.field public final c:Landroid/view/View;

.field public final d:Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;

.field public final e:Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;

.field public final f:Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;

.field public g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

.field private final h:Landroid/support/v4/app/q;

.field private final i:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private final j:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 6

    .prologue
    const/16 v5, 0x8

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->h:Landroid/support/v4/app/q;

    .line 61
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->i:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->i:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/google/android/gms/googlehelp/search/c;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    sget v3, Lcom/google/android/gms/j;->iX:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    sget v4, Lcom/google/android/gms/j;->hI:I

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/googlehelp/search/c;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Landroid/view/View;I)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->b:Lcom/google/android/gms/googlehelp/search/c;

    .line 70
    sget v0, Lcom/google/android/gms/j;->ik:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->c:Landroid/view/View;

    .line 72
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v2

    .line 73
    sget v0, Lcom/google/android/gms/j;->iU:I

    invoke-virtual {v2, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->d:Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;

    .line 75
    sget v0, Lcom/google/android/gms/j;->iH:I

    invoke-virtual {v2, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->e:Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;

    .line 77
    sget v0, Lcom/google/android/gms/j;->hE:I

    invoke-virtual {v2, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->f:Lcom/google/android/gms/googlehelp/fragments/BrowseAllArticlesFragment;

    .line 79
    sget v0, Lcom/google/android/gms/j;->iq:I

    invoke-virtual {v2, v0}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->j:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->i:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->O()Z

    move-result v0

    if-eqz v0, :cond_2

    sget v0, Lcom/google/android/gms/j;->hM:I

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v3, Lcom/google/android/gms/j;->hK:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    move v1, v0

    :goto_1
    invoke-virtual {v2, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    if-nez v0, :cond_0

    invoke-virtual {v2}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    invoke-direct {v2}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/h;->g:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 83
    :cond_0
    return-void

    .line 65
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 82
    :cond_2
    sget v0, Lcom/google/android/gms/j;->hK:I

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->getView()Landroid/view/View;

    move-result-object v1

    sget v3, Lcom/google/android/gms/j;->hM:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    move v1, v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/t;)V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->e:Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a(Lcom/google/android/gms/googlehelp/common/t;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v1, "SHOWN_SUGGESTIONS"

    const-string v2, ""

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/t;Ljava/lang/String;)V

    .line 127
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/f/a;)V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->j:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->a(Lcom/google/android/gms/googlehelp/f/a;)V

    .line 156
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v8, 0x0

    .line 160
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Lcom/google/android/gms/googlehelp/common/t;

    .line 161
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/googlehelp/common/t;

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->h:Landroid/support/v4/app/q;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 163
    invoke-virtual {v5}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 164
    invoke-virtual {p0, v5}, Lcom/google/android/gms/googlehelp/fragments/h;->a(Lcom/google/android/gms/googlehelp/common/t;)V

    .line 165
    invoke-virtual {p0, v4}, Lcom/google/android/gms/googlehelp/fragments/h;->b(Lcom/google/android/gms/googlehelp/common/t;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->j:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->dismiss()V

    .line 169
    :cond_0
    new-instance v0, Lcom/google/android/gms/googlehelp/f/g;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/h;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const-string v2, ""

    invoke-virtual {v5}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v3, v6

    :goto_0
    invoke-virtual {v5}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v7

    if-eqz v7, :cond_2

    :goto_1
    invoke-virtual {v5}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/f/g;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/util/Map;Lcom/google/android/gms/googlehelp/common/t;Z)V

    new-array v1, v8, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/g;->a([Ljava/lang/Object;)V

    .line 184
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->h:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->supportInvalidateOptionsMenu()V

    .line 185
    return-void

    .line 169
    :cond_1
    iget-object v3, v5, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    goto :goto_0

    :cond_2
    move-object v4, v6

    goto :goto_1

    .line 178
    :cond_3
    invoke-virtual {v5}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->i:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->M()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->i:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->N()Ljava/util/List;

    move-result-object v0

    const-string v1, "offline_suggestions"

    invoke-static {v1}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v1

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/k;->c(Lcom/google/android/gms/googlehelp/common/k;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    new-instance v5, Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v0

    const-string v1, "SUGGESTION_CLICKED"

    const-string v3, ""

    invoke-direct {v5, v0, v2, v1, v3}, Lcom/google/android/gms/googlehelp/common/t;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0, v5}, Lcom/google/android/gms/googlehelp/fragments/h;->a(Lcom/google/android/gms/googlehelp/common/t;)V

    .line 181
    invoke-virtual {p0, v4}, Lcom/google/android/gms/googlehelp/fragments/h;->b(Lcom/google/android/gms/googlehelp/common/t;)V

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->j:Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContentLoadingFragment;->dismiss()V

    goto :goto_2
.end method

.method public final b(Lcom/google/android/gms/googlehelp/common/t;)V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/h;->d:Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a(Lcom/google/android/gms/googlehelp/common/t;)V

    .line 131
    return-void
.end method
