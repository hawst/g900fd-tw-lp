.class public Lcom/google/android/gms/reminders/provider/RemindersProvider;
.super Lcom/android/a/a/c;
.source "SourceFile"


# static fields
.field private static final b:Landroid/content/UriMatcher;

.field private static final c:Ljava/util/Map;

.field private static final d:Ljava/util/Map;

.field private static final e:Ljava/util/Map;

.field private static final f:Ljava/util/Map;

.field private static final g:Ljava/util/Map;

.field private static final h:Ljava/util/Map;


# instance fields
.field private volatile i:Z

.field private volatile j:Z

.field private final k:Ljava/lang/ThreadLocal;

.field private l:Landroid/database/sqlite/SQLiteOpenHelper;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v0, 0x0

    .line 59
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    .line 61
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->c:Ljava/util/Map;

    .line 62
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->d:Ljava/util/Map;

    .line 63
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->e:Ljava/util/Map;

    .line 64
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->f:Ljava/util/Map;

    .line 65
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->g:Ljava/util/Map;

    .line 66
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->h:Ljava/util/Map;

    .line 88
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "account"

    const/16 v4, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 89
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "account/#"

    const/16 v4, 0x65

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 90
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "reminders"

    const/16 v4, 0xc8

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 91
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "reminders/#"

    const/16 v4, 0xc9

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 92
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "reminders/upsert"

    const/16 v4, 0xca

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 93
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "notification"

    const/16 v4, 0x12c

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 94
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "notification/#"

    const/16 v4, 0x12d

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 95
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "updated_notifications"

    const/16 v4, 0x12e

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 96
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "reminder_events"

    const/16 v4, 0x190

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 97
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.reminders"

    const-string v3, "packages"

    const/16 v4, 0x1f4

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 99
    new-array v2, v10, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v0

    const-string v1, "account_name"

    aput-object v1, v2, v8

    const-string v1, "storage_version"

    aput-object v1, v2, v9

    .line 105
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 106
    sget-object v5, Lcom/google/android/gms/reminders/provider/RemindersProvider;->c:Ljava/util/Map;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "account."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 109
    :cond_0
    const/16 v1, 0x26

    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v2, v0

    const-string v1, "account_id"

    aput-object v1, v2, v8

    const-string v1, "is_dirty"

    aput-object v1, v2, v9

    const-string v1, "client_assigned_id"

    aput-object v1, v2, v10

    const-string v1, "server_assigned_id"

    aput-object v1, v2, v11

    const/4 v1, 0x5

    const-string v3, "client_assigned_thread_id"

    aput-object v3, v2, v1

    const/4 v1, 0x6

    const-string v3, "task_list"

    aput-object v3, v2, v1

    const/4 v1, 0x7

    const-string v3, "title"

    aput-object v3, v2, v1

    const/16 v1, 0x8

    const-string v3, "created_time_millis"

    aput-object v3, v2, v1

    const/16 v1, 0x9

    const-string v3, "archived_time_ms"

    aput-object v3, v2, v1

    const/16 v1, 0xa

    const-string v3, "archived"

    aput-object v3, v2, v1

    const/16 v1, 0xb

    const-string v3, "deleted"

    aput-object v3, v2, v1

    const/16 v1, 0xc

    const-string v3, "pinned"

    aput-object v3, v2, v1

    const/16 v1, 0xd

    const-string v3, "snoozed"

    aput-object v3, v2, v1

    const/16 v1, 0xe

    const-string v3, "snoozed_time_millis"

    aput-object v3, v2, v1

    const/16 v1, 0xf

    const-string v3, "location_snoozed_until_ms"

    aput-object v3, v2, v1

    const/16 v1, 0x10

    const-string v3, "due_date_year"

    aput-object v3, v2, v1

    const/16 v1, 0x11

    const-string v3, "due_date_month"

    aput-object v3, v2, v1

    const/16 v1, 0x12

    const-string v3, "due_date_day"

    aput-object v3, v2, v1

    const/16 v1, 0x13

    const-string v3, "due_date_hour"

    aput-object v3, v2, v1

    const/16 v1, 0x14

    const-string v3, "due_date_minute"

    aput-object v3, v2, v1

    const/16 v1, 0x15

    const-string v3, "due_date_second"

    aput-object v3, v2, v1

    const/16 v1, 0x16

    const-string v3, "due_date_period"

    aput-object v3, v2, v1

    const/16 v1, 0x17

    const-string v3, "due_date_absolute_time_ms"

    aput-object v3, v2, v1

    const/16 v1, 0x18

    const-string v3, "event_date_year"

    aput-object v3, v2, v1

    const/16 v1, 0x19

    const-string v3, "event_date_month"

    aput-object v3, v2, v1

    const/16 v1, 0x1a

    const-string v3, "event_date_day"

    aput-object v3, v2, v1

    const/16 v1, 0x1b

    const-string v3, "event_date_hour"

    aput-object v3, v2, v1

    const/16 v1, 0x1c

    const-string v3, "event_date_minute"

    aput-object v3, v2, v1

    const/16 v1, 0x1d

    const-string v3, "event_date_second"

    aput-object v3, v2, v1

    const/16 v1, 0x1e

    const-string v3, "event_date_period"

    aput-object v3, v2, v1

    const/16 v1, 0x1f

    const-string v3, "event_date_absolute_time_ms"

    aput-object v3, v2, v1

    const/16 v1, 0x20

    const-string v3, "lat"

    aput-object v3, v2, v1

    const/16 v1, 0x21

    const-string v3, "lng"

    aput-object v3, v2, v1

    const/16 v1, 0x22

    const-string v3, "name"

    aput-object v3, v2, v1

    const/16 v1, 0x23

    const-string v3, "radius_meters"

    aput-object v3, v2, v1

    const/16 v1, 0x24

    const-string v3, "location_type"

    aput-object v3, v2, v1

    const/16 v1, 0x25

    const-string v3, "display_address"

    aput-object v3, v2, v1

    .line 153
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 154
    sget-object v5, Lcom/google/android/gms/reminders/provider/RemindersProvider;->d:Ljava/util/Map;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "reminders."

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 153
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 157
    :cond_1
    const/16 v1, 0x8

    new-array v3, v1, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v3, v0

    const-string v1, "is_stale"

    aput-object v1, v3, v8

    const-string v1, "trigger_time"

    aput-object v1, v3, v9

    const-string v1, "create_time"

    aput-object v1, v3, v10

    const-string v1, "schedule_time"

    aput-object v1, v3, v11

    const/4 v1, 0x5

    const-string v4, "fire_time"

    aput-object v4, v3, v1

    const/4 v1, 0x6

    const-string v4, "snooze_time"

    aput-object v4, v3, v1

    const/4 v1, 0x7

    const-string v4, "dismiss_time"

    aput-object v4, v3, v1

    .line 168
    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_2

    aget-object v5, v3, v1

    .line 169
    sget-object v6, Lcom/google/android/gms/reminders/provider/RemindersProvider;->e:Ljava/util/Map;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "notification."

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 172
    :cond_2
    array-length v1, v2

    :goto_3
    if-ge v0, v1, :cond_3

    aget-object v3, v2, v0

    .line 173
    sget-object v4, Lcom/google/android/gms/reminders/provider/RemindersProvider;->f:Ljava/util/Map;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "reminders."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 172
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 176
    :cond_3
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->f:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->e:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 178
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->g:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->d:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 179
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->g:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "reminders._id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->g:Ljava/util/Map;

    const-string v1, "account_name"

    const-string v2, "account.account_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->h:Ljava/util/Map;

    const-string v1, "_id"

    const-string v2, "package._id"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->h:Ljava/util/Map;

    const-string v1, "package_name"

    const-string v2, "package.package_name"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->h:Ljava/util/Map;

    const-string v1, "is_google_signed"

    const-string v2, "package.is_google_signed"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/android/a/a/c;-><init>()V

    .line 82
    iput-boolean v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->i:Z

    .line 83
    iput-boolean v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->j:Z

    .line 85
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->k:Ljava/lang/ThreadLocal;

    return-void
.end method

.method private a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v2, 0x1

    .line 436
    const-string v0, "is_dirty"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Missing is_dirty when updating reminders"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 438
    const-string v0, "is_dirty"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 439
    iput-boolean v2, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->i:Z

    .line 442
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->j:Z

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "reminders"

    invoke-virtual {v0, v1, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v8

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "reminders"

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v4

    move-object v3, p2

    move-object v4, p3

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 445
    :cond_2
    return v8
.end method

.method private a(Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 305
    const-string v0, "is_dirty"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Missing is_dirty when inserting reminder"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 307
    const-string v0, "is_dirty"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v2, :cond_0

    .line 308
    iput-boolean v2, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->i:Z

    .line 311
    :cond_0
    iput-boolean v2, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->j:Z

    .line 313
    const-string v0, "due_date_hour"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "due_date_minute"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_1

    const-string v0, "due_date_second"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_2

    :cond_1
    const-string v0, "due_date_hour"

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v0, "due_date_minute"

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v0, "due_date_second"

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    :cond_2
    const-string v0, "event_date_hour"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "event_date_minute"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "event_date_second"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_4

    :cond_3
    const-string v0, "event_date_hour"

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v0, "event_date_minute"

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    const-string v0, "event_date_second"

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 314
    :cond_4
    const-string v0, "reminders"

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 316
    if-eqz v0, :cond_5

    .line 317
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(J)V

    .line 320
    :cond_5
    return-object v0
.end method

.method private a(Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 297
    iget-object v1, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1, p1, v0, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 298
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 301
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p2, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private a(J)V
    .locals 3

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->k:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->k:Ljava/lang/ThreadLocal;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 470
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->k:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 471
    return-void
.end method

.method private static a(Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 492
    invoke-virtual {p0, p1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    if-nez v0, :cond_0

    .line 493
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 495
    :cond_0
    return-void
.end method

.method private b(Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 324
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 325
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v4

    :goto_0
    const-string v1, "Missing package_namewhen updating package table"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "package"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "package_name"

    aput-object v3, v2, v4

    const/4 v3, 0x2

    const-string v8, "is_google_signed"

    aput-object v8, v2, v3

    const-string v3, "package_name=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object v7, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 332
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 333
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "package"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "_id="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v0, v4, p1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 335
    sget-object v0, Lcom/google/android/gms/reminders/internal/a/i;->a:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 340
    if-eqz v1, :cond_0

    .line 341
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_0
    :goto_1
    return-object v0

    :cond_1
    move v0, v6

    .line 325
    goto :goto_0

    .line 337
    :cond_2
    :try_start_1
    const-string v0, "package"

    sget-object v2, Lcom/google/android/gms/reminders/internal/a/i;->a:Landroid/net/Uri;

    invoke-direct {p0, v0, v2, p1}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 340
    if-eqz v1, :cond_0

    .line 341
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 340
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    .line 341
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private c(Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v2, 0x1

    const/4 v7, 0x0

    .line 350
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Missing account_id when upserting reminder"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 352
    const-string v0, "client_assigned_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "Missing client_assigned_id when upserting reminder"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 355
    const-string v0, "server_assigned_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 357
    const-string v3, "(server_assigned_id=? OR client_assigned_id=?) AND account_id=?"

    .line 360
    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const-string v0, "server_assigned_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v0, "client_assigned_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 371
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "reminders"

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v2, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 375
    if-eqz v1, :cond_0

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 376
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 384
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v0

    .line 365
    :cond_1
    const-string v3, "client_assigned_id=? AND account_id=?"

    .line 366
    new-array v4, v6, [Ljava/lang/String;

    const-string v0, "client_assigned_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v2

    goto :goto_0

    .line 378
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 379
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 380
    const-string v0, "_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-direct {p0, p1, v0, v4}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 381
    sget-object v0, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 384
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method protected final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 391
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 392
    sparse-switch v1, :sswitch_data_0

    .line 432
    :goto_0
    return v0

    .line 395
    :sswitch_0
    const/16 v2, 0x65

    if-ne v1, v2, :cond_0

    .line 396
    const-string p3, "_id=?"

    .line 397
    new-array p4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p4, v0

    .line 399
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "account"

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 403
    :sswitch_1
    const/16 v2, 0xc9

    if-ne v1, v2, :cond_1

    .line 404
    const-string p3, "_id=?"

    .line 405
    new-array p4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p4, v0

    .line 407
    :cond_1
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 411
    :sswitch_2
    const/16 v2, 0x12d

    if-ne v1, v2, :cond_2

    .line 412
    const-string p3, "_id=?"

    .line 413
    new-array p4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p4, v0

    .line 416
    :cond_2
    const-string v0, "state"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 417
    const-string v0, "state"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 418
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 419
    if-ne v0, v4, :cond_4

    .line 420
    const-string v0, "schedule_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 429
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "notification"

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 421
    :cond_4
    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 422
    const-string v0, "fire_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 423
    :cond_5
    const/4 v1, 0x3

    if-ne v0, v1, :cond_6

    .line 424
    const-string v0, "snooze_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 425
    :cond_6
    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 426
    const-string v0, "dismiss_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 392
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xc9 -> :sswitch_1
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 499
    sget-object v1, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 500
    sparse-switch v1, :sswitch_data_0

    .line 534
    :goto_0
    return v0

    .line 503
    :sswitch_0
    const/16 v2, 0x65

    if-ne v1, v2, :cond_0

    .line 504
    const-string p2, "_id=?"

    .line 505
    new-array p3, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p3, v0

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "account"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 511
    :sswitch_1
    const/16 v2, 0xc9

    if-ne v1, v2, :cond_1

    .line 512
    const-string p2, "_id=?"

    .line 513
    new-array p3, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p3, v0

    .line 516
    :cond_1
    const-string v0, "Cannot delete reminders with null selection"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 518
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "reminders"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 519
    iput-boolean v3, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->j:Z

    goto :goto_0

    .line 524
    :sswitch_2
    const/16 v2, 0x12d

    if-ne v1, v2, :cond_2

    .line 525
    const-string p2, "_id=?"

    .line 526
    new-array p3, v3, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p3, v0

    .line 528
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "notification"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 531
    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "package"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 500
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xc9 -> :sswitch_1
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x1f4 -> :sswitch_3
    .end sparse-switch
.end method

.method protected final a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteOpenHelper;
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->l:Landroid/database/sqlite/SQLiteOpenHelper;

    if-nez v0, :cond_0

    .line 203
    invoke-static {p1}, Lcom/google/android/gms/reminders/provider/a;->a(Landroid/content/Context;)Lcom/google/android/gms/reminders/provider/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->l:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 205
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->l:Landroid/database/sqlite/SQLiteOpenHelper;

    return-object v0
.end method

.method protected final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 282
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 283
    sparse-switch v0, :sswitch_data_0

    .line 293
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 285
    :sswitch_0
    const-string v0, "account"

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Ljava/lang/String;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 287
    :sswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 289
    :sswitch_2
    invoke-direct {p0, p2}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->c(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 291
    :sswitch_3
    invoke-direct {p0, p2}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b(Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 283
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xca -> :sswitch_2
        0x1f4 -> :sswitch_3
    .end sparse-switch
.end method

.method protected final a()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 539
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 541
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->k:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 542
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 543
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v5, v1, [J

    move v2, v3

    .line 544
    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_0

    .line 545
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v5, v2

    .line 544
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 547
    :cond_0
    invoke-static {v4, v5}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->a(Landroid/content/Context;[J)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 548
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->k:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->remove()V

    .line 552
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->i:Z

    if-eqz v0, :cond_2

    .line 553
    iput-boolean v3, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->i:Z

    .line 554
    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/f;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v5, 0x1

    invoke-virtual {v0, v1, v2, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 557
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->j:Z

    if-eqz v0, :cond_3

    .line 558
    iput-boolean v3, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->j:Z

    .line 559
    invoke-static {v4}, Lcom/google/android/gms/reminders/notification/NotificationService;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 561
    :cond_3
    return-void
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 193
    invoke-super {p0}, Lcom/android/a/a/c;->onCreate()Z

    .line 194
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 195
    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/reminders/a/b;

    invoke-direct {v2, v0}, Lcom/google/android/gms/reminders/a/b;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    .line 196
    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/provider/RemindersProvider;->a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->l:Landroid/database/sqlite/SQLiteOpenHelper;

    .line 197
    return v4
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 223
    sget-object v0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->b:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/reminders/provider/RemindersProvider;->l:Landroid/database/sqlite/SQLiteOpenHelper;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 226
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 231
    const/16 v3, 0xe

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 232
    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setStrict(Z)V

    .line 235
    :cond_0
    sparse-switch v2, :sswitch_data_0

    .line 277
    :goto_0
    return-object v5

    .line 238
    :sswitch_0
    const/16 v3, 0x65

    if-ne v2, v3, :cond_2

    .line 239
    const-string v3, "_id=?"

    .line 240
    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v6

    .line 242
    :goto_1
    const-string v2, "account"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 243
    sget-object v2, Lcom/google/android/gms/reminders/provider/RemindersProvider;->c:Ljava/util/Map;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object v2, p2

    move-object v6, v5

    move-object v7, p5

    .line 244
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 248
    :sswitch_1
    const/16 v3, 0xc9

    if-ne v2, v3, :cond_1

    .line 249
    const-string v3, "reminders._id=?"

    .line 250
    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v6

    .line 252
    :goto_2
    const-string v2, "reminders"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 253
    sget-object v2, Lcom/google/android/gms/reminders/provider/RemindersProvider;->d:Ljava/util/Map;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object v2, p2

    move-object v6, v5

    move-object v7, p5

    .line 254
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 258
    :sswitch_2
    const-string v2, "notification LEFT OUTER JOIN reminders ON reminders._id = notification._id"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 259
    sget-object v2, Lcom/google/android/gms/reminders/provider/RemindersProvider;->f:Ljava/util/Map;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 260
    const-string v3, "notification.is_stale=1"

    move-object v2, p2

    move-object v4, v5

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 266
    :sswitch_3
    const-string v2, "reminders LEFT OUTER JOIN account ON reminders.account_id = account._id"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 267
    sget-object v2, Lcom/google/android/gms/reminders/provider/RemindersProvider;->g:Ljava/util/Map;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, v5

    .line 268
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    .line 272
    :sswitch_4
    const-string v2, "package"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 273
    sget-object v2, Lcom/google/android/gms/reminders/provider/RemindersProvider;->h:Ljava/util/Map;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, p5

    .line 274
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    :cond_1
    move-object v4, p4

    move-object v3, p3

    goto :goto_2

    :cond_2
    move-object v4, p4

    move-object v3, p3

    goto :goto_1

    .line 235
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_0
        0xc8 -> :sswitch_1
        0xc9 -> :sswitch_1
        0x12e -> :sswitch_2
        0x190 -> :sswitch_3
        0x1f4 -> :sswitch_4
    .end sparse-switch
.end method
