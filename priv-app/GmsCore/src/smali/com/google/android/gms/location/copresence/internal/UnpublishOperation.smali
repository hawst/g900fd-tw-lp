.class public Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/x;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->c:I

    .line 53
    iput p2, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    .line 54
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    .line 55
    return-void
.end method

.method private constructor <init>(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->c:I

    .line 59
    iput p1, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    .line 60
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    .line 61
    return-void
.end method

.method public static a()Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;
    .locals 3

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;-><init>(ILjava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 70
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "PublishId cannot be null or empty"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 72
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;-><init>(ILjava/lang/String;)V

    return-object v0

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final b()I
    .locals 1

    .prologue
    .line 76
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 96
    if-ne p0, p1, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 99
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 100
    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_3
    check-cast p1, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;

    .line 106
    iget v2, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    iget v3, p1, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->a:I

    packed-switch v0, :pswitch_data_0

    .line 117
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 113
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unpublish(id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 115
    :pswitch_1
    const-string v0, "UnpublishAll"

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 86
    invoke-static {p0, p1}, Lcom/google/android/gms/location/copresence/internal/x;->a(Lcom/google/android/gms/location/copresence/internal/UnpublishOperation;Landroid/os/Parcel;)V

    .line 87
    return-void
.end method
