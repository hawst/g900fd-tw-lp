.class public final Lcom/google/android/gms/drive/database/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/r;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/i;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/drive/g/i;

.field private final d:Lcom/google/android/gms/drive/database/model/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/g/i;)V
    .locals 4

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-object p2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    .line 167
    iput-object p1, p0, Lcom/google/android/gms/drive/database/d;->b:Landroid/content/Context;

    .line 168
    iput-object p3, p0, Lcom/google/android/gms/drive/database/d;->c:Lcom/google/android/gms/drive/g/i;

    .line 169
    new-instance v0, Lcom/google/android/gms/drive/database/model/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/model/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/database/d;->d:Lcom/google/android/gms/drive/database/model/b;

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    new-instance v1, Lcom/google/android/gms/drive/database/k;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/database/k;-><init>(Lcom/google/android/gms/drive/database/i;)V

    iget-object v2, v0, Lcom/google/android/gms/drive/database/i;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/google/android/gms/drive/database/m;

    const-string v3, "Open database in background"

    invoke-direct {v2, v0, v3, v1}, Lcom/google/android/gms/drive/database/m;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Lcom/google/android/gms/drive/g/ak;)V

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/m;->start()V

    .line 171
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    .line 348
    if-eqz p1, :cond_0

    .line 349
    iget-wide v0, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object p3

    .line 351
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v1, p2

    move-object v2, p5

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/Cursor;Lcom/google/android/gms/drive/auth/g;)Landroid/util/SparseArray;
    .locals 6

    .prologue
    .line 587
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    .line 589
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_2

    .line 592
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->S:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_1

    .line 594
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    .line 599
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    .line 600
    new-instance v2, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v2}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    .line 602
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/y;

    .line 603
    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;)Lcom/google/android/gms/drive/metadata/internal/a;

    goto :goto_1

    .line 605
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 590
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 608
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 609
    return-object v1
.end method

.method private static a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 8

    .prologue
    .line 141
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->j:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5, p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->k:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->e()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 147
    return-object v0
.end method

.method private static a(JLjava/lang/String;JZ)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 5

    .prologue
    .line 153
    if-eqz p5, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->a()V

    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IS NOT NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    :goto_0
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    const/4 v3, 0x3

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->W:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->X:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4, p3, p4}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0

    .line 153
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->c()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 378
    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    :goto_0
    return-object p2

    .line 382
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 383
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->c()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v3, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->f(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    :cond_1
    invoke-virtual {p2, v1, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object p2

    goto :goto_0

    .line 384
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->a:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 385
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v0, Lcom/google/android/gms/drive/database/model/ak;->b:Lcom/google/android/gms/drive/database/model/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ak;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iget-wide v2, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v3, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->f(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    :goto_1
    invoke-virtual {p2, v1, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object p2

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->c()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_1

    .line 387
    :cond_4
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v2, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->f(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object p2

    goto/16 :goto_0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "This should never happen, since all AuthorizedApps must have at least one scope."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 453
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->R:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 454
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 1875
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->a:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->a:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->c()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;
    .locals 2

    .prologue
    .line 2017
    new-instance v0, Lcom/google/android/gms/drive/database/b/b;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/drive/database/b/b;-><init>(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)V

    return-object v0
.end method

.method private a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;
    .locals 2

    .prologue
    .line 2021
    new-instance v0, Lcom/google/android/gms/drive/database/b/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/gms/drive/database/b/c;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/f;)V

    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;
    .locals 6

    .prologue
    .line 359
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v2, p2

    .line 368
    :goto_0
    invoke-direct {p0, p1, p4}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    .line 369
    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-direct {p0, v0, p6}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v2, p3

    .line 366
    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1590
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1591
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/aj;->a()Lcom/google/android/gms/drive/database/model/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aj;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1598
    new-instance v1, Lcom/google/android/gms/drive/database/a/d;

    invoke-direct {v1, p1}, Lcom/google/android/gms/drive/database/a/d;-><init>(Lcom/google/android/gms/drive/database/model/ah;)V

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 912
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v1, p1

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/a/h;->a()Lcom/google/android/gms/drive/database/a/h;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/a/f;)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 460
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v1

    .line 462
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 467
    :goto_0
    return-object v5

    .line 465
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/database/b/d;->get(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 467
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0
.end method

.method private static a(Lcom/google/android/gms/drive/database/b/d;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2055
    :try_start_0
    invoke-interface {p0}, Lcom/google/android/gms/drive/database/b/d;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 2056
    invoke-interface {p0}, Lcom/google/android/gms/drive/database/b/d;->close()V

    const/4 v0, 0x0

    .line 2065
    :goto_0
    return-object v0

    .line 2057
    :cond_0
    :try_start_1
    invoke-interface {p0}, Lcom/google/android/gms/drive/database/b/d;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 2058
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Lcom/google/android/gms/drive/database/b/d;->get(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2065
    invoke-interface {p0}, Lcom/google/android/gms/drive/database/b/d;->close()V

    goto :goto_0

    .line 2060
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Result list has "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p0}, Lcom/google/android/gms/drive/database/b/d;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " items, but only 1 was expected."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2065
    :catchall_0
    move-exception v0

    invoke-interface {p0}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0
.end method

.method private a(JLcom/google/android/gms/drive/database/SqlWhereClause;)Ljava/util/List;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 1255
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1256
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 1258
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    const-string v1, "AuthorizedAppScopeView"

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1267
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1268
    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1269
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/f;

    .line 1271
    if-nez v0, :cond_1

    .line 1272
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/database/model/f;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v0

    .line 1273
    iget-wide v10, v0, Lcom/google/android/gms/drive/database/model/f;->a:J

    cmp-long v1, v10, p1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1274
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v6, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1275
    const-class v1, Lcom/google/android/gms/drive/aa;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 1276
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v8, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1280
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/f;->k()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1281
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    sget-object v3, Lcom/google/android/gms/drive/database/model/q;->a:Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/q;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v3, Lcom/google/android/gms/drive/database/model/q;->b:Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/q;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->intValue()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/drive/aa;->a(I)Lcom/google/android/gms/drive/aa;

    move-result-object v3

    new-instance v7, Lcom/google/android/gms/drive/database/model/o;

    invoke-direct {v7, v0, v4, v5, v3}, Lcom/google/android/gms/drive/database/model/o;-><init>(Lcom/google/android/gms/drive/database/i;JLcom/google/android/gms/drive/aa;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/p;->a()Lcom/google/android/gms/drive/database/model/p;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/p;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v7, v4, v5}, Lcom/google/android/gms/drive/database/model/o;->d(J)V

    iget-object v0, v7, Lcom/google/android/gms/drive/database/model/o;->a:Lcom/google/android/gms/drive/aa;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 1284
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1273
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 1278
    :cond_1
    :try_start_1
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1284
    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1287
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 1288
    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Ljava/util/Map$Entry;

    .line 1289
    new-instance v0, Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/drive/database/d;->a(J)Lcom/google/android/gms/drive/database/model/a;

    move-result-object v1

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/database/model/f;

    iget-wide v2, v2, Lcom/google/android/gms/drive/database/model/f;->b:J

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/drive/database/model/f;

    iget-object v4, v4, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/drive/database/model/f;

    iget-wide v5, v5, Lcom/google/android/gms/drive/database/model/f;->d:J

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v8, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Set;

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/auth/g;-><init>(Lcom/google/android/gms/drive/database/model/a;JLcom/google/android/gms/drive/auth/AppIdentity;JLjava/util/Set;)V

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1296
    :cond_3
    return-object v9
.end method

.method private a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;Z)Ljava/util/Set;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1648
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1649
    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v5, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "=?"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v8, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1651
    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/google/android/gms/drive/database/model/k;->e:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "=?"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    if-eqz p3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v7, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1654
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    new-array v7, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    aput-object v5, v7, v3

    invoke-virtual {v0, v4, v7}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1655
    if-eqz p2, :cond_0

    .line 1657
    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/google/android/gms/drive/database/model/k;->c:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, "=?"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v7, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1660
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    aput-object v4, v1, v3

    invoke-virtual {v0, v5, v1}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    move-object v4, v0

    .line 1663
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    const-string v1, "AppDataScopeView"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1672
    :cond_1
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1673
    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/j;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1675
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/i;

    .line 1676
    if-nez v0, :cond_1

    .line 1677
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/i;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/i;

    move-result-object v0

    .line 1678
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v6, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1682
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move v0, v3

    .line 1651
    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 1657
    goto :goto_1

    .line 1682
    :cond_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1685
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1686
    invoke-interface {v6}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1687
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/i;

    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/i;->d:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1689
    :cond_5
    return-object v1
.end method

.method private static a(Landroid/database/Cursor;)Z
    .locals 3

    .prologue
    .line 544
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->c:Lcom/google/android/gms/drive/metadata/internal/a/c;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/a;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 545
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    .line 546
    const/4 v0, 0x0

    .line 549
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/drive/database/SqlWhereClause;)I
    .locals 4

    .prologue
    .line 1300
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/g;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private b(JLcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/auth/g;
    .locals 5

    .prologue
    .line 1320
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/database/d;->a(JLcom/google/android/gms/drive/database/SqlWhereClause;)Ljava/util/List;

    move-result-object v0

    .line 1321
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 1322
    const/4 v0, 0x0

    .line 1324
    :goto_0
    return-object v0

    .line 1323
    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 1324
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/g;

    goto :goto_0

    .line 1328
    :cond_1
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Multiple apps with same account/identity: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
.end method

.method private static b(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 1488
    sget-object v0, Lcom/google/android/gms/drive/database/model/ak;->a:Lcom/google/android/gms/drive/database/model/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ak;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 8

    .prologue
    .line 676
    new-instance v1, Lcom/google/android/gms/drive/database/model/ah;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    move-object v3, p0

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/database/model/ah;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;JLjava/lang/String;Ljava/lang/String;)V

    .line 677
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 683
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->b(Z)V

    .line 685
    :cond_0
    return-object v1
.end method

.method private static c(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 6

    .prologue
    .line 1178
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/h;->d:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1181
    sget-object v1, Lcom/google/android/gms/drive/database/model/h;->e:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/auth/AppIdentity;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    .line 1183
    sget-object v2, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-static {p0, p1}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private static c(JLcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 8

    .prologue
    .line 1828
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v1, Lcom/google/android/gms/drive/database/model/by;->g:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/gms/drive/database/model/by;->b:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    int-to-long v6, p3

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    return-object v0
.end method

.method private d(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/model/f;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1228
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/drive/database/d;->c(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1229
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/g;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1238
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1239
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/f;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v2

    .line 1240
    iget-wide v4, v2, Lcom/google/android/gms/drive/database/model/f;->a:J

    cmp-long v0, v4, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1241
    invoke-interface {v1}, Landroid/database/Cursor;->isLast()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1244
    new-instance v0, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Multiple apps with same account/identity: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1250
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1240
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1250
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v2
.end method

.method private static f(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 443
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private g(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 1697
    iget-object v6, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 1698
    iget-wide v4, v6, Lcom/google/android/gms/drive/database/model/a;->b:J

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Not persisted: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object v6, v5, v3

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 1699
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1700
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v4, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    const-string v4, "App not authorized for appdata"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1703
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "=?"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-wide v8, v6, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v5, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    iget-wide v8, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v7, v8}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v3

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1709
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/j;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1712
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 1715
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1719
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    const-string v1, "application/vnd.google-apps.folder"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "appdata."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v7

    .line 1721
    iget-wide v0, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/Long;)V

    .line 1722
    const/4 v0, 0x0

    invoke-virtual {v7, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 1724
    new-instance v0, Lcom/google/android/gms/drive/database/model/i;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-wide v2, v6, Lcom/google/android/gms/drive/database/model/a;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const/4 v7, 0x1

    const/4 v8, 0x0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/model/i;-><init>(Lcom/google/android/gms/drive/database/i;JJLjava/lang/Long;ZZ)V

    .line 1726
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/i;->i()V

    .line 1730
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1732
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 1733
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1735
    return-object v0

    :cond_0
    move v0, v3

    .line 1698
    goto/16 :goto_0

    .line 1728
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v9}, Lcom/google/android/gms/drive/database/model/i;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/i;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 1732
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 1733
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private h(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/d;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/database/model/e;->a:Lcom/google/android/gms/drive/database/model/e;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/e;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 229
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 230
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 234
    :goto_0
    return-object v2

    .line 232
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/c;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 234
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->s:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private static i(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    .prologue
    .line 1190
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/h;->d:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private j(Ljava/lang/String;)J
    .locals 12

    .prologue
    const-wide/16 v6, -0x1

    const/4 v2, 0x0

    .line 1434
    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->i(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1436
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/g;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move-wide v0, v6

    .line 1446
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1447
    iget-object v3, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/database/model/f;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v3

    .line 1448
    cmp-long v4, v0, v6

    if-nez v4, :cond_1

    .line 1449
    iget-wide v0, v3, Lcom/google/android/gms/drive/database/model/f;->b:J

    goto :goto_0

    .line 1450
    :cond_1
    iget-wide v4, v3, Lcom/google/android/gms/drive/database/model/f;->b:J

    cmp-long v4, v0, v4

    if-eqz v4, :cond_0

    .line 1451
    const-string v4, "DatabaseModelLoader"

    const-string v5, "appAuthMetadata for %s has different packaging id (%d vs %d)"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object p1, v8, v9

    const/4 v9, 0x1

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    iget-wide v10, v3, Lcom/google/android/gms/drive/database/model/f;->b:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-static {v4, v5, v8}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1457
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0
.end method

.method private j(J)Lcom/google/android/gms/drive/database/model/c;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/d;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/d;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 244
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 245
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 250
    :goto_0
    return-object v2

    .line 247
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/c;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 250
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private k(Ljava/lang/String;)J
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    .line 2031
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2033
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 2034
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v0, v6

    .line 2042
    :goto_0
    return-wide v0

    .line 2036
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 2037
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-wide v0, v6

    goto :goto_0

    .line 2039
    :cond_1
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    .line 2042
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;J)I
    .locals 8

    .prologue
    .line 1127
    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 1128
    const-wide/16 v2, 0x0

    cmp-long v0, p2, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 1129
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1131
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    iget-wide v2, v1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v2, 0x5

    new-array v2, v2, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v3, 0x0

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/ab;->a(Z)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NOT IN (SELECT "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/bh;->d:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " FROM "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/bg;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " WHERE "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/bh;->d:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " IS NOT NULL)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " NOT IN (SELECT "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/k;->d:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " FROM "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/j;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->t:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "<?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v4, v2, v3

    const/4 v3, 0x4

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->k:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    const-wide/16 v6, 0x0

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1149
    new-instance v7, Ljava/util/HashSet;

    invoke-direct {v7}, Ljava/util/HashSet;-><init>()V

    .line 1150
    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gms/drive/database/a/g;->a()Lcom/google/android/gms/drive/database/a/g;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v1

    .line 1153
    :try_start_0
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 1154
    invoke-interface {v7, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 1157
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0

    .line 1128
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 1157
    :cond_1
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 1159
    const/4 v0, 0x0

    .line 1160
    invoke-interface {v7}, Ljava/util/Set;->size()I

    move-result v2

    .line 1161
    invoke-interface {v7}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 1162
    iget-object v4, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v0

    sget-object v5, Lcom/google/android/gms/drive/database/model/ah;->a:Landroid/net/Uri;

    invoke-virtual {v4, v6, v7, v0, v5}, Lcom/google/android/gms/drive/database/i;->a(JLcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1164
    goto :goto_2

    .line 1165
    :cond_2
    if-eq v1, v2, :cond_3

    .line 1166
    const-string v0, "DatabaseModelLoader"

    const-string v3, "Only %d of %d obsolete entries deleted successfully"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1169
    :cond_3
    return v1
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 556
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 557
    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    .line 559
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 561
    const-string v2, "MetadataView"

    .line 567
    :goto_0
    new-instance v6, Landroid/os/Bundle;

    invoke-direct {v6}, Landroid/os/Bundle;-><init>()V

    .line 569
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 571
    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    move-object v0, p0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 572
    const-string v1, "dbInstanceId"

    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->g()J

    move-result-wide v2

    invoke-virtual {v6, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 573
    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 574
    const-string v1, "customPropertiesExtra"

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/auth/g;)Landroid/util/SparseArray;

    move-result-object v2

    invoke-virtual {v6, v1, v2}, Landroid/os/Bundle;->putSparseParcelableArray(Ljava/lang/String;Landroid/util/SparseArray;)V

    .line 577
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 579
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 581
    new-instance v1, Lcom/google/android/gms/common/data/DataHolder;

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2, v6}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v1

    .line 564
    :cond_1
    const-string v2, "ScopedMetadataView"

    goto :goto_0

    .line 579
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ah;JLcom/google/android/gms/drive/auth/a;)Lcom/google/android/gms/drive/auth/a;
    .locals 8

    .prologue
    .line 1529
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->z()Z

    move-result v0

    const-string v1, "entry must be saved to database before setting auth state"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1531
    sget-object v0, Lcom/google/android/gms/drive/database/model/ak;->b:Lcom/google/android/gms/drive/database/model/ak;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ak;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 1533
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 1536
    :try_start_0
    sget-object v1, Lcom/google/android/gms/drive/database/e;->a:[I

    invoke-virtual {p4}, Lcom/google/android/gms/drive/auth/a;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 1564
    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1569
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0

    .line 1538
    :pswitch_0
    :try_start_1
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v1

    .line 1541
    :try_start_2
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/auth/a;->b:Lcom/google/android/gms/drive/auth/a;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-object v6, v0

    .line 1544
    :goto_0
    :try_start_3
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 1546
    sget-object v0, Lcom/google/android/gms/drive/auth/a;->b:Lcom/google/android/gms/drive/auth/a;

    if-ne v6, v0, :cond_0

    .line 1547
    new-instance v0, Lcom/google/android/gms/drive/database/model/ai;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v2

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/ai;-><init>(Lcom/google/android/gms/drive/database/i;JJ)V

    .line 1549
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ai;->i()V

    .line 1566
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1569
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    return-object v6

    .line 1541
    :cond_1
    :try_start_4
    sget-object v0, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-object v6, v0

    goto :goto_0

    .line 1544
    :catchall_1
    move-exception v0

    :try_start_5
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0

    .line 1553
    :pswitch_1
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 1556
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/aj;->a()Lcom/google/android/gms/drive/database/model/aj;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/aj;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1560
    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/drive/auth/a;->b:Lcom/google/android/gms/drive/auth/a;

    :goto_2
    move-object v6, v0

    .line 1562
    goto :goto_1

    .line 1560
    :cond_2
    sget-object v0, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2

    .line 1536
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(JJ)Lcom/google/android/gms/drive/auth/g;
    .locals 5

    .prologue
    .line 1315
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/h;->b:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-static {p1, p2}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/d;->b(JLcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 1309
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/drive/database/d;->c(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/d;->b(JLcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/database/b/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 704
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/d;->a()Lcom/google/android/gms/drive/database/model/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/d;->e()Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 706
    invoke-static {}, Lcom/google/android/gms/drive/database/a/a;->a()Lcom/google/android/gms/drive/database/a/a;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/database/b/d;
    .locals 7

    .prologue
    .line 711
    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v0, "childId=?"

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    const-string v2, "ParentCollectionView"

    const-string v3, "ScopedParentCollectionView"

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/b/d;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 1888
    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 1889
    if-eqz p1, :cond_0

    .line 1890
    sget-object v1, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    new-array v3, v7, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-static {v2}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v0, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 1892
    :cond_0
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v3, Lcom/google/android/gms/drive/database/model/by;->b:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    int-to-long v4, p2

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    new-array v4, v7, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    aput-object v0, v4, v6

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1895
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bx;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1902
    sget-object v1, Lcom/google/android/gms/drive/database/model/bw;->a:Lcom/google/android/gms/drive/database/model/bw;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/b/d;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 616
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 617
    if-nez p2, :cond_0

    move-object v0, v2

    :goto_0
    sget-object v1, Lcom/google/android/gms/drive/database/model/aa;->a:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    if-eqz v0, :cond_1

    sget-object v3, Lcom/google/android/gms/drive/database/model/aa;->d:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/aa;->b:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    sget-object v4, Lcom/google/android/gms/drive/database/model/aa;->d:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    const-wide/16 v6, 0x1

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    sget-object v6, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v0, v6, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-virtual {v3, v5, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    move-object v4, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->a()Lcom/google/android/gms/drive/database/model/z;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/z;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 618
    invoke-static {}, Lcom/google/android/gms/drive/database/a/b;->a()Lcom/google/android/gms/drive/database/a/b;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0

    .line 617
    :cond_0
    iget-wide v0, p2, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v4, v1

    goto :goto_1
.end method

.method public final a(J)Lcom/google/android/gms/drive/database/model/a;
    .locals 7

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->d:Lcom/google/android/gms/drive/database/model/b;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    .line 191
    if-nez v0, :cond_0

    .line 192
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/database/d;->j(J)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v1

    .line 193
    if-nez v1, :cond_1

    .line 194
    const/4 v0, 0x0

    .line 199
    :cond_0
    :goto_0
    return-object v0

    .line 196
    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/database/model/a;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/c;->h()Ljava/lang/String;

    move-result-object v2

    iget-wide v4, v1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-direct {v0, v2, v4, v5}, Lcom/google/android/gms/drive/database/model/a;-><init>(Ljava/lang/String;J)V

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->d:Lcom/google/android/gms/drive/database/model/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/b;->a(Lcom/google/android/gms/drive/database/model/a;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/a;
    .locals 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->d:Lcom/google/android/gms/drive/database/model/b;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    .line 180
    if-nez v0, :cond_0

    .line 181
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/d;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v1

    .line 182
    new-instance v0, Lcom/google/android/gms/drive/database/model/a;

    iget-wide v2, v1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-direct {v0, p1, v2, v3}, Lcom/google/android/gms/drive/database/model/a;-><init>(Ljava/lang/String;J)V

    .line 183
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->d:Lcom/google/android/gms/drive/database/model/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/b;->a(Lcom/google/android/gms/drive/database/model/a;)V

    .line 185
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 2

    .prologue
    .line 858
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 859
    invoke-virtual {p1}, Lcom/google/android/gms/drive/auth/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 861
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;

    move-result-object v0

    .line 862
    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/i;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 863
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 864
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    return-object v0

    .line 859
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 2

    .prologue
    .line 501
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 502
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->x()Z

    move-result v1

    if-nez v1, :cond_1

    .line 503
    :cond_0
    const/4 v0, 0x0

    .line 505
    :cond_1
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 6

    .prologue
    .line 529
    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v5

    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/drive/database/d;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/a/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Lcom/google/android/gms/drive/database/model/ah;
    .locals 7

    .prologue
    .line 519
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    move-object v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(JLjava/lang/String;JZ)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 522
    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 523
    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/a/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 838
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 840
    const-string v0, "application/vnd.google-apps.folder"

    const-string v1, "root"

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 842
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->M()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->y()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 849
    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;)V

    .line 850
    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/database/model/ah;->b(Z)V

    .line 851
    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 853
    :cond_1
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 6

    .prologue
    .line 659
    iget-wide v0, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v0, v1, p3}, Lcom/google/android/gms/drive/database/d;->a(JLjava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 660
    invoke-static {p1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/a/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 662
    if-nez v0, :cond_1

    .line 663
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 672
    :cond_0
    :goto_0
    return-object v0

    .line 665
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->K()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 668
    const-string v1, "DatabaseModelLoader"

    const-string v2, "Fetching %s as MIME type %s: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->K()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p3, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/t;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 643
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 644
    new-instance v1, Lcom/google/android/gms/drive/database/model/ah;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    move-object v3, p0

    move-object v6, p3

    move-object v7, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/database/model/ah;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;JLjava/lang/String;Lcom/google/android/gms/drive/database/t;)V

    .line 647
    invoke-virtual {v1, p2}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;)V

    .line 648
    invoke-virtual {v1, p3}, Lcom/google/android/gms/drive/database/model/ah;->l(Ljava/lang/String;)V

    .line 649
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 650
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->b(Ljava/util/Date;)V

    .line 651
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/util/Date;)V

    .line 652
    invoke-virtual {v1, v8}, Lcom/google/android/gms/drive/database/model/ah;->d(Z)V

    .line 653
    invoke-virtual {v1, v8}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 654
    return-object v1
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/am;
    .locals 6

    .prologue
    .line 782
    new-instance v0, Lcom/google/android/gms/drive/database/model/am;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v2

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/am;-><init>(Lcom/google/android/gms/drive/database/i;JJ)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;
    .locals 11

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 277
    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Not persisted: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 278
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 280
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/google/android/gms/drive/database/model/bd;->b:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p2, Lcom/google/android/gms/drive/metadata/sync/a/c;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/metadata/sync/a/e;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v6, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v8, Lcom/google/android/gms/drive/database/model/bd;->c:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/sync/a/c;->a()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v6, v5, v3

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/google/android/gms/drive/database/model/bd;->a:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-wide v8, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v6, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v3, v5, v1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->a()Lcom/google/android/gms/drive/database/model/bc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bc;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 293
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    .line 295
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-wide v2, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    new-instance v0, Lcom/google/android/gms/drive/database/model/bb;

    const/4 v5, 0x0

    const-wide/16 v7, 0x0

    move-object v4, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/model/bb;-><init>(Lcom/google/android/gms/drive/database/i;JLcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;Ljava/lang/Long;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 299
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v0

    :cond_0
    move v0, v3

    .line 277
    goto/16 :goto_0

    .line 297
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v9}, Lcom/google/android/gms/drive/database/model/bb;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/bb;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 299
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)Lcom/google/android/gms/drive/database/model/bi;
    .locals 10

    .prologue
    .line 968
    new-instance v1, Lcom/google/android/gms/drive/database/model/bi;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/drive/database/model/bi;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/auth/AppIdentity;J)V

    .line 970
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bi;->i()V

    .line 971
    return-object v1
.end method

.method public final a(JLcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/model/bv;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1837
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gms/drive/database/d;->c(JLcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1839
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bx;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1846
    sget-object v1, Lcom/google/android/gms/drive/database/model/bw;->a:Lcom/google/android/gms/drive/database/model/bw;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    .line 1847
    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/b/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bv;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ce;)Ljava/lang/Long;
    .locals 7

    .prologue
    .line 1935
    sget-object v0, Lcom/google/android/gms/drive/database/model/cg;->a:Lcom/google/android/gms/drive/database/model/cg;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cg;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/drive/database/model/ce;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 1937
    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/cf;->f()Ljava/lang/String;

    move-result-object v6

    .line 1938
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/cf;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v6, v2, v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1947
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1948
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1949
    invoke-virtual {p1, v2, v3}, Lcom/google/android/gms/drive/database/model/ce;->d(J)V

    .line 1951
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ce;->i()V

    .line 1952
    iget-wide v2, p1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1954
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/e;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 305
    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Not persisted: %s"

    new-array v5, v1, [Ljava/lang/Object;

    aput-object p1, v5, v3

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 306
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 308
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v6, Lcom/google/android/gms/drive/database/model/bd;->b:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "=?"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/gms/drive/metadata/sync/a/e;->a()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v5, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/google/android/gms/drive/database/model/bd;->a:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    iget-wide v8, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v5, v1, v3

    invoke-virtual {v0, v4, v1}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->a()Lcom/google/android/gms/drive/database/model/bc;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bc;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 317
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 319
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 320
    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v2, v1}, Lcom/google/android/gms/drive/database/model/bb;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 323
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move v0, v3

    .line 305
    goto/16 :goto_0

    .line 323
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 325
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Ljava/util/Set;)Ljava/util/Set;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 726
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 727
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 728
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v0

    const/16 v3, 0x32

    if-gt v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 731
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 732
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 762
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 728
    goto :goto_0

    .line 736
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v2

    .line 737
    :goto_2
    invoke-interface {p2}, Ljava/util/Set;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 738
    const-string v4, "?"

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 737
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 740
    :cond_2
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "%s IN (%s)"

    new-array v5, v8, [Ljava/lang/Object;

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->j:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    const-string v6, ","

    invoke-static {v6, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-static {v0, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v0, v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 748
    iget-object v3, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 749
    sget-object v4, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    iget-wide v6, v3, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v6, v7}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    new-array v5, v8, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->k:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->e()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v6

    aput-object v6, v5, v2

    aput-object v0, v5, v1

    invoke-virtual {v4, v3, v5}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 754
    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    .line 758
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 759
    invoke-interface {v0}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 760
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_3
    move-object v0, v1

    .line 762
    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 1638
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ah;)Ljava/util/Set;
    .locals 6

    .prologue
    .line 1575
    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v1

    .line 1578
    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 1579
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ai;

    .line 1580
    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/ai;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1584
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    return-object v2
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/database/model/EntrySpec;)V
    .locals 5

    .prologue
    .line 788
    sget-object v0, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 790
    sget-object v1, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    .line 793
    sget-object v2, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 794
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/r;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 796
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/a;)V
    .locals 6

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 206
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/d;->h(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v1

    .line 207
    if-eqz v1, :cond_0

    .line 208
    iget-wide v2, v1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 209
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/c;->j()V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->d:Lcom/google/android/gms/drive/database/model/b;

    iget-object v1, v0, Lcom/google/android/gms/drive/database/model/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v2, p1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    iget-wide v2, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 215
    return-void

    .line 208
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 214
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ah;Ljava/util/Set;)V
    .locals 7

    .prologue
    .line 1493
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->z()Z

    move-result v0

    const-string v1, "entry must be saved to database before authorized apps are set"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1495
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 1497
    :try_start_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1500
    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 1503
    :try_start_1
    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ai;

    .line 1504
    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/ai;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1505
    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/ai;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1507
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ai;->j()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1511
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1522
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0

    .line 1511
    :cond_1
    :try_start_3
    invoke-interface {v2}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 1514
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v2

    .line 1516
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1517
    new-instance v0, Lcom/google/android/gms/drive/database/model/ai;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/ai;-><init>(Lcom/google/android/gms/drive/database/i;JJ)V

    .line 1518
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ai;->i()V

    goto :goto_1

    .line 1520
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1522
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 1523
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/g/at;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 1122
    iget-object v4, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v0, v4, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-gtz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/ai;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sget-object v0, Lcom/google/android/gms/drive/ai;->ah:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/drive/g/at;->e()V

    invoke-interface {p1}, Lcom/google/android/gms/drive/g/at;->f()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move v0, v1

    :goto_1
    if-lez v5, :cond_1

    add-int/lit8 v3, v0, 0x1

    if-ge v0, v6, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isDbLockedByOtherThreads()Z

    move-result v2

    if-nez v2, :cond_2

    sget v7, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v0, Lcom/google/android/gms/drive/ai;->x:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v7, v0, :cond_2

    iget-object v0, v4, Lcom/google/android/gms/drive/database/i;->d:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v8

    iget-object v0, v4, Lcom/google/android/gms/drive/database/i;->c:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v0, v8, v10

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_1

    int-to-long v8, v5

    :try_start_1
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v3

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    .line 1123
    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_2
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 6

    .prologue
    .line 1009
    sget-object v0, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    .line 1011
    const-string v3, ","

    if-nez p1, :cond_0

    const-string v0, ""

    .line 1012
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN WipeoutFileContentHashView AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " NOT IN ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1016
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/az;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1017
    return-void

    .line 1011
    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_2
    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_1
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 947
    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v1, Lcom/google/android/gms/drive/database/model/bk;->a:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/gms/drive/database/model/bk;->b:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 950
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bj;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 958
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 960
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return v1

    :catchall_0
    move-exception v1

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/ae;)Z
    .locals 1

    .prologue
    .line 2013
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/i;->a(Lcom/google/android/gms/drive/database/model/ae;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 719
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->i()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v2, "%s IN (SELECT %s FROM %s WHERE %s=?)"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/r;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    sget-object v5, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p1, v1, v6, v6}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 12

    .prologue
    .line 512
    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v5

    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v6

    sget-object v4, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/d;->i(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v8, 0x0

    new-instance v9, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v11

    invoke-virtual {v11}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "=? "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v9, v10, v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v9, v1, v8

    invoke-virtual {v4, v0, v1}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Lcom/google/android/gms/drive/database/a/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    return-object v0
.end method

.method public final b(J)Lcom/google/android/gms/drive/database/model/bi;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 921
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bj;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 922
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bj;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/a/i;->a()Lcom/google/android/gms/drive/database/a/i;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    .line 927
    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/b/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bi;

    return-object v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;
    .locals 2

    .prologue
    .line 256
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/d;->h(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v0

    .line 257
    if-nez v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 262
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/d;->h(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/c;

    move-result-object v0

    .line 263
    if-nez v0, :cond_0

    .line 264
    new-instance v0, Lcom/google/android/gms/drive/database/model/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/drive/database/model/c;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;)V

    .line 265
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/c;->i()V

    .line 267
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 272
    :cond_1
    return-object v0

    .line 269
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 869
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 871
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/d;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/a;

    .line 873
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v3, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=?"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v4, v4, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 879
    sget-object v3, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v3, v1, v4}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 882
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    const-string v1, "AppDataScopeView"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 890
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 891
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/i;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 894
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Ljava/util/List;
    .locals 7

    .prologue
    .line 691
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 692
    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    move-object v2, p2

    move-wide v3, p3

    move v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(JLjava/lang/String;JZ)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    .line 695
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ASC"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 697
    const-string v2, "EntryView"

    const-string v3, "ScopedEntryView"

    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    .line 699
    invoke-interface {v0}, Lcom/google/android/gms/drive/database/b/d;->b()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 800
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 801
    return-void
.end method

.method public final b(JLcom/google/android/gms/drive/auth/AppIdentity;)V
    .locals 1

    .prologue
    .line 1334
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/drive/database/d;->c(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/database/SqlWhereClause;)I

    .line 1335
    return-void
.end method

.method public final b(JLcom/google/android/gms/drive/database/model/EntrySpec;I)V
    .locals 5

    .prologue
    .line 1853
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/gms/drive/database/d;->c(JLcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 1855
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/bx;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1858
    return-void
.end method

.method public final b(Lcom/google/android/gms/drive/database/model/a;)V
    .locals 4

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    sget-object v1, Lcom/google/android/gms/drive/database/model/c;->a:Landroid/net/Uri;

    iget-wide v2, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/i;->a(Landroid/net/Uri;J)V

    .line 221
    return-void
.end method

.method public final c(Lcom/google/android/gms/drive/database/model/a;)I
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 330
    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/a;->b:J

    .line 331
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->a()Lcom/google/android/gms/drive/database/model/bc;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bc;->e()Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/google/android/gms/drive/database/model/bd;->a:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "=?"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v0, v3, v6, v1}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0

    :cond_0
    move v0, v2

    .line 331
    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;
    .locals 2

    .prologue
    .line 901
    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/az;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/b/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ax;

    return-object v0
.end method

.method public final c(J)Lcom/google/android/gms/drive/database/model/be;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1622
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bg;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1623
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bg;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/drive/database/model/be;->a(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/a/c;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    .line 1633
    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/b/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/be;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;
    .locals 3

    .prologue
    .line 1221
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/drive/database/d;->d(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 805
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 806
    return-void
.end method

.method public final d(Lcom/google/android/gms/drive/database/model/a;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 1643
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;Z)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final d(J)V
    .locals 5

    .prologue
    .line 1862
    sget-object v0, Lcom/google/android/gms/drive/database/model/by;->f:Lcom/google/android/gms/drive/database/model/by;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/by;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->d()V

    sget-object v1, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "<?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1865
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/bx;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1868
    return-void
.end method

.method public final d(Lcom/google/android/gms/drive/auth/g;)V
    .locals 9

    .prologue
    .line 1352
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 1354
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/drive/database/d;->d(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v0

    .line 1358
    if-eqz v0, :cond_0

    .line 1359
    iget-wide v2, v0, Lcom/google/android/gms/drive/database/model/f;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 1362
    const-string v1, "DatabaseModelLoader"

    const-string v2, "Existing appAuthMetadata has different packaging id (%d vs %d)"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v6, v0, Lcom/google/android/gms/drive/database/model/f;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1364
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/drive/database/d;->b(JLcom/google/android/gms/drive/auth/AppIdentity;)V

    .line 1365
    const/4 v0, 0x0

    .line 1379
    :cond_0
    :goto_0
    if-nez v0, :cond_4

    .line 1380
    const-string v0, "DatabaseModelLoader"

    const-string v1, "No matching appAuthMetadata entry, inserting a new one"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1381
    new-instance v0, Lcom/google/android/gms/drive/database/model/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v2, v2, Lcom/google/android/gms/drive/database/model/a;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    iget-object v6, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v7, p1, Lcom/google/android/gms/drive/auth/g;->d:J

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/model/f;-><init>(Lcom/google/android/gms/drive/database/i;JJLcom/google/android/gms/drive/auth/AppIdentity;J)V

    .line 1387
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/f;->i()V

    move-object v1, v0

    .line 1390
    :goto_1
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/aa;

    .line 1391
    new-instance v3, Lcom/google/android/gms/drive/database/model/o;

    iget-object v4, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-wide v6, v1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-direct {v3, v4, v6, v7, v0}, Lcom/google/android/gms/drive/database/model/o;-><init>(Lcom/google/android/gms/drive/database/i;JLcom/google/android/gms/drive/aa;)V

    .line 1393
    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/o;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 1400
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0

    .line 1367
    :cond_1
    :try_start_1
    const-string v1, "DatabaseModelLoader"

    const-string v2, "AppAuthMetadata entry already exists, simply updating it"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 1368
    iget-wide v2, p1, Lcom/google/android/gms/drive/auth/g;->d:J

    iput-wide v2, v0, Lcom/google/android/gms/drive/database/model/f;->d:J

    .line 1369
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/f;->i()V

    .line 1374
    sget-object v1, Lcom/google/android/gms/drive/database/model/q;->a:Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/q;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    iget-wide v2, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/p;->a()Lcom/google/android/gms/drive/database/model/p;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/p;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0

    .line 1395
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1396
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/d;->g(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;

    .line 1398
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1400
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 1401
    return-void

    :cond_4
    move-object v1, v0

    goto :goto_1
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1085
    new-instance v0, Lcom/google/android/gms/drive/database/model/t;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/drive/database/model/t;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;)V

    .line 1087
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/t;->i()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1091
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->a()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    return v0
.end method

.method public final e(J)Lcom/google/android/gms/drive/database/model/ce;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1921
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/cf;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/cf;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1929
    invoke-static {}, Lcom/google/android/gms/drive/database/a/j;->a()Lcom/google/android/gms/drive/database/a/j;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    .line 1930
    invoke-static {v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/b/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ce;

    return-object v0
.end method

.method public final e(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/l;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 2094
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/d;->c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v6

    .line 2095
    const-string v0, "Authorized app doesn\'t exist"

    invoke-static {v6, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2097
    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/drive/database/model/n;->a:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-wide v8, v6, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2101
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->a()Lcom/google/android/gms/drive/database/model/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/m;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 2109
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2110
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    new-instance v0, Lcom/google/android/gms/drive/database/model/l;

    sget-object v2, Lcom/google/android/gms/drive/database/model/n;->a:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v4, Lcom/google/android/gms/drive/database/model/n;->b:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->intValue()I

    move-result v4

    sget-object v5, Lcom/google/android/gms/drive/database/model/n;->c:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5, v7}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/n;->d:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/l;-><init>(Lcom/google/android/gms/drive/database/i;JIIZ)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->a()Lcom/google/android/gms/drive/database/model/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/m;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/l;->d(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2120
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    .line 2112
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/drive/database/model/l;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-wide v2, v6, Lcom/google/android/gms/drive/database/model/ad;->f:J

    const/4 v4, 0x1

    const/16 v5, 0x100

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/l;-><init>(Lcom/google/android/gms/drive/database/i;JIIZ)V

    .line 2116
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/l;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2120
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 815
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V

    .line 816
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1095
    sget-object v0, Lcom/google/android/gms/drive/database/model/v;->a:Lcom/google/android/gms/drive/database/model/v;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/v;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 1097
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/u;->a()Lcom/google/android/gms/drive/database/model/u;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/u;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1099
    return-void
.end method

.method public final f(J)Lcom/google/android/gms/drive/database/model/f;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1961
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/g;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/g;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1965
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1966
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1971
    :goto_0
    return-object v2

    .line 1968
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/f;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1971
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 820
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/i;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/ak;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ak;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 823
    :cond_0
    return-void
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1108
    new-instance v4, Lcom/google/android/gms/drive/database/SqlWhereClause;

    const-string v0, "filename=?"

    invoke-direct {v4, v0, p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1111
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    const-string v1, "CannotDeleteFilenameView"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1114
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 1116
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return v0

    .line 1114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1116
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final g(J)Lcom/google/android/gms/drive/database/model/bl;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1978
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bp;->a()Lcom/google/android/gms/drive/database/model/bp;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bp;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bp;->a()Lcom/google/android/gms/drive/database/model/bp;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/bp;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1982
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 1983
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1988
    :goto_0
    return-object v2

    .line 1985
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/bl;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/bl;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 1988
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final g()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 980
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Z)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    .line 982
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 985
    :try_start_0
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v2, Lcom/google/android/gms/drive/database/SqlWhereClause;->a:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a(Lcom/google/android/gms/drive/database/x;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v3

    const/4 v1, 0x0

    const-string v2, "EntryView"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 986
    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 987
    :try_start_1
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 988
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/database/model/ah;->l(Z)V

    .line 989
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 993
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 994
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 996
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0

    .line 991
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 993
    if-eqz v1, :cond_2

    .line 994
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 996
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 997
    return-void

    .line 993
    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method public final g(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 1406
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 1409
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/d;->j(Ljava/lang/String;)J

    move-result-wide v0

    .line 1411
    invoke-static {p1}, Lcom/google/android/gms/drive/database/d;->i(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    .line 1412
    invoke-direct {p0, v2}, Lcom/google/android/gms/drive/database/d;->b(Lcom/google/android/gms/drive/database/SqlWhereClause;)I

    .line 1413
    iget-object v3, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bx;->a()Lcom/google/android/gms/drive/database/model/bx;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/bx;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v5, v2}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1416
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    .line 1418
    sget-object v2, Lcom/google/android/gms/drive/database/model/bd;->b:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/drive/metadata/sync/a/e;->d:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/metadata/sync/a/e;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "%"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/bd;->c:Lcom/google/android/gms/drive/database/model/bd;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bd;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    new-instance v3, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " LIKE ?"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1, v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v4, 0x0

    aput-object v3, v1, v4

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bc;->a()Lcom/google/android/gms/drive/database/model/bc;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/bc;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1421
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V

    .line 1422
    const-string v0, "DatabaseModelLoader"

    const-string v1, "Uninstalled %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1424
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 1425
    return-void

    .line 1424
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0
.end method

.method public final h(J)Ljava/util/List;
    .locals 9

    .prologue
    .line 1994
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bp;->a()Lcom/google/android/gms/drive/database/model/bp;

    move-result-object v1

    .line 1996
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bp;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ASC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 1997
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bp;->e()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/database/model/bq;->d:Lcom/google/android/gms/drive/database/model/bq;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/bq;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "<=?"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2001
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2002
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2003
    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v2, v1}, Lcom/google/android/gms/drive/database/model/bl;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/bl;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2007
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method public final h()V
    .locals 4

    .prologue
    .line 1002
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN CachedFileContentHashView"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1004
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/az;->e()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1005
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1103
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/u;->a()Lcom/google/android/gms/drive/database/model/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/u;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1104
    return-void
.end method

.method public final j()J
    .locals 2

    .prologue
    .line 1174
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 1603
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/d;->a(Lcom/google/android/gms/drive/database/model/ae;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Lcom/google/android/gms/drive/database/b/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1608
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bg;->f()Ljava/lang/String;

    move-result-object v3

    .line 1609
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bg;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ASC"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1616
    invoke-static {p0}, Lcom/google/android/gms/drive/database/model/be;->a(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/a/c;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final m()Lcom/google/android/gms/drive/database/b/d;
    .locals 2

    .prologue
    .line 1740
    const-string v0, "InternalContentView"

    sget-object v1, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final n()J
    .locals 2

    .prologue
    .line 1746
    const-string v0, "InternalContentSizeView"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/d;->k(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final o()Lcom/google/android/gms/drive/database/b/d;
    .locals 2

    .prologue
    .line 1751
    const-string v0, "InternalCachedContentView"

    sget-object v1, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final p()J
    .locals 2

    .prologue
    .line 1757
    const-string v0, "InternalCachedContentSizeView"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/d;->k(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final q()Lcom/google/android/gms/drive/database/b/d;
    .locals 2

    .prologue
    .line 1762
    const-string v0, "SharedCachedContentView"

    sget-object v1, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Ljava/lang/String;Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final r()J
    .locals 2

    .prologue
    .line 1768
    const-string v0, "SharedCachedContentSizeView"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/d;->k(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final s()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1773
    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SELECT COUNT(*) FROM "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ar;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=1"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1777
    iget-object v2, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1781
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1782
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1787
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return v0

    .line 1784
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final t()J
    .locals 6

    .prologue
    .line 1793
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "SELECT * FROM "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/az;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " JOIN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ar;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ON "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->z:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1800
    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 1803
    const-wide/16 v0, 0x0

    .line 1805
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1806
    iget-object v3, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v3, v2}, Lcom/google/android/gms/drive/database/model/ax;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v3

    .line 1807
    iget-wide v4, v3, Lcom/google/android/gms/drive/database/model/ax;->h:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-long/2addr v0, v4

    .line 1808
    goto :goto_0

    .line 1810
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1813
    return-wide v0

    .line 1810
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public final u()Lcom/google/android/gms/drive/database/t;
    .locals 2

    .prologue
    .line 1882
    new-instance v0, Lcom/google/android/gms/drive/database/g;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/database/g;-><init>(Lcom/google/android/gms/drive/database/i;)V

    return-object v0
.end method

.method public final v()Lcom/google/android/gms/drive/database/b/d;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1907
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    const-string v1, "PinnedDownloadRequiredView"

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 1915
    invoke-static {}, Lcom/google/android/gms/drive/database/a/e;->a()Lcom/google/android/gms/drive/database/a/e;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/database/d;->a(Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/f;)Lcom/google/android/gms/drive/database/b/d;

    move-result-object v0

    return-object v0
.end method

.method public final w()Ljava/util/Set;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2071
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->h:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 2074
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 2075
    iget-object v0, p0, Lcom/google/android/gms/drive/database/d;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->a()Lcom/google/android/gms/drive/database/model/bt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bt;->e()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2083
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2084
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2087
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2089
    return-object v6
.end method
