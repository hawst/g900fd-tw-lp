.class final Lcom/google/android/gms/analytics/ad;
.super Lcom/google/android/gms/analytics/bs;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static p:Lcom/google/android/gms/analytics/ad;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lcom/google/android/gms/analytics/g;

.field private volatile d:Lcom/google/android/gms/analytics/i;

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Z

.field private k:Lcom/google/android/gms/analytics/h;

.field private l:Landroid/os/Handler;

.field private m:Lcom/google/android/gms/analytics/ac;

.field private n:Z

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 88
    invoke-direct {p0}, Lcom/google/android/gms/analytics/bs;-><init>()V

    .line 49
    const/16 v0, 0x708

    iput v0, p0, Lcom/google/android/gms/analytics/ad;->e:I

    .line 50
    iput-boolean v1, p0, Lcom/google/android/gms/analytics/ad;->f:Z

    .line 55
    iput-boolean v1, p0, Lcom/google/android/gms/analytics/ad;->i:Z

    .line 59
    iput-boolean v1, p0, Lcom/google/android/gms/analytics/ad;->j:Z

    .line 61
    new-instance v0, Lcom/google/android/gms/analytics/ae;

    invoke-direct {v0, p0}, Lcom/google/android/gms/analytics/ae;-><init>(Lcom/google/android/gms/analytics/ad;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ad;->k:Lcom/google/android/gms/analytics/h;

    .line 75
    iput-boolean v2, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    .line 76
    iput-boolean v2, p0, Lcom/google/android/gms/analytics/ad;->o:Z

    .line 89
    return-void
.end method

.method public static a()Lcom/google/android/gms/analytics/ad;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/gms/analytics/ad;->p:Lcom/google/android/gms/analytics/ad;

    if-nez v0, :cond_0

    .line 83
    new-instance v0, Lcom/google/android/gms/analytics/ad;

    invoke-direct {v0}, Lcom/google/android/gms/analytics/ad;-><init>()V

    sput-object v0, Lcom/google/android/gms/analytics/ad;->p:Lcom/google/android/gms/analytics/ad;

    .line 85
    :cond_0
    sget-object v0, Lcom/google/android/gms/analytics/ad;->p:Lcom/google/android/gms/analytics/ad;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/analytics/ad;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/analytics/ad;)I
    .locals 1

    .prologue
    .line 30
    iget v0, p0, Lcom/google/android/gms/analytics/ad;->e:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/analytics/ad;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/analytics/ad;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method final declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 228
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 229
    const-string v0, "Dispatch period set with null handler. Dispatch will run once initialization is complete."

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 231
    iput p1, p0, Lcom/google/android/gms/analytics/ad;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 245
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 235
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/analytics/at;->T:Lcom/google/android/gms/analytics/at;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/as;->a(Lcom/google/android/gms/analytics/at;)V

    .line 237
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->i:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/analytics/ad;->e:I

    if-lez v0, :cond_2

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 240
    :cond_2
    iput p1, p0, Lcom/google/android/gms/analytics/ad;->e:I

    .line 241
    if-lez p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->i:Z

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    mul-int/lit16 v2, p1, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Landroid/content/Context;Lcom/google/android/gms/analytics/i;)V
    .locals 2

    .prologue
    .line 145
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->b:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 161
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 148
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/ad;->b:Landroid/content/Context;

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->d:Lcom/google/android/gms/analytics/i;

    if-nez v0, :cond_0

    .line 151
    iput-object p2, p0, Lcom/google/android/gms/analytics/ad;->d:Lcom/google/android/gms/analytics/i;

    .line 152
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->f:Z

    if-eqz v0, :cond_2

    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/analytics/ad;->c()V

    .line 154
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->f:Z

    .line 156
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->g:Z

    if-eqz v0, :cond_0

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->d:Lcom/google/android/gms/analytics/i;

    if-nez v0, :cond_3

    const-string v0, "setForceLocalDispatch() queued. It will be called once initialization is complete."

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->g:Z

    .line 158
    :goto_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 157
    :cond_3
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/analytics/at;->af:Lcom/google/android/gms/analytics/at;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/as;->a(Lcom/google/android/gms/analytics/at;)V

    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->d:Lcom/google/android/gms/analytics/i;

    invoke-interface {v0}, Lcom/google/android/gms/analytics/i;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 279
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/analytics/ad;->a(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 280
    monitor-exit p0

    return-void

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(ZZ)V
    .locals 4

    .prologue
    .line 261
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p2, :cond_0

    .line 275
    :goto_0
    monitor-exit p0

    return-void

    .line 264
    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/analytics/ad;->e:I

    if-lez v0, :cond_2

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 267
    :cond_2
    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    iget v0, p0, Lcom/google/android/gms/analytics/ad;->e:I

    if-lez v0, :cond_3

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/analytics/ad;->e:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 271
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "PowerSaveMode "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_4

    if-nez p2, :cond_5

    :cond_4
    const-string v0, "initiated."

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 273
    iput-boolean p1, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    .line 274
    iput-boolean p2, p0, Lcom/google/android/gms/analytics/ad;->i:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 271
    :cond_5
    :try_start_2
    const-string v0, "terminated."
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method final declared-synchronized b()Lcom/google/android/gms/analytics/g;
    .locals 4

    .prologue
    .line 173
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->c:Lcom/google/android/gms/analytics/g;

    if-nez v0, :cond_1

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->b:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 177
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cant get a store unless we have a context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 179
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/analytics/bo;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->k:Lcom/google/android/gms/analytics/h;

    iget-object v2, p0, Lcom/google/android/gms/analytics/ad;->b:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/analytics/n;

    invoke-direct {v3}, Lcom/google/android/gms/analytics/n;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/analytics/bo;-><init>(Lcom/google/android/gms/analytics/h;Landroid/content/Context;Lcom/google/android/gms/analytics/u;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ad;->c:Lcom/google/android/gms/analytics/g;

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->c:Lcom/google/android/gms/analytics/g;

    iget-boolean v1, p0, Lcom/google/android/gms/analytics/ad;->o:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/analytics/g;->a(Z)V

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->c:Lcom/google/android/gms/analytics/g;

    invoke-interface {v0}, Lcom/google/android/gms/analytics/g;->c()Lcom/google/android/gms/analytics/x;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->h:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/analytics/x;->a(Ljava/lang/String;)V

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/analytics/ad;->h:Ljava/lang/String;

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 189
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/analytics/af;

    invoke-direct {v2, p0}, Lcom/google/android/gms/analytics/af;-><init>(Lcom/google/android/gms/analytics/ad;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    iget v0, p0, Lcom/google/android/gms/analytics/ad;->e:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/analytics/ad;->e:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 191
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->m:Lcom/google/android/gms/analytics/ac;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->j:Z

    if-eqz v0, :cond_3

    .line 192
    new-instance v0, Lcom/google/android/gms/analytics/ac;

    invoke-direct {v0, p0}, Lcom/google/android/gms/analytics/ac;-><init>(Lcom/google/android/gms/analytics/bs;)V

    iput-object v0, p0, Lcom/google/android/gms/analytics/ad;->m:Lcom/google/android/gms/analytics/ac;

    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->m:Lcom/google/android/gms/analytics/ac;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/ac;->a(Landroid/content/Context;)V

    .line 194
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->c:Lcom/google/android/gms/analytics/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method final declared-synchronized c()V
    .locals 2

    .prologue
    .line 216
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->d:Lcom/google/android/gms/analytics/i;

    if-nez v0, :cond_0

    .line 217
    const-string v0, "Dispatch call queued. Dispatch will run once initialization is complete."

    invoke-static {v0}, Lcom/google/android/gms/analytics/bl;->c(Ljava/lang/String;)V

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    :goto_0
    monitor-exit p0

    return-void

    .line 222
    :cond_0
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/analytics/at;->S:Lcom/google/android/gms/analytics/at;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/as;->a(Lcom/google/android/gms/analytics/at;)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->d:Lcom/google/android/gms/analytics/i;

    invoke-interface {v0}, Lcom/google/android/gms/analytics/i;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 216
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized d()V
    .locals 4

    .prologue
    .line 284
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->n:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/analytics/ad;->i:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/analytics/ad;->e:I

    if-lez v0, :cond_0

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/analytics/ad;->l:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/analytics/ad;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    :cond_0
    monitor-exit p0

    return-void

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
