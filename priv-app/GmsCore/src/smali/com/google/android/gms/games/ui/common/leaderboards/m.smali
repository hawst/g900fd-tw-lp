.class final Lcom/google/android/gms/games/ui/common/leaderboards/m;
.super Lcom/google/android/gms/games/ui/f;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final k:Landroid/view/View;

.field private final n:Landroid/view/View;

.field private final o:Landroid/view/View;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/TextView;

.field private final r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final s:Landroid/widget/TextView;

.field private final t:Landroid/database/CharArrayBuffer;

.field private final u:Landroid/widget/TextView;

.field private final v:Landroid/database/CharArrayBuffer;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 3

    .prologue
    const/16 v2, 0x40

    .line 151
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/f;-><init>(Landroid/view/View;)V

    .line 154
    sget v0, Lcom/google/android/gms/j;->nf:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->k:Landroid/view/View;

    .line 155
    sget v0, Lcom/google/android/gms/j;->gM:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->n:Landroid/view/View;

    .line 156
    sget v0, Lcom/google/android/gms/j;->jq:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->o:Landroid/view/View;

    .line 158
    sget v0, Lcom/google/android/gms/j;->kh:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->p:Landroid/widget/TextView;

    .line 159
    sget v0, Lcom/google/android/gms/j;->qn:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->q:Landroid/widget/TextView;

    .line 161
    sget v0, Lcom/google/android/gms/j;->oN:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->U:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->T:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 169
    sget v0, Lcom/google/android/gms/j;->oO:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->s:Landroid/widget/TextView;

    .line 170
    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v2}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->t:Landroid/database/CharArrayBuffer;

    .line 171
    sget v0, Lcom/google/android/gms/j;->oT:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->u:Landroid/widget/TextView;

    .line 172
    new-instance v0, Landroid/database/CharArrayBuffer;

    invoke-direct {v0, v2}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->v:Landroid/database/CharArrayBuffer;

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 175
    return-void
.end method

.method private a(ZI)Landroid/graphics/drawable/Drawable;
    .locals 6

    .prologue
    const/16 v5, 0x8

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 284
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 285
    invoke-virtual {v0, v3}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 286
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 288
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/k;->q:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-float v1, v1

    .line 289
    if-eqz p1, :cond_0

    .line 291
    new-array v2, v5, [F

    aput v4, v2, v3

    const/4 v3, 0x1

    aput v4, v2, v3

    const/4 v3, 0x2

    aput v4, v2, v3

    const/4 v3, 0x3

    aput v4, v2, v3

    const/4 v3, 0x4

    aput v1, v2, v3

    const/4 v3, 0x5

    aput v1, v2, v3

    const/4 v3, 0x6

    aput v1, v2, v3

    const/4 v3, 0x7

    aput v1, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 296
    :goto_0
    return-object v0

    .line 293
    :cond_0
    new-array v1, v5, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    goto :goto_0

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 10

    .prologue
    .line 133
    check-cast p3, Lcom/google/android/gms/games/e/e;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/f;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/k;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->n:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-interface {p3}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-interface {v7}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    const/4 v2, 0x4

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/view/View;

    const/4 v4, 0x0

    iget-object v8, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->o:Landroid/view/View;

    aput-object v8, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/games/ui/d/al;->a(ZI[Landroid/view/View;)V

    invoke-interface {p3}, Lcom/google/android/gms/games/e/e;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/games/ui/d/o;->a(J)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->q:Landroid/widget/TextView;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->p:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-interface {p3}, Lcom/google/android/gms/games/e/e;->d()Ljava/lang/String;

    move-result-object v2

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v3, v7}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->s:Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/p;->kh:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    sget v1, Lcom/google/android/gms/p;->kh:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v1, Lcom/google/android/gms/f;->p:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->j()Z

    move-result v0

    if-eqz v0, :cond_7

    add-int/lit8 v0, v4, -0x1

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/m;->s()I

    move-result v4

    if-ne v4, v0, :cond_4

    const/4 v0, 0x1

    move v4, v0

    :goto_4
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->n:Landroid/view/View;

    invoke-direct {p0, v4, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/m;->a(ZI)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->n:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, 0x0

    if-eqz v4, :cond_0

    sget v1, Lcom/google/android/gms/g;->D:I

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    :cond_0
    const/4 v4, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v0, v4, v8, v9, v1}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->n:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->v:Landroid/database/CharArrayBuffer;

    invoke-interface {p3, v0}, Lcom/google/android/gms/games/e/e;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->u:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->v:Landroid/database/CharArrayBuffer;

    iget-object v1, v1, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v4, 0x0

    iget-object v8, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->v:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v0, v1, v4, v8}, Landroid/widget/TextView;->setText([CII)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->k:Landroid/view/View;

    sget v4, Lcom/google/android/gms/p;->jR:I

    const/4 v0, 0x4

    new-array v8, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    aput-object v2, v8, v0

    const/4 v0, 0x1

    aput-object v3, v8, v0

    const/4 v2, 0x2

    invoke-interface {v7}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v3

    if-eqz v3, :cond_6

    sget v3, Lcom/google/android/gms/p;->hL:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-virtual {v5, v3, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_6
    aput-object v0, v8, v2

    const/4 v0, 0x3

    invoke-interface {p3}, Lcom/google/android/gms/games/e/e;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v8, v0

    invoke-virtual {v6, v4, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    const-wide/16 v8, 0x64

    mul-long/2addr v2, v8

    iget-wide v8, v0, Lcom/google/android/gms/games/ui/common/leaderboards/k;->h:J

    div-long/2addr v2, v8

    long-to-int v2, v2

    const/4 v3, 0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    sget v3, Lcom/google/android/gms/p;->ke:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v8

    invoke-virtual {v6, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->q:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->p:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->t:Landroid/database/CharArrayBuffer;

    invoke-interface {p3, v1}, Lcom/google/android/gms/games/e/e;->b(Landroid/database/CharArrayBuffer;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->s:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->t:Landroid/database/CharArrayBuffer;

    iget-object v3, v3, Landroid/database/CharArrayBuffer;->data:[C

    const/4 v4, 0x0

    iget-object v8, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->t:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v1, v3, v4, v8}, Landroid/widget/TextView;->setText([CII)V

    invoke-interface {p3}, Lcom/google/android/gms/games/e/e;->h()Ljava/lang/String;

    move-result-object v3

    const/4 v1, -0x1

    goto/16 :goto_2

    :cond_4
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->n:Landroid/view/View;

    invoke-direct {p0, v4, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/m;->a(ZI)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_5

    :cond_6
    const-string v0, ""

    goto :goto_6

    :cond_7
    move v0, v4

    goto/16 :goto_3
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/m;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/k;

    .line 276
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Lcom/google/android/gms/games/ui/common/leaderboards/k;)Lcom/google/android/gms/games/ui/common/leaderboards/l;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 277
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/m;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/e/e;

    invoke-interface {v1}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    .line 278
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/k;->a(Lcom/google/android/gms/games/ui/common/leaderboards/k;)Lcom/google/android/gms/games/ui/common/leaderboards/l;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/l;->a(Lcom/google/android/gms/games/Player;)V

    .line 280
    :cond_0
    return-void
.end method
