.class final Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;
.super Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/drive/database/model/bb;

.field private final c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/bb;Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 101
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bb;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;->b:Lcom/google/android/gms/drive/database/model/bb;

    .line 102
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    .line 104
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/d/g;)V
    .locals 6

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;->b()Ljava/util/Date;

    move-result-object v0

    .line 109
    invoke-interface {p1}, Lcom/google/android/gms/drive/d/g;->b()Ljava/lang/String;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;->b:Lcom/google/android/gms/drive/database/model/bb;

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/drive/database/model/bb;->a(Ljava/lang/String;Ljava/lang/Long;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;->b:Lcom/google/android/gms/drive/database/model/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bb;->i()V

    .line 113
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a(Lcom/google/android/gms/drive/d/g;)V

    .line 114
    return-void

    .line 111
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
