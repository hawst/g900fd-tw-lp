.class public Lcom/google/android/gms/wallet/ia/CartDetailsView;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bj;


# instance fields
.field a:Landroid/widget/TextView;

.field b:Landroid/widget/TextView;

.field c:Landroid/widget/ImageView;

.field d:Landroid/view/ViewGroup;

.field e:Landroid/view/ViewGroup;

.field f:Z

.field g:Z

.field private h:Lcom/google/android/gms/wallet/common/ui/bq;

.field private i:Landroid/view/LayoutInflater;

.field private j:Lcom/google/android/gms/wallet/common/ui/bl;

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 61
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->k:I

    .line 62
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    .line 63
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Z

    .line 64
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->l:I

    .line 65
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->m:I

    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/content/Context;)V

    .line 71
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 61
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->k:I

    .line 62
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    .line 63
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Z

    .line 64
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->l:I

    .line 65
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->m:I

    .line 76
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/content/Context;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 61
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->k:I

    .line 62
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    .line 63
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Z

    .line 64
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->l:I

    .line 65
    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->m:I

    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/content/Context;)V

    .line 84
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 87
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->hp:I

    invoke-virtual {v0, v1, p0, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 89
    sget v0, Lcom/google/android/gms/j;->lZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    .line 90
    sget v0, Lcom/google/android/gms/j;->th:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/widget/TextView;

    .line 91
    sget v0, Lcom/google/android/gms/j;->fO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/widget/ImageView;

    .line 93
    sget v0, Lcom/google/android/gms/j;->lf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/FrameLayout;

    .line 94
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-direct {v0, p1}, Lcom/google/android/gms/wallet/common/ui/bq;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/bq;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->hr:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->hq:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0, v1, v2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    .line 103
    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    new-instance v0, Lcom/google/android/gms/wallet/ia/g;

    new-array v3, v9, [Ljava/lang/Object;

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/ia/g;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;Landroid/content/Context;[Ljava/lang/Object;Landroid/view/ViewGroup;Landroid/view/ViewGroup;)V

    invoke-virtual {v7, v0, v8}, Lcom/google/android/gms/wallet/common/ui/bq;->a(Landroid/widget/ArrayAdapter;Z)V

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 115
    iput-object v4, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    .line 116
    iput-object v5, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/q;->t:I

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/q;->t:I

    invoke-virtual {v0, p1, v1}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 130
    :goto_0
    invoke-virtual {p0, p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 131
    invoke-direct {p0, v8}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(Z)V

    .line 132
    return-void

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->hr:I

    invoke-virtual {v0, v1, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->hq:I

    invoke-virtual {v0, v1, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v6, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private static a(Landroid/view/ViewGroup;)V
    .locals 6

    .prologue
    .line 311
    const/4 v1, 0x0

    .line 312
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    move v0, v1

    move v1, v5

    :goto_0
    if-ltz v1, :cond_1

    .line 313
    invoke-virtual {p0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 314
    const-string v3, "line_item"

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 315
    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->removeViewInLayout(Landroid/view/View;)V

    .line 316
    const/4 v0, 0x1

    .line 312
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 319
    :cond_1
    if-eqz v0, :cond_2

    .line 320
    invoke-virtual {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 321
    invoke-virtual {p0}, Landroid/view/ViewGroup;->invalidate()V

    .line 323
    :cond_2
    return-void
.end method

.method private a(Z)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 157
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->m:I

    if-nez v0, :cond_0

    .line 158
    sget v0, Lcom/google/android/gms/d;->d:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->m:I

    .line 160
    :cond_0
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 161
    if-eqz p1, :cond_2

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->j:Lcom/google/android/gms/wallet/common/ui/bl;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->j:Lcom/google/android/gms/wallet/common/ui/bl;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bl;->a()V

    .line 174
    :cond_1
    :goto_0
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    .line 175
    return-void

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bq;->b(Z)V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/widget/ImageView;

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    goto :goto_0

    .line 170
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 326
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    if-eqz v0, :cond_0

    .line 327
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Z)V

    .line 331
    :goto_0
    return-void

    .line 329
    :cond_0
    iget v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->l:I

    if-nez v0, :cond_1

    sget v0, Lcom/google/android/gms/d;->e:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->l:I

    :cond_1
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->j:Lcom/google/android/gms/wallet/common/ui/bl;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->j:Lcom/google/android/gms/wallet/common/ui/bl;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bl;->b()V

    :cond_2
    :goto_1
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bq;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/widget/ImageView;

    iget v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->l:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_1
.end method

.method private c(I)I
    .locals 3

    .prologue
    .line 305
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 306
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 307
    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    return v0
.end method

.method private d()Lcom/google/android/gms/wallet/common/ui/bk;
    .locals 1

    .prologue
    .line 421
    new-instance v0, Lcom/google/android/gms/wallet/ia/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/j;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;)V

    return-object v0
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 220
    const-string v1, "line_item"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 221
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 222
    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 142
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->k()Landroid/widget/ArrayAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 145
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/bl;)V
    .locals 1

    .prologue
    .line 136
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->j:Lcom/google/android/gms/wallet/common/ui/bl;

    .line 139
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 235
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->rJ:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 250
    sget v0, Lcom/google/android/gms/j;->rG:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 251
    sget v0, Lcom/google/android/gms/j;->rI:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 252
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 253
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    return-void
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 341
    new-instance v0, Lcom/google/android/gms/wallet/ia/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/h;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 342
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->m()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 343
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    return-void
.end method

.method public final b(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->i:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 227
    const-string v1, "line_item"

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    iget v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->k:I

    invoke-virtual {v1, v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 229
    iget v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->k:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->k:I

    .line 230
    return-object v0
.end method

.method public final b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 201
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Z)V

    .line 202
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setFocusable(Z)V

    .line 203
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 204
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->c:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Z

    .line 208
    return-void
.end method

.method public final b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->ti:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 239
    if-eqz p1, :cond_0

    .line 240
    sget v0, Lcom/google/android/gms/j;->kK:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 246
    :goto_0
    return-void

    .line 244
    :cond_0
    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->ev:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 257
    sget v0, Lcom/google/android/gms/j;->eu:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    sget v0, Lcom/google/android/gms/j;->et:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 260
    return-void
.end method

.method public final b(Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->j()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->g:Z

    if-nez v0, :cond_0

    .line 377
    new-instance v0, Lcom/google/android/gms/wallet/ia/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/i;-><init>(Lcom/google/android/gms/wallet/ia/CartDetailsView;)V

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->h:Lcom/google/android/gms/wallet/common/ui/bq;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/bq;->n()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 381
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/view/ViewGroup;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->a(Landroid/view/ViewGroup;)V

    .line 281
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->k:I

    .line 282
    return-void
.end method

.method public final c(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->sw:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->sx:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 274
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 275
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 276
    return-void
.end method

.method public final c(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->sx:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->e:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/j;->sw:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 266
    sget v1, Lcom/google/android/gms/j;->fH:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 267
    sget v1, Lcom/google/android/gms/j;->fG:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 269
    return-void
.end method

.method public final c(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 412
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 413
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->d()Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_0
    return-void
.end method

.method public final h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 369
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->CC:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 406
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->BE:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 335
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    .line 336
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(Z)V

    .line 337
    return-void

    .line 335
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 286
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 287
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 294
    :goto_0
    return-void

    .line 290
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 291
    const-string v0, "superInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/RelativeLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 292
    const-string v0, "isShowingLongView"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    .line 293
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ia/CartDetailsView;->b(Z)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 298
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 299
    const-string v1, "superInstanceState"

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 300
    const-string v1, "isShowingLongView"

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/ia/CartDetailsView;->f:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 301
    return-object v0
.end method
