.class public final Lcom/google/android/gms/ads/internal/o/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/h/c;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# static fields
.field public static final a:Ljava/lang/String;

.field private static final c:Lcom/google/android/gms/ads/internal/o/e;


# instance fields
.field public final b:Ljava/lang/String;

.field private final d:Ljava/lang/Object;

.field private final e:Lcom/google/android/gms/ads/internal/o/f;

.field private f:Lcom/google/android/gms/common/util/p;

.field private g:Lcom/google/android/gms/ads/internal/a/c;

.field private h:Ljava/math/BigInteger;

.field private final i:Ljava/util/HashSet;

.field private final j:Ljava/util/HashMap;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Landroid/content/Context;

.field private o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field private p:Z

.field private q:Lcom/google/android/gms/ads/internal/b/c;

.field private r:Lcom/google/android/gms/ads/internal/b/d;

.field private s:Lcom/google/android/gms/ads/internal/b/b;

.field private final t:Ljava/util/LinkedList;

.field private u:Z

.field private v:Landroid/os/Bundle;

.field private final w:Lcom/google/android/gms/ads/internal/m/a;

.field private x:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/o/e;-><init>()V

    .line 57
    sput-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/e;->b:Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/ads/internal/o/e;->a:Ljava/lang/String;

    .line 58
    return-void
.end method

.method private constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    .line 72
    sget-object v0, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->h:Ljava/math/BigInteger;

    .line 74
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->i:Ljava/util/HashSet;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->j:Ljava/util/HashMap;

    .line 78
    iput-boolean v2, p0, Lcom/google/android/gms/ads/internal/o/e;->k:Z

    .line 80
    iput-boolean v3, p0, Lcom/google/android/gms/ads/internal/o/e;->l:Z

    .line 82
    iput-boolean v2, p0, Lcom/google/android/gms/ads/internal/o/e;->m:Z

    .line 86
    iput-boolean v3, p0, Lcom/google/android/gms/ads/internal/o/e;->p:Z

    .line 87
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->q:Lcom/google/android/gms/ads/internal/b/c;

    .line 88
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->r:Lcom/google/android/gms/ads/internal/b/d;

    .line 89
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->s:Lcom/google/android/gms/ads/internal/b/b;

    .line 91
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->t:Ljava/util/LinkedList;

    .line 92
    iput-boolean v2, p0, Lcom/google/android/gms/ads/internal/o/e;->u:Z

    .line 160
    invoke-static {}, Lcom/google/android/gms/ads/internal/d/b;->a()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->v:Landroid/os/Bundle;

    .line 161
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->w:Lcom/google/android/gms/ads/internal/m/a;

    .line 431
    invoke-static {}, Lcom/google/android/gms/ads/internal/util/g;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->b:Ljava/lang/String;

    .line 432
    new-instance v0, Lcom/google/android/gms/ads/internal/o/f;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/o/f;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->e:Lcom/google/android/gms/ads/internal/o/f;

    .line 433
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/o/g;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 341
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/ads/internal/o/e;->b(Landroid/content/Context;Lcom/google/android/gms/ads/internal/o/g;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public static a()Lcom/google/android/gms/ads/internal/o/e;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    return-object v0
.end method

.method public static a(ILjava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 370
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-boolean v1, v1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->e:Z

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/e;->n:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    :goto_0
    if-nez v0, :cond_1

    :goto_1
    return-object p1

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/e;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->e(Landroid/content/Context;)Landroid/content/res/Resources;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0, p0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 10

    .prologue
    .line 296
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, v0, Lcom/google/android/gms/ads/internal/o/e;->m:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/o/e;->n:Landroid/content/Context;

    iput-object p1, v0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    const-string v2, "admob"

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string v3, "use_https"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/o/e;->l:Z

    invoke-static {p0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    invoke-static {p0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/Context;)I

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {}, Lcom/google/android/gms/ads/internal/d/b;->a()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/ads/internal/h/c;->a(Landroid/os/Bundle;)V

    :goto_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-boolean v4, v0, Lcom/google/android/gms/ads/internal/o/e;->u:Z

    if-eqz v4, :cond_2

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/o/e;->n:Landroid/content/Context;

    iget-object v5, v0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-static {v4, v2, v5}, Lcom/google/android/gms/ads/internal/m/a;->a(Landroid/content/Context;Ljava/lang/Thread;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/m/a;

    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, p1, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    invoke-static {p0, v2}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/o/e;->x:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/common/util/r;

    invoke-direct {v2}, Lcom/google/android/gms/common/util/r;-><init>()V

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/o/e;->f:Lcom/google/android/gms/common/util/p;

    new-instance v2, Lcom/google/android/gms/ads/internal/a/c;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    new-instance v5, Lcom/google/android/gms/ads/internal/i/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    iget-object v7, v0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    sget-object v8, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v8}, Lcom/google/android/gms/ads/internal/o/e;->p()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "gads:sdk_core_location"

    invoke-virtual {v8, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/gms/ads/internal/i/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;)V

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/ads/internal/a/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/i/a;)V

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/o/e;->g:Lcom/google/android/gms/ads/internal/a/c;

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/ads/internal/o/e;->m:Z

    :cond_0
    monitor-exit v1

    return-void

    :cond_1
    new-instance v2, Lcom/google/android/gms/ads/internal/h/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/ads/internal/h/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/h/c;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_3
    iget-object v4, v0, Lcom/google/android/gms/ads/internal/o/e;->t:Ljava/util/LinkedList;

    invoke-virtual {v4, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v2, v0, Lcom/google/android/gms/ads/internal/o/e;->l:Z

    if-eq p1, v2, :cond_0

    iput-boolean p1, v0, Lcom/google/android/gms/ads/internal/o/e;->l:Z

    const-string v0, "admob"

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "use_https"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 419
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    iget-boolean v1, v0, Lcom/google/android/gms/ads/internal/o/e;->m:Z

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/android/gms/ads/internal/m/a;

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/o/e;->n:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-direct {v1, v2, v0, v3, v3}, Lcom/google/android/gms/ads/internal/m/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-virtual {v1, p0}, Lcom/google/android/gms/ads/internal/m/a;->a(Ljava/lang/Throwable;)V

    .line 420
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/HashSet;)V
    .locals 2

    .prologue
    .line 215
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/e;->i:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(Landroid/content/Context;Lcom/google/android/gms/ads/internal/o/g;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 6

    .prologue
    .line 350
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 351
    :try_start_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 352
    const-string v0, "app"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->e:Lcom/google/android/gms/ads/internal/o/f;

    invoke-virtual {v1, p1, p3}, Lcom/google/android/gms/ads/internal/o/f;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 353
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 355
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->j:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/ads/internal/o/h;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/o/h;->a()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 366
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 357
    :cond_0
    :try_start_1
    const-string v0, "slots"

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 358
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/o/c;

    .line 360
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/o/c;->a()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 362
    :cond_1
    const-string v0, "ads"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->i:Ljava/util/HashSet;

    invoke-interface {p2, v0}, Lcom/google/android/gms/ads/internal/o/g;->a(Ljava/util/HashSet;)V

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 365
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method

.method public static c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/o/e;->k()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/android/gms/ads/internal/o/f;
    .locals 1

    .prologue
    .line 199
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/o/e;->l()Lcom/google/android/gms/ads/internal/o/f;

    move-result-object v0

    return-object v0
.end method

.method public static e()Z
    .locals 1

    .prologue
    .line 248
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/o/e;->m()Z

    move-result v0

    return v0
.end method

.method public static f()Z
    .locals 1

    .prologue
    .line 276
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/o/e;->n()Z

    move-result v0

    return v0
.end method

.method public static g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/o/e;->o()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static i()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 394
    sget-object v0, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/o/e;->p()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method private j()Z
    .locals 2

    .prologue
    .line 155
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 156
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/o/e;->p:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private k()Ljava/lang/String;
    .locals 4

    .prologue
    .line 175
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->h:Ljava/math/BigInteger;

    invoke-virtual {v0}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object v0

    .line 177
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/o/e;->h:Ljava/math/BigInteger;

    sget-object v3, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v2, v3}, Ljava/math/BigInteger;->add(Ljava/math/BigInteger;)Ljava/math/BigInteger;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/ads/internal/o/e;->h:Ljava/math/BigInteger;

    .line 178
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private l()Lcom/google/android/gms/ads/internal/o/f;
    .locals 2

    .prologue
    .line 205
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 206
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->e:Lcom/google/android/gms/ads/internal/o/f;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private m()Z
    .locals 3

    .prologue
    .line 255
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 256
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/o/e;->k:Z

    .line 257
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/ads/internal/o/e;->k:Z

    .line 258
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 280
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 281
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/o/e;->l:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private o()Ljava/lang/String;
    .locals 2

    .prologue
    .line 290
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 291
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->x:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private p()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 398
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 399
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->v:Landroid/os/Bundle;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 400
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/google/android/gms/ads/internal/b/d;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 105
    sget-object v1, Lcom/google/android/gms/ads/internal/o/e;->c:Lcom/google/android/gms/ads/internal/o/e;

    invoke-direct {v1}, Lcom/google/android/gms/ads/internal/o/e;->p()Landroid/os/Bundle;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/ads/internal/d/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0xe

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/o/e;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    :cond_0
    :goto_0
    return-object v0

    .line 112
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 113
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/o/e;->q:Lcom/google/android/gms/ads/internal/b/c;

    if-nez v2, :cond_3

    .line 114
    instance-of v2, p1, Landroid/app/Activity;

    if-nez v2, :cond_2

    .line 115
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 117
    :cond_2
    :try_start_1
    new-instance v2, Lcom/google/android/gms/ads/internal/b/c;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Application;

    check-cast p1, Landroid/app/Activity;

    invoke-direct {v2, v0, p1}, Lcom/google/android/gms/ads/internal/b/c;-><init>(Landroid/app/Application;Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/google/android/gms/ads/internal/o/e;->q:Lcom/google/android/gms/ads/internal/b/c;

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->s:Lcom/google/android/gms/ads/internal/b/b;

    if-nez v0, :cond_4

    .line 121
    new-instance v0, Lcom/google/android/gms/ads/internal/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->s:Lcom/google/android/gms/ads/internal/b/b;

    .line 123
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->r:Lcom/google/android/gms/ads/internal/b/d;

    if-nez v0, :cond_5

    .line 124
    new-instance v0, Lcom/google/android/gms/ads/internal/b/d;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/o/e;->q:Lcom/google/android/gms/ads/internal/b/c;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/o/e;->s:Lcom/google/android/gms/ads/internal/b/b;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/o/e;->v:Landroid/os/Bundle;

    new-instance v5, Lcom/google/android/gms/ads/internal/m/a;

    iget-object v6, p0, Lcom/google/android/gms/ads/internal/o/e;->n:Landroid/content/Context;

    iget-object v7, p0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/google/android/gms/ads/internal/m/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/Thread$UncaughtExceptionHandler;Ljava/lang/Thread$UncaughtExceptionHandler;)V

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/ads/internal/b/d;-><init>(Lcom/google/android/gms/ads/internal/b/c;Lcom/google/android/gms/ads/internal/b/b;Landroid/os/Bundle;Lcom/google/android/gms/ads/internal/m/a;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->r:Lcom/google/android/gms/ads/internal/b/d;

    .line 130
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->r:Lcom/google/android/gms/ads/internal/b/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/d;->a()V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->r:Lcom/google/android/gms/ads/internal/b/d;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 384
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 385
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/o/e;->u:Z

    .line 386
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/o/e;->v:Landroid/os/Bundle;

    .line 387
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->t:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/o/e;->n:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->t:Ljava/util/LinkedList;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/o/e;->o:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/ads/internal/m/a;->a(Landroid/content/Context;Ljava/lang/Thread;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/m/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/o/c;)V
    .locals 2

    .prologue
    .line 193
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 194
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->i:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 195
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/o/h;)V
    .locals 2

    .prologue
    .line 239
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 240
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 149
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 150
    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/gms/ads/internal/o/e;->p:Z

    .line 151
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Lcom/google/android/gms/common/util/p;
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/o/e;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->f:Lcom/google/android/gms/common/util/p;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 138
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()Lcom/google/android/gms/ads/internal/a/c;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/o/e;->g:Lcom/google/android/gms/ads/internal/a/c;

    return-object v0
.end method
