.class public final Lcom/google/android/gms/auth/authzen/transaction/m;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/content/Context;[B)[B
    .locals 8

    .prologue
    .line 97
    :try_start_0
    new-instance v0, Lcom/google/ab/b/a/a/p;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/p;-><init>()V

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/a/p;

    .line 98
    iget-object v1, v0, Lcom/google/ab/b/a/a/p;->a:Lcom/google/ab/b/a/a/s;

    .line 100
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/s;->a:Z

    if-nez v2, :cond_0

    .line 101
    new-instance v2, Lcom/google/ab/b/a/c/b;

    invoke-direct {v2}, Lcom/google/ab/b/a/c/b;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/ab/b/a/a/s;->a(Lcom/google/ab/b/a/c/b;)Lcom/google/ab/b/a/a/s;

    .line 103
    :cond_0
    iget-object v1, v1, Lcom/google/ab/b/a/a/s;->b:Lcom/google/ab/b/a/c/b;

    invoke-static {p0}, Lcom/google/android/gms/auth/authzen/a/a;->b(Landroid/content/Context;)Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/ab/b/a/c/b;->a(Z)Lcom/google/ab/b/a/c/b;

    move-result-object v1

    .line 105
    iget-boolean v2, v1, Lcom/google/ab/b/a/c/b;->a:Z

    if-nez v2, :cond_1

    .line 137
    :goto_0
    return-object p1

    .line 109
    :cond_1
    iget-boolean v2, v1, Lcom/google/ab/b/a/c/b;->b:Z

    if-nez v2, :cond_2

    .line 111
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/ab/b/a/c/b;->b(Z)Lcom/google/ab/b/a/c/b;

    .line 114
    :cond_2
    new-instance v2, Lcom/google/android/gms/common/b;

    invoke-direct {v2}, Lcom/google/android/gms/common/b;-><init>()V

    .line 115
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/google/android/gms/auth/devicesignals/DeviceSignalsService;

    invoke-direct {v4, p0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v5, 0x1

    invoke-virtual {v3, p0, v4, v2, v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    if-eqz v3, :cond_3

    .line 119
    :try_start_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/auth/firstparty/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/auth/firstparty/a/a;

    move-result-object v3

    .line 121
    invoke-interface {v3}, Lcom/google/android/gms/auth/firstparty/a/a;->b()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Lcom/google/ab/b/a/c/b;->a(J)Lcom/google/ab/b/a/c/b;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-interface {v3}, Lcom/google/android/gms/auth/firstparty/a/a;->a()J

    move-result-wide v6

    sub-long/2addr v4, v6

    invoke-virtual {v1, v4, v5}, Lcom/google/ab/b/a/c/b;->b(J)Lcom/google/ab/b/a/c/b;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    invoke-virtual {v1, p0, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 134
    :goto_1
    invoke-virtual {v0}, Lcom/google/ab/b/a/a/p;->g()[B
    :try_end_2
    .catch Lcom/google/protobuf/a/e; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object p1

    goto :goto_0

    .line 126
    :catch_0
    move-exception v1

    :goto_2
    :try_start_3
    const-string v1, "AuthZen"

    const-string v3, "Bind to DeviceSignalsService interrupted"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 129
    :try_start_4
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    invoke-virtual {v1, p0, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_4
    .catch Lcom/google/protobuf/a/e; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    .line 136
    :catch_1
    move-exception v0

    const-string v0, "AuthZen"

    const-string v1, "Failed to parse TxLedger"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    invoke-virtual {v1, p0, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0

    .line 132
    :cond_3
    const-string v1, "AuthZen"

    const-string v2, "Failed to bind to DeviceSignalsService"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_5
    .catch Lcom/google/protobuf/a/e; {:try_start_5 .. :try_end_5} :catch_1

    goto :goto_1

    .line 126
    :catch_2
    move-exception v1

    goto :goto_2
.end method
