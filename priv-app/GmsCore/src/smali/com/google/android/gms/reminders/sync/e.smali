.class public final Lcom/google/android/gms/reminders/sync/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final g:Lcom/google/f/a/a/w;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/reminders/sync/d;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Lcom/google/android/gms/reminders/a/d;

.field private final e:Landroid/content/ContentResolver;

.field private final f:Landroid/content/SyncResult;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lcom/google/f/a/a/w;

    invoke-direct {v0}, Lcom/google/f/a/a/w;-><init>()V

    .line 69
    sput-object v0, Lcom/google/android/gms/reminders/sync/e;->g:Lcom/google/f/a/a/w;

    const/16 v1, 0xb

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    iput-object v1, v0, Lcom/google/f/a/a/w;->a:[I

    .line 83
    return-void

    .line 69
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0x3
        0x4
        0x5
        0x7
        0xa
        0xd
        0xe
        0xf
        0x10
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/reminders/sync/d;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/reminders/a/d;Landroid/content/SyncResult;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/google/android/gms/reminders/sync/e;->a:Landroid/content/Context;

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/reminders/sync/e;->b:Lcom/google/android/gms/reminders/sync/d;

    .line 57
    iput-object p3, p0, Lcom/google/android/gms/reminders/sync/e;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 58
    iput-object p4, p0, Lcom/google/android/gms/reminders/sync/e;->d:Lcom/google/android/gms/reminders/a/d;

    .line 59
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->e:Landroid/content/ContentResolver;

    .line 60
    iput-object p5, p0, Lcom/google/android/gms/reminders/sync/e;->f:Landroid/content/SyncResult;

    .line 61
    return-void
.end method

.method private b()I
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->e:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    new-array v2, v4, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v6

    const-string v3, "client_assigned_id"

    aput-object v3, v2, v8

    const-string v3, "server_assigned_id"

    aput-object v3, v2, v9

    const-string v3, "is_dirty=? AND account_id=? AND deleted=?"

    new-array v4, v4, [Ljava/lang/String;

    const-string v7, "1"

    aput-object v7, v4, v6

    iget-object v7, p0, Lcom/google/android/gms/reminders/sync/e;->d:Lcom/google/android/gms/reminders/a/d;

    iget-wide v10, v7, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v8

    const-string v7, "1"

    aput-object v7, v4, v9

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 191
    if-nez v1, :cond_0

    move v0, v6

    .line 234
    :goto_0
    return v0

    .line 195
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 196
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 199
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 200
    const/4 v0, 0x2

    invoke-static {v1, v0}, Lcom/google/android/gms/reminders/c/c;->a(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_1

    .line 204
    new-instance v4, Lcom/google/f/a/w;

    invoke-direct {v4}, Lcom/google/f/a/w;-><init>()V

    .line 205
    const/4 v6, 0x1

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/google/f/a/w;->b:Ljava/lang/String;

    .line 206
    iput-object v0, v4, Lcom/google/f/a/w;->a:Ljava/lang/Long;

    .line 207
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    :cond_1
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 214
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 217
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 218
    new-instance v0, Lcom/google/f/a/a/d;

    invoke-direct {v0}, Lcom/google/f/a/a/d;-><init>()V

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/reminders/sync/b;->a(Landroid/content/Context;)Lcom/google/f/a/a/t;

    move-result-object v1

    iput-object v1, v0, Lcom/google/f/a/a/d;->a:Lcom/google/f/a/a/t;

    .line 220
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/f/a/w;

    iput-object v1, v0, Lcom/google/f/a/a/d;->b:[Lcom/google/f/a/w;

    .line 221
    iget-object v1, v0, Lcom/google/f/a/a/d;->b:[Lcom/google/f/a/w;

    invoke-interface {v2, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 223
    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/e;->b:Lcom/google/android/gms/reminders/sync/d;

    iget-object v7, p0, Lcom/google/android/gms/reminders/sync/e;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v6, v1, Lcom/google/android/gms/reminders/sync/d;->a:Lcom/google/android/gms/common/server/q;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/reminders/delete?alt=proto"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v10

    new-instance v11, Lcom/google/f/a/a/e;

    invoke-direct {v11}, Lcom/google/f/a/a/e;-><init>()V

    invoke-virtual/range {v6 .. v11}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/f/a/a/e;

    .line 225
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Delete response:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v0, Landroid/content/SyncStats;->numDeletes:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v0, Landroid/content/SyncStats;->numDeletes:J

    .line 229
    :cond_3
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->e:Landroid/content/ContentResolver;

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "_id IN ("

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, ","

    invoke-static {v6, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 234
    :cond_4
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 12

    .prologue
    .line 89
    invoke-direct {p0}, Lcom/google/android/gms/reminders/sync/e;->b()I

    move-result v7

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->a:Landroid/content/Context;

    sget-object v1, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    const-string v2, "is_dirty=? AND account_id=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/gms/reminders/sync/e;->d:Lcom/google/android/gms/reminders/a/d;

    iget-wide v8, v5, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    new-instance v8, Lcom/google/android/gms/reminders/model/e;

    invoke-direct {v8, v0}, Lcom/google/android/gms/reminders/model/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v8}, Lcom/google/android/gms/reminders/model/e;->c()I

    move-result v9

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dirty reminders: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_4

    new-instance v0, Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-virtual {v8, v6}, Lcom/google/android/gms/reminders/model/e;->b(I)Lcom/google/android/gms/reminders/model/Task;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/reminders/model/TaskEntity;-><init>(Lcom/google/android/gms/reminders/model/Task;)V

    new-instance v10, Lcom/google/f/a/a/r;

    invoke-direct {v10}, Lcom/google/f/a/a/r;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/google/f/a/w;

    invoke-direct {v1}, Lcom/google/f/a/w;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskId;->a()Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/w;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskId;->d()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/w;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskId;->e()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/w;->c:Ljava/lang/String;

    iput-object v1, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/google/f/a/a/u;

    invoke-direct {v1}, Lcom/google/f/a/a/u;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskList;->a()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/a/u;->a:Ljava/lang/Integer;

    iput-object v1, v10, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->n()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/DateTime;)Lcom/google/f/a/h;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->o()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/DateTime;)Lcom/google/f/a/h;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->g()Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->h()Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->i()Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->j()Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->k()Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->l()Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->m()Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->q()Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v10, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->p()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v1

    if-nez v1, :cond_2

    const/4 v0, 0x0

    :goto_1
    iput-object v0, v10, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Dirty reminders "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    iget-object v1, v1, Lcom/google/f/a/w;->a:Ljava/lang/Long;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    iget-object v0, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    iget-object v0, v0, Lcom/google/f/a/w;->a:Ljava/lang/Long;

    if-nez v0, :cond_3

    iget-object v0, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    new-instance v4, Lcom/google/f/a/a/b;

    invoke-direct {v4}, Lcom/google/f/a/a/b;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/reminders/sync/b;->a(Landroid/content/Context;)Lcom/google/f/a/a/t;

    move-result-object v1

    iput-object v1, v4, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    iput-object v10, v4, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    iget-object v1, v10, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    iput-object v1, v4, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    iput-object v0, v4, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    const/4 v0, 0x0

    iput-object v0, v10, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    const/4 v0, 0x0

    iput-object v0, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    const/4 v0, 0x0

    iput-object v0, v10, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    const/4 v0, 0x0

    iput-object v0, v10, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    const/4 v0, 0x0

    iput-object v0, v10, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    const/4 v0, 0x0

    iput-object v0, v10, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->b:Lcom/google/android/gms/reminders/sync/d;

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/e;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/reminders/sync/d;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/reminders/create?alt=proto"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/f/a/a/c;

    invoke-direct {v5}, Lcom/google/f/a/a/c;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numInserts:J

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Create response: newId"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    :cond_2
    new-instance v0, Lcom/google/f/a/j;

    invoke-direct {v0}, Lcom/google/f/a/j;-><init>()V

    invoke-interface {v1}, Lcom/google/android/gms/reminders/model/Location;->a()Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    invoke-interface {v1}, Lcom/google/android/gms/reminders/model/Location;->d()Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    invoke-interface {v1}, Lcom/google/android/gms/reminders/model/Location;->e()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/reminders/model/Location;->f()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    invoke-interface {v1}, Lcom/google/android/gms/reminders/model/Location;->g()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    invoke-interface {v1}, Lcom/google/android/gms/reminders/model/Location;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    goto/16 :goto_1

    :cond_3
    iget-object v0, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    new-instance v4, Lcom/google/f/a/a/v;

    invoke-direct {v4}, Lcom/google/f/a/a/v;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/reminders/sync/b;->a(Landroid/content/Context;)Lcom/google/f/a/a/t;

    move-result-object v1

    iput-object v1, v4, Lcom/google/f/a/a/v;->a:Lcom/google/f/a/a/t;

    iput-object v10, v4, Lcom/google/f/a/a/v;->f:Lcom/google/f/a/a/r;

    iput-object v0, v4, Lcom/google/f/a/a/v;->b:Lcom/google/f/a/w;

    sget-object v0, Lcom/google/android/gms/reminders/sync/e;->g:Lcom/google/f/a/a/w;

    iput-object v0, v4, Lcom/google/f/a/a/v;->d:Lcom/google/f/a/a/w;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v4, Lcom/google/f/a/a/v;->e:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->b:Lcom/google/android/gms/reminders/sync/d;

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/e;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/reminders/sync/d;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/reminders/update?alt=proto"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/f/a/a/x;

    invoke-direct {v5}, Lcom/google/f/a/a/x;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/e;->f:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numUpdates:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numUpdates:J

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Update response: newId"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v10, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    goto/16 :goto_2

    :cond_4
    invoke-virtual {v8}, Lcom/google/android/gms/reminders/model/e;->w_()V

    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "is_dirty"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/e;->e:Landroid/content/ContentResolver;

    sget-object v2, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    const-string v3, "is_dirty=? AND account_id=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Lcom/google/android/gms/reminders/sync/e;->d:Lcom/google/android/gms/reminders/a/d;

    iget-wide v10, v6, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v0, v3, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 92
    add-int v0, v7, v9

    if-nez v0, :cond_5

    .line 93
    const/4 v0, -0x1

    .line 96
    :goto_3
    return v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_3
.end method
