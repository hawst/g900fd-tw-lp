.class public abstract Lcom/google/android/gms/plus/internal/j;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/plus/internal/j;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/i;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/plus/internal/i;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/internal/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/internal/k;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 10

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x0

    const/4 v9, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 560
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v9

    :goto_0
    return v9

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 55
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V

    .line 56
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 61
    :sswitch_2
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 67
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 68
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 74
    :sswitch_3
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 79
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/j;->b(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V

    .line 80
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 85
    :sswitch_4
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/j;->a()Ljava/lang/String;

    move-result-object v0

    .line 87
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 88
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 93
    :sswitch_5
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 97
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/j;->c(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V

    .line 99
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 104
    :sswitch_6
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 107
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;)V

    .line 108
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 113
    :sswitch_7
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v2

    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    move-object v1, v0

    .line 124
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 125
    sget-object v0, Landroid/os/Bundle;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 130
    :goto_2
    invoke-virtual {p0, v2, v1, v0}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Landroid/net/Uri;Landroid/os/Bundle;)V

    .line 131
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_0
    move-object v1, v5

    .line 121
    goto :goto_1

    :cond_1
    move-object v0, v5

    .line 128
    goto :goto_2

    .line 136
    :sswitch_8
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 140
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 141
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/j;->d(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V

    .line 142
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 147
    :sswitch_9
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 151
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 152
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 158
    :sswitch_a
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v1

    .line 162
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    .line 169
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 170
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 175
    :sswitch_b
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 182
    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;ILjava/lang/String;)V

    .line 183
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 188
    :sswitch_c
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v1

    .line 192
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v0, v9

    .line 194
    :goto_3
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    move v8, v9

    .line 195
    :cond_2
    invoke-virtual {p0, v1, v0, v8}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;ZZ)V

    .line 196
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_3
    move v0, v8

    .line 192
    goto :goto_3

    .line 201
    :sswitch_d
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 205
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_4

    .line 206
    sget-object v1, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->CREATOR:Lcom/google/android/gms/plus/internal/model/acls/a;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/acls/a;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    move-result-object v5

    .line 211
    :cond_4
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V

    .line 212
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 217
    :sswitch_e
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 219
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 221
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 223
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_5

    .line 224
    sget-object v2, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/apps/d;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/apps/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v5

    .line 229
    :cond_5
    invoke-virtual {p0, v0, v1, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V

    .line 230
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 235
    :sswitch_f
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 239
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 241
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_6

    move v8, v9

    .line 243
    :cond_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 244
    invoke-virtual {p0, v0, v1, v8, v2}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ZLjava/lang/String;)V

    .line 245
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 250
    :sswitch_10
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 252
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 254
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_7

    .line 255
    sget-object v1, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->CREATOR:Lcom/google/android/gms/plus/internal/model/acls/a;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/acls/a;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    move-result-object v5

    .line 260
    :cond_7
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->b(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V

    .line 261
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 266
    :sswitch_11
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 268
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 270
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_8

    .line 271
    sget-object v1, Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;->CREATOR:Lcom/google/android/gms/plus/internal/model/acls/a;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/acls/a;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;

    move-result-object v5

    .line 276
    :cond_8
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->c(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V

    .line 277
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 282
    :sswitch_12
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 284
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 286
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 288
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_9

    .line 289
    sget-object v2, Lcom/google/android/gms/common/people/data/Audience;->CREATOR:Lcom/google/android/gms/common/people/data/b;

    invoke-static {p2}, Lcom/google/android/gms/common/people/data/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v5

    .line 294
    :cond_9
    invoke-virtual {p0, v0, v1, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    .line 295
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 300
    :sswitch_13
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 302
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v1

    .line 304
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 306
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 307
    sget-object v0, Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/apps/d;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/apps/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;

    move-result-object v3

    .line 313
    :goto_4
    sget-object v0, Lcom/google/android/gms/common/people/data/AudienceMember;->CREATOR:Lcom/google/android/gms/common/people/data/d;

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v4

    .line 315
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    move v5, v9

    .line 317
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    move v6, v9

    .line 319
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_e

    move v7, v9

    .line 321
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    move v8, v9

    :cond_a
    move-object v0, p0

    .line 322
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)V

    .line 323
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_b
    move-object v3, v5

    .line 310
    goto :goto_4

    :cond_c
    move v5, v8

    .line 315
    goto :goto_5

    :cond_d
    move v6, v8

    .line 317
    goto :goto_6

    :cond_e
    move v7, v8

    .line 319
    goto :goto_7

    .line 328
    :sswitch_14
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 330
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 332
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_f

    .line 333
    sget-object v1, Lcom/google/android/gms/plus/model/posts/Comment;->CREATOR:Lcom/google/android/gms/plus/model/posts/b;

    invoke-static {p2}, Lcom/google/android/gms/plus/model/posts/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Comment;

    move-result-object v5

    .line 338
    :cond_f
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Comment;)V

    .line 339
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 344
    :sswitch_15
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 348
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_10

    .line 349
    sget-object v1, Lcom/google/android/gms/plus/model/posts/Post;->CREATOR:Lcom/google/android/gms/plus/model/posts/d;

    invoke-static {p2}, Lcom/google/android/gms/plus/model/posts/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v5

    .line 354
    :cond_10
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 355
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 360
    :sswitch_16
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 364
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_11

    .line 365
    sget-object v1, Lcom/google/android/gms/plus/model/posts/Post;->CREATOR:Lcom/google/android/gms/plus/model/posts/d;

    invoke-static {p2}, Lcom/google/android/gms/plus/model/posts/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v5

    .line 370
    :cond_11
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->b(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 371
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 376
    :sswitch_17
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 378
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 380
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_12

    .line 381
    sget-object v1, Lcom/google/android/gms/plus/model/posts/Post;->CREATOR:Lcom/google/android/gms/plus/model/posts/d;

    invoke-static {p2}, Lcom/google/android/gms/plus/model/posts/d;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v5

    .line 386
    :cond_12
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->c(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 387
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 392
    :sswitch_18
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 394
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v1

    .line 396
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 398
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 400
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v4

    .line 402
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    .line 403
    sget-object v0, Landroid/net/Uri;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    move-object v5, v0

    .line 409
    :cond_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    .line 410
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V

    .line 411
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 416
    :sswitch_19
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 418
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 420
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 421
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/j;->e(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V

    .line 422
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 427
    :sswitch_1a
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 429
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 431
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 432
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/internal/j;->f(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V

    .line 433
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 438
    :sswitch_1b
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 440
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 442
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 444
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 446
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 447
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;)V

    .line 448
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 453
    :sswitch_1c
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 455
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v1

    .line 457
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 459
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 461
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_14

    move v4, v9

    .line 463
    :goto_8
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    .line 464
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 465
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :cond_14
    move v4, v8

    .line 461
    goto :goto_8

    .line 470
    :sswitch_1d
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 472
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 473
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/internal/j;->b(Lcom/google/android/gms/plus/internal/f;)V

    .line 474
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 479
    :sswitch_1e
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 481
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 483
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 485
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 487
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 488
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V

    .line 489
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 494
    :sswitch_1f
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 496
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 498
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 500
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_15

    .line 501
    sget-object v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;->CREATOR:Lcom/google/android/gms/plus/service/v1whitelisted/models/fo;

    invoke-static {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fo;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;

    move-result-object v5

    .line 506
    :cond_15
    invoke-virtual {p0, v0, v1, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    .line 507
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 512
    :sswitch_20
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 514
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 516
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 518
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 520
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 521
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/j;->b(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V

    .line 522
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 527
    :sswitch_21
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 529
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 531
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    if-eqz v1, :cond_16

    .line 532
    sget-object v1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/a;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/smart_profile/a;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;

    move-result-object v5

    .line 537
    :cond_16
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V

    .line 538
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 543
    :sswitch_22
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 545
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/g;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/f;

    move-result-object v0

    .line 547
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 549
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_17

    .line 550
    sget-object v2, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;->CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/c;

    invoke-static {p2}, Lcom/google/android/gms/plus/internal/model/smart_profile/c;->a(Landroid/os/Parcel;)Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;

    move-result-object v5

    .line 555
    :cond_17
    invoke-virtual {p0, v0, v1, v5}, Lcom/google/android/gms/plus/internal/j;->a(Lcom/google/android/gms/plus/internal/f;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V

    .line 556
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
