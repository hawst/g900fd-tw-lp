.class public final Lcom/google/android/gms/social/location/ag;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/social/location/ag;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    invoke-static {p0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 49
    const-string v1, "message"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    new-instance v1, Lcom/google/android/gms/social/location/ag;

    invoke-direct {v1}, Lcom/google/android/gms/social/location/ag;-><init>()V

    .line 52
    invoke-virtual {v1, v0}, Lcom/google/android/gms/social/location/ag;->setArguments(Landroid/os/Bundle;)V

    .line 53
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/social/location/ag;->a(Z)V

    .line 54
    return-object v1
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 76
    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/ag;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/ag;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    .line 62
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 63
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 64
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setTitle(Ljava/lang/CharSequence;)V

    .line 66
    :cond_0
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 67
    iget-boolean v0, p0, Landroid/support/v4/app/m;->c:Z

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 68
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 70
    return-object v2
.end method
