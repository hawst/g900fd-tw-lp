.class public Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/be/proximity/authorization/c;


# static fields
.field private static final c:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

.field b:Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;

.field private final f:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;

.field private final g:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

.field private final h:Ljava/lang/Object;

.field private final i:Lcom/google/android/gms/auth/be/proximity/g;

.field private final j:Lcom/google/android/gms/auth/be/proximity/a/c;

.field private final k:Lcom/google/android/gms/auth/be/proximity/authorization/j;

.field private final l:Lcom/google/ab/b/a/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 69
    const-class v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/a/c;)V
    .locals 9

    .prologue
    .line 113
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/j;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/authorization/j;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/gms/auth/be/proximity/g;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/g;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/auth/b/a;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v5, Lcom/google/ab/b/a/d/a;

    invoke-direct {v5}, Lcom/google/ab/b/a/d/a;-><init>()V

    :goto_0
    new-instance v6, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/at;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/trustagent/at;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    move-result-object v1

    invoke-direct {v6, p1, p2, v0, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/a/c;Lcom/google/android/gms/auth/trustagent/at;Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;)V

    new-instance v7, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;

    invoke-direct {v7}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;-><init>()V

    new-instance v8, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

    new-instance v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;-><init>(Landroid/content/Context;)V

    invoke-direct {v8, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;-><init>(Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/a/c;Lcom/google/android/gms/auth/be/proximity/authorization/j;Lcom/google/android/gms/auth/be/proximity/g;Lcom/google/ab/b/a/d/a;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;)V

    .line 126
    return-void

    .line 113
    :cond_0
    new-instance v5, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a/a;

    invoke-direct {v5}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a/a;-><init>()V

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/a/c;B)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/a/c;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/a/c;Lcom/google/android/gms/auth/be/proximity/authorization/j;Lcom/google/android/gms/auth/be/proximity/g;Lcom/google/ab/b/a/d/a;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->d:Landroid/content/Context;

    .line 139
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/a/c;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->j:Lcom/google/android/gms/auth/be/proximity/a/c;

    .line 140
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/j;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->k:Lcom/google/android/gms/auth/be/proximity/authorization/j;

    .line 141
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/g;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->i:Lcom/google/android/gms/auth/be/proximity/g;

    .line 142
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/d/a;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    .line 143
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;

    .line 144
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->f:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;

    .line 145
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->g:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

    .line 146
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->h:Ljava/lang/Object;

    .line 147
    return-void
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;[B)Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;
    .locals 3

    .prologue
    .line 292
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a(Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->g:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

    invoke-virtual {v2, p2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->b([B)[B

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/ab/b/a/d/a;->a([BLjava/util/List;[B)I

    move-result v0

    .line 296
    if-gez v0, :cond_0

    .line 298
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    const-string v1, "No PermitAccess can be found for current connection."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->i:Lcom/google/android/gms/auth/be/proximity/g;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/g;->b()V

    .line 300
    const/4 v0, 0x0

    .line 302
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    const-string v2, "AUTHZEN_PUBLIC_KEY"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 303
    :catch_0
    move-exception v0

    .line 304
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Error when initializing the secure channel."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 305
    :catch_1
    move-exception v0

    .line 306
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Error when initializing the secure channel."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;)Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->g:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a()Ljava/security/KeyPair;

    move-result-object v2

    .line 258
    if-nez v2, :cond_0

    .line 260
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 279
    :goto_0
    return-object v0

    .line 263
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 265
    const-string v0, "AUTHZEN_PUBLIC_KEY"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    .line 267
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;->c()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/m;->a([B)Lcom/google/ab/b/a/f/m;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/d;->a(Lcom/google/ab/b/a/f/m;)Ljava/security/PublicKey;

    move-result-object v0

    .line 269
    invoke-virtual {v2}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v4

    invoke-static {v4, v0}, Lcom/google/ab/b/a/e/i;->a(Ljava/security/PrivateKey;Ljava/security/PublicKey;)Ljavax/crypto/SecretKey;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 272
    :catch_0
    move-exception v0

    .line 273
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Error when creating symmetric key."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 275
    :catch_1
    move-exception v0

    .line 276
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Error when creating symmetric key."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    move-object v0, v1

    .line 279
    goto :goto_0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 188
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    const-string v1, "Sending status update..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 190
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    invoke-virtual {v0}, Lcom/google/ab/b/a/d/a;->a()Lcom/google/ab/b/a/d/b;

    move-result-object v0

    sget-object v2, Lcom/google/ab/b/a/d/b;->c:Lcom/google/ab/b/a/d/b;

    if-eq v0, v2, :cond_0

    .line 191
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    const-string v2, "No need to send status update message when secure channel is not ready."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    monitor-exit v1

    .line 202
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    if-nez v0, :cond_1

    .line 197
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Permit should not be null when secure channel is established."

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 201
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->a(Lcom/google/ab/b/a/d/a;)V

    .line 202
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/auth/be/proximity/authorization/d;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 208
    instance-of v0, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;

    if-nez v0, :cond_0

    .line 209
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Received unexpected message: %s."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 214
    :try_start_0
    check-cast p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;

    .line 215
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/b;->a:[I

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    invoke-virtual {v2}, Lcom/google/ab/b/a/d/a;->a()Lcom/google/ab/b/a/d/b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/ab/b/a/d/b;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    .line 248
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Secure channel is in wrong state."

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 250
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 217
    :pswitch_0
    :try_start_1
    iget-object v0, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;->b:Ljava/lang/String;

    .line 218
    if-nez v0, :cond_1

    .line 219
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Missing mandatory permit Id."

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/proximity/b/d;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/b/c;

    move-result-object v0

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/be/proximity/b/c;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    if-nez v0, :cond_2

    .line 224
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Cannot get permit with permitId %s from database."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;->a:[B

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a(Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;[B)Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->b:Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    invoke-virtual {v0}, Lcom/google/ab/b/a/d/a;->b()[B

    move-result-object v0

    .line 233
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->g:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a([B)V

    .line 234
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->j:Lcom/google/android/gms/auth/be/proximity/a/c;

    new-instance v3, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;

    invoke-direct {v3, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;-><init>([B)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/auth/be/proximity/a/c;->a(Ljava/lang/Object;)V

    .line 250
    :goto_0
    monitor-exit v1

    return-void

    .line 238
    :pswitch_1
    iget-object v0, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;->a:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/d/a;->a([B)[B
    :try_end_2
    .catch Ljava/security/SignatureException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 242
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/f;->a(Lcom/google/ab/b/a/d/a;)V

    goto :goto_0

    .line 239
    :catch_0
    move-exception v0

    new-instance v2, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v3, "Error when finishing initialization of the secure channel."

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 245
    :pswitch_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    iget-object v2, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/g;->a:[B

    invoke-virtual {v0, v2}, Lcom/google/ab/b/a/d/a;->b([B)[B
    :try_end_4
    .catch Ljava/security/SignatureException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v0

    :try_start_5
    new-instance v2, Lorg/json/JSONObject;

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([B)V

    invoke-direct {v2, v3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "type"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v3, "event"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/l;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/be/proximity/authorization/a/c/l;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    const-string v3, "Handling status update message..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/l;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->k:Lcom/google/android/gms/auth/be/proximity/authorization/j;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/j;->b()V
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catch_1
    move-exception v0

    :try_start_6
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Exception when handling the message."

    invoke-direct {v2, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception v0

    new-instance v2, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v3, "Error when decoding the message."

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :pswitch_4
    :try_start_7
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->k:Lcom/google/android/gms/auth/be/proximity/authorization/j;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/j;->a()V

    goto :goto_0

    :cond_3
    const-string v3, "decrypt_request"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-static {v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    const-string v3, "Handling decrypt request message..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Lorg/json/JSONException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->g:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;

    iget-object v0, v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;->a:[B

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->c([B)[B

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->j:Lcom/google/android/gms/auth/be/proximity/a/c;

    new-instance v3, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;

    iget-object v4, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    iget-object v5, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->f:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/k;

    new-instance v5, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/j;

    const/4 v6, 0x0

    invoke-direct {v5, v0, v6}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/j;-><init>([BB)V

    invoke-virtual {v5}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/j;->a()[B

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/ab/b/a/d/a;->c([B)[B

    move-result-object v0

    invoke-direct {v3, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;-><init>([B)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/auth/be/proximity/a/c;->a(Ljava/lang/Object;)V
    :try_end_8
    .catch Lorg/json/JSONException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_0

    :catch_3
    move-exception v0

    :try_start_9
    new-instance v2, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v3, "Error when serializing decrypted message."

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_4
    const-string v2, "unlock_request"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/o;->a()Lcom/google/android/gms/auth/be/proximity/authorization/a/c/o;

    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    const-string v2, "Handling unlock request message..."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->j:Lcom/google/android/gms/auth/be/proximity/a/c;

    new-instance v2, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;

    iget-object v3, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->l:Lcom/google/ab/b/a/d/a;

    new-instance v4, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/p;

    invoke-direct {v4}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/p;-><init>()V

    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/p;->a()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/ab/b/a/d/a;->c([B)[B

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/h;-><init>([B)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/auth/be/proximity/a/c;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_5
    new-instance v2, Ljava/io/IOException;

    const-string v3, "Received unexpected type of message: %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_9
    .catch Lorg/json/JSONException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 245
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Ljava/util/Set;)V
    .locals 3

    .prologue
    .line 156
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->c:Ljava/lang/String;

    const-string v1, "Processing permit update..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    if-nez v0, :cond_0

    .line 162
    monitor-exit v1

    .line 176
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->a:Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/proximity/data/Permit;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 166
    monitor-exit v1

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 168
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->b:Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    if-nez v0, :cond_2

    .line 171
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Exception in authorization since the data has been changed."

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/a;->b:Lcom/google/android/gms/auth/firstparty/proximity/data/PermitAccess;

    invoke-interface {p2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 176
    monitor-exit v1

    goto :goto_0

    .line 179
    :cond_3
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Exception in authorization since the data has been changed."

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0
.end method
