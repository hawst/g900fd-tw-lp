.class public final Lcom/google/android/gms/cast/activity/l;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Landroid/widget/Button;

.field private c:[Landroid/widget/EditText;

.field private d:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/gms/cast/a/b;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/cast/activity/l;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 70
    return-void
.end method

.method public static a(Z)Lcom/google/android/gms/cast/activity/l;
    .locals 3

    .prologue
    .line 89
    new-instance v0, Lcom/google/android/gms/cast/activity/l;

    invoke-direct {v0}, Lcom/google/android/gms/cast/activity/l;-><init>()V

    .line 90
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 91
    const-string v2, "connectionError"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 92
    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/activity/l;->setArguments(Landroid/os/Bundle;)V

    .line 93
    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/google/android/gms/cast/activity/l;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/activity/l;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/l;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/l;->c()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/activity/l;)[Landroid/widget/EditText;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    return-object v0
.end method

.method private b()Ljava/lang/String;
    .locals 6

    .prologue
    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    const/4 v0, 0x4

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 104
    iget-object v3, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 105
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 106
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 107
    const-string v0, " "

    .line 109
    :cond_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 104
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 111
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/activity/l;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/l;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/activity/l;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->b:Landroid/widget/Button;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->d:Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v1, v1, v2

    invoke-virtual {v1}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 211
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/cast/activity/l;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/l;->c()V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/cast/activity/l;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->d:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method


# virtual methods
.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v1, 0x0

    .line 127
    sget v0, Lcom/google/android/gms/l;->ay:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 128
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/l;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const-string v3, "input_method"

    invoke-virtual {v0, v3}, Landroid/support/v4/app/q;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/l;->d:Landroid/view/inputmethod/InputMethodManager;

    .line 130
    sget v0, Lcom/google/android/gms/j;->dp:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/l;->b:Landroid/widget/Button;

    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/l;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "connectionError"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 132
    sget v0, Lcom/google/android/gms/j;->dr:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    if-eqz v3, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    .line 135
    sget v0, Lcom/google/android/gms/j;->lk:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 136
    new-instance v4, Lcom/google/android/gms/cast/activity/m;

    invoke-direct {v4, p0, v3}, Lcom/google/android/gms/cast/activity/m;-><init>(Lcom/google/android/gms/cast/activity/l;Z)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->b:Landroid/widget/Button;

    new-instance v3, Lcom/google/android/gms/cast/activity/n;

    invoke-direct {v3, p0}, Lcom/google/android/gms/cast/activity/n;-><init>(Lcom/google/android/gms/cast/activity/l;)V

    invoke-virtual {v0, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 158
    sget v0, Lcom/google/android/gms/j;->eo:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/cast/activity/o;

    invoke-direct {v3, p0}, Lcom/google/android/gms/cast/activity/o;-><init>(Lcom/google/android/gms/cast/activity/l;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 167
    const/4 v0, 0x4

    new-array v0, v0, [Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    iget-object v3, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    sget v0, Lcom/google/android/gms/j;->fe:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    aput-object v0, v3, v1

    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v0, v0, v1

    new-instance v3, Lcom/google/android/gms/cast/activity/q;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/cast/activity/q;-><init>(Lcom/google/android/gms/cast/activity/l;I)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v3, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    sget v0, Lcom/google/android/gms/j;->ff:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    aput-object v0, v3, v6

    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v0, v0, v6

    new-instance v3, Lcom/google/android/gms/cast/activity/q;

    invoke-direct {v3, p0, v6}, Lcom/google/android/gms/cast/activity/q;-><init>(Lcom/google/android/gms/cast/activity/l;I)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v3, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    sget v0, Lcom/google/android/gms/j;->fg:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    aput-object v0, v3, v7

    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v0, v0, v7

    new-instance v3, Lcom/google/android/gms/cast/activity/q;

    invoke-direct {v3, p0, v7}, Lcom/google/android/gms/cast/activity/q;-><init>(Lcom/google/android/gms/cast/activity/l;I)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v3, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    sget v0, Lcom/google/android/gms/j;->fh:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    aput-object v0, v3, v5

    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v0, v0, v5

    new-instance v3, Lcom/google/android/gms/cast/activity/q;

    invoke-direct {v3, p0, v5}, Lcom/google/android/gms/cast/activity/q;-><init>(Lcom/google/android/gms/cast/activity/l;I)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v0, v0, v5

    new-instance v3, Lcom/google/android/gms/cast/activity/r;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/cast/activity/r;-><init>(Lcom/google/android/gms/cast/activity/l;B)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v0, v0, v1

    new-instance v3, Lcom/google/android/gms/cast/activity/p;

    invoke-direct {v3, p0}, Lcom/google/android/gms/cast/activity/p;-><init>(Lcom/google/android/gms/cast/activity/l;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v3, v4, v5}, Landroid/widget/EditText;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 169
    if-eqz p3, :cond_2

    .line 170
    const-string v0, "enteredDigits"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 171
    const-string v0, "enteredDigits"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-ge v1, v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    array-length v3, v3

    if-ge v1, v3, :cond_2

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/cast/activity/l;->c:[Landroid/widget/EditText;

    aget-object v4, v4, v1

    invoke-virtual {v4, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 132
    :cond_1
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 174
    :cond_2
    return-object v2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/l;->b()Ljava/lang/String;

    move-result-object v0

    .line 99
    const-string v1, "enteredDigits"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 100
    return-void
.end method
