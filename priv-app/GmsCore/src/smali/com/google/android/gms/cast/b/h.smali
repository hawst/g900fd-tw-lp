.class final Lcom/google/android/gms/cast/b/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/b/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/b/e;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    const/16 v7, 0xf

    const/4 v6, 0x0

    .line 306
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 308
    iget-object v2, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->p(Lcom/google/android/gms/cast/b/e;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-void

    .line 312
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->q(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/ab;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 313
    iget-object v2, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->q(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/ab;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/cast/b/ab;->a(J)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "disconnecting due to heartbeat timeout"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->r(Lcom/google/android/gms/cast/b/e;)V

    goto :goto_0

    .line 318
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->t(Lcom/google/android/gms/cast/b/e;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v3}, Lcom/google/android/gms/cast/b/e;->s(Lcom/google/android/gms/cast/b/e;)Ljava/lang/Runnable;

    move-result-object v3

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 322
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->u(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/af;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/google/android/gms/cast/b/h;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v2}, Lcom/google/android/gms/cast/b/e;->u(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/af;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/gms/cast/b/af;->c:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v0, v1, v7}, Lcom/google/android/gms/cast/internal/p;->b(JI)Z

    iget-object v3, v2, Lcom/google/android/gms/cast/b/af;->d:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v0, v1, v7}, Lcom/google/android/gms/cast/internal/p;->b(JI)Z

    iget-object v3, v2, Lcom/google/android/gms/cast/b/af;->e:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v0, v1, v7}, Lcom/google/android/gms/cast/internal/p;->b(JI)Z

    iget-object v3, v2, Lcom/google/android/gms/cast/b/af;->f:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v0, v1, v6}, Lcom/google/android/gms/cast/internal/p;->b(JI)Z

    iget-object v2, v2, Lcom/google/android/gms/cast/b/af;->g:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v2, v0, v1, v6}, Lcom/google/android/gms/cast/internal/p;->b(JI)Z

    goto :goto_0
.end method
