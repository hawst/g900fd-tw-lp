.class public Lcom/google/android/gms/kids/account/AccountChangeReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 52
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 25
    const-string v0, "AccountChangeReceiver"

    const-string v3, "onReceive: action=%s"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 26
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 43
    :goto_0
    return-void

    .line 29
    :cond_0
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->b()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "pref_kids_account_name_"

    invoke-interface {v0, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/kids/account/b;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 30
    const-string v0, "AccountChangeReceiver"

    const-string v3, "Account removed"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 31
    new-instance v0, Lcom/google/android/gms/kids/account/a;

    invoke-direct {v0, v2}, Lcom/google/android/gms/kids/account/a;-><init>(B)V

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/service/KidsApiService;->a(Landroid/content/Context;Lcom/google/android/gms/kids/common/c/b;)V

    .line 32
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.kids.account.receiver.UpdateProfileOwnerReceiver"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 34
    const-string v2, "is_kid_account_removed"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 35
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 36
    invoke-static {v6}, Lcom/google/android/gms/kids/common/h;->a(Ljava/lang/String;)V

    .line 37
    const-class v0, Lcom/google/android/gms/kids/timeouts/TimeoutsChangedReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->b(Landroid/content/Context;Ljava/lang/Class;)V

    const-class v0, Lcom/google/android/gms/kids/timeouts/DeviceTimeAndDateChangeReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->b(Landroid/content/Context;Ljava/lang/Class;)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 29
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_1

    .line 39
    :cond_3
    const-class v0, Lcom/google/android/gms/kids/account/RetryKidsAccountSetupReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->a(Landroid/content/Context;Ljava/lang/Class;)V

    .line 40
    invoke-static {v2}, Lcom/google/android/gms/kids/common/h;->a(Z)V

    .line 41
    invoke-static {}, Lcom/google/android/gms/kids/account/KidsAccountSetupService;->a()V

    goto :goto_0
.end method
