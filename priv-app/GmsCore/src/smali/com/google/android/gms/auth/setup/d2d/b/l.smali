.class public final Lcom/google/android/gms/auth/setup/d2d/b/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/auth/setup/d2d/b/j;

.field public b:Lcom/google/android/gms/auth/setup/d2d/b/f;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Lcom/google/android/gms/auth/setup/d2d/b/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->d:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/b/q;->a()[Lcom/google/android/gms/auth/setup/d2d/b/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->cachedSize:I

    .line 40
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/auth/setup/d2d/b/l;
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/l;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/l;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/setup/d2d/b/l;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 80
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 81
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    if-eqz v1, :cond_0

    .line 82
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 85
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    if-eqz v1, :cond_1

    .line 86
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 90
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 94
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 98
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 99
    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    aget-object v2, v2, v0

    .line 100
    if-eqz v2, :cond_4

    .line 101
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 98
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 106
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/j;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/b/f;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/b/f;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/auth/setup/d2d/b/q;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lcom/google/android/gms/auth/setup/d2d/b/q;

    invoke-direct {v3}, Lcom/google/android/gms/auth/setup/d2d/b/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lcom/google/android/gms/auth/setup/d2d/b/q;

    invoke-direct {v3}, Lcom/google/android/gms/auth/setup/d2d/b/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->a:Lcom/google/android/gms/auth/setup/d2d/b/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    if-eqz v0, :cond_1

    .line 59
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->b:Lcom/google/android/gms/auth/setup/d2d/b/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 62
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 64
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 65
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 67
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 68
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/l;->e:[Lcom/google/android/gms/auth/setup/d2d/b/q;

    aget-object v1, v1, v0

    .line 70
    if-eqz v1, :cond_4

    .line 71
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 68
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 76
    return-void
.end method
