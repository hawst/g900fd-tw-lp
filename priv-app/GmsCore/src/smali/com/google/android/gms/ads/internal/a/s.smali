.class public final Lcom/google/android/gms/ads/internal/a/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field final synthetic c:Lcom/google/android/gms/ads/internal/util/j;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/google/android/gms/ads/internal/a/r;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/a/r;Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Lcom/google/android/gms/ads/internal/util/j;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/a/s;->e:Lcom/google/android/gms/ads/internal/a/r;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/a/s;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/a/s;->b:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iput-object p4, p0, Lcom/google/android/gms/ads/internal/a/s;->c:Lcom/google/android/gms/ads/internal/util/j;

    iput-object p5, p0, Lcom/google/android/gms/ads/internal/a/s;->d:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/s;->e:Lcom/google/android/gms/ads/internal/a/r;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/s;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/s;->b:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/s;->c:Lcom/google/android/gms/ads/internal/util/j;

    new-instance v4, Lcom/google/android/gms/ads/internal/a/u;

    invoke-direct {v4, v1, v2}, Lcom/google/android/gms/ads/internal/a/u;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    new-instance v1, Lcom/google/android/gms/ads/internal/a/t;

    invoke-direct {v1, v0, v3, v4}, Lcom/google/android/gms/ads/internal/a/t;-><init>(Lcom/google/android/gms/ads/internal/a/r;Lcom/google/android/gms/ads/internal/util/j;Lcom/google/android/gms/ads/internal/a/p;)V

    invoke-interface {v4, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Lcom/google/android/gms/ads/internal/a/q;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/s;->d:Ljava/lang/String;

    invoke-interface {v4, v0}, Lcom/google/android/gms/ads/internal/a/p;->b(Ljava/lang/String;)V

    .line 65
    return-void
.end method
