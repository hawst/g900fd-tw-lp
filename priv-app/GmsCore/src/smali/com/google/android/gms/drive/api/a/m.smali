.class public final Lcom/google/android/gms/drive/api/a/m;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/api/k;

.field private final f:Lcom/google/android/gms/drive/internal/CreateContentsRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/internal/CreateContentsRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x3

    invoke-direct {p0, p1, p4, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/m;->e:Lcom/google/android/gms/drive/api/k;

    .line 32
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/m;->f:Lcom/google/android/gms/drive/internal/CreateContentsRequest;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/m;->c:Lcom/google/android/gms/drive/c/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/m;->f:Lcom/google/android/gms/drive/internal/CreateContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CreateContentsRequest;->a()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/a;->a(I)Lcom/google/android/gms/drive/c/a;

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/m;->e:Lcom/google/android/gms/drive/api/k;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/m;->f:Lcom/google/android/gms/drive/internal/CreateContentsRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CreateContentsRequest;->a()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/m;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v2}, Lcom/google/android/gms/drive/internal/ca;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/gms/drive/api/k;->a(Lcom/google/android/gms/drive/auth/g;ILandroid/os/IBinder;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/m;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnContentsResponse;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/drive/internal/OnContentsResponse;-><init>(Lcom/google/android/gms/drive/Contents;Z)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnContentsResponse;)V

    .line 42
    return-void
.end method
