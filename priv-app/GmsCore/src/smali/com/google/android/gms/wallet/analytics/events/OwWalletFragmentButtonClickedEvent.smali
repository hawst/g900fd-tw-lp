.class public Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field public final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/s;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/s;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->d:I

    .line 40
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 41
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 30
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->j:Ljava/lang/String;

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->a:Ljava/lang/String;

    .line 32
    iput p3, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->d:I

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 34
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    .line 35
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;I)V

    .line 47
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 48
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 59
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 61
    return-void
.end method
