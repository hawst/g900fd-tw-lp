.class abstract Lcom/google/android/gms/people/service/a/c;
.super Lcom/google/android/gms/people/service/a;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/people/internal/f;

.field private final h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3, p6}, Lcom/google/android/gms/people/service/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;)V

    .line 26
    iput-object p4, p0, Lcom/google/android/gms/people/service/a/c;->g:Lcom/google/android/gms/people/internal/f;

    .line 27
    iput-boolean p5, p0, Lcom/google/android/gms/people/service/a/c;->h:Z

    .line 28
    return-void
.end method


# virtual methods
.method public abstract a(Landroid/content/Context;)[Lcom/google/android/gms/common/data/DataHolder;
.end method

.method public final e()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 32
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a;->e:Z

    if-eqz v0, :cond_0

    .line 75
    :goto_0
    return-void

    .line 39
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/c;->a:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/service/a/c;->a(Landroid/content/Context;)[Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 40
    sget-object v0, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    .line 42
    if-eqz v2, :cond_1

    .line 43
    array-length v4, v2

    :goto_1
    if-ge v3, v4, :cond_1

    aget-object v5, v2, v3

    .line 44
    invoke-virtual {p0, v5}, Lcom/google/android/gms/people/service/a/c;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    .line 43
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 48
    :catch_0
    move-exception v0

    .line 49
    const-string v2, "PeopleService"

    const-string v3, "Error during operation"

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 52
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/c;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/RuntimeException;Ljava/lang/String;)V

    .line 56
    sget-object v0, Lcom/google/android/gms/people/service/b;->h:Lcom/google/android/gms/people/service/b;

    move-object v2, v1

    .line 64
    :cond_1
    :goto_2
    :try_start_1
    iget-boolean v3, p0, Lcom/google/android/gms/people/service/a/c;->h:Z

    if-eqz v3, :cond_2

    .line 65
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/c;->g:Lcom/google/android/gms/people/internal/f;

    iget v3, v0, Lcom/google/android/gms/people/service/b;->a:I

    iget-object v0, v0, Lcom/google/android/gms/people/service/b;->b:Landroid/os/Bundle;

    invoke-interface {v1, v3, v0, v2}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;[Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 74
    :catch_1
    move-exception v0

    goto :goto_0

    .line 58
    :catch_2
    move-exception v0

    .line 59
    const-string v2, "PeopleService"

    const-string v3, "Error during operation"

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    .line 61
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/c;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/c;->c()Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v0}, Lcom/google/android/gms/people/f/n;->a(Landroid/content/Context;Ljava/lang/Exception;)Lcom/google/android/gms/people/service/b;

    move-result-object v0

    move-object v2, v1

    goto :goto_2

    .line 67
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/c;->g:Lcom/google/android/gms/people/internal/f;

    iget v4, v0, Lcom/google/android/gms/people/service/b;->a:I

    iget-object v5, v0, Lcom/google/android/gms/people/service/b;->b:Landroid/os/Bundle;

    if-eqz v2, :cond_3

    const/4 v0, 0x0

    aget-object v0, v2, v0

    :goto_3
    invoke-interface {v3, v4, v5, v0}, Lcom/google/android/gms/people/internal/f;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_2
    .catch Landroid/os/DeadObjectException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_3

    goto :goto_0

    .line 72
    :catch_3
    move-exception v0

    .line 73
    const-string v1, "PeopleService"

    const-string v2, "Unknown error"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 67
    goto :goto_3
.end method
