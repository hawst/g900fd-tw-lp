.class final Lcom/google/android/gms/cast/c/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/b/t;


# instance fields
.field final a:Lcom/google/android/gms/cast/e/h;

.field final b:Lcom/google/android/gms/cast/c/j;

.field final c:Lcom/google/android/gms/cast/c/d;

.field final synthetic d:Lcom/google/android/gms/cast/c/h;

.field private final e:Lcom/google/android/gms/cast/b/o;

.field private final f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:I

.field private i:J

.field private j:Z

.field private final k:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/c/h;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 192
    iput-object p1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 158
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "DeviceFilter"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    .line 162
    new-instance v0, Lcom/google/android/gms/cast/c/j;

    invoke-direct {v0, v6}, Lcom/google/android/gms/cast/c/j;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/n;->b:Lcom/google/android/gms/cast/c/j;

    .line 164
    iput v6, p0, Lcom/google/android/gms/cast/c/n;->h:I

    .line 165
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/c/n;->i:J

    .line 167
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    .line 168
    new-instance v0, Lcom/google/android/gms/cast/c/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/c/o;-><init>(Lcom/google/android/gms/cast/c/n;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/n;->c:Lcom/google/android/gms/cast/c/d;

    .line 193
    invoke-static {}, Lcom/google/android/gms/cast/c/h;->a()I

    move-result v7

    .line 194
    const-string v0, "MiniDeviceController-%d"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;)V

    .line 196
    new-instance v0, Lcom/google/android/gms/cast/b/o;

    invoke-static {p1}, Lcom/google/android/gms/cast/c/h;->c(Lcom/google/android/gms/cast/c/h;)Landroid/content/Context;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v3

    const/16 v5, 0x4000

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/cast/b/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/cast/b/t;Landroid/os/Handler;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/n;->e:Lcom/google/android/gms/cast/b/o;

    .line 198
    const-string v0, "%s-%d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/gms/cast/c/h;->d(Lcom/google/android/gms/cast/c/h;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/n;->f:Ljava/lang/String;

    .line 199
    return-void
.end method

.method private a(Lorg/json/JSONObject;)Lcom/google/android/gms/cast/b/b;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 426
    :try_start_0
    const-string v0, "status"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 427
    const-string v1, "applications"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 428
    const-string v1, "applications"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 429
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 431
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 433
    :try_start_1
    new-instance v0, Lcom/google/android/gms/cast/b/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/b/b;-><init>(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 444
    :goto_0
    return-object v0

    .line 435
    :catch_0
    move-exception v0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Error extracting the application info."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 444
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 441
    :catch_1
    move-exception v0

    .line 442
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "No namespaces found in receiver response: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 388
    invoke-static {}, Lcom/google/android/gms/cast/c/h;->b()[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 389
    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 390
    const/4 v0, 0x1

    .line 393
    :cond_0
    return v0

    .line 388
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 397
    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v4, "Sending text message to %s: (ns=%s, dest=%s) %s"

    const/4 v0, 0x4

    new-array v5, v0, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v2

    aput-object p1, v5, v1

    const/4 v0, 0x2

    const-string v6, "receiver-0"

    aput-object v6, v5, v0

    const/4 v0, 0x3

    aput-object p2, v5, v0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    new-instance v0, Lcom/google/g/a/g;

    invoke-direct {v0}, Lcom/google/g/a/g;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/g/a/g;->a(I)Lcom/google/g/a/g;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/g/a/g;->a(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    const-string v3, "receiver-0"

    invoke-virtual {v0, v3}, Lcom/google/g/a/g;->b(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/g/a/g;->c(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/g/a/g;->b(I)Lcom/google/g/a/g;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/g/a/g;->d(Ljava/lang/String;)Lcom/google/g/a/g;

    move-result-object v0

    .line 408
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->e:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/b/o;->a(Lcom/google/g/a/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    move v0, v1

    .line 419
    :goto_0
    return v0

    .line 410
    :catch_0
    move-exception v0

    .line 411
    invoke-virtual {p0, v2}, Lcom/google/android/gms/cast/c/n;->a(Z)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    .line 412
    if-eqz v1, :cond_0

    .line 413
    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v3}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/cast/c/k;

    iget-object v5, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "sendMessage failed: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v1, v0}, Lcom/google/android/gms/cast/c/k;-><init>(Lcom/google/android/gms/cast/c/h;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    move v0, v2

    .line 416
    goto :goto_0

    .line 417
    :catch_1
    move-exception v0

    .line 418
    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v4, "Unable to send the text message: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {v3, v4, v1}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v2

    .line 419
    goto :goto_0
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/gms/cast/CastDevice;
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/CastDevice;

    .line 249
    if-eqz v0, :cond_0

    .line 250
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "DEACTIVATE; finished=%b; socket status=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lcom/google/android/gms/cast/c/n;->e:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/b/o;->h()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 253
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->a(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/c/a;->a()V

    .line 254
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->a(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->c:Lcom/google/android/gms/cast/c/d;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/c/a;->b(Lcom/google/android/gms/cast/c/d;)V

    .line 256
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->e:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->h()I

    move-result v1

    .line 257
    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "got socket state: %d"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    packed-switch v1, :pswitch_data_0

    .line 270
    :cond_0
    :goto_0
    return-object v0

    .line 262
    :pswitch_0
    iput-boolean p1, p0, Lcom/google/android/gms/cast/c/n;->j:Z

    .line 263
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->e:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/o;->c()V

    goto :goto_0

    .line 268
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/cast/c/i;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-direct {v2, v3, v6}, Lcom/google/android/gms/cast/c/i;-><init>(Lcom/google/android/gms/cast/c/h;B)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 259
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method final a()V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 448
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "finish()"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 449
    iget-object v5, p0, Lcom/google/android/gms/cast/c/n;->g:Ljava/lang/String;

    .line 451
    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/c/n;->a(Z)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v3

    .line 452
    if-nez v3, :cond_0

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "filter wasn\'t active, so discarding filter results"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 468
    :goto_0
    return-void

    .line 458
    :cond_0
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 459
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->g(Lcom/google/android/gms/cast/c/h;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aa;

    .line 460
    iget-object v6, p0, Lcom/google/android/gms/cast/c/n;->b:Lcom/google/android/gms/cast/c/j;

    iget-object v8, v0, Lcom/google/android/gms/cast/c/aa;->b:Ljava/lang/String;

    if-eqz v8, :cond_2

    iget-object v9, v6, Lcom/google/android/gms/cast/c/j;->b:Ljava/util/Set;

    invoke-interface {v9, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    :cond_2
    iget-object v6, v6, Lcom/google/android/gms/cast/c/j;->a:Ljava/util/Set;

    iget-object v8, v0, Lcom/google/android/gms/cast/c/aa;->c:Ljava/util/Set;

    invoke-static {v8}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v8

    invoke-interface {v6, v8}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v6

    if-eqz v6, :cond_3

    move v6, v1

    :goto_2
    if-eqz v6, :cond_1

    .line 461
    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move v6, v2

    .line 460
    goto :goto_2

    .line 465
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->g(Lcom/google/android/gms/cast/c/h;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    :cond_5
    move v2, v1

    .line 466
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v6

    new-instance v0, Lcom/google/android/gms/cast/c/l;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/cast/c/l;-><init>(Lcom/google/android/gms/cast/c/h;ZLcom/google/android/gms/cast/CastDevice;Ljava/util/Set;Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(I)V
    .locals 6

    .prologue
    .line 317
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/n;->a(Z)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v0

    .line 318
    if-eqz v0, :cond_0

    .line 319
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/cast/c/k;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "connection failed: error="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/gms/cast/c/k;-><init>(Lcom/google/android/gms/cast/c/h;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 322
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/g/a/g;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 326
    iget v0, p1, Lcom/google/g/a/g;->b:I

    if-eqz v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 330
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onMessageReceived from %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->f:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v3, p1, Lcom/google/g/a/g;->c:Ljava/lang/String;

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 333
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    iget-object v1, p1, Lcom/google/g/a/g;->c:Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 334
    const-string v1, "requestId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 338
    const-string v1, "requestId"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 339
    const-wide/16 v4, 0x1

    cmp-long v1, v2, v4

    if-nez v1, :cond_3

    .line 340
    iget v1, p0, Lcom/google/android/gms/cast/c/n;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/gms/cast/c/n;->h:I

    .line 341
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->b:Lcom/google/android/gms/cast/c/j;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/c/j;->a(Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :cond_2
    :goto_1
    iget v0, p0, Lcom/google/android/gms/cast/c/n;->h:I

    if-nez v0, :cond_0

    .line 359
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/n;->a()V

    goto :goto_0

    .line 342
    :cond_3
    const-wide/16 v4, 0x2

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    .line 343
    :try_start_1
    iget v1, p0, Lcom/google/android/gms/cast/c/n;->h:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/gms/cast/c/n;->h:I

    .line 344
    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/n;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/cast/b/b;

    move-result-object v0

    .line 345
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/cast/c/n;->g:Ljava/lang/String;

    .line 346
    if-eqz v0, :cond_2

    .line 347
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->b:Lcom/google/android/gms/cast/c/j;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/b;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/c/j;->a(Ljava/util/List;)V

    .line 348
    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/b;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/cast/c/n;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 349
    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/b;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/n;->g:Ljava/lang/String;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 355
    :catch_0
    move-exception v0

    .line 356
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Failed to parse response: %s"

    new-array v3, v7, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 353
    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Unrecognized request ID: %d"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 208
    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->e:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/o;->g()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, p1}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 232
    :goto_0
    return v0

    .line 211
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "ACTIVATE"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 212
    iget-wide v2, p0, Lcom/google/android/gms/cast/c/n;->i:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/cast/c/n;->i:J

    .line 214
    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v2}, Lcom/google/android/gms/cast/c/h;->e(Lcom/google/android/gms/cast/c/h;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v2}, Lcom/google/android/gms/cast/c/h;->f(Lcom/google/android/gms/cast/c/h;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 215
    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "No filtering criteria. Automatically accepting: %s"

    new-array v4, v0, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/n;->a()V

    goto :goto_0

    .line 217
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v2

    if-nez v2, :cond_3

    .line 218
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->a(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->c:Lcom/google/android/gms/cast/c/d;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/c/a;->a(Lcom/google/android/gms/cast/c/d;)V

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->a(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v2}, Lcom/google/android/gms/cast/c/h;->g(Lcom/google/android/gms/cast/c/h;)Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/cast/c/a;->a(Ljava/util/Set;)V

    goto :goto_0

    .line 222
    :cond_3
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "connecting to: %s:%d (%s)"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v6

    invoke-virtual {v6}, Ljava/net/Inet4Address;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 224
    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->e:Lcom/google/android/gms/cast/b/o;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/b/o;->a(Ljava/net/Inet4Address;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 225
    :catch_0
    move-exception v2

    .line 226
    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v4, "Exception while connecting socket"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/c/n;->a(Z)Lcom/google/android/gms/cast/CastDevice;

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v1

    new-instance v3, Lcom/google/android/gms/cast/c/k;

    iget-object v4, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-virtual {v2}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v4, p1, v2}, Lcom/google/android/gms/cast/c/k;-><init>(Lcom/google/android/gms/cast/c/h;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0
.end method

.method public final c(I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onDisconnected"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->b:Lcom/google/android/gms/cast/c/j;

    iget-object v1, v0, Lcom/google/android/gms/cast/c/j;->a:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->clear()V

    iget-object v0, v0, Lcom/google/android/gms/cast/c/j;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 367
    iput-object v6, p0, Lcom/google/android/gms/cast/c/n;->g:Ljava/lang/String;

    .line 368
    iput v5, p0, Lcom/google/android/gms/cast/c/n;->h:I

    .line 369
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/n;->j:Z

    .line 370
    iput-boolean v5, p0, Lcom/google/android/gms/cast/c/n;->j:Z

    .line 372
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Device filter disconnected; error=%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 373
    if-nez v0, :cond_1

    .line 375
    invoke-virtual {p0, v5}, Lcom/google/android/gms/cast/c/n;->a(Z)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v0

    .line 376
    if-eqz v0, :cond_0

    .line 377
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v1}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/cast/c/k;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "socket disconnected; error="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/gms/cast/c/k;-><init>(Lcom/google/android/gms/cast/c/h;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 385
    :cond_0
    :goto_0
    return-void

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/cast/c/i;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-direct {v1, v2, v5}, Lcom/google/android/gms/cast/c/i;-><init>(Lcom/google/android/gms/cast/c/h;B)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final m()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 284
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "onConnected to %s"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Inet4Address;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    sget-object v0, Lcom/google/android/gms/cast/internal/j;->b:Ljava/lang/String;

    const-string v1, "{\"type\":\"CONNECT\",\"package\":\"%s\",\"origin\":{}}"

    new-array v2, v4, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v3}, Lcom/google/android/gms/cast/c/h;->d(Lcom/google/android/gms/cast/c/h;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/c/n;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 293
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->e(Lcom/google/android/gms/cast/c/h;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 294
    sget-object v0, Lcom/google/android/gms/cast/internal/j;->a:Ljava/lang/String;

    const-string v1, "{\"type\":\"GET_APP_AVAILABILITY\",\"appId\":[%s],\"requestId\":%d}"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v3}, Lcom/google/android/gms/cast/c/h;->e(Lcom/google/android/gms/cast/c/h;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/c/n;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 297
    iget v0, p0, Lcom/google/android/gms/cast/c/n;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/cast/c/n;->h:I

    .line 302
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->d:Lcom/google/android/gms/cast/c/h;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/h;->f(Lcom/google/android/gms/cast/c/h;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->k()Z

    move-result v0

    if-nez v0, :cond_4

    .line 303
    :cond_3
    sget-object v0, Lcom/google/android/gms/cast/internal/j;->a:Ljava/lang/String;

    const-string v1, "{\"type\":\"GET_STATUS\",\"requestId\":%d}"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/c/n;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 305
    iget v0, p0, Lcom/google/android/gms/cast/c/n;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/cast/c/n;->h:I

    .line 309
    :cond_4
    iget v0, p0, Lcom/google/android/gms/cast/c/n;->h:I

    if-nez v0, :cond_0

    .line 310
    iget-object v1, p0, Lcom/google/android/gms/cast/c/n;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Nothing to filter: %s"

    new-array v3, v4, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/cast/c/n;->k:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 311
    invoke-virtual {p0, v4}, Lcom/google/android/gms/cast/c/n;->a(Z)Lcom/google/android/gms/cast/CastDevice;

    goto/16 :goto_0
.end method
