.class public final Lcom/google/android/gms/drive/database/model/br;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:J

.field public final c:Ljava/lang/String;

.field public d:Z

.field public g:Z

.field public h:I

.field private final i:Lcom/google/android/gms/drive/database/model/EntrySpec;

.field private j:J

.field private k:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 14

    .prologue
    .line 49
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-wide/from16 v4, p4

    move-wide/from16 v6, p6

    move-object/from16 v8, p8

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/drive/database/model/br;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;JJLjava/lang/String;IZZI)V

    .line 51
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;JJLjava/lang/String;IZZI)V
    .locals 2

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->a()Lcom/google/android/gms/drive/database/model/bt;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/br;->i:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 66
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/br;->a:Ljava/lang/String;

    .line 67
    iput-wide p4, p0, Lcom/google/android/gms/drive/database/model/br;->b:J

    .line 68
    iput-wide p6, p0, Lcom/google/android/gms/drive/database/model/br;->j:J

    .line 69
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/br;->c:Ljava/lang/String;

    .line 70
    iput p9, p0, Lcom/google/android/gms/drive/database/model/br;->k:I

    .line 71
    iput-boolean p10, p0, Lcom/google/android/gms/drive/database/model/br;->d:Z

    .line 72
    iput-boolean p11, p0, Lcom/google/android/gms/drive/database/model/br;->g:Z

    .line 73
    iput p12, p0, Lcom/google/android/gms/drive/database/model/br;->h:I

    .line 74
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/br;
    .locals 13

    .prologue
    .line 183
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->e:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 186
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->d:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 189
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 190
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    .line 192
    :goto_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->f:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 194
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->g:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v9

    .line 196
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->h:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    .line 198
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->i:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v10

    .line 201
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->j:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v11

    .line 202
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->k:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->intValue()I

    move-result v12

    .line 203
    new-instance v0, Lcom/google/android/gms/drive/database/model/br;

    move-object v1, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/drive/database/model/br;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;JJLjava/lang/String;IZZI)V

    .line 208
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->a()Lcom/google/android/gms/drive/database/model/bt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bt;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/br;->d(J)V

    .line 210
    return-object v0

    .line 190
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/DriveId;
    .locals 6

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/br;->i:Lcom/google/android/gms/drive/database/model/EntrySpec;

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Lcom/google/android/gms/drive/DriveId;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/br;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/model/br;->i:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v2

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/br;->e:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/i;->g()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/DriveId;-><init>(Ljava/lang/String;JJ)V

    .line 81
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/br;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 154
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->e:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/br;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/br;->i:Lcom/google/android/gms/drive/database/model/EntrySpec;

    if-eqz v0, :cond_0

    .line 157
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/br;->i:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 163
    :goto_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->d:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/br;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->f:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/br;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 168
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->g:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget v3, p0, Lcom/google/android/gms/drive/database/model/br;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 169
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->h:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/drive/database/model/br;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->i:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    iget-boolean v0, p0, Lcom/google/android/gms/drive/database/model/br;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 175
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->j:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/google/android/gms/drive/database/model/br;->g:Z

    if-eqz v3, :cond_2

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 176
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->k:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/database/model/br;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 177
    return-void

    .line 161
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_1
    move v0, v2

    .line 172
    goto :goto_1

    :cond_2
    move v1, v2

    .line 175
    goto :goto_2
.end method
