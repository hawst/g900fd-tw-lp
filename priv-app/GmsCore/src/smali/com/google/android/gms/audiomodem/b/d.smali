.class final Lcom/google/android/gms/audiomodem/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/IBinder;

.field final synthetic b:Lcom/google/android/gms/audiomodem/a/n;

.field final synthetic c:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

.field final synthetic d:Lcom/google/android/gms/audiomodem/a/e;

.field final synthetic e:Lcom/google/android/gms/audiomodem/b/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/audiomodem/b/a;Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/b/d;->e:Lcom/google/android/gms/audiomodem/b/a;

    iput-object p2, p0, Lcom/google/android/gms/audiomodem/b/d;->a:Landroid/os/IBinder;

    iput-object p3, p0, Lcom/google/android/gms/audiomodem/b/d;->b:Lcom/google/android/gms/audiomodem/a/n;

    iput-object p4, p0, Lcom/google/android/gms/audiomodem/b/d;->c:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iput-object p5, p0, Lcom/google/android/gms/audiomodem/b/d;->d:Lcom/google/android/gms/audiomodem/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/d;->e:Lcom/google/android/gms/audiomodem/b/a;

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/b/a;->a(Lcom/google/android/gms/audiomodem/b/a;)Lcom/google/android/gms/audiomodem/n;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/d;->a:Landroid/os/IBinder;

    iget-object v3, p0, Lcom/google/android/gms/audiomodem/b/d;->b:Lcom/google/android/gms/audiomodem/a/n;

    iget-object v4, p0, Lcom/google/android/gms/audiomodem/b/d;->c:Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;

    iget-object v5, p0, Lcom/google/android/gms/audiomodem/b/d;->d:Lcom/google/android/gms/audiomodem/a/e;

    invoke-virtual {v2}, Lcom/google/android/gms/audiomodem/n;->b()Z

    move-result v6

    if-nez v6, :cond_1

    const/16 v0, 0x1965

    invoke-static {v5, v0}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    .line 85
    :cond_0
    :goto_0
    return-void

    .line 84
    :cond_1
    new-instance v6, Lcom/google/android/gms/audiomodem/p;

    invoke-direct {v6, v3}, Lcom/google/android/gms/audiomodem/p;-><init>(Lcom/google/android/gms/audiomodem/a/n;)V

    iget-object v3, v2, Lcom/google/android/gms/audiomodem/n;->a:Lcom/google/android/gms/audiomodem/a;

    invoke-virtual {v3, v6, v0}, Lcom/google/android/gms/audiomodem/a;->a(Lcom/google/android/gms/audiomodem/aq;Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/c;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->d()[Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v7

    array-length v8, v7

    move v0, v1

    :goto_1
    if-ge v0, v8, :cond_2

    aget-object v9, v7, v0

    new-instance v10, Lcom/google/android/gms/audiomodem/bl;

    invoke-virtual {v4}, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->c()I

    move-result v11

    invoke-virtual {v4}, Lcom/google/android/gms/audiomodem/TokenBroadcaster$Params;->b()[B

    move-result-object v12

    invoke-direct {v10, v6, v9, v11, v12}, Lcom/google/android/gms/audiomodem/bl;-><init>(Lcom/google/android/gms/audiomodem/p;Lcom/google/android/gms/audiomodem/Encoding;I[B)V

    invoke-interface {v3, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/p;Ljava/util/Set;)V

    invoke-static {v5, v1}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    goto :goto_0
.end method
