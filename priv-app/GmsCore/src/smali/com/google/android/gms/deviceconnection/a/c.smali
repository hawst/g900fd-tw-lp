.class public final Lcom/google/android/gms/deviceconnection/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/input/InputManager$InputDeviceListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final a:[I

.field private static final b:[I


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/deviceconnection/a/a;

.field private final e:Landroid/hardware/input/InputManager;

.field private final f:Lcom/google/android/gms/deviceconnection/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/deviceconnection/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/deviceconnection/b/a;->a(Ljava/lang/String;)[I

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/deviceconnection/a/c;->a:[I

    .line 37
    sget-object v0, Lcom/google/android/gms/deviceconnection/b/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/deviceconnection/b/a;->a(Ljava/lang/String;)[I

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/deviceconnection/a/c;->b:[I

    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/deviceconnection/a/a;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/deviceconnection/a/c;->d:Lcom/google/android/gms/deviceconnection/a/a;

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/deviceconnection/a/c;->c:Landroid/content/Context;

    .line 54
    const-string v0, "input"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/input/InputManager;

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->e:Landroid/hardware/input/InputManager;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->e:Landroid/hardware/input/InputManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->e:Landroid/hardware/input/InputManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Landroid/hardware/input/InputManager;->registerInputDeviceListener(Landroid/hardware/input/InputManager$InputDeviceListener;Landroid/os/Handler;)V

    .line 62
    new-instance v0, Lcom/google/android/gms/deviceconnection/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/deviceconnection/a/d;-><init>(Lcom/google/android/gms/deviceconnection/a/c;)V

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->f:Lcom/google/android/gms/deviceconnection/a/d;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->f:Lcom/google/android/gms/deviceconnection/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/deviceconnection/a/d;->run()V

    .line 64
    return-void
.end method

.method private static varargs a(Landroid/view/InputDevice;[I)I
    .locals 3

    .prologue
    .line 260
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/view/InputDevice;->hasKeys([I)[Z

    move-result-object v0

    .line 261
    :goto_0
    const/4 v1, 0x0

    :goto_1
    array-length v2, v0

    if-ge v1, v2, :cond_2

    .line 262
    aget-boolean v2, v0, v1

    if-nez v2, :cond_1

    .line 266
    :goto_2
    return v1

    .line 260
    :cond_0
    invoke-virtual {p0}, Landroid/view/InputDevice;->getKeyCharacterMap()Landroid/view/KeyCharacterMap;

    invoke-static {p1}, Landroid/view/KeyCharacterMap;->deviceHasKeys([I)[Z

    move-result-object v0

    goto :goto_0

    .line 261
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 266
    :cond_2
    array-length v1, p1

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/deviceconnection/a/c;)Lcom/google/android/gms/deviceconnection/a/a;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->d:Lcom/google/android/gms/deviceconnection/a/a;

    return-object v0
.end method

.method private static a(Landroid/view/InputDevice;I)Z
    .locals 1

    .prologue
    .line 248
    invoke-virtual {p0}, Landroid/view/InputDevice;->getSources()I

    move-result v0

    and-int/2addr v0, p1

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/view/InputDevice;)[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const v5, 0x1000010

    const/4 v0, 0x0

    .line 67
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/gms/deviceconnection/a/c;->b(Landroid/view/InputDevice;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    const-string v2, "android.hardware.dpad"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 71
    :cond_0
    const v2, 0x10004

    invoke-static {p1, v2}, Lcom/google/android/gms/deviceconnection/a/c;->a(Landroid/view/InputDevice;I)Z

    move-result v2

    if-eqz v2, :cond_5

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    .line 72
    const-string v2, "android.hardware.trackball"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    :cond_1
    const v2, 0x100008

    invoke-static {p1, v2}, Lcom/google/android/gms/deviceconnection/a/c;->a(Landroid/view/InputDevice;I)Z

    move-result v2

    if-eqz v2, :cond_6

    move v2, v1

    :goto_1
    if-eqz v2, :cond_2

    .line 75
    const-string v2, "android.hardware.touchpad"

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/deviceconnection/a/c;->b(Landroid/view/InputDevice;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_3
    :goto_2
    if-eqz v0, :cond_4

    .line 78
    const-string v0, "android.hardware.gamepad"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0

    :cond_5
    move v2, v0

    .line 71
    goto :goto_0

    :cond_6
    move v2, v0

    .line 74
    goto :goto_1

    .line 77
    :cond_7
    sget-object v2, Lcom/google/android/gms/deviceconnection/a/c;->b:[I

    invoke-static {p1, v2}, Lcom/google/android/gms/deviceconnection/a/c;->a(Landroid/view/InputDevice;[I)I

    move-result v2

    sget-object v4, Lcom/google/android/gms/deviceconnection/a/c;->b:[I

    array-length v4, v4

    if-lt v2, v4, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/deviceconnection/a/c;->c(Landroid/view/InputDevice;)Landroid/util/SparseIntArray;

    move-result-object v2

    const/16 v4, 0x16

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    if-eq v4, v5, :cond_8

    const/16 v4, 0x12

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    if-ne v4, v5, :cond_3

    :cond_8
    const/16 v4, 0x17

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    if-eq v4, v5, :cond_9

    const/16 v4, 0x11

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    if-ne v4, v5, :cond_3

    :cond_9
    invoke-virtual {v2, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    if-ne v4, v5, :cond_3

    invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    if-ne v4, v5, :cond_3

    const/16 v4, 0xb

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v4

    if-ne v4, v5, :cond_3

    const/16 v4, 0xe

    invoke-virtual {v2, v4}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    if-ne v2, v5, :cond_3

    move v0, v1

    goto :goto_2
.end method

.method private b(Landroid/view/InputDevice;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 145
    sget-object v2, Lcom/google/android/gms/deviceconnection/a/c;->a:[I

    invoke-static {p1, v2}, Lcom/google/android/gms/deviceconnection/a/c;->a(Landroid/view/InputDevice;[I)I

    move-result v2

    sget-object v3, Lcom/google/android/gms/deviceconnection/a/c;->a:[I

    array-length v3, v3

    if-ne v2, v3, :cond_1

    .line 163
    :cond_0
    :goto_0
    return v0

    .line 150
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/deviceconnection/a/c;->c(Landroid/view/InputDevice;)Landroid/util/SparseIntArray;

    move-result-object v2

    .line 153
    const/16 v3, 0xf

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v3

    if-ltz v3, :cond_2

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v2

    if-ltz v2, :cond_2

    move v2, v0

    .line 157
    :goto_1
    if-nez v2, :cond_0

    move v0, v1

    .line 163
    goto :goto_0

    :cond_2
    move v2, v1

    .line 153
    goto :goto_1
.end method

.method private static c(Landroid/view/InputDevice;)Landroid/util/SparseIntArray;
    .locals 6

    .prologue
    .line 238
    invoke-virtual {p0}, Landroid/view/InputDevice;->getMotionRanges()Ljava/util/List;

    move-result-object v2

    .line 239
    new-instance v3, Landroid/util/SparseIntArray;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 240
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 241
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/InputDevice$MotionRange;

    .line 242
    invoke-virtual {v0}, Landroid/view/InputDevice$MotionRange;->getAxis()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/InputDevice$MotionRange;->getSource()I

    move-result v0

    invoke-virtual {v3, v5, v0}, Landroid/util/SparseIntArray;->put(II)V

    .line 240
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 244
    :cond_0
    return-object v3
.end method


# virtual methods
.method public final a()[Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->e:Landroid/hardware/input/InputManager;

    invoke-virtual {v0}, Landroid/hardware/input/InputManager;->getInputDeviceIds()[I

    move-result-object v3

    .line 115
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    move v0, v1

    .line 117
    :goto_0
    array-length v2, v3

    if-ge v0, v2, :cond_1

    .line 118
    iget-object v2, p0, Lcom/google/android/gms/deviceconnection/a/c;->e:Landroid/hardware/input/InputManager;

    aget v5, v3, v0

    invoke-virtual {v2, v5}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/deviceconnection/a/c;->a(Landroid/view/InputDevice;)[Ljava/lang/String;

    move-result-object v5

    move v2, v1

    .line 119
    :goto_1
    array-length v6, v5

    if-ge v2, v6, :cond_0

    .line 120
    aget-object v6, v5, v2

    invoke-virtual {v4, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 119
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 117
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_1
    new-array v0, v1, [Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public final onInputDeviceAdded(I)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->e:Landroid/hardware/input/InputManager;

    invoke-virtual {v0, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/a/c;->d:Lcom/google/android/gms/deviceconnection/a/a;

    invoke-direct {p0, v0}, Lcom/google/android/gms/deviceconnection/a/c;->a(Landroid/view/InputDevice;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/deviceconnection/a/a;->a([Ljava/lang/String;)I

    .line 89
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.deviceconnection.input_device_connected"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 90
    const-string v1, "input_device_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/a/c;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 92
    return-void
.end method

.method public final onInputDeviceChanged(I)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/a/c;->e:Landroid/hardware/input/InputManager;

    invoke-virtual {v0, p1}, Landroid/hardware/input/InputManager;->getInputDevice(I)Landroid/view/InputDevice;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/a/c;->d:Lcom/google/android/gms/deviceconnection/a/a;

    invoke-direct {p0, v0}, Lcom/google/android/gms/deviceconnection/a/c;->a(Landroid/view/InputDevice;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/deviceconnection/a/a;->a([Ljava/lang/String;)I

    .line 109
    return-void
.end method

.method public final onInputDeviceRemoved(I)V
    .locals 2

    .prologue
    .line 97
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.deviceconnection.input_device_disconnected"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 98
    const-string v1, "input_device_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/a/c;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 100
    return-void
.end method
