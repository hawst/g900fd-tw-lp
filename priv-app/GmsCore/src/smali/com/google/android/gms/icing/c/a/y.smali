.class public final Lcom/google/android/gms/icing/c/a/y;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile i:[Lcom/google/android/gms/icing/c/a/y;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/android/gms/icing/c/a/h;

.field public d:[Lcom/google/android/gms/icing/c/a/aa;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:[Lcom/google/android/gms/icing/c/a/z;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3407
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3408
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/h;->a()[Lcom/google/android/gms/icing/c/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/aa;->a()[Lcom/google/android/gms/icing/c/a/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/z;->a()[Lcom/google/android/gms/icing/c/a/z;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/y;->cachedSize:I

    .line 3409
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/y;
    .locals 2

    .prologue
    .line 3372
    sget-object v0, Lcom/google/android/gms/icing/c/a/y;->i:[Lcom/google/android/gms/icing/c/a/y;

    if-nez v0, :cond_1

    .line 3373
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3375
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/y;->i:[Lcom/google/android/gms/icing/c/a/y;

    if-nez v0, :cond_0

    .line 3376
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/y;

    sput-object v0, Lcom/google/android/gms/icing/c/a/y;->i:[Lcom/google/android/gms/icing/c/a/y;

    .line 3378
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3380
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/y;->i:[Lcom/google/android/gms/icing/c/a/y;

    return-object v0

    .line 3378
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3554
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3555
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3556
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3559
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3560
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3563
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 3564
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 3565
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    aget-object v3, v3, v0

    .line 3566
    if-eqz v3, :cond_2

    .line 3567
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3564
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 3572
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    array-length v2, v2

    if-lez v2, :cond_7

    move v2, v0

    move v0, v1

    .line 3573
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    array-length v3, v3

    if-ge v0, v3, :cond_6

    .line 3574
    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    aget-object v3, v3, v0

    .line 3575
    if-eqz v3, :cond_5

    .line 3576
    const/4 v4, 0x4

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3573
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_6
    move v0, v2

    .line 3581
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    .line 3582
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3585
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 3586
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3589
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 3590
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3593
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 3594
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 3595
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    aget-object v2, v2, v1

    .line 3596
    if-eqz v2, :cond_b

    .line 3597
    const/16 v3, 0x8

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3594
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3602
    :cond_c
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3427
    if-ne p1, p0, :cond_1

    .line 3428
    const/4 v0, 0x1

    .line 3481
    :cond_0
    :goto_0
    return v0

    .line 3430
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/y;

    if-eqz v1, :cond_0

    .line 3433
    check-cast p1, Lcom/google/android/gms/icing/c/a/y;

    .line 3434
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 3435
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3441
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 3442
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3448
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3452
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3456
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 3457
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3463
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 3464
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3470
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 3471
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3477
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3481
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/y;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3438
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3445
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 3460
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 3467
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 3474
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3486
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3489
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3491
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3493
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3495
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3497
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3499
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 3501
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3503
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/y;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3504
    return v0

    .line 3486
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3489
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3495
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3497
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3499
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3092
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/y;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/h;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/c/a/h;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/c/a/h;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/aa;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/icing/c/a/aa;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/aa;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/icing/c/a/aa;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/aa;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/z;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/android/gms/icing/c/a/z;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/z;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/android/gms/icing/c/a/z;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/z;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3510
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3511
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3513
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3514
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3516
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 3517
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 3518
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->c:[Lcom/google/android/gms/icing/c/a/h;

    aget-object v2, v2, v0

    .line 3519
    if-eqz v2, :cond_2

    .line 3520
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3517
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3524
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 3525
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 3526
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->d:[Lcom/google/android/gms/icing/c/a/aa;

    aget-object v2, v2, v0

    .line 3527
    if-eqz v2, :cond_4

    .line 3528
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3525
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3532
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 3533
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3535
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 3536
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3538
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 3539
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/y;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3541
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 3542
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 3543
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/y;->h:[Lcom/google/android/gms/icing/c/a/z;

    aget-object v0, v0, v1

    .line 3544
    if-eqz v0, :cond_9

    .line 3545
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3542
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3549
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3550
    return-void
.end method
