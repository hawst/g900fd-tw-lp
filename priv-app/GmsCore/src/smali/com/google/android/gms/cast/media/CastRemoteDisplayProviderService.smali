.class public final Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/media/ad;


# instance fields
.field private a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->stopForeground(Z)V

    .line 55
    return-void
.end method

.method public final a(ILandroid/app/Notification;)V
    .locals 0

    .prologue
    .line 49
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->startForeground(ILandroid/app/Notification;)V

    .line 50
    return-void
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/gms/cast/a/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.android.media.remotedisplay.RemoteDisplayProvider"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    if-nez v0, :cond_0

    .line 26
    invoke-static {p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    .line 27
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 28
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/google/android/gms/cast/media/ad;)V

    .line 31
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 33
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    if-eqz v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a(Lcom/google/android/gms/cast/media/ad;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;->a()V

    .line 41
    iput-object v1, p0, Lcom/google/android/gms/cast/media/CastRemoteDisplayProviderService;->a:Lcom/google/android/gms/cast/media/CastRemoteDisplayProvider;

    .line 44
    :cond_0
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 45
    return-void
.end method
