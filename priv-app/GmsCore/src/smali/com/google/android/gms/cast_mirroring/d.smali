.class final Lcom/google/android/gms/cast_mirroring/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast_mirroring/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast_mirroring/c;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/c;->b:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->b()Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    .line 177
    if-eqz v1, :cond_2

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    invoke-static {v0}, Lcom/google/android/gms/cast_mirroring/c;->a(Lcom/google/android/gms/cast_mirroring/c;)I

    move-result v0

    const v2, 0x5b8d80

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/c;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast_mirroring/b/a;->a(Lcom/google/android/gms/cast/CastDevice;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/c;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 189
    :goto_2
    return-void

    .line 178
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 181
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/c;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0}, Lcom/google/android/gms/cast_mirroring/b/a;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 188
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/c;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_2

    .line 184
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/c;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    invoke-interface {v0}, Lcom/google/android/gms/cast_mirroring/b/a;->b()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 188
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/d;->a:Lcom/google/android/gms/cast_mirroring/c;

    iget-object v1, v1, Lcom/google/android/gms/cast_mirroring/c;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
