.class public final Lcom/google/android/gms/auth/setup/d2d/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "BluetoothConnectionSecurer"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/f;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method static synthetic a()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/f;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/auth/setup/d2d/a/b;Lcom/google/android/gms/auth/setup/d2d/j;[BLcom/google/android/gms/auth/setup/d2d/b/g;)Lcom/google/android/gms/auth/setup/d2d/k;
    .locals 3

    .prologue
    .line 55
    invoke-static {p3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/ab/b/a/e/f;->a([B[B)Lcom/google/ab/b/a/e/g;

    move-result-object v0

    .line 60
    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/h;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/auth/setup/d2d/h;-><init>(Lcom/google/android/gms/auth/setup/d2d/j;Lcom/google/ab/b/a/e/g;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/setup/d2d/a/b;->a(Lcom/google/android/gms/auth/setup/d2d/a/g;)V

    .line 63
    invoke-virtual {v0}, Lcom/google/ab/b/a/e/g;->a()[B

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/setup/d2d/a/b;->a([B)V

    .line 65
    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/i;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/gms/auth/setup/d2d/i;-><init>(Lcom/google/android/gms/auth/setup/d2d/a/b;Lcom/google/ab/b/a/e/g;B)V

    return-object v1
.end method

.method public static a(Lcom/google/android/gms/auth/setup/d2d/a/b;Lcom/google/android/gms/auth/setup/d2d/j;Lcom/google/ab/b/a/e/h;Lcom/google/android/gms/auth/setup/d2d/g;)V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/h;

    invoke-direct {v0, p1, p2, p0, p3}, Lcom/google/android/gms/auth/setup/d2d/h;-><init>(Lcom/google/android/gms/auth/setup/d2d/j;Lcom/google/ab/b/a/e/h;Lcom/google/android/gms/auth/setup/d2d/a/b;Lcom/google/android/gms/auth/setup/d2d/g;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/a/b;->a(Lcom/google/android/gms/auth/setup/d2d/a/g;)V

    .line 86
    return-void
.end method
