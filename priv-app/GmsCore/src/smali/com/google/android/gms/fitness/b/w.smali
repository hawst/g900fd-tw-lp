.class public final Lcom/google/android/gms/fitness/b/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/b/l;


# instance fields
.field private final a:F

.field private final b:F


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/b/x;)V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iget v0, p1, Lcom/google/android/gms/fitness/b/x;->a:F

    iput v0, p0, Lcom/google/android/gms/fitness/b/w;->a:F

    .line 15
    iget v0, p1, Lcom/google/android/gms/fitness/b/x;->b:F

    iput v0, p0, Lcom/google/android/gms/fitness/b/w;->b:F

    .line 16
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/b/x;B)V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/b/w;-><init>(Lcom/google/android/gms/fitness/b/x;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/b/j;)Z
    .locals 2

    .prologue
    .line 20
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/j;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/j;->c()F

    move-result v0

    iget v1, p0, Lcom/google/android/gms/fitness/b/w;->a:F

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/j;->c()F

    move-result v0

    iget v1, p0, Lcom/google/android/gms/fitness/b/w;->b:F

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
