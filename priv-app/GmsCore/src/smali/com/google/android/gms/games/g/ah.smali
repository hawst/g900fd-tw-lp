.class public final Lcom/google/android/gms/games/g/ah;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/android/gms/games/g/aj;

.field public c:[Lcom/google/android/gms/games/g/ai;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6983
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6984
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/games/g/aj;->a()[Lcom/google/android/gms/games/g/aj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    invoke-static {}, Lcom/google/android/gms/games/g/ai;->a()[Lcom/google/android/gms/games/g/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ah;->cachedSize:I

    .line 6985
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/games/g/ah;
    .locals 1

    .prologue
    .line 7152
    new-instance v0, Lcom/google/android/gms/games/g/ah;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ah;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/g/ah;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7061
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7062
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 7063
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7066
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 7067
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 7068
    iget-object v3, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    aget-object v3, v3, v0

    .line 7069
    if-eqz v3, :cond_1

    .line 7070
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 7067
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 7075
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 7076
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 7077
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    aget-object v2, v2, v1

    .line 7078
    if-eqz v2, :cond_4

    .line 7079
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 7076
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7084
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6997
    if-ne p1, p0, :cond_1

    .line 7019
    :cond_0
    :goto_0
    return v0

    .line 7000
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ah;

    if-nez v2, :cond_2

    move v0, v1

    .line 7001
    goto :goto_0

    .line 7003
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ah;

    .line 7004
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 7005
    iget-object v2, p1, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 7006
    goto :goto_0

    .line 7008
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 7009
    goto :goto_0

    .line 7011
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 7013
    goto :goto_0

    .line 7015
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 7017
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 7024
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7027
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7029
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7031
    return v0

    .line 7024
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6466
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/g/aj;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/games/g/aj;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/aj;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/games/g/aj;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/aj;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/g/ai;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/games/g/ai;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/ai;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/games/g/ai;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/ai;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7037
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7038
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7040
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 7041
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 7042
    iget-object v2, p0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    aget-object v2, v2, v0

    .line 7043
    if-eqz v2, :cond_1

    .line 7044
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7041
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7048
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 7049
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 7050
    iget-object v0, p0, Lcom/google/android/gms/games/g/ah;->c:[Lcom/google/android/gms/games/g/ai;

    aget-object v0, v0, v1

    .line 7051
    if-eqz v0, :cond_3

    .line 7052
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7049
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 7056
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7057
    return-void
.end method
