.class final Lcom/google/android/gms/security/verifier/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field final synthetic a:Lcom/google/android/gms/security/verifier/h;

.field private b:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

.field private c:Z

.field private d:Lcom/google/android/gms/security/verifier/ApkUploadEntry;


# direct methods
.method constructor <init>(Lcom/google/android/gms/security/verifier/h;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    iput-object p1, p0, Lcom/google/android/gms/security/verifier/i;->a:Lcom/google/android/gms/security/verifier/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    iput-object v1, p0, Lcom/google/android/gms/security/verifier/i;->b:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/i;->c:Z

    .line 121
    iput-object v1, p0, Lcom/google/android/gms/security/verifier/i;->d:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/i;->c:Z

    if-nez v0, :cond_0

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/i;->b:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/i;->b:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    iget-wide v0, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    .line 133
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/i;->a:Lcom/google/android/gms/security/verifier/h;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/h;J)Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/i;->d:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    .line 134
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/i;->c:Z

    .line 136
    :cond_0
    return-void

    .line 131
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final hasNext()Z
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/i;->a()V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/i;->d:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/gms/security/verifier/i;->a()V

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/i;->d:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/i;->d:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/i;->b:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/i;->d:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/i;->c:Z

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/i;->b:Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
