.class public Lcom/google/android/gms/wallet/service/PaymentService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/wallet/c/b;

.field private b:Lcom/google/android/gms/wallet/service/ow/w;

.field private c:Lcom/google/android/gms/wallet/service/ia/m;

.field private d:Landroid/content/Context;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/PaymentService;->c:Lcom/google/android/gms/wallet/service/ia/m;

    .line 73
    :goto_0
    return-object v0

    .line 68
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.service.BIND"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    new-instance v0, Lcom/google/android/gms/wallet/service/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/PaymentService;->a:Lcom/google/android/gms/wallet/c/b;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/wallet/service/q;-><init>(Lcom/google/android/gms/wallet/c/a;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/q;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0

    .line 70
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.wallet.service.ow.IOwInternalService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/PaymentService;->b:Lcom/google/android/gms/wallet/service/ow/w;

    goto :goto_0

    .line 73
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 25

    .prologue
    .line 78
    invoke-super/range {p0 .. p0}, Landroid/app/Service;->onCreate()V

    .line 80
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/service/PaymentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    .line 81
    new-instance v2, Lcom/google/android/gms/wallet/common/b/a;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/wallet/common/b/a;-><init>(Lcom/android/volley/s;)V

    .line 83
    new-instance v22, Lcom/google/android/gms/wallet/common/ab;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v0, v3}, Lcom/google/android/gms/wallet/common/ab;-><init>(Landroid/content/Context;)V

    .line 85
    new-instance v3, Lcom/google/android/gms/wallet/service/ia/o;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v3, v4, v2, v0}, Lcom/google/android/gms/wallet/service/ia/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/b/a;Lcom/google/android/gms/wallet/common/ab;)V

    .line 87
    new-instance v5, Lcom/google/android/gms/wallet/service/r;

    invoke-direct {v5}, Lcom/google/android/gms/wallet/service/r;-><init>()V

    .line 88
    new-instance v10, Lcom/google/android/gms/wallet/service/ow/ar;

    move-object/from16 v0, p0

    invoke-direct {v10, v0}, Lcom/google/android/gms/wallet/service/ow/ar;-><init>(Landroid/content/Context;)V

    .line 89
    new-instance v6, Lcom/google/android/gms/wallet/cache/j;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v6, v4}, Lcom/google/android/gms/wallet/cache/j;-><init>(Landroid/content/Context;)V

    .line 90
    new-instance v8, Lcom/google/android/gms/wallet/cache/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v8, v4}, Lcom/google/android/gms/wallet/cache/f;-><init>(Landroid/content/Context;)V

    .line 92
    new-instance v4, Lcom/google/android/gms/wallet/service/ia/ad;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v4, v7, v3}, Lcom/google/android/gms/wallet/service/ia/ad;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ia/o;)V

    .line 94
    new-instance v3, Lcom/google/android/gms/wallet/service/ia/d;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/gms/wallet/service/ia/d;-><init>(Lcom/google/android/gms/wallet/service/ia/m;Lcom/google/android/gms/wallet/service/r;Lcom/google/android/gms/wallet/cache/j;)V

    .line 96
    new-instance v4, Lcom/google/android/gms/wallet/service/ia/at;

    invoke-direct {v4, v3}, Lcom/google/android/gms/wallet/service/ia/at;-><init>(Lcom/google/android/gms/wallet/service/ia/m;)V

    .line 97
    new-instance v3, Lcom/google/android/gms/wallet/service/ia/i;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v3, v7, v4}, Lcom/google/android/gms/wallet/service/ia/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ia/m;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->c:Lcom/google/android/gms/wallet/service/ia/m;

    .line 99
    new-instance v24, Lcom/google/android/gms/wallet/service/ow/an;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v24

    invoke-direct {v0, v3}, Lcom/google/android/gms/wallet/service/ow/an;-><init>(Landroid/content/Context;)V

    .line 100
    new-instance v13, Lcom/google/android/gms/wallet/service/ow/ak;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v24

    invoke-direct {v13, v3, v2, v0}, Lcom/google/android/gms/wallet/service/ow/ak;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/b/a;Lcom/google/android/gms/wallet/service/ow/an;)V

    .line 103
    new-instance v7, Lcom/google/android/gms/wallet/cache/h;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v7, v2}, Lcom/google/android/gms/wallet/cache/h;-><init>(Landroid/content/Context;)V

    .line 104
    new-instance v9, Lcom/google/android/gms/wallet/service/ow/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v9, v2}, Lcom/google/android/gms/wallet/service/ow/a;-><init>(Landroid/content/Context;)V

    .line 105
    new-instance v4, Lcom/google/android/gms/wallet/service/ow/y;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v4, v2, v13, v9, v0}, Lcom/google/android/gms/wallet/service/ow/y;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/ak;Lcom/google/android/gms/wallet/service/ow/a;Lcom/google/android/gms/wallet/common/ab;)V

    .line 108
    new-instance v2, Lcom/google/android/gms/wallet/service/ow/d;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/k;->a()Lcom/google/android/gms/wallet/service/ow/k;

    move-result-object v11

    invoke-direct/range {v2 .. v11}, Lcom/google/android/gms/wallet/service/ow/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/service/r;Lcom/google/android/gms/wallet/cache/j;Lcom/google/android/gms/wallet/cache/h;Lcom/google/android/gms/wallet/cache/f;Lcom/google/android/gms/wallet/service/ow/a;Lcom/google/android/gms/wallet/service/ow/ar;Lcom/google/android/gms/wallet/service/ow/k;)V

    .line 112
    new-instance v3, Lcom/google/android/gms/wallet/service/ow/aq;

    invoke-direct {v3, v2}, Lcom/google/android/gms/wallet/service/ow/aq;-><init>(Lcom/google/android/gms/wallet/service/ow/w;)V

    .line 114
    new-instance v4, Lcom/google/android/gms/wallet/service/ow/g;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v4, v11, v3}, Lcom/google/android/gms/wallet/service/ow/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/gms/wallet/service/PaymentService;->b:Lcom/google/android/gms/wallet/service/ow/w;

    .line 117
    new-instance v18, Lcom/google/android/gms/wallet/service/ow/m;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/k;->a()Lcom/google/android/gms/wallet/service/ow/k;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-direct {v0, v3, v2, v4}, Lcom/google/android/gms/wallet/service/ow/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/service/ow/k;)V

    .line 119
    new-instance v11, Lcom/google/android/gms/wallet/service/ow/af;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/k;->a()Lcom/google/android/gms/wallet/service/ow/k;

    move-result-object v20

    move-object v14, v10

    move-object v15, v7

    move-object/from16 v16, v9

    move-object/from16 v17, v6

    move-object/from16 v19, v2

    move-object/from16 v21, v8

    move-object/from16 v23, v5

    invoke-direct/range {v11 .. v24}, Lcom/google/android/gms/wallet/service/ow/af;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/ak;Lcom/google/android/gms/wallet/service/ow/ar;Lcom/google/android/gms/wallet/cache/h;Lcom/google/android/gms/wallet/service/ow/a;Lcom/google/android/gms/wallet/cache/j;Lcom/google/android/gms/wallet/service/ow/m;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/service/ow/k;Lcom/google/android/gms/wallet/cache/f;Lcom/google/android/gms/wallet/common/ab;Lcom/google/android/gms/wallet/service/r;Lcom/google/android/gms/wallet/service/ow/an;)V

    .line 124
    new-instance v2, Lcom/google/android/gms/wallet/service/ow/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-direct {v2, v3, v11}, Lcom/google/android/gms/wallet/service/ow/h;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/c/b;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->a:Lcom/google/android/gms/wallet/c/b;

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/PaymentService;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 128
    new-instance v2, Ljava/util/Timer;

    invoke-direct {v2}, Ljava/util/Timer;-><init>()V

    new-instance v3, Lcom/google/android/gms/wallet/service/j;

    move-object/from16 v0, p0

    invoke-direct {v3, v0}, Lcom/google/android/gms/wallet/service/j;-><init>(Lcom/google/android/gms/wallet/service/PaymentService;)V

    const-wide/16 v4, 0x7530

    invoke-virtual {v2, v3, v4, v5}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 138
    :cond_0
    return-void
.end method
