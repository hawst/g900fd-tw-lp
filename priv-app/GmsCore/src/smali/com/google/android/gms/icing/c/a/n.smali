.class public final Lcom/google/android/gms/icing/c/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2085
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2086
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/n;->cachedSize:I

    .line 2087
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2130
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2131
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    if-eqz v1, :cond_0

    .line 2132
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2135
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2098
    if-ne p1, p0, :cond_1

    .line 2099
    const/4 v0, 0x1

    .line 2108
    :cond_0
    :goto_0
    return v0

    .line 2101
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/n;

    if-eqz v1, :cond_0

    .line 2104
    check-cast p1, Lcom/google/android/gms/icing/c/a/n;

    .line 2105
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/n;->a:Z

    if-ne v1, v2, :cond_0

    .line 2108
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/n;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2113
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2115
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/n;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2116
    return v0

    .line 2113
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2065
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2122
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    if-eqz v0, :cond_0

    .line 2123
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/n;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2125
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2126
    return-void
.end method
