.class public final Lcom/google/android/gms/photos/autobackup/ui/promo/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/i/af;
.implements Lcom/google/android/libraries/social/i/ag;
.implements Lcom/google/android/libraries/social/i/ak;


# instance fields
.field private final a:Landroid/app/Activity;

.field private final b:Lcom/google/android/gms/common/api/x;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/libraries/social/i/w;Lcom/google/android/gms/common/api/x;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/l;->a:Landroid/app/Activity;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/l;->b:Lcom/google/android/gms/common/api/x;

    .line 30
    invoke-virtual {p2, p0}, Lcom/google/android/libraries/social/i/w;->a(Lcom/google/android/libraries/social/i/ak;)Lcom/google/android/libraries/social/i/ak;

    .line 31
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/l;->a:Landroid/app/Activity;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/ui/ae;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/l;->b:Lcom/google/android/gms/common/api/x;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/x;)V

    .line 38
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/l;->a:Landroid/app/Activity;

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/ui/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/ui/ae;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/l;->b:Lcom/google/android/gms/common/api/x;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 45
    return-void
.end method
