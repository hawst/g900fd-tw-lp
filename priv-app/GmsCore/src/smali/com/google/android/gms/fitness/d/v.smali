.class final Lcom/google/android/gms/fitness/d/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/v;

.field final synthetic b:Lcom/google/android/gms/fitness/d/t;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/d/t;Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/gms/fitness/d/v;->b:Lcom/google/android/gms/fitness/d/t;

    iput-object p2, p0, Lcom/google/android/gms/fitness/d/v;->a:Lcom/google/android/gms/common/api/v;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 2

    .prologue
    .line 207
    check-cast p1, Lcom/google/android/gms/wearable/u;

    invoke-interface {p1}, Lcom/google/android/gms/wearable/u;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/d/v;->b:Lcom/google/android/gms/fitness/d/t;

    iget-object v0, v0, Lcom/google/android/gms/fitness/d/t;->b:Lcom/google/android/gms/fitness/d/x;

    invoke-interface {p1}, Lcom/google/android/gms/wearable/u;->b()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/d/x;->a(Ljava/util/Collection;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/d/v;->b:Lcom/google/android/gms/fitness/d/t;

    iget-object v1, p0, Lcom/google/android/gms/fitness/d/v;->a:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/d/t;->a(Lcom/google/android/gms/common/api/v;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "WearableAccountManager error getting nodes: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/wearable/u;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
