.class public abstract Lcom/google/android/gms/drive/database/model/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput p1, p0, Lcom/google/android/gms/drive/database/model/ae;->a:I

    .line 37
    return-void
.end method

.method public static a([Lcom/google/android/gms/drive/g/ak;I)Ljava/lang/StringBuilder;
    .locals 7

    .prologue
    .line 214
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 215
    array-length v3, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, p0, v1

    .line 216
    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ak;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ab;

    .line 217
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 218
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(I)Lcom/google/android/gms/drive/database/model/at;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, v0, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v0, Lcom/google/android/gms/drive/database/model/at;->b:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/aw;->name()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v6, v0, Lcom/google/android/gms/drive/database/model/at;->h:Z

    if-eqz v6, :cond_0

    const-string v6, " NOT NULL"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v6, v0, Lcom/google/android/gms/drive/database/model/at;->g:Ljava/lang/Object;

    if-eqz v6, :cond_1

    const-string v6, " DEFAULT "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/at;->g:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 215
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 222
    :cond_3
    return-object v2
.end method

.method private static a([Lcom/google/android/gms/drive/g/ak;ILjava/lang/String;Z)Ljava/lang/StringBuilder;
    .locals 5

    .prologue
    .line 274
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 275
    array-length v3, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, p0, v1

    .line 276
    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ak;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ab;

    .line 277
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 278
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/drive/database/model/ab;->a(ILjava/lang/String;Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 275
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 284
    :cond_1
    if-eqz p3, :cond_2

    .line 285
    const-string v0, " LIMIT 0"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 289
    :goto_1
    return-object v2

    .line 287
    :cond_2
    const-string v0, " FROM "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method

.method public static b([Lcom/google/android/gms/drive/g/ak;I)Ljava/lang/StringBuilder;
    .locals 7

    .prologue
    .line 234
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 235
    array-length v3, p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v0, p0, v1

    .line 236
    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ak;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ab;

    .line 237
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 238
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(I)Lcom/google/android/gms/drive/database/model/at;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v4, Lcom/google/android/gms/drive/database/model/at;->c:Lcom/google/android/gms/drive/database/model/ae;

    if-nez v5, :cond_1

    const/4 v0, 0x0

    .line 241
    :goto_1
    if-eqz v0, :cond_0

    .line 242
    const-string v4, ", "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 235
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 238
    :cond_1
    const-string v6, "FOREIGN KEY("

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v4, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, ") REFERENCES "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p1}, Lcom/google/android/gms/drive/database/model/ae;->a(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "("

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v6, v4, Lcom/google/android/gms/drive/database/model/at;->d:Lcom/google/android/gms/drive/database/model/ab;

    if-eqz v6, :cond_2

    iget-object v5, v4, Lcom/google/android/gms/drive/database/model/at;->d:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v5, p1}, Lcom/google/android/gms/drive/database/model/ab;->c(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    const-string v5, ") ON DELETE "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, v4, Lcom/google/android/gms/drive/database/model/at;->i:Lcom/google/android/gms/drive/database/model/av;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/av;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 244
    :cond_3
    return-object v2
.end method


# virtual methods
.method public final a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ae;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/String;)Ljava/lang/StringBuilder;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 254
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/model/ae;->b(I)Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 255
    add-int/lit8 v1, p1, -0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/database/model/ae;->b(I)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 259
    :goto_0
    if-eqz v1, :cond_0

    move v0, p1

    .line 260
    :cond_0
    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    .line 264
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    const-string v3, "SELECT "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 266
    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(ILjava/lang/String;Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ae;->c()[Lcom/google/android/gms/drive/g/ak;

    move-result-object v0

    invoke-static {v0, p1, p2, v1}, Lcom/google/android/gms/drive/database/model/ae;->a([Lcom/google/android/gms/drive/g/ak;ILjava/lang/String;Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 268
    return-object v2

    :cond_1
    move v1, v0

    .line 255
    goto :goto_0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public final b(I)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ae;->c()[Lcom/google/android/gms/drive/g/ak;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 88
    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ak;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const/4 v0, 0x1

    .line 92
    :goto_1
    return v0

    .line 87
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 92
    goto :goto_1
.end method

.method public abstract c()[Lcom/google/android/gms/drive/g/ak;
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ae;->g()Z

    move-result v0

    const-string v1, "Table not present in the current version."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 56
    iget v0, p0, Lcom/google/android/gms/drive/database/model/ae;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ae;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ae;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lcom/google/android/gms/drive/database/model/ae;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ae;->b(I)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 294
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "DatabaseTable[%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
