.class public Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/g/at;

.field private final b:Lcom/google/android/gms/drive/d/f;

.field private final c:Lcom/google/android/gms/drive/database/r;

.field private final d:Ljava/util/concurrent/BlockingQueue;

.field private final e:Lcom/google/android/gms/common/server/ClientContext;

.field private final f:Lcom/google/android/gms/common/server/ClientContext;

.field private final g:Lcom/google/android/gms/drive/database/model/a;

.field private final h:Lcom/google/android/gms/drive/metadata/sync/a/d;

.field private final i:I

.field private final j:I

.field private volatile k:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;II)V
    .locals 8

    .prologue
    .line 65
    new-instance v7, Lcom/google/android/gms/drive/g/e;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->b()Lcom/google/android/gms/drive/g/i;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/ai;->C:Lcom/google/android/gms/common/a/d;

    sget-object v2, Lcom/google/android/gms/drive/ai;->D:Lcom/google/android/gms/common/a/d;

    invoke-direct {v7, v0, v1, v2}, Lcom/google/android/gms/drive/g/e;-><init>(Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;-><init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;IILcom/google/android/gms/drive/g/at;)V

    .line 69
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/BlockingQueue;Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;IILcom/google/android/gms/drive/g/at;)V
    .locals 4

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->k:Z

    .line 80
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-interface {v0, p4}, Lcom/google/android/gms/drive/database/r;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->g:Lcom/google/android/gms/drive/database/model/a;

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->g:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v0}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->e:Lcom/google/android/gms/common/server/ClientContext;

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->g:Lcom/google/android/gms/drive/database/model/a;

    new-instance v1, Lcom/google/android/gms/drive/auth/g;

    sget-object v2, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    sget-object v3, Lcom/google/android/gms/drive/aa;->d:Lcom/google/android/gms/drive/aa;

    invoke-static {v2, v3}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/auth/g;-><init>(Lcom/google/android/gms/drive/database/model/a;Ljava/util/Set;)V

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->f:Lcom/google/android/gms/common/server/ClientContext;

    .line 83
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/at;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->a:Lcom/google/android/gms/drive/g/at;

    .line 84
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->b:Lcom/google/android/gms/drive/d/f;

    .line 85
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->c:Lcom/google/android/gms/drive/database/r;

    .line 86
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/BlockingQueue;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->d:Ljava/util/concurrent/BlockingQueue;

    .line 87
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/a/d;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->h:Lcom/google/android/gms/drive/metadata/sync/a/d;

    .line 88
    iput p5, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->i:I

    .line 89
    iput p6, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->j:I

    .line 90
    const-class v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->setName(Ljava/lang/String;)V

    .line 91
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 194
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->start()V

    .line 195
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->join()V

    .line 200
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 204
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->isAlive()Z

    move-result v0

    return v0
.end method

.method public interrupt()V
    .locals 1

    .prologue
    .line 210
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->k:Z

    .line 211
    invoke-super {p0}, Ljava/lang/Thread;->interrupt()V

    .line 212
    return-void
.end method

.method public run()V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 95
    iget-object v8, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->h:Lcom/google/android/gms/drive/metadata/sync/a/d;

    move v9, v10

    .line 98
    :goto_0
    :try_start_0
    invoke-virtual {v8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->i:I

    if-ge v9, v2, :cond_1

    .line 99
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->a:Lcom/google/android/gms/drive/g/at;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/at;->e()V

    .line 104
    iget-object v2, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/a/c;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    .line 105
    sget-object v3, Lcom/google/android/gms/drive/metadata/sync/syncadapter/l;->a:[I

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/a/e;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 119
    new-instance v3, Ljava/lang/RuntimeException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unsupported feed type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 128
    :catchall_0
    move-exception v2

    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->a:Lcom/google/android/gms/drive/g/at;

    invoke-interface {v3}, Lcom/google/android/gms/drive/g/at;->f()V

    throw v2
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 139
    :catch_0
    move-exception v2

    move-object v2, v8

    :goto_1
    const-string v3, "ResultsPageProducer"

    const-string v4, "Process was interrupted. Cannot fetch feed."

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v3, "ResultsPageProducer"

    const-string v4, "Process was interrupted. Cannot fetch %s."

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    aput-object v2, v5, v10

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 158
    :cond_0
    :goto_2
    return-void

    .line 107
    :pswitch_0
    :try_start_3
    iget-object v2, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    instance-of v2, v2, Lcom/google/android/gms/drive/metadata/sync/a/j;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    iget-object v2, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    check-cast v2, Lcom/google/android/gms/drive/metadata/sync/a/j;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->b:Lcom/google/android/gms/drive/d/f;

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v5, v2, Lcom/google/android/gms/drive/metadata/sync/a/j;->b:Ljava/util/Set;

    invoke-virtual {v8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a()Ljava/lang/String;

    move-result-object v6

    iget-boolean v2, v2, Lcom/google/android/gms/drive/metadata/sync/a/j;->a:Z

    invoke-interface {v3, v4, v5, v6, v2}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Z)Lcom/google/android/gms/drive/d/g;

    move-result-object v2

    .line 122
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->d:Ljava/util/concurrent/BlockingQueue;

    iget v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->j:I

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v4, v2, v6, v7}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;-><init>(ILcom/google/android/gms/drive/d/g;Ljava/lang/Exception;Ljava/lang/String;)V

    const-wide v6, 0x7fffffffffffffffL

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v5, v6, v7, v4}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z

    .line 125
    invoke-interface {v2}, Lcom/google/android/gms/drive/d/g;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v8, v2}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/d;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 126
    add-int/lit8 v3, v9, 0x1

    .line 128
    :try_start_4
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->a:Lcom/google/android/gms/drive/g/at;

    invoke-interface {v4}, Lcom/google/android/gms/drive/g/at;->f()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    move v9, v3

    move-object v8, v2

    .line 129
    goto/16 :goto_0

    .line 110
    :pswitch_1
    :try_start_5
    iget-object v2, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    instance-of v2, v2, Lcom/google/android/gms/drive/metadata/sync/a/b;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    iget-object v2, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/a/b;

    move-object v6, v0

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->b:Lcom/google/android/gms/drive/d/f;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, v6, Lcom/google/android/gms/drive/metadata/sync/a/b;->b:Ljava/util/Set;

    invoke-virtual {v8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a()Ljava/lang/String;

    move-result-object v5

    iget-wide v6, v6, Lcom/google/android/gms/drive/metadata/sync/a/b;->a:J

    invoke-interface/range {v2 .. v7}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;J)Lcom/google/android/gms/drive/d/g;

    move-result-object v2

    goto :goto_3

    .line 113
    :pswitch_2
    iget-object v2, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    check-cast v2, Lcom/google/android/gms/drive/metadata/sync/a/k;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->g:Lcom/google/android/gms/drive/database/model/a;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Ljava/util/Set;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->b:Lcom/google/android/gms/drive/d/f;

    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->e:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/a/k;->a:Ljava/lang/String;

    invoke-interface {v4, v5, v3, v6, v2}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/d/g;

    move-result-object v2

    goto :goto_3

    .line 116
    :pswitch_3
    iget-object v2, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    check-cast v2, Lcom/google/android/gms/drive/metadata/sync/a/a;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->b:Lcom/google/android/gms/drive/d/f;

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->f:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, v2, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    invoke-virtual {v8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-interface {v3, v4, v2, v5, v6}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/d/g;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    move-result-object v2

    goto :goto_3

    .line 130
    :cond_1
    :try_start_6
    invoke-virtual {v8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->b()Z

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 135
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->d:Ljava/util/concurrent/BlockingQueue;

    iget v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->j:I

    invoke-virtual {v8}, Lcom/google/android/gms/drive/metadata/sync/a/d;->a()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v3, v6, v7, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;-><init>(ILcom/google/android/gms/drive/d/g;Ljava/lang/Exception;Ljava/lang/String;)V

    const-wide v6, 0x7fffffffffffffffL

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v5, v6, v7, v3}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto/16 :goto_2

    .line 141
    :catch_1
    move-exception v2

    .line 142
    :goto_4
    const-string v3, "ResultsPageProducer"

    const-string v4, "Error fetching feed"

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    const-string v3, "ResultsPageProducer"

    const-string v4, "Error fetching %s."

    new-array v5, v11, [Ljava/lang/Object;

    iget-object v6, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    aput-object v6, v5, v10

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 146
    iget-boolean v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->k:Z

    if-nez v3, :cond_0

    .line 148
    :try_start_7
    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->d:Ljava/util/concurrent/BlockingQueue;

    iget v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/k;->j:I

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v5, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v5, v4, v6, v2, v7}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/m;-><init>(ILcom/google/android/gms/drive/d/g;Ljava/lang/Exception;Ljava/lang/String;)V

    const-wide v6, 0x7fffffffffffffffL

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v5, v6, v7, v2}, Ljava/util/concurrent/BlockingQueue;->offer(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Z
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_2

    goto/16 :goto_2

    .line 152
    :catch_2
    move-exception v2

    const-string v2, "ResultsPageProducer"

    const-string v3, "Producer cannot be terminated gracefully"

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    const-string v2, "ResultsPageProducer"

    const-string v3, "Producer cannot be terminated gracefully %s."

    new-array v4, v11, [Ljava/lang/Object;

    iget-object v5, v8, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    aput-object v5, v4, v10

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_2

    .line 141
    :catch_3
    move-exception v3

    move-object v8, v2

    move-object v2, v3

    goto :goto_4

    .line 139
    :catch_4
    move-exception v3

    goto/16 :goto_1

    .line 105
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
