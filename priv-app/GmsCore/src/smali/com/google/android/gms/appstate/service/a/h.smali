.class public final Lcom/google/android/gms/appstate/service/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/appstate/service/c;


# instance fields
.field private final a:Lcom/google/android/gms/common/internal/bg;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/android/gms/appstate/service/a/h;->a:Lcom/google/android/gms/common/internal/bg;

    .line 58
    iput p2, p0, Lcom/google/android/gms/appstate/service/a/h;->b:I

    .line 59
    iput-object p3, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    .line 60
    iput-object p4, p0, Lcom/google/android/gms/appstate/service/a/h;->d:Ljava/lang/String;

    .line 61
    iput-object p5, p0, Lcom/google/android/gms/appstate/service/a/h;->e:[Ljava/lang/String;

    .line 62
    return-void
.end method

.method private a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 236
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 237
    if-eqz p4, :cond_0

    .line 238
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p1, v1, p4, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 240
    const-string v2, "pendingIntent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 243
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a/h;->a:Lcom/google/android/gms/common/internal/bg;

    invoke-interface {v1, p2, p3, v0}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/appstate/a/e;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v5, 0x1

    const/16 v8, 0xa

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 71
    invoke-static {p1}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    new-instance v0, Lcom/google/android/gms/common/ui/i;

    sget v1, Lcom/google/android/gms/p;->eu:I

    sget v2, Lcom/google/android/gms/p;->eF:I

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/common/ui/i;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/i;->a()Lcom/google/android/gms/common/ui/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    .line 77
    const/4 v1, 0x6

    invoke-direct {p0, p1, v1, v4, v0}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    .line 192
    :goto_0
    return-void

    .line 83
    :cond_0
    iget v6, p0, Lcom/google/android/gms/appstate/service/a/h;->b:I

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    const-string v1, "com.google.android.gms.appstate.APP_ID"

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    if-nez v0, :cond_1

    .line 89
    const-string v0, ""

    move-object v1, v0

    .line 105
    :goto_1
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/appstate/service/a/h;->b:I

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    if-eq v0, v3, :cond_2

    .line 106
    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing metadata tag with the name \"com.google.android.gms.appstate.APP_ID\" in the application tag of the manifest for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    invoke-direct {p0, p1, v8, v4, v4}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto :goto_0

    .line 94
    :cond_1
    :try_start_0
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v1, v0

    .line 101
    goto :goto_1

    .line 96
    :catch_0
    move-exception v1

    const-string v1, "ValidateServiceOp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Application ID ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") must be a numeric value. Please verify that your manifest refers to the correct project ID."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-direct {p0, p1, v8, v4, v4}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto :goto_0

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/h;->d:Ljava/lang/String;

    const-string v3, "<<default account>>"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/google/android/gms/common/util/a;->e(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    if-nez v0, :cond_6

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v5, :cond_5

    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v5, v0

    .line 114
    :goto_2
    if-nez v5, :cond_7

    .line 115
    invoke-static {}, Lcom/google/android/gms/common/internal/ay;->b()Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-direct {p0, p1, v9, v4, v0}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 113
    :cond_4
    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    invoke-static {p1, v0, v3}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    move-object v5, v4

    goto :goto_2

    :cond_5
    move-object v5, v4

    goto :goto_2

    :cond_6
    move-object v5, v0

    goto :goto_2

    .line 125
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    invoke-static {p1, v6, v5, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    .line 127
    if-eqz v3, :cond_8

    move v0, v2

    .line 129
    :goto_3
    iget-object v7, p0, Lcom/google/android/gms/appstate/service/a/h;->e:[Ljava/lang/String;

    array-length v7, v7

    if-ge v0, v7, :cond_8

    .line 130
    iget-object v7, p0, Lcom/google/android/gms/appstate/service/a/h;->e:[Ljava/lang/String;

    aget-object v7, v7, v0

    invoke-virtual {v3, v7}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_a

    move-object v3, v4

    .line 141
    :cond_8
    if-nez v3, :cond_e

    .line 142
    :try_start_1
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v7, p0, Lcom/google/android/gms/appstate/service/a/h;->c:Ljava/lang/String;

    invoke-direct {v0, v6, v5, v5, v7}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 147
    :try_start_2
    iget-object v3, p0, Lcom/google/android/gms/appstate/service/a/h;->e:[Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    .line 148
    new-instance v3, Lcom/google/android/gms/common/server/a/a;

    const/4 v5, 0x1

    invoke-direct {v3, v0, v5}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 150
    invoke-interface {v3, p1}, Lcom/google/android/gms/common/server/a/c;->b(Landroid/content/Context;)Ljava/lang/String;

    .line 154
    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 176
    :goto_4
    invoke-static {v0}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/appstate/service/a;

    move-result-object v3

    .line 177
    if-eqz v3, :cond_9

    invoke-virtual {v3}, Lcom/google/android/gms/appstate/service/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_d

    .line 178
    :cond_9
    new-instance v3, Lcom/google/android/gms/appstate/service/a;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v3, v5, v0, v1}, Lcom/google/android/gms/appstate/service/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    invoke-static {v0, v3}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/service/a;)V

    move-object v1, v3

    .line 183
    :goto_5
    if-nez v1, :cond_c

    .line 184
    invoke-direct {p0, p1, v8, v4, v4}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 129
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 156
    :catch_1
    move-exception v0

    .line 158
    :goto_6
    invoke-virtual {v3, p1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    .line 160
    instance-of v1, v0, Lcom/google/android/gms/auth/ae;

    if-eqz v1, :cond_b

    .line 161
    check-cast v0, Lcom/google/android/gms/auth/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v9, v4, v0}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 168
    :cond_b
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0, v4, v4}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 171
    :catch_2
    move-exception v0

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0, v4, v4}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 190
    :cond_c
    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 191
    invoke-direct {p0, p1, v2, v1, v4}, Lcom/google/android/gms/appstate/service/a/h;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 156
    :catch_3
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_6

    :cond_d
    move-object v1, v3

    goto :goto_5

    :cond_e
    move-object v0, v3

    goto :goto_4
.end method
