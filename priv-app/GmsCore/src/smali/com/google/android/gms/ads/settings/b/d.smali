.class public final Lcom/google/android/gms/ads/settings/b/d;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static declared-synchronized a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 98
    const-class v1, Lcom/google/android/gms/ads/settings/b/d;

    monitor-enter v1

    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/f;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 100
    if-eq v0, v3, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_1

    .line 194
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 117
    :cond_1
    :try_start_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xf

    if-lt v0, v2, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x16

    if-gt v0, v2, :cond_0

    .line 122
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->a()Lcom/google/android/gms/ads/identifier/b/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b/b;->f()I

    move-result v0

    if-ne v0, v3, :cond_0

    .line 129
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/f;->a(I)V

    .line 134
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/i;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 137
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/b;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/ads/settings/c/b;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lcom/google/android/gms/ads/settings/c/f;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/ads/settings/c/f;-><init>(Landroid/content/Context;)V

    .line 142
    new-instance v2, Lcom/google/android/gms/ads/settings/b/e;

    invoke-direct {v2}, Lcom/google/android/gms/ads/settings/b/e;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/settings/c/f;->a(Lcom/google/android/gms/ads/settings/c/h;)V

    .line 192
    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/f;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v5, 0x7

    const/4 v4, 0x5

    const/4 v0, 0x0

    .line 79
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v2, ".-"

    invoke-direct {v1, p0, v2}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v2

    const/4 v3, 0x3

    if-lt v2, v3, :cond_2

    .line 82
    :try_start_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 83
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 84
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 85
    if-gt v2, v4, :cond_1

    if-ne v2, v4, :cond_0

    if-gt v3, v5, :cond_1

    :cond_0
    if-ne v2, v4, :cond_2

    if-ne v3, v5, :cond_2

    if-ltz v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 93
    :cond_2
    :goto_0
    return v0

    .line 90
    :catch_0
    move-exception v1

    goto :goto_0
.end method
