.class final Lcom/google/android/gms/wallet/dynamite/ui/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/dynamite/ui/b;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Landroid/graphics/Paint;

.field i:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/ui/f;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/res/Resources;Landroid/graphics/Canvas;I)V
    .locals 6

    .prologue
    .line 137
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->a:I

    add-int/2addr v0, p3

    .line 138
    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->c:I

    add-int/2addr v1, p3

    .line 139
    iget v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->i:I

    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 140
    iget v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->b:I

    iget v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->d:I

    invoke-virtual {v2, v0, v3, v1, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->f:Ljava/lang/String;

    int-to-float v3, p3

    iget v4, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->e:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->h:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v3, v4, v5}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 146
    :cond_0
    invoke-virtual {v2, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->g:Ljava/lang/String;

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->e:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->h:Landroid/graphics/Paint;

    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 150
    :cond_1
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " Google "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/f;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
