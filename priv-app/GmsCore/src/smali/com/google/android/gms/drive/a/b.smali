.class public abstract Lcom/google/android/gms/drive/a/b;
.super Lcom/google/android/gms/drive/a/a;
.source "SourceFile"


# instance fields
.field final c:Lcom/google/android/gms/drive/database/model/EntrySpec;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;)V
    .locals 7

    .prologue
    .line 39
    sget-object v6, Lcom/google/android/gms/drive/a/a/l;->a:Lcom/google/android/gms/drive/a/a/l;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V

    .line 41
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V
    .locals 6

    .prologue
    .line 33
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;Lcom/google/android/gms/drive/a/a/l;)V

    .line 34
    const-string v0, "Entryspec must not be null"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/EntrySpec;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 35
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/a;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 46
    const-string v0, "entrySqlId"

    invoke-virtual {p3, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 47
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 48
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-interface {p1, p2, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 85
    if-nez v0, :cond_0

    .line 86
    new-instance v0, Lcom/google/android/gms/drive/a/u;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/a/u;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0

    .line 88
    :cond_0
    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/drive/a/b;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/drive/g/aw;)V
    .locals 3

    .prologue
    .line 97
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 98
    iget-object v1, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 99
    invoke-virtual {p2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/drive/a/b;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V

    .line 101
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
.end method

.method protected final a(Lcom/google/android/gms/drive/a/a;)Z
    .locals 2

    .prologue
    .line 116
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/a/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/a/a;->j()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/drive/a/c;)Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/a/c;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/a;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/b;->f(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic b(Lcom/google/android/gms/drive/g/aw;)V
    .locals 0

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->b(Lcom/google/android/gms/drive/g/aw;)V

    return-void
.end method

.method protected c(Lcom/google/android/gms/drive/g/aw;)V
    .locals 3

    .prologue
    .line 54
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 56
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v2

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 57
    if-nez v0, :cond_0

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/a/u;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/a/u;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0

    .line 60
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 61
    new-instance v0, Lcom/google/android/gms/drive/a/v;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/a/v;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0

    .line 63
    :cond_1
    return-void
.end method

.method public final bridge synthetic d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->d(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    return-object v0
.end method

.method protected final f(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/a/a;->e(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    return-object v0
.end method

.method public f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 110
    const-string v1, "entrySqlId"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 111
    return-object v0
.end method

.method public final bridge synthetic g()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/gms/drive/a/a;->g()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    return-object v0
.end method

.method protected final i()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->hashCode()I

    move-result v0

    return v0
.end method

.method public final j()Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    return-object v0
.end method
