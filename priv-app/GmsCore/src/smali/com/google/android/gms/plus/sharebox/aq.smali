.class public final Lcom/google/android/gms/plus/sharebox/aq;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/gms/plus/audience/bh;


# instance fields
.field protected a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

.field protected b:Landroid/view/ViewGroup;

.field protected c:Landroid/widget/CheckBox;

.field protected d:Z

.field protected e:Lcom/google/android/gms/plus/model/posts/Settings;

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Lcom/google/android/gms/plus/sharebox/as;

.field private m:Lcom/google/android/gms/plus/audience/ae;

.field private n:Lcom/google/android/gms/plus/sharebox/h;

.field private o:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

.field private p:Landroid/view/View;

.field private q:Landroid/widget/ScrollView;

.field private r:Lcom/google/android/gms/plus/sharebox/AudienceView;

.field private s:Landroid/widget/ImageView;

.field private t:Lcom/google/android/gms/plus/data/a/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 100
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/aq;)Landroid/view/View;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/aq;)Landroid/widget/ScrollView;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->q:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/sharebox/aq;)Lcom/google/android/gms/plus/sharebox/as;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/sharebox/aq;)Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->i:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/sharebox/aq;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->i:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/sharebox/aq;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->g:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/sharebox/aq;)Z
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->g:Z

    return v0
.end method

.method private l()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Settings;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->k:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    const-string v3, "Audience must not be null."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_3

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    if-eq v0, v1, :cond_0

    const/4 v6, 0x4

    if-ne v0, v6, :cond_2

    :cond_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->o()V

    .line 391
    iput-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->k:Z

    .line 393
    :cond_1
    return-void

    .line 387
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 381
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->j:Z

    .line 382
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/aq;->l()V

    .line 383
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->s:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 429
    return-void
.end method

.method final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->b()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 592
    iput-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->i:Z

    .line 594
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 598
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 599
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/AudienceView;->setEnabled(Z)V

    .line 602
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->d:Z

    if-nez v0, :cond_2

    .line 603
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->l:Z

    if-eqz v0, :cond_3

    .line 604
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/sharebox/as;->b(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 609
    :cond_2
    :goto_0
    return-void

    .line 606
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->e()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/data/a/a;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 519
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/aq;->t:Lcom/google/android/gms/plus/data/a/a;

    .line 520
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->d:Z

    if-eqz v0, :cond_1

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 525
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->t:Lcom/google/android/gms/plus/data/a/a;

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/a/b;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 531
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->t:Lcom/google/android/gms/plus/data/a/a;

    if-nez v0, :cond_4

    .line 532
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/sharebox/at;->a(Ljava/lang/String;Lcom/google/android/gms/plus/sharebox/a/b;)Lcom/google/android/gms/plus/data/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->t:Lcom/google/android/gms/plus/data/a/a;

    .line 535
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->h:Z

    if-nez v0, :cond_4

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 537
    iput-boolean v3, p0, Lcom/google/android/gms/plus/sharebox/aq;->h:Z

    .line 541
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 542
    new-instance v0, Lcom/google/android/gms/plus/sharebox/ab;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/sharebox/ab;-><init>(Landroid/content/Context;)V

    .line 543
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/au;->a()Lcom/google/android/gms/plus/internal/ab;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ab;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 544
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->t:Lcom/google/android/gms/plus/data/a/a;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/aq;->t:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/data/a/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/ab;->a(Lcom/google/android/gms/plus/data/a/a;Ljava/lang/String;)Z

    .line 545
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 547
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->h:Z

    if-nez v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 549
    iput-boolean v3, p0, Lcom/google/android/gms/plus/sharebox/aq;->h:Z

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 7

    .prologue
    .line 554
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/aq;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    .line 557
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Settings;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->c()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/sharebox/as;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 570
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/aq;->l()V

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a(Z)V

    .line 576
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/au;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/common/analytics/a;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v5}, Lcom/google/android/gms/plus/sharebox/as;->getCallingPackage()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v6}, Lcom/google/android/gms/plus/sharebox/as;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Landroid/support/v4/app/au;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/audience/bg;)V

    .line 582
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->p()V

    .line 584
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->f:Z

    if-nez v0, :cond_1

    .line 585
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 586
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->f:Z

    .line 588
    :cond_1
    return-void

    .line 561
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Settings;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 562
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->e:Lcom/google/android/gms/plus/model/posts/Settings;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/model/posts/Settings;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/sharebox/as;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    goto :goto_0

    .line 567
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    sget-object v1, Lcom/google/android/gms/common/people/data/a;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/sharebox/as;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V
    .locals 5

    .prologue
    .line 613
    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    const-string v1, "ShareBox"

    const-string v2, "Loaded add to circle data: %s"

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    if-nez p1, :cond_2

    const/4 v0, 0x0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 617
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 618
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->e()V

    .line 626
    :cond_1
    :goto_1
    return-void

    .line 614
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 620
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v0, :cond_1

    .line 621
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 623
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/sharebox/h;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/plus/sharebox/Circle;)V
    .locals 1

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v0, :cond_0

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/sharebox/h;->a(Lcom/google/android/gms/plus/sharebox/Circle;)V

    .line 639
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 372
    if-eq p1, p0, :cond_0

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 376
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/aq;->l()V

    .line 377
    return-void
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 399
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->i:Z

    return v0
.end method

.method final c()Lcom/google/android/gms/plus/model/posts/Post;
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v1, 0x0

    .line 437
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 471
    :goto_0
    return-object v1

    .line 442
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 445
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/text/Spannable;)Ljava/lang/String;

    move-result-object v4

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->g()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->p:Lcom/google/android/gms/plus/sharebox/a/a;

    iget-object v7, v0, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    .line 448
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    iget-object v8, v0, Lcom/google/android/gms/plus/sharebox/a/b;->a:Landroid/os/Bundle;

    .line 452
    :goto_3
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->d:Z

    if-eqz v0, :cond_a

    .line 453
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v5}, Lcom/google/android/gms/plus/sharebox/as;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/common/analytics/e;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v5

    invoke-virtual {v0, v2, v1, v5, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    move v2, v0

    .line 466
    :goto_4
    new-instance v0, Lcom/google/android/gms/plus/model/posts/Post;

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v5}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v5

    iget-object v9, v5, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v11

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/as;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v12

    move-object v2, v1

    move-object v5, v1

    move-object v6, v1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/plus/model/posts/Post;-><init>(Ljava/lang/String;Ljava/util/List;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V

    .line 470
    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->k:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->l()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->x:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->j:Z

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    if-ne v1, v13, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->E:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->j:Z

    if-nez v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->F:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/bg;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/a/b;->d()Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/u;->y:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    :cond_6
    move-object v1, v0

    .line 471
    goto/16 :goto_0

    :cond_7
    move-object v3, v1

    .line 443
    goto/16 :goto_1

    :cond_8
    move-object v7, v1

    .line 446
    goto/16 :goto_2

    :cond_9
    move-object v8, v1

    .line 448
    goto/16 :goto_3

    :cond_a
    move v2, v13

    goto/16 :goto_4
.end method

.method final d()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->o()Z

    move-result v0

    if-nez v0, :cond_1

    .line 491
    :cond_0
    :goto_0
    return v1

    .line 484
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/a/b;->b()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v0, v2

    .line 487
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->f()Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/a/b;->d()Z

    move-result v3

    if-eqz v3, :cond_6

    move v3, v2

    .line 489
    :goto_2
    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-interface {v4}, Landroid/text/Editable;->length()I

    move-result v4

    if-lez v4, :cond_7

    move v4, v2

    .line 491
    :goto_3
    if-nez v0, :cond_4

    if-nez v3, :cond_4

    if-eqz v4, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0

    :cond_5
    move v0, v1

    .line 484
    goto :goto_1

    :cond_6
    move v3, v1

    .line 487
    goto :goto_2

    :cond_7
    move v4, v1

    .line 489
    goto :goto_3
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/h;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 633
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/h;->a()V

    .line 645
    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/h;->a(Z)V

    .line 651
    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/h;->c()V

    .line 657
    :cond_0
    return-void
.end method

.method public final i()Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v0, :cond_0

    .line 661
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/h;->b()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    .line 663
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 667
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-eqz v1, :cond_0

    .line 668
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/h;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/h;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 671
    :cond_0
    return v0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 675
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->h()Lcom/google/android/gms/plus/sharebox/bh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 676
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->h()Lcom/google/android/gms/plus/sharebox/bh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/bh;->a(Z)V

    .line 678
    :cond_0
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 255
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 256
    if-nez p1, :cond_0

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->s:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setSelection(I)V

    .line 263
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v1}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->d:Z

    .line 265
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->d:Z

    if-eqz v0, :cond_2

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.intent.extra.SHARE_ON_PLUS"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 269
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->rx:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->c:Landroid/widget/CheckBox;

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->b:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->ry:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 274
    :cond_2
    return-void
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 201
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 202
    instance-of v0, p1, Lcom/google/android/gms/plus/sharebox/as;

    if-nez v0, :cond_0

    .line 203
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/as;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 206
    check-cast v0, Lcom/google/android/gms/plus/sharebox/as;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    .line 207
    instance-of v0, p1, Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_1

    .line 208
    check-cast p1, Lcom/google/android/gms/plus/audience/ae;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    .line 210
    :cond_1
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 407
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->bj:I

    if-ne v0, v1, :cond_0

    .line 408
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->d()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->n()V

    .line 413
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 214
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 216
    if-eqz p1, :cond_0

    .line 217
    const-string v0, "logged_expand_sharebox"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->f:Z

    .line 219
    const-string v0, "logged_comment_added"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->g:Z

    .line 220
    const-string v0, "logged_preview_shown"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->h:Z

    .line 221
    const-string v0, "user_edited"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->i:Z

    .line 222
    const-string v0, "saw_domain_restriction"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->j:Z

    .line 224
    const-string v0, "saw_underage_warning"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->k:Z

    .line 225
    const-string v0, "add_to_circle_data"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->o:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    .line 227
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 232
    sget v0, Lcom/google/android/gms/l;->eS:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->lN:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ScrollView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->q:Landroid/widget/ScrollView;

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->bj:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AudienceView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    if-eqz p3, :cond_0

    const-string v0, "audience_view_enabled"

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->setEnabled(Z)V

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->by:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->s:Landroid/widget/ImageView;

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->s:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->Q:I

    invoke-static {v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/ac;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->dk:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    new-instance v1, Lcom/google/android/gms/plus/sharebox/ar;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/plus/sharebox/ar;-><init>(Lcom/google/android/gms/plus/sharebox/aq;Landroid/content/res/Resources;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->lg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->b:Landroid/view/ViewGroup;

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->p:Landroid/view/View;

    return-object v0

    :cond_0
    move v0, v1

    .line 238
    goto :goto_0
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    .line 417
    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->dk:I

    if-ne v0, v1, :cond_0

    .line 418
    packed-switch p2, :pswitch_data_0

    .line 424
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 420
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 421
    const/4 v0, 0x1

    goto :goto_0

    .line 418
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method public final onHiddenChanged(Z)V
    .locals 2

    .prologue
    .line 344
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onHiddenChanged(Z)V

    .line 347
    if-nez p1, :cond_1

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->h()Lcom/google/android/gms/plus/sharebox/bh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/bh;->a(I)V

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->a(Z)V

    .line 355
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;->requestFocusFromTouch()Z

    .line 356
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->a:Lcom/google/android/gms/plus/sharebox/MentionMultiAutoCompleteTextView;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/ae;->a(Landroid/content/Context;Landroid/view/View;)Z

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 368
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->b(Lcom/google/android/gms/plus/audience/bh;)V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 328
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 330
    const-string v0, "logged_expand_sharebox"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->f:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 331
    const-string v0, "logged_comment_added"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 332
    const-string v0, "logged_preview_shown"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 333
    const-string v0, "user_edited"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 334
    const-string v0, "saw_domain_restriction"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->j:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 335
    const-string v0, "saw_underage_warning"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    if-eqz v0, :cond_0

    .line 337
    const-string v0, "audience_view_enabled"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->r:Lcom/google/android/gms/plus/sharebox/AudienceView;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AudienceView;->isEnabled()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 339
    :cond_0
    const-string v0, "add_to_circle_data"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/aq;->o:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 340
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 310
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->f()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/model/posts/Settings;)V

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->g()Lcom/google/android/gms/plus/data/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/data/a/a;)V

    .line 318
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->m()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->f()Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->l()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/aq;->a(Landroid/graphics/Bitmap;)V

    .line 321
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->l:Z

    if-nez v0, :cond_3

    .line 322
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->e()V

    .line 324
    :cond_3
    return-void
.end method

.method public final onViewStateRestored(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 278
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onViewStateRestored(Landroid/os/Bundle;)V

    .line 280
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/aq;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 281
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 283
    const-string v2, "add_to_circle_fragment"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/h;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    if-nez v0, :cond_0

    .line 286
    new-instance v0, Lcom/google/android/gms/plus/sharebox/h;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    .line 287
    sget v0, Lcom/google/android/gms/j;->R:I

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    const-string v3, "add_to_circle_fragment"

    invoke-virtual {v1, v0, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 290
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->l:Lcom/google/android/gms/plus/sharebox/as;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/as;->e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->l()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 294
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->d:Z

    if-eqz v0, :cond_3

    .line 295
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->n:Lcom/google/android/gms/plus/sharebox/h;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 297
    :cond_3
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->d()Z

    move-result v0

    if-nez v0, :cond_4

    .line 298
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->b()I

    .line 301
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    if-eqz v0, :cond_5

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/aq;->m:Lcom/google/android/gms/plus/audience/ae;

    invoke-interface {v0}, Lcom/google/android/gms/plus/audience/ae;->g()Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 306
    :cond_5
    return-void
.end method
