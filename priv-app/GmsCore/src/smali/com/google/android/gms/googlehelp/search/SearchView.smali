.class public Lcom/google/android/gms/googlehelp/search/SearchView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/googlehelp/search/o;


# instance fields
.field a:Landroid/view/View$OnKeyListener;

.field private final c:Landroid/content/Intent;

.field private d:Lcom/google/android/gms/googlehelp/search/p;

.field private e:Lcom/google/android/gms/googlehelp/search/q;

.field private f:Landroid/support/v4/widget/f;

.field private g:Landroid/view/View;

.field private h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Z

.field private n:I

.field private o:Z

.field private p:Ljava/lang/CharSequence;

.field private q:Ljava/lang/CharSequence;

.field private r:Ljava/lang/Runnable;

.field private s:Ljava/lang/Runnable;

.field private t:Ljava/lang/Runnable;

.field private final u:Landroid/view/View$OnClickListener;

.field private final v:Landroid/widget/TextView$OnEditorActionListener;

.field private final w:Landroid/widget/AdapterView$OnItemClickListener;

.field private final x:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private y:Landroid/text/TextWatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/gms/googlehelp/search/o;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/search/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->b:Lcom/google/android/gms/googlehelp/search/o;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 165
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 168
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    new-instance v0, Lcom/google/android/gms/googlehelp/search/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/d;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->r:Ljava/lang/Runnable;

    .line 113
    new-instance v0, Lcom/google/android/gms/googlehelp/search/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/g;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->s:Ljava/lang/Runnable;

    .line 119
    new-instance v0, Lcom/google/android/gms/googlehelp/search/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/h;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->t:Ljava/lang/Runnable;

    .line 461
    new-instance v0, Lcom/google/android/gms/googlehelp/search/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/k;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->u:Landroid/view/View$OnClickListener;

    .line 479
    new-instance v0, Lcom/google/android/gms/googlehelp/search/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/l;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->a:Landroid/view/View$OnKeyListener;

    .line 562
    new-instance v0, Lcom/google/android/gms/googlehelp/search/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/m;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->v:Landroid/widget/TextView$OnEditorActionListener;

    .line 669
    new-instance v0, Lcom/google/android/gms/googlehelp/search/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/n;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->w:Landroid/widget/AdapterView$OnItemClickListener;

    .line 680
    new-instance v0, Lcom/google/android/gms/googlehelp/search/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/e;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->x:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 846
    new-instance v0, Lcom/google/android/gms/googlehelp/search/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/search/f;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->y:Landroid/text/TextWatcher;

    .line 170
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 172
    sget v3, Lcom/google/android/gms/l;->cp:I

    invoke-virtual {v0, v3, p0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 174
    sget v0, Lcom/google/android/gms/j;->iZ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->a(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    .line 177
    sget v0, Lcom/google/android/gms/j;->iY:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->g:Landroid/view/View;

    .line 178
    sget v0, Lcom/google/android/gms/j;->jf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->j:Landroid/view/View;

    .line 179
    sget v0, Lcom/google/android/gms/j;->iW:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->i:Landroid/widget/ImageView;

    .line 180
    sget v0, Lcom/google/android/gms/j;->ja:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->k:Landroid/view/View;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->i:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->k:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->u:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setThreshold(I)V

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->y:Landroid/text/TextWatcher;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->v:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->w:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->x:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->a:Landroid/view/View$OnKeyListener;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 193
    sget-object v0, Lcom/google/android/gms/r;->at:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 194
    sget v3, Lcom/google/android/gms/r;->au:I

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    .line 195
    if-eq v3, v4, :cond_0

    .line 196
    iput v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->n:I

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->requestLayout()V

    .line 199
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 201
    sget-object v0, Lcom/google/android/gms/r;->aK:[I

    invoke-virtual {p1, p2, v0, v2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 204
    sget v3, Lcom/google/android/gms/r;->aL:I

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v3

    .line 205
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 206
    invoke-virtual {p0, v3}, Lcom/google/android/gms/googlehelp/search/SearchView;->setFocusable(Z)V

    .line 208
    new-instance v0, Landroid/content/Intent;

    const-string v3, "android.speech.action.RECOGNIZE_SPEECH"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->c:Landroid/content/Intent;

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->c:Landroid/content/Intent;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->c:Landroid/content/Intent;

    const/high16 v4, 0x10000

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->o:Z

    .line 210
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->o:Z

    if-eqz v0, :cond_1

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    const-string v1, "nm"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 216
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->f()V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->clearFocus()V

    .line 218
    return-void

    :cond_2
    move v0, v2

    .line 209
    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;ILjava/lang/String;)Landroid/content/Intent;
    .locals 5

    .prologue
    .line 818
    :try_start_0
    const-string v0, "suggest_intent_query"

    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/search/r;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 819
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.SEARCH"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v2, "user_query"

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->q:Ljava/lang/CharSequence;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    if-eqz v0, :cond_0

    const-string v2, "query"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getComponentName()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 829
    :goto_0
    return-object v0

    .line 820
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 823
    :try_start_1
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v0

    .line 827
    :goto_1
    const-string v2, "GOOGLEHELP_SearchView"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Search suggestions cursor at row "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " returned exception."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 829
    const/4 v0, 0x0

    goto :goto_0

    .line 825
    :catch_1
    move-exception v0

    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->hasFocus()Z

    move-result v1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->FOCUSED_STATE_SET:[I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v1, :cond_1

    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->FOCUSED_STATE_SET:[I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->invalidate()V

    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->EMPTY_STATE_SET:[I

    goto :goto_0

    :cond_1
    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->EMPTY_STATE_SET:[I

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/search/SearchView;Ljava/lang/CharSequence;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 68
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->q:Ljava/lang/CharSequence;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-nez v2, :cond_1

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->h()V

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->g()V

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->p:Ljava/lang/CharSequence;

    return-void

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 769
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 771
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 772
    return-void

    .line 771
    :cond_0
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    goto :goto_0
.end method

.method private a(Z)V
    .locals 3

    .prologue
    .line 448
    if-eqz p1, :cond_1

    .line 449
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->r:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 451
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->r:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 452
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 455
    if-eqz v0, :cond_0

    .line 456
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0
.end method

.method private a(ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/16 v2, 0x15

    const/4 v1, 0x0

    .line 515
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    if-nez v0, :cond_1

    .line 549
    :cond_0
    :goto_0
    return v1

    .line 518
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/support/v4/view/z;->b(Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    const/16 v0, 0x42

    if-eq p1, v0, :cond_2

    const/16 v0, 0x54

    if-eq p1, v0, :cond_2

    const/16 v0, 0x3d

    if-ne p1, v0, :cond_3

    .line 523
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getListSelection()I

    move-result v0

    .line 524
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->b(I)Z

    move-result v1

    goto :goto_0

    .line 529
    :cond_3
    if-eq p1, v2, :cond_4

    const/16 v0, 0x16

    if-ne p1, v0, :cond_6

    .line 532
    :cond_4
    if-ne p1, v2, :cond_5

    move v0, v1

    .line 534
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setListSelection(I)V

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->clearListSelection()V

    .line 537
    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->b:Lcom/google/android/gms/googlehelp/search/o;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/search/o;->a(Landroid/widget/AutoCompleteTextView;)V

    .line 539
    const/4 v1, 0x1

    goto :goto_0

    .line 532
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->length()I

    move-result v0

    goto :goto_1

    .line 543
    :cond_6
    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getListSelection()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0
.end method

.method static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 839
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/search/SearchView;I)Z
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/search/SearchView;->b(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/search/SearchView;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/search/SearchView;)Landroid/support/v4/widget/f;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/search/SearchView;I)V
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    invoke-virtual {v1}, Landroid/support/v4/widget/f;->a()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    invoke-virtual {v2, v1}, Landroid/support/v4/widget/f;->c(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 559
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->k:Landroid/view/View;

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->o:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 560
    return-void

    .line 559
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private b(I)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 655
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->e:Lcom/google/android/gms/googlehelp/search/q;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->e:Lcom/google/android/gms/googlehelp/search/q;

    invoke-interface {v1, p1}, Lcom/google/android/gms/googlehelp/search/q;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    .line 657
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    invoke-virtual {v1}, Landroid/support/v4/widget/f;->a()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Landroid/database/Cursor;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    if-eqz v2, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 658
    :cond_1
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Z)V

    .line 659
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->dismissDropDown()V

    .line 660
    const/4 v0, 0x1

    .line 662
    :cond_2
    return v0

    .line 657
    :catch_0
    move-exception v1

    const-string v3, "GOOGLEHELP_SearchView"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed launch activity: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 4

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getDropDownBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, 0x0

    sub-int v2, v0, v2

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setDropDownHorizontalOffset(I)V

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->l:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget v3, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v3

    iget v1, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x0

    sub-int v0, v1, v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setDropDownWidth(I)V

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/googlehelp/search/SearchView;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->i:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic d()Lcom/google/android/gms/googlehelp/search/o;
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->b:Lcom/google/android/gms/googlehelp/search/o;

    return-object v0
.end method

.method private e()I
    .locals 2

    .prologue
    .line 403
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->a:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->requestFocus()Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Z)V

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/googlehelp/search/SearchView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->k:Landroid/view/View;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 411
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->h()V

    .line 412
    if-nez v0, :cond_1

    :goto_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->b(Z)V

    .line 413
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->g()V

    .line 414
    return-void

    :cond_0
    move v0, v2

    .line 409
    goto :goto_0

    :cond_1
    move v1, v2

    .line 412
    goto :goto_1
.end method

.method private g()V
    .locals 2

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 420
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->j:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 421
    return-void

    .line 417
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 4

    .prologue
    .line 68
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->c:Landroid/content/Intent;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v2, "android.speech.extra.LANGUAGE_MODEL"

    const-string v3, "free_form"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.speech.extra.PROMPT"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.speech.extra.MAX_RESULTS"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "calling_package"

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/16 v2, 0x6f

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GOOGLEHELP_SearchView"

    const-string v1, "Could not find voice search activity"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic h(Lcom/google/android/gms/googlehelp/search/SearchView;)Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 425
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 426
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->i:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->ENABLED_STATE_SET:[I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 427
    return-void

    :cond_0
    move v0, v1

    .line 424
    goto :goto_0

    .line 425
    :cond_1
    const/16 v1, 0x8

    goto :goto_1

    .line 426
    :cond_2
    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->EMPTY_STATE_SET:[I

    goto :goto_2
.end method

.method private i()V
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->s:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 431
    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->k()V

    return-void
.end method

.method private j()V
    .locals 2

    .prologue
    .line 584
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    .line 585
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->getTrimmedLength(Ljava/lang/CharSequence;)I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->d:Lcom/google/android/gms/googlehelp/search/p;

    if-eqz v1, :cond_0

    .line 587
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->d:Lcom/google/android/gms/googlehelp/search/p;

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/googlehelp/search/p;->a(Ljava/lang/String;)Z

    .line 589
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->j()V

    return-void
.end method

.method private k()V
    .locals 3

    .prologue
    .line 834
    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->b:Lcom/google/android/gms/googlehelp/search/o;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/search/o;->a:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/googlehelp/search/o;->a:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 835
    :cond_0
    :goto_0
    sget-object v0, Lcom/google/android/gms/googlehelp/search/SearchView;->b:Lcom/google/android/gms/googlehelp/search/o;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/search/o;->b:Ljava/lang/reflect/Method;

    if-eqz v2, :cond_1

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/googlehelp/search/o;->b:Ljava/lang/reflect/Method;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    .line 836
    :cond_1
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/android/gms/googlehelp/search/SearchView;)V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Z)V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setDropDownAnchor(I)V

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getDropDownAnchor()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->l:Landroid/view/View;

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 225
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->l:Landroid/view/View;

    new-instance v1, Lcom/google/android/gms/googlehelp/search/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/search/i;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 231
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->l:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/googlehelp/search/j;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/search/j;-><init>(Lcom/google/android/gms/googlehelp/search/SearchView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public final a(Landroid/support/v4/widget/f;)V
    .locals 2

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 341
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/search/p;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->d:Lcom/google/android/gms/googlehelp/search/p;

    .line 296
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/search/q;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->e:Lcom/google/android/gms/googlehelp/search/q;

    .line 305
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 2

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setText(Ljava/lang/CharSequence;)V

    .line 326
    if-eqz p1, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->setSelection(I)V

    .line 328
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->q:Ljava/lang/CharSequence;

    .line 332
    :cond_0
    if-eqz p2, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 333
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->j()V

    .line 335
    :cond_1
    return-void
.end method

.method public final b()Landroid/support/v4/widget/f;
    .locals 1

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->f:Landroid/support/v4/widget/f;

    return-object v0
.end method

.method final c()V
    .locals 1

    .prologue
    .line 623
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->f()V

    .line 626
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->i()V

    .line 627
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 628
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->k()V

    .line 630
    :cond_0
    return-void
.end method

.method public clearFocus()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 281
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->m:Z

    .line 282
    invoke-direct {p0, v1}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Z)V

    .line 283
    invoke-super {p0}, Landroid/widget/LinearLayout;->clearFocus()V

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->clearFocus()V

    .line 285
    iput-boolean v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->m:Z

    .line 286
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->s:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->t:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->post(Ljava/lang/Runnable;)Z

    .line 444
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    .line 445
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 375
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 376
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 378
    sparse-switch v1, :sswitch_data_0

    .line 398
    :cond_0
    :goto_0
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v0, p2}, Landroid/widget/LinearLayout;->onMeasure(II)V

    .line 400
    return-void

    .line 381
    :sswitch_0
    iget v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->n:I

    if-lez v1, :cond_1

    .line 382
    iget v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->n:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 384
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->e()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 389
    :sswitch_1
    iget v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->n:I

    if-lez v1, :cond_0

    .line 390
    iget v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->n:I

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    .line 395
    :sswitch_2
    iget v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->n:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->n:I

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->e()I

    move-result v0

    goto :goto_0

    .line 378
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_2
        0x40000000 -> :sswitch_1
    .end sparse-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    .prologue
    .line 634
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    .line 636
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->i()V

    .line 637
    return-void
.end method

.method public requestFocus(ILandroid/graphics/Rect;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 267
    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->m:Z

    if-eqz v1, :cond_1

    .line 275
    :cond_0
    :goto_0
    return v0

    .line 269
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->isFocusable()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/SearchView;->h:Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/googlehelp/search/SearchView$SearchAutoComplete;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 272
    if-eqz v0, :cond_0

    .line 273
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->f()V

    goto :goto_0
.end method
