.class public abstract Lcom/google/android/gms/common/ui/widget/e;
.super Lcom/google/android/gms/common/ui/widget/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/ui/widget/a;

.field private final c:Lcom/google/android/gms/common/data/d;

.field private final d:Ljava/lang/Integer;

.field private final e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/ui/widget/a;Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 356
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/gms/common/ui/widget/e;-><init>(Lcom/google/android/gms/common/ui/widget/a;Lcom/google/android/gms/common/data/d;Ljava/lang/Integer;Ljava/lang/Integer;)V

    .line 357
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/ui/widget/a;Lcom/google/android/gms/common/data/d;Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 361
    iput-object p1, p0, Lcom/google/android/gms/common/ui/widget/e;->a:Lcom/google/android/gms/common/ui/widget/a;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/c;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    .line 362
    if-nez p3, :cond_0

    .line 363
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    .line 366
    :cond_0
    if-nez p4, :cond_1

    .line 367
    if-nez p2, :cond_2

    .line 368
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    .line 374
    :cond_1
    :goto_0
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v1

    :goto_1
    const-string v3, "rangeStart"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 375
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p2}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v3

    if-gt v0, v3, :cond_4

    :goto_2
    const-string v0, "rangeEnd"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 377
    iput-object p2, p0, Lcom/google/android/gms/common/ui/widget/e;->c:Lcom/google/android/gms/common/data/d;

    .line 378
    iput-object p3, p0, Lcom/google/android/gms/common/ui/widget/e;->d:Ljava/lang/Integer;

    .line 379
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/widget/e;->e:Ljava/lang/Integer;

    .line 380
    return-void

    .line 370
    :cond_2
    invoke-interface {p2}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p4

    goto :goto_0

    :cond_3
    move v0, v2

    .line 374
    goto :goto_1

    :cond_4
    move v1, v2

    .line 375
    goto :goto_2
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/e;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/ui/widget/e;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3, p4}, Lcom/google/android/gms/common/ui/widget/e;->a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 389
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/e;->c:Lcom/google/android/gms/common/data/d;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/e;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
