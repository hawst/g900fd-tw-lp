.class public final Lcom/google/android/gms/people/service/a/f;
.super Lcom/google/android/gms/people/service/a/d;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/List;

.field private final i:Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;)V
    .locals 9

    .prologue
    .line 83
    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/f;->g:Ljava/lang/String;

    .line 86
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/f;->h:Ljava/util/List;

    .line 87
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/f;->i:Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    .line 88
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Landroid/content/Context;Lcom/google/android/gms/people/a/b;)Lcom/google/android/gms/people/service/g;
    .locals 1

    .prologue
    .line 157
    invoke-static {p0, p1}, Lcom/google/android/gms/people/service/g;->a(Landroid/content/Context;Lcom/google/android/gms/people/a/b;)Lcom/google/android/gms/people/service/g;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/people/service/a/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/f;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/people/service/a/f;)Ljava/util/List;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/f;->h:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/people/service/a/f;)Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/f;->i:Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/people/service/a/f;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 75
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v0, 0x0

    .line 93
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 94
    const-string v2, "response_complete"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 97
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    .line 99
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/f;->i:Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 100
    iget-object v3, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 101
    const-string v3, "PeopleService"

    const-string v4, "Can not query gaia map, no account provided"

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/f;->i:Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 108
    iget-object v3, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 109
    const-string v3, "PeopleService"

    const-string v4, "Can not query database, no account provided"

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/f;->i:Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 116
    new-instance v3, Lcom/google/android/gms/people/service/a/j;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/people/service/a/j;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v3, Lcom/google/android/gms/people/service/a/l;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/people/service/a/l;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 118
    new-instance v3, Lcom/google/android/gms/people/service/a/k;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/people/service/a/k;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 119
    new-instance v3, Lcom/google/android/gms/people/service/a/m;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/people/service/a/m;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_2
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 124
    const-string v3, "PeopleService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Running "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " tasks"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    :cond_3
    :goto_2
    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_9

    .line 130
    iget-boolean v3, p0, Lcom/google/android/gms/people/service/a;->e:Z

    if-eqz v3, :cond_6

    .line 131
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->e:Lcom/google/android/gms/people/service/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 150
    :goto_3
    return-object v0

    .line 103
    :cond_4
    new-instance v3, Lcom/google/android/gms/people/service/a/h;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/people/service/a/h;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_5
    new-instance v3, Lcom/google/android/gms/people/service/a/i;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/people/service/a/i;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    invoke-interface {v2, v3}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 134
    :cond_6
    if-eqz v0, :cond_7

    .line 135
    sget-object v0, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/people/service/b;Landroid/os/Bundle;)V

    .line 138
    :cond_7
    invoke-interface {v2}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/a/q;

    .line 140
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 141
    const-string v3, "PeopleService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Running task: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :cond_8
    invoke-interface {v0, p1, p3, v1}, Lcom/google/android/gms/people/service/a/q;->a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Landroid/os/Bundle;)Z

    move-result v0

    goto :goto_2

    .line 148
    :cond_9
    const-string v0, "response_complete"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 150
    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_3
.end method
