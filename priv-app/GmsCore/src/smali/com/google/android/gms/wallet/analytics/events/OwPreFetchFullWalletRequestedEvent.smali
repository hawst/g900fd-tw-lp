.class public Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field public final d:Ljava/lang/String;

.field public final e:D

.field public final f:D

.field public final g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/q;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 67
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->d:Ljava/lang/String;

    .line 69
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->e:D

    .line 70
    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->f:D

    .line 71
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->g:J

    .line 72
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;DDJ)V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 55
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->j:Ljava/lang/String;

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->a:Ljava/lang/String;

    .line 57
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->d:Ljava/lang/String;

    .line 59
    iput-wide p4, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->e:D

    .line 60
    iput-wide p6, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->f:D

    .line 61
    iput-wide p8, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->g:J

    .line 62
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    .line 63
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;DDJ)Ljava/lang/String;
    .locals 10

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-wide/from16 v6, p6

    move-wide/from16 v8, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;DDJ)V

    .line 48
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 49
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 76
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 79
    iget-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->e:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 80
    iget-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->f:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    .line 81
    iget-wide v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 82
    return-void
.end method
