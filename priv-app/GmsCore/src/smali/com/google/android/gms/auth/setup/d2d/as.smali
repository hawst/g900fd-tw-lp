.class final Lcom/google/android/gms/auth/setup/d2d/as;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/setup/d2d/aq;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/setup/d2d/aq;)V
    .locals 0

    .prologue
    .line 95
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->a(Lcom/google/android/gms/auth/setup/d2d/aq;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/at;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/at;-><init>(Lcom/google/android/gms/auth/setup/d2d/as;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/aq;->a(Lcom/google/android/gms/auth/setup/d2d/aq;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.bluetooth.adapter.action.STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.nfc.action.ADAPTER_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v1}, Lcom/google/android/gms/auth/setup/d2d/aq;->d(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v2}, Lcom/google/android/gms/auth/setup/d2d/aq;->c(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/content/BroadcastReceiver;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->e(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->e(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->enable()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/aq;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Enabling Bluetooth."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->f(Lcom/google/android/gms/auth/setup/d2d/aq;)Z

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->h(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->h(Lcom/google/android/gms/auth/setup/d2d/aq;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/nfc/NfcAdapter;->enable()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/aq;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Enabling NFC."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->i(Lcom/google/android/gms/auth/setup/d2d/aq;)Z

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :cond_2
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/aq;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Could not enable Bluetooth."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->g(Lcom/google/android/gms/auth/setup/d2d/aq;)Lcom/google/android/gms/auth/setup/d2d/aw;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/aw;->j()V

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/aq;->b()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Could not enable NFC."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/as;->a:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->g(Lcom/google/android/gms/auth/setup/d2d/aq;)Lcom/google/android/gms/auth/setup/d2d/aw;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/aw;->j()V

    goto :goto_0
.end method
