.class public final Lcom/google/android/gms/social/location/an;
.super Lcom/google/android/gms/social/location/ad;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/social/location/ad;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/social/location/TimeShare;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    new-instance v0, Lcom/google/ac/e/a/a/b;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/b;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->b()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->e()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v0, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    .line 31
    new-instance v3, Lcom/google/ac/e/a/a/l;

    invoke-direct {v3}, Lcom/google/ac/e/a/a/l;-><init>()V

    .line 32
    new-array v4, v1, [Lcom/google/ac/e/a/a/b;

    aput-object v0, v4, v2

    iput-object v4, v3, Lcom/google/ac/e/a/a/l;->a:[Lcom/google/ac/e/a/a/b;

    .line 34
    new-instance v0, Lcom/google/ac/e/a/a/m;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/m;-><init>()V

    const-string v4, "updaterenderedtimeshares"

    invoke-virtual {p0, v3, v0, v4}, Lcom/google/android/gms/social/location/an;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;Ljava/lang/String;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/e/a/a/m;

    .line 37
    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    move v0, v2

    goto :goto_0
.end method
