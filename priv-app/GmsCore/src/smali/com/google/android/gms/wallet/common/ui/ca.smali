.class public final Lcom/google/android/gms/wallet/common/ui/ca;
.super Lcom/google/android/gms/common/ui/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/a;-><init>()V

    return-void
.end method

.method public static b()Lcom/google/android/gms/wallet/common/ui/ca;
    .locals 4

    .prologue
    .line 28
    sget v0, Lcom/google/android/gms/p;->Cd:I

    sget v1, Lcom/google/android/gms/p;->BW:I

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "titleId"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "bodyId"

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ca;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/ca;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ca;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ca;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 45
    const-string v1, "titleId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 46
    const-string v2, "bodyId"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ca;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lcom/google/android/gms/l;->hv:I

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ClickSpan;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 48
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ca;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 51
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/ca;->a(Z)V

    .line 52
    sget v1, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v0, v1, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
