.class public Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Landroid/view/View;

.field private b:Landroid/widget/TextView;

.field private c:Lcom/google/android/gms/smart_profile/card/h;

.field private final d:Lcom/google/android/gms/smart_profile/card/view/i;

.field private e:Z

.field private f:I

.field private g:I

.field private h:I

.field private i:Ljava/util/List;

.field private j:Landroid/widget/LinearLayout;

.field private k:I

.field private l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

.field private m:Lcom/google/android/gms/common/api/v;

.field private n:Landroid/support/v4/app/au;

.field private o:Lcom/google/android/gms/smart_profile/card/a/c;

.field private p:I

.field private q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 350
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 262
    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/smart_profile/card/view/i;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;B)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d:Lcom/google/android/gms/smart_profile/card/view/i;

    .line 263
    iput-boolean v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    .line 351
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b()V

    .line 352
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 355
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 262
    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/smart_profile/card/view/i;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;B)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d:Lcom/google/android/gms/smart_profile/card/view/i;

    .line 263
    iput-boolean v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    .line 356
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b()V

    .line 357
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 360
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 262
    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/i;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/smart_profile/card/view/i;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;B)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d:Lcom/google/android/gms/smart_profile/card/view/i;

    .line 263
    iput-boolean v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    .line 361
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b()V

    .line 362
    return-void
.end method

.method private a(Ljava/util/List;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 426
    move v1, v0

    move v2, v0

    .line 427
    :goto_0
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 428
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/view/g;

    .line 429
    iget-object v3, v0, Lcom/google/android/gms/smart_profile/card/view/g;->n:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 430
    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/g;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->g:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 427
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 433
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 436
    :cond_1
    return v2
.end method

.method private a(III)Landroid/animation/ValueAnimator;
    .locals 2

    .prologue
    .line 880
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 881
    new-instance v1, Lcom/google/android/gms/smart_profile/card/view/e;

    invoke-direct {v1, p0, p3}, Lcom/google/android/gms/smart_profile/card/view/e;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 888
    new-instance v1, Lcom/google/android/gms/smart_profile/card/view/f;

    invoke-direct {v1, p0, p3}, Lcom/google/android/gms/smart_profile/card/view/f;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;I)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 898
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;III)Landroid/animation/ValueAnimator;
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(III)Landroid/animation/ValueAnimator;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 962
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->k:I

    invoke-static {v0, p1}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;Lcom/google/android/gms/smart_profile/card/view/g;)Landroid/view/View;
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 629
    sget v0, Lcom/google/android/gms/l;->fb:I

    invoke-virtual {p1, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 633
    iget-object v7, p2, Lcom/google/android/gms/smart_profile/card/view/g;->n:Ljava/util/List;

    .line 634
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/smart_profile/card/view/g;

    .line 637
    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/g;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_0
    invoke-direct {p0, p1, v1, v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/smart_profile/card/view/g;I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    move v4, v5

    .line 639
    :goto_1
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 640
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/smart_profile/card/view/g;

    .line 641
    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/card/view/g;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v6, 0x4

    :goto_2
    invoke-direct {p0, p1, v2, v6}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/smart_profile/card/view/g;I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 639
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_0
    move v2, v3

    .line 637
    goto :goto_0

    :cond_1
    move v6, v3

    .line 641
    goto :goto_2

    .line 644
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/g;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 645
    sget v1, Lcom/google/android/gms/k;->E:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 647
    :cond_3
    sget v1, Lcom/google/android/gms/k;->C:I

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->setTag(ILjava/lang/Object;)V

    .line 648
    return-object v0
.end method

.method private a(Landroid/view/LayoutInflater;Lcom/google/android/gms/smart_profile/card/view/g;I)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v11, 0x1

    const/16 v10, 0x8

    .line 653
    sget v0, Lcom/google/android/gms/l;->fc:I

    invoke-virtual {p1, v0, p0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 655
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->p:Ljava/lang/String;

    invoke-virtual {v6, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 658
    :cond_0
    sget v0, Lcom/google/android/gms/j;->jV:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 659
    iget-boolean v4, p2, Lcom/google/android/gms/smart_profile/card/view/g;->o:Z

    .line 660
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_9

    .line 661
    if-eqz v4, :cond_8

    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 663
    sget v0, Lcom/google/android/gms/k;->E:I

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 673
    :cond_1
    :goto_1
    invoke-virtual {v2, p3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 675
    sget v0, Lcom/google/android/gms/j;->jw:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 676
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->c:Ljava/lang/String;

    if-eqz v1, :cond_a

    .line 677
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 682
    :goto_2
    sget v0, Lcom/google/android/gms/j;->se:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 683
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->d:Ljava/lang/String;

    if-eqz v1, :cond_b

    .line 684
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 685
    iget-boolean v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->j:Z

    if-eqz v1, :cond_2

    .line 686
    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setSingleLine(Z)V

    .line 687
    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 688
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 694
    :cond_2
    :goto_3
    sget v0, Lcom/google/android/gms/j;->jX:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 695
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_c

    .line 696
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 701
    :goto_4
    sget v0, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 702
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->f:Ljava/lang/String;

    if-eqz v1, :cond_d

    .line 703
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 708
    :goto_5
    sget v0, Lcom/google/android/gms/j;->jY:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 709
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->g:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_e

    .line 710
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 715
    :goto_6
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->h:Landroid/content/Intent;

    if-eqz v0, :cond_3

    .line 716
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d:Lcom/google/android/gms/smart_profile/card/view/i;

    invoke-virtual {v6, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 717
    sget v0, Lcom/google/android/gms/k;->D:I

    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->h:Landroid/content/Intent;

    invoke-virtual {v6, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 718
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    if-eqz v0, :cond_3

    .line 719
    sget v0, Lcom/google/android/gms/k;->B:I

    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v6, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 723
    :cond_3
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->k:Landroid/graphics/drawable/Drawable;

    .line 724
    if-eqz v1, :cond_7

    .line 725
    sget v0, Lcom/google/android/gms/j;->az:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 726
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 727
    sget v0, Lcom/google/android/gms/j;->ay:I

    invoke-virtual {v6, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 728
    if-eqz v4, :cond_4

    invoke-direct {p0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_4
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 730
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->q:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 731
    iget-object v1, p2, Lcom/google/android/gms/smart_profile/card/view/g;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 733
    :cond_5
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->l:Landroid/content/Intent;

    .line 734
    if-eqz v0, :cond_6

    .line 735
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d:Lcom/google/android/gms/smart_profile/card/view/i;

    invoke-virtual {v2, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 736
    sget v1, Lcom/google/android/gms/k;->A:I

    invoke-virtual {v6, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 738
    :cond_6
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 739
    if-eqz v0, :cond_7

    .line 740
    sget v1, Lcom/google/android/gms/k;->z:I

    invoke-virtual {v6, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 744
    :cond_7
    return-object v6

    .line 661
    :cond_8
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->a:Landroid/graphics/drawable/Drawable;

    goto/16 :goto_0

    .line 664
    :cond_9
    iget-object v0, p2, Lcom/google/android/gms/smart_profile/card/view/g;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->m:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->n:Landroid/support/v4/app/au;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->o:Lcom/google/android/gms/smart_profile/card/a/c;

    if-eqz v0, :cond_1

    .line 668
    iget-object v7, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->n:Landroid/support/v4/app/au;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->o:Lcom/google/android/gms/smart_profile/card/a/c;

    invoke-interface {v0}, Lcom/google/android/gms/smart_profile/card/a/c;->b()I

    move-result v8

    const/4 v9, 0x0

    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/j;

    iget-object v3, p2, Lcom/google/android/gms/smart_profile/card/view/g;->b:Ljava/lang/String;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/card/view/j;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/widget/ImageView;Ljava/lang/String;ZB)V

    invoke-virtual {v7, v8, v9, v0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 671
    sget v0, Lcom/google/android/gms/k;->E:I

    invoke-static {v11}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v6, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_1

    .line 679
    :cond_a
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 691
    :cond_b
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 698
    :cond_c
    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_4

    .line 705
    :cond_d
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    .line 712
    :cond_e
    invoke-virtual {v0, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_6
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Landroid/view/View;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    return-object v0
.end method

.method private a(II)V
    .locals 2

    .prologue
    .line 902
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 903
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 904
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 905
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 912
    :goto_0
    return-void

    .line 907
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 909
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 910
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, p2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Z)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 511
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 512
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 513
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/f;->ar:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 515
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    .line 516
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 517
    sget v0, Lcom/google/android/gms/g;->bR:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 519
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v4/f/f;->a(Ljava/util/Locale;)I

    move-result v0

    const/4 v5, 0x1

    if-ne v0, v5, :cond_2

    .line 521
    sget v0, Lcom/google/android/gms/k;->E:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/g;->bN:I

    :goto_0
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    .line 526
    sget v0, Lcom/google/android/gms/g;->bM:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 537
    :goto_1
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 538
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 543
    :cond_0
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 544
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 545
    const/4 v1, -0x2

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 546
    invoke-virtual {p1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 551
    sget v0, Lcom/google/android/gms/k;->C:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_5

    if-eqz p2, :cond_5

    move-object v0, p1

    .line 553
    check-cast v0, Landroid/view/ViewGroup;

    .line 554
    iget v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->g:I

    :goto_2
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_5

    .line 556
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    iget-boolean v2, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    if-eqz v2, :cond_4

    move v2, v3

    :goto_3
    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    .line 555
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 521
    :cond_1
    sget v0, Lcom/google/android/gms/g;->bM:I

    goto :goto_0

    .line 529
    :cond_2
    sget v0, Lcom/google/android/gms/k;->E:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_3

    sget v0, Lcom/google/android/gms/g;->bN:I

    :goto_4
    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 534
    sget v0, Lcom/google/android/gms/g;->bM:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    goto :goto_1

    .line 529
    :cond_3
    sget v0, Lcom/google/android/gms/g;->bM:I

    goto :goto_4

    .line 556
    :cond_4
    const/16 v2, 0x8

    goto :goto_3

    .line 559
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 560
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;II)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(II)V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 748
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/h;->cL:I

    .line 750
    :goto_0
    iget v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->k:I

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 752
    invoke-static {}, Lcom/google/android/gms/common/util/ag;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 753
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v3, v3, v0, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 759
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 760
    return-void

    .line 748
    :cond_0
    sget v0, Lcom/google/android/gms/h;->cQ:I

    goto :goto_0

    .line 756
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0, v3, v3, v3}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method private a(Ljava/util/List;IZLandroid/net/Uri;Lcom/google/android/gms/common/api/v;Landroid/support/v4/app/au;Lcom/google/android/gms/smart_profile/card/a/c;I)V
    .locals 7

    .prologue
    const/16 v4, 0x8

    const/4 v2, 0x0

    .line 401
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    :goto_0
    return-void

    .line 404
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 405
    iput-boolean p3, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    .line 406
    iput p8, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->k:I

    .line 407
    iput-object p5, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->m:Lcom/google/android/gms/common/api/v;

    .line 408
    iput-object p6, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->n:Landroid/support/v4/app/au;

    .line 409
    iput-object p7, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->o:Lcom/google/android/gms/smart_profile/card/a/c;

    .line 410
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v2

    :goto_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/view/g;

    iget-object v3, v0, Lcom/google/android/gms/smart_profile/card/view/g;->n:Ljava/util/List;

    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-direct {p0, v5, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/smart_profile/card/view/g;)Landroid/view/View;

    move-result-object v0

    :goto_2
    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/view/g;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    move v3, v2

    :goto_3
    invoke-direct {p0, v5, v0, v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/view/LayoutInflater;Lcom/google/android/gms/smart_profile/card/view/g;I)Landroid/view/View;

    move-result-object v0

    goto :goto_2

    :cond_2
    move v3, v4

    goto :goto_3

    :cond_3
    iput-object v6, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->i:Ljava/util/List;

    .line 411
    if-eqz p4, :cond_4

    .line 412
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->i:Ljava/util/List;

    sget v0, Lcom/google/android/gms/l;->fc:I

    invoke-virtual {v5, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/gms/j;->se:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->jw:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v6, Lcom/google/android/gms/p;->ww:I

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v4, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->k:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/c;

    invoke-direct {v0, p0, v3, p4}, Lcom/google/android/gms/smart_profile/card/view/c;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/view/View;Landroid/net/Uri;)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 414
    :cond_4
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f:I

    .line 415
    iput p2, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->g:I

    .line 416
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->q:I

    .line 417
    invoke-static {p1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b(Ljava/util/List;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->p:I

    .line 418
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f:I

    mul-int/lit8 v0, v0, 0x2

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h:I

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    if-nez v0, :cond_5

    .line 420
    sget v0, Lcom/google/android/gms/l;->eY:I

    invoke-virtual {v5, v0, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->k:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/lang/CharSequence;)V

    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d:Lcom/google/android/gms/smart_profile/card/view/i;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 422
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c()V

    goto/16 :goto_0

    .line 420
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method

.method static synthetic a(I)Z
    .locals 1

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c(I)Z

    move-result v0

    return v0
.end method

.method private b(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 850
    const/4 v1, -0x1

    if-ne p1, v1, :cond_2

    .line 851
    iget v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f:I

    if-nez v1, :cond_1

    .line 870
    :cond_0
    :goto_0
    return v0

    .line 854
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->i:Ljava/util/List;

    iget v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f:I

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 855
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0

    .line 857
    :cond_2
    iget v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h:I

    if-ge p1, v1, :cond_0

    .line 860
    invoke-static {p1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 861
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    goto :goto_0

    .line 863
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 865
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    .line 866
    sget v2, Lcom/google/android/gms/k;->C:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    iget v2, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->g:I

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    mul-int/2addr v0, v2

    div-int/2addr v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    goto :goto_0
.end method

.method private static b(Ljava/util/List;)I
    .locals 4

    .prologue
    .line 440
    const/4 v0, 0x0

    .line 441
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/view/g;

    .line 442
    iget-object v3, v0, Lcom/google/android/gms/smart_profile/card/view/g;->n:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 443
    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/g;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_0

    .line 445
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 447
    goto :goto_0

    .line 448
    :cond_1
    return v1
.end method

.method private b()V
    .locals 5

    .prologue
    .line 468
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->setOrientation(I)V

    .line 469
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 470
    sget v1, Lcom/google/android/gms/l;->fh:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 471
    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/f;->ar:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->generateDefaultLayoutParams()Landroid/widget/LinearLayout$LayoutParams;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/g;->bR:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->height:I

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->addView(Landroid/view/View;)V

    .line 472
    sget v1, Lcom/google/android/gms/l;->eZ:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 473
    sget v0, Lcom/google/android/gms/j;->dC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    .line 474
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    return v0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    move v1, v2

    .line 483
    :goto_0
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f:I

    if-ge v1, v0, :cond_0

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const/4 v3, 0x1

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/view/View;Z)V

    .line 483
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 486
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    if-eqz v0, :cond_1

    .line 487
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f:I

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 488
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->i:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Landroid/view/View;Z)V

    .line 487
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 492
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->removeView(Landroid/view/View;)V

    .line 493
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->q:I

    iget v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->p:I

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-nez v0, :cond_2

    .line 494
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a:Landroid/view/View;

    const/4 v1, -0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->addView(Landroid/view/View;I)V

    .line 496
    :cond_2
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, -0x1

    const/4 v2, 0x0

    .line 50
    iput-boolean v2, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    move v1, v2

    :goto_0
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h:I

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v3, Lcom/google/android/gms/k;->C:I

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getTag(I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->g:I

    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h:I

    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/lang/CharSequence;)V

    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_5

    invoke-static {v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c(I)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b(I)I

    move-result v1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(III)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    invoke-direct {p0, v5}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b(I)I

    move-result v0

    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(II)V

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f()V

    :cond_5
    return-void
.end method

.method private static c(I)Z
    .locals 1

    .prologue
    .line 958
    rem-int/lit8 v0, p0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 563
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->xj:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V
    .locals 4

    .prologue
    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c()V

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    new-array v1, v0, [I

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->h:I

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v3}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    if-ge v0, v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/smart_profile/card/view/d;

    invoke-direct {v2, p0, v0, v1}, Lcom/google/android/gms/smart_profile/card/view/d;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;Landroid/view/ViewTreeObserver;[I)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/smart_profile/card/h;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c:Lcom/google/android/gms/smart_profile/card/h;

    return-object v0
.end method

.method private e()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 567
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->xl:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    .line 915
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c()V

    .line 918
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 919
    const/4 v1, -0x1

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 920
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 921
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 922
    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 923
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 926
    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    .line 927
    const/4 v3, -0x2

    iput v3, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 928
    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 921
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 930
    :cond_1
    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->m:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->j:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 464
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->l:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 465
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/card/h;)V
    .locals 0

    .prologue
    .line 457
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c:Lcom/google/android/gms/smart_profile/card/h;

    .line 458
    return-void
.end method

.method public final a(Ljava/util/List;IZI)V
    .locals 6

    .prologue
    .line 371
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZLandroid/net/Uri;I)V

    .line 373
    return-void
.end method

.method public final a(Ljava/util/List;IZLandroid/net/Uri;I)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 393
    iget-object v7, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->o:Lcom/google/android/gms/smart_profile/card/a/c;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v6, v5

    move v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZLandroid/net/Uri;Lcom/google/android/gms/common/api/v;Landroid/support/v4/app/au;Lcom/google/android/gms/smart_profile/card/a/c;I)V

    .line 396
    return-void
.end method

.method public final a(Ljava/util/List;IZLcom/google/android/gms/common/api/v;Landroid/support/v4/app/au;Lcom/google/android/gms/smart_profile/card/a/c;I)V
    .locals 9

    .prologue
    .line 383
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZLandroid/net/Uri;Lcom/google/android/gms/common/api/v;Landroid/support/v4/app/au;Lcom/google/android/gms/smart_profile/card/a/c;I)V

    .line 385
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 936
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e:Z

    return v0
.end method
