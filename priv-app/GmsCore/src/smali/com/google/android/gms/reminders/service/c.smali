.class final Lcom/google/android/gms/reminders/service/c;
.super Lcom/google/android/gms/reminders/internal/i;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/reminders/internal/i;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/reminders/service/c;->a:Landroid/content/Context;

    .line 78
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/reminders/internal/b;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/reminders/service/c;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;)V

    .line 89
    return-void
.end method

.method public final a(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/LoadRemindersOptions;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/reminders/service/c;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/LoadRemindersOptions;)V

    .line 84
    return-void
.end method

.method public final a(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/reminders/service/c;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V

    .line 95
    return-void
.end method

.method public final a(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskIdEntity;)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/reminders/service/c;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskIdEntity;)V

    .line 107
    return-void
.end method

.method public final b(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/reminders/service/c;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V

    .line 101
    return-void
.end method
