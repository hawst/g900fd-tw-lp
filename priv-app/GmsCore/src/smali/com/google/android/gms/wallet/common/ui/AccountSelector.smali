.class public Lcom/google/android/gms/wallet/common/ui/AccountSelector;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field a:Landroid/widget/Spinner;

.field b:Lcom/google/android/gms/wallet/common/ui/cq;

.field private c:Landroid/accounts/Account;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 45
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/content/Context;)V

    .line 46
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 50
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/content/Context;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 60
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 61
    sget v1, Lcom/google/android/gms/l;->hl:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 63
    sget v0, Lcom/google/android/gms/j;->i:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 65
    return-void
.end method

.method private c()V
    .locals 5

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/b;

    if-eqz v0, :cond_2

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/b;->getCount()I

    move-result v1

    if-ge v2, v1, :cond_2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/b;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/a;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/a;->a:Landroid/accounts/Account;

    invoke-static {v1, v4}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 118
    :cond_0
    return-void

    .line 116
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_2
    const/4 v2, -0x1

    goto :goto_1
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 6

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "account_selection"

    const-string v2, "account_selected"

    const-string v3, "account_spinner"

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 83
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    .line 84
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c()V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Lcom/google/android/gms/wallet/common/ui/cq;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Lcom/google/android/gms/wallet/common/ui/cq;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/ui/cq;->a(Landroid/accounts/Account;)V

    .line 89
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/cq;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Lcom/google/android/gms/wallet/common/ui/cq;

    .line 140
    return-void
.end method

.method public final a([Lcom/google/android/gms/wallet/common/ui/a;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 92
    .line 93
    array-length v0, p1

    if-le v0, v1, :cond_1

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/b;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v3, v4, p1}, Lcom/google/android/gms/wallet/common/ui/b;-><init>(Landroid/content/Context;[Lcom/google/android/gms/wallet/common/ui/a;)V

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    move v0, v1

    .line 101
    :goto_0
    if-eqz v0, :cond_3

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 107
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c()V

    .line 109
    array-length v0, p1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Lcom/google/android/gms/wallet/common/ui/cq;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->b:Lcom/google/android/gms/wallet/common/ui/cq;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cq;->a(Landroid/accounts/Account;)V

    .line 112
    :cond_0
    return-void

    .line 97
    :cond_1
    array-length v0, p1

    if-ne v0, v1, :cond_2

    .line 98
    aget-object v0, p1, v2

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/a;->a:Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    :cond_2
    move v0, v2

    goto :goto_0

    .line 104
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    sget v1, Lcom/google/android/gms/h;->dl:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setBackgroundResource(I)V

    .line 268
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    .prologue
    .line 177
    if-nez p2, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-void

    .line 181
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/a;

    .line 182
    if-eqz v0, :cond_0

    .line 183
    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/a;->a:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 152
    instance-of v0, p1, Landroid/os/Bundle;

    if-eqz v0, :cond_0

    .line 153
    check-cast p1, Landroid/os/Bundle;

    .line 154
    const-string v0, "instanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 155
    const-string v0, "currentAccount"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    .line 160
    :goto_0
    return-void

    .line 159
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 144
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 145
    const-string v1, "instanceState"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    const-string v1, "currentAccount"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->c:Landroid/accounts/Account;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 147
    return-object v0
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setEnabled(Z)V

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a:Landroid/widget/Spinner;

    invoke-virtual {v0, p1}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 169
    :cond_0
    return-void
.end method
