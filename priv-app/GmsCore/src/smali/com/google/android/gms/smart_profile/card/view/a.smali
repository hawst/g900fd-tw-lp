.class public final Lcom/google/android/gms/smart_profile/card/view/a;
.super Landroid/widget/ImageView;
.source "SourceFile"


# static fields
.field private static a:Landroid/graphics/Paint;

.field private static b:I


# instance fields
.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/smart_profile/card/view/a;-><init>(Landroid/content/Context;B)V

    .line 25
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/smart_profile/card/view/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    .line 32
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 35
    sget-object v1, Lcom/google/android/gms/smart_profile/card/view/a;->a:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    .line 36
    sget v1, Lcom/google/android/gms/g;->bO:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lcom/google/android/gms/smart_profile/card/view/a;->b:I

    .line 38
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    .line 39
    sput-object v1, Lcom/google/android/gms/smart_profile/card/view/a;->a:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 40
    sget-object v1, Lcom/google/android/gms/smart_profile/card/view/a;->a:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/gms/smart_profile/card/view/a;->b:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 41
    sget-object v1, Lcom/google/android/gms/smart_profile/card/view/a;->a:Landroid/graphics/Paint;

    sget v2, Lcom/google/android/gms/f;->d:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 43
    sget-object v0, Lcom/google/android/gms/smart_profile/card/view/a;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 46
    :cond_0
    new-instance v0, Lcom/google/android/gms/smart_profile/card/view/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/smart_profile/card/view/b;-><init>(Lcom/google/android/gms/smart_profile/card/view/a;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/a;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/card/view/a;Z)Z
    .locals 0

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/google/android/gms/smart_profile/card/view/a;->c:Z

    return p1
.end method


# virtual methods
.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 68
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 70
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/view/a;->c:Z

    if-nez v0, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/view/a;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 75
    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    .line 77
    sget v1, Lcom/google/android/gms/smart_profile/card/view/a;->b:I

    int-to-float v1, v1

    div-float/2addr v1, v2

    sub-float v1, v0, v1

    sget-object v2, Lcom/google/android/gms/smart_profile/card/view/a;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v0, v1, v2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method
