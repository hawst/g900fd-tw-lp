.class public final Lcom/google/android/gms/games/g/am;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8684
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8685
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/games/g/am;->a:[I

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/am;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/am;->cachedSize:I

    .line 8686
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 8739
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v2

    .line 8740
    iget-object v1, p0, Lcom/google/android/gms/games/g/am;->a:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/g/am;->a:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v0

    .line 8742
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/games/g/am;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 8743
    iget-object v3, p0, Lcom/google/android/gms/games/g/am;->a:[I

    aget v3, v3, v0

    .line 8744
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 8742
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8747
    :cond_0
    add-int v0, v2, v1

    .line 8748
    iget-object v1, p0, Lcom/google/android/gms/games/g/am;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8750
    :goto_1
    iget v1, p0, Lcom/google/android/gms/games/g/am;->b:I

    if-eqz v1, :cond_1

    .line 8751
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/g/am;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8754
    :cond_1
    return v0

    :cond_2
    move v0, v2

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8697
    if-ne p1, p0, :cond_1

    .line 8711
    :cond_0
    :goto_0
    return v0

    .line 8700
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/am;

    if-nez v2, :cond_2

    move v0, v1

    .line 8701
    goto :goto_0

    .line 8703
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/am;

    .line 8704
    iget-object v2, p0, Lcom/google/android/gms/games/g/am;->a:[I

    iget-object v3, p1, Lcom/google/android/gms/games/g/am;->a:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 8706
    goto :goto_0

    .line 8708
    :cond_3
    iget v2, p0, Lcom/google/android/gms/games/g/am;->b:I

    iget v3, p1, Lcom/google/android/gms/games/g/am;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 8709
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 8716
    iget-object v0, p0, Lcom/google/android/gms/games/g/am;->a:[I

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 8719
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/am;->b:I

    add-int/2addr v0, v1

    .line 8720
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 8652
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/g/am;->a:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/android/gms/games/g/am;->a:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/am;->a:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/games/g/am;->a:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/android/gms/games/g/am;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/g/am;->a:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/g/am;->a:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/games/g/am;->a:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/android/gms/games/g/am;->a:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/am;->b:I

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 8726
    iget-object v0, p0, Lcom/google/android/gms/games/g/am;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/g/am;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 8727
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/am;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 8728
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/am;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8727
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8731
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/g/am;->b:I

    if-eqz v0, :cond_1

    .line 8732
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/g/am;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8734
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8735
    return-void
.end method
