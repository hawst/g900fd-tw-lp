.class public Lcom/google/android/gms/social/location/TimeShareView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/social/location/TimeShare;

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Lcom/google/android/gms/social/location/c;

.field private g:Lcom/google/android/gms/social/location/am;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 47
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/social/location/TimeShareView;)Lcom/google/android/gms/social/location/am;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->g:Lcom/google/android/gms/social/location/am;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/social/location/TimeShareView;)Lcom/google/android/gms/social/location/TimeShare;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->a:Lcom/google/android/gms/social/location/TimeShare;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/social/location/TimeShareView;)Lcom/google/android/gms/social/location/c;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->f:Lcom/google/android/gms/social/location/c;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/social/location/TimeShare;Lcom/google/android/gms/social/location/am;Lcom/google/android/gms/common/internal/a/b;)V
    .locals 12

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/gms/social/location/TimeShareView;->a:Lcom/google/android/gms/social/location/TimeShare;

    .line 52
    iput-object p2, p0, Lcom/google/android/gms/social/location/TimeShareView;->g:Lcom/google/android/gms/social/location/am;

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    iget-object v2, p0, Lcom/google/android/gms/social/location/TimeShareView;->c:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->f()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/TimeShareView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/TimeUnit;->toMinutes(J)J

    move-result-wide v0

    const-wide/16 v4, 0xf

    cmp-long v4, v0, v4

    if-gez v4, :cond_1

    sget v0, Lcom/google/android/gms/p;->qo:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {p1}, Lcom/google/android/gms/social/location/TimeShare;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Lcom/google/android/gms/common/internal/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 57
    if-nez v0, :cond_6

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/social/location/TimeShareView;->f:Lcom/google/android/gms/social/location/c;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/c;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 59
    new-instance v0, Lcom/google/android/gms/social/location/al;

    iget-object v1, p0, Lcom/google/android/gms/social/location/TimeShareView;->d:Landroid/widget/ImageView;

    invoke-direct {v0, p0, v1, p3}, Lcom/google/android/gms/social/location/al;-><init>(Lcom/google/android/gms/social/location/TimeShareView;Landroid/widget/ImageView;Lcom/google/android/gms/common/internal/a/b;)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/social/location/TimeShare;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/social/location/al;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 63
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->e:Landroid/widget/ImageView;

    new-instance v1, Lcom/google/android/gms/social/location/ak;

    invoke-direct {v1, p0}, Lcom/google/android/gms/social/location/ak;-><init>(Lcom/google/android/gms/social/location/TimeShareView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 71
    return-void

    .line 55
    :cond_1
    const-wide/16 v4, 0x3c

    rem-long v4, v0, v4

    const-wide/16 v6, 0x3c

    div-long v6, v0, v6

    sget v0, Lcom/google/android/gms/p;->qm:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v1, v8

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/n;->q:I

    long-to-int v8, v6

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v3, v1, v8, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-wide/16 v8, 0x0

    cmp-long v8, v6, v8

    if-eqz v8, :cond_0

    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-nez v0, :cond_2

    move-object v0, v1

    goto :goto_0

    :cond_2
    const-wide/16 v0, 0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_3

    const-wide/16 v0, 0x1

    cmp-long v0, v4, v0

    if-nez v0, :cond_3

    sget v0, Lcom/google/android/gms/p;->qj:I

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    const-wide/16 v0, 0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_4

    sget v0, Lcom/google/android/gms/p;->ql:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v6

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_4
    const-wide/16 v0, 0x1

    cmp-long v0, v4, v0

    if-nez v0, :cond_5

    sget v0, Lcom/google/android/gms/p;->qn:I

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v1, v4

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_5
    sget v0, Lcom/google/android/gms/p;->qk:I

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v1, v8

    const/4 v6, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v6

    invoke-virtual {v3, v0, v1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 61
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/social/location/TimeShareView;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->b:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 113
    sget v0, Lcom/google/android/gms/j;->mi:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/TimeShareView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->b:Landroid/widget/TextView;

    .line 114
    sget v0, Lcom/google/android/gms/j;->sM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/TimeShareView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->c:Landroid/widget/TextView;

    .line 116
    new-instance v0, Lcom/google/android/gms/social/location/c;

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/TimeShareView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/social/location/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->f:Lcom/google/android/gms/social/location/c;

    .line 117
    sget v0, Lcom/google/android/gms/j;->by:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/TimeShareView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->d:Landroid/widget/ImageView;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->d:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/social/location/TimeShareView;->f:Lcom/google/android/gms/social/location/c;

    invoke-virtual {v1}, Lcom/google/android/gms/social/location/c;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 120
    sget v0, Lcom/google/android/gms/j;->ct:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/social/location/TimeShareView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/social/location/TimeShareView;->e:Landroid/widget/ImageView;

    .line 122
    :cond_0
    return-void
.end method
