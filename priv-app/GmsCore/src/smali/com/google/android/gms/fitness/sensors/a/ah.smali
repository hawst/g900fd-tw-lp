.class final Lcom/google/android/gms/fitness/sensors/a/ah;
.super Lcom/google/android/gms/fitness/sensors/a/ak;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/k/k/a/aq;

.field final synthetic c:Lcom/google/android/gms/fitness/sensors/a/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/a/ag;Ljava/lang/String;Lcom/google/k/k/a/aq;)V
    .locals 1

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/ah;->c:Lcom/google/android/gms/fitness/sensors/a/ag;

    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/a/ah;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/fitness/sensors/a/ah;->b:Lcom/google/k/k/a/aq;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/a/ak;-><init>(Lcom/google/android/gms/fitness/sensors/a/ag;B)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/ah;->b:Lcom/google/k/k/a/aq;

    new-instance v1, Ljava/util/concurrent/TimeoutException;

    const-string v2, "Scan stopped before device found"

    invoke-direct {v1, v2}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Throwable;)Z

    .line 61
    return-void
.end method

.method protected final a(Lcom/google/android/gms/fitness/data/BleDevice;)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/ah;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/BleDevice;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/sensors/a/ah;->b()Z

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/ah;->b:Lcom/google/k/k/a/aq;

    invoke-virtual {v0, p1}, Lcom/google/k/k/a/aq;->a(Ljava/lang/Object;)Z

    .line 56
    :cond_0
    return-void
.end method
