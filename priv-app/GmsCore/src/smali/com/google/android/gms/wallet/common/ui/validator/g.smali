.class public final Lcom/google/android/gms/wallet/common/ui/validator/g;
.super Lcom/google/android/gms/wallet/common/ui/validator/r;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final c:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;[I)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/validator/r;-><init>(Ljava/lang/CharSequence;)V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/validator/g;->a:Landroid/content/Context;

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/validator/g;->c:[I

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 39
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 40
    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v3

    .line 42
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/validator/g;->c:[I

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v1

    :goto_0
    if-nez v2, :cond_1

    .line 44
    packed-switch v3, :pswitch_data_0

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/g;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->Bm:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/g;->b:Ljava/lang/CharSequence;

    .line 56
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 42
    goto :goto_0

    .line 46
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/g;->a:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->Bl:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/g;->b:Ljava/lang/CharSequence;

    goto :goto_1

    :cond_1
    move v0, v1

    .line 56
    goto :goto_1

    .line 44
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method
