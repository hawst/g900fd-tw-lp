.class final Lcom/google/android/gms/wallet/service/ia/aj;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ia/ad;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ia/ad;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/aj;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ia/aj;->c:Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ia/aj;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 76
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ia/aj;->c:Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->b()Lcom/google/checkout/inapp/proto/ap;

    move-result-object v1

    .line 78
    iget-object v2, v1, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    .line 79
    iget v3, v2, Lcom/google/checkout/a/a/a/d;->a:I

    const/4 v4, 0x1

    if-eq v3, v4, :cond_0

    .line 80
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Update instrument currently only supports credit cards."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v6

    check-cast v6, Lcom/google/checkout/inapp/proto/ap;

    .line 87
    iget-object v1, v2, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-eqz v1, :cond_1

    .line 88
    iget-object v1, v2, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iget-object v2, v1, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    .line 90
    :goto_0
    iget-object v1, v6, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iput-object v0, v1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/aj;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/service/ia/ad;)Lcom/google/android/gms/wallet/service/ia/o;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/aj;->e:Lcom/google/android/gms/wallet/service/ia/ad;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/aj;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/ad;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/aj;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/aj;->a:Landroid/accounts/Account;

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/aj;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v8

    iget-object v0, v6, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ia/o;->a(Lcom/google/checkout/a/a/a/d;)V

    iget-object v9, v1, Lcom/google/android/gms/wallet/service/ia/o;->a:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ia/x;

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/wallet/service/ia/x;-><init>(Lcom/google/android/gms/wallet/service/ia/o;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/checkout/inapp/proto/ap;Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "update_instrument"

    invoke-static {v9, v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0

    :cond_1
    move-object v2, v0

    goto :goto_0
.end method
