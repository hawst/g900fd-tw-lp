.class public final Lcom/google/android/gms/lockbox/d/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;
.implements Lcom/google/android/gms/lockbox/d/c;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/google/android/gms/location/m;

.field private c:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lcom/google/android/gms/lockbox/d/a;->a:Landroid/content/Context;

    .line 41
    return-void
.end method

.method private c()Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 82
    invoke-static {}, Landroid/location/Geocoder;->isPresent()Z

    move-result v1

    if-nez v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return-object v0

    .line 87
    :cond_1
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/lockbox/d/a;->c:Ljava/util/concurrent/CountDownLatch;

    .line 88
    new-instance v1, Lcom/google/android/gms/location/m;

    iget-object v2, p0, Lcom/google/android/gms/lockbox/d/a;->a:Landroid/content/Context;

    invoke-direct {v1, v2, p0, p0}, Lcom/google/android/gms/location/m;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)V

    iput-object v1, p0, Lcom/google/android/gms/lockbox/d/a;->b:Lcom/google/android/gms/location/m;

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/lockbox/d/a;->b:Lcom/google/android/gms/location/m;

    invoke-virtual {v1}, Lcom/google/android/gms/location/m;->a()V

    .line 92
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/lockbox/d/a;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/lockbox/d/a;->b:Lcom/google/android/gms/location/m;

    if-eqz v1, :cond_0

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/lockbox/d/a;->b:Lcom/google/android/gms/location/m;

    iget-object v1, v1, Lcom/google/android/gms/location/m;->a:Lcom/google/android/gms/location/internal/w;

    invoke-virtual {v1}, Lcom/google/android/gms/location/internal/w;->f()Landroid/location/Location;

    move-result-object v4

    .line 101
    if-nez v4, :cond_2

    .line 102
    const-string v1, "FusedLocationProvider"

    const-string v2, "location=null"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 93
    :catch_0
    move-exception v1

    .line 94
    const-string v2, "FusedLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Interrupted: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 106
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/lockbox/d/a;->b:Lcom/google/android/gms/location/m;

    invoke-virtual {v1}, Lcom/google/android/gms/location/m;->b()V

    .line 109
    new-instance v1, Landroid/location/Geocoder;

    iget-object v2, p0, Lcom/google/android/gms/lockbox/d/a;->a:Landroid/content/Context;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Landroid/location/Geocoder;-><init>(Landroid/content/Context;Ljava/util/Locale;)V

    .line 110
    :try_start_1
    invoke-virtual {v4}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v4}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Landroid/location/Geocoder;->getFromLocation(DDI)Ljava/util/List;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v1

    .line 121
    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-nez v2, :cond_4

    .line 122
    :cond_3
    const-string v1, "FusedLocationProvider"

    const-string v2, "No address is returned"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 114
    :catch_1
    move-exception v1

    .line 115
    const-string v2, "FusedLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "I/O error: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 117
    :catch_2
    move-exception v1

    .line 118
    const-string v2, "FusedLocationProvider"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not translate location into address: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 126
    :cond_4
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Address;

    invoke-virtual {v0}, Landroid/location/Address;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0
.end method


# virtual methods
.method public final T_()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/lockbox/d/a;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 134
    return-void
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public final a()Ljava/lang/String;
    .locals 8

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/lockbox/d/a;->a:Landroid/content/Context;

    const-string v1, "FusedLocationProvider"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 59
    const-string v0, "countryCode"

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 60
    const-string v2, "lastQuery"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 61
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 66
    if-eqz v0, :cond_1

    const-wide/32 v6, 0x1499700

    add-long/2addr v2, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    .line 78
    :cond_0
    :goto_0
    return-object v0

    .line 71
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/lockbox/d/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 73
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 74
    const-string v2, "countryCode"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 75
    const-string v2, "lastQuery"

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 76
    invoke-static {v1}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 138
    const-string v0, "FusedLocationProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/lockbox/d/a;->b:Lcom/google/android/gms/location/m;

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/lockbox/d/a;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 141
    return-void
.end method
