.class public final Lcom/google/android/gms/ads/internal/p/a;
.super Landroid/webkit/WebView;
.source "SourceFile"

# interfaces
.implements Landroid/webkit/DownloadListener;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/p/c;

.field private final b:Lcom/google/android/gms/ads/internal/p/b;

.field private final c:Ljava/lang/Object;

.field private final d:Lcom/google/android/a/w;

.field private final e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

.field private f:Lcom/google/android/gms/ads/internal/overlay/c;

.field private g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private final l:Landroid/view/WindowManager;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/ads/internal/p/b;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLcom/google/android/a/w;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 155
    invoke-direct {p0, p1}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 121
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    .line 156
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p/a;->b:Lcom/google/android/gms/ads/internal/p/b;

    .line 158
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 159
    iput-boolean p3, p0, Lcom/google/android/gms/ads/internal/p/a;->h:Z

    .line 160
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/p/a;->d:Lcom/google/android/a/w;

    .line 161
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/p/a;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->l:Landroid/view/WindowManager;

    .line 165
    invoke-virtual {p0, v3}, Lcom/google/android/gms/ads/internal/p/a;->setBackgroundColor(I)V

    .line 168
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    .line 169
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    .line 170
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setSavePassword(Z)V

    .line 171
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setSupportMultipleWindows(Z)V

    .line 172
    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setJavaScriptCanOpenWindowsAutomatically(Z)V

    .line 173
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_0

    .line 175
    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setMixedContentMode(I)V

    .line 179
    :cond_0
    iget-object v1, p6, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/ads/internal/util/g;->a(Landroid/content/Context;Ljava/lang/String;Landroid/webkit/WebSettings;)V

    .line 182
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_3

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/o;->a(Landroid/content/Context;Landroid/webkit/WebSettings;)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebSettings;->setMediaPlaybackRequiresUserGesture(Z)V

    .line 189
    :cond_1
    :goto_0
    invoke-virtual {p0, p0}, Lcom/google/android/gms/ads/internal/p/a;->setDownloadListener(Landroid/webkit/DownloadListener;)V

    .line 192
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_4

    .line 193
    new-instance v0, Lcom/google/android/gms/ads/internal/p/n;

    invoke-direct {v0, p0, p4}, Lcom/google/android/gms/ads/internal/p/n;-><init>(Lcom/google/android/gms/ads/internal/p/a;Z)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->a:Lcom/google/android/gms/ads/internal/p/c;

    .line 197
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->a:Lcom/google/android/gms/ads/internal/p/c;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 201
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-lt v0, v1, :cond_5

    .line 202
    new-instance v0, Lcom/google/android/gms/ads/internal/p/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/p/o;-><init>(Lcom/google/android/gms/ads/internal/p/a;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    .line 207
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/p/a;->m()V

    .line 208
    return-void

    .line 184
    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v4, :cond_1

    .line 185
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/o;->a(Landroid/content/Context;Landroid/webkit/WebSettings;)V

    goto :goto_0

    .line 195
    :cond_4
    new-instance v0, Lcom/google/android/gms/ads/internal/p/c;

    invoke-direct {v0, p0, p4}, Lcom/google/android/gms/ads/internal/p/c;-><init>(Lcom/google/android/gms/ads/internal/p/a;Z)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->a:Lcom/google/android/gms/ads/internal/p/c;

    goto :goto_1

    .line 203
    :cond_5
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v4, :cond_2

    .line 204
    new-instance v0, Lcom/google/android/gms/ads/internal/p/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/p/f;-><init>(Lcom/google/android/gms/ads/internal/p/a;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->setWebChromeClient(Landroid/webkit/WebChromeClient;)V

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLcom/google/android/a/w;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/p/a;
    .locals 7

    .prologue
    .line 142
    new-instance v1, Lcom/google/android/gms/ads/internal/p/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/ads/internal/p/b;-><init>(Landroid/content/Context;)V

    .line 143
    new-instance v0, Lcom/google/android/gms/ads/internal/p/a;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/ads/internal/p/a;-><init>(Lcom/google/android/gms/ads/internal/p/b;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLcom/google/android/a/w;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    .line 529
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 530
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->h:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-boolean v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    if-eqz v0, :cond_2

    .line 531
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-ge v0, v2, :cond_1

    .line 532
    const-string v0, "Disabling hardware acceleration on an overlay."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 533
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/p/a;->n()V

    .line 547
    :goto_0
    monitor-exit v1

    return-void

    .line 535
    :cond_1
    const-string v0, "Enabling hardware acceleration on an overlay."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 536
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/p/a;->o()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 547
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 539
    :cond_2
    :try_start_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-ge v0, v2, :cond_3

    .line 540
    const-string v0, "Disabling hardware acceleration on an AdView."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 541
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/p/a;->n()V

    goto :goto_0

    .line 543
    :cond_3
    const-string v0, "Enabling hardware acceleration on an AdView."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 544
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/p/a;->o()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method private n()V
    .locals 3

    .prologue
    .line 555
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 556
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->i:Z

    if-nez v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 558
    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 560
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->i:Z

    .line 561
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private o()V
    .locals 3

    .prologue
    .line 569
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 570
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->i:Z

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_0

    .line 572
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 574
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->i:Z

    .line 575
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->a:Lcom/google/android/gms/ads/internal/p/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/c;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 241
    :goto_0
    return-void

    .line 214
    :cond_0
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 215
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->l:Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    .line 216
    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/g;->c(Landroid/content/Context;)I

    move-result v2

    .line 225
    const/high16 v3, 0x43200000    # 160.0f

    iget v4, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    .line 226
    iget v4, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    mul-float/2addr v4, v3

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 227
    iget v5, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    sub-int v2, v5, v2

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 230
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "width"

    invoke-virtual {v3, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "height"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "density"

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v4, v0

    invoke-virtual {v2, v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "rotation"

    invoke-virtual {v1}, Landroid/view/Display;->getRotation()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    .line 235
    const-string v1, "onScreenInfoChanged"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 236
    :catch_0
    move-exception v0

    .line 237
    const-string v1, "Error occured while obtaining screen information."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 238
    :catch_1
    move-exception v0

    .line 239
    const-string v1, "Error occured while obtaining screen information."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 511
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->b:Lcom/google/android/gms/ads/internal/p/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/p/b;->setBaseContext(Landroid/content/Context;)V

    .line 512
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    .locals 2

    .prologue
    .line 470
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 471
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->b:Lcom/google/android/gms/ads/internal/p/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/p/b;->setBaseContext(Landroid/content/Context;)V

    .line 472
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    .line 473
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 474
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->h:Z

    .line 475
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->k:Z

    .line 476
    invoke-static {p0}, Lcom/google/android/gms/ads/internal/util/g;->b(Landroid/webkit/WebView;)V

    .line 477
    const-string v0, "about:blank"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->loadUrl(Ljava/lang/String;)V

    .line 478
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->a:Lcom/google/android/gms/ads/internal/p/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/c;->d()V

    .line 479
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 480
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 481
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    .locals 2

    .prologue
    .line 494
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 495
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 496
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->requestLayout()V

    .line 497
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/overlay/c;)V
    .locals 2

    .prologue
    .line 488
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 489
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    .line 490
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 286
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/ads/internal/util/g;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 290
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/ads/internal/p/a;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 292
    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "javascript:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->l()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->loadUrl(Ljava/lang/String;)V

    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    const-string v0, "The webview is destroyed. Ignoring action."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 249
    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/ads/internal/util/g;->a(Ljava/util/Map;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 257
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/ads/internal/p/a;->b(Ljava/lang/String;Lorg/json/JSONObject;)V

    .line 258
    :goto_0
    return-void

    .line 251
    :catch_0
    move-exception v0

    const-string v0, "Could not convert parameters to JSON."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 253
    :catch_1
    move-exception v0

    .line 254
    const-string v1, "Error occured while dispatching active view measurements."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 4

    .prologue
    .line 297
    if-nez p2, :cond_0

    .line 298
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    .line 300
    :cond_0
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 301
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 302
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 303
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 304
    const-string v0, ");"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->a(Ljava/lang/String;)V

    .line 306
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 504
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 505
    :try_start_0
    iput-boolean p1, p0, Lcom/google/android/gms/ads/internal/p/a;->h:Z

    .line 506
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/p/a;->m()V

    .line 507
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 328
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 329
    const-string v1, "version"

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/a;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    const-string v1, "onhide"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/ads/internal/p/a;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 331
    return-void
.end method

.method public final b(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 312
    if-nez p2, :cond_0

    .line 313
    new-instance p2, Lorg/json/JSONObject;

    invoke-direct {p2}, Lorg/json/JSONObject;-><init>()V

    .line 315
    :cond_0
    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    .line 316
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 317
    const-string v2, "AFMA_ReceiveMessage(\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 318
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 319
    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 321
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 322
    const-string v0, ");"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Dispatching AFMA event: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 324
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->a(Ljava/lang/String;)V

    .line 325
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 515
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 516
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/a;->a:Lcom/google/android/gms/ads/internal/p/c;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/internal/p/c;->b()Z

    move-result v2

    invoke-virtual {v0, v2, p1}, Lcom/google/android/gms/ads/internal/overlay/c;->a(ZZ)V

    .line 521
    :goto_0
    monitor-exit v1

    return-void

    .line 519
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/gms/ads/internal/p/a;->k:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 521
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 334
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 335
    const-string v1, "version"

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/a;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    const-string v1, "onshow"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/ads/internal/p/a;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 337
    return-void
.end method

.method public final d()Lcom/google/android/gms/ads/internal/overlay/c;
    .locals 2

    .prologue
    .line 344
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 345
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 346
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final destroy()V
    .locals 2

    .prologue
    .line 580
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 581
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    if-eqz v0, :cond_0

    .line 582
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/overlay/c;->a()V

    .line 583
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->f:Lcom/google/android/gms/ads/internal/overlay/c;

    .line 585
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->j:Z

    if-eqz v0, :cond_1

    .line 586
    monitor-exit v1

    .line 590
    :goto_0
    return-void

    .line 588
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->j:Z

    .line 589
    invoke-super {p0}, Landroid/webkit/WebView;->destroy()V

    .line 590
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    .locals 2

    .prologue
    .line 354
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 355
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 263
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 264
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 265
    const-string v0, "The webview is destroyed. Ignoring action."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 266
    if-eqz p2, :cond_0

    .line 267
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/webkit/ValueCallback;->onReceiveValue(Ljava/lang/Object;)V

    .line 269
    :cond_0
    monitor-exit v1

    .line 272
    :goto_0
    return-void

    .line 271
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->evaluateJavascript(Ljava/lang/String;Landroid/webkit/ValueCallback;)V

    .line 272
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Lcom/google/android/gms/ads/internal/p/c;
    .locals 1

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->a:Lcom/google/android/gms/ads/internal/p/c;

    return-object v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 364
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->k:Z

    return v0
.end method

.method public final h()Lcom/google/android/a/w;
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->d:Lcom/google/android/a/w;

    return-object v0
.end method

.method public final i()Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->e:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 383
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 384
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->h:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k()Landroid/content/Context;
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->b:Lcom/google/android/gms/ads/internal/p/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/b;->a()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 594
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 595
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/p/a;->j:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 596
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onDownloadStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 392
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 393
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 394
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 398
    :goto_0
    return-void

    .line 396
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Couldn\'t find an Activity to view url/mimetype: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " / "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x15
    .end annotation

    .prologue
    .line 601
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->isAttachedToWindow()Z

    move-result v0

    if-nez v0, :cond_0

    .line 607
    :goto_0
    return-void

    .line 606
    :cond_0
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected final onMeasure(II)V
    .locals 9

    .prologue
    const v0, 0x7fffffff

    const/high16 v8, 0x40000000    # 2.0f

    const/16 v7, 0x8

    const/high16 v6, -0x80000000

    .line 411
    iget-object v4, p0, Lcom/google/android/gms/ads/internal/p/a;->c:Ljava/lang/Object;

    monitor-enter v4

    .line 413
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/ads/internal/p/a;->h:Z

    if-eqz v1, :cond_1

    .line 414
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/webkit/WebView;->onMeasure(II)V

    .line 415
    monitor-exit v4

    .line 463
    :goto_0
    return-void

    .line 419
    :cond_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 420
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v3

    .line 421
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 422
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 426
    if-eq v2, v6, :cond_2

    if-ne v2, v8, :cond_9

    :cond_2
    move v2, v3

    .line 430
    :goto_1
    if-eq v5, v6, :cond_3

    if-ne v5, v8, :cond_4

    :cond_3
    move v0, v1

    .line 434
    :cond_4
    iget-object v5, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v5, v5, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    if-gt v5, v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v2, v2, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    if-le v2, v0, :cond_7

    .line 438
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->b:Lcom/google/android/gms/ads/internal/p/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/b;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    .line 439
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Not enough space to show ad. Needs "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v5, v5, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    int-to-float v5, v5

    div-float/2addr v5, v0

    float-to-int v5, v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "x"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v5, v5, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    int-to-float v5, v5

    div-float/2addr v5, v0

    float-to-int v5, v5

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " dp, but only has "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-float v3, v3

    div-float/2addr v3, v0

    float-to-int v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    int-to-float v1, v1

    div-float v0, v1, v0

    float-to-int v0, v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dp."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_6

    .line 451
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->setVisibility(I)V

    .line 453
    :cond_6
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->setMeasuredDimension(II)V

    .line 463
    :goto_2
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 458
    :cond_7
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->getVisibility()I

    move-result v0

    if-eq v0, v7, :cond_8

    .line 459
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/p/a;->setVisibility(I)V

    .line 461
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v0, v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/p/a;->g:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget v1, v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->setMeasuredDimension(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :cond_9
    move v2, v0

    goto/16 :goto_1
.end method

.method public final onPause()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 612
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 621
    :goto_0
    return-void

    .line 617
    :cond_0
    :try_start_0
    invoke-super {p0}, Landroid/webkit/WebView;->onPause()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 618
    :catch_0
    move-exception v0

    .line 619
    const-string v1, "Could not pause webview."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 626
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 635
    :goto_0
    return-void

    .line 631
    :cond_0
    :try_start_0
    invoke-super {p0}, Landroid/webkit/WebView;->onResume()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 632
    :catch_0
    move-exception v0

    .line 633
    const-string v1, "Could not resume webview."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->d:Lcom/google/android/a/w;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/p/a;->d:Lcom/google/android/a/w;

    iget-object v0, v0, Lcom/google/android/a/w;->d:Lcom/google/android/a/p;

    invoke-interface {v0, p1}, Lcom/google/android/a/p;->a(Landroid/view/MotionEvent;)V

    .line 406
    :cond_0
    invoke-super {p0, p1}, Landroid/webkit/WebView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public final stopLoading()V
    .locals 2

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/p/a;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 648
    :goto_0
    return-void

    .line 644
    :cond_0
    :try_start_0
    invoke-super {p0}, Landroid/webkit/WebView;->stopLoading()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 645
    :catch_0
    move-exception v0

    .line 646
    const-string v1, "Could not stop loading webview."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
