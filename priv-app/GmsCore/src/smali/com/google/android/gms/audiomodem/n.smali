.class public final Lcom/google/android/gms/audiomodem/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/audiomodem/b;


# instance fields
.field public final a:Lcom/google/android/gms/audiomodem/a;

.field public b:Lcom/google/android/gms/audiomodem/bh;

.field c:Z

.field private final d:Landroid/content/Context;

.field private final e:Landroid/os/Handler;

.field private f:Lcom/google/android/gms/audiomodem/bk;

.field private g:Z

.field private final h:Lcom/google/android/gms/audiomodem/at;

.field private final i:Lcom/google/android/gms/audiomodem/at;

.field private final j:Lcom/google/android/gms/audiomodem/k;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/n;->d:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/n;->e:Landroid/os/Handler;

    .line 52
    invoke-static {}, Lcom/google/android/gms/audiomodem/a;->a()Lcom/google/android/gms/audiomodem/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/n;->a:Lcom/google/android/gms/audiomodem/a;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/n;->a:Lcom/google/android/gms/audiomodem/a;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/audiomodem/a;->a(Lcom/google/android/gms/audiomodem/b;)V

    .line 54
    new-instance v0, Lcom/google/android/gms/audiomodem/at;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/at;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/n;->h:Lcom/google/android/gms/audiomodem/at;

    .line 55
    new-instance v0, Lcom/google/android/gms/audiomodem/at;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/at;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/n;->i:Lcom/google/android/gms/audiomodem/at;

    .line 57
    new-instance v0, Lcom/google/android/gms/audiomodem/k;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/gms/audiomodem/k;-><init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/audiomodem/n;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/n;->j:Lcom/google/android/gms/audiomodem/k;

    .line 60
    invoke-static {p1}, Lcom/google/android/gms/audiomodem/ap;->a(Landroid/content/Context;)V

    .line 61
    return-void
.end method

.method public static a(Lcom/google/android/gms/audiomodem/a/e;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    .line 245
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1, v1, v1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    if-nez p0, :cond_1

    const-string v1, "audioModem"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Callback is null. Could not send statusCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    :try_start_0
    invoke-interface {p0, v0}, Lcom/google/android/gms/audiomodem/a/e;->a(Lcom/google/android/gms/common/api/Status;)V

    const-string v1, "audioModem"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sent callback message statusCode:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "audioModem"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Failed to send callback message"

    const-string v2, "audioModem"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/audiomodem/a/n;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 2

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/google/android/gms/audiomodem/n;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    const/16 v0, 0x1965

    invoke-static {p2, v0}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    .line 103
    :goto_0
    return-void

    .line 98
    :cond_0
    new-instance v0, Lcom/google/android/gms/audiomodem/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/audiomodem/p;-><init>(Lcom/google/android/gms/audiomodem/a/n;)V

    .line 100
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/p;Ljava/util/Set;)V

    .line 102
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/audiomodem/n;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 148
    const/16 v0, 0x1964

    invoke-static {p2, v0}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    .line 158
    :goto_0
    return-void

    .line 152
    :cond_0
    new-instance v0, Lcom/google/android/gms/audiomodem/q;

    invoke-direct {v0, p1}, Lcom/google/android/gms/audiomodem/q;-><init>(Lcom/google/android/gms/audiomodem/a/q;)V

    .line 155
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/q;Ljava/util/Set;)V

    .line 157
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/audiomodem/aq;)V
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/n;->e:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/audiomodem/o;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/audiomodem/o;-><init>(Lcom/google/android/gms/audiomodem/n;Lcom/google/android/gms/audiomodem/aq;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 242
    return-void
.end method

.method public final a(Lcom/google/android/gms/audiomodem/p;Ljava/util/Set;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/n;->h:Lcom/google/android/gms/audiomodem/at;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/audiomodem/at;->a(Ljava/lang/Object;Ljava/util/Set;)Lcom/google/android/gms/audiomodem/au;

    move-result-object v5

    .line 110
    iget-object v0, v5, Lcom/google/android/gms/audiomodem/au;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    :pswitch_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/bl;

    .line 111
    iget-object v7, p0, Lcom/google/android/gms/audiomodem/n;->f:Lcom/google/android/gms/audiomodem/bk;

    iget-object v1, v0, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/Encoding;->b()I

    move-result v8

    iget-object v1, v7, Lcom/google/android/gms/audiomodem/bk;->a:Lcom/google/android/gms/audiomodem/af;

    iget-object v9, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aget-object v9, v9, v8

    if-nez v9, :cond_0

    iget-object v1, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aput-object v0, v1, v8

    move v1, v2

    :goto_1
    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    invoke-virtual {v7, v8}, Lcom/google/android/gms/audiomodem/bk;->b(I)V

    goto :goto_0

    :cond_0
    iget-object v9, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aget-object v9, v9, v8

    invoke-virtual {v9, v0}, Lcom/google/android/gms/audiomodem/bl;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    iget-object v9, v1, Lcom/google/android/gms/audiomodem/af;->b:[Ljava/util/Queue;

    aget-object v9, v9, v8

    invoke-interface {v9, v0}, Ljava/util/Queue;->contains(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_1
    move v1, v3

    goto :goto_1

    :cond_2
    iget-object v1, v1, Lcom/google/android/gms/audiomodem/af;->b:[Ljava/util/Queue;

    aget-object v1, v1, v8

    invoke-interface {v1, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    move v1, v4

    goto :goto_1

    :pswitch_2
    iget-object v0, v0, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/audiomodem/p;->a(I)V

    goto :goto_0

    .line 114
    :cond_3
    iget-object v0, v5, Lcom/google/android/gms/audiomodem/au;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    :pswitch_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/bl;

    .line 115
    iget-object v6, p0, Lcom/google/android/gms/audiomodem/n;->f:Lcom/google/android/gms/audiomodem/bk;

    iget-object v1, v0, Lcom/google/android/gms/audiomodem/bl;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/Encoding;->b()I

    move-result v7

    iget-object v1, v6, Lcom/google/android/gms/audiomodem/bk;->a:Lcom/google/android/gms/audiomodem/af;

    iget-object v8, v1, Lcom/google/android/gms/audiomodem/af;->b:[Ljava/util/Queue;

    aget-object v8, v8, v7

    invoke-interface {v8, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    const/4 v1, 0x3

    :goto_3
    packed-switch v1, :pswitch_data_1

    goto :goto_2

    :pswitch_4
    iget-object v0, v0, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/audiomodem/p;->a(I)V

    invoke-virtual {v6, v7}, Lcom/google/android/gms/audiomodem/bk;->c(I)V

    goto :goto_2

    :cond_4
    iget-object v8, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aget-object v8, v8, v7

    if-eqz v8, :cond_5

    iget-object v8, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    aget-object v8, v8, v7

    invoke-virtual {v8, v0}, Lcom/google/android/gms/audiomodem/bl;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_6

    :cond_5
    move v1, v4

    goto :goto_3

    :cond_6
    iget-object v8, v1, Lcom/google/android/gms/audiomodem/af;->b:[Ljava/util/Queue;

    aget-object v8, v8, v7

    invoke-interface {v8}, Ljava/util/Queue;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_7

    iget-object v1, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    const/4 v8, 0x0

    aput-object v8, v1, v7

    move v1, v2

    goto :goto_3

    :cond_7
    iget-object v8, v1, Lcom/google/android/gms/audiomodem/af;->a:[Lcom/google/android/gms/audiomodem/bl;

    iget-object v1, v1, Lcom/google/android/gms/audiomodem/af;->b:[Ljava/util/Queue;

    aget-object v1, v1, v7

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/audiomodem/bl;

    aput-object v1, v8, v7

    move v1, v3

    goto :goto_3

    :pswitch_5
    iget-object v0, v0, Lcom/google/android/gms/audiomodem/bl;->a:Lcom/google/android/gms/audiomodem/p;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/audiomodem/p;->a(I)V

    invoke-virtual {v6, v7}, Lcom/google/android/gms/audiomodem/bk;->c(I)V

    invoke-virtual {v6, v7}, Lcom/google/android/gms/audiomodem/bk;->b(I)V

    goto :goto_2

    .line 117
    :cond_8
    return-void

    .line 111
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 115
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/audiomodem/q;Ljava/util/Set;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/n;->i:Lcom/google/android/gms/audiomodem/at;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/audiomodem/at;->a(Ljava/lang/Object;Ljava/util/Set;)Lcom/google/android/gms/audiomodem/au;

    move-result-object v1

    .line 165
    iget-object v0, v1, Lcom/google/android/gms/audiomodem/au;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/bi;

    .line 166
    iget-object v3, p0, Lcom/google/android/gms/audiomodem/n;->b:Lcom/google/android/gms/audiomodem/bh;

    iget-boolean v4, v3, Lcom/google/android/gms/audiomodem/bh;->c:Z

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/audiomodem/bh;->a()V

    :cond_1
    iget-object v4, v0, Lcom/google/android/gms/audiomodem/bi;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/audiomodem/bh;->a(Lcom/google/android/gms/audiomodem/Encoding;)Lcom/google/android/gms/audiomodem/TokenDecoder;

    move-result-object v4

    iget-object v5, v0, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/audiomodem/TokenDecoder;->a(Lcom/google/android/gms/audiomodem/q;)Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/audiomodem/q;->a(I)V

    iget-object v3, v3, Lcom/google/android/gms/audiomodem/bh;->a:Lcom/google/android/gms/audiomodem/y;

    iget-object v0, v3, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget-object v5, v3, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    if-nez v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/audiomodem/y;->a:Landroid/content/Context;

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.google.android.googlequicksearchbox.action.PAUSE_HOTWORD"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v5, "com.google.android.googlequicksearchbox"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    const-string v5, "com.google.android.googlequicksearchbox.extra.PAUSE_HOTWORD_REQUESTING_PACKAGE"

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-boolean v0, v3, Lcom/google/android/gms/audiomodem/y;->f:Z

    if-nez v0, :cond_0

    iput-boolean v7, v3, Lcom/google/android/gms/audiomodem/y;->f:Z

    iget-object v0, v3, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/bb;->a()Landroid/media/AudioRecord;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/AudioRecord;->startRecording()V

    new-instance v4, Ljava/io/PipedInputStream;

    sget-object v0, Lcom/google/android/gms/audiomodem/m;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/io/PipedInputStream;-><init>(I)V

    new-instance v0, Ljava/io/PipedOutputStream;

    invoke-direct {v0}, Ljava/io/PipedOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {v4, v0}, Ljava/io/PipedInputStream;->connect(Ljava/io/PipedOutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v5, Lcom/google/android/gms/audiomodem/aa;

    invoke-direct {v5, v3, v0}, Lcom/google/android/gms/audiomodem/aa;-><init>(Lcom/google/android/gms/audiomodem/y;Ljava/io/PipedOutputStream;)V

    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, v5}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    new-instance v5, Lcom/google/android/gms/audiomodem/ab;

    invoke-direct {v5, v3, v4, v0}, Lcom/google/android/gms/audiomodem/ab;-><init>(Lcom/google/android/gms/audiomodem/y;Ljava/io/PipedInputStream;Ljava/lang/Thread;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, v3, Lcom/google/android/gms/audiomodem/y;->e:Landroid/os/Handler;

    invoke-virtual {v0, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 169
    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/audiomodem/au;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/bi;

    .line 170
    iget-object v3, p0, Lcom/google/android/gms/audiomodem/n;->b:Lcom/google/android/gms/audiomodem/bh;

    iget-object v4, v0, Lcom/google/android/gms/audiomodem/bi;->b:Lcom/google/android/gms/audiomodem/Encoding;

    iget-object v1, v3, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/audiomodem/ar;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v3, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/audiomodem/ar;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/audiomodem/TokenDecoder;

    iget-object v5, v0, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    invoke-virtual {v1, v5}, Lcom/google/android/gms/audiomodem/TokenDecoder;->b(Lcom/google/android/gms/audiomodem/q;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/audiomodem/q;->a(I)V

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/TokenDecoder;->b()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, v3, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    iget-object v5, v0, Lcom/google/android/gms/audiomodem/ar;->b:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, v0, Lcom/google/android/gms/audiomodem/ar;->a:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/ar;->a()I

    move-result v4

    iput v4, v0, Lcom/google/android/gms/audiomodem/ar;->c:I

    iget-object v0, v3, Lcom/google/android/gms/audiomodem/bh;->a:Lcom/google/android/gms/audiomodem/y;

    iget-object v3, v0, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    iget-object v4, v0, Lcom/google/android/gms/audiomodem/y;->c:Ljava/util/Set;

    invoke-interface {v4, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    if-ne v3, v7, :cond_3

    iget-boolean v1, v0, Lcom/google/android/gms/audiomodem/y;->f:Z

    if-eqz v1, :cond_3

    iput-boolean v7, v0, Lcom/google/android/gms/audiomodem/y;->g:Z

    goto :goto_1

    .line 172
    :cond_4
    return-void
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 202
    iget-boolean v1, p0, Lcom/google/android/gms/audiomodem/n;->c:Z

    if-nez v1, :cond_0

    .line 204
    :try_start_0
    new-instance v1, Lcom/google/android/gms/audiomodem/bh;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/n;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/audiomodem/bh;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/audiomodem/n;->b:Lcom/google/android/gms/audiomodem/bh;

    .line 205
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/audiomodem/n;->c:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 212
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/n;->j:Lcom/google/android/gms/audiomodem/k;

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/k;->a()V

    .line 214
    :cond_0
    :goto_0
    return v0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    const-string v1, "audioModem"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioModemHelper: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "audioModem"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 210
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 218
    iget-boolean v1, p0, Lcom/google/android/gms/audiomodem/n;->g:Z

    if-nez v1, :cond_0

    .line 220
    :try_start_0
    new-instance v1, Lcom/google/android/gms/audiomodem/bk;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/n;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/audiomodem/bk;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/audiomodem/n;->f:Lcom/google/android/gms/audiomodem/bk;

    .line 221
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/audiomodem/n;->g:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 229
    :cond_0
    :goto_0
    return v0

    .line 222
    :catch_0
    move-exception v0

    .line 223
    const-string v1, "audioModem"

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioModemHelper: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "audioModem"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 226
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
