.class public final Lcom/google/android/gms/fitness/sensors/a/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/sensors/a/z;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/fitness/sensors/a/aa;

.field final b:Ljava/util/concurrent/ConcurrentHashMap;

.field private final c:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/sensors/a/aa;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 41
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->a:Lcom/google/android/gms/fitness/sensors/a/aa;

    .line 42
    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->c:Landroid/os/Handler;

    .line 43
    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/sensors/a/ak;I)V
    .locals 6

    .prologue
    .line 104
    const-string v0, "Starting BLE scan"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->a:Lcom/google/android/gms/fitness/sensors/a/aa;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/sensors/a/aa;->a(Lcom/google/android/gms/fitness/sensors/a/ab;)Z

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->c:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/aj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/fitness/sensors/a/aj;-><init>(Lcom/google/android/gms/fitness/sensors/a/ag;Lcom/google/android/gms/fitness/sensors/a/ak;)V

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p2

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 116
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/k/k/a/af;
    .locals 3

    .prologue
    .line 47
    invoke-static {}, Lcom/google/k/k/a/aq;->b()Lcom/google/k/k/a/aq;

    move-result-object v0

    .line 49
    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/ah;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/a/ah;-><init>(Lcom/google/android/gms/fitness/sensors/a/ag;Ljava/lang/String;Lcom/google/k/k/a/aq;)V

    const/16 v2, 0xf

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/fitness/sensors/a/ag;->a(Lcom/google/android/gms/fitness/sensors/a/ak;I)V

    .line 63
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/request/StartBleScanRequest;)V
    .locals 3

    .prologue
    .line 68
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/StartBleScanRequest;->c()Lcom/google/android/gms/fitness/request/h;

    move-result-object v0

    .line 69
    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/ai;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/a/ai;-><init>(Lcom/google/android/gms/fitness/sensors/a/ag;Lcom/google/android/gms/fitness/request/StartBleScanRequest;Lcom/google/android/gms/fitness/request/h;)V

    .line 91
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/StartBleScanRequest;->b()I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/fitness/sensors/a/ag;->a(Lcom/google/android/gms/fitness/sensors/a/ak;I)V

    .line 92
    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/request/h;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/h;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-interface {p1}, Lcom/google/android/gms/fitness/request/h;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/a/ab;

    .line 98
    if-eqz v0, :cond_0

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/ag;->a:Lcom/google/android/gms/fitness/sensors/a/aa;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/sensors/a/aa;->b(Lcom/google/android/gms/fitness/sensors/a/ab;)V

    .line 101
    :cond_0
    return-void
.end method
