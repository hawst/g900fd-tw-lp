.class public final Lcom/google/android/gms/games/ui/common/matches/m;
.super Lcom/google/android/gms/games/ui/b/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 475
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 480
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/m;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/k;

    .line 481
    const/4 v1, 0x0

    .line 482
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->f(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/q;

    move-result-object v2

    instance-of v2, v2, Lcom/google/android/gms/games/ui/cg;

    if-eqz v2, :cond_0

    .line 483
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->g(Lcom/google/android/gms/games/ui/common/matches/k;)Lcom/google/android/gms/games/ui/q;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/cg;

    .line 485
    :cond_0
    packed-switch p2, :pswitch_data_0

    .line 501
    const-string v0, "InvitationListFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled dialog action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    :goto_0
    return-void

    .line 487
    :pswitch_0
    if-eqz v1, :cond_1

    .line 488
    const/16 v2, 0x15

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/ui/cg;->a(I)V

    .line 490
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->j:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/k;->b(Z)V

    goto :goto_0

    .line 494
    :pswitch_1
    if-eqz v1, :cond_2

    .line 495
    const/16 v2, 0x16

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/ui/cg;->a(I)V

    .line 497
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/k;->a()V

    goto :goto_0

    .line 485
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
