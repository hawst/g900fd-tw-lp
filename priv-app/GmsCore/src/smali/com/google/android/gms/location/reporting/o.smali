.class public final Lcom/google/android/gms/location/reporting/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/accounts/Account;

.field final b:Ljava/lang/String;

.field final c:J

.field d:J

.field e:J

.field f:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/accounts/Account;Ljava/lang/String;J)V
    .locals 3

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 228
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    iput-wide v0, p0, Lcom/google/android/gms/location/reporting/o;->d:J

    .line 224
    iput-wide v0, p0, Lcom/google/android/gms/location/reporting/o;->e:J

    .line 229
    const-string v0, "account"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/location/reporting/o;->a:Landroid/accounts/Account;

    .line 230
    const-string v0, "reason"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/location/reporting/o;->b:Ljava/lang/String;

    .line 231
    iput-wide p3, p0, Lcom/google/android/gms/location/reporting/o;->c:J

    .line 232
    return-void
.end method

.method synthetic constructor <init>(Landroid/accounts/Account;Ljava/lang/String;JB)V
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/location/reporting/o;-><init>(Landroid/accounts/Account;Ljava/lang/String;J)V

    return-void
.end method
