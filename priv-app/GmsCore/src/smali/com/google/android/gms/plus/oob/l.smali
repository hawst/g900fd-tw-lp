.class public final Lcom/google/android/gms/plus/oob/l;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"


# instance fields
.field private d:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 28
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 32
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/l;->a:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/l;->eh:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/l;->eg:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 2

    .prologue
    .line 53
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 54
    sget v0, Lcom/google/android/gms/p;->uR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/l;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/l;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/l;->d:Landroid/widget/TextView;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/l;->d:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/l;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/l;->d:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 58
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    return v0
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/l;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    return-object v0
.end method

.method protected final g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    .prologue
    .line 62
    const-string v0, "domainInfo"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/l;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 68
    :goto_0
    return-object v0

    .line 65
    :cond_0
    const-string v0, "termsOfService"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/l;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0

    .line 68
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
