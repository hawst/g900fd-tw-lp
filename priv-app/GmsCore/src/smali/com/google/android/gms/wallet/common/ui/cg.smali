.class final Lcom/google/android/gms/wallet/common/ui/cg;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/ce;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Lcom/google/android/gms/wallet/shared/common/b/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/ce;Landroid/content/Context;Ljava/util/List;)V
    .locals 2

    .prologue
    .line 503
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    .line 504
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 505
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cg;->b:Landroid/view/LayoutInflater;

    .line 506
    new-instance v0, Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-direct {v0, p2}, Lcom/google/android/gms/wallet/shared/common/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cg;->c:Lcom/google/android/gms/wallet/shared/common/b/a;

    .line 507
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cg;->c:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-static {p2}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Landroid/content/Context;)Lcom/google/android/gms/wallet/dynamite/image/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Lcom/google/android/gms/wallet/dynamite/image/a;)V

    .line 508
    return-void
.end method

.method private static a(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 564
    instance-of v0, p0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    .line 565
    const/4 v0, 0x0

    .line 569
    :goto_0
    return v0

    .line 566
    :cond_0
    instance-of v0, p0, Lcom/google/android/gms/wallet/common/ui/cf;

    if-eqz v0, :cond_1

    .line 567
    const/4 v0, 0x1

    goto :goto_0

    .line 569
    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 13

    .prologue
    .line 615
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cg;->a(I)Ljava/lang/Object;

    move-result-object v10

    .line 618
    if-eqz p3, :cond_0

    .line 619
    invoke-static {v10}, Lcom/google/android/gms/wallet/common/ui/cg;->a(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual/range {p3 .. p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/cg;->a(Ljava/lang/Object;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 620
    const/16 p3, 0x0

    .line 624
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/android/gms/wallet/common/ui/ce;)Landroid/widget/AdapterView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AdapterView;->isEnabled()Z

    move-result v12

    .line 625
    const/4 v4, 0x0

    .line 627
    instance-of v1, v10, Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_f

    .line 628
    if-eqz p3, :cond_1

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/wallet/common/ui/cg;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 629
    :cond_1
    if-eqz p2, :cond_8

    .line 630
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->b:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/ce;->d(Lcom/google/android/gms/wallet/common/ui/ce;)I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 638
    :cond_2
    :goto_0
    sget v1, Lcom/google/android/gms/j;->kt:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 640
    sget v2, Lcom/google/android/gms/j;->ky:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Landroid/widget/ImageView;

    move-object v2, v10

    .line 643
    check-cast v2, Lcom/google/checkout/inapp/proto/j;

    .line 646
    if-eqz v12, :cond_9

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v3

    if-eqz v3, :cond_9

    const/4 v3, 0x1

    move v12, v3

    .line 648
    :goto_1
    iget-object v3, v2, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 650
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->c:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-static {v9, v2, v1}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/widget/ImageView;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/common/b/a;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 652
    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 653
    iget v1, v2, Lcom/google/checkout/inapp/proto/j;->d:I

    sparse-switch v1, :sswitch_data_0

    .line 678
    const-string v1, "InstrumentSelectorCommon"

    const-string v3, "Unexpected FOP type %d , no content description set"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v7, v2, Lcom/google/checkout/inapp/proto/j;->d:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 687
    :goto_2
    if-eqz p2, :cond_14

    .line 688
    sget v1, Lcom/google/android/gms/j;->ae:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 690
    sget v3, Lcom/google/android/gms/j;->nO:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 693
    const/4 v5, 0x0

    .line 694
    const/4 v4, 0x0

    .line 695
    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/ui/ce;->g(Lcom/google/android/gms/wallet/common/ui/ce;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, v2, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v6, :cond_3

    .line 696
    iget-object v6, v2, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    .line 697
    iget-object v5, v6, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    const-string v7, ", "

    const/4 v8, 0x0

    const/4 v11, 0x0

    invoke-static {v5, v7, v8, v11}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;Ljava/lang/String;[C[C)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 699
    const/4 v5, 0x1

    .line 701
    iget-object v6, v6, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    .line 702
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 703
    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 704
    const/4 v4, 0x1

    .line 707
    :cond_3
    if-eqz v5, :cond_b

    .line 708
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 712
    :goto_3
    if-eqz v4, :cond_c

    .line 713
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 718
    :goto_4
    sget v1, Lcom/google/android/gms/j;->fC:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Landroid/widget/TextView;

    .line 719
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cg;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/ce;->h(Lcom/google/android/gms/wallet/common/ui/ce;)Z

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/ui/ce;->i(Lcom/google/android/gms/wallet/common/ui/ce;)Z

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v5}, Lcom/google/android/gms/wallet/common/ui/ce;->j(Lcom/google/android/gms/wallet/common/ui/ce;)Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Lcom/google/android/gms/wallet/common/ui/ce;)[I

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v7}, Lcom/google/android/gms/wallet/common/ui/ce;->c(Lcom/google/android/gms/wallet/common/ui/ce;)[I

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v8}, Lcom/google/android/gms/wallet/common/ui/ce;->k(Lcom/google/android/gms/wallet/common/ui/ce;)[Ljava/lang/String;

    move-result-object v8

    invoke-static/range {v1 .. v8}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/Context;Lcom/google/checkout/inapp/proto/j;ZZZ[I[I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 728
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_d

    .line 729
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/ce;->b()Landroid/graphics/ColorMatrixColorFilter;

    move-result-object v3

    invoke-virtual {v9, v3}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 730
    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 731
    const/4 v1, 0x0

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 743
    :cond_4
    :goto_5
    sget v1, Lcom/google/android/gms/j;->kl:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 744
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Lcom/google/android/gms/wallet/common/ui/ce;)[I

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/ui/ce;->c(Lcom/google/android/gms/wallet/common/ui/ce;)[I

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;[I[I)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 747
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 748
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 752
    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    move-object v4, v1

    .line 790
    :cond_5
    :goto_6
    if-eqz p3, :cond_7

    .line 791
    if-nez p2, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ce;->n(Lcom/google/android/gms/wallet/common/ui/ce;)I

    move-result v1

    if-eqz v1, :cond_6

    .line 792
    sget v1, Lcom/google/android/gms/j;->kS:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 793
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/ce;->n(Lcom/google/android/gms/wallet/common/ui/ce;)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 794
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 797
    :cond_6
    move-object/from16 v0, p3

    invoke-virtual {v0, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 798
    move-object/from16 v0, p3

    invoke-static {v0, v12}, Lcom/google/android/gms/common/util/aw;->a(Landroid/view/View;Z)V

    .line 799
    if-eqz v4, :cond_7

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_7

    .line 801
    const/4 v1, 0x1

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 804
    const/4 v1, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 808
    :cond_7
    return-object p3

    .line 633
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->b:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/ce;->e(Lcom/google/android/gms/wallet/common/ui/ce;)I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    goto/16 :goto_0

    .line 646
    :cond_9
    const/4 v3, 0x0

    move v12, v3

    goto/16 :goto_1

    .line 655
    :sswitch_0
    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/j;)Lcom/google/aa/b/a/g;

    move-result-object v1

    iget-object v1, v1, Lcom/google/aa/b/a/g;->a:Ljava/lang/String;

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 659
    :sswitch_1
    const-string v1, ""

    .line 660
    iget v3, v2, Lcom/google/checkout/inapp/proto/j;->c:I

    packed-switch v3, :pswitch_data_0

    .line 670
    const-string v3, "InstrumentSelectorCommon"

    const-string v5, "Unexpected FOP type %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, v2, Lcom/google/checkout/inapp/proto/j;->c:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    :goto_7
    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 662
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ce;->f(Lcom/google/android/gms/wallet/common/ui/ce;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->CE:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_7

    .line 666
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ce;->f(Lcom/google/android/gms/wallet/common/ui/ce;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->Co:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    goto :goto_7

    .line 684
    :cond_a
    const/16 v1, 0x8

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_2

    .line 710
    :cond_b
    const/16 v5, 0x8

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 715
    :cond_c
    const/16 v1, 0x8

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 734
    :cond_d
    const/4 v1, 0x0

    invoke-virtual {v9, v1}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 735
    const/16 v1, 0x8

    invoke-virtual {v11, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 736
    sget v1, Lcom/google/android/gms/j;->qm:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioButton;

    .line 738
    if-eqz v1, :cond_4

    .line 739
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setVisibility(I)V

    goto/16 :goto_5

    .line 754
    :cond_e
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 755
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    :goto_8
    move-object v4, v1

    .line 758
    goto/16 :goto_6

    :cond_f
    instance-of v1, v10, Lcom/google/android/gms/wallet/common/ui/cf;

    if-eqz v1, :cond_5

    .line 759
    if-eqz p3, :cond_10

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/wallet/common/ui/cg;->a(Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 760
    :cond_10
    if-eqz p2, :cond_12

    .line 761
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->b:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/ce;->l(Lcom/google/android/gms/wallet/common/ui/ce;)I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    :cond_11
    :goto_9
    move-object v1, v10

    .line 776
    check-cast v1, Lcom/google/android/gms/wallet/common/ui/cf;

    .line 777
    sget v2, Lcom/google/android/gms/j;->eg:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 778
    iget-object v3, v1, Lcom/google/android/gms/wallet/common/ui/cf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 779
    iget v3, v1, Lcom/google/android/gms/wallet/common/ui/cf;->c:I

    if-lez v3, :cond_13

    .line 780
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cg;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    const/4 v5, 0x1

    new-array v5, v5, [I

    const/4 v6, 0x0

    iget v1, v1, Lcom/google/android/gms/wallet/common/ui/cf;->c:I

    aput v1, v5, v6

    invoke-virtual {v3, v5}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 782
    const/4 v3, 0x0

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v2, v3, v5, v6, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 784
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    goto/16 :goto_6

    .line 764
    :cond_12
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cg;->b:Landroid/view/LayoutInflater;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/ce;->m(Lcom/google/android/gms/wallet/common/ui/ce;)I

    move-result v2

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p3

    .line 767
    sget v1, Lcom/google/android/gms/j;->fO:I

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 769
    if-eqz v1, :cond_11

    .line 770
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cg;->getContext()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->Bx:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 786
    :cond_13
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v2, v1, v3, v5, v6}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_6

    :cond_14
    move-object v1, v4

    goto/16 :goto_8

    .line 653
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_1
    .end sparse-switch

    .line 660
    :pswitch_data_0
    .packed-switch 0x20
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/view/View;)Z
    .locals 2

    .prologue
    .line 818
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/checkout/inapp/proto/j;)I
    .locals 6

    .prologue
    const/4 v2, -0x1

    .line 535
    if-eqz p1, :cond_2

    .line 536
    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cg;->getCount()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_2

    .line 537
    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/cg;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 538
    instance-of v4, v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v4, :cond_1

    .line 539
    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 540
    iget-object v4, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 541
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 549
    :goto_1
    return v0

    :cond_0
    move v0, v2

    .line 544
    goto :goto_1

    .line 536
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 549
    goto :goto_1
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 523
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ch;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/android/gms/wallet/common/ui/ch;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 577
    if-nez p1, :cond_0

    .line 578
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cg;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 579
    instance-of v1, v0, Lcom/google/android/gms/wallet/common/ui/cf;

    if-eqz v1, :cond_0

    .line 580
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cf;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/ui/cf;->b:Z

    if-nez v0, :cond_0

    .line 581
    new-instance v0, Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cg;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 585
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/cg;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 559
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cg;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ch;

    .line 560
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/google/android/gms/wallet/common/ui/ch;->b:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 590
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p3}, Lcom/google/android/gms/wallet/common/ui/cg;->a(IZLandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x1

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 596
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cg;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 597
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/ce;->a(Lcom/google/android/gms/wallet/common/ui/ce;)Landroid/widget/AdapterView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/AdapterView;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 609
    :goto_0
    return v0

    .line 599
    :cond_0
    instance-of v2, v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v2, :cond_3

    .line 600
    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 601
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/ce;->b(Lcom/google/android/gms/wallet/common/ui/ce;)[I

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cg;->a:Lcom/google/android/gms/wallet/common/ui/ce;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ui/ce;->c(Lcom/google/android/gms/wallet/common/ui/ce;)[I

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;[I[I)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    .line 606
    :cond_3
    instance-of v2, v0, Lcom/google/android/gms/wallet/common/ui/cf;

    if-eqz v2, :cond_4

    .line 607
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cf;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/ui/cf;->b:Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 609
    goto :goto_0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 512
    invoke-super {p0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 513
    return-void
.end method
