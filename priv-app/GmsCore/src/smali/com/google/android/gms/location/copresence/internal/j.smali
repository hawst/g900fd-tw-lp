.class public abstract Lcom/google/android/gms/location/copresence/internal/j;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/location/copresence/internal/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceCallbacks"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/location/copresence/internal/j;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/location/copresence/internal/i;
    .locals 2

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceCallbacks"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/location/copresence/internal/i;

    if-eqz v1, :cond_1

    .line 33
    check-cast v0, Lcom/google/android/gms/location/copresence/internal/i;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/location/copresence/internal/k;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 97
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.location.copresence.internal.ICopresenceCallbacks"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 48
    goto :goto_0

    .line 52
    :sswitch_1
    const-string v2, "com.google.android.gms.location.copresence.internal.ICopresenceCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    sget-object v0, Lcom/google/android/gms/common/api/Status;->CREATOR:Lcom/google/android/gms/common/api/as;

    invoke-static {p2}, Lcom/google/android/gms/common/api/as;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 60
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/location/copresence/internal/j;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 61
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 62
    goto :goto_0

    .line 66
    :sswitch_2
    const-string v2, "com.google.android.gms.location.copresence.internal.ICopresenceCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 68
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 71
    sget-object v0, Lcom/google/android/gms/location/copresence/CopresenceSettings;->CREATOR:Lcom/google/android/gms/location/copresence/m;

    invoke-static {p2}, Lcom/google/android/gms/location/copresence/m;->a(Landroid/os/Parcel;)Lcom/google/android/gms/location/copresence/CopresenceSettings;

    move-result-object v0

    .line 76
    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/location/copresence/internal/j;->a(ILcom/google/android/gms/location/copresence/CopresenceSettings;)V

    .line 77
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 78
    goto :goto_0

    .line 82
    :sswitch_3
    const-string v2, "com.google.android.gms.location.copresence.internal.ICopresenceCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 86
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 87
    sget-object v0, Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;->CREATOR:Lcom/google/android/gms/location/copresence/debug/b;

    invoke-static {p2}, Lcom/google/android/gms/location/copresence/debug/b;->a(Landroid/os/Parcel;)Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;

    move-result-object v0

    .line 92
    :cond_2
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/location/copresence/internal/j;->a(ILcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;)V

    .line 93
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 94
    goto :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x5 -> :sswitch_2
        0x9 -> :sswitch_3
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
