.class public final Lcom/google/android/gms/auth/be/a/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/auth/be/a/a/b;


# instance fields
.field public a:I

.field public b:I

.field public c:[B

.field public d:[[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 36
    iput v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    iput v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->g:[[B

    iput-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->cachedSize:I

    .line 37
    return-void
.end method

.method public static a()[Lcom/google/android/gms/auth/be/a/a/b;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/gms/auth/be/a/a/b;->e:[Lcom/google/android/gms/auth/be/a/a/b;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/be/a/a/b;->e:[Lcom/google/android/gms/auth/be/a/a/b;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/auth/be/a/a/b;

    sput-object v0, Lcom/google/android/gms/auth/be/a/a/b;->e:[Lcom/google/android/gms/auth/be/a/a/b;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/gms/auth/be/a/a/b;->e:[Lcom/google/android/gms/auth/be/a/a/b;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 74
    iget v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    if-eqz v2, :cond_0

    .line 75
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 78
    :cond_0
    iget v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    if-eqz v2, :cond_1

    .line 79
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 82
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    sget-object v3, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_2

    .line 83
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 89
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 90
    iget-object v4, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    aget-object v4, v4, v1

    .line 91
    if-eqz v4, :cond_3

    .line 92
    add-int/lit8 v3, v3, 0x1

    .line 93
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b([B)I

    move-result v4

    add-int/2addr v2, v4

    .line 89
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 97
    :cond_4
    add-int/2addr v0, v2

    .line 98
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 100
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 51
    iget v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 54
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    if-eqz v0, :cond_1

    .line 55
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 58
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 60
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v0, v0

    if-lez v0, :cond_4

    .line 61
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 62
    iget-object v1, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    aget-object v1, v1, v0

    .line 63
    if-eqz v1, :cond_3

    .line 64
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 61
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 69
    return-void
.end method
