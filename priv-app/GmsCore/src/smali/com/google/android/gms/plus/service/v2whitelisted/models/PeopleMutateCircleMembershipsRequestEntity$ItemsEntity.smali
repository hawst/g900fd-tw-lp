.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/service/v2whitelisted/models/p;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/u;

.field private static final f:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

.field e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 116
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/u;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/u;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/u;

    .line 133
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 136
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->f:Ljava/util/HashMap;

    const-string v1, "circleId"

    const-string v2, "circleId"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->f:Ljava/util/HashMap;

    const-string v1, "circleMemberId"

    const-string v2, "circleMemberId"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->f:Ljava/util/HashMap;

    const-string v1, "mutationType"

    const-string v2, "mutationType"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 176
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->b:I

    .line 177
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a:Ljava/util/Set;

    .line 178
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 188
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a:Ljava/util/Set;

    .line 189
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->b:I

    .line 190
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->c:Ljava/lang/String;

    .line 191
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    .line 192
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->e:Ljava/lang/String;

    .line 193
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 202
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a:Ljava/util/Set;

    .line 203
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->b:I

    .line 204
    iput-object p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->c:Ljava/lang/String;

    .line 205
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    .line 206
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->e:Ljava/lang/String;

    .line 207
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 145
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 612
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 613
    packed-switch v0, :pswitch_data_0

    .line 618
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 615
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    .line 622
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 623
    return-void

    .line 613
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 594
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 595
    packed-switch v0, :pswitch_data_0

    .line 603
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 597
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->c:Ljava/lang/String;

    .line 606
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 607
    return-void

    .line 600
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 595
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 566
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 574
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->c:Ljava/lang/String;

    .line 572
    :goto_0
    return-object v0

    .line 570
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    goto :goto_0

    .line 572
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 566
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 112
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 551
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/u;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 651
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 682
    :goto_0
    return v0

    .line 656
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 657
    goto :goto_0

    .line 660
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;

    .line 661
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 662
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 663
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 665
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 667
    goto :goto_0

    :cond_3
    move v0, v1

    .line 672
    goto :goto_0

    .line 675
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 677
    goto :goto_0

    :cond_5
    move v0, v2

    .line 682
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 638
    const/4 v0, 0x0

    .line 639
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 640
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 641
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 642
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 644
    goto :goto_0

    .line 645
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 556
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/u;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/u;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;Landroid/os/Parcel;I)V

    .line 557
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 633
    const/4 v0, 0x1

    return v0
.end method
