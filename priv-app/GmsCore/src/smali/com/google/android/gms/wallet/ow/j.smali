.class final Lcom/google/android/gms/wallet/ow/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cd;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 0

    .prologue
    .line 1364
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->f(Lcom/google/android/gms/wallet/ow/g;)V

    .line 1406
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 3

    .prologue
    .line 1370
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    if-eq p1, v0, :cond_0

    .line 1371
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    .line 1375
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-nez v0, :cond_2

    .line 1377
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eqz v0, :cond_1

    .line 1401
    :goto_0
    return-void

    .line 1380
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->i:Lcom/google/checkout/inapp/proto/j;

    .line 1382
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1397
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 1398
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/android/gms/wallet/ow/g;)V

    goto :goto_0

    .line 1385
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_3

    .line 1386
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/j;)V

    goto :goto_0

    .line 1390
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    .line 1391
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Z)V

    goto :goto_0

    .line 1382
    nop

    :pswitch_data_0
    .packed-switch 0x75
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;)V
    .locals 2

    .prologue
    .line 1410
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1424
    :goto_0
    return-void

    .line 1412
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/android/gms/wallet/common/ui/cz;)V

    goto :goto_0

    .line 1416
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/cz;->b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/android/gms/wallet/common/ui/cz;)V

    goto :goto_0

    .line 1420
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/j;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->g(Lcom/google/android/gms/wallet/ow/g;)Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/cz;->c(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/android/gms/wallet/common/ui/cz;)V

    goto :goto_0

    .line 1410
    :pswitch_data_0
    .packed-switch 0x79
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
