.class public final Lcom/google/android/gms/common/images/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static c:Lcom/google/android/gms/common/images/a;


# instance fields
.field private final b:Lcom/google/android/gms/common/server/n;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "url"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "local"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/common/images/a;->a:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;)Lcom/google/android/gms/common/server/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/images/a;->b:Lcom/google/android/gms/common/server/n;

    .line 48
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;
    .locals 2

    .prologue
    .line 40
    const-class v1, Lcom/google/android/gms/common/images/a;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/images/a;->c:Lcom/google/android/gms/common/images/a;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/google/android/gms/common/images/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/images/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/common/images/a;->c:Lcom/google/android/gms/common/images/a;

    .line 43
    :cond_0
    sget-object v0, Lcom/google/android/gms/common/images/a;->c:Lcom/google/android/gms/common/images/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->b:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 177
    :goto_0
    return-object v0

    .line 176
    :catch_0
    move-exception v0

    const-string v0, "ImageBroker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error retrieving image at URL "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 136
    .line 138
    const-string v1, "android.resource"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 160
    :cond_0
    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    :cond_1
    :goto_1
    return-object v3

    .line 143
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 144
    sget-object v2, Lcom/google/android/gms/common/images/a;->a:[Ljava/lang/String;

    move-object v1, p1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 146
    if-eqz v1, :cond_3

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 148
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 155
    if-eqz v1, :cond_0

    .line 156
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 151
    :cond_3
    :try_start_1
    const-string v0, "ImageBroker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No image store record found for image ID "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 152
    if-eqz v1, :cond_1

    .line 156
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 155
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 156
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/common/images/a;->b:Lcom/google/android/gms/common/server/n;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/common/server/n;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 191
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 93
    invoke-static {p1, p2}, Lcom/google/android/gms/common/images/a;->b(Landroid/content/Context;Landroid/net/Uri;)Landroid/util/Pair;

    move-result-object v2

    .line 94
    if-nez v2, :cond_0

    move-object v0, v1

    .line 123
    :goto_0
    return-object v0

    .line 98
    :cond_0
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 99
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 102
    if-nez v3, :cond_2

    .line 103
    if-nez v0, :cond_1

    .line 104
    const-string v0, "ImageBroker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No local image data and no external image URL found for image "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_1
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B

    move-result-object v0

    if-nez v0, :cond_3

    move-object v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_2
    const/4 v2, 0x2

    if-ne v3, v2, :cond_3

    if-eqz v0, :cond_3

    .line 115
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/images/a;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V

    .line 120
    :cond_3
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "r"

    invoke-virtual {v0, p2, v2}, Landroid/content/ContentResolver;->openAssetFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 122
    :catch_0
    move-exception v0

    const-string v0, "ImageBroker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fetched image data was not stored for image URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 123
    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 62
    const-string v0, "url IN "

    invoke-static {v0, p3}, Lcom/google/android/gms/common/e/d;->a(Ljava/lang/String;Ljava/util/Collection;)Lcom/google/android/gms/common/e/d;

    move-result-object v1

    .line 63
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/common/images/a;->a:[Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/common/e/d;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/gms/common/e/d;->a:[Ljava/lang/String;

    const/4 v5, 0x0

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 66
    if-eqz v1, :cond_2

    .line 67
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    const/4 v0, 0x1

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 69
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 70
    if-eq v0, v6, :cond_0

    if-eqz v2, :cond_0

    .line 71
    invoke-direct {p0, p1, p2, v2}, Lcom/google/android/gms/common/images/a;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 77
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 76
    :cond_2
    if-eqz v1, :cond_3

    .line 77
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 80
    :cond_3
    return-void
.end method
