.class final Lcom/google/android/gms/smart_profile/aw;
.super Landroid/view/animation/Animation;
.source "SourceFile"


# instance fields
.field final synthetic a:F

.field final synthetic b:F

.field final synthetic c:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field final synthetic d:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;FFLandroid/view/animation/AccelerateDecelerateInterpolator;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/aw;->d:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    iput p2, p0, Lcom/google/android/gms/smart_profile/aw;->a:F

    iput p3, p0, Lcom/google/android/gms/smart_profile/aw;->b:F

    iput-object p4, p0, Lcom/google/android/gms/smart_profile/aw;->c:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    return-void
.end method


# virtual methods
.method protected final applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 5

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/aw;->d:Lcom/google/android/gms/smart_profile/SmartProfileContainerView;

    iget v1, p0, Lcom/google/android/gms/smart_profile/aw;->a:F

    iget v2, p0, Lcom/google/android/gms/smart_profile/aw;->b:F

    iget v3, p0, Lcom/google/android/gms/smart_profile/aw;->a:F

    sub-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/aw;->c:Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-virtual {v4, p1}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v4

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/SmartProfileContainerView;->a(Lcom/google/android/gms/smart_profile/SmartProfileContainerView;F)V

    .line 407
    return-void
.end method
