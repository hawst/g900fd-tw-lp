.class public final Lcom/google/android/gms/games/ui/cs;
.super Landroid/support/v7/widget/aa;
.source "SourceFile"


# instance fields
.field final o:Ljava/util/ArrayList;

.field final p:Ljava/util/ArrayList;

.field private final q:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v7/widget/aa;-><init>()V

    .line 26
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/cs;->p:Ljava/util/ArrayList;

    return-void
.end method

.method static a(Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Landroid/view/View;->setAlpha(F)V

    .line 146
    invoke-virtual {p0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 147
    invoke-virtual {p0, v1}, Landroid/view/View;->setRotation(F)V

    .line 148
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/cs;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/cs;->f()V

    return-void
.end method

.method private f()V
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/cs;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 140
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/cs;->e()V

    .line 142
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Landroid/support/v7/widget/aa;->a()V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 37
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 40
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 42
    new-instance v1, Lcom/google/android/gms/games/ui/ct;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/games/ui/ct;-><init>(Lcom/google/android/gms/games/ui/cs;Ljava/util/ArrayList;)V

    .line 51
    invoke-interface {v1}, Ljava/lang/Runnable;->run()V

    .line 53
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Landroid/support/v7/widget/aa;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/cs;)Z
    .locals 2

    .prologue
    .line 58
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/games/ui/ae;

    if-eqz v0, :cond_0

    move-object v0, p1

    .line 59
    check-cast v0, Lcom/google/android/gms/games/ui/ae;

    .line 60
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ae;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/cs;->c(Landroid/support/v7/widget/cs;)V

    .line 62
    iget-object v0, p1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    const/4 v0, 0x1

    .line 67
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/widget/aa;->b(Landroid/support/v7/widget/cs;)Z

    move-result v0

    goto :goto_0
.end method

.method public final c(Landroid/support/v7/widget/cs;)V
    .locals 4

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/support/v7/widget/aa;->c(Landroid/support/v7/widget/cs;)V

    .line 103
    iget-object v2, p1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 105
    invoke-static {v2}, Lcom/google/android/gms/games/ui/cs;->a(Landroid/view/View;)V

    .line 106
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/cs;->f(Landroid/support/v7/widget/cs;)V

    .line 108
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 110
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 111
    invoke-static {v2}, Lcom/google/android/gms/games/ui/cs;->a(Landroid/view/View;)V

    .line 112
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/cs;->f(Landroid/support/v7/widget/cs;)V

    .line 113
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 114
    iget-object v3, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 108
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "after animation is cancelled, item should not be in mAddAnimations list"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/cs;->f()V

    .line 125
    return-void
.end method

.method public final d()V
    .locals 5

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 153
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    .line 155
    iget-object v2, v0, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 156
    invoke-static {v2}, Lcom/google/android/gms/games/ui/cs;->a(Landroid/view/View;)V

    .line 157
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/cs;->f(Landroid/support/v7/widget/cs;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->q:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 153
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 161
    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_3

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 163
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 164
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    :goto_2
    if-ltz v2, :cond_2

    .line 165
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/cs;

    .line 166
    iget-object v4, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 167
    invoke-static {v4}, Lcom/google/android/gms/games/ui/cs;->a(Landroid/view/View;)V

    .line 168
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/cs;->f(Landroid/support/v7/widget/cs;)V

    .line 169
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 170
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 171
    iget-object v1, p0, Lcom/google/android/gms/games/ui/cs;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 164
    :cond_1
    add-int/lit8 v1, v2, -0x1

    move v2, v1

    goto :goto_2

    .line 161
    :cond_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    .line 175
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/cs;->p:Ljava/util/ArrayList;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_4

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    iget-object v0, v0, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 177
    :cond_4
    invoke-super {p0}, Landroid/support/v7/widget/aa;->d()V

    .line 178
    return-void
.end method
