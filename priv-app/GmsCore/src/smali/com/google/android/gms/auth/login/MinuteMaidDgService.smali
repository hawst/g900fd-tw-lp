.class public Lcom/google/android/gms/auth/login/MinuteMaidDgService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/droidguard/b;

.field private b:Lcom/google/android/gms/auth/login/av;

.field private final c:Lcom/google/android/gms/auth/login/ah;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 21
    iput-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->a:Lcom/google/android/gms/droidguard/b;

    .line 22
    iput-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->b:Lcom/google/android/gms/auth/login/av;

    .line 23
    new-instance v0, Lcom/google/android/gms/auth/login/au;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/login/au;-><init>(Lcom/google/android/gms/auth/login/MinuteMaidDgService;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->c:Lcom/google/android/gms/auth/login/ah;

    .line 53
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/MinuteMaidDgService;)Lcom/google/android/gms/droidguard/b;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->a:Lcom/google/android/gms/droidguard/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/login/MinuteMaidDgService;Lcom/google/android/gms/droidguard/b;)Lcom/google/android/gms/droidguard/b;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->a:Lcom/google/android/gms/droidguard/b;

    return-object p1
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/gms/auth/login/av;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/auth/login/av;-><init>(Lcom/google/android/gms/auth/login/MinuteMaidDgService;Lcom/google/android/gms/auth/login/MinuteMaidDgService;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->b:Lcom/google/android/gms/auth/login/av;

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->b:Lcom/google/android/gms/auth/login/av;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/av;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->c:Lcom/google/android/gms/auth/login/ah;

    return-object v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->a:Lcom/google/android/gms/droidguard/b;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/auth/login/MinuteMaidDgService;->a:Lcom/google/android/gms/droidguard/b;

    invoke-interface {v0}, Lcom/google/android/gms/droidguard/b;->a()V

    .line 46
    const-string v0, "MM-DG-SERVICE"

    const-string v1, "Closing DgHandle."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Service;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0

    .line 48
    :cond_0
    const-string v0, "MM-DG-SERVICE"

    const-string v1, "DgHandle was null when unbinding."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
