.class public final Lcom/google/android/gms/car/bd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field private final b:Lcom/google/android/gms/car/bc;

.field private final c:Lcom/google/android/gms/car/kj;

.field private d:Ljava/io/InputStream;

.field private volatile e:I

.field private final f:Lcom/google/android/gms/car/be;


# direct methods
.method private declared-synchronized b()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 93
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/bd;->e:I

    if-eq v0, v4, :cond_1

    .line 94
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    const-string v0, "CAR.AUDIO"

    const-string v1, "stop while not started"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 111
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 99
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/gms/car/bd;->e:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 101
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/bd;->c:Lcom/google/android/gms/car/kj;

    iget-object v1, p0, Lcom/google/android/gms/car/bd;->f:Lcom/google/android/gms/car/be;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/kj;->b(Lcom/google/android/gms/car/km;)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/car/bd;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 108
    :cond_2
    :goto_1
    :try_start_3
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "CAR.AUDIO"

    const-string v1, "stopped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 103
    :catch_0
    move-exception v0

    .line 104
    :try_start_4
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RemoteException from car service:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/google/android/gms/car/bd;->e:I

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/bd;->d:Ljava/io/InputStream;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    if-eqz v0, :cond_3

    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/car/bd;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_3
    :goto_2
    :try_start_6
    iget v0, p0, Lcom/google/android/gms/car/bd;->e:I

    if-eq v0, v5, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/bd;->b:Lcom/google/android/gms/car/bc;

    iget v1, p0, Lcom/google/android/gms/car/bd;->a:I

    if-eqz v1, :cond_4

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong stream type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/bd;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v1, v0, Lcom/google/android/gms/car/bc;->b:Ljava/lang/Object;

    monitor-enter v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :try_start_7
    iget-object v0, v0, Lcom/google/android/gms/car/bc;->c:Ljava/util/LinkedList;

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/4 v0, 0x2

    :try_start_8
    iput v0, p0, Lcom/google/android/gms/car/bd;->e:I

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 134
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/bd;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_1

    .line 150
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 137
    :cond_1
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/car/bd;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 138
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/bd;->c:Lcom/google/android/gms/car/kj;

    iget-object v1, p0, Lcom/google/android/gms/car/bd;->f:Lcom/google/android/gms/car/be;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/kj;->d(Lcom/google/android/gms/car/km;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 146
    :goto_1
    const/4 v0, 0x2

    :try_start_3
    iput v0, p0, Lcom/google/android/gms/car/bd;->e:I

    .line 147
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    const-string v0, "CAR.AUDIO"

    const-string v1, "released"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
