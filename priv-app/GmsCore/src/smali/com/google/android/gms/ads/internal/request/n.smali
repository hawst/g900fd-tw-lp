.class final Lcom/google/android/gms/ads/internal/request/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/request/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/k;)V
    .locals 0

    .prologue
    .line 187
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v6, v0, Lcom/google/android/gms/ads/internal/request/k;->c:Ljava/lang/Object;

    monitor-enter v6

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    .line 194
    monitor-exit v6

    .line 213
    :goto_0
    return-void

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/p/e;)V

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->e:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_1

    .line 200
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Loading URL in WebView: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/a;->loadUrl(Ljava/lang/String;)V

    .line 213
    :goto_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 203
    :cond_1
    :try_start_1
    const-string v0, "Loading HTML in WebView."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/k;->b:Lcom/google/android/gms/ads/internal/p/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/g;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/n;->a:Lcom/google/android/gms/ads/internal/request/k;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/k;->d:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->c:Ljava/lang/String;

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/p/a;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method
