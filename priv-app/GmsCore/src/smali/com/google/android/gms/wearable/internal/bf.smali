.class final Lcom/google/android/gms/wearable/internal/bf;
.super Lcom/google/android/gms/wearable/internal/be;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0, p1}, Lcom/google/android/gms/wearable/internal/be;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 78
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/internal/GetConnectedNodesResponse;)V
    .locals 3

    .prologue
    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 83
    iget-object v1, p1, Lcom/google/android/gms/wearable/internal/GetConnectedNodesResponse;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 84
    new-instance v1, Lcom/google/android/gms/wearable/internal/au;

    iget v2, p1, Lcom/google/android/gms/wearable/internal/GetConnectedNodesResponse;->b:I

    invoke-static {v2}, Lcom/google/android/gms/wearable/internal/ba;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wearable/internal/au;-><init>(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wearable/internal/bf;->a(Ljava/lang/Object;)V

    .line 86
    return-void
.end method
