.class final Lcom/google/android/gms/photos/autobackup/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/service/a/g;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/service/a/a;

.field final synthetic b:Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

.field final synthetic c:Lcom/google/android/gms/photos/autobackup/p;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/p;Lcom/google/android/gms/photos/autobackup/service/a/a;Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;)V
    .locals 0

    .prologue
    .line 68
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/q;->c:Lcom/google/android/gms/photos/autobackup/p;

    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/q;->a:Lcom/google/android/gms/photos/autobackup/service/a/a;

    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/q;->b:Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/gms/photos/autobackup/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/r;-><init>(Lcom/google/android/gms/photos/autobackup/q;)V

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/q;->c:Lcom/google/android/gms/photos/autobackup/p;

    iget-object v1, v1, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/service/a/b;->c()Landroid/os/Bundle;

    move-result-object v1

    .line 94
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/q;->b:Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    new-instance v3, Lcom/google/android/gms/photos/autobackup/y;

    iget-object v4, p0, Lcom/google/android/gms/photos/autobackup/q;->a:Lcom/google/android/gms/photos/autobackup/service/a/a;

    invoke-direct {v3, v0, v1, v4}, Lcom/google/android/gms/photos/autobackup/y;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;Landroid/os/Bundle;Lcom/google/android/gms/photos/autobackup/service/a/a;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 97
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 102
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/q;->c:Lcom/google/android/gms/photos/autobackup/p;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/p;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILjava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 108
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/q;->c:Lcom/google/android/gms/photos/autobackup/p;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->e()V

    .line 109
    return-void

    .line 106
    :catch_0
    move-exception v0

    const-string v0, "GetAutoBackupSettingsOp"

    const-string v1, "Failed trying to deliver failure"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
