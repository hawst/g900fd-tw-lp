.class final Lcom/google/android/gms/wallet/dynamite/ui/c;
.super Lcom/google/android/gms/wallet/dynamite/image/d;
.source "SourceFile"


# static fields
.field static g:Lcom/google/android/gms/wallet/dynamite/ui/c;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/dynamite/image/d;-><init>(Landroid/content/Context;)V

    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/dynamite/ui/c;->a()V

    .line 37
    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Landroid/content/Context;)Lcom/google/android/gms/wallet/dynamite/image/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/ui/c;->a(Lcom/google/android/gms/wallet/dynamite/image/a;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 22
    check-cast p1, Lcom/google/android/gms/wallet/dynamite/ui/d;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/ui/c;->c:Landroid/content/res/Resources;

    iget v0, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->e:I

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/gms/h;->dr:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iget v0, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->e:I

    packed-switch v0, :pswitch_data_1

    sget v0, Lcom/google/android/gms/h;->dn:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    iget v0, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->e:I

    packed-switch v0, :pswitch_data_2

    sget v0, Lcom/google/android/gms/h;->dH:I

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v4, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->b:I

    iget v5, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->a:I

    iget v6, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->b:I

    invoke-virtual {v2, v4, v7, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget v4, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->b:I

    iget v5, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->b:I

    invoke-virtual {v3, v7, v7, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget v4, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->b:I

    int-to-float v4, v4

    const v5, 0x3e19999a    # 0.15f

    mul-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    iget v5, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->b:I

    sub-int/2addr v5, v4

    invoke-virtual {v0, v4, v4, v5, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget v4, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->a:I

    iget v5, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->b:I

    sget-object v6, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v5, v6}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v4

    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v4}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v5}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v3, v5}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->d:Lcom/google/android/gms/wallet/dynamite/ui/b;

    iget v2, p1, Lcom/google/android/gms/wallet/dynamite/ui/d;->c:I

    invoke-interface {v0, v1, v5, v2}, Lcom/google/android/gms/wallet/dynamite/ui/b;->a(Landroid/content/res/Resources;Landroid/graphics/Canvas;I)V

    return-object v4

    :pswitch_0
    sget v0, Lcom/google/android/gms/h;->dr:I

    goto :goto_0

    :pswitch_1
    sget v0, Lcom/google/android/gms/h;->ds:I

    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/gms/h;->dt:I

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/gms/h;->dn:I

    goto :goto_1

    :pswitch_4
    sget v0, Lcom/google/android/gms/h;->do:I

    goto :goto_1

    :pswitch_5
    sget v0, Lcom/google/android/gms/h;->dp:I

    goto :goto_1

    :pswitch_6
    sget v0, Lcom/google/android/gms/h;->dH:I

    goto :goto_2

    :pswitch_7
    sget v0, Lcom/google/android/gms/h;->dI:I

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_6
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
