.class final Lcom/google/android/gms/wallet/common/ui/bo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bo;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/bo;->a:Ljava/util/List;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v2

    .line 218
    :goto_0
    if-ge v1, v3, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bo;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bk;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bk;->onAnimationEnd()V

    .line 218
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bo;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Z)Z

    .line 222
    return-void
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 213
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bo;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Z)Z

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 207
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bo;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bk;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bk;->onAnimationStart()V

    .line 207
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 210
    :cond_0
    return-void
.end method
