.class public Lcom/google/android/gms/plus/oob/PlusActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/plus/internal/PlusCommonExtras;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 91
    if-nez p1, :cond_0

    .line 92
    const-string v1, "OutOfBox"

    const-string v2, "Out of box flow must be initiated by a startActivityForResult call."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :goto_0
    return v0

    .line 96
    :cond_0
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    const/4 v0, 0x1

    goto :goto_0

    .line 98
    :catch_0
    move-exception v1

    const-string v1, "OutOfBox"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Package for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 35
    if-eqz p1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/oob/PlusActivity;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 42
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/oob/PlusActivity;->setResult(I)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->finish()V

    goto :goto_0

    .line 47
    :cond_2
    if-nez p1, :cond_0

    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/PlusActivity;->a:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/PlusActivity;->a:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    const-string v2, ""

    invoke-static {p0, v0, v2}, Lcom/google/android/gms/plus/k;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;Ljava/lang/String;)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/PlusActivity;->a:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b()Ljava/lang/String;

    move-result-object v0

    .line 54
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.GPSRC"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/plus/oob/PlusActivity;->a:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "com.google.android.gms.plus.OVERRIDE_THEME"

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "com.google.android.gms.common.oob.EXTRA_BACK_BUTTON_NAME"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 62
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v5, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_TEXT"

    invoke-virtual {v0, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v6, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_PACKAGE"

    invoke-virtual {v0, v6}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v7, "com.google.android.gms.plus.intent.extra.CLIENT_CALLING_PACKAGE"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_4

    move-object v0, v1

    .line 73
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/PlusActivity;->a:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-static {p0, v2, v0, v1, v3}, Lcom/google/android/gms/plus/oob/UpgradeAccountActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/PlusCommonExtras;I)Landroid/content/Intent;

    move-result-object v0

    .line 75
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 76
    const-string v1, "com.google.android.gms.common.oob.EXTRA_BACK_BUTTON_NAME"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    :cond_5
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 79
    const-string v1, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_TEXT"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 81
    :cond_6
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 82
    const-string v1, "com.google.android.gms.common.oob.EXTRAS_PROMO_APP_PACKAGE"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 85
    :cond_7
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/PlusActivity;->startActivity(Landroid/content/Intent;)V

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/PlusActivity;->finish()V

    goto/16 :goto_0
.end method
