.class final Lcom/google/android/gms/fitness/service/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic c:Lcom/google/android/gms/fitness/service/i;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/service/i;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicInteger;)V
    .locals 0

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/u;->c:Lcom/google/android/gms/fitness/service/i;

    iput-object p2, p0, Lcom/google/android/gms/fitness/service/u;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/fitness/service/u;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 348
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/u;->c:Lcom/google/android/gms/fitness/service/i;

    invoke-static {v0}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/l/z;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/l/z;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/c;

    .line 350
    iget-object v2, v0, Lcom/google/android/gms/fitness/c/c;->c:Lcom/google/android/gms/fitness/c/e;

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    if-ne v2, v3, :cond_0

    .line 351
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/u;->c:Lcom/google/android/gms/fitness/service/i;

    invoke-static {v2}, Lcom/google/android/gms/fitness/service/i;->d(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/k/a;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/c/c;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/c;

    .line 352
    iget-object v3, v0, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    .line 354
    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v3

    .line 355
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/fitness/service/u;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 357
    iget-object v3, p0, Lcom/google/android/gms/fitness/service/u;->c:Lcom/google/android/gms/fitness/service/i;

    invoke-static {v3}, Lcom/google/android/gms/fitness/service/i;->e(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/service/av;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/service/av;->a(Lcom/google/android/gms/fitness/c/c;)Lcom/google/k/k/a/af;
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 362
    :catch_0
    move-exception v0

    const-string v1, "Failed to recreate device registrations, retrying..."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/u;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    .line 365
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->ac:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/u;->c:Lcom/google/android/gms/fitness/service/i;

    invoke-static {v0}, Lcom/google/android/gms/fitness/service/i;->c(Lcom/google/android/gms/fitness/service/i;)Landroid/os/Handler;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->ad:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 369
    :cond_2
    return-void
.end method
