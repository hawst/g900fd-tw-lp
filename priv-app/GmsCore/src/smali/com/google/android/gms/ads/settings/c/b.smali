.class public final Lcom/google/android/gms/ads/settings/c/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Landroid/content/SharedPreferences;


# instance fields
.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/android/volley/s;

.field private final f:Ljava/util/Set;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/settings/c/b;->a:Ljava/lang/Object;

    .line 38
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "ads_prefs_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->f:Ljava/util/Set;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/ads/settings/c/b;->g:I

    .line 74
    iput-object p1, p0, Lcom/google/android/gms/ads/settings/c/b;->d:Landroid/content/Context;

    .line 75
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->e:Lcom/android/volley/s;

    .line 76
    new-instance v0, Lcom/google/android/gms/ads/settings/c/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/settings/c/c;-><init>(Lcom/google/android/gms/ads/settings/c/b;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->c:Landroid/content/BroadcastReceiver;

    .line 89
    return-void
.end method

.method private static a(Z)V
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "stop_scheduled"

    invoke-interface {v0, v1, p0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 135
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/settings/c/d;

    .line 240
    invoke-interface {v0, p1}, Lcom/google/android/gms/ads/settings/c/d;->a(I)V

    goto :goto_0

    .line 242
    :cond_0
    return-void
.end method

.method public static c()Z
    .locals 3

    .prologue
    .line 125
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    const-string v1, "ads_opt_in_in_progress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static d()Z
    .locals 3

    .prologue
    .line 129
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    const-string v1, "ads_opt_out_in_progress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static f()V
    .locals 2

    .prologue
    .line 225
    sget-object v1, Lcom/google/android/gms/ads/settings/c/b;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 226
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 227
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static g()Z
    .locals 3

    .prologue
    .line 231
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    const-string v1, "ads_opt_in_in_progress"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static h()Z
    .locals 3

    .prologue
    .line 235
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    const-string v1, "ads_opt_out_in_progress"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic i()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->a:Ljava/lang/Object;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/b;->a(Z)V

    .line 115
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/b;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 122
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 92
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 93
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 94
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 95
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/b;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/c/b;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 96
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 99
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/ads/settings/c/b;->g:I

    .line 100
    return-void
.end method

.method public final a(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 195
    const-string v0, "AdsPrefsState"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "AdsPrefsState"

    const-string v1, "Failed to retrieve server value of personalized ads."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/ads/settings/c/b;->c(I)V

    .line 199
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/settings/c/d;)V
    .locals 1

    .prologue
    .line 142
    if-eqz p1, :cond_0

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 25
    check-cast p1, Ljava/lang/Boolean;

    const-string v0, "AdsPrefsState"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AdsPrefsState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successful response of server value of personalized ads. (response="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    sget-object v1, Lcom/google/android/gms/ads/settings/c/b;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "ads_opt_in_in_progress"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    :goto_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    const-string v2, "stop_scheduled"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/b;->j()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/c/b;->c(I)V

    return-void

    :cond_2
    :try_start_1
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "ads_opt_out_in_progress"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_3
    const/4 v0, 0x4

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 103
    sget-object v1, Lcom/google/android/gms/ads/settings/c/b;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 104
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/ads/settings/c/b;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/ads/settings/c/b;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    :cond_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/ads/settings/c/b;->a(Z)V

    .line 109
    :goto_0
    monitor-exit v1

    return-void

    .line 107
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/b;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(I)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 166
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/c/b;->c(I)V

    .line 168
    const/4 v0, 0x0

    .line 169
    sget-object v6, Lcom/google/android/gms/ads/settings/c/b;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 172
    if-ne p1, v1, :cond_2

    .line 173
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ads_opt_in_in_progress"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 175
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ads_opt_out_in_progress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 177
    new-instance v0, Lcom/google/android/gms/ads/settings/c/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/b;->d:Landroid/content/Context;

    const-string v2, "mobile_optin"

    const/4 v3, -0x1

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/settings/c/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/android/volley/x;Lcom/android/volley/w;)V

    .line 187
    :cond_0
    :goto_0
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    if-eqz v0, :cond_1

    .line 189
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/b;->e:Lcom/android/volley/s;

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 191
    :cond_1
    return-void

    .line 179
    :cond_2
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 180
    :try_start_1
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ads_opt_in_in_progress"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 182
    sget-object v0, Lcom/google/android/gms/ads/settings/c/b;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "ads_opt_out_in_progress"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 184
    new-instance v0, Lcom/google/android/gms/ads/settings/c/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/b;->d:Landroid/content/Context;

    const-string v2, "mobile_optout"

    const/4 v3, -0x1

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/settings/c/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/android/volley/x;Lcom/android/volley/w;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final b(Lcom/google/android/gms/ads/settings/c/d;)V
    .locals 1

    .prologue
    .line 148
    if-eqz p1, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 151
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/b;->d:Landroid/content/Context;

    const-string v2, "connectivity"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 155
    invoke-direct {p0, v6}, Lcom/google/android/gms/ads/settings/c/b;->c(I)V

    .line 156
    new-instance v0, Lcom/google/android/gms/ads/settings/c/a;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/b;->d:Landroid/content/Context;

    const-string v2, "mobile_view"

    iget v3, p0, Lcom/google/android/gms/ads/settings/c/b;->g:I

    move-object v4, p0

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/settings/c/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/android/volley/x;Lcom/android/volley/w;)V

    .line 158
    invoke-virtual {v0, v6}, Lcom/google/android/gms/ads/settings/c/a;->a(Z)Lcom/android/volley/p;

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/b;->e:Lcom/android/volley/s;

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 163
    :goto_1
    return-void

    :cond_0
    move v0, v6

    .line 154
    goto :goto_0

    .line 161
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/ads/settings/c/b;->c(I)V

    goto :goto_1
.end method
