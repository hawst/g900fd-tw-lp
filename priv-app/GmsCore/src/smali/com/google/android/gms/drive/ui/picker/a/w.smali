.class public final Lcom/google/android/gms/drive/ui/picker/a/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/picker/a/a/p;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/util/Pair;


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/w;->a:Ljava/lang/String;

    .line 47
    new-instance v0, Landroid/util/Pair;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, v1, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/w;->b:Landroid/util/Pair;

    .line 48
    return-void
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/w;->b:Landroid/util/Pair;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/w;->a:Ljava/lang/String;

    return-object v0
.end method
