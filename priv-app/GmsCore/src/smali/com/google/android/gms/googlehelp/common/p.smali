.class public final Lcom/google/android/gms/googlehelp/common/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    .line 132
    iput v1, p0, Lcom/google/android/gms/googlehelp/common/p;->b:I

    .line 133
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/p;->c:Ljava/lang/String;

    .line 134
    iput v1, p0, Lcom/google/android/gms/googlehelp/common/p;->d:I

    .line 135
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/p;->e:Ljava/lang/String;

    .line 136
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/o;
    .locals 14

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    new-instance v1, Lcom/google/android/gms/googlehelp/common/o;

    const/4 v2, -0x1

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/common/p;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->h()Ljava/lang/String;

    move-result-object v6

    iget v7, p0, Lcom/google/android/gms/googlehelp/common/p;->b:I

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    iget-object v10, p0, Lcom/google/android/gms/googlehelp/common/p;->c:Ljava/lang/String;

    iget v11, p0, Lcom/google/android/gms/googlehelp/common/p;->d:I

    iget-object v12, p0, Lcom/google/android/gms/googlehelp/common/p;->e:Ljava/lang/String;

    const/4 v13, 0x0

    invoke-direct/range {v1 .. v13}, Lcom/google/android/gms/googlehelp/common/o;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Z)V

    return-object v1
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/p;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/googlehelp/common/p;->d:I

    .line 161
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/p;->e:Ljava/lang/String;

    .line 162
    return-object p0
.end method
