.class public Lcom/google/android/gms/app/settings/OpenSourceLicensesActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 19
    sget v0, Lcom/google/android/gms/l;->ac:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/OpenSourceLicensesActivity;->setContentView(I)V

    .line 21
    sget v0, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/app/settings/OpenSourceLicensesActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 22
    invoke-static {p0}, Lcom/google/android/gms/common/ew;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 24
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 25
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 29
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 34
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 31
    :pswitch_0
    invoke-static {p0}, Landroid/support/v4/app/ay;->a(Landroid/app/Activity;)V

    .line 32
    const/4 v0, 0x1

    goto :goto_0

    .line 29
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
