.class public Lcom/google/android/gms/googlehelp/service/MetricsReportService;
.super Landroid/app/IntentService;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x3
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 45
    const-string v0, "MetricsReportService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 46
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 86
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/googlehelp/service/MetricsReportService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 87
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 88
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 50
    new-instance v1, Lcom/google/android/gms/googlehelp/d/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/googlehelp/d/d;-><init>(Landroid/content/Context;)V

    .line 51
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/d/d;->e()Ljava/util/List;

    move-result-object v0

    .line 52
    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 54
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/o;

    .line 55
    const/4 v4, 0x0

    new-instance v5, Lcom/google/android/gms/googlehelp/service/b;

    invoke-direct {v5, p0, v2}, Lcom/google/android/gms/googlehelp/service/b;-><init>(Lcom/google/android/gms/googlehelp/service/MetricsReportService;Ljava/util/concurrent/CountDownLatch;)V

    new-instance v6, Lcom/google/android/gms/googlehelp/service/c;

    invoke-direct {v6, p0, v0, v2}, Lcom/google/android/gms/googlehelp/service/c;-><init>(Lcom/google/android/gms/googlehelp/service/MetricsReportService;Lcom/google/android/gms/googlehelp/common/o;Ljava/util/concurrent/CountDownLatch;)V

    invoke-static {p0, v4, v0, v5, v6}, Lcom/google/android/gms/googlehelp/a/o;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/google/android/gms/googlehelp/common/o;Lcom/android/volley/x;Lcom/android/volley/w;)V

    goto :goto_0

    .line 74
    :cond_0
    :try_start_0
    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 75
    const-string v0, "GOOGLEHELP_MetricsReportService"

    const-string v2, "Offline metrics reporting is done."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/d/d;->f()I

    .line 81
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    .line 82
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/service/ConnectivityBroadcastReceiver;->a(Landroid/content/Context;Z)V

    .line 83
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const-string v2, "GOOGLEHELP_MetricsReportService"

    const-string v3, "Reporting metrics interrupted."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
