.class public Lcom/google/android/gms/car/CarMediaBrowserSongNode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

.field public c:[B

.field public d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Lcom/google/android/gms/car/fa;

    invoke-direct {v0}, Lcom/google/android/gms/car/fa;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->a:I

    .line 49
    new-instance v0, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    invoke-direct {v0}, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    .line 50
    return-void
.end method

.method public constructor <init>(ILcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;[BI)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->a:I

    .line 42
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    .line 43
    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->c:[B

    .line 44
    iput p4, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->d:I

    .line 45
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 59
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/fa;->a(Lcom/google/android/gms/car/CarMediaBrowserSongNode;Landroid/os/Parcel;I)V

    .line 60
    return-void
.end method
