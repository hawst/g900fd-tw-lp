.class final Lcom/google/android/gms/drive/metadata/sync/c/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/c/b;


# instance fields
.field final a:Ljava/util/Set;

.field final b:Ljava/lang/String;

.field final c:Lcom/google/android/gms/drive/metadata/sync/c/k;

.field d:Lcom/google/android/gms/drive/metadata/sync/c/a;

.field e:Z

.field f:Z

.field g:Z

.field private final h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

.field private final i:Lcom/google/android/gms/drive/g/ae;

.field private final j:[Lcom/google/android/gms/drive/metadata/sync/c/a;

.field private final k:Lcom/google/android/gms/drive/metadata/sync/c/f;

.field private l:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Lcom/google/android/gms/drive/g/ae;[Lcom/google/android/gms/drive/metadata/sync/c/a;Lcom/google/android/gms/drive/metadata/sync/c/k;)V
    .locals 7

    .prologue
    .line 60
    new-instance v6, Lcom/google/android/gms/drive/metadata/sync/c/e;

    invoke-direct {v6, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/c/e;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/c/d;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Lcom/google/android/gms/drive/g/ae;[Lcom/google/android/gms/drive/metadata/sync/c/a;Lcom/google/android/gms/drive/metadata/sync/c/k;Lcom/google/android/gms/drive/metadata/sync/c/f;)V

    .line 74
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;Lcom/google/android/gms/drive/g/ae;[Lcom/google/android/gms/drive/metadata/sync/c/a;Lcom/google/android/gms/drive/metadata/sync/c/k;Lcom/google/android/gms/drive/metadata/sync/c/f;)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->a:Ljava/util/Set;

    .line 52
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->l:J

    .line 80
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    .line 81
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    .line 82
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->i:Lcom/google/android/gms/drive/g/ae;

    .line 83
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->j:[Lcom/google/android/gms/drive/metadata/sync/c/a;

    .line 84
    iput-object p5, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->c:Lcom/google/android/gms/drive/metadata/sync/c/k;

    .line 85
    iput-object p6, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->k:Lcom/google/android/gms/drive/metadata/sync/c/f;

    .line 86
    return-void
.end method

.method static g()J
    .locals 2

    .prologue
    .line 278
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->b()Lcom/google/android/gms/drive/g/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method private declared-synchronized h()Z
    .locals 10

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 173
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-eqz v2, :cond_0

    move v2, v0

    :goto_0
    const-string v3, "Not initialized yet"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 174
    sget-object v3, Lcom/google/android/gms/drive/metadata/sync/c/g;->a:Lcom/google/android/gms/drive/metadata/sync/c/a;

    .line 175
    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/c/d;->g()J

    move-result-wide v6

    .line 176
    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->j:[Lcom/google/android/gms/drive/metadata/sync/c/a;

    array-length v8, v5

    move v4, v1

    :goto_1
    if-ge v4, v8, :cond_3

    aget-object v2, v5, v4

    .line 177
    invoke-interface {v2, p0, v6, v7}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a(Lcom/google/android/gms/drive/metadata/sync/c/b;J)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 182
    :goto_2
    const-string v3, "PushNotificationController"

    const-string v4, "evaluateLevel[%s] currentLevel=%s newLevel=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    invoke-interface {v7}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-interface {v2}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 184
    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-eq v2, v3, :cond_2

    .line 185
    iput-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    .line 186
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/c/d;->i()V

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/c/d;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    :goto_3
    monitor-exit p0

    return v0

    :cond_0
    move v2, v1

    .line 173
    goto :goto_0

    .line 176
    :cond_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_2
    move v0, v1

    .line 190
    goto :goto_3

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v2, v3

    goto :goto_2
.end method

.method private i()V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    sget-object v1, Lcom/google/android/gms/drive/metadata/sync/c/g;->a:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-ne v0, v1, :cond_0

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->k:Lcom/google/android/gms/drive/metadata/sync/c/f;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/c/f;->a(Z)V

    .line 221
    :goto_0
    return-void

    .line 219
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->k:Lcom/google/android/gms/drive/metadata/sync/c/f;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/c/f;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/metadata/sync/c/k;
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->c:Lcom/google/android/gms/drive/metadata/sync/c/k;

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/drive/metadata/sync/c/a;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-nez v2, :cond_0

    :goto_0
    const-string v1, "Initial level has been set"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 93
    const-string v0, "PushNotificationController"

    const-string v1, "init[%s] at level %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-interface {p1}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 94
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    .line 95
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/c/d;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    monitor-exit p0

    return-void

    :cond_0
    move v0, v1

    .line 92
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-eqz v2, :cond_1

    :goto_0
    const-string v1, "Not initialized yet"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 133
    const-string v0, "PushNotificationController"

    const-string v1, "handleSyncCompleted[%s] success=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 134
    if-eqz p1, :cond_0

    .line 135
    iget-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->g:Z

    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->e:Z

    .line 136
    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/c/d;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->l:J

    .line 138
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    monitor-exit p0

    return-void

    :cond_1
    move v0, v1

    .line 132
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 122
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    if-eqz v2, :cond_0

    :goto_0
    const-string v1, "Not initialized yet"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 123
    const-string v0, "PushNotificationController"

    const-string v1, "handleSyncStarted[%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->f:Z

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    monitor-exit p0

    return-void

    :cond_0
    move v0, v1

    .line 122
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final d()V
    .locals 4

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    const/16 v2, 0x67

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;II)I

    .line 161
    return-void
.end method

.method final declared-synchronized e()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 200
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/metadata/sync/c/d;->h()Z

    move-result v1

    .line 201
    iget-boolean v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->e:Z

    if-eqz v2, :cond_2

    .line 203
    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    sget-object v2, Lcom/google/android/gms/drive/metadata/sync/c/g;->a:Lcom/google/android/gms/drive/metadata/sync/c/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v1, v2, :cond_1

    .line 212
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 206
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/c/d;->g()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->l:J

    sub-long/2addr v2, v4

    .line 208
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a(J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-eqz v1, :cond_0

    .line 212
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()V
    .locals 10

    .prologue
    .line 227
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->i:Lcom/google/android/gms/drive/g/ae;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->d:Lcom/google/android/gms/drive/metadata/sync/c/a;

    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/c/d;->c:Lcom/google/android/gms/drive/metadata/sync/c/k;

    iget-object v0, v5, Lcom/google/android/gms/drive/metadata/sync/c/k;->a:[J

    array-length v0, v0

    new-array v6, v0, [J

    const/4 v1, 0x0

    array-length v0, v6

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/drive/metadata/sync/c/k;->a(I)J

    move-result-wide v8

    aput-wide v8, v6, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/c;

    invoke-interface {v4}, Lcom/google/android/gms/drive/metadata/sync/c/a;->a()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v6}, Lcom/google/android/gms/drive/metadata/sync/c/c;-><init>(Ljava/lang/String;[J)V

    invoke-interface {v2, v3, v0}, Lcom/google/android/gms/drive/g/ae;->a(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/c/c;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 229
    const-string v0, "PushNotificationController"

    const-string v1, "persistActivityState failed to persist state"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 231
    :cond_1
    return-void
.end method
