.class final Lcom/google/android/gms/ads/identifier/service/a;
.super Lcom/google/android/gms/ads/identifier/a/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/ads/identifier/b/b;

.field private final b:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/ads/identifier/a/b;-><init>()V

    .line 48
    invoke-static {}, Lcom/google/android/gms/ads/identifier/b/b;->a()Lcom/google/android/gms/ads/identifier/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->a:Lcom/google/android/gms/ads/identifier/b/b;

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/ads/identifier/service/a;->b:Landroid/content/Context;

    .line 50
    return-void
.end method

.method private b()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 56
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/ads/identifier/service/a;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.google.android.gms"

    const/16 v4, 0x80

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 58
    iget-object v2, v2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 61
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->a:Lcom/google/android/gms/ads/identifier/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b/b;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->a:Lcom/google/android/gms/ads/identifier/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b/b;->c()V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->a:Lcom/google/android/gms/ads/identifier/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b/b;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->b:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/service/a;->a:Lcom/google/android/gms/ads/identifier/b/b;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/identifier/b/b;->a(Z)V

    .line 101
    return-void
.end method

.method public final a(Z)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 72
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/service/a;->a:Lcom/google/android/gms/ads/identifier/b/b;

    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/b/b;->f()I

    move-result v1

    .line 73
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 74
    if-ne v1, v0, :cond_0

    .line 82
    :goto_0
    return v0

    .line 74
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 76
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/ads/identifier/service/a;->b()Z

    move-result v1

    if-nez v1, :cond_2

    .line 77
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 82
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/service/a;->a:Lcom/google/android/gms/ads/identifier/b/b;

    if-eqz p1, :cond_3

    const/4 v0, 0x2

    :cond_3
    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/identifier/b/b;->a(I)Z

    move-result v0

    goto :goto_0
.end method
