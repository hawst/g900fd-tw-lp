.class public final Lcom/google/android/gms/plus/audience/aa;
.super Lcom/google/android/gms/common/ui/widget/d;
.source "SourceFile"


# instance fields
.field public c:Ljava/lang/String;

.field final synthetic d:Lcom/google/android/gms/plus/audience/o;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 1060
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/aa;->d:Lcom/google/android/gms/plus/audience/o;

    .line 1061
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/ui/widget/d;-><init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/util/Collection;)V

    .line 1057
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/aa;->e:Ljava/util/ArrayList;

    .line 1062
    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;Landroid/view/View;Z)Landroid/view/View;
    .locals 13

    .prologue
    .line 1056
    move-object v1, p1

    check-cast v1, Lcom/google/android/gms/people/model/j;

    const/4 v7, 0x0

    const/4 v12, 0x1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aa;->d:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/o;->d(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/plus/audience/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/common/people/data/e;->a(Lcom/google/android/gms/people/model/j;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v7, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/aa;->d:Lcom/google/android/gms/plus/audience/o;

    invoke-static {v2}, Lcom/google/android/gms/plus/audience/o;->d(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/plus/audience/bg;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "checkboxEnabled"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aa;->d:Lcom/google/android/gms/plus/audience/o;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/j;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/aa;->c:Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/people/model/j;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1}, Lcom/google/android/gms/people/model/j;->c()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    sget v8, Lcom/google/android/gms/l;->dw:I

    const/4 v11, 0x0

    move-object v9, p2

    move/from16 v10, p3

    invoke-virtual/range {v0 .. v12}, Lcom/google/android/gms/plus/audience/o;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b(Z)V

    return-object v0
.end method

.method public final b(I)I
    .locals 1

    .prologue
    .line 1087
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/aa;->d:Lcom/google/android/gms/plus/audience/o;

    iget v0, v0, Lcom/google/android/gms/plus/audience/o;->i:I

    return v0
.end method
