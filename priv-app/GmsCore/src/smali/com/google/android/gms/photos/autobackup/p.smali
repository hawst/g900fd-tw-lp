.class public final Lcom/google/android/gms/photos/autobackup/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field final a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field b:Lcom/google/android/gms/photos/autobackup/service/a/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/p;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/p;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->c(I)V

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->e()V

    .line 119
    :cond_0
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 24
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    invoke-static {p1}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/photos/autobackup/service/a/b;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/photos/autobackup/service/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/photos/autobackup/service/a/f;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "plusone:autobackup_allow_migration"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/p;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILjava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GetAutoBackupSettingsOp"

    const-string v1, "failed trying to deliver settings list"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const-class v0, Lcom/google/android/gms/photos/autobackup/service/a/a;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/service/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/a;->b()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    if-nez v1, :cond_3

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/a;->a()V

    new-instance v0, Lcom/google/android/gms/photos/autobackup/x;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/p;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    invoke-direct {v0, v1}, Lcom/google/android/gms/photos/autobackup/x;-><init>(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/p;->b:Lcom/google/android/gms/photos/autobackup/service/a/b;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/q;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/photos/autobackup/q;-><init>(Lcom/google/android/gms/photos/autobackup/p;Lcom/google/android/gms/photos/autobackup/service/a/a;Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/photos/autobackup/service/a/b;->a(Lcom/google/android/gms/photos/autobackup/service/a/g;)V

    goto :goto_0
.end method
