.class public final Lcom/google/android/gms/reminders/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/reminders/model/TaskEntity;)Landroid/content/ContentValues;
    .locals 3

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->a()Landroid/content/ContentValues;

    move-result-object v0

    .line 35
    const-string v1, "archived"

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->i()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 36
    const-string v1, "deleted"

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->j()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 37
    const-string v1, "pinned"

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->k()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 38
    const-string v1, "snoozed"

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->l()Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 41
    new-instance v1, Lcom/google/android/gms/reminders/model/TaskIdEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/reminders/model/TaskIdEntity;-><init>(Lcom/google/android/gms/reminders/model/TaskId;)V

    invoke-virtual {v1}, Lcom/google/android/gms/reminders/model/TaskIdEntity;->f()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 45
    new-instance v1, Lcom/google/android/gms/reminders/model/TaskListEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->e()Lcom/google/android/gms/reminders/model/TaskList;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/reminders/model/TaskListEntity;-><init>(Lcom/google/android/gms/reminders/model/TaskList;)V

    invoke-virtual {v1}, Lcom/google/android/gms/reminders/model/TaskListEntity;->d()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 48
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->n()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    const-string v2, "due_date_"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/DateTime;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 49
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->o()Lcom/google/android/gms/reminders/model/DateTime;

    move-result-object v1

    const-string v2, "event_date_"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/DateTime;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/model/TaskEntity;->p()Lcom/google/android/gms/reminders/model/Location;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v1, "lat"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "lng"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "name"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "radius_meters"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "location_type"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v1, "display_address"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 51
    :goto_0
    return-object v0

    .line 50
    :cond_2
    new-instance v2, Lcom/google/android/gms/reminders/model/LocationEntity;

    invoke-direct {v2, v1}, Lcom/google/android/gms/reminders/model/LocationEntity;-><init>(Lcom/google/android/gms/reminders/model/Location;)V

    invoke-virtual {v2}, Lcom/google/android/gms/reminders/model/LocationEntity;->i()Landroid/content/ContentValues;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/reminders/model/DateTime;)Lcom/google/f/a/h;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 145
    if-nez p0, :cond_0

    .line 155
    :goto_0
    return-object v0

    .line 148
    :cond_0
    new-instance v1, Lcom/google/f/a/h;

    invoke-direct {v1}, Lcom/google/f/a/h;-><init>()V

    .line 149
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->a()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    .line 150
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->d()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    .line 151
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->e()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    .line 152
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->g()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    .line 153
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->h()Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    .line 154
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->f()Lcom/google/android/gms/reminders/model/Time;

    move-result-object v2

    if-nez v2, :cond_1

    :goto_1
    iput-object v0, v1, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    move-object v0, v1

    .line 155
    goto :goto_0

    .line 154
    :cond_1
    new-instance v0, Lcom/google/f/a/i;

    invoke-direct {v0}, Lcom/google/f/a/i;-><init>()V

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/Time;->a()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/f/a/i;->a:Ljava/lang/Integer;

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/Time;->d()Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v0, Lcom/google/f/a/i;->b:Ljava/lang/Integer;

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/Time;->e()Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v0, Lcom/google/f/a/i;->c:Ljava/lang/Integer;

    goto :goto_1
.end method

.method public static a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 1

    .prologue
    .line 189
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 190
    return-void

    .line 189
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/reminders/model/DateTime;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 57
    if-nez p0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "year"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 59
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "month"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "period"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "absolute_time_ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x0

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/Time;Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 72
    :goto_0
    return-void

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "year"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 66
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "month"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->e()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 68
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "period"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->g()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 69
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "absolute_time_ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->h()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 70
    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/DateTime;->f()Lcom/google/android/gms/reminders/model/Time;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/Time;Landroid/content/ContentValues;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/gms/reminders/model/Time;Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 76
    if-nez p0, :cond_0

    .line 77
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hour"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 78
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "minute"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "second"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 85
    :goto_0
    return-void

    .line 81
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hour"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Time;->a()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 82
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "minute"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Time;->d()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "second"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/reminders/model/Time;->e()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method
