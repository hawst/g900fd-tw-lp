.class final Lcom/google/android/gms/update/j;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/SystemUpdateActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/gms/update/j;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 167
    const-string v0, "SystemUpdateActivity"

    const-string v1, "Received media unmounted intent, disabling SD Card option."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/update/j;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    sget v1, Lcom/google/android/gms/j;->A:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 169
    if-eqz v0, :cond_0

    .line 170
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 172
    :cond_0
    return-void
.end method
