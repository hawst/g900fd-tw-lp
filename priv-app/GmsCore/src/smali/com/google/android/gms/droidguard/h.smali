.class public final Lcom/google/android/gms/droidguard/h;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 141
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/droidguard/h;->b:Ljava/lang/String;

    .line 158
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/droidguard/h;->d:Ljava/lang/String;

    .line 194
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/droidguard/h;->e:I

    .line 136
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/google/android/gms/droidguard/h;->e:I

    if-gez v0, :cond_0

    .line 199
    invoke-virtual {p0}, Lcom/google/android/gms/droidguard/h;->b()I

    .line 201
    :cond_0
    iget v0, p0, Lcom/google/android/gms/droidguard/h;->e:I

    return v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/h;
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/droidguard/h;->a:Z

    .line 146
    iput-object p1, p0, Lcom/google/android/gms/droidguard/h;->b:Ljava/lang/String;

    .line 147
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/h;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/h;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/h;->b(Ljava/lang/String;)Lcom/google/android/gms/droidguard/h;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 186
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/h;->a:Z

    if-eqz v0, :cond_0

    .line 187
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/droidguard/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 189
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/h;->c:Z

    if-eqz v0, :cond_1

    .line 190
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/droidguard/h;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 192
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 206
    const/4 v0, 0x0

    .line 207
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/h;->a:Z

    if-eqz v1, :cond_0

    .line 208
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/droidguard/h;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 211
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/droidguard/h;->c:Z

    if-eqz v1, :cond_1

    .line 212
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/droidguard/h;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_1
    iput v0, p0, Lcom/google/android/gms/droidguard/h;->e:I

    .line 216
    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/droidguard/h;
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/droidguard/h;->c:Z

    .line 163
    iput-object p1, p0, Lcom/google/android/gms/droidguard/h;->d:Ljava/lang/String;

    .line 164
    return-object p0
.end method
