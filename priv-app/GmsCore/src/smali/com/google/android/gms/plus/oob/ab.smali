.class public final Lcom/google/android/gms/plus/oob/ab;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field protected a:Lcom/google/android/gms/plus/oob/e;

.field private b:Lcom/google/android/gms/plus/oob/ac;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 41
    return-void
.end method

.method public static a(ZLcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;)Lcom/google/android/gms/plus/oob/ab;
    .locals 3

    .prologue
    .line 98
    new-instance v0, Lcom/google/android/gms/plus/oob/ab;

    invoke-direct {v0}, Lcom/google/android/gms/plus/oob/ab;-><init>()V

    .line 99
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 100
    const-string v2, "is_setup_wizard_theme"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 101
    const-string v2, "description"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 102
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/ab;->setArguments(Landroid/os/Bundle;)V

    .line 103
    return-object v0
.end method

.method public static a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/ab;
    .locals 3

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/gms/plus/oob/ab;

    invoke-direct {v0}, Lcom/google/android/gms/plus/oob/ab;-><init>()V

    .line 65
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 66
    const-string v2, "is_setup_wizard_theme"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 67
    const-string v2, "title"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v2, "text"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v2, "button_text"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/ab;->setArguments(Landroid/os/Bundle;)V

    .line 71
    return-object v0
.end method

.method public static a(ZLjava/util/ArrayList;)Lcom/google/android/gms/plus/oob/ab;
    .locals 3

    .prologue
    .line 82
    new-instance v0, Lcom/google/android/gms/plus/oob/ab;

    invoke-direct {v0}, Lcom/google/android/gms/plus/oob/ab;-><init>()V

    .line 83
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 84
    const-string v2, "is_setup_wizard_theme"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 85
    const-string v2, "errors"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 86
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/ab;->setArguments(Landroid/os/Bundle;)V

    .line 87
    return-object v0
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 108
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 109
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/ac;

    if-nez v0, :cond_0

    .line 110
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/oob/ac;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move-object v0, p1

    .line 113
    check-cast v0, Lcom/google/android/gms/plus/oob/ac;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/ab;->b:Lcom/google/android/gms/plus/oob/ac;

    .line 114
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/e;

    if-nez v0, :cond_1

    .line 115
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/oob/e;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/oob/e;

    iput-object p1, p0, Lcom/google/android/gms/plus/oob/ab;->a:Lcom/google/android/gms/plus/oob/e;

    .line 119
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 176
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->bO:I

    if-ne v0, v1, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ab;->b:Lcom/google/android/gms/plus/oob/ac;

    invoke-interface {v0}, Lcom/google/android/gms/plus/oob/ac;->c()V

    .line 179
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v7, 0x0

    .line 130
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ab;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "is_setup_wizard_theme"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v8

    .line 134
    sget v0, Lcom/google/android/gms/l;->eA:I

    invoke-virtual {p1, v0, p2, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v9

    .line 138
    invoke-static {p1, v9, v8}, Lcom/google/android/gms/plus/oob/ae;->a(Landroid/view/LayoutInflater;Landroid/view/View;Z)V

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ab;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ab;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->vR:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 143
    :goto_0
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 144
    invoke-static {v1}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    sget v0, Lcom/google/android/gms/j;->sA:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/LinearLayout;

    .line 148
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ab;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "description"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;

    if-eqz v4, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/ab;->a:Lcom/google/android/gms/plus/oob/e;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity$DescriptionEntity;->e()Ljava/util/List;

    move-result-object v5

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/oob/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ab;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "errors"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v7

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ErrorsEntity;

    iget-object v4, p0, Lcom/google/android/gms/plus/oob/ab;->a:Lcom/google/android/gms/plus/oob/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ErrorsEntity;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ErrorsEntity;->d()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ErrorsEntity;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v4, v5, v11, v0}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/oob/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ab;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_2
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v7

    .line 150
    :goto_2
    if-ge v2, v3, :cond_4

    .line 151
    if-eqz v8, :cond_3

    sget v0, Lcom/google/android/gms/l;->eC:I

    .line 154
    :goto_3
    const/4 v1, 0x1

    invoke-virtual {p1, v0, v6, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 156
    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 157
    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 158
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 150
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 151
    :cond_3
    sget v0, Lcom/google/android/gms/l;->eB:I

    goto :goto_3

    .line 162
    :cond_4
    sget v0, Lcom/google/android/gms/j;->bO:I

    invoke-virtual {v9, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 163
    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 164
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/ab;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "button_text"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 165
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 166
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :goto_4
    return-object v9

    .line 168
    :cond_5
    sget v1, Lcom/google/android/gms/p;->uM:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_4

    :cond_6
    move-object v1, v0

    goto/16 :goto_0
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/ab;->b:Lcom/google/android/gms/plus/oob/ac;

    .line 125
    return-void
.end method
