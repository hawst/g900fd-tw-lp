.class final Lcom/google/android/gms/smart_profile/o;
.super Lcom/google/android/gms/smart_profile/u;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/smart_profile/ad;
.implements Lcom/google/android/gms/smart_profile/card/h;
.implements Lcom/google/android/gms/smart_profile/header/view/b;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

.field private c:Lcom/google/android/gms/common/api/v;

.field private d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

.field private e:Lcom/google/android/gms/smart_profile/ak;

.field private f:Lcom/google/android/gms/common/api/am;

.field private g:Lcom/google/android/gms/common/api/am;

.field private h:Ljava/lang/String;

.field private i:Lcom/google/android/gms/smart_profile/z;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Ljava/lang/String;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/smart_profile/header/view/HeaderView;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/u;-><init>()V

    .line 111
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    .line 112
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    .line 113
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/o;)Z
    .locals 1

    .prologue
    .line 82
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/o;->s:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/o;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/o;->c()V

    return-void
.end method

.method private c()V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v4

    .line 193
    if-eqz v4, :cond_5

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->n:Ljava/lang/String;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->o:Ljava/lang/String;

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->m()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->p:Ljava/lang/String;

    .line 204
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->as:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 206
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 208
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    move v1, v0

    .line 213
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v2

    .line 214
    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/o;->n:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/smart_profile/o;->q:Ljava/lang/String;

    invoke-static {v5, v6, v7, v8, v0}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 217
    if-eqz v0, :cond_8

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 220
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/o;->o:Ljava/lang/String;

    const/4 v7, 0x2

    invoke-interface {v0, v5, v6, v7, v2}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->f:Lcom/google/android/gms/common/api/am;

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->f:Lcom/google/android/gms/common/api/am;

    new-instance v5, Lcom/google/android/gms/smart_profile/q;

    invoke-direct {v5, p0, v3}, Lcom/google/android/gms/smart_profile/q;-><init>(Lcom/google/android/gms/smart_profile/o;B)V

    invoke-interface {v0, v5}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 226
    :cond_3
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/o;->p:Ljava/lang/String;

    invoke-interface {v0, v5, v6}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->g:Lcom/google/android/gms/common/api/am;

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->g:Lcom/google/android/gms/common/api/am;

    new-instance v5, Lcom/google/android/gms/smart_profile/r;

    invoke-direct {v5, p0, v3}, Lcom/google/android/gms/smart_profile/r;-><init>(Lcom/google/android/gms/smart_profile/o;B)V

    invoke-interface {v0, v5}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 238
    :cond_4
    :goto_2
    new-instance v0, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/smart_profile/HeaderViewCreatorImpl;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/smart_profile/o;->e:Lcom/google/android/gms/smart_profile/ak;

    iget-object v7, v7, Lcom/google/android/gms/smart_profile/ak;->a:Ljava/lang/String;

    invoke-direct {v0, v5, v6, v7}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;-><init>(Landroid/content/Context;Landroid/content/Context;Ljava/lang/String;)V

    .line 241
    iget-object v5, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/smart_profile/header/CommunicateCardHelper;->a(Lcom/google/android/gms/smart_profile/SmartProfilePerson;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 243
    if-nez v0, :cond_9

    const/4 v0, 0x0

    .line 245
    :goto_3
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_a

    .line 246
    iget-object v4, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a()Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    move-result-object v4

    invoke-virtual {v4, v0, v2, v3, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZI)V

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/header/HeaderUtil;->a(Lcom/google/android/gms/smart_profile/header/view/HeaderView;I)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->e(Z)V

    .line 255
    :cond_5
    :goto_4
    sget-object v0, Lcom/google/android/gms/plus/f;->j:Lcom/google/android/gms/plus/b;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->k:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/smart_profile/b;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v4, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/b;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    sget v1, Lcom/google/android/gms/j;->ta:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 263
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 264
    return-void

    :catch_0
    move-exception v1

    :cond_6
    move v1, v0

    goto/16 :goto_0

    :cond_7
    move v0, v3

    .line 213
    goto/16 :goto_1

    .line 230
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->o:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 232
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/o;->o:Ljava/lang/String;

    const/4 v7, 0x3

    invoke-interface {v0, v5, v6, v7, v2}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->g:Lcom/google/android/gms/common/api/am;

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->g:Lcom/google/android/gms/common/api/am;

    new-instance v5, Lcom/google/android/gms/smart_profile/r;

    invoke-direct {v5, p0, v3}, Lcom/google/android/gms/smart_profile/r;-><init>(Lcom/google/android/gms/smart_profile/o;B)V

    invoke-interface {v0, v5}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_2

    .line 243
    :cond_9
    iget-object v0, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    goto :goto_3

    .line 250
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->e(Z)V

    goto :goto_4
.end method

.method static synthetic c(Lcom/google/android/gms/smart_profile/o;)Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/o;->r:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/smart_profile/o;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/smart_profile/o;)Lcom/google/android/gms/smart_profile/header/view/HeaderView;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/smart_profile/o;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->f:Lcom/google/android/gms/common/api/am;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/smart_profile/o;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->g:Lcom/google/android/gms/common/api/am;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 353
    :cond_0
    return-void
.end method

.method public final a(Landroid/content/Intent;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->i:Lcom/google/android/gms/smart_profile/z;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 398
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->i:Lcom/google/android/gms/smart_profile/z;

    invoke-interface {v0, p1}, Lcom/google/android/gms/smart_profile/z;->onClick(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 403
    :cond_0
    :goto_0
    return-void

    .line 399
    :catch_0
    move-exception v0

    .line 400
    const-string v1, "HeaderViewCreatorImpl"

    const-string v2, "onClick"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 279
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/z;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 343
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/o;->i:Lcom/google/android/gms/smart_profile/z;

    .line 344
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    if-eqz p1, :cond_0

    move-object v0, p0

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a(Lcom/google/android/gms/smart_profile/header/view/b;)V

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->b:Lcom/google/android/gms/smart_profile/header/view/HeaderView;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/header/view/HeaderView;->a()Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    move-result-object v0

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, p0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/h;)V

    .line 346
    return-void

    :cond_0
    move-object v0, v1

    .line 344
    goto :goto_0

    :cond_1
    move-object p0, v1

    .line 345
    goto :goto_1
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 184
    iget-boolean v0, p0, Lcom/google/android/gms/smart_profile/o;->r:Z

    if-eqz v0, :cond_0

    .line 185
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/o;->c()V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/gms/smart_profile/o;->s:Z

    goto :goto_0
.end method

.method public final b()V
    .locals 6

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->i:Lcom/google/android/gms/smart_profile/z;

    if-eqz v0, :cond_1

    .line 358
    const/4 v0, 0x0

    .line 359
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->t()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f()Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/gms/smart_profile/o;->m:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 377
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 379
    sget-object v1, Lcom/google/android/gms/plus/f;->j:Lcom/google/android/gms/plus/b;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/o;->k:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/smart_profile/b;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lcom/google/android/gms/smart_profile/c;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/google/android/gms/plus/b;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 385
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->i:Lcom/google/android/gms/smart_profile/z;

    invoke-interface {v1, v0}, Lcom/google/android/gms/smart_profile/z;->onClick(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    :cond_1
    :goto_1
    return-void

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->i()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/smart_profile/o;->m:I

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->u()Ljava/util/List;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)Landroid/content/Intent;

    move-result-object v0

    goto :goto_0

    .line 386
    :catch_0
    move-exception v0

    .line 387
    const-string v1, "HeaderViewCreatorImpl"

    const-string v2, "onClick"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 117
    if-eqz p1, :cond_0

    .line 118
    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_ACCOUNT_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    .line 119
    const-string v0, "com.google.android.gms.people.smart_profile.VIEWER_PAGE_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->k:Ljava/lang/String;

    .line 120
    const-string v0, "com.google.android.gms.people.smart_profile.QUALIFIED_ID"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->l:Ljava/lang/String;

    .line 121
    const-string v0, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/smart_profile/o;->m:I

    .line 123
    const-string v0, "com.google.android.gms.people.smart_profile.THEME_COLOR"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->h:Ljava/lang/String;

    .line 124
    const-string v0, "com.google.android.gms.people.smart_profile.DISPLAY_NAME"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->n:Ljava/lang/String;

    .line 125
    const-string v0, "com.google.android.gms.people.smart_profile.AVATAR_URL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->o:Ljava/lang/String;

    .line 126
    const-string v0, "com.google.android.gms.people.smart_profile.COVER_PHOTO_URL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->p:Ljava/lang/String;

    .line 127
    const-string v0, "com.google.android.gms.people.smart_profile.TAGLINE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->q:Ljava/lang/String;

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/HeaderViewCreatorImpl;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 132
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 176
    :cond_1
    :goto_0
    return-void

    .line 137
    :cond_2
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    .line 139
    :cond_3
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    iget v1, p0, Lcom/google/android/gms/smart_profile/o;->m:I

    iput v1, v0, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    .line 142
    new-instance v1, Lcom/google/android/gms/common/api/w;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    .line 143
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 144
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    .line 146
    :cond_4
    invoke-virtual {v1, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v2

    invoke-virtual {v2, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/plus/f;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    .line 151
    invoke-virtual {v1}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    .line 152
    new-instance v0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/HeaderViewCreatorImpl;->a(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/o;->k:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/o;->l:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/gms/smart_profile/o;->m:I

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    .line 155
    new-instance v0, Lcom/google/android/gms/smart_profile/ak;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/o;->j:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->k:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/o;->l:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/o;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/smart_profile/ak;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/o;->e:Lcom/google/android/gms/smart_profile/ak;

    .line 160
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 161
    const-string v1, "internal_call_method"

    const-string v2, "LOAD_GSERVICES_VALUES_FOR_SMART_PROFILE"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    sget-object v1, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/o;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/s;->a(Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/smart_profile/p;

    invoke-direct {v1, p0}, Lcom/google/android/gms/smart_profile/p;-><init>(Lcom/google/android/gms/smart_profile/o;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/ad;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/o;->d:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a()V

    goto/16 :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 268
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 272
    return-void
.end method
