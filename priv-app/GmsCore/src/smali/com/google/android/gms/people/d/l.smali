.class public final Lcom/google/android/gms/people/d/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/people/d/s;

.field public b:Lcom/google/android/gms/people/d/n;

.field public c:Lcom/google/android/gms/people/d/o;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2573
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2574
    iput-object v0, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    iput-object v0, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    iput-object v0, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/l;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/l;->cachedSize:I

    .line 2575
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2661
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2662
    iget-object v1, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    if-eqz v1, :cond_0

    .line 2663
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2666
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    if-eqz v1, :cond_1

    .line 2667
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2670
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    if-eqz v1, :cond_2

    .line 2671
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2674
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/l;->d:Z

    if-eqz v1, :cond_3

    .line 2675
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/l;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2678
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2588
    if-ne p1, p0, :cond_1

    .line 2625
    :cond_0
    :goto_0
    return v0

    .line 2591
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 2592
    goto :goto_0

    .line 2594
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/l;

    .line 2595
    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    if-nez v2, :cond_3

    .line 2596
    iget-object v2, p1, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2597
    goto :goto_0

    .line 2600
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    iget-object v3, p1, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/s;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2601
    goto :goto_0

    .line 2604
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    if-nez v2, :cond_5

    .line 2605
    iget-object v2, p1, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2606
    goto :goto_0

    .line 2609
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    iget-object v3, p1, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2610
    goto :goto_0

    .line 2613
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    if-nez v2, :cond_7

    .line 2614
    iget-object v2, p1, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2615
    goto :goto_0

    .line 2618
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    iget-object v3, p1, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2619
    goto :goto_0

    .line 2622
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/l;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/l;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2623
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2630
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2633
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2635
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2637
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/l;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v1

    .line 2638
    return v0

    .line 2630
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/s;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2633
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/n;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2635
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    invoke-virtual {v1}, Lcom/google/android/gms/people/d/o;->hashCode()I

    move-result v1

    goto :goto_2

    .line 2637
    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/s;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/people/d/n;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/people/d/o;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/l;->d:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2644
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    if-eqz v0, :cond_0

    .line 2645
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/people/d/l;->a:Lcom/google/android/gms/people/d/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2647
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    if-eqz v0, :cond_1

    .line 2648
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/people/d/l;->b:Lcom/google/android/gms/people/d/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2650
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    if-eqz v0, :cond_2

    .line 2651
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/people/d/l;->c:Lcom/google/android/gms/people/d/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2653
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/l;->d:Z

    if-eqz v0, :cond_3

    .line 2654
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/l;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2656
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2657
    return-void
.end method
