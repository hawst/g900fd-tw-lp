.class final Lcom/google/android/gms/drive/metadata/sync/c/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/events/b;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/drive/metadata/sync/c/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/sync/c/g;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/c/j;->b:Lcom/google/android/gms/drive/metadata/sync/c/g;

    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/c/j;->a:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 274
    if-lez p1, :cond_2

    .line 275
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/j;->b:Lcom/google/android/gms/drive/metadata/sync/c/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/j;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a(Lcom/google/android/gms/drive/metadata/sync/c/g;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;

    move-result-object v1

    const-string v2, "PushNotificationController"

    const-string v3, "registerChangeListener[%s]"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    monitor-enter v1

    :try_start_0
    iget-object v2, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->a:Ljava/util/Set;

    invoke-interface {v2, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/c/d;->e()Z

    move-result v0

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/c/d;->d()V

    .line 279
    :cond_1
    :goto_0
    return-void

    .line 275
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 277
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/c/j;->b:Lcom/google/android/gms/drive/metadata/sync/c/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/c/j;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a(Lcom/google/android/gms/drive/metadata/sync/c/g;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/d;

    move-result-object v1

    const-string v2, "PushNotificationController"

    const-string v3, "unregisterResourceListener[%s]"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->b:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    monitor-enter v1

    :try_start_1
    iget-object v2, v1, Lcom/google/android/gms/drive/metadata/sync/c/d;->a:Ljava/util/Set;

    invoke-interface {v2, p0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/c/d;->e()Z

    move-result v0

    :cond_3
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/c/d;->d()V

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
