.class public final Lcom/google/android/gms/plus/service/a/n;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/plus/internal/c;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/n;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/n;->c:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/n;->b:Lcom/google/android/gms/plus/internal/c;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 35
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/n;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/n;->c:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/plus/b/b;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;

    move-result-object v5

    .line 39
    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v0

    .line 40
    new-instance v3, Lcom/google/android/gms/plus/internal/model/people/PersonEntity$ImageEntity;

    invoke-direct {v3, v0}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity$ImageEntity;-><init>(Ljava/lang/String;)V

    .line 41
    new-instance v0, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->g()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PersonEntity;->o()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/people/PersonEntity$ImageEntity;ILjava/lang/String;)V

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/n;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v2, 0x0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/plus/internal/c;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 49
    :goto_0
    return-void

    .line 45
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/n;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x4

    invoke-interface {v0, v1, v6}, Lcom/google/android/gms/plus/internal/c;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V

    goto :goto_0

    .line 47
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/n;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v6}, Lcom/google/android/gms/plus/internal/c;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/n;->b:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/n;->b:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILcom/google/android/gms/plus/internal/model/people/PersonEntity;)V

    .line 56
    :cond_0
    return-void
.end method
