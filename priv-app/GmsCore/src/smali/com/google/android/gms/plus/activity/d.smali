.class final Lcom/google/android/gms/plus/activity/d;
.super Lcom/google/android/gms/plus/activity/j;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    .prologue
    .line 484
    iput-object p1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/j;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V
    .locals 0

    .prologue
    .line 484
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/d;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public final b()Z
    .locals 1

    .prologue
    .line 530
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 488
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->e(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 494
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    sget v2, Lcom/google/android/gms/p;->tJ:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    .line 504
    :goto_0
    return-void

    .line 499
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 501
    new-instance v1, Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "com.google"

    invoke-direct {v1, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/common/internal/aj;->f:[Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, p0, v3}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    goto :goto_0
.end method

.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3

    .prologue
    .line 509
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 510
    if-nez v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    sget v2, Lcom/google/android/gms/p;->tI:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    .line 526
    :goto_0
    return-void

    .line 515
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    invoke-static {v1, v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->a(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;I)I

    .line 518
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->d(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 520
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    sget v2, Lcom/google/android/gms/p;->tI:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 515
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 522
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    sget v2, Lcom/google/android/gms/p;->tI:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 524
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/d;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    sget v2, Lcom/google/android/gms/p;->bG:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;Ljava/lang/String;)V

    goto :goto_0
.end method
