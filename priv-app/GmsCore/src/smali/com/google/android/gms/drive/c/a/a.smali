.class final Lcom/google/android/gms/drive/c/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/c/a/g;


# instance fields
.field private final a:Lcom/google/android/gms/drive/c/a/n;

.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/Map;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/a;->c:Ljava/util/Map;

    .line 32
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/c/a/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/a;->a:Lcom/google/android/gms/drive/c/a/n;

    .line 33
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/a;->b:Landroid/content/Context;

    .line 34
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/c/a/d;)V
    .locals 5

    .prologue
    .line 57
    iget-object v1, p1, Lcom/google/android/gms/drive/c/a/d;->c:Ljava/lang/String;

    .line 60
    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/a;->c:Ljava/util/Map;

    monitor-enter v2

    .line 61
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/a;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/a;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/c/a/m;

    .line 76
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/c/a/m;->a(Lcom/google/android/gms/drive/c/a/d;)V

    .line 79
    return-void

    .line 66
    :cond_0
    if-nez v1, :cond_1

    const/4 v0, 0x4

    .line 69
    :goto_1
    :try_start_1
    new-instance v3, Lcom/google/android/libraries/rocket/impressions/i;

    invoke-direct {v3}, Lcom/google/android/libraries/rocket/impressions/i;-><init>()V

    iget-object v4, p0, Lcom/google/android/gms/drive/c/a/a;->a:Lcom/google/android/gms/drive/c/a/n;

    invoke-interface {v4, v1}, Lcom/google/android/gms/drive/c/a/n;->a(Ljava/lang/String;)Lcom/google/android/libraries/rocket/impressions/o;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/drive/c/a/h;->a(I)Lcom/google/android/libraries/rocket/impressions/m;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Lcom/google/android/libraries/rocket/impressions/i;->a(Lcom/google/android/libraries/rocket/impressions/o;Lcom/google/android/libraries/rocket/impressions/m;)Lcom/google/android/libraries/rocket/impressions/h;

    move-result-object v3

    .line 72
    new-instance v0, Lcom/google/android/gms/drive/c/a/m;

    iget-object v4, p0, Lcom/google/android/gms/drive/c/a/a;->b:Landroid/content/Context;

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/drive/c/a/m;-><init>(Lcom/google/android/libraries/rocket/impressions/h;Landroid/content/Context;)V

    .line 73
    invoke-virtual {v0}, Lcom/google/android/gms/drive/c/a/m;->a()V

    .line 74
    iget-object v3, p0, Lcom/google/android/gms/drive/c/a/a;->c:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 66
    :cond_1
    const/16 v0, 0xe

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/a;->c:Ljava/util/Map;

    monitor-enter v1

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/c/a/a;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/c/a/m;

    .line 45
    invoke-virtual {v0}, Lcom/google/android/gms/drive/c/a/m;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final synthetic c()Lcom/google/android/gms/drive/c/a;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/drive/c/a/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/a;->b:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/drive/c/a/d;-><init>(Lcom/google/android/gms/drive/c/a/g;Landroid/content/Context;)V

    return-object v0
.end method
