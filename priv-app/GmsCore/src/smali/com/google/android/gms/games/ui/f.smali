.class public abstract Lcom/google/android/gms/games/ui/f;
.super Lcom/google/android/gms/games/ui/ae;
.source "SourceFile"


# instance fields
.field private k:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 691
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/ae;-><init>(Landroid/view/View;)V

    .line 692
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 695
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/ae;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 696
    iput-object p3, p0, Lcom/google/android/gms/games/ui/f;->k:Ljava/lang/Object;

    .line 697
    return-void
.end method

.method protected final o()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 700
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->k:Ljava/lang/Object;

    instance-of v0, v0, Lcom/google/android/gms/common/data/u;

    if-eqz v0, :cond_1

    .line 701
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/data/u;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/u;->z_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->k:Ljava/lang/Object;

    .line 704
    :goto_0
    return-object v0

    .line 701
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 704
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->k:Ljava/lang/Object;

    goto :goto_0
.end method

.method protected final p()Lcom/google/android/gms/common/data/d;
    .locals 1

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/gms/games/ui/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d;->f()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    return-object v0
.end method
