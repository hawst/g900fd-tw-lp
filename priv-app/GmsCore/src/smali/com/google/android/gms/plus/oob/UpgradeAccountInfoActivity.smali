.class public Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;
.super Lcom/google/android/gms/plus/oob/x;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/oob/ac;


# instance fields
.field private d:Landroid/support/v4/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/x;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "text"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "button_text"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.OVERRIDE_THEME"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a()V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/x;->a()V

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 19
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/x;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 0

    .prologue
    .line 19
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/x;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->setResult(I)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->finish()V

    .line 78
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/x;->onCreate(Landroid/os/Bundle;)V

    .line 53
    sget v0, Lcom/google/android/gms/l;->ey:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->setContentView(I)V

    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 56
    const-string v1, "content_fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->d:Landroid/support/v4/app/Fragment;

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->d:Landroid/support/v4/app/Fragment;

    if-nez v1, :cond_0

    .line 58
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->e()Z

    move-result v2

    const-string v3, "title"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "text"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "button_text"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v3, v4, v1}, Lcom/google/android/gms/plus/oob/ab;->a(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/oob/ab;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->d:Landroid/support/v4/app/Fragment;

    .line 59
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 60
    sget v1, Lcom/google/android/gms/j;->dD:I

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->d:Landroid/support/v4/app/Fragment;

    const-string v3, "content_fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 61
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 63
    :cond_0
    return-void
.end method
