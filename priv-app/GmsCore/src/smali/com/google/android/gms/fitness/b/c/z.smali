.class public final Lcom/google/android/gms/fitness/b/c/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/b/ad;


# static fields
.field private static final c:Ljava/util/Map;


# instance fields
.field private final a:Lcom/google/android/gms/fitness/b/b;

.field private final b:Z

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 48
    sput-object v0, Lcom/google/android/gms/fitness/b/c/z;->c:Ljava/util/Map;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/android/gms/fitness/b/c/z;->c:Ljava/util/Map;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x40933333    # 4.6f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    sget-object v0, Lcom/google/android/gms/fitness/b/c/z;->c:Ljava/util/Map;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x41466666    # 12.4f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/google/android/gms/fitness/b/c/z;->c:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x4214a3d7    # 37.16f

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/google/android/gms/fitness/b/c/z;->c:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const v2, 0x43303893

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/fitness/b/b;)V
    .locals 1

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/z;->d:Ljava/lang/String;

    .line 74
    iput-object p2, p0, Lcom/google/android/gms/fitness/b/c/z;->e:Ljava/lang/String;

    .line 75
    iput-object p3, p0, Lcom/google/android/gms/fitness/b/c/z;->a:Lcom/google/android/gms/fitness/b/b;

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/z;->b:Z

    .line 77
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/b/c;)I
    .locals 2

    .prologue
    .line 198
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v0

    return v0
.end method

.method private static a(Ljava/util/Iterator;)Lcom/google/android/gms/fitness/b/c;
    .locals 3

    .prologue
    .line 298
    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 299
    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/c;

    .line 300
    sget-object v1, Lcom/google/android/gms/fitness/b/c/z;->c:Ljava/util/Map;

    invoke-static {v0}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/c;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 304
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/b/f;)Lcom/google/android/gms/fitness/b/f;
    .locals 2

    .prologue
    .line 226
    const-string v0, "com.google.distance.delta"

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/h;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    const-string v1, "pruned_distance"

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/z;->a:Lcom/google/android/gms/fitness/b/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/t;Lcom/google/android/gms/fitness/b/h;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;
    .locals 2

    .prologue
    .line 204
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/t;->a()Lcom/google/android/gms/fitness/b/s;

    move-result-object v0

    .line 205
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/s;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v1

    invoke-interface {v1, p1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/h;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/android/gms/fitness/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v1

    invoke-interface {v1, p3}, Lcom/google/android/gms/fitness/b/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    .line 209
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/s;->d()Lcom/google/android/gms/fitness/b/r;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;
    .locals 3

    .prologue
    .line 110
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/r;

    .line 111
    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 115
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/u;
    .locals 3

    .prologue
    .line 241
    new-instance v0, Lcom/google/android/gms/fitness/b/v;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/b/v;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/fitness/b/v;->a:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/fitness/b/c/z;->b:Z

    iput-boolean v1, v0, Lcom/google/android/gms/fitness/b/v;->e:Z

    new-instance v1, Lcom/google/android/gms/fitness/b/aa;

    invoke-direct {v1}, Lcom/google/android/gms/fitness/b/aa;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/fitness/b/c/z;->a:Lcom/google/android/gms/fitness/b/b;

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/b/aa;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/aa;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/google/android/gms/fitness/b/aa;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/aa;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/fitness/b/aa;->a:Lcom/google/android/gms/fitness/b/y;

    iput-object v1, v0, Lcom/google/android/gms/fitness/b/v;->b:Lcom/google/android/gms/fitness/b/o;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/b/v;->a()Lcom/google/android/gms/fitness/b/u;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;)V
    .locals 4

    .prologue
    .line 260
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/s;->b()Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/c;->b()Lcom/google/android/gms/fitness/b/e;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/d;->a(Lcom/google/android/gms/fitness/b/e;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v1}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/fitness/b/d;->b(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, v1}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/fitness/b/d;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/d;->a()Lcom/google/android/gms/fitness/b/k;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/j;->c()F

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/b/k;->a(F)Lcom/google/android/gms/fitness/b/k;

    .line 265
    return-void
.end method

.method private static a(Lcom/google/android/gms/fitness/b/s;Ljava/util/Iterator;)V
    .locals 1

    .prologue
    .line 253
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/c;

    .line 255
    invoke-static {p0, v0}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;)V

    goto :goto_0

    .line 257
    :cond_0
    return-void
.end method

.method private static b(Lcom/google/android/gms/fitness/b/c;)J
    .locals 4

    .prologue
    .line 293
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v0}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p0, v2}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    const-string v0, "com.google.distance.delta"

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/b/g;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 221
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/g;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/f;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/f;->a()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/List;JJLcom/google/android/gms/fitness/b/t;)Ljava/util/List;
    .locals 16

    .prologue
    .line 82
    const-string v2, "com.google.distance.delta"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/b/c/z;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v3

    .line 83
    const-string v2, "com.google.activity.segment"

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/b/c/z;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    .line 85
    if-nez v3, :cond_0

    .line 86
    const-string v3, "No distance data source found in input"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    .line 87
    sget-object v3, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    const-string v4, "com.google.distance.delta"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/fitness/b/c/z;->d:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-static {v0, v3, v4, v5}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/t;Lcom/google/android/gms/fitness/b/h;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v3

    .line 93
    :cond_0
    if-nez v2, :cond_e

    .line 94
    const-string v2, "No segments data source found in input"

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    .line 95
    sget-object v2, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    const-string v4, "com.google.activity.segment"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/fitness/b/c/z;->e:Ljava/lang/String;

    move-object/from16 v0, p6

    invoke-static {v0, v2, v4, v5}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/t;Lcom/google/android/gms/fitness/b/h;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    move-object v4, v2

    .line 101
    :goto_0
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v5, 0x2

    if-le v2, v5, :cond_1

    .line 102
    const-string v2, "Input contains more than two data streams"

    const/4 v5, 0x0

    invoke-static {v2, v5}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    .line 105
    :cond_1
    invoke-interface/range {p6 .. p6}, Lcom/google/android/gms/fitness/b/t;->a()Lcom/google/android/gms/fitness/b/s;

    move-result-object v6

    invoke-interface {v6}, Lcom/google/android/gms/fitness/b/s;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/f;)Lcom/google/android/gms/fitness/b/f;

    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/r;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v7

    invoke-interface {v4}, Lcom/google/android/gms/fitness/b/r;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/fitness/b/c/z;->a(Ljava/util/Iterator;)Lcom/google/android/gms/fitness/b/c;

    move-result-object v2

    move-object v5, v2

    :goto_1
    if-eqz v5, :cond_9

    :goto_2
    invoke-interface {v7}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v7}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/b/c;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v3}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v3, v10, v12

    if-gtz v3, :cond_2

    const/4 v3, 0x1

    :goto_3
    if-eqz v3, :cond_3

    invoke-static {v6, v2}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;)V

    goto :goto_2

    :cond_2
    const/4 v3, 0x0

    goto :goto_3

    :cond_3
    if-eqz v2, :cond_5

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v3}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v3, v10, v12

    if-lez v3, :cond_5

    const/4 v3, 0x1

    :goto_4
    if-eqz v3, :cond_6

    invoke-interface {v7}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    :cond_4
    invoke-static {v8}, Lcom/google/android/gms/fitness/b/c/z;->a(Ljava/util/Iterator;)Lcom/google/android/gms/fitness/b/c;

    move-result-object v2

    move-object v5, v2

    goto :goto_1

    :cond_5
    const/4 v3, 0x0

    goto :goto_4

    :cond_6
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v3}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v5, v3}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v14

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v12

    sub-long/2addr v10, v12

    invoke-static {v5}, Lcom/google/android/gms/fitness/b/c/z;->b(Lcom/google/android/gms/fitness/b/c;)J

    move-result-wide v12

    invoke-static {v2}, Lcom/google/android/gms/fitness/b/c/z;->b(Lcom/google/android/gms/fitness/b/c;)J

    move-result-wide v14

    add-long/2addr v12, v14

    cmp-long v3, v10, v12

    if-lez v3, :cond_7

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Points [%s] and [%s] do not overlap."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v5, v6, v7

    const/4 v5, 0x1

    aput-object v2, v6, v5

    invoke-static {v4, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_7
    invoke-static {v5}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/c;)I

    move-result v3

    sget-object v9, Lcom/google/android/gms/fitness/b/c/z;->c:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v9, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sget-object v9, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v12, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v9, v10, v11, v12}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v10

    long-to-float v9, v10

    mul-float/2addr v9, v3

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v3

    const/4 v10, 0x0

    invoke-interface {v3, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/j;->c()F

    move-result v3

    cmpl-float v10, v9, v3

    if-ltz v10, :cond_8

    invoke-static {v6, v2}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/c;)V

    goto/16 :goto_2

    :cond_8
    const-string v10, "Reported distance [%fm] exceeds maximum possible distance [%fm]"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v11, v12

    const/4 v3, 0x1

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    aput-object v9, v11, v3

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v2}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    goto/16 :goto_2

    :cond_9
    invoke-interface {v4}, Lcom/google/android/gms/fitness/b/r;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_c

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/b/c;

    :goto_5
    if-eqz v2, :cond_a

    invoke-static {v2}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/c;)I

    move-result v2

    const/4 v3, 0x3

    if-ne v2, v3, :cond_d

    :cond_a
    const/4 v2, 0x1

    :goto_6
    if-nez v2, :cond_b

    invoke-static {v6, v7}, Lcom/google/android/gms/fitness/b/c/z;->a(Lcom/google/android/gms/fitness/b/s;Ljava/util/Iterator;)V

    :cond_b
    invoke-interface {v6}, Lcom/google/android/gms/fitness/b/s;->d()Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    return-object v2

    :cond_c
    const/4 v2, 0x0

    goto :goto_5

    :cond_d
    const/4 v2, 0x0

    goto :goto_6

    :cond_e
    move-object v4, v2

    goto/16 :goto_0
.end method

.method public final b()Ljava/util/List;
    .locals 4

    .prologue
    .line 235
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/fitness/b/u;

    const/4 v1, 0x0

    const-string v2, "com.google.distance.delta"

    iget-object v3, p0, Lcom/google/android/gms/fitness/b/c/z;->d:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/fitness/b/c/z;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/u;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.google.activity.segment"

    iget-object v3, p0, Lcom/google/android/gms/fitness/b/c/z;->e:Ljava/lang/String;

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/fitness/b/c/z;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/fitness/b/u;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
