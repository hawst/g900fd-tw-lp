.class abstract Lcom/google/android/gms/deviceconnection/a/e;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 15
    iput-wide p1, p0, Lcom/google/android/gms/deviceconnection/a/e;->a:J

    .line 16
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 23
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/deviceconnection/a/e;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 25
    iget-wide v0, p0, Lcom/google/android/gms/deviceconnection/a/e;->a:J

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/gms/deviceconnection/a/e;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 26
    return-void

    .line 25
    :catchall_0
    move-exception v0

    iget-wide v2, p0, Lcom/google/android/gms/deviceconnection/a/e;->a:J

    invoke-virtual {p0, p0, v2, v3}, Lcom/google/android/gms/deviceconnection/a/e;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v0
.end method
