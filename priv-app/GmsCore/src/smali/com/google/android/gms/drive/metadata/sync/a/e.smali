.class public abstract enum Lcom/google/android/gms/drive/metadata/sync/a/e;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/metadata/sync/a/e;

.field public static final enum b:Lcom/google/android/gms/drive/metadata/sync/a/e;

.field public static final enum c:Lcom/google/android/gms/drive/metadata/sync/a/e;

.field public static final enum d:Lcom/google/android/gms/drive/metadata/sync/a/e;

.field private static final synthetic f:[Lcom/google/android/gms/drive/metadata/sync/a/e;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 11
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/a/f;

    const-string v1, "CHANGELOG"

    const-string v2, "changes"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/a/f;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->a:Lcom/google/android/gms/drive/metadata/sync/a/e;

    .line 18
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/a/g;

    const-string v1, "FULL"

    const-string v2, "full"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/a/g;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->b:Lcom/google/android/gms/drive/metadata/sync/a/e;

    .line 25
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/a/h;

    const-string v1, "QUERY"

    const-string v2, "query"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/a/h;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    .line 32
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/a/i;

    const-string v1, "APPDATA"

    const-string v2, "appData"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/a/i;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->d:Lcom/google/android/gms/drive/metadata/sync/a/e;

    .line 10
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/metadata/sync/a/e;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/metadata/sync/a/e;->a:Lcom/google/android/gms/drive/metadata/sync/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/drive/metadata/sync/a/e;->b:Lcom/google/android/gms/drive/metadata/sync/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/gms/drive/metadata/sync/a/e;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/gms/drive/metadata/sync/a/e;->d:Lcom/google/android/gms/drive/metadata/sync/a/e;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->f:[Lcom/google/android/gms/drive/metadata/sync/a/e;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 43
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/e;->e:Ljava/lang/String;

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;B)V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/metadata/sync/a/e;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/e;
    .locals 5

    .prologue
    .line 53
    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/a/e;->values()[Lcom/google/android/gms/drive/metadata/sync/a/e;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 54
    iget-object v4, v3, Lcom/google/android/gms/drive/metadata/sync/a/e;->e:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 55
    return-object v3

    .line 53
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown FeedType value: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/e;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/a/e;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/metadata/sync/a/e;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->f:[Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/metadata/sync/a/e;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/metadata/sync/a/e;

    return-object v0
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/c;
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/e;->e:Ljava/lang/String;

    return-object v0
.end method
