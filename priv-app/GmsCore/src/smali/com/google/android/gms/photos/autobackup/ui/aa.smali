.class public final Lcom/google/android/gms/photos/autobackup/ui/aa;
.super Lcom/google/android/libraries/social/a/a/c/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/google/android/libraries/social/a/a/c/a;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;ZZ)Lcom/google/android/gms/photos/autobackup/ui/aa;
    .locals 2

    .prologue
    .line 21
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 22
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 23
    const-string v1, "master_sync_enabled"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 24
    const-string v1, "account_sync_enabled"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 26
    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/aa;

    invoke-direct {v1}, Lcom/google/android/gms/photos/autobackup/ui/aa;-><init>()V

    .line 27
    invoke-virtual {v1, v0}, Lcom/google/android/gms/photos/autobackup/ui/aa;->setArguments(Landroid/os/Bundle;)V

    .line 28
    return-object v1
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/gms/photos/autobackup/ui/ab;

    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/aa;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/photos/autobackup/ui/ab;-><init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;)V

    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/aa;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/aa;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "master_sync_enabled"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/aa;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "account_sync_enabled"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/photos/autobackup/ui/ab;->a(Ljava/lang/String;ZZ)Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/a/a/c/a;->onCancel(Landroid/content/DialogInterface;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/photos/autobackup/ui/aa;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, p0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Landroid/support/v4/app/Fragment;)Lcom/google/android/libraries/social/a/a;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/photos/autobackup/ui/ad;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/ui/ad;

    .line 46
    if-eqz v0, :cond_0

    .line 47
    invoke-interface {v0}, Lcom/google/android/gms/photos/autobackup/ui/ad;->b()V

    .line 49
    :cond_0
    return-void
.end method
