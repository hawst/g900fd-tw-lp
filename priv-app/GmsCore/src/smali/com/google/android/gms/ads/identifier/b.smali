.class final Lcom/google/android/gms/ads/identifier/b;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field a:Ljava/util/concurrent/CountDownLatch;

.field b:Z

.field private c:Ljava/lang/ref/WeakReference;

.field private d:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/identifier/a;J)V
    .locals 2

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 83
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/b;->c:Ljava/lang/ref/WeakReference;

    .line 84
    iput-wide p2, p0, Lcom/google/android/gms/ads/identifier/b;->d:J

    .line 85
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/b;->a:Ljava/util/concurrent/CountDownLatch;

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/identifier/b;->b:Z

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/ads/identifier/b;->start()V

    .line 88
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/b;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/identifier/a;

    .line 100
    if-eqz v0, :cond_0

    .line 101
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/a;->c()V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/identifier/b;->b:Z

    .line 104
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/b;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 92
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 95
    iget-boolean v0, p0, Lcom/google/android/gms/ads/identifier/b;->b:Z

    return v0
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/b;->a:Ljava/util/concurrent/CountDownLatch;

    iget-wide v2, p0, Lcom/google/android/gms/ads/identifier/b;->d:J

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/ads/identifier/b;->c()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    :goto_0
    return-void

    .line 116
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/ads/identifier/b;->c()V

    goto :goto_0
.end method
