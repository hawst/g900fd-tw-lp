.class public final Lcom/google/android/gms/googlehelp/e/m;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 349
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 350
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/m;->cachedSize:I

    .line 351
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 449
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 450
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 451
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 454
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 455
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 458
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 459
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 462
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 463
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 466
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 467
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 470
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 365
    if-ne p1, p0, :cond_1

    .line 407
    :cond_0
    :goto_0
    return v0

    .line 368
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 369
    goto :goto_0

    .line 371
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/m;

    .line 372
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 373
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 374
    goto :goto_0

    .line 376
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 377
    goto :goto_0

    .line 379
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 380
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 381
    goto :goto_0

    .line 383
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 384
    goto :goto_0

    .line 386
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 387
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 388
    goto :goto_0

    .line 390
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 391
    goto :goto_0

    .line 393
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 394
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 395
    goto :goto_0

    .line 397
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 398
    goto :goto_0

    .line 400
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 401
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 402
    goto :goto_0

    .line 404
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 405
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 415
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 417
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 419
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 421
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 423
    return v0

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 415
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 417
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 419
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 421
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 317
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 430
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 433
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 435
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 436
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 438
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 439
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 441
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 442
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 444
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 445
    return-void
.end method
