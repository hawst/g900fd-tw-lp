.class public final Lcom/google/android/gms/people/service/a/b/f;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 30
    move-object v1, p0

    move-object v2, p1

    move-object v3, p6

    move-object v4, p2

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/f;->j:Ljava/lang/String;

    .line 33
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/people/service/a/b/f;->i:Ljava/lang/String;

    .line 34
    return-void

    :cond_0
    move-object p2, p3

    .line 33
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 6

    .prologue
    .line 43
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/f;->j:Ljava/lang/String;

    invoke-virtual {p2, p3, v0}, Lcom/google/android/gms/people/service/e;->f(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V

    .line 47
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/f;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/f;->g:Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/f;->j:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/f;->h:Ljava/lang/String;

    move-object v0, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/people/service/a/b/f;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V

    .line 53
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/f;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/f;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/f;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/f;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 59
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0
.end method
