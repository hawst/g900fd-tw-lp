.class public final Lcom/google/android/gms/smart_profile/card/a/e;
.super Lcom/google/android/gms/smart_profile/card/a/b;
.source "SourceFile"


# instance fields
.field private e:Lcom/google/android/gms/smart_profile/bg;

.field private f:Lcom/google/android/gms/smart_profile/card/a/c;

.field private g:Z

.field private h:Ljava/util/List;

.field private i:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/b;-><init>()V

    return-void
.end method

.method private d()[Lcom/google/ac/c/a/a/a/o;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 119
    invoke-super {p0, p1}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Landroid/os/Bundle;)V

    .line 120
    const-string v0, "is_expanded"

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/e;->i:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 121
    return-void
.end method

.method public final a(Landroid/support/v4/app/au;)V
    .locals 8

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/e;->i:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/e;->h:Ljava/util/List;

    sget-object v2, Lcom/google/android/gms/smart_profile/a/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-boolean v3, p0, Lcom/google/android/gms/smart_profile/card/a/e;->g:Z

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/a/e;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v4}, Lcom/google/android/gms/smart_profile/bg;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v4

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/card/a/e;->f:Lcom/google/android/gms/smart_profile/card/a/c;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/card/a/e;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v5}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v7

    move-object v5, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Ljava/util/List;IZLcom/google/android/gms/common/api/v;Landroid/support/v4/app/au;Lcom/google/android/gms/smart_profile/card/a/c;I)V

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/e;->i:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/b;->c:Lcom/google/android/gms/smart_profile/card/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/h;)V

    .line 115
    return-void
.end method

.method public final a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 50
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/smart_profile/card/a/b;->a(Lcom/google/android/gms/smart_profile/card/a/c;Landroid/os/Bundle;Lcom/google/android/gms/smart_profile/bg;)V

    .line 52
    if-nez p3, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/e;->d()[Lcom/google/ac/c/a/a/a/o;

    move-result-object v4

    .line 57
    if-eqz v4, :cond_0

    .line 61
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/a/e;->f:Lcom/google/android/gms/smart_profile/card/a/c;

    .line 62
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/card/a/e;->e:Lcom/google/android/gms/smart_profile/bg;

    .line 65
    iput-boolean v3, p0, Lcom/google/android/gms/smart_profile/card/a/e;->g:Z

    .line 66
    if-eqz p2, :cond_2

    .line 67
    const-string v0, "is_expanded"

    invoke-virtual {p2, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/smart_profile/card/a/e;->g:Z

    .line 71
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/e;->h:Ljava/util/List;

    .line 72
    array-length v5, v4

    move v2, v3

    :goto_1
    if-ge v2, v5, :cond_4

    aget-object v6, v4, v2

    .line 73
    const/4 v0, 0x0

    .line 74
    iget-object v1, v6, Lcom/google/ac/c/a/a/a/o;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 76
    :try_start_0
    iget-object v1, v6, Lcom/google/ac/c/a/a/a/o;->e:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v1, v7}, Landroid/content/Intent;->parseUri(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 82
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/a/e;->h:Ljava/util/List;

    new-instance v7, Lcom/google/android/gms/smart_profile/card/view/h;

    invoke-direct {v7}, Lcom/google/android/gms/smart_profile/card/view/h;-><init>()V

    iget-object v8, v6, Lcom/google/ac/c/a/a/a/o;->a:Ljava/lang/String;

    iget-object v9, v7, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    iput-object v8, v9, Lcom/google/android/gms/smart_profile/card/view/g;->b:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/ac/c/a/a/a/o;->b:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v7

    iget-object v8, v6, Lcom/google/ac/c/a/a/a/o;->c:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/smart_profile/card/view/h;->b(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v7

    iget-object v6, v6, Lcom/google/ac/c/a/a/a/o;->d:Ljava/lang/String;

    invoke-virtual {v7, v6}, Lcom/google/android/gms/smart_profile/card/view/h;->c(Ljava/lang/String;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v6

    invoke-virtual {v6, v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a(Landroid/content/Intent;)Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/card/view/h;->a()Lcom/google/android/gms/smart_profile/card/view/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/smart_profile/card/view/h;->a:Lcom/google/android/gms/smart_profile/card/view/g;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 72
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 77
    :catch_0
    move-exception v1

    .line 78
    const-string v7, "SmartProfile"

    invoke-virtual {v1}, Ljava/net/URISyntaxException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v7, v1}, Lcom/google/android/gms/smart_profile/bc;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 93
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    if-nez v0, :cond_6

    :cond_5
    const-string v0, ""

    move-object v1, v0

    .line 94
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/support/v7/widget/CardView;

    sget v2, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/CardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 95
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/a/e;->e:Lcom/google/android/gms/smart_profile/bg;

    invoke-virtual {v2}, Lcom/google/android/gms/smart_profile/bg;->q()I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 96
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 97
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->b:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/e;->i:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/e;->i:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    sget-object v1, Lcom/google/android/gms/smart_profile/c;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0

    .line 93
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/a/b;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    move-object v1, v0

    goto :goto_3

    .line 99
    :cond_7
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/card/a/e;->d()[Lcom/google/ac/c/a/a/a/o;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 131
    sget-object v0, Lcom/google/android/gms/smart_profile/c;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method
