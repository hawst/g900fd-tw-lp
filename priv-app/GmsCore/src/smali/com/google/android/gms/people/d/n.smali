.class public final Lcom/google/android/gms/people/d/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/people/d/r;

.field public b:I

.field public c:J

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:Ljava/lang/String;

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 348
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 349
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    iput v2, p0, Lcom/google/android/gms/people/d/n;->b:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/n;->c:J

    iput-boolean v2, p0, Lcom/google/android/gms/people/d/n;->d:Z

    iput-boolean v2, p0, Lcom/google/android/gms/people/d/n;->e:Z

    iput-boolean v2, p0, Lcom/google/android/gms/people/d/n;->f:Z

    iput-boolean v2, p0, Lcom/google/android/gms/people/d/n;->g:Z

    iput-boolean v2, p0, Lcom/google/android/gms/people/d/n;->h:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/gms/people/d/n;->j:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/n;->cachedSize:I

    .line 350
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 476
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 477
    iget-object v1, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v1, :cond_0

    .line 478
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    :cond_0
    iget v1, p0, Lcom/google/android/gms/people/d/n;->b:I

    if-eqz v1, :cond_1

    .line 482
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/people/d/n;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 485
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/people/d/n;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 486
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/people/d/n;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 489
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->d:Z

    if-eqz v1, :cond_3

    .line 490
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 493
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->e:Z

    if-eqz v1, :cond_4

    .line 494
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 497
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->g:Z

    if-eqz v1, :cond_5

    .line 498
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 501
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->f:Z

    if-eqz v1, :cond_6

    .line 502
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 505
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->h:Z

    if-eqz v1, :cond_7

    .line 506
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 509
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 510
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 513
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->j:Z

    if-eqz v1, :cond_9

    .line 514
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->j:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 517
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 369
    if-ne p1, p0, :cond_1

    .line 416
    :cond_0
    :goto_0
    return v0

    .line 372
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 373
    goto :goto_0

    .line 375
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/n;

    .line 376
    iget-object v2, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    if-nez v2, :cond_3

    .line 377
    iget-object v2, p1, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v2, :cond_4

    move v0, v1

    .line 378
    goto :goto_0

    .line 381
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    iget-object v3, p1, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 382
    goto :goto_0

    .line 385
    :cond_4
    iget v2, p0, Lcom/google/android/gms/people/d/n;->b:I

    iget v3, p1, Lcom/google/android/gms/people/d/n;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 386
    goto :goto_0

    .line 388
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/people/d/n;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/n;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 389
    goto :goto_0

    .line 391
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->d:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/n;->d:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 392
    goto :goto_0

    .line 394
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->e:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/n;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 395
    goto :goto_0

    .line 397
    :cond_8
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->f:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/n;->f:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 398
    goto :goto_0

    .line 400
    :cond_9
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->g:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/n;->g:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 401
    goto :goto_0

    .line 403
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->h:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/n;->h:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 404
    goto :goto_0

    .line 406
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 407
    iget-object v2, p1, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 408
    goto :goto_0

    .line 410
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 411
    goto :goto_0

    .line 413
    :cond_d
    iget-boolean v2, p0, Lcom/google/android/gms/people/d/n;->j:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/d/n;->j:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 414
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 424
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/people/d/n;->b:I

    add-int/2addr v0, v4

    .line 425
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/android/gms/people/d/n;->c:J

    iget-wide v6, p0, Lcom/google/android/gms/people/d/n;->c:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 427
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->d:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    .line 428
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->e:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    .line 429
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->f:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 430
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->g:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 431
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->h:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v4

    .line 432
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    if-nez v4, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 434
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->j:Z

    if-eqz v1, :cond_7

    :goto_7
    add-int/2addr v0, v2

    .line 435
    return v0

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/r;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 427
    goto :goto_1

    :cond_2
    move v0, v3

    .line 428
    goto :goto_2

    :cond_3
    move v0, v3

    .line 429
    goto :goto_3

    :cond_4
    move v0, v3

    .line 430
    goto :goto_4

    :cond_5
    move v0, v3

    .line 431
    goto :goto_5

    .line 432
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6

    :cond_7
    move v2, v3

    .line 434
    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 301
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/r;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/r;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/n;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/n;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/n;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/n;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/n;->g:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/n;->f:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/n;->h:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/d/n;->j:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    if-eqz v0, :cond_0

    .line 442
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/people/d/n;->a:Lcom/google/android/gms/people/d/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 444
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/d/n;->b:I

    if-eqz v0, :cond_1

    .line 445
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/people/d/n;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 447
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/people/d/n;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 448
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/people/d/n;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 450
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->d:Z

    if-eqz v0, :cond_3

    .line 451
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 453
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->e:Z

    if-eqz v0, :cond_4

    .line 454
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 456
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->g:Z

    if-eqz v0, :cond_5

    .line 457
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 459
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->f:Z

    if-eqz v0, :cond_6

    .line 460
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 462
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->h:Z

    if-eqz v0, :cond_7

    .line 463
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 465
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 466
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/people/d/n;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 468
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/people/d/n;->j:Z

    if-eqz v0, :cond_9

    .line 469
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/android/gms/people/d/n;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 471
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 472
    return-void
.end method
