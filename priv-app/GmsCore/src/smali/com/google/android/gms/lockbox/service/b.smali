.class final Lcom/google/android/gms/lockbox/service/b;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/lockbox/service/LockboxBrokerService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/lockbox/service/LockboxBrokerService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/gms/lockbox/service/b;->a:Lcom/google/android/gms/lockbox/service/LockboxBrokerService;

    .line 50
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 51
    return-void
.end method


# virtual methods
.method public final l(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/b;->a:Lcom/google/android/gms/lockbox/service/LockboxBrokerService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/lockbox/service/b;->a:Lcom/google/android/gms/lockbox/service/LockboxBrokerService;

    invoke-virtual {v0}, Lcom/google/android/gms/lockbox/service/LockboxBrokerService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/google/android/gms/lockbox/service/a;

    iget-object v2, p0, Lcom/google/android/gms/lockbox/service/b;->a:Lcom/google/android/gms/lockbox/service/LockboxBrokerService;

    invoke-direct {v1, v2}, Lcom/google/android/gms/lockbox/service/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/google/android/gms/lockbox/service/a;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    .line 68
    const-string v1, "LockboxBrokerService"

    const-string v2, "Client died while brokering service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
