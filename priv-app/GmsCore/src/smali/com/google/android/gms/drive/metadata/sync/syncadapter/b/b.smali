.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/r;

.field private final b:Lcom/google/android/gms/drive/database/model/a;

.field private final c:Ljava/util/Set;

.field private final d:I

.field private final e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;ILcom/google/android/gms/drive/metadata/sync/syncadapter/g;)V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->a:Lcom/google/android/gms/drive/database/r;

    .line 42
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->b:Lcom/google/android/gms/drive/database/model/a;

    .line 43
    iput p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->d:I

    .line 44
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->a:Lcom/google/android/gms/drive/database/r;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->c:Ljava/util/Set;

    .line 46
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 8

    .prologue
    .line 82
    if-nez p2, :cond_1

    .line 92
    :cond_0
    return-void

    .line 85
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v2, v0, Lcom/google/android/gms/drive/database/model/a;->b:J

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 87
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->a:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v4, v2, v3, v6, v7}, Lcom/google/android/gms/drive/database/r;->a(JJ)Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    .line 88
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v4, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;

    move-result-object v0

    .line 89
    const/4 v4, 0x1

    iput-boolean v4, v0, Lcom/google/android/gms/drive/database/model/i;->c:Z

    .line 90
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/i;->i()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V
    .locals 8

    .prologue
    const-wide v6, 0x7fffffffffffffffL

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->b:Lcom/google/android/gms/drive/database/model/a;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, p2, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;->a(Landroid/content/SyncResult;Lcom/google/android/gms/drive/database/model/a;Ljava/lang/Boolean;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    move-result-object v0

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->e:Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;

    invoke-static {v0, v6, v7}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/g;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;J)Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->c:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->b:Lcom/google/android/gms/drive/database/model/a;

    new-instance v4, Lcom/google/android/gms/drive/metadata/sync/a/a;

    invoke-direct {v4, v1}, Lcom/google/android/gms/drive/metadata/sync/a/a;-><init>(Ljava/util/Set;)V

    invoke-interface {v2, v3, v4, v6, v7}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/c;-><init>(Lcom/google/android/gms/drive/database/model/bb;Lcom/google/android/gms/drive/metadata/sync/syncadapter/w;)V

    iget-object v0, v1, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->b:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->d:I

    invoke-virtual {p1, v0, v1, v3, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;->a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;ILcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 62
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/b;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
