.class public final Lcom/google/android/gms/common/analytics/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 50
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-static {v0, v1, v1}, Lcom/google/android/gms/common/analytics/e;->a(Ljava/util/List;ZZ)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 133
    if-nez p0, :cond_0

    move-object v0, v1

    .line 152
    :goto_0
    return-object v0

    .line 136
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 137
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    .line 138
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    .line 139
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 140
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 141
    new-instance v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bz;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 146
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 147
    const-string v0, "AclDetails"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 148
    const-string v0, "AclDetails"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no LoggedCircles added for circle IDs: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v0, v1

    .line 150
    goto :goto_0

    .line 152
    :cond_4
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    goto :goto_0
.end method

.method public static a(Ljava/util/List;ZZ)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v10, 0x5

    .line 61
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 62
    if-eqz p2, :cond_0

    .line 63
    const-string v0, "6"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    :cond_0
    if-eqz p1, :cond_1

    .line 66
    const-string v0, "5"

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 68
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    .line 122
    :goto_0
    return-object v0

    .line 74
    :cond_1
    if-nez p0, :cond_2

    move-object v0, v1

    .line 75
    goto :goto_0

    .line 77
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 78
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 80
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v6

    .line 81
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v6, :cond_c

    .line 82
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 83
    if-eqz v0, :cond_3

    .line 84
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->h()Z

    move-result v7

    if-eqz v7, :cond_4

    .line 87
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    invoke-direct {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;-><init>()V

    invoke-virtual {v7, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bz;

    move-result-object v0

    .line 88
    if-eqz v0, :cond_3

    .line 89
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 81
    :cond_3
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 91
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v7

    if-eqz v7, :cond_9

    .line 92
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_6

    const-string v7, "AclDetails"

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_5

    const-string v7, "AclDetails"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "unhandled people qualified ID: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move-object v0, v1

    .line 94
    :goto_3
    if-eqz v0, :cond_3

    .line 95
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 92
    :cond_6
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;-><init>()V

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_7

    invoke-virtual {v0, v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;

    :cond_7
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_8

    invoke-virtual {v0, v8}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;

    :cond_8
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/cc;

    move-result-object v0

    goto :goto_3

    .line 97
    :cond_9
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->i()Z

    move-result v7

    if-eqz v7, :cond_b

    .line 98
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v7

    packed-switch v7, :pswitch_data_0

    const-string v7, "AclDetails"

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_a

    const-string v7, "AclDetails"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "unhandled PeopleConstants.CircleType: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    const-string v0, "0"

    .line 100
    :goto_4
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 98
    :pswitch_0
    const-string v0, "1"

    goto :goto_4

    :pswitch_1
    const-string v0, "2"

    goto :goto_4

    :pswitch_2
    const-string v0, "4"

    goto :goto_4

    :pswitch_3
    const-string v0, "3"

    goto :goto_4

    .line 102
    :cond_b
    const-string v7, "AclDetails"

    invoke-static {v7, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 103
    const-string v7, "AclDetails"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "unhandled AudienceMember type: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 106
    :cond_c
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 107
    const-string v0, "AclDetails"

    invoke-static {v0, v10}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 108
    const-string v0, "AclDetails"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no AclDetails from audience: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    move-object v0, v1

    .line 110
    goto/16 :goto_0

    .line 112
    :cond_e
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;-><init>()V

    .line 113
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    .line 114
    invoke-virtual {v0, v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    .line 116
    :cond_f
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_10

    .line 117
    iput-object v5, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a:Ljava/util/List;

    iget-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->b:Ljava/util/Set;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 119
    :cond_10
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_11

    .line 120
    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    .line 122
    :cond_11
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    goto/16 :goto_0

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 165
    if-nez p0, :cond_0

    move-object v0, v1

    .line 188
    :goto_0
    return-object v0

    .line 168
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 169
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    .line 170
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    .line 171
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 172
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 173
    const-string v5, "p"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "s"

    invoke-virtual {v0, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 175
    const-string v5, "AclDetails"

    const-string v6, "Circle ID should start with \'p\' or \'s\'"

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_1
    new-instance v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bz;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 182
    :cond_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 183
    const-string v0, "AclDetails"

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 184
    const-string v0, "AclDetails"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no LoggedCircles added for circle IDs: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move-object v0, v1

    .line 186
    goto :goto_0

    .line 188
    :cond_5
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    goto :goto_0
.end method
