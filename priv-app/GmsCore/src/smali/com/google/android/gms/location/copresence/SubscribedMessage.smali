.class public final Lcom/google/android/gms/location/copresence/SubscribedMessage;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/gms/location/copresence/Message;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/location/copresence/w;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/w;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/location/copresence/Message;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput p1, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->a:I

    .line 44
    iput-object p2, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->b:Lcom/google/android/gms/location/copresence/Message;

    .line 45
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/location/copresence/Message;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->a:I

    .line 56
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/Message;

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->b:Lcom/google/android/gms/location/copresence/Message;

    .line 57
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->a:I

    return v0
.end method

.method public final b()Lcom/google/android/gms/location/copresence/Message;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->b:Lcom/google/android/gms/location/copresence/Message;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 80
    if-ne p0, p1, :cond_1

    .line 81
    const/4 v0, 0x1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    if-eqz p1, :cond_0

    .line 86
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    .line 89
    check-cast p1, Lcom/google/android/gms/location/copresence/SubscribedMessage;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->b:Lcom/google/android/gms/location/copresence/Message;

    iget-object v1, p1, Lcom/google/android/gms/location/copresence/SubscribedMessage;->b:Lcom/google/android/gms/location/copresence/Message;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/location/copresence/SubscribedMessage;->b:Lcom/google/android/gms/location/copresence/Message;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 100
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/w;->a(Lcom/google/android/gms/location/copresence/SubscribedMessage;Landroid/os/Parcel;I)V

    .line 101
    return-void
.end method
