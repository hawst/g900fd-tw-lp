.class public final Lcom/google/android/gms/plus/service/DefaultIntentService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static b:Lcom/google/android/gms/common/service/d;


# instance fields
.field private c:Lcom/google/android/gms/plus/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    const-string v0, "DefaultIntentService"

    sget-object v1, Lcom/google/android/gms/plus/service/DefaultIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 30
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    .locals 2

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    .line 36
    const-string v0, "com.google.android.gms.plus.service.default.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/plus/service/DefaultIntentService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 38
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 39
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/b/b;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/plus/service/DefaultIntentService;->c:Lcom/google/android/gms/plus/b/b;

    return-object v0
.end method

.method public final onCreate()V
    .locals 1

    .prologue
    .line 43
    invoke-super {p0}, Lcom/google/android/gms/common/service/c;->onCreate()V

    .line 44
    invoke-static {p0}, Lcom/google/android/gms/plus/b/b;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/b/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/service/DefaultIntentService;->c:Lcom/google/android/gms/plus/b/b;

    .line 45
    return-void
.end method

.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 50
    const-string v0, "isLoggingIntent"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    sget-object v0, Lcom/google/android/gms/plus/service/DefaultIntentService;->b:Lcom/google/android/gms/common/service/d;

    new-instance v1, Lcom/google/android/gms/plus/a/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/a/c;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/service/d;->addFirst(Ljava/lang/Object;)V

    .line 53
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/service/c;->onHandleIntent(Landroid/content/Intent;)V

    .line 54
    return-void
.end method
