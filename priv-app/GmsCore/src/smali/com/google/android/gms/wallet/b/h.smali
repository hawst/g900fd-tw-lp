.class public final Lcom/google/android/gms/wallet/b/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 308
    const-string v0, "wallet.wallet_balance.default_value"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/b/h;->a:Lcom/google/android/gms/common/a/d;

    .line 320
    const-string v0, "wallet.wallet_balance.two_line_payment_description"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/b/h;->b:Lcom/google/android/gms/common/a/d;

    .line 329
    const-string v0, "wallet.wallet_balance.allowed_with_billing_agreement"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/b/h;->c:Lcom/google/android/gms/common/a/d;

    return-void
.end method
