.class final Lcom/google/android/gms/plus/service/k;
.super Lcom/google/android/gms/plus/internal/q;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Lcom/google/android/gms/common/images/a;

.field private final e:Lcom/google/android/gms/plus/service/i;

.field private final f:[Ljava/lang/String;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 311
    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/q;-><init>()V

    .line 312
    iput-object p1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    .line 313
    iput-object p2, p0, Lcom/google/android/gms/plus/service/k;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 314
    iput-object p3, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 315
    invoke-static {p1}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/service/k;->d:Lcom/google/android/gms/common/images/a;

    .line 316
    iput-object p4, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    .line 317
    iput-object p5, p0, Lcom/google/android/gms/plus/service/k;->f:[Ljava/lang/String;

    .line 318
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/internal/c;IIILjava/lang/String;)Lcom/google/android/gms/common/internal/bd;
    .locals 7

    .prologue
    .line 394
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    new-instance v0, Lcom/google/android/gms/plus/service/a/x;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/a/x;-><init>(Lcom/google/android/gms/common/server/ClientContext;IIILjava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 397
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 399
    new-instance v1, Lcom/google/android/gms/plus/service/a/y;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/service/a/y;-><init>(Lcom/google/android/gms/plus/service/a/x;)V

    return-object v1
.end method

.method public final a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 455
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 456
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.permission.GET_ACCOUNTS"

    iget-object v2, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 461
    :goto_0
    if-nez v0, :cond_1

    .line 462
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Missing android.permission.GET_ACCOUNTS"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 456
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/service/k;->a(Lcom/google/android/gms/plus/internal/c;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V

    .line 360
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/c;)V
    .locals 3

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/o;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/plus/service/a/o;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/c;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 448
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/c;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 348
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 349
    invoke-static {p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The userId parameter is required."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 351
    new-instance v0, Lcom/google/android/gms/plus/service/a/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    const-string v7, "vault"

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v8, p1

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/service/a/v;-><init>(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 354
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 355
    return-void

    .line 349
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/c;Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 489
    const/4 v0, 0x0

    .line 490
    if-eqz p3, :cond_0

    .line 491
    const-string v0, "bounding_box"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 493
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/plus/service/a/aa;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/k;->d:Lcom/google/android/gms/common/images/a;

    invoke-direct {v2, v3, p2, v0, p1}, Lcom/google/android/gms/plus/service/a/aa;-><init>(Lcom/google/android/gms/common/images/a;Landroid/net/Uri;ILcom/google/android/gms/plus/internal/c;)V

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/service/ImageIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 495
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/c;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
    .locals 3

    .prologue
    .line 366
    if-nez p2, :cond_0

    .line 367
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "momentJson must not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 370
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->toString()Ljava/lang/String;

    move-result-object v0

    .line 372
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 377
    new-instance v1, Lcom/google/android/gms/plus/service/a/av;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, v0, p1}, Lcom/google/android/gms/plus/service/a/av;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 380
    return-void

    .line 373
    :catch_0
    move-exception v0

    .line 374
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "momentJson must be valid JSON"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 322
    const-string v0, "URL must not be null."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 325
    new-instance v0, Lcom/google/android/gms/plus/service/a/ae;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/ae;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 327
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 328
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 332
    new-instance v0, Lcom/google/android/gms/plus/service/a/s;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p3, p1}, Lcom/google/android/gms/plus/service/a/s;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 334
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 335
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/c;Ljava/util/List;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 405
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 409
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    .line 410
    :goto_1
    if-ge v1, v2, :cond_1

    .line 411
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v3, "personId cannot be empty."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 410
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 407
    goto :goto_0

    .line 414
    :cond_1
    new-instance v0, Lcom/google/android/gms/plus/service/a/l;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/l;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/util/List;Lcom/google/android/gms/plus/internal/c;)V

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 417
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 385
    new-instance v0, Lcom/google/android/gms/plus/service/a/aj;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/plus/service/a/aj;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 387
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 388
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    .prologue
    .line 523
    new-instance v0, Lcom/google/android/gms/common/server/y;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/k;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/y;->e()Landroid/content/Intent;

    move-result-object v0

    .line 530
    new-instance v1, Lcom/google/android/gms/plus/a/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/plus/a/c;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    .line 532
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 533
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/e;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/service/a/e;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 477
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/internal/c;)V
    .locals 3

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/e;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/service/a/e;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 483
    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    new-instance v1, Lcom/google/android/gms/plus/service/a/al;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v1, v2, p1}, Lcom/google/android/gms/plus/service/a/al;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/c;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 485
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/gms/plus/service/a/f;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/f;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 341
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 342
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 505
    const/4 v0, 0x0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 433
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The userId parameter is required."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 436
    new-instance v0, Lcom/google/android/gms/plus/service/a/m;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/m;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 438
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 439
    return-void

    .line 434
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 500
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 511
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 517
    const/4 v0, 0x0

    return-object v0
.end method

.method public final e(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 422
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 423
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "The userId parameter is required."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 425
    new-instance v0, Lcom/google/android/gms/plus/service/a/n;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p1}, Lcom/google/android/gms/plus/service/a/n;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/plus/internal/c;)V

    .line 427
    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->e:Lcom/google/android/gms/plus/service/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/k;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/plus/service/DefaultIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 428
    return-void

    .line 423
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
