.class public Lcom/google/android/gms/appdatasearch/StorageStats;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ax;


# instance fields
.field final a:I

.field public final b:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

.field public final c:J

.field public final d:J

.field public final e:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/appdatasearch/ax;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ax;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/StorageStats;->CREATOR:Lcom/google/android/gms/appdatasearch/ax;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;JJJ)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput p1, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->a:I

    .line 60
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->b:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    .line 61
    iput-wide p3, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->c:J

    .line 62
    iput-wide p5, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->d:J

    .line 63
    iput-wide p7, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->e:J

    .line 64
    return-void
.end method

.method public constructor <init>([Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;JJJ)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->a:I

    .line 69
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->b:[Lcom/google/android/gms/appdatasearch/RegisteredPackageInfo;

    .line 70
    iput-wide p2, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->c:J

    .line 71
    iput-wide p4, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->d:J

    .line 72
    iput-wide p6, p0, Lcom/google/android/gms/appdatasearch/StorageStats;->e:J

    .line 73
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 78
    sget-object v0, Lcom/google/android/gms/appdatasearch/StorageStats;->CREATOR:Lcom/google/android/gms/appdatasearch/ax;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 84
    sget-object v0, Lcom/google/android/gms/appdatasearch/StorageStats;->CREATOR:Lcom/google/android/gms/appdatasearch/ax;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ax;->a(Lcom/google/android/gms/appdatasearch/StorageStats;Landroid/os/Parcel;I)V

    .line 85
    return-void
.end method
