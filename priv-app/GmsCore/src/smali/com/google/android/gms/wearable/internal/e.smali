.class public final Lcom/google/android/gms/wearable/internal/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/gms/wearable/internal/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/wearable/internal/i;-><init>(Lcom/google/android/gms/wearable/internal/e;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Landroid/net/Uri;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/android/gms/wearable/internal/h;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/wearable/internal/h;-><init>(Lcom/google/android/gms/wearable/internal/e;Lcom/google/android/gms/common/api/v;Landroid/net/Uri;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/wearable/PutDataRequest;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/gms/wearable/internal/f;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/wearable/internal/f;-><init>(Lcom/google/android/gms/wearable/internal/e;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/wearable/PutDataRequest;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Lcom/google/android/gms/wearable/internal/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/wearable/internal/g;-><init>(Lcom/google/android/gms/wearable/internal/e;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
