.class public final Lcom/google/android/gms/ads/internal/h/d;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 6

    .prologue
    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 37
    const v0, 0x6768a8

    iput v0, p0, Lcom/google/android/gms/ads/internal/h/d;->a:I

    .line 38
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 22
    invoke-static {p1}, Lcom/google/android/gms/ads/internal/h/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/ads/internal/h/e;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 62
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 63
    iget v1, p0, Lcom/google/android/gms/ads/internal/h/d;->a:I

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 68
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    const-string v0, "com.google.android.gms.ads.gservice.START"

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "com.google.android.gms.ads.internal.gservice.IGservicesValueService"

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/ads/internal/h/e;
    .locals 1

    .prologue
    .line 71
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/h/e;

    return-object v0
.end method
