.class final Lcom/google/android/gms/fitness/sensors/sample/b;
.super Lcom/google/android/gms/fitness/data/m;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/fitness/data/l;

.field private final b:Ljava/util/concurrent/CountDownLatch;

.field private final c:J

.field private final d:Lcom/google/android/gms/fitness/sensors/sample/c;

.field private final e:I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/data/l;IILcom/google/android/gms/fitness/sensors/sample/c;I)V
    .locals 6

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/fitness/data/m;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->a:Lcom/google/android/gms/fitness/data/l;

    .line 116
    iput-object p4, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->d:Lcom/google/android/gms/fitness/sensors/sample/c;

    .line 117
    iput p5, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->e:I

    .line 118
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, p2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->b:Ljava/util/concurrent/CountDownLatch;

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    int-to-long v4, p3

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->c:J

    .line 120
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/data/l;IILcom/google/android/gms/fitness/sensors/sample/c;IB)V
    .locals 0

    .prologue
    .line 96
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/fitness/sensors/sample/b;-><init>(Lcom/google/android/gms/fitness/data/l;IILcom/google/android/gms/fitness/sensors/sample/c;I)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 134
    const-string v0, "%s. unregistering..."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->d:Lcom/google/android/gms/fitness/sensors/sample/c;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/sample/c;->b()Lcom/google/android/gms/fitness/sensors/a;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/data/l;)Z

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->d:Lcom/google/android/gms/fitness/sensors/sample/c;

    iget v1, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->e:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/sample/c;->b(I)V

    .line 137
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->a:Lcom/google/android/gms/fitness/data/l;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/data/l;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_1

    .line 127
    const-string v0, "All events received"

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/sample/b;->a(Ljava/lang/String;)V

    .line 131
    :cond_0
    :goto_0
    return-void

    .line 128
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/fitness/sensors/sample/b;->c:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 129
    const-string v0, "Timeout reached"

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/sample/b;->a(Ljava/lang/String;)V

    goto :goto_0
.end method
