.class public final Lcom/google/android/gms/ads/internal/k/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:Z

.field public c:Lcom/google/android/gms/ads/internal/k/g;

.field private final d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

.field private final e:Lcom/google/android/gms/ads/internal/k/a/a;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/gms/ads/internal/k/d;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/k/d;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/k/a;->a:Ljava/lang/Object;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/k/a;->b:Z

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/k/a;->f:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/k/a;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/k/a;->e:Lcom/google/android/gms/ads/internal/k/a/a;

    .line 37
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/k/a;->g:Lcom/google/android/gms/ads/internal/k/d;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(J)Lcom/google/android/gms/ads/internal/k/i;
    .locals 13

    .prologue
    .line 57
    const-string v0, "Starting mediation."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/a;->g:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/k/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/ads/internal/k/c;

    .line 60
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Trying mediation network: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v5, Lcom/google/android/gms/ads/internal/k/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 61
    iget-object v0, v5, Lcom/google/android/gms/ads/internal/k/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_1
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 63
    iget-object v11, p0, Lcom/google/android/gms/ads/internal/k/a;->a:Ljava/lang/Object;

    monitor-enter v11

    .line 64
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/k/a;->b:Z

    if-eqz v0, :cond_2

    .line 65
    new-instance v0, Lcom/google/android/gms/ads/internal/k/i;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/k/i;-><init>(I)V

    monitor-exit v11

    .line 105
    :goto_1
    return-object v0

    .line 67
    :cond_2
    new-instance v0, Lcom/google/android/gms/ads/internal/k/g;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/a;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/k/a;->e:Lcom/google/android/gms/ads/internal/k/a/a;

    iget-object v4, p0, Lcom/google/android/gms/ads/internal/k/a;->g:Lcom/google/android/gms/ads/internal/k/d;

    iget-object v6, p0, Lcom/google/android/gms/ads/internal/k/a;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v6, v6, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    iget-object v7, p0, Lcom/google/android/gms/ads/internal/k/a;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v7, v7, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iget-object v8, p0, Lcom/google/android/gms/ads/internal/k/a;->d:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v8, v8, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->k:Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/ads/internal/k/g;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/a/a;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/k/a;->c:Lcom/google/android/gms/ads/internal/k/g;

    .line 76
    monitor-exit v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/a;->c:Lcom/google/android/gms/ads/internal/k/g;

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, p1, p2, v2, v3}, Lcom/google/android/gms/ads/internal/k/g;->a(JJ)Lcom/google/android/gms/ads/internal/k/i;

    move-result-object v0

    .line 83
    iget v1, v0, Lcom/google/android/gms/ads/internal/k/i;->a:I

    if-nez v1, :cond_3

    .line 84
    const-string v1, "Adapter succeeded."

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0

    .line 89
    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/ads/internal/k/i;->c:Lcom/google/android/gms/ads/internal/k/a/d;

    if-eqz v1, :cond_1

    .line 91
    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/ads/internal/k/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/ads/internal/k/b;-><init>(Lcom/google/android/gms/ads/internal/k/a;Lcom/google/android/gms/ads/internal/k/i;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 105
    :cond_4
    new-instance v0, Lcom/google/android/gms/ads/internal/k/i;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/internal/k/i;-><init>(I)V

    goto :goto_1
.end method
