.class public final Lcom/google/android/gms/drive/database/model/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/database/i;

.field final b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljavax/crypto/SecretKey;

.field final f:J

.field final g:J


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;JJ)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ay;->a:Lcom/google/android/gms/drive/database/i;

    .line 39
    invoke-static {p2}, Lcom/google/android/gms/drive/g/y;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ay;->b:Ljava/lang/String;

    .line 40
    iput-wide p3, p0, Lcom/google/android/gms/drive/database/model/ay;->f:J

    .line 41
    iput-wide p5, p0, Lcom/google/android/gms/drive/database/model/ay;->g:J

    .line 42
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;JJB)V
    .locals 1

    .prologue
    .line 27
    invoke-direct/range {p0 .. p6}, Lcom/google/android/gms/drive/database/model/ay;-><init>(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;JJ)V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/database/model/ax;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/gms/drive/database/model/ax;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/drive/database/model/ax;-><init>(Lcom/google/android/gms/drive/database/model/ay;B)V

    return-object v0
.end method
