.class public final Lcom/google/android/gms/plus/apps/ap;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/apps/j;


# instance fields
.field private final a:Lcom/google/android/gms/plus/apps/c;

.field private final b:Landroid/view/LayoutInflater;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private e:Lcom/google/android/gms/plus/apps/aq;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/c;Lcom/google/android/gms/plus/apps/aq;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->d:Ljava/util/ArrayList;

    .line 58
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->b:Landroid/view/LayoutInflater;

    .line 59
    iput-object p2, p0, Lcom/google/android/gms/plus/apps/ap;->a:Lcom/google/android/gms/plus/apps/c;

    .line 60
    iput-object p3, p0, Lcom/google/android/gms/plus/apps/ap;->e:Lcom/google/android/gms/plus/apps/aq;

    .line 61
    return-void
.end method

.method private c()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->e:Lcom/google/android/gms/plus/apps/aq;

    invoke-interface {v0}, Lcom/google/android/gms/plus/apps/aq;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 105
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ap;->notifyDataSetChanged()V

    .line 93
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/apps/aq;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/ap;->e:Lcom/google/android/gms/plus/apps/aq;

    .line 65
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 186
    if-eqz p1, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 189
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ap;->notifyDataSetChanged()V

    .line 190
    return-void
.end method

.method public final a(Ljava/util/Collection;Z)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 175
    iput-boolean p2, p0, Lcom/google/android/gms/plus/apps/ap;->f:Z

    .line 176
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/a/a;

    .line 177
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ap;->e:Lcom/google/android/gms/plus/apps/aq;

    invoke-interface {v2, v0}, Lcom/google/android/gms/plus/apps/aq;->a(Lcom/google/android/gms/plus/model/a/a;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 181
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ap;->notifyDataSetChanged()V

    .line 182
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/apps/ap;->f:Z

    .line 196
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ap;->notifyDataSetInvalidated()V

    .line 197
    return-void
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ap;->c()I

    move-result v1

    add-int/2addr v1, v0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/apps/ap;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 126
    :goto_0
    return-object v0

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v0, p1, v0

    .line 120
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ap;->c()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ap;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 126
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 131
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ap;->c()I

    move-result v1

    add-int/2addr v0, v1

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    .line 143
    if-nez p2, :cond_0

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->dN:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/model/a/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ap;->a:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/apps/c;->c()Lcom/google/android/gms/plus/apps/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/a;->a(Lcom/google/android/gms/plus/model/a/a;)Lcom/google/android/gms/plus/apps/b;

    move-result-object v2

    sget v1, Lcom/google/android/gms/j;->aH:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v3, v2, Lcom/google/android/gms/plus/apps/b;->a:Ljava/lang/CharSequence;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v1, Lcom/google/android/gms/j;->aG:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v3, v2, Lcom/google/android/gms/plus/apps/b;->b:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->e()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, v2, Lcom/google/android/gms/plus/apps/b;->c:Z

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ap;->a:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v2, p0, v0, v1}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/plus/apps/j;Lcom/google/android/gms/plus/model/a/a;Ljava/lang/String;)V

    .line 160
    :cond_1
    :goto_0
    return-object p2

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    sub-int v0, p1, v0

    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ap;->c()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 149
    if-nez p2, :cond_3

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->dN:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->d:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ap;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/BleDevice;

    sget v1, Lcom/google/android/gms/j;->aH:I

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/BleDevice;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v0, Lcom/google/android/gms/j;->aG:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    sget v1, Lcom/google/android/gms/h;->bh:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 155
    :cond_4
    if-nez p2, :cond_1

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ap;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->dO:I

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 170
    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    return v0
.end method
