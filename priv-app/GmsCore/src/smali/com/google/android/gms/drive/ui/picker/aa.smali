.class final Lcom/google/android/gms/drive/ui/picker/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/picker/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/picker/m;)V
    .locals 0

    .prologue
    .line 765
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/aa;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 765
    check-cast p1, Lcom/google/android/gms/drive/z;

    invoke-interface {p1}, Lcom/google/android/gms/drive/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "PickEntryDialogFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fetch metadata, status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/drive/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/drive/z;->b()Lcom/google/android/gms/drive/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/aa;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/ui/picker/m;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/aa;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v1, v0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/aj;Lcom/google/android/gms/drive/aj;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/aa;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/aj;)V

    goto :goto_0
.end method
