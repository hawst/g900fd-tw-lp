.class public abstract Lcom/google/android/gms/photos/autobackup/a/ag;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/a/af;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 18
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 19
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/af;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/photos/autobackup/a/af;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/google/android/gms/photos/autobackup/a/af;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/google/android/gms/photos/autobackup/a/ah;

    invoke-direct {v0, p0}, Lcom/google/android/gms/photos/autobackup/a/ah;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 201
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 46
    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 53
    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->a(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    .line 54
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 55
    goto :goto_0

    .line 59
    :sswitch_2
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 62
    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->b(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    .line 63
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 64
    goto :goto_0

    .line 68
    :sswitch_3
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 72
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 73
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/photos/autobackup/a/ag;->a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 74
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 75
    goto :goto_0

    .line 79
    :sswitch_4
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 82
    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->c(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    .line 83
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 84
    goto :goto_0

    .line 88
    :sswitch_5
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 93
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/photos/autobackup/a/ag;->b(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 94
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 95
    goto :goto_0

    .line 99
    :sswitch_6
    const-string v2, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v2

    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_0

    .line 106
    sget-object v0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    .line 111
    :cond_0
    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V

    .line 112
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 113
    goto/16 :goto_0

    .line 117
    :sswitch_7
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 119
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 121
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 122
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/photos/autobackup/a/ag;->c(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 123
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 124
    goto/16 :goto_0

    .line 128
    :sswitch_8
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 130
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 131
    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->d(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    .line 132
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 133
    goto/16 :goto_0

    .line 137
    :sswitch_9
    const-string v2, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v2

    .line 141
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_1

    .line 142
    sget-object v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    .line 147
    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->a(Lcom/google/android/gms/photos/autobackup/a/ac;Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;)V

    .line 148
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 149
    goto/16 :goto_0

    .line 153
    :sswitch_a
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 158
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/photos/autobackup/a/ag;->d(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 159
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 160
    goto/16 :goto_0

    .line 164
    :sswitch_b
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 166
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 168
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 169
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/photos/autobackup/a/ag;->e(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;)V

    .line 170
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 171
    goto/16 :goto_0

    .line 175
    :sswitch_c
    const-string v2, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 177
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v2

    .line 179
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 181
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_2

    .line 182
    sget-object v0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;

    .line 187
    :cond_2
    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->a(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/SyncSettings;)V

    .line 188
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 189
    goto/16 :goto_0

    .line 193
    :sswitch_d
    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 195
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/a/ad;->a(Landroid/os/IBinder;)Lcom/google/android/gms/photos/autobackup/a/ac;

    move-result-object v0

    .line 196
    invoke-virtual {p0, v0}, Lcom/google/android/gms/photos/autobackup/a/ag;->e(Lcom/google/android/gms/photos/autobackup/a/ac;)V

    .line 197
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    move v0, v1

    .line 198
    goto/16 :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
