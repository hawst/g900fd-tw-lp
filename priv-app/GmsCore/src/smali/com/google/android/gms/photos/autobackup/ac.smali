.class final Lcom/google/android/gms/photos/autobackup/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/c;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

.field final synthetic b:Lcom/google/android/gms/photos/autobackup/ab;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/ab;Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ac;->b:Lcom/google/android/gms/photos/autobackup/ab;

    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/ac;->a:Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 53
    if-eqz p1, :cond_4

    .line 54
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/photos/autobackup/ac;->b:Lcom/google/android/gms/photos/autobackup/ab;

    iget-object v5, p0, Lcom/google/android/gms/photos/autobackup/ac;->a:Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    const-class v0, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    const-class v1, Lcom/google/android/libraries/social/account/b;

    invoke-static {v5, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/account/b;

    iget-object v6, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v6}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a()Ljava/lang/String;

    move-result-object v6

    iget-object v7, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v7}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->c()Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v7

    const-string v8, "account_name"

    invoke-interface {v7, v8}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/autobackup/s;->b(Landroid/content/Context;I)V

    move v0, v2

    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v1, v6}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v0

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;I)V

    :cond_0
    :goto_1
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iget-object v1, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v0

    iget-object v1, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->g()Z

    move-result v1

    invoke-static {v5, v0, v1}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;IZ)V

    iget-object v0, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->c()Z

    move-result v0

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;Z)V

    iget-object v0, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->f()Z

    move-result v0

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/autobackup/s;->b(Landroid/content/Context;Z)V

    iget-object v0, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->d()Z

    move-result v0

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/autobackup/s;->c(Landroid/content/Context;Z)V

    iget-object v0, v4, Lcom/google/android/gms/photos/autobackup/ab;->b:Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;->e()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    :goto_2
    invoke-static {v5, v0}, Lcom/google/android/libraries/social/autobackup/s;->d(Landroid/content/Context;Z)V

    iget-object v0, v4, Lcom/google/android/gms/photos/autobackup/ab;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->c(I)V

    .line 61
    :goto_3
    return-void

    :cond_1
    move v0, v3

    .line 54
    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/autobackup/o;->d()I

    move-result v0

    invoke-static {v5, v0}, Lcom/google/android/libraries/social/autobackup/s;->b(Landroid/content/Context;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 59
    :catch_0
    move-exception v0

    const-string v0, "SetAutoBackupSettingsOp"

    const-string v1, "Failed to deliver result"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_3
    move v0, v3

    .line 54
    goto :goto_2

    .line 56
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ac;->b:Lcom/google/android/gms/photos/autobackup/ab;

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/ab;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->c(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_0
.end method
