.class final Lcom/google/android/gms/wallet/common/ui/bm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bl;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/bj;

.field final synthetic b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Lcom/google/android/gms/wallet/common/ui/bj;)V
    .locals 0

    .prologue
    .line 82
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/bm;->a:Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 85
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 87
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bj;

    .line 89
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/bm;->a:Lcom/google/android/gms/wallet/common/ui/bj;

    if-ne v0, v4, :cond_0

    .line 90
    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bj;->a(Ljava/util/ArrayList;)V

    .line 87
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 92
    :cond_0
    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bj;->b(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bm;->a:Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/bj;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Ljava/util/List;)V

    .line 102
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 106
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 108
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bj;

    .line 110
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/bm;->a:Lcom/google/android/gms/wallet/common/ui/bj;

    if-ne v0, v4, :cond_0

    .line 111
    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bj;->b(Ljava/util/ArrayList;)V

    .line 108
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 113
    :cond_0
    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/bj;->c(Ljava/util/ArrayList;)V

    goto :goto_1

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->b(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;)Lcom/google/android/gms/wallet/common/ui/bk;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bm;->a:Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/bj;->i()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 122
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bm;->b:Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;Ljava/util/List;)V

    .line 123
    return-void
.end method
