.class public final Lcom/google/android/gms/plus/sharebox/AddToCircleData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/sharebox/g;


# instance fields
.field private final a:I

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/gms/common/people/data/Audience;

.field private e:Lcom/google/android/gms/common/people/data/Audience;

.field private f:Ljava/util/ArrayList;

.field private g:Ljava/lang/Integer;

.field private h:Ljava/lang/Boolean;

.field private i:Ljava/lang/Boolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/plus/sharebox/g;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->CREATOR:Lcom/google/android/gms/plus/sharebox/g;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V
    .locals 0

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    iput p1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a:I

    .line 84
    iput-object p2, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->b:Ljava/lang/String;

    .line 85
    iput-object p3, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->c:Ljava/lang/String;

    .line 86
    iput-object p4, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    .line 87
    iput-object p5, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    .line 88
    iput-object p6, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    .line 89
    iput-object p7, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    .line 90
    iput-object p8, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h:Ljava/lang/Boolean;

    .line 91
    iput-object p9, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i:Ljava/lang/Boolean;

    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 57
    const/4 v1, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    move-object v9, v4

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;Ljava/util/ArrayList;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/Boolean;)V

    .line 66
    return-void
.end method

.method private a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 280
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 281
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_1

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/Circle;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 287
    :goto_1
    return v0

    .line 281
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 287
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private n()Z
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 9

    .prologue
    const/4 v0, -0x1

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 109
    if-nez p1, :cond_0

    .line 110
    const-string v0, "ShareBox"

    const-string v1, "Cannot calculate add to circle state unless audience is set"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iput-object v3, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    .line 112
    iput-object v3, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h:Ljava/lang/Boolean;

    .line 113
    iput-object v3, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i:Ljava/lang/Boolean;

    .line 134
    :goto_0
    return-void

    .line 116
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, p2}, Lcom/google/android/gms/plus/sharebox/j;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_13

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    if-nez v1, :cond_4

    :cond_1
    :goto_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    .line 125
    sget-object v0, Lcom/google/android/gms/plus/c/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->n()Z

    move-result v0

    if-eqz v0, :cond_12

    move v0, v5

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h:Ljava/lang/Boolean;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gez v0, :cond_3

    :cond_2
    move v2, v5

    :cond_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i:Ljava/lang/Boolean;

    goto :goto_0

    .line 123
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    goto :goto_1

    :cond_5
    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_6
    move v0, v2

    :goto_3
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->c:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    if-nez v0, :cond_e

    move v1, v2

    :goto_4
    move v6, v2

    move v4, v2

    :goto_5
    if-ge v6, v1, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->h()Z

    move-result v7

    if-eqz v7, :cond_14

    add-int/lit8 v4, v4, 0x1

    if-nez v3, :cond_14

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    move v3, v4

    :goto_6
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    move-object v3, v0

    goto :goto_5

    :cond_8
    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v1, v2

    :goto_7
    if-ge v1, v6, :cond_d

    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v7

    if-nez v7, :cond_a

    :cond_9
    move v0, v2

    :goto_8
    if-nez v0, :cond_c

    move v0, v2

    goto :goto_3

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_b

    const-string v7, "objectType"

    invoke-virtual {v0, v7}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_b

    const-string v7, "page"

    const-string v8, "objectType"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    move v0, v5

    goto :goto_8

    :cond_b
    move v0, v2

    goto :goto_8

    :cond_c
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    :cond_d
    move v0, v5

    goto :goto_3

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v1, v0

    goto :goto_4

    :cond_f
    if-ne v4, v5, :cond_10

    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_1

    :cond_10
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->n()Z

    move-result v0

    if-nez v0, :cond_11

    move v0, v2

    goto/16 :goto_1

    :cond_11
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->b:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_1

    :cond_12
    move v0, v2

    .line 125
    goto/16 :goto_2

    .line 131
    :cond_13
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    .line 132
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h:Ljava/lang/Boolean;

    .line 133
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    :cond_14
    move-object v0, v3

    move v3, v4

    goto/16 :goto_6
.end method

.method public final a(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 265
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    .line 266
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 345
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final g()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 316
    sget-object v0, Lcom/google/android/gms/plus/c/a;->V:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/j;->b(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 329
    new-instance v0, Ljava/lang/StringBuffer;

    const-class v2, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v2, "<circles="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, " audience="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v2, " uncircledPeople="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    if-nez v2, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->i()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " checked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, " hidden="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->l()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Z)Ljava/lang/StringBuffer;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " peopleToAdd="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/j;->b(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->d:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->e:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    goto :goto_2
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 350
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/sharebox/g;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Landroid/os/Parcel;I)V

    .line 351
    return-void
.end method
