.class Lcom/google/android/gms/audiomodem/TokenEncoder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Z

.field private final b:Lcom/google/android/gms/audiomodem/Encoding;

.field private c:[B

.field private d:Lcom/google/whispernet/Data$EncodeResults;

.field private e:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/audiomodem/Encoding;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->b:Lcom/google/android/gms/audiomodem/Encoding;

    .line 27
    return-void
.end method

.method private static native nativeEncode([BLcom/google/whispernet/Data$EncodeResults;I)V
.end method

.method private static native nativeInitializeDsss(IZZIIFIFIIIZ)V
.end method

.method private static native nativeInitializeDtmf(IZFIFI)V
.end method

.method private static native nativeRelease()V
.end method


# virtual methods
.method public final a([B)Lcom/google/whispernet/Data$EncodeResults;
    .locals 14

    .prologue
    const/4 v12, 0x1

    const/4 v0, 0x0

    .line 37
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->c:[B

    invoke-static {p1, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->d:Lcom/google/whispernet/Data$EncodeResults;

    .line 47
    :goto_0
    return-object v0

    .line 40
    :cond_0
    invoke-static {}, Lcom/google/android/gms/audiomodem/av;->a()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    :goto_1
    if-nez v0, :cond_5

    .line 41
    const/4 v0, 0x0

    goto :goto_0

    .line 40
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->a:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/Encoding;->b()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v1, "audioModem"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "TokenEncoder: Received an Encoder with unknown type"

    const-string v2, "audioModem"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/Encoding;->c()Lcom/google/android/gms/audiomodem/DsssEncoding;

    move-result-object v13

    const-string v0, "audioModem"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TokenEncoder: Initializing DSSS encoder with shouldUseOdp: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/audiomodem/m;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->n()I

    move-result v0

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->c()Z

    move-result v1

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->d()Z

    move-result v2

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->e()I

    move-result v3

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->f()I

    move-result v4

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->g()F

    move-result v5

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->h()I

    move-result v6

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->i()F

    move-result v7

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->j()I

    move-result v8

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->k()I

    move-result v9

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->l()I

    move-result v10

    sget-object v11, Lcom/google/android/gms/audiomodem/m;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v11}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Boolean;

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v11

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/audiomodem/TokenEncoder;->nativeInitializeDsss(IZZIIFIFIIIZ)V

    invoke-virtual {v13}, Lcom/google/android/gms/audiomodem/DsssEncoding;->m()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->e:I

    :goto_2
    iput-boolean v12, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->a:Z

    :cond_4
    move v0, v12

    goto/16 :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/Encoding;->d()Lcom/google/android/gms/audiomodem/DtmfEncoding;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->i()I

    move-result v0

    invoke-virtual {v6}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->c()Z

    move-result v1

    invoke-virtual {v6}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->d()F

    move-result v2

    invoke-virtual {v6}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->e()I

    move-result v3

    invoke-virtual {v6}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->f()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v4, v5

    invoke-virtual {v6}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->g()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/audiomodem/TokenEncoder;->nativeInitializeDtmf(IZFIFI)V

    invoke-virtual {v6}, Lcom/google/android/gms/audiomodem/DtmfEncoding;->h()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->e:I

    goto :goto_2

    .line 43
    :cond_5
    new-instance v0, Lcom/google/whispernet/Data$EncodeResults;

    invoke-direct {v0}, Lcom/google/whispernet/Data$EncodeResults;-><init>()V

    .line 44
    iget v1, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->e:I

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/audiomodem/TokenEncoder;->nativeEncode([BLcom/google/whispernet/Data$EncodeResults;I)V

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->c:[B

    .line 46
    iput-object v0, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->d:Lcom/google/whispernet/Data$EncodeResults;

    goto/16 :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 100
    iget-boolean v0, p0, Lcom/google/android/gms/audiomodem/TokenEncoder;->a:Z

    if-eqz v0, :cond_0

    .line 101
    invoke-static {}, Lcom/google/android/gms/audiomodem/TokenEncoder;->nativeRelease()V

    .line 103
    :cond_0
    return-void
.end method
