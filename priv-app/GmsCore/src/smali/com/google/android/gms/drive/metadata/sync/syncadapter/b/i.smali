.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;


# instance fields
.field private final a:Ljava/util/Collection;


# direct methods
.method private constructor <init>(Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;->a:Ljava/util/Collection;

    .line 27
    return-void
.end method

.method public synthetic constructor <init>(Ljava/util/Collection;B)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;-><init>(Ljava/util/Collection;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/SyncResult;Z)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;

    .line 39
    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;->a(Landroid/content/SyncResult;Z)V

    goto :goto_0

    .line 41
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;

    .line 32
    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/f;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/f;Landroid/content/SyncResult;)V

    goto :goto_0

    .line 34
    :cond_0
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 45
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "CompositeSyncAlgorithm[delegates=%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/b/i;->a:Ljava/util/Collection;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
