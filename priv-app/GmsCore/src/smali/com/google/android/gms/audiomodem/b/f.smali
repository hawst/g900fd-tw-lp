.class final Lcom/google/android/gms/audiomodem/b/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/audiomodem/a/k;

.field final synthetic b:Lcom/google/android/gms/audiomodem/Snoop$Params;

.field final synthetic c:Lcom/google/android/gms/audiomodem/a/e;

.field final synthetic d:Lcom/google/android/gms/audiomodem/b/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/audiomodem/b/a;Lcom/google/android/gms/audiomodem/a/k;Lcom/google/android/gms/audiomodem/Snoop$Params;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/b/f;->d:Lcom/google/android/gms/audiomodem/b/a;

    iput-object p2, p0, Lcom/google/android/gms/audiomodem/b/f;->a:Lcom/google/android/gms/audiomodem/a/k;

    iput-object p3, p0, Lcom/google/android/gms/audiomodem/b/f;->b:Lcom/google/android/gms/audiomodem/Snoop$Params;

    iput-object p4, p0, Lcom/google/android/gms/audiomodem/b/f;->c:Lcom/google/android/gms/audiomodem/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/f;->d:Lcom/google/android/gms/audiomodem/b/a;

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/b/a;->a(Lcom/google/android/gms/audiomodem/b/a;)Lcom/google/android/gms/audiomodem/n;

    move-result-object v7

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/b/f;->a:Lcom/google/android/gms/audiomodem/a/k;

    iget-object v8, p0, Lcom/google/android/gms/audiomodem/b/f;->b:Lcom/google/android/gms/audiomodem/Snoop$Params;

    iget-object v9, p0, Lcom/google/android/gms/audiomodem/b/f;->c:Lcom/google/android/gms/audiomodem/a/e;

    invoke-virtual {v7}, Lcom/google/android/gms/audiomodem/n;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x1964

    invoke-static {v9, v0}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    .line 112
    :goto_0
    return-void

    .line 111
    :cond_0
    new-instance v0, Lcom/google/android/gms/audiomodem/r;

    invoke-virtual {v8}, Lcom/google/android/gms/audiomodem/Snoop$Params;->c()Z

    move-result v2

    invoke-virtual {v8}, Lcom/google/android/gms/audiomodem/Snoop$Params;->d()Z

    move-result v3

    invoke-virtual {v8}, Lcom/google/android/gms/audiomodem/Snoop$Params;->e()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/audiomodem/r;-><init>(Lcom/google/android/gms/audiomodem/a/k;ZZJ)V

    invoke-virtual {v8}, Lcom/google/android/gms/audiomodem/Snoop$Params;->b()[Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v2

    array-length v3, v2

    move v1, v6

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    new-instance v5, Lcom/google/android/gms/audiomodem/bj;

    invoke-direct {v5, v0, v4}, Lcom/google/android/gms/audiomodem/bj;-><init>(Lcom/google/android/gms/audiomodem/r;Lcom/google/android/gms/audiomodem/Encoding;)V

    iget-object v4, v7, Lcom/google/android/gms/audiomodem/n;->b:Lcom/google/android/gms/audiomodem/bh;

    iget-object v8, v5, Lcom/google/android/gms/audiomodem/bj;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-virtual {v4, v8}, Lcom/google/android/gms/audiomodem/bh;->a(Lcom/google/android/gms/audiomodem/Encoding;)Lcom/google/android/gms/audiomodem/TokenDecoder;

    move-result-object v4

    iget-object v5, v5, Lcom/google/android/gms/audiomodem/bj;->a:Lcom/google/android/gms/audiomodem/r;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/audiomodem/TokenDecoder;->a(Lcom/google/android/gms/audiomodem/r;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-static {v9, v6}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    goto :goto_0
.end method
