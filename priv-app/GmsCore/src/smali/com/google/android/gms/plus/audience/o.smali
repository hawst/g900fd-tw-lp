.class public Lcom/google/android/gms/plus/audience/o;
.super Lcom/google/android/gms/common/ui/widget/h;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/audience/ap;
.implements Lcom/google/android/gms/plus/audience/bh;


# instance fields
.field private A:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

.field private B:Ljava/util/List;

.field private C:I

.field private D:Ljava/util/List;

.field private E:Z

.field private final F:Lcom/google/android/gms/plus/audience/y;

.field private final G:Lcom/google/android/gms/common/ui/widget/f;

.field private H:Landroid/content/Context;

.field private I:Lcom/google/android/gms/common/ui/widget/c;

.field private J:Lcom/google/android/gms/common/ui/widget/c;

.field private K:Lcom/google/android/gms/common/ui/widget/c;

.field private L:I

.field private M:I

.field private N:I

.field private final O:Lcom/google/android/gms/common/api/v;

.field private final P:Ljava/util/Map;

.field private final Q:Lcom/google/android/gms/common/internal/a/a;

.field private e:Landroid/view/View$OnClickListener;

.field private final f:Lcom/google/android/gms/plus/internal/ad;

.field protected final g:I

.field protected final h:I

.field protected final i:I

.field protected final j:I

.field protected final k:I

.field protected final l:I

.field public final m:Ljava/lang/String;

.field public final n:Ljava/lang/String;

.field o:Lcom/google/android/gms/common/ui/widget/c;

.field p:Lcom/google/android/gms/common/ui/widget/c;

.field q:Lcom/google/android/gms/common/ui/widget/c;

.field private final r:Lcom/google/android/gms/plus/audience/bg;

.field private final s:Z

.field private final t:I

.field private final u:I

.field private final v:I

.field private w:Z

.field private x:Lcom/google/android/gms/common/data/d;

.field private final y:Ljava/util/List;

.field private z:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 11

    .prologue
    .line 186
    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    sget-object v10, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/audience/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;ZLcom/google/android/gms/plus/internal/ad;)V

    .line 189
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;Z)V
    .locals 11

    .prologue
    .line 173
    sget-object v10, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/audience/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;ZLcom/google/android/gms/plus/internal/ad;)V

    .line 176
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/audience/bg;Ljava/lang/String;Ljava/lang/String;ZIILjava/util/List;ZLcom/google/android/gms/plus/internal/ad;)V
    .locals 3

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/h;-><init>(Landroid/content/Context;)V

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->g:I

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->h:I

    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->i:I

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->j:I

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->k:I

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->l:I

    .line 142
    new-instance v0, Lcom/google/android/gms/plus/audience/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/audience/y;-><init>(Lcom/google/android/gms/plus/audience/o;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->F:Lcom/google/android/gms/plus/audience/y;

    .line 143
    new-instance v0, Lcom/google/android/gms/common/ui/widget/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/ui/widget/f;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->G:Lcom/google/android/gms/common/ui/widget/f;

    .line 158
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->P:Ljava/util/Map;

    .line 160
    new-instance v0, Lcom/google/android/gms/common/internal/a/a;

    const/high16 v1, 0x500000

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/a/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->Q:Lcom/google/android/gms/common/internal/a/a;

    .line 203
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/o;->H:Landroid/content/Context;

    .line 204
    iput-object p2, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    .line 205
    iput-object p3, p0, Lcom/google/android/gms/plus/audience/o;->m:Ljava/lang/String;

    .line 206
    iput-object p4, p0, Lcom/google/android/gms/plus/audience/o;->n:Ljava/lang/String;

    .line 207
    iput-boolean p5, p0, Lcom/google/android/gms/plus/audience/o;->s:Z

    .line 208
    iput p6, p0, Lcom/google/android/gms/plus/audience/o;->u:I

    .line 209
    iput p7, p0, Lcom/google/android/gms/plus/audience/o;->v:I

    .line 210
    iput-object p10, p0, Lcom/google/android/gms/plus/audience/o;->f:Lcom/google/android/gms/plus/internal/ad;

    .line 211
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    .line 212
    iput-object p8, p0, Lcom/google/android/gms/plus/audience/o;->D:Ljava/util/List;

    .line 213
    iput-boolean p9, p0, Lcom/google/android/gms/plus/audience/o;->E:Z

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->f:Lcom/google/android/gms/plus/internal/ad;

    const/16 v1, 0x50

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->n:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->O:Lcom/google/android/gms/common/api/v;

    .line 219
    new-instance v0, Lcom/google/android/gms/common/ui/widget/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/ui/widget/f;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->J:Lcom/google/android/gms/common/ui/widget/c;

    .line 221
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/k;->y:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->t:I

    .line 225
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/o;->a(Ljava/lang/Object;)V

    .line 226
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/o;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/a;->a:Landroid/content/Context;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 497
    .line 498
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 499
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->g()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->ae_()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v2, v3, v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    .line 512
    :cond_0
    :goto_1
    if-eqz v1, :cond_2

    .line 513
    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "contactType"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 516
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 517
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->l()Lcom/google/android/gms/plus/service/v1whitelisted/models/es;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/es;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 518
    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "isCircled"

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->l()Lcom/google/android/gms/plus/service/v1whitelisted/models/es;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/es;->d()Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 523
    :cond_1
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 524
    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "objectType"

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->j()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    :cond_2
    return-object v1

    :cond_3
    move-object v0, v1

    .line 499
    goto :goto_0

    .line 501
    :cond_4
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->e()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/eo;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/eo;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->e()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/eo;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/eo;->d()Ljava/lang/String;

    move-result-object v0

    .line 504
    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->ae_()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ep;->d()Ljava/lang/String;

    move-result-object v1

    :cond_5
    invoke-static {v2, v3, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    .line 507
    invoke-interface {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 509
    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "secondaryText"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method private a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;Landroid/database/Cursor;)Lcom/google/android/gms/common/ui/widget/c;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x2

    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    if-nez v0, :cond_2

    .line 580
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    .line 586
    :goto_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 587
    if-eqz p2, :cond_3

    .line 588
    const/4 v0, -0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 589
    :cond_0
    :goto_1
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/plus/audience/o;->C:I

    if-ge v0, v1, :cond_3

    .line 591
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 592
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 593
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 594
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    invoke-interface {p2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v8, v9, v4}, Landroid/provider/ContactsContract$Contacts;->getLookupUri(JLjava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 597
    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v7

    .line 599
    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v8

    const-string v9, "contactsAvatarUri"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v9, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 603
    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v4

    const-string v8, "secondaryText"

    invoke-virtual {v4, v8, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    :cond_1
    invoke-virtual {v7}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "contactType"

    invoke-virtual {v0, v4, v10}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 608
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 582
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0

    .line 615
    :cond_3
    if-eqz p1, :cond_6

    .line 616
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v4

    move v1, v6

    .line 618
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 619
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;

    .line 621
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 622
    invoke-static {v0}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    .line 624
    if-eqz v0, :cond_5

    iget-object v7, p0, Lcom/google/android/gms/plus/audience/o;->D:Ljava/util/List;

    if-eqz v7, :cond_4

    iget-object v7, p0, Lcom/google/android/gms/plus/audience/o;->D:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 627
    :cond_4
    iget-object v7, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 618
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 634
    :cond_6
    iput v6, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    .line 635
    iput v6, p0, Lcom/google/android/gms/plus/audience/o;->M:I

    .line 636
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_8

    .line 639
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->u:I

    if-lez v0, :cond_9

    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->t:I

    if-lez v0, :cond_9

    .line 642
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->u:I

    iget v1, p0, Lcom/google/android/gms/plus/audience/o;->t:I

    div-int/2addr v0, v1

    invoke-static {v10, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 643
    iget v1, p0, Lcom/google/android/gms/plus/audience/o;->u:I

    iget v3, p0, Lcom/google/android/gms/plus/audience/o;->t:I

    mul-int/2addr v0, v3

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    .line 645
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    .line 646
    new-instance v0, Lcom/google/android/gms/plus/audience/ab;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    iget v3, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    invoke-direct {v0, p0, v1, v3}, Lcom/google/android/gms/plus/audience/ab;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;I)V

    move-object v7, v0

    .line 649
    :goto_3
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->v:I

    if-lez v0, :cond_7

    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    .line 652
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->v:I

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    sub-int/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->M:I

    .line 654
    new-instance v0, Lcom/google/android/gms/plus/audience/t;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    iget v3, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    iget v1, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    iget v4, p0, Lcom/google/android/gms/plus/audience/o;->M:I

    add-int/2addr v4, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/t;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;III)V

    move-object v2, v0

    .line 658
    :cond_7
    new-instance v0, Lcom/google/android/gms/plus/audience/s;

    sget v1, Lcom/google/android/gms/p;->sS:I

    const-string v3, "\u2605"

    new-instance v4, Lcom/google/android/gms/common/ui/widget/b;

    new-array v5, v5, [Lcom/google/android/gms/common/ui/widget/c;

    aput-object v7, v5, v6

    aput-object v2, v5, v10

    invoke-direct {v4, p0, v5}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-direct {v0, p0, v1, v3, v4}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;ILjava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    move-object v2, v0

    .line 665
    :cond_8
    return-object v2

    :cond_9
    move-object v7, v2

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Landroid/graphics/Bitmap;Z)V
    .locals 0

    .prologue
    .line 60
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/audience/o;->b(Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/model/d;Lcom/google/android/gms/people/model/d;)Z
    .locals 2

    .prologue
    .line 60
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-eqz p0, :cond_1

    if-nez p1, :cond_2

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/common/internal/a/a;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->Q:Lcom/google/android/gms/common/internal/a/a;

    return-object v0
.end method

.method private static b(Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Landroid/graphics/Bitmap;Z)V
    .locals 0

    .prologue
    .line 934
    if-eqz p2, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/common/util/ac;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Landroid/graphics/Bitmap;)V

    .line 935
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/plus/audience/o;)Z
    .locals 1

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/o;->E:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/audience/o;)Lcom/google/android/gms/plus/audience/bg;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/audience/o;)I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->t:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/audience/o;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/a;->b:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/audience/o;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/a;->b:Landroid/view/LayoutInflater;

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 938
    sget v0, Lcom/google/android/gms/l;->dv:I

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/audience/o;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v1

    .line 939
    sget v0, Lcom/google/android/gms/j;->td:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 940
    sget v0, Lcom/google/android/gms/j;->td:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 943
    :cond_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    .line 944
    invoke-virtual {v1, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 945
    return-object v1

    .line 940
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method protected a(Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/ui/widget/c;
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 451
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 452
    const/4 v3, 0x0

    .line 455
    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v6

    .line 456
    iput v6, p0, Lcom/google/android/gms/plus/audience/o;->N:I

    move v1, v4

    move v2, v4

    .line 457
    :goto_0
    if-ge v1, v6, :cond_1

    .line 458
    invoke-interface {p1, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/j;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/j;->b()Ljava/lang/String;

    move-result-object v0

    .line 459
    if-eqz v0, :cond_3

    .line 460
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    const/4 v7, 0x1

    invoke-virtual {v0, v4, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 461
    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 462
    new-instance v7, Lcom/google/android/gms/plus/audience/s;

    new-instance v8, Lcom/google/android/gms/plus/audience/z;

    invoke-direct {v8, p0, p1, v2, v1}, Lcom/google/android/gms/plus/audience/z;-><init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;II)V

    invoke-direct {v7, p0, v3, v8}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 464
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 457
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move-object v3, v2

    move v2, v0

    goto :goto_0

    .line 460
    :cond_0
    const-string v0, ""

    goto :goto_1

    .line 470
    :cond_1
    if-eqz v3, :cond_2

    .line 471
    new-instance v0, Lcom/google/android/gms/plus/audience/s;

    new-instance v1, Lcom/google/android/gms/plus/audience/z;

    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v4

    invoke-direct {v1, p0, p1, v2, v4}, Lcom/google/android/gms/plus/audience/z;-><init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;II)V

    invoke-direct {v0, p0, v3, v1}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 476
    :cond_2
    new-instance v1, Lcom/google/android/gms/common/ui/widget/b;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/common/ui/widget/c;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/common/ui/widget/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    return-object v1

    :cond_3
    move v0, v2

    move-object v2, v3

    goto :goto_2
.end method

.method protected a(Lcom/google/android/gms/people/model/d;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 3

    .prologue
    .line 758
    sget v0, Lcom/google/android/gms/l;->dr:I

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/plus/audience/o;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    .line 761
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a()V

    .line 762
    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Ljava/lang/Object;)V

    .line 763
    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Ljava/lang/String;)V

    .line 764
    invoke-virtual {v0, p4}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Z)V

    .line 766
    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->g()I

    move-result v1

    .line 767
    if-gez v1, :cond_0

    .line 768
    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b()V

    .line 773
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/common/people/data/e;->a(Lcom/google/android/gms/people/model/d;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->setChecked(Z)V

    .line 775
    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(Lcom/google/android/gms/plus/audience/ap;)V

    .line 777
    return-object v0

    .line 770
    :cond_0
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->a(I)V

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;Landroid/view/View;)Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;
    .locals 1

    .prologue
    .line 717
    sget v0, Lcom/google/android/gms/l;->du:I

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/plus/audience/o;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;

    .line 720
    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListHeaderView;->a(Ljava/lang/String;)V

    .line 722
    return-object v0
.end method

.method protected a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZILandroid/view/View;ZZZ)Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;
    .locals 14

    .prologue
    .line 832
    move-object/from16 v0, p9

    move/from16 v1, p8

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/o;->a(Landroid/view/View;I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;

    .line 833
    if-nez p11, :cond_2

    const/4 v7, 0x1

    .line 835
    :goto_0
    invoke-virtual {v6}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a()V

    .line 836
    invoke-virtual {v6, p1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Ljava/lang/Object;)V

    .line 837
    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Ljava/lang/String;)V

    .line 838
    move/from16 v0, p10

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Z)V

    .line 839
    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b(Z)V

    .line 840
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v6, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->e(Z)V

    .line 841
    move-object/from16 v0, p3

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->b(Ljava/lang/String;)V

    .line 844
    move/from16 v0, p7

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->setChecked(Z)V

    .line 845
    move/from16 v0, p12

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->c(Z)V

    .line 846
    invoke-virtual {v6, p0}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Lcom/google/android/gms/plus/audience/ap;)V

    .line 848
    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/people/internal/at;->i(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 849
    const/4 v8, 0x0

    .line 850
    invoke-static/range {p6 .. p6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 851
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->Q:Lcom/google/android/gms/common/internal/a/a;

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/internal/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Landroid/graphics/Bitmap;

    .line 852
    if-nez v8, :cond_0

    .line 853
    new-instance v2, Lcom/google/android/gms/plus/audience/p;

    move-object v3, p0

    move-object/from16 v4, p6

    move-object v5, p1

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/plus/audience/p;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/lang/String;Ljava/lang/Object;Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Z)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/plus/audience/p;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 883
    :cond_0
    if-eqz v8, :cond_4

    .line 884
    invoke-static {v6, v8, v7}, Lcom/google/android/gms/plus/audience/o;->b(Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Landroid/graphics/Bitmap;Z)V

    .line 928
    :cond_1
    :goto_2
    return-object v6

    .line 833
    :cond_2
    const/4 v7, 0x0

    goto :goto_0

    .line 840
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 885
    :cond_4
    if-eqz p11, :cond_5

    .line 886
    sget v2, Lcom/google/android/gms/h;->Q:I

    invoke-virtual {v6, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(I)V

    goto :goto_2

    .line 888
    :cond_5
    sget v2, Lcom/google/android/gms/h;->G:I

    invoke-virtual {v6, v2}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(I)V

    goto :goto_2

    .line 890
    :cond_6
    if-eqz p5, :cond_1

    .line 892
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->Q:Lcom/google/android/gms/common/internal/a/a;

    move-object/from16 v0, p5

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/internal/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 893
    if-eqz v2, :cond_7

    .line 894
    invoke-static {v6, v2, v7}, Lcom/google/android/gms/plus/audience/o;->b(Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Landroid/graphics/Bitmap;Z)V

    goto :goto_2

    .line 896
    :cond_7
    sget-object v2, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v3, p0, Lcom/google/android/gms/plus/audience/o;->O:Lcom/google/android/gms/common/api/v;

    const/4 v4, 0x2

    const/4 v5, 0x1

    move-object/from16 v0, p5

    invoke-interface {v2, v3, v0, v4, v5}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v2

    .line 900
    new-instance v3, Lcom/google/android/gms/plus/audience/q;

    invoke-direct {v3, p0, v2}, Lcom/google/android/gms/plus/audience/q;-><init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/api/am;)V

    invoke-virtual {v6, v3}, Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;->a(Lcom/google/android/gms/plus/audience/aq;)V

    .line 907
    new-instance v8, Lcom/google/android/gms/plus/audience/r;

    move-object v9, p0

    move-object v10, p1

    move-object v11, v6

    move-object/from16 v12, p5

    move v13, v7

    invoke-direct/range {v8 .. v13}, Lcom/google/android/gms/plus/audience/r;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/lang/Object;Lcom/google/android/gms/plus/audience/AudienceSelectionListPersonView;Ljava/lang/String;Z)V

    invoke-interface {v2, v8}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/gms/people/model/d;Landroid/view/View;I)Lcom/google/android/gms/plus/audience/ar;
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v2, 0x0

    .line 783
    if-eqz p2, :cond_2

    .line 784
    check-cast p2, Lcom/google/android/gms/plus/audience/ar;

    .line 789
    :goto_0
    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->H:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bx:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    move v4, v0

    .line 793
    :goto_1
    const/4 v0, 0x1

    if-ne p3, v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->H:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->by:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    move v3, v0

    .line 795
    :goto_2
    const/4 v0, 0x2

    if-ne p3, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->H:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->by:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    move v1, v0

    .line 797
    :goto_3
    new-instance v5, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v5, v6, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 799
    sget v0, Lcom/google/android/gms/j;->bh:I

    invoke-virtual {p2, v0}, Lcom/google/android/gms/plus/audience/ar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    .line 801
    invoke-virtual {v0, v5}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 802
    invoke-virtual {v0, v2, v3, v2, v1}, Landroid/widget/RelativeLayout;->setPadding(IIII)V

    .line 804
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-ge v0, v1, :cond_0

    .line 805
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 808
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 809
    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    .line 810
    sget v1, Lcom/google/android/gms/j;->aV:I

    invoke-virtual {p2, v1}, Lcom/google/android/gms/plus/audience/ar;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 813
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/plus/audience/ar;->a()V

    .line 814
    invoke-virtual {p2, p1}, Lcom/google/android/gms/plus/audience/ar;->a(Ljava/lang/Object;)V

    .line 815
    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->g()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/plus/audience/ar;->a(Ljava/lang/String;I)V

    .line 817
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/common/people/data/e;->a(Lcom/google/android/gms/people/model/d;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/gms/plus/audience/ar;->setChecked(Z)V

    .line 819
    invoke-virtual {p2, p0}, Lcom/google/android/gms/plus/audience/ar;->a(Lcom/google/android/gms/plus/audience/ap;)V

    .line 821
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->e:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    .line 822
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/plus/audience/ar;->a(Landroid/view/View$OnClickListener;)V

    .line 825
    :cond_1
    return-object p2

    .line 786
    :cond_2
    new-instance p2, Lcom/google/android/gms/plus/audience/ar;

    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/a;->a:Landroid/content/Context;

    invoke-direct {p2, v0, v2}, Lcom/google/android/gms/plus/audience/ar;-><init>(Landroid/content/Context;B)V

    goto/16 :goto_0

    .line 789
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->H:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->bw:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    move v4, v0

    goto/16 :goto_1

    :cond_4
    move v3, v2

    .line 793
    goto/16 :goto_2

    :cond_5
    move v1, v2

    .line 795
    goto :goto_3
.end method

.method public final a(Landroid/database/Cursor;I)V
    .locals 2

    .prologue
    .line 383
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/o;->z:Landroid/database/Cursor;

    .line 384
    iput p2, p0, Lcom/google/android/gms/plus/audience/o;->C:I

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->A:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->z:Landroid/database/Cursor;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;Landroid/database/Cursor;)Lcom/google/android/gms/common/ui/widget/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->K:Lcom/google/android/gms/common/ui/widget/c;

    .line 386
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    .line 387
    return-void
.end method

.method public final a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 314
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/o;->e:Landroid/view/View$OnClickListener;

    .line 315
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/audience/ao;Z)V
    .locals 7

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 671
    invoke-virtual {p1}, Lcom/google/android/gms/plus/audience/ao;->g()Ljava/lang/Object;

    move-result-object v0

    .line 672
    instance-of v3, v0, Lcom/google/android/gms/people/model/j;

    if-eqz v3, :cond_3

    .line 673
    check-cast v0, Lcom/google/android/gms/people/model/j;

    .line 674
    invoke-static {v0}, Lcom/google/android/gms/common/people/data/e;->a(Lcom/google/android/gms/people/model/j;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v4

    .line 675
    invoke-virtual {p1}, Lcom/google/android/gms/plus/audience/ao;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 676
    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "selectionSource"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 681
    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v2

    const-string v0, "contactType"

    move v6, v1

    move-object v1, v0

    move v0, v6

    .line 690
    :goto_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v0, v4

    .line 706
    :goto_1
    if-eqz p2, :cond_5

    .line 707
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/people/data/g;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    .line 712
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    invoke-virtual {v1, v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 713
    :cond_0
    return-void

    .line 685
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v5, "selectionSource"

    invoke-virtual {v3, v5, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 690
    invoke-virtual {v4}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v3

    const-string v1, "contactType"

    invoke-interface {v0}, Lcom/google/android/gms/people/model/j;->d()[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_2

    const/4 v0, 0x2

    move-object v2, v3

    goto :goto_0

    :cond_2
    move v0, v2

    move-object v2, v3

    goto :goto_0

    .line 696
    :cond_3
    instance-of v1, v0, Lcom/google/android/gms/people/model/d;

    if-eqz v1, :cond_4

    .line 697
    check-cast v0, Lcom/google/android/gms/people/model/d;

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/e;->a(Lcom/google/android/gms/people/model/d;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_1

    .line 698
    :cond_4
    instance-of v1, v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    if-eqz v1, :cond_0

    .line 699
    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    goto :goto_1

    .line 709
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, v1, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/people/data/g;->b(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 365
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->N:I

    .line 366
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/o;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    .line 367
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 281
    if-eq p1, p0, :cond_4

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    if-nez v0, :cond_1

    .line 283
    :cond_0
    const-string v0, "AudienceSelectionAdapter"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Couldn\'t build audience blocks, unexpected selectionState = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :goto_0
    return-void

    .line 287
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    .line 289
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 290
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->P:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 292
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/o;->w:Z

    .line 293
    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->P:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/e;->a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/people/model/j;

    move-result-object v0

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 297
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    goto :goto_0

    .line 299
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method protected final b(Lcom/google/android/gms/people/model/d;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;
    .locals 2

    .prologue
    .line 732
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/people/model/d;Landroid/view/View;Landroid/view/ViewGroup;Z)Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;

    move-result-object v0

    .line 735
    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->e()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 753
    :goto_0
    return-object v0

    .line 737
    :pswitch_0
    sget v1, Lcom/google/android/gms/h;->cp:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    .line 741
    :pswitch_1
    sget v1, Lcom/google/android/gms/h;->bf:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    .line 745
    :pswitch_2
    sget v1, Lcom/google/android/gms/h;->cn:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    .line 749
    :pswitch_3
    sget v1, Lcom/google/android/gms/h;->co:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionListCircleView;->b(I)V

    goto :goto_0

    .line 735
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public b(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 321
    if-nez p1, :cond_0

    .line 322
    new-instance v0, Lcom/google/android/gms/common/ui/widget/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/ui/widget/f;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->p:Lcom/google/android/gms/common/ui/widget/c;

    .line 326
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    .line 327
    return-void

    .line 324
    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/audience/u;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/audience/u;-><init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->p:Lcom/google/android/gms/common/ui/widget/c;

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 13

    .prologue
    const/4 v7, 0x1

    const/4 v5, 0x0

    .line 373
    iput-boolean v7, p0, Lcom/google/android/gms/plus/audience/o;->w:Z

    .line 374
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v5

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/em;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/em;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/audience/o;->N:I

    .line 375
    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;->d()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    move v6, v7

    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    move v4, v5

    move v3, v5

    :goto_2
    if-ge v4, v11, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v0, v5, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v9, v0

    :goto_3
    invoke-virtual {v9, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    new-instance v12, Lcom/google/android/gms/plus/audience/s;

    new-instance v0, Lcom/google/android/gms/plus/audience/t;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/t;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;III)V

    invoke-direct {v12, p0, v8, v0}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-virtual {v10, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v9}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    move v3, v4

    :goto_4
    add-int/lit8 v4, v4, 0x1

    move-object v8, v0

    goto :goto_2

    :cond_2
    move v6, v5

    goto :goto_1

    :cond_3
    const-string v0, ""

    move-object v9, v0

    goto :goto_3

    :cond_4
    if-eqz v8, :cond_5

    new-instance v9, Lcom/google/android/gms/plus/audience/s;

    new-instance v0, Lcom/google/android/gms/plus/audience/t;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/audience/t;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/List;III)V

    invoke-direct {v9, p0, v8, v0}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    new-instance v1, Lcom/google/android/gms/common/ui/widget/b;

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/common/ui/widget/c;

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/common/ui/widget/c;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    if-eqz v6, :cond_6

    new-instance v0, Lcom/google/android/gms/common/ui/widget/b;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/gms/common/ui/widget/c;

    aput-object v1, v2, v5

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->F:Lcom/google/android/gms/plus/audience/y;

    aput-object v1, v2, v7

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    :goto_5
    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->q:Lcom/google/android/gms/common/ui/widget/c;

    .line 376
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    .line 377
    return-void

    :cond_6
    move-object v0, v1

    goto :goto_5

    :cond_7
    move-object v0, v8

    goto :goto_4
.end method

.method public final c(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 341
    if-nez p1, :cond_0

    .line 342
    new-instance v0, Lcom/google/android/gms/common/ui/widget/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/ui/widget/f;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->o:Lcom/google/android/gms/common/ui/widget/c;

    .line 346
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    .line 347
    return-void

    .line 344
    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/audience/x;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/audience/x;-><init>(Lcom/google/android/gms/plus/audience/o;Lcom/google/android/gms/common/data/d;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->o:Lcom/google/android/gms/common/ui/widget/c;

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
    .locals 2

    .prologue
    .line 393
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/o;->A:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->A:Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/o;->z:Landroid/database/Cursor;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;Landroid/database/Cursor;)Lcom/google/android/gms/common/ui/widget/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->K:Lcom/google/android/gms/common/ui/widget/c;

    .line 395
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    .line 396
    return-void
.end method

.method protected d()Lcom/google/android/gms/common/ui/widget/c;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 405
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/o;->w:Z

    if-eqz v0, :cond_3

    .line 406
    iput-boolean v2, p0, Lcom/google/android/gms/plus/audience/o;->w:Z

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->x:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_0

    move v1, v2

    .line 409
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->x:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 410
    iget-object v3, p0, Lcom/google/android/gms/plus/audience/o;->P:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->x:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/model/j;

    invoke-interface {v0}, Lcom/google/android/gms/people/model/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    if-eqz v0, :cond_1

    move v1, v2

    .line 415
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 416
    iget-object v3, p0, Lcom/google/android/gms/plus/audience/o;->P:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->y:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 419
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    if-eqz v0, :cond_2

    move v1, v2

    .line 421
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 422
    iget-object v3, p0, Lcom/google/android/gms/plus/audience/o;->P:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->B:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 426
    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/audience/s;

    sget v1, Lcom/google/android/gms/p;->sR:I

    const-string v3, "\ud83d\udd0d"

    new-instance v4, Lcom/google/android/gms/plus/audience/aa;

    iget-object v5, p0, Lcom/google/android/gms/plus/audience/o;->P:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/google/android/gms/plus/audience/aa;-><init>(Lcom/google/android/gms/plus/audience/o;Ljava/util/Collection;)V

    invoke-direct {v0, p0, v1, v3, v4}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;ILjava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->J:Lcom/google/android/gms/common/ui/widget/c;

    .line 431
    :cond_3
    new-instance v1, Lcom/google/android/gms/common/ui/widget/b;

    const/4 v0, 0x6

    new-array v3, v0, [Lcom/google/android/gms/common/ui/widget/c;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->J:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v0, v3, v2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->K:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v0, v3, v9

    new-instance v4, Lcom/google/android/gms/plus/audience/s;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/o;->s:Z

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/gms/p;->sQ:I

    :goto_3
    const-string v5, "\u25ef"

    new-instance v6, Lcom/google/android/gms/common/ui/widget/b;

    new-array v7, v10, [Lcom/google/android/gms/common/ui/widget/c;

    iget-object v8, p0, Lcom/google/android/gms/plus/audience/o;->o:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v8, v7, v2

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->p:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v2, v7, v9

    invoke-direct {v6, p0, v7}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    invoke-direct {v4, p0, v0, v5, v6}, Lcom/google/android/gms/plus/audience/s;-><init>(Lcom/google/android/gms/plus/audience/o;ILjava/lang/String;Lcom/google/android/gms/common/ui/widget/c;)V

    aput-object v4, v3, v10

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->q:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v2, v3, v0

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->F:Lcom/google/android/gms/plus/audience/y;

    :goto_4
    aput-object v0, v3, v2

    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->I:Lcom/google/android/gms/common/ui/widget/c;

    aput-object v2, v3, v0

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/common/ui/widget/b;-><init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V

    return-object v1

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->G:Lcom/google/android/gms/common/ui/widget/f;

    goto :goto_4
.end method

.method public final d(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 353
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/audience/o;->w:Z

    .line 354
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/o;->x:Lcom/google/android/gms/common/data/d;

    .line 355
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/ui/widget/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->q:Lcom/google/android/gms/common/ui/widget/c;

    .line 356
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    .line 357
    return-void
.end method

.method protected e()Z
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->o:Lcom/google/android/gms/common/ui/widget/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->o:Lcom/google/android/gms/common/ui/widget/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/widget/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->p:Lcom/google/android/gms/common/ui/widget/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->p:Lcom/google/android/gms/common/ui/widget/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/widget/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->q:Lcom/google/android/gms/common/ui/widget/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->q:Lcom/google/android/gms/common/ui/widget/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/widget/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->K:Lcom/google/android/gms/common/ui/widget/c;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->K:Lcom/google/android/gms/common/ui/widget/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/widget/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final f()Lcom/google/android/gms/common/ui/widget/c;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->p:Lcom/google/android/gms/common/ui/widget/c;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 262
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->L:I

    return v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->M:I

    return v0
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/google/android/gms/plus/audience/o;->N:I

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    .line 276
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/o;->getItemViewType(I)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/plus/audience/o;->c:I

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->O:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 306
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->r:Lcom/google/android/gms/plus/audience/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->b(Lcom/google/android/gms/plus/audience/bh;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/o;->O:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 311
    return-void
.end method

.method public final l()V
    .locals 1

    .prologue
    .line 333
    new-instance v0, Lcom/google/android/gms/plus/audience/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/audience/w;-><init>(Lcom/google/android/gms/plus/audience/o;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/o;->I:Lcom/google/android/gms/common/ui/widget/c;

    .line 334
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->m()V

    .line 335
    return-void
.end method

.method protected final m()V
    .locals 1

    .prologue
    .line 400
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/o;->d()Lcom/google/android/gms/common/ui/widget/c;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/o;->a(Lcom/google/android/gms/common/ui/widget/c;)V

    .line 401
    return-void
.end method

.method protected final n()Lcom/google/android/gms/plus/audience/am;
    .locals 4

    .prologue
    .line 727
    new-instance v0, Lcom/google/android/gms/plus/audience/am;

    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/a;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/o;->e:Landroid/view/View$OnClickListener;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/audience/am;-><init>(Landroid/content/Context;Landroid/view/View$OnClickListener;B)V

    return-object v0
.end method
