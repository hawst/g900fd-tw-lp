.class final Lcom/google/android/gms/drive/ui/picker/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/picker/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/picker/m;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/q;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1007
    check-cast p1, Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/q;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.PICK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v2, "response_drive_id"

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/q;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v3}, Lcom/google/android/gms/drive/ui/picker/m;->e(Lcom/google/android/gms/drive/ui/picker/m;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/q;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v1, v6}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/ui/picker/m;I)V

    :goto_0
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_0
    const-string v1, "PickEntryDialogFragment"

    const-string v2, "Cannot grant access"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "PickEntryDialogFragment"

    const-string v2, "Cannot grant app %d access to driveId %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/q;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v4}, Lcom/google/android/gms/drive/ui/picker/m;->f(Lcom/google/android/gms/drive/ui/picker/m;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v6

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/q;->a:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-static {v5}, Lcom/google/android/gms/drive/ui/picker/m;->e(Lcom/google/android/gms/drive/ui/picker/m;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/q;->a:Lcom/google/android/gms/drive/ui/picker/m;

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/ui/picker/m;I)V

    goto :goto_0
.end method
