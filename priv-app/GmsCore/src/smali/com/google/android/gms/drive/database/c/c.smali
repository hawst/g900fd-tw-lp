.class final enum Lcom/google/android/gms/drive/database/c/c;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/c/c;

.field public static final enum b:Lcom/google/android/gms/drive/database/c/c;

.field public static final enum c:Lcom/google/android/gms/drive/database/c/c;

.field public static final enum d:Lcom/google/android/gms/drive/database/c/c;

.field private static final synthetic f:[Lcom/google/android/gms/drive/database/c/c;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 10
    new-instance v0, Lcom/google/android/gms/drive/database/c/c;

    const-string v1, "LEFT"

    const-string v2, "LEFT "

    invoke-direct {v0, v1, v3, v2}, Lcom/google/android/gms/drive/database/c/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/c/c;->a:Lcom/google/android/gms/drive/database/c/c;

    .line 11
    new-instance v0, Lcom/google/android/gms/drive/database/c/c;

    const-string v1, "LEFT_OUTER"

    const-string v2, "LEFT OUTER "

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/gms/drive/database/c/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/c/c;->b:Lcom/google/android/gms/drive/database/c/c;

    .line 12
    new-instance v0, Lcom/google/android/gms/drive/database/c/c;

    const-string v1, "INNER"

    const-string v2, ""

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/gms/drive/database/c/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/c/c;->c:Lcom/google/android/gms/drive/database/c/c;

    .line 13
    new-instance v0, Lcom/google/android/gms/drive/database/c/c;

    const-string v1, "CROSS"

    const-string v2, "CROSS "

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/drive/database/c/c;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/database/c/c;->d:Lcom/google/android/gms/drive/database/c/c;

    .line 9
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/database/c/c;

    sget-object v1, Lcom/google/android/gms/drive/database/c/c;->a:Lcom/google/android/gms/drive/database/c/c;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/gms/drive/database/c/c;->b:Lcom/google/android/gms/drive/database/c/c;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/gms/drive/database/c/c;->c:Lcom/google/android/gms/drive/database/c/c;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/android/gms/drive/database/c/c;->d:Lcom/google/android/gms/drive/database/c/c;

    aput-object v1, v0, v6

    sput-object v0, Lcom/google/android/gms/drive/database/c/c;->f:[Lcom/google/android/gms/drive/database/c/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 18
    iput-object p3, p0, Lcom/google/android/gms/drive/database/c/c;->e:Ljava/lang/String;

    .line 19
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/c;
    .locals 1

    .prologue
    .line 9
    const-class v0, Lcom/google/android/gms/drive/database/c/c;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/c/c;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/c/c;
    .locals 1

    .prologue
    .line 9
    sget-object v0, Lcom/google/android/gms/drive/database/c/c;->f:[Lcom/google/android/gms/drive/database/c/c;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/c/c;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/c/c;

    return-object v0
.end method


# virtual methods
.method final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/drive/database/c/c;->e:Ljava/lang/String;

    return-object v0
.end method
