.class public final Lcom/google/android/gms/wallet/payform/a;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bp;


# static fields
.field static final a:[I

.field private static final v:Ljava/util/ArrayList;


# instance fields
.field private A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private B:Ljava/util/ArrayList;

.field private C:Landroid/accounts/Account;

.field private D:Z

.field private E:Ljava/util/ArrayList;

.field private F:I

.field private G:Z

.field private H:Z

.field private I:Z

.field private J:Z

.field private K:Lcom/google/aa/b/a/a/a/a/i;

.field private L:Lcom/google/android/gms/wallet/payform/f;

.field private M:I

.field private N:I

.field private O:Lcom/google/android/apps/common/a/a/i;

.field private P:Lcom/google/android/apps/common/a/a/h;

.field b:Landroid/view/View;

.field c:Landroid/widget/ProgressBar;

.field d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field e:Lcom/google/android/gms/wallet/common/ui/cc;

.field f:Lcom/google/android/gms/wallet/common/ui/v;

.field g:Landroid/view/View;

.field h:Landroid/view/View;

.field i:Lcom/google/android/gms/wallet/common/ui/v;

.field j:Lcom/google/android/gms/wallet/service/k;

.field k:Lcom/google/android/gms/wallet/common/PaymentModel;

.field l:Z

.field m:Z

.field n:Z

.field o:Lcom/google/aa/a/a/a/f;

.field p:I

.field q:Lcom/google/checkout/inapp/proto/a/b;

.field final r:Lcom/google/android/gms/wallet/service/l;

.field s:Lcom/google/android/gms/wallet/common/ui/w;

.field t:Lcom/google/android/gms/wallet/common/ui/w;

.field u:Lcom/google/android/gms/wallet/common/ui/cd;

.field private w:Landroid/view/View;

.field private x:Landroid/view/View;

.field private y:Landroid/view/View;

.field private z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 143
    sput-object v0, Lcom/google/android/gms/wallet/payform/a;->v:Ljava/util/ArrayList;

    .line 149
    sput-object v0, Lcom/google/android/gms/wallet/payform/a;->a:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 71
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 184
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    .line 185
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    .line 186
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    .line 187
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->l:Z

    .line 190
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->m:Z

    .line 192
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    .line 196
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/payform/a;->p:I

    .line 1054
    new-instance v0, Lcom/google/android/gms/wallet/payform/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/payform/b;-><init>(Lcom/google/android/gms/wallet/payform/a;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->r:Lcom/google/android/gms/wallet/service/l;

    .line 1217
    new-instance v0, Lcom/google/android/gms/wallet/payform/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/payform/c;-><init>(Lcom/google/android/gms/wallet/payform/a;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->s:Lcom/google/android/gms/wallet/common/ui/w;

    .line 1242
    new-instance v0, Lcom/google/android/gms/wallet/payform/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/payform/d;-><init>(Lcom/google/android/gms/wallet/payform/a;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->t:Lcom/google/android/gms/wallet/common/ui/w;

    .line 1275
    new-instance v0, Lcom/google/android/gms/wallet/payform/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/payform/e;-><init>(Lcom/google/android/gms/wallet/payform/a;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->u:Lcom/google/android/gms/wallet/common/ui/cd;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/payform/a;Lcom/google/checkout/inapp/proto/a/b;)I
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    return v0
.end method

.method private a(Lcom/google/checkout/inapp/proto/a/b;)I
    .locals 1

    .prologue
    .line 1036
    if-nez p1, :cond_0

    .line 1037
    const/16 v0, 0x7b

    .line 1045
    :goto_0
    return v0

    .line 1038
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1039
    const/16 v0, 0x7c

    goto :goto_0

    .line 1040
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1041
    const/16 v0, 0x7d

    goto :goto_0

    .line 1042
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1043
    const/16 v0, 0x7e

    goto :goto_0

    .line 1045
    :cond_3
    const/16 v0, 0x7f

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/checkout/inapp/proto/j;)I
    .locals 1

    .prologue
    .line 71
    invoke-static {p0}, Lcom/google/android/gms/wallet/payform/a;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    return v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;ZII)Lcom/google/android/gms/wallet/payform/a;
    .locals 3

    .prologue
    .line 225
    new-instance v0, Lcom/google/android/gms/wallet/payform/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/payform/a;-><init>()V

    .line 226
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 227
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 228
    const-string v2, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 229
    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 230
    const-string v2, "allowChangeAccounts"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 231
    const-string v2, "startingContentHeight"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 232
    const-string v2, "startingContentWidth"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 233
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/a;->setArguments(Landroid/os/Bundle;)V

    .line 234
    return-object v0
.end method

.method private a([Lcom/google/checkout/inapp/proto/a/b;Z)Lcom/google/checkout/inapp/proto/a/b;
    .locals 5

    .prologue
    .line 991
    array-length v2, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v0, p1, v1

    .line 992
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v3

    .line 993
    const/16 v4, 0x7f

    if-eq v3, v4, :cond_0

    if-eqz p2, :cond_1

    const/16 v4, 0x7d

    if-ne v3, v4, :cond_1

    .line 998
    :cond_0
    :goto_1
    return-object v0

    .line 991
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 998
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 766
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 767
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 769
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/payform/a;->a(ILandroid/content/Intent;)V

    .line 770
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 773
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 774
    invoke-virtual {v0, p1, p2}, Landroid/app/Activity;->setResult(ILandroid/content/Intent;)V

    .line 775
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 778
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/payform/a;)V
    .locals 8

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/a;->C:Landroid/accounts/Account;

    const-string v3, "US"

    sget-object v4, Lcom/google/android/gms/wallet/payform/a;->v:Ljava/util/ArrayList;

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    iget-object v6, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-boolean v7, p0, Lcom/google/android/gms/wallet/payform/a;->D:Z

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/ow/SignupActivity;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/aa/b/a/a/a/a/u;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/ImmediateFullWalletRequest;Z)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/a;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/payform/a;I)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/a;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/payform/a;ILjava/util/ArrayList;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->C:Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/gms/wallet/payform/a;->E:Ljava/util/ArrayList;

    iget-boolean v8, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i()Z

    move-result v10

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v11

    const/4 v9, 0x1

    move-object v3, p2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;ZZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/payform/a;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V
    .locals 5

    .prologue
    .line 71
    iget-object v0, p1, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->E:Ljava/util/ArrayList;

    iget-object v0, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-lez v0, :cond_1

    iget-object v1, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/payform/a;->E:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/payform/a;Lcom/google/checkout/inapp/proto/a/b;ILjava/util/ArrayList;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->C:Landroid/accounts/Account;

    iget-object v6, p0, Lcom/google/android/gms/wallet/payform/a;->E:Ljava/util/ArrayList;

    iget-boolean v7, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v9

    const/4 v8, 0x1

    move-object v2, p1

    move-object v3, p3

    move-object v5, v4

    invoke-static/range {v0 .. v9}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/a/b;Ljava/util/ArrayList;Ljava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/wallet/payform/a;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/payform/a;Lcom/google/checkout/inapp/proto/j;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->C:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v10

    const/4 v9, 0x1

    move-object v2, p1

    move v4, v3

    move-object v6, v5

    move-object v7, v5

    move-object v8, v5

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;ZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/a;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private static b(Lcom/google/checkout/inapp/proto/j;)I
    .locals 5

    .prologue
    const/16 v0, 0x79

    .line 870
    if-nez p0, :cond_1

    .line 871
    const/16 v0, 0x77

    .line 898
    :cond_0
    :goto_0
    return v0

    .line 873
    :cond_1
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    .line 874
    sget-object v2, Lcom/google/android/gms/wallet/payform/a;->a:[I

    invoke-static {v2, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 875
    const/4 v0, 0x3

    if-ne v1, v0, :cond_2

    .line 876
    const/16 v0, 0x7b

    goto :goto_0

    .line 878
    :cond_2
    const/16 v0, 0x7a

    goto :goto_0

    .line 880
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    if-lez v1, :cond_5

    .line 882
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v4, v1

    .line 883
    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_4

    .line 884
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v1, v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 885
    const/16 v3, 0x7d

    .line 883
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_4
    move v0, v1

    .line 890
    goto :goto_0

    .line 892
    :cond_5
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 894
    :pswitch_0
    const/16 v0, 0x7f

    goto :goto_0

    .line 896
    :pswitch_1
    const/16 v0, 0x78

    goto :goto_0

    .line 892
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/payform/a;)Landroid/view/View;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V
    .locals 9

    .prologue
    const/16 v7, 0x7f

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 71
    iget-object v4, p1, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v4, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/payform/a;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    if-nez v2, :cond_8

    array-length v5, v4

    move v2, v3

    :goto_0
    if-ge v2, v5, :cond_5

    aget-object v0, v4, v2

    iget-boolean v6, v0, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v6, :cond_4

    :goto_1
    if-nez v0, :cond_0

    array-length v5, v4

    move v2, v3

    :goto_2
    if-ge v2, v5, :cond_7

    aget-object v0, v4, v2

    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v6

    if-ne v6, v7, :cond_6

    :cond_0
    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/wallet/payform/a;->b(Lcom/google/checkout/inapp/proto/j;)I

    move-result v2

    :goto_4
    if-eq v2, v7, :cond_1

    move-object v0, v1

    :cond_1
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    :cond_2
    const-string v2, "LocalPaymentDetailsFra"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "updatePaymentInstruments selectedInstrument id = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v3}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Z)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v3}, Lcom/google/android/gms/wallet/common/ui/cc;->b_(Z)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    sget-object v2, Lcom/google/android/gms/wallet/payform/a;->a:[I

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->a([I)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v4}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_1

    :cond_6
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto :goto_3

    :cond_8
    move v8, v0

    move-object v0, v2

    move v2, v8

    goto :goto_4
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 691
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 692
    if-eqz p1, :cond_0

    .line 693
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->c(Z)V

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v0

    .line 697
    new-instance v1, Lcom/google/aa/b/a/a/a/a/o;

    invoke-direct {v1}, Lcom/google/aa/b/a/a/a/a/o;-><init>()V

    .line 698
    iput-object v0, v1, Lcom/google/aa/b/a/a/a/a/o;->a:Lcom/google/aa/b/a/a/a/a/u;

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->j:Lcom/google/android/gms/wallet/service/k;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/o;Z)V

    .line 701
    :cond_1
    return-void
.end method

.method static synthetic c()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/gms/wallet/payform/a;->v:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/payform/a;)V
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->c(Z)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 71
    iget-object v4, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v4, v1}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v1

    if-nez v3, :cond_7

    array-length v5, v4

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_5

    aget-object v1, v4, v3

    iget-boolean v6, v1, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    if-eqz v6, :cond_4

    :goto_1
    if-nez v1, :cond_6

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-direct {p0, v4, v0}, Lcom/google/android/gms/wallet/payform/a;->a([Lcom/google/checkout/inapp/proto/a/b;Z)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v1

    :goto_3
    const/16 v3, 0x7f

    if-eq v1, v3, :cond_2

    const/16 v3, 0x7d

    if-ne v1, v3, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    if-eqz v1, :cond_2

    :cond_1
    move-object v0, v2

    :cond_2
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v4}, Lcom/google/android/gms/wallet/common/ui/v;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/common/ui/v;->a(Z)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    :cond_3
    return-void

    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_5
    move-object v1, v2

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2

    :cond_7
    move-object v0, v3

    goto :goto_3
.end method

.method private c(Z)V
    .locals 3

    .prologue
    .line 725
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/wallet/payform/a;->F:I

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/a;->F:I

    .line 726
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->f()V

    .line 727
    return-void

    .line 725
    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->O:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 641
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    .line 642
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/payform/a;->m:Z

    .line 643
    iput v2, p0, Lcom/google/android/gms/wallet/payform/a;->F:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->f()V

    .line 645
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->j:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->r:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;)V

    .line 646
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->l:Z

    if-eqz v0, :cond_2

    .line 647
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    if-eqz v0, :cond_1

    .line 648
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->r:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/aa/a/a/a/f;)V

    .line 650
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_0

    .line 651
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/payform/a;->c(Z)V

    .line 660
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->j:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->r:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/payform/a;->p:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/payform/a;->p:I

    .line 661
    return-void

    .line 654
    :cond_1
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/payform/a;->b(Z)V

    goto :goto_0

    .line 657
    :cond_2
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/payform/a;->b(Z)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/payform/a;Lcom/google/aa/a/a/a/f;)V
    .locals 11

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 71
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    if-eqz v0, :cond_4

    iget-object v6, p1, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v6

    new-array v7, v0, [Lcom/google/checkout/inapp/proto/a/b;

    array-length v8, v6

    move v2, v3

    move v4, v3

    :goto_0
    if-ge v2, v8, :cond_1

    aget-object v0, v6, v2

    iget-boolean v5, v0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eqz v5, :cond_0

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/wallet/payform/a;->B:Ljava/util/ArrayList;

    iget-object v9, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v9, v9, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-boolean v3, v0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    const/4 v9, 0x2

    invoke-static {v5, v9}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v5

    iput-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    :cond_0
    add-int/lit8 v5, v4, 0x1

    aput-object v0, v7, v4

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v4, v5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v7, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    if-nez v2, :cond_7

    array-length v4, v7

    move v2, v3

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v0, v7, v2

    iget-boolean v5, v0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    if-eqz v5, :cond_5

    :goto_2
    if-nez v0, :cond_2

    invoke-direct {p0, v7, v3}, Lcom/google/android/gms/wallet/payform/a;->a([Lcom/google/checkout/inapp/proto/a/b;Z)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v2

    :goto_3
    const/16 v3, 0x7f

    if-eq v2, v3, :cond_3

    move-object v0, v1

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v7}, Lcom/google/android/gms/wallet/common/ui/v;->a([Lcom/google/checkout/inapp/proto/a/b;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    invoke-interface {v1, v2}, Lcom/google/android/gms/wallet/common/ui/v;->a(Z)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    :cond_4
    return-void

    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_2

    :cond_7
    move v10, v0

    move-object v0, v2

    move v2, v10

    goto :goto_3
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->P:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 671
    iget v0, p0, Lcom/google/android/gms/wallet/payform/a;->p:I

    if-gez v0, :cond_0

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->j:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->r:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/a;->p:I

    .line 676
    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->O:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 731
    iget v0, p0, Lcom/google/android/gms/wallet/payform/a;->F:I

    if-lez v0, :cond_1

    move v0, v1

    .line 732
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->h()Z

    move-result v3

    if-eq v0, v3, :cond_0

    .line 733
    if-eqz v0, :cond_2

    .line 734
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 735
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 736
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    .line 737
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    .line 746
    :cond_0
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->g()V

    .line 747
    return-void

    :cond_1
    move v0, v2

    .line 731
    goto :goto_0

    .line 740
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->c:Landroid/widget/ProgressBar;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 741
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 742
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    .line 743
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->setEnabled(Z)V

    goto :goto_1
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->P:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private g()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 750
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 753
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 754
    return-void

    :cond_1
    move v0, v1

    .line 750
    goto :goto_0

    :cond_2
    move v2, v1

    .line 753
    goto :goto_1
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/aa/b/a/a/a/a/i;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->K:Lcom/google/aa/b/a/a/a/a/i;

    return-object v0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/payform/a;)Lcom/google/android/gms/wallet/payform/f;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->L:Lcom/google/android/gms/wallet/payform/f;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/payform/a;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->B:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/payform/a;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->g()V

    return-void
.end method

.method static synthetic l(Lcom/google/android/gms/wallet/payform/a;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/wallet/payform/a;)V
    .locals 15

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->C:Landroid/accounts/Account;

    sget-object v6, Lcom/google/android/gms/wallet/payform/a;->a:[I

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->i()Z

    move-result v13

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v14

    const/4 v12, 0x1

    move-object v3, v2

    move v5, v4

    move-object v7, v2

    move v8, v4

    move-object v9, v2

    move-object v10, v2

    move-object v11, v2

    invoke-static/range {v0 .. v14}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;ZZLjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x1f6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/a;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 636
    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->x:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->y:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 638
    return-void

    :cond_0
    move v0, v2

    .line 636
    goto :goto_0

    :cond_1
    move v1, v2

    .line 637
    goto :goto_1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 685
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 441
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    .line 442
    packed-switch p1, :pswitch_data_0

    .line 617
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_2

    .line 620
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "payment_details"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 624
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->m:Z

    if-eqz v0, :cond_1

    .line 625
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->d()V

    .line 627
    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/payform/a;->c(Z)V

    .line 630
    :cond_2
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 631
    :goto_1
    return-void

    .line 444
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 464
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add billing address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 447
    :pswitch_1
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 448
    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 451
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    .line 452
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 459
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 470
    :pswitch_3
    packed-switch p2, :pswitch_data_2

    .line 490
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to update billing address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 473
    :pswitch_4
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 474
    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 477
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    .line 478
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 481
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 485
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 496
    :pswitch_6
    packed-switch p2, :pswitch_data_3

    .line 516
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add shipping address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 499
    :pswitch_7
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 500
    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 503
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 504
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 511
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 522
    :pswitch_9
    packed-switch p2, :pswitch_data_4

    .line 542
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to update shipping address to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 525
    :pswitch_a
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 526
    const-string v0, "com.google.android.gms.wallet.address"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 529
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 530
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 533
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 537
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/v;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 548
    :pswitch_c
    packed-switch p2, :pswitch_data_5

    .line 569
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add instrument to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 551
    :pswitch_d
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 552
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 555
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 556
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 559
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 563
    :pswitch_e
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 564
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 575
    :pswitch_f
    packed-switch p2, :pswitch_data_6

    .line 596
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to add instrument to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 578
    :pswitch_10
    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 579
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 582
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 583
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 590
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 601
    :pswitch_12
    packed-switch p2, :pswitch_data_7

    .line 612
    :pswitch_13
    const-string v0, "LocalPaymentDetailsFra"

    const-string v1, "Failed to signup to local storage"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 603
    :pswitch_14
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->L:Lcom/google/android/gms/wallet/payform/f;

    const-string v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/payform/f;->a(Landroid/accounts/Account;)V

    goto/16 :goto_1

    .line 609
    :pswitch_15
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/payform/a;->a(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 442
    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_12
        :pswitch_c
        :pswitch_0
        :pswitch_6
        :pswitch_f
        :pswitch_3
        :pswitch_9
    .end packed-switch

    .line 444
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 470
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 496
    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_7
        :pswitch_8
    .end packed-switch

    .line 522
    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 548
    :pswitch_data_5
    .packed-switch -0x1
        :pswitch_d
        :pswitch_e
    .end packed-switch

    .line 575
    :pswitch_data_6
    .packed-switch -0x1
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 601
    :pswitch_data_7
    .packed-switch -0x1
        :pswitch_15
        :pswitch_15
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 4

    .prologue
    .line 246
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 248
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 249
    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 250
    const-string v0, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->g()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    .line 254
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    if-eqz v0, :cond_1

    .line 255
    const/16 v0, 0x194

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->a(I)V

    .line 277
    :goto_1
    return-void

    .line 252
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->e()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    .line 259
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->C:Landroid/accounts/Account;

    .line 260
    const-string v0, "allowChangeAccounts"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->D:Z

    .line 261
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    if-eqz v0, :cond_2

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->k()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->b(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->B:Ljava/util/ArrayList;

    .line 268
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->j:Lcom/google/android/gms/wallet/service/k;

    if-nez v0, :cond_3

    .line 269
    new-instance v0, Lcom/google/android/gms/wallet/service/d;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/a;->C:Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, p1, v3}, Lcom/google/android/gms/wallet/service/d;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->j:Lcom/google/android/gms/wallet/service/k;

    .line 274
    :cond_3
    :try_start_0
    check-cast p1, Lcom/google/android/gms/wallet/payform/f;

    iput-object p1, p0, Lcom/google/android/gms/wallet/payform/a;->L:Lcom/google/android/gms/wallet/payform/f;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 275
    :catch_0
    move-exception v0

    .line 276
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Activity must implement PaymentsDialogEventListener"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1051
    new-instance v2, Lcom/google/aa/b/a/a/a/a/i;

    invoke-direct {v2}, Lcom/google/aa/b/a/a/a/a/i;-><init>()V

    new-instance v3, Lcom/google/aa/b/a/d;

    invoke-direct {v3}, Lcom/google/aa/b/a/d;-><init>()V

    iput-object v3, v2, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    iget-object v3, v2, Lcom/google/aa/b/a/a/a/a/i;->p:Lcom/google/aa/b/a/d;

    const-string v4, "USD"

    iput-object v4, v3, Lcom/google/aa/b/a/d;->a:Ljava/lang/String;

    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/i;->m:Z

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/payform/a;->H:Z

    iput-boolean v3, v2, Lcom/google/aa/b/a/a/a/a/i;->h:Z

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v3, v3, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, v3, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/aa/b/a/a/a/a/i;->b:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/payform/a;->J:Z

    if-nez v3, :cond_0

    move v0, v1

    :cond_0
    iput-boolean v0, v2, Lcom/google/aa/b/a/a/a/a/i;->i:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->A:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/i;->z:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v3, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v2, p0, Lcom/google/android/gms/wallet/payform/a;->K:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->j:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/aa/b/a/a/a/a/i;Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/payform/a;->c(Z)V

    .line 1052
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 282
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 283
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->setRetainInstance(Z)V

    .line 285
    if-eqz p1, :cond_4

    .line 286
    const-string v0, "model"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 287
    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    .line 289
    const-string v0, "walletItemsReceived"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->l:Z

    .line 290
    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    const-string v0, "getWalletItemsResponse"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/aa/a/a/a/f;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/a/a/a/f;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    .line 295
    :cond_0
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 296
    const-string v0, "serviceConnectionSavePoint"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/a;->p:I

    .line 299
    :cond_1
    const-string v0, "fullWalletRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 300
    const-string v0, "fullWalletRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/aa/b/a/a/a/a/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/i;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->K:Lcom/google/aa/b/a/a/a/a/i;

    .line 304
    :cond_2
    const-string v0, "selectedBillingAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 305
    const-string v0, "selectedBillingAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    .line 325
    :cond_3
    :goto_0
    return-void

    .line 310
    :cond_4
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "get_wallet_items_from_chrome"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->O:Lcom/google/android/apps/common/a/a/i;

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->O:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->P:Lcom/google/android/apps/common/a/a/h;

    .line 313
    new-instance v0, Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/PaymentModel;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 315
    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/payform/a;->M:I

    .line 316
    const-string v1, "startingContentHeight"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 317
    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/payform/a;->N:I

    .line 318
    const-string v1, "startingContentWidth"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 321
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->z:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "payment_details"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v5, -0x1

    .line 331
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 332
    sget v0, Lcom/google/android/gms/l;->gG:I

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 334
    sget v2, Lcom/google/android/gms/j;->cc:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/wallet/payform/a;->x:Landroid/view/View;

    .line 336
    sget v2, Lcom/google/android/gms/j;->te:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/wallet/payform/a;->y:Landroid/view/View;

    move-object v2, v0

    .line 340
    :goto_0
    sget v0, Lcom/google/android/gms/j;->nC:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->b:Landroid/view/View;

    .line 341
    sget v0, Lcom/google/android/gms/j;->dG:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    .line 342
    sget v0, Lcom/google/android/gms/j;->nE:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->c:Landroid/widget/ProgressBar;

    .line 343
    iget v0, p0, Lcom/google/android/gms/wallet/payform/a;->M:I

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_1
    iget v4, p0, Lcom/google/android/gms/wallet/payform/a;->N:I

    if-ne v4, v5, :cond_4

    move v4, v1

    :goto_2
    if-ne v0, v4, :cond_5

    :goto_3
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 345
    iget v0, p0, Lcom/google/android/gms/wallet/payform/a;->M:I

    if-eq v0, v5, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->w:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 351
    iget v1, p0, Lcom/google/android/gms/wallet/payform/a;->M:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 352
    iget v1, p0, Lcom/google/android/gms/wallet/payform/a;->N:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 353
    iput v5, p0, Lcom/google/android/gms/wallet/payform/a;->M:I

    .line 354
    iput v5, p0, Lcom/google/android/gms/wallet/payform/a;->N:I

    .line 356
    :cond_0
    sget v0, Lcom/google/android/gms/j;->nD:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->u:Lcom/google/android/gms/wallet/common/ui/cd;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 359
    sget v0, Lcom/google/android/gms/j;->nB:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/v;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->t:Lcom/google/android/gms/wallet/common/ui/w;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/android/gms/wallet/common/ui/w;)V

    .line 362
    sget v0, Lcom/google/android/gms/j;->nG:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->g:Landroid/view/View;

    .line 364
    sget v0, Lcom/google/android/gms/j;->nH:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->h:Landroid/view/View;

    .line 366
    sget v0, Lcom/google/android/gms/j;->nF:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/v;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    .line 368
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->G:Z

    if-eqz v0, :cond_6

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->s:Lcom/google/android/gms/wallet/common/ui/w;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/v;->a(Lcom/google/android/gms/wallet/common/ui/w;)V

    .line 376
    :goto_4
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 377
    sget v0, Lcom/google/android/gms/j;->fQ:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    .line 379
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 380
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->f:Lcom/google/android/gms/wallet/common/ui/v;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 381
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 382
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bp;)V

    .line 385
    :cond_1
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->d:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 388
    return-object v2

    .line 338
    :cond_2
    sget v0, Lcom/google/android/gms/l;->gF:I

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v2, v0

    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 343
    goto/16 :goto_1

    :cond_4
    move v4, v3

    goto/16 :goto_2

    :cond_5
    move v1, v3

    goto/16 :goto_3

    .line 371
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->g:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 372
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->h:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->i:Lcom/google/android/gms/wallet/common/ui/v;

    invoke-interface {v0, v6}, Lcom/google/android/gms/wallet/common/ui/v;->setVisibility(I)V

    goto :goto_4
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 435
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 436
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->L:Lcom/google/android/gms/wallet/payform/f;

    .line 437
    return-void
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 404
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    .line 405
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 406
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->e()V

    .line 407
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 393
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 395
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    if-eqz v0, :cond_0

    .line 396
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->m:Z

    .line 400
    :goto_0
    return-void

    .line 398
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->d()V

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 411
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 413
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/a;->e()V

    .line 415
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/payform/a;->p:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 416
    const-string v0, "model"

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->k:Lcom/google/android/gms/wallet/common/PaymentModel;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 417
    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 418
    const-string v0, "walletItemsReceived"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/a;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    if-eqz v0, :cond_0

    .line 420
    const-string v0, "getWalletItemsResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->o:Lcom/google/aa/a/a/a/f;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 423
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->K:Lcom/google/aa/b/a/a/a/a/i;

    if-eqz v0, :cond_1

    .line 424
    const-string v0, "fullWalletRequest"

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->K:Lcom/google/aa/b/a/a/a/a/i;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 427
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_2

    .line 428
    const-string v0, "selectedBillingAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/a;->q:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 431
    :cond_2
    return-void
.end method

.method public final startActivityForResult(Landroid/content/Intent;I)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 1210
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/a;->I:Z

    .line 1211
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/payform/a;->n:Z

    .line 1212
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/a;->c(Z)V

    .line 1213
    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 1214
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->E:I

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/q;->overridePendingTransition(II)V

    .line 1215
    return-void
.end method
