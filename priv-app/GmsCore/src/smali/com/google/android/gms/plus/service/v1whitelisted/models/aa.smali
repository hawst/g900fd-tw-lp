.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;

.field public c:Ljava/lang/String;

.field public final d:Ljava/util/Set;

.field private e:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

.field private f:I

.field private g:Ljava/util/List;

.field private h:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    .line 115
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;
    .locals 2

    .prologue
    .line 149
    iput p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->f:I

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 151
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/v;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;
    .locals 2

    .prologue
    .line 123
    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 125
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;
    .locals 2

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->h:Ljava/util/List;

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    const/16 v1, 0x5c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 194
    return-object p0
.end method

.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;
    .locals 9

    .prologue
    .line 201
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->f:I

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->g:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;

    iget-object v7, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->c:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->h:Ljava/util/List;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclDetailsEntity;Ljava/lang/String;ILjava/util/List;Lcom/google/android/gms/plus/service/v1whitelisted/models/LoggedProfileEntity;Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method
