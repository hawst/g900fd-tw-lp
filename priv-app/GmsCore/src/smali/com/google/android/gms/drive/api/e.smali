.class public final Lcom/google/android/gms/drive/api/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/api/d;


# static fields
.field public static a:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private static final b:Ljava/util/Set;


# instance fields
.field private final c:Lcom/google/android/gms/drive/g/aw;

.field private final d:Lcom/google/android/gms/drive/auth/g;

.field private final e:Lcom/google/android/gms/drive/database/r;

.field private final f:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

.field private final g:Lcom/google/android/gms/drive/a/a/a;

.field private final h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

.field private final i:Lcom/google/android/gms/drive/metadata/d;

.field private final j:Lcom/google/android/gms/drive/auth/f;

.field private final k:Lcom/google/android/gms/common/server/ClientContext;

.field private final l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

.field private final m:Landroid/content/Context;

.field private final n:Lcom/google/android/gms/drive/DriveId;

.field private final o:Lcom/google/android/gms/drive/database/i;

.field private final p:I

.field private final q:I

.field private final r:Lcom/google/android/gms/drive/events/a;

.field private final s:Lcom/google/android/gms/drive/events/w;

.field private final t:Lcom/google/android/gms/drive/e/b;

.field private final u:Lcom/google/android/gms/drive/b/d;

.field private final v:Lcom/google/android/gms/drive/g/n;

.field private final w:Lcom/google/android/gms/drive/metadata/sync/b/j;

.field private final x:Z

.field private final y:Ljava/lang/String;

.field private final z:Lcom/google/android/gms/drive/c/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 120
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 121
    const-string v1, "com.google.android.gms"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 124
    const-string v1, "com.google.android.gms.drive.sample.texteditor"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 125
    const-string v1, "com.google.android.gms.apitest"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 127
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/api/e;->b:Ljava/util/Set;

    .line 130
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/drive/api/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;IILcom/google/android/gms/drive/g/aw;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    sget-object v2, Lcom/google/android/gms/drive/api/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 177
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x10

    const-string v2, "Connections are not allowed at this time"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;)V

    throw v0

    .line 181
    :cond_0
    if-nez p2, :cond_2

    move v3, v1

    :goto_0
    if-nez p5, :cond_3

    move v2, v1

    :goto_1
    if-ne v3, v2, :cond_1

    move v0, v1

    :cond_1
    const-string v2, "proxyType must be NO_PROXY iff firstPartyProxyPackage is null"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 185
    iput p4, p0, Lcom/google/android/gms/drive/api/e;->p:I

    .line 186
    iput-object p1, p0, Lcom/google/android/gms/drive/api/e;->k:Lcom/google/android/gms/common/server/ClientContext;

    .line 187
    iput-object p2, p0, Lcom/google/android/gms/drive/api/e;->y:Ljava/lang/String;

    .line 188
    iput p5, p0, Lcom/google/android/gms/drive/api/e;->q:I

    .line 190
    iput-object p6, p0, Lcom/google/android/gms/drive/api/e;->c:Lcom/google/android/gms/drive/g/aw;

    .line 191
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->p()Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    .line 192
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->l()Lcom/google/android/gms/drive/a/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    .line 193
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->g()Lcom/google/android/gms/drive/database/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    .line 194
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    .line 195
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->m:Landroid/content/Context;

    .line 196
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->x()Lcom/google/android/gms/drive/auth/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->j:Lcom/google/android/gms/drive/auth/f;

    .line 197
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    invoke-direct {v0, p6}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    .line 198
    new-instance v0, Lcom/google/android/gms/drive/metadata/d;

    invoke-direct {v0, p6}, Lcom/google/android/gms/drive/metadata/d;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->i:Lcom/google/android/gms/drive/metadata/d;

    .line 199
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->t()Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->f:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    .line 200
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->r()Lcom/google/android/gms/drive/events/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->s:Lcom/google/android/gms/drive/events/w;

    .line 201
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->u()Lcom/google/android/gms/drive/e/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->t:Lcom/google/android/gms/drive/e/b;

    .line 202
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->v()Lcom/google/android/gms/drive/b/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->u:Lcom/google/android/gms/drive/b/d;

    .line 203
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->e()Lcom/google/android/gms/drive/g/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->v:Lcom/google/android/gms/drive/g/n;

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->j:Lcom/google/android/gms/drive/auth/f;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/drive/auth/f;->a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    .line 206
    iget-object v1, v0, Lcom/google/android/gms/drive/auth/d;->a:Lcom/google/android/gms/drive/auth/e;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/e;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 207
    iget-object v0, v0, Lcom/google/android/gms/drive/auth/d;->c:Lcom/google/android/gms/drive/auth/c;

    throw v0

    :cond_2
    move v3, v0

    .line 181
    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    .line 209
    :cond_4
    iget-object v0, v0, Lcom/google/android/gms/drive/auth/d;->b:Lcom/google/android/gms/drive/auth/g;

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    .line 210
    new-instance v0, Lcom/google/android/gms/drive/events/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/events/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->r:Lcom/google/android/gms/drive/events/a;

    .line 211
    sget-object v0, Lcom/google/android/gms/drive/ai;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 212
    invoke-static {}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a()Lcom/google/android/gms/drive/metadata/sync/c/g;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, v1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->r:Lcom/google/android/gms/drive/events/a;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a(Ljava/lang/String;Lcom/google/android/gms/drive/events/a;)V

    .line 215
    :cond_5
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/j;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-direct {v0, p6, v1}, Lcom/google/android/gms/drive/metadata/sync/b/j;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/auth/g;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->w:Lcom/google/android/gms/drive/metadata/sync/b/j;

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, v1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->m:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->gY:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 220
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/q;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/i;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    .line 221
    const-string v0, "<<default account>>"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/api/e;->x:Z

    .line 222
    invoke-virtual {p6}, Lcom/google/android/gms/drive/g/aw;->i()Lcom/google/android/gms/drive/c/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/auth/CallingAppInfo;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-direct {v1, v2, p5}, Lcom/google/android/gms/drive/auth/CallingAppInfo;-><init>(Lcom/google/android/gms/drive/auth/g;I)V

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v2, v2, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/c/c;->a(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;)Lcom/google/android/gms/drive/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/e;->z:Lcom/google/android/gms/drive/c/b;

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->z:Lcom/google/android/gms/drive/c/b;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/b;->a()V

    .line 226
    return-void
.end method

.method private static b(Ljava/util/List;)[Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0xa

    const/4 v4, 0x0

    .line 669
    if-eqz p0, :cond_3

    sget-object v0, Lcom/google/android/gms/drive/ai;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 670
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 671
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 672
    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/f;

    move-result-object v3

    .line 673
    if-nez v3, :cond_0

    .line 674
    new-instance v1, Lcom/google/android/gms/common/service/h;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown metadata field requested: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v5, v0, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1

    .line 677
    :cond_0
    invoke-interface {v3}, Lcom/google/android/gms/drive/metadata/f;->b()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 679
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 680
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Need at least one column to project"

    invoke-direct {v0, v5, v1, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 683
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 685
    :goto_1
    return-object v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private c(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/EntrySpec;
    .locals 4

    .prologue
    .line 240
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->c()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 242
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 244
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 5

    .prologue
    const/16 v4, 0x5de

    const/4 v3, 0x0

    .line 316
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/e;->c(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_0

    .line 318
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 320
    if-eqz v0, :cond_0

    .line 321
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->D()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 322
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Drive item not found, or you are not authorized to access it."

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 329
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    .line 330
    const-string v0, "DataServiceConnectionImpl"

    const-string v1, "Could not find entry, and no valid resource id: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 331
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Provided DriveId is invalid."

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 336
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "appdata"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 348
    :goto_0
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->D()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 349
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Drive item not found, or you are not authorized to access it."

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 341
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "The current scope does not allow use of the AppData folder"

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 345
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    goto :goto_0

    .line 352
    :cond_4
    return-object v0
.end method

.method private e(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 609
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    .line 626
    :goto_0
    return-object v0

    .line 616
    :cond_1
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;
    :try_end_0
    .catch Lcom/google/android/gms/common/service/h; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 622
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->x()Z

    move-result v1

    if-nez v1, :cond_3

    .line 623
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "Invalid parent folder."

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 619
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x5de

    const-string v2, "Invalid parent folder."

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 626
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    goto :goto_0
.end method

.method private t()Ljava/lang/String;
    .locals 1

    .prologue
    .line 771
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->y:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->y:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 690
    new-instance v0, Lcom/google/android/gms/drive/query/b;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/query/b;-><init>(Lcom/google/android/gms/drive/auth/g;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->a()Lcom/google/android/gms/drive/query/Filter;

    move-result-object v1

    if-nez v1, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/database/SqlWhereClause;->b:Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-object v1, v0

    .line 692
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->c()Lcom/google/android/gms/drive/query/SortOrder;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/SortOrder;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v2

    invoke-virtual {v2, v11}, Lcom/google/android/gms/drive/metadata/a/ay;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v5, "%s <> \"%s\""

    new-array v6, v10, [Ljava/lang/Object;

    aput-object v2, v6, v8

    const-string v2, "application/vnd.google-apps.folder"

    aput-object v2, v6, v9

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/SortOrder;->a()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/SortOrder;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/internal/f;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/f;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v1, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0xa

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Incorrect sorting field provided: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0, v8}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1

    .line 690
    :cond_1
    invoke-interface {v1, v0}, Lcom/google/android/gms/drive/query/Filter;->a(Lcom/google/android/gms/drive/query/internal/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/query/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/query/d;->b()Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 692
    :cond_2
    invoke-static {v2}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v5

    invoke-virtual {v5, v11}, Lcom/google/android/gms/drive/metadata/a/ay;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    instance-of v2, v2, Lcom/google/android/gms/drive/metadata/internal/m;

    if-eqz v2, :cond_3

    const-string v2, "TRIM(%s) COLLATE LOCALIZED %s"

    :goto_2
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v5, v7, v8

    invoke-virtual {v0}, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "ASC"

    :goto_3
    aput-object v0, v7, v9

    invoke-static {v6, v2, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    const-string v2, "%s COLLATE LOCALIZED %s"

    goto :goto_2

    :cond_4
    const-string v0, "DESC"

    goto :goto_3

    :cond_5
    const-string v0, ","

    invoke-static {v0, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 694
    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 696
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->c:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " DESC"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 698
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->d()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/drive/api/e;->b(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v2

    .line 699
    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v4, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v3, v4, v1, v0, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/SqlWhereClause;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 701
    iget v1, p0, Lcom/google/android/gms/drive/api/e;->p:I

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/ak;->a(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 702
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/DriveId;
    .locals 9

    .prologue
    .line 431
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/e;->e(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    .line 433
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, v1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-wide v4, v3, Lcom/google/android/gms/drive/auth/g;->b:J

    move-object v3, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/metadata/d;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;J)V

    .line 437
    :cond_0
    new-instance v3, Lcom/google/android/gms/drive/a/n;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v4, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v5, v0, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    sget-object v8, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    move-object v6, p2

    move-object v7, v2

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/drive/a/n;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/ac;)V

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v0

    .line 442
    if-eqz v0, :cond_2

    .line 445
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    const/16 v0, 0x5de

    .line 449
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Failed to create folder."

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1

    .line 445
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 451
    :cond_2
    iget-object v0, v3, Lcom/google/android/gms/drive/a/n;->d:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;IIIZLjava/lang/String;)Lcom/google/android/gms/drive/DriveId;
    .locals 16

    .prologue
    .line 458
    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/drive/api/e;->e(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v6

    .line 460
    const/4 v4, 0x1

    move/from16 v0, p4

    if-ne v0, v4, :cond_0

    .line 461
    const/16 p3, 0x0

    .line 463
    :cond_0
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/gms/drive/api/e;->x:Z

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/drive/api/e;->y:Ljava/lang/String;

    move/from16 v0, p5

    move/from16 v1, p6

    move-object/from16 v2, p7

    invoke-static {v0, v1, v4, v2, v5}, Lcom/google/android/gms/drive/a/a/l;->a(IZZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/a/a/l;

    move-result-object v12

    .line 467
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/drive/api/e;->i:Lcom/google/android/gms/drive/metadata/d;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    if-nez p4, :cond_1

    iget-object v4, v15, Lcom/google/android/gms/drive/metadata/d;->b:Lcom/google/android/gms/drive/api/k;

    iget-object v5, v10, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    move/from16 v0, p3

    invoke-interface {v4, v5, v0}, Lcom/google/android/gms/drive/api/k;->a(Lcom/google/android/gms/drive/auth/AppIdentity;I)V

    :cond_1
    const-string v4, "application/vnd.google-apps.folder"

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    const/4 v4, 0x1

    :goto_0
    const-string v5, "This method may not be used to create folders."

    invoke-static {v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/CharSequence;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    iget-object v5, v15, Lcom/google/android/gms/drive/metadata/d;->c:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v7, Lcom/google/android/gms/p;->fL:I

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    :cond_2
    if-nez p4, :cond_6

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_3

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v5, "application/octet-stream"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    :cond_3
    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v15, Lcom/google/android/gms/drive/metadata/d;->a:Lcom/google/android/gms/drive/database/r;

    iget-object v5, v10, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-wide v8, v10, Lcom/google/android/gms/drive/auth/g;->b:J

    move-object/from16 v7, p2

    invoke-static/range {v4 .. v9}, Lcom/google/android/gms/drive/metadata/d;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;J)V

    :cond_4
    iget-object v7, v15, Lcom/google/android/gms/drive/metadata/d;->b:Lcom/google/android/gms/drive/api/k;

    move-object v8, v10

    move/from16 v9, p3

    move-object/from16 v10, p2

    move-object v11, v6

    invoke-interface/range {v7 .. v12}, Lcom/google/android/gms/drive/api/k;->a(Lcom/google/android/gms/drive/auth/g;ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/a/a/l;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    :goto_1
    return-object v4

    :cond_5
    const/4 v4, 0x0

    goto :goto_0

    :cond_6
    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v4

    if-nez v4, :cond_7

    const/4 v4, 0x1

    :goto_2
    const-string v5, "Creating singleton shortcut file is not supported."

    invoke-static {v4, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v5, "application/vnd.google-apps.drive-sdk"

    move-object/from16 v0, p2

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    new-instance v8, Lcom/google/android/gms/drive/a/p;

    iget-object v9, v10, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v10, v10, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v12

    move-object/from16 v11, p2

    move-object v14, v6

    invoke-direct/range {v8 .. v14}, Lcom/google/android/gms/drive/a/p;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;JLcom/google/android/gms/drive/DriveId;)V

    iget-object v4, v15, Lcom/google/android/gms/drive/metadata/d;->d:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v4, v8}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    iget-object v4, v8, Lcom/google/android/gms/drive/a/p;->d:Lcom/google/android/gms/drive/DriveId;

    goto :goto_1

    :cond_7
    const/4 v4, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Z)Lcom/google/android/gms/drive/DriveId;
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 1047
    const-string v0, "DataServiceConnectionImpl"

    const-string v1, "getDriveIdFromUniqueIdentifier for unique identifier %s, isInAppFolder: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v8

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1050
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1051
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "The current scope of your application does not allow use of the App Folder"

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 1055
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-wide v4, v0, Lcom/google/android/gms/drive/auth/g;->b:J

    move-object v3, p1

    move v6, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Ljava/util/List;

    move-result-object v0

    .line 1057
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1058
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/ah;

    .line 1059
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->D()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1060
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Found entry for unique identifier: %s"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1061
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 1087
    :goto_0
    return-object v0

    .line 1065
    :cond_2
    const-string v0, "DataServiceConnectionImpl"

    const-string v1, "Drive item not found, or you are not authorized to access it."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 1066
    goto :goto_0

    .line 1072
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;ZZ)V

    .line 1076
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-wide v4, v0, Lcom/google/android/gms/drive/auth/g;->b:J

    move-object v3, p1

    move v6, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;JZ)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 1078
    if-eqz v0, :cond_4

    .line 1079
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    goto :goto_0

    .line 1081
    :cond_4
    const-string v0, "DataServiceConnectionImpl"

    const-string v1, "Drive item not found, or you are not authorized to access it."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    move-object v0, v7

    .line 1082
    goto :goto_0

    .line 1084
    :catch_0
    move-exception v0

    .line 1085
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Failed to sync with the server and no local entry for unique identifier %s"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object p1, v3, v8

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v7

    .line 1087
    goto :goto_0

    .line 1090
    :catch_1
    move-exception v0

    .line 1091
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Processing remote result failed!"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object v0, v3, v8

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1092
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to process item."

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
.end method

.method public final a()Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 386
    const-string v0, "DataServiceConnectionImpl"

    const-string v1, "Get metadata for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/api/e;->c(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot find root folder id"

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, v1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    invoke-static {v1}, Lcom/google/android/gms/drive/auth/g;->a(Lcom/google/android/gms/drive/database/model/a;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot find root folder"

    invoke-direct {v0, v4, v1, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    move-object v1, v0

    .line 390
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/f;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/f;

    invoke-static {v0}, Lcom/google/android/gms/drive/metadata/a/am;->a(Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/metadata/a/ay;

    move-result-object v0

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/gms/drive/metadata/a/ay;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/auth/g;)V

    goto :goto_1

    .line 387
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 391
    :cond_3
    iget v0, p0, Lcom/google/android/gms/drive/api/e;->p:I

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(I)V

    .line 392
    return-object v3
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Lcom/google/android/gms/drive/c/a;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;
    .locals 6

    .prologue
    const/16 v2, 0xa

    const/4 v5, 0x0

    .line 398
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 399
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot edit metadata of the root folder"

    invoke-direct {v0, v2, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 403
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 404
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 407
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 408
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->E()Z

    move-result v1

    if-nez v1, :cond_2

    .line 409
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "The user cannot edit the resource."

    invoke-direct {v0, v2, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 412
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-static {v1, v0, p2}, Lcom/google/android/gms/drive/metadata/e;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Z

    .line 414
    invoke-interface {p3, v0}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;

    move-result-object v1

    invoke-interface {v1, p2}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/c/a;

    .line 416
    new-instance v1, Lcom/google/android/gms/drive/e/a;

    invoke-direct {v1, v0, p2}, Lcom/google/android/gms/drive/e/a;-><init>(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    .line 417
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 418
    new-instance v2, Lcom/google/android/gms/drive/a/x;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v3, v3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v4, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v4, v4, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {v2, v3, v4, v0, p2}, Lcom/google/android/gms/drive/a/x;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 421
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to process update"

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 424
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->t:Lcom/google/android/gms/drive/e/b;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->u:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/drive/e/a;->a(Lcom/google/android/gms/drive/e/b;Lcom/google/android/gms/drive/b/d;)V

    .line 425
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/util/Set;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 1099
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a(JLcom/google/android/gms/drive/DriveId;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 844
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->c:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 845
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "App must have full Drive scope to perform this action."

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 849
    :cond_0
    invoke-virtual {p0, p3}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v3

    .line 850
    new-instance v0, Lcom/google/android/gms/drive/a/ad;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, v1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v2, v2, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    sget-object v6, Lcom/google/android/gms/drive/auth/a;->a:Lcom/google/android/gms/drive/auth/a;

    sget-object v7, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    move-wide v4, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/a/ad;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;JLcom/google/android/gms/drive/auth/a;Lcom/google/android/gms/drive/a/ac;)V

    .line 853
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v0

    if-eqz v0, :cond_1

    .line 854
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to process authorization"

    invoke-direct {v0, v1, v2, v8}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 857
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;I)V
    .locals 11

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x0

    .line 872
    iget-object v5, p0, Lcom/google/android/gms/drive/api/e;->s:Lcom/google/android/gms/drive/events/w;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    :goto_0
    :try_start_0
    iget-object v3, v5, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v3, v1}, Lcom/google/android/gms/drive/database/r;->c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v3, v1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    iget-object v1, v5, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v3, v4, v2, p2}, Lcom/google/android/gms/drive/database/r;->a(JLcom/google/android/gms/drive/database/model/EntrySpec;I)Lcom/google/android/gms/drive/database/model/bv;
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    if-nez v8, :cond_1

    :try_start_1
    new-instance v0, Lcom/google/android/gms/drive/database/model/bv;

    iget-object v1, v5, Lcom/google/android/gms/drive/events/w;->b:Lcom/google/android/gms/drive/database/i;

    iget-object v5, v5, Lcom/google/android/gms/drive/events/w;->c:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v5}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v6

    move v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/database/model/bv;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;JIJ)V
    :try_end_1
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_1

    move-object v1, v0

    :goto_1
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bv;->i()V
    :try_end_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_2

    return-void

    :cond_0
    move-object v2, v0

    goto :goto_0

    :cond_1
    :try_start_3
    iget-object v0, v5, Lcom/google/android/gms/drive/events/w;->c:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    iput-wide v0, v8, Lcom/google/android/gms/drive/database/model/bv;->b:J
    :try_end_3
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v1, v8

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v10, v1

    move-object v1, v0

    move-object v0, v10

    :goto_2
    const-string v2, "SubscriptionStore"

    const-string v3, "Unable to insert subscription: "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v9

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Unable to store subscription"

    invoke-direct {v0, v1, v2, v9}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :catch_1
    move-exception v0

    move-object v1, v8

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V
    .locals 4

    .prologue
    .line 861
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->r:Lcom/google/android/gms/drive/events/a;

    iget-object v2, v1, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v3, v1, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v3, Lcom/google/android/gms/drive/events/c;

    invoke-direct {v3, p2, p3}, Lcom/google/android/gms/drive/events/c;-><init>(ILcom/google/android/gms/drive/internal/cd;)V

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/events/a;->a()V

    .line 862
    return-void

    .line 861
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)V
    .locals 9

    .prologue
    const/16 v8, 0xa

    const/4 v7, 0x0

    .line 473
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot trash root folder"

    invoke-direct {v0, v8, v1, v7}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 478
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v6

    .line 479
    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 480
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot trash App Folder  or files inside the App Folder."

    invoke-direct {v0, v8, v1, v7}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 484
    :cond_1
    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ah;->c()Z

    move-result v0

    if-nez v0, :cond_2

    .line 485
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot trash files that the user does not own."

    invoke-direct {v0, v8, v1, v7}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 489
    :cond_2
    invoke-interface {p2, v6}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;

    .line 491
    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    .line 492
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v2, v0, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    new-instance v0, Lcom/google/android/gms/drive/a/ai;

    sget-object v4, Lcom/google/android/gms/drive/database/model/ap;->c:Lcom/google/android/gms/drive/database/model/ap;

    sget-object v5, Lcom/google/android/gms/drive/a/ac;->a:Lcom/google/android/gms/drive/a/ac;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/ai;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/database/model/ap;Lcom/google/android/gms/drive/a/ac;)V

    .line 494
    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ah;->x()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 495
    sget-object v1, Lcom/google/android/gms/drive/af;->f:Lcom/google/android/gms/drive/af;

    invoke-static {v1}, Lcom/google/android/gms/drive/ag;->a(Lcom/google/android/gms/drive/af;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 496
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot trash folders"

    invoke-direct {v0, v8, v1, v7}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 502
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->v:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v1

    if-nez v1, :cond_4

    .line 503
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot trash folders while offline."

    invoke-direct {v0, v8, v1, v7}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 508
    :cond_4
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->c:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/a/ai;->a(Lcom/google/android/gms/drive/g/aw;)V

    .line 510
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v1, v1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    .line 511
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    const/16 v3, 0x66

    const/4 v4, 0x4

    invoke-virtual {v2, v1, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;II)I

    .line 514
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/a/ai;->c(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/a/g;
    :try_end_0
    .catch Lcom/google/android/gms/drive/a/h; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/drive/a/i; {:try_start_0 .. :try_end_0} :catch_2

    .line 535
    :cond_5
    return-void

    .line 517
    :catch_0
    move-exception v0

    .line 518
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Failed to trash folder."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/service/h;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 520
    :catch_1
    move-exception v0

    .line 521
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Failed to trash folder."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/service/h;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 523
    :catch_2
    move-exception v0

    .line 524
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Unexpected conflict on trash"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Unexpected conflict."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/service/h;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 529
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v0

    if-eqz v0, :cond_5

    .line 531
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to trash file."

    invoke-direct {v0, v1, v2, v7}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V
    .locals 6

    .prologue
    .line 632
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/e;->d(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v1

    .line 633
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v0

    .line 634
    :goto_0
    if-eqz v0, :cond_5

    .line 637
    if-eqz v1, :cond_3

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 640
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->b(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/d/d;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/drive/d/d;->L()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "SingleItemSynchronizer"

    const-string v2, "Head revision ID did not change so not persisting metadata"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 653
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 654
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->D()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 655
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Drive item not found, or you are not authorized to access it."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 633
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 640
    :cond_2
    :try_start_1
    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/d/d;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/text/ParseException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 648
    :catch_0
    move-exception v1

    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Failed to sync metadata"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 643
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/text/ParseException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 651
    :catch_1
    move-exception v1

    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Failed to parse response of metadata sync"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 658
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->f:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;)V

    .line 663
    :goto_2
    return-void

    .line 661
    :cond_5
    const/4 v0, 0x3

    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/b;->a(I)V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;Ljava/util/List;Lcom/google/android/gms/drive/c/a;)V
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x0

    .line 992
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 993
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "The parent list cannot be empty."

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 996
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 997
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot set parents of the root folder."

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 1000
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v1

    .line 1001
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v2

    .line 1002
    if-eqz v2, :cond_2

    .line 1003
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/e;->d()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 1004
    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1005
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot set parents of the App folder."

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 1009
    :cond_2
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 1010
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 1011
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->x()Z

    move-result v4

    if-nez v4, :cond_4

    .line 1012
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "At least one of the provided parents is not a collection. All parents must be collections."

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 1016
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v0

    if-eq v2, v0, :cond_3

    .line 1018
    if-eqz v2, :cond_5

    .line 1019
    const-string v0, "Unable to assign a parent that does not belong to the App folder to a resource from within the App folder."

    .line 1027
    :goto_0
    new-instance v1, Lcom/google/android/gms/common/service/h;

    invoke-direct {v1, v6, v0, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v1

    .line 1023
    :cond_5
    const-string v0, "Unable to assign a parent that belongs to the App folder to a resource that is not from within the App folder."

    goto :goto_0

    .line 1031
    :cond_6
    invoke-interface {p3, v1}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;

    .line 1033
    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    .line 1034
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 1035
    new-instance v2, Lcom/google/android/gms/drive/a/ag;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v3, v3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v4, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v4, v4, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {v2, v3, v4, v0, v1}, Lcom/google/android/gms/drive/a/ag;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/util/Set;)V

    .line 1037
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v0

    if-eqz v0, :cond_7

    .line 1038
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to process update"

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 1041
    :cond_7
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/events/DriveEvent;)V
    .locals 4

    .prologue
    .line 883
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->r:Lcom/google/android/gms/drive/events/a;

    instance-of v0, p1, Lcom/google/android/gms/drive/events/ResourceEvent;

    const-string v2, "Only resource events supported"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/drive/events/ResourceEvent;

    invoke-interface {v0}, Lcom/google/android/gms/drive/events/ResourceEvent;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iget-object v2, v1, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/drive/events/a;->a(Lcom/google/android/gms/drive/events/DriveEvent;Lcom/google/android/gms/drive/DriveId;)Z

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v3}, Lcom/google/android/gms/drive/events/a;->a(Lcom/google/android/gms/drive/events/DriveEvent;Lcom/google/android/gms/drive/DriveId;)Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/events/a;->a()V

    .line 884
    :cond_1
    return-void

    .line 883
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;)V
    .locals 4

    .prologue
    .line 1122
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->a()I

    move-result v0

    .line 1123
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->c()I

    move-result v1

    .line 1125
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v2, v3}, Lcom/google/android/gms/drive/database/r;->e(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/l;

    move-result-object v2

    .line 1126
    if-eqz v0, :cond_0

    .line 1127
    iput v0, v2, Lcom/google/android/gms/drive/database/model/l;->a:I

    .line 1129
    :cond_0
    if-eqz v1, :cond_1

    .line 1130
    iput v1, v2, Lcom/google/android/gms/drive/database/model/l;->b:I

    .line 1132
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;->b()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/drive/database/model/l;->c:Z

    .line 1133
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/l;->i()V

    .line 1134
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/a;->a()Lcom/google/android/gms/drive/a/a/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/m;->c()V

    .line 1135
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/query/Query;Lcom/google/android/gms/drive/api/a/al;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 899
    iget-object v6, p0, Lcom/google/android/gms/drive/api/e;->w:Lcom/google/android/gms/drive/metadata/sync/b/j;

    iget-object v0, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/drive/metadata/sync/b/j;->a(Lcom/google/android/gms/drive/database/model/a;)Ljava/util/Date;

    move-result-object v7

    if-nez v7, :cond_0

    invoke-virtual {p2, v9}, Lcom/google/android/gms/drive/api/a/al;->a(Z)V

    .line 900
    :goto_0
    return-void

    .line 899
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    iget-object v1, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iget-object v1, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a(Lcom/google/android/gms/drive/database/model/a;)V

    invoke-virtual {v6, p1, v7}, Lcom/google/android/gms/drive/metadata/sync/b/j;->a(Lcom/google/android/gms/drive/query/Query;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    iget-object v0, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v1, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/a/k;

    invoke-direct {v2, v8}, Lcom/google/android/gms/drive/metadata/sync/a/k;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v9}, Lcom/google/android/gms/drive/api/a/al;->a(Z)V

    goto :goto_0

    :cond_1
    iget-object v2, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/drive/database/model/bb;

    aput-object v0, v1, v9

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    new-instance v5, Landroid/content/SyncResult;

    invoke-direct {v5}, Landroid/content/SyncResult;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;

    iget-object v0, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v4, v0, v2, v5, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/v;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;Landroid/content/SyncResult;Ljava/lang/Boolean;)V

    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/h;

    iget-object v1, v6, Lcom/google/android/gms/drive/metadata/sync/b/j;->a:Lcom/google/android/gms/drive/g/aw;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/metadata/sync/b/h;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/model/a;Ljava/util/List;Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;Landroid/content/SyncResult;)V

    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/b/k;

    invoke-direct {v2, v6, v8, v7, p2}, Lcom/google/android/gms/drive/metadata/sync/b/k;-><init>(Lcom/google/android/gms/drive/metadata/sync/b/j;Ljava/lang/String;Ljava/util/Date;Lcom/google/android/gms/drive/api/a/al;)V

    sget-object v1, Lcom/google/android/gms/drive/ai;->ai:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/drive/metadata/sync/b/f;->a(Lcom/google/android/gms/drive/metadata/sync/b/g;I)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 819
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v1, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    .line 821
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/e;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x67

    .line 823
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    const/4 v3, 0x1

    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;II)I

    move-result v0

    .line 825
    packed-switch v0, :pswitch_data_0

    .line 837
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Internal error while requesting sync."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 821
    :cond_0
    const/16 v0, 0x64

    goto :goto_0

    .line 828
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;Ljava/lang/Runnable;)V

    .line 829
    return-void

    .line 831
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "Sync request rate limit exceeded."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 834
    :pswitch_2
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/4 v1, 0x7

    const-string v2, "Cannot request a sync while the device is offline."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 825
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 1139
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    .line 1140
    if-nez v0, :cond_0

    .line 1141
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Problem determining the application authorization."

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 1144
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 1145
    if-nez v1, :cond_1

    .line 1146
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Assert appIdentity not null failed."

    invoke-direct {v0, v3, v1, v2}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 1149
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 1150
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/a/a/a;->a()Lcom/google/android/gms/drive/a/a/m;

    move-result-object v2

    invoke-virtual {v2, v1, v0, p1}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/a;Ljava/util/List;)V

    .line 1151
    return-void
.end method

.method public final b()Lcom/google/android/gms/drive/c/b;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->z:Lcom/google/android/gms/drive/c/b;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 281
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/api/e;->d(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 282
    if-eqz v0, :cond_1

    .line 305
    :cond_0
    return-object v0

    .line 288
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->h:Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_1

    .line 298
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 301
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->D()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 302
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x5de

    const-string v2, "Drive item not found, or you are not authorized to access it."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 289
    :catch_0
    move-exception v0

    .line 290
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Remote request failed"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 291
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/4 v1, 0x7

    const-string v2, "Failed to retrieve item from network."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 293
    :catch_1
    move-exception v0

    .line 294
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Processing remote result failed"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 295
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to process item."

    invoke-direct {v0, v1, v2, v4}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;I)V
    .locals 6

    .prologue
    .line 878
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->s:Lcom/google/android/gms/drive/events/w;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    :try_start_0
    iget-object v2, v1, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v2, v0}, Lcom/google/android/gms/drive/database/r;->c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-wide v2, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v0

    :goto_0
    iget-object v1, v1, Lcom/google/android/gms/drive/events/w;->a:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1, v2, v3, v0, p2}, Lcom/google/android/gms/drive/database/r;->b(JLcom/google/android/gms/drive/database/model/EntrySpec;I)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to remove subscription: id = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", type = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V
    .locals 4

    .prologue
    .line 866
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->r:Lcom/google/android/gms/drive/events/a;

    iget-object v2, v1, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    iget-object v0, v1, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_0

    new-instance v3, Lcom/google/android/gms/drive/events/c;

    invoke-direct {v3, p2, p3}, Lcom/google/android/gms/drive/events/c;-><init>(ILcom/google/android/gms/drive/internal/cd;)V

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/events/a;->a()V

    .line 867
    return-void

    .line 866
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)V
    .locals 7

    .prologue
    const/16 v6, 0xa

    const/4 v5, 0x0

    .line 539
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 540
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot delete root folder"

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 544
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 545
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->an()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 546
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/e;->d()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    .line 547
    if-nez v1, :cond_1

    .line 548
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Check that your app has access to the App Folder."

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 551
    :cond_1
    invoke-virtual {v1, p1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 552
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot delete App Folder"

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 557
    :cond_2
    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;

    .line 559
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    .line 560
    new-instance v2, Lcom/google/android/gms/drive/a/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v3, v3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v4, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v4, v4, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/gms/drive/a/r;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    .line 562
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->x()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 565
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->v:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 566
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const-string v1, "Cannot delete folders while offline."

    invoke-direct {v0, v6, v1, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 571
    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->c:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/a/r;->a(Lcom/google/android/gms/drive/g/aw;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    .line 574
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    const/16 v2, 0x66

    const/4 v3, 0x4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;II)I
    :try_end_0
    .catch Lcom/google/android/gms/drive/a/h; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/drive/a/i; {:try_start_0 .. :try_end_0} :catch_2

    .line 596
    :cond_4
    return-void

    .line 578
    :catch_0
    move-exception v0

    .line 579
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Failed to delete folder."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/service/h;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 581
    :catch_1
    move-exception v0

    .line 582
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Failed to delete folder."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/service/h;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 584
    :catch_2
    move-exception v0

    .line 585
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Unexpected conflict on delete"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 586
    new-instance v1, Lcom/google/android/gms/common/service/h;

    const-string v2, "Unexpected conflict"

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/service/h;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 590
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/a/a/a;->a(Lcom/google/android/gms/drive/a/c;)I

    move-result v0

    if-eqz v0, :cond_4

    .line 592
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0x8

    const-string v2, "Failed to delete file."

    invoke-direct {v0, v1, v2, v5}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/query/Query;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 894
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->w:Lcom/google/android/gms/drive/metadata/sync/b/j;

    iget-object v2, v1, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/metadata/sync/b/j;->a(Lcom/google/android/gms/drive/database/model/a;)Ljava/util/Date;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {v1, p1, v2}, Lcom/google/android/gms/drive/metadata/sync/b/j;->a(Lcom/google/android/gms/drive/query/Query;Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lcom/google/android/gms/drive/metadata/sync/b/j;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    new-instance v5, Lcom/google/android/gms/drive/metadata/sync/a/k;

    invoke-direct {v5, v3}, Lcom/google/android/gms/drive/metadata/sync/a/k;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-interface {v4, v1, v5, v2, v3}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bb;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/c/a;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 788
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/api/e;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 789
    invoke-interface {p2, v0}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;

    .line 790
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 791
    iget v1, p0, Lcom/google/android/gms/drive/api/e;->p:I

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/ak;->a(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 792
    return-object v0
.end method

.method public final c()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->n:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/drive/DriveId;
    .locals 3

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    const/4 v0, 0x0

    .line 270
    :goto_0
    return-object v0

    .line 260
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->c:Lcom/google/android/gms/drive/g/aw;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    .line 262
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a;->a(Lcom/google/android/gms/drive/auth/g;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 267
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    .line 268
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/q;->a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/database/i;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    goto :goto_0

    .line 263
    :catch_0
    move-exception v0

    .line 264
    const-string v1, "DataServiceConnectionImpl"

    const-string v2, "Failed to get real appData folder from server"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 275
    iget-boolean v0, p0, Lcom/google/android/gms/drive/api/e;->x:Z

    return v0
.end method

.method public final f()Lcom/google/android/gms/drive/auth/g;
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->j:Lcom/google/android/gms/drive/auth/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->k:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0, v1, v7}, Lcom/google/android/gms/drive/auth/f;->a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/drive/auth/d;

    move-result-object v0

    .line 745
    iget-object v1, v0, Lcom/google/android/gms/drive/auth/d;->a:Lcom/google/android/gms/drive/auth/e;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/e;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 746
    iget-object v0, v0, Lcom/google/android/gms/drive/auth/d;->c:Lcom/google/android/gms/drive/auth/c;

    throw v0

    .line 748
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/drive/auth/d;->b:Lcom/google/android/gms/drive/auth/g;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/auth/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 749
    new-instance v1, Lcom/google/android/gms/drive/auth/c;

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "Authorized app changed from %s to %s."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    aput-object v6, v4, v5

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/d;->b:Lcom/google/android/gms/drive/auth/g;

    aput-object v0, v4, v7

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/auth/c;-><init>(Ljava/lang/String;)V

    throw v1

    .line 752
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    return-object v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/drive/api/e;->t()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 763
    sget-object v0, Lcom/google/android/gms/drive/api/e;->b:Ljava/util/Set;

    invoke-direct {p0}, Lcom/google/android/gms/drive/api/e;->t()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 783
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->y:Ljava/lang/String;

    return-object v0
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->r:Lcom/google/android/gms/drive/events/a;

    iget-object v1, v0, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/drive/events/a;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->clear()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/events/a;->a()V

    .line 890
    return-void

    .line 889
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k()Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 905
    :try_start_0
    sget-object v2, Lcom/google/android/gms/drive/api/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 908
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a()Ljava/util/List;

    .line 909
    iget-object v2, p0, Lcom/google/android/gms/drive/api/e;->m:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/drive/api/DriveAsyncService;->a(Landroid/content/Context;)V

    .line 912
    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/i;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/android/gms/drive/database/o;->values()[Lcom/google/android/gms/drive/database/o;

    move-result-object v4

    array-length v5, v4

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    invoke-static {v6}, Lcom/google/android/gms/drive/database/o;->a(Lcom/google/android/gms/drive/database/o;)Lcom/google/android/gms/drive/database/model/ae;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ae;->g()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v6}, Lcom/google/android/gms/drive/database/o;->a(Lcom/google/android/gms/drive/database/o;)Lcom/google/android/gms/drive/database/model/ae;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v3, v6, v7, v8}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    :goto_1
    if-nez v2, :cond_3

    .line 913
    sget-object v2, Lcom/google/android/gms/drive/api/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 920
    :goto_2
    return v0

    :cond_2
    move v2, v0

    .line 912
    goto :goto_1

    .line 915
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->u:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/d;->d()V

    .line 918
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->E()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 920
    sget-object v0, Lcom/google/android/gms/drive/api/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    move v0, v1

    goto :goto_2

    :catchall_0
    move-exception v0

    sget-object v2, Lcom/google/android/gms/drive/api/e;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    throw v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 930
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->g:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/a;->a()Lcom/google/android/gms/drive/a/a/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/a/a/m;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 931
    const/4 v0, 0x1

    .line 933
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 939
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->h()V

    .line 940
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->u:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/d;->d()V

    .line 941
    return-void
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 945
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->i()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 946
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->g()V

    .line 948
    :cond_0
    return-void
.end method

.method public final o()Lcom/google/android/gms/drive/StorageStats;
    .locals 11

    .prologue
    const-wide/16 v2, 0x0

    .line 952
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->i()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/drive/database/f;->a:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sub-long v0, v4, v0

    cmp-long v4, v0, v2

    if-gtz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/drive/api/e;->o:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/i;->h()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 953
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->p()J

    move-result-wide v0

    iget-object v4, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v4}, Lcom/google/android/gms/drive/database/r;->r()J

    move-result-wide v4

    add-long/2addr v4, v0

    .line 955
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->t()J

    move-result-wide v6

    .line 956
    add-long v0, v2, v4

    add-long v8, v0, v6

    .line 957
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->s()I

    move-result v10

    .line 959
    new-instance v1, Lcom/google/android/gms/drive/StorageStats;

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/drive/StorageStats;-><init>(JJJJI)V

    return-object v1

    .line 952
    :cond_0
    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    move-wide v2, v0

    goto :goto_0

    :cond_1
    const-wide/16 v2, 0x1000

    goto :goto_0
.end method

.method public final p()I
    .locals 1

    .prologue
    .line 1104
    iget v0, p0, Lcom/google/android/gms/drive/api/e;->p:I

    return v0
.end method

.method public final q()Lcom/google/android/gms/drive/g/aw;
    .locals 1

    .prologue
    .line 1109
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->c:Lcom/google/android/gms/drive/g/aw;

    return-object v0
.end method

.method public final r()Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;
    .locals 4

    .prologue
    .line 1114
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->e(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/l;

    move-result-object v0

    .line 1116
    new-instance v1, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;

    iget v2, v0, Lcom/google/android/gms/drive/database/model/l;->a:I

    iget v3, v0, Lcom/google/android/gms/drive/database/model/l;->b:I

    iget-boolean v0, v0, Lcom/google/android/gms/drive/database/model/l;->c:Z

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/drive/internal/FileUploadPreferencesImpl;-><init>(IIZ)V

    return-object v1
.end method

.method public final s()V
    .locals 8

    .prologue
    const/16 v1, 0x65

    const/4 v7, 0x2

    .line 798
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/e;->f()Lcom/google/android/gms/drive/auth/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v0, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    .line 802
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    sget-object v3, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-interface {v0, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 803
    iget-object v0, p0, Lcom/google/android/gms/drive/api/e;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/e;->d:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v0, v3}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/i;

    move-result-object v0

    .line 804
    iget-boolean v0, v0, Lcom/google/android/gms/drive/database/model/i;->b:Z

    if-eqz v0, :cond_1

    .line 806
    const/16 v0, 0x66

    .line 810
    :goto_0
    const-string v3, "DataServiceConnectionImpl"

    const-string v4, "Requesting %s initial sync for %s."

    new-array v5, v7, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-ne v0, v1, :cond_0

    const-string v1, "throttled"

    :goto_1
    aput-object v1, v5, v6

    const/4 v1, 0x1

    aput-object v2, v5, v1

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 813
    iget-object v1, p0, Lcom/google/android/gms/drive/api/e;->l:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v1, v2, v0, v7}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a(Ljava/lang/String;II)I

    .line 815
    return-void

    .line 810
    :cond_0
    const-string v1, "unthrottled"

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method
