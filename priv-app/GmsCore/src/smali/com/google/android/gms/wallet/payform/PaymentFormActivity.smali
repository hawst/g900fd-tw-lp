.class public Lcom/google/android/gms/wallet/payform/PaymentFormActivity;
.super Lcom/google/android/gms/wallet/common/ui/ds;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/cq;
.implements Lcom/google/android/gms/wallet/common/ui/de;
.implements Lcom/google/android/gms/wallet/payform/f;


# static fields
.field private static f:Landroid/accounts/Account;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/bb;

.field b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

.field c:Landroid/view/ViewGroup;

.field d:Landroid/widget/CheckBox;

.field e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private g:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

.field private h:Z

.field private i:Landroid/accounts/Account;

.field private j:Ljava/util/HashSet;

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ds;-><init>()V

    .line 89
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    .line 92
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->k:Z

    return-void
.end method

.method public static a(Landroid/content/Context;)Landroid/accounts/Account;
    .locals 3

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->f:Landroid/accounts/Account;

    if-nez v0, :cond_0

    .line 107
    sget v0, Lcom/google/android/gms/p;->Bb:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 108
    new-instance v1, Landroid/accounts/Account;

    const-string v2, "com.google"

    invoke-direct {v1, v0, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v1, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->f:Landroid/accounts/Account;

    .line 111
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->f:Landroid/accounts/Account;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 96
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 98
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 99
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 100
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 102
    return-object v0
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 339
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1, p1}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 341
    return-void
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 409
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 410
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 411
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(ILandroid/content/Intent;)V

    .line 412
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 400
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 402
    return-void
.end method

.method private g()Landroid/support/v4/app/Fragment;
    .locals 2

    .prologue
    .line 344
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method private h()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v5, -0x1

    .line 348
    .line 350
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g()Landroid/support/v4/app/Fragment;

    move-result-object v1

    .line 351
    instance-of v0, v1, Lcom/google/android/gms/wallet/ow/g;

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 352
    check-cast v0, Lcom/google/android/gms/wallet/ow/g;

    .line 353
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->c()I

    move-result v4

    .line 354
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->d()I

    move-result v5

    .line 362
    :goto_0
    if-eqz v1, :cond_0

    .line 363
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 367
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    if-nez v0, :cond_5

    .line 369
    invoke-static {p0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_1
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "PaymentFormActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 378
    :goto_1
    return-void

    .line 355
    :cond_2
    instance-of v0, v1, Lcom/google/android/gms/wallet/payform/a;

    if-eqz v0, :cond_7

    move-object v0, v1

    .line 356
    check-cast v0, Lcom/google/android/gms/wallet/payform/a;

    .line 358
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/a;->a()I

    move-result v4

    .line 359
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/a;->b()I

    move-result v5

    goto :goto_0

    .line 372
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->j:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->j:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    new-array v1, v3, [Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 373
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-static {v0, v1, v2, v4, v5}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;II)Lcom/google/android/gms/wallet/ow/g;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    .line 376
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a()I

    move-result v0

    if-nez v0, :cond_6

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/payform/a;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/accounts/Account;ZII)Lcom/google/android/gms/wallet/payform/a;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto :goto_1

    :cond_6
    move v3, v2

    goto :goto_2

    :cond_7
    move v4, v5

    goto/16 :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 420
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->k:Z

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->c:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b(I)V

    .line 423
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b()V

    .line 425
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 291
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    if-nez v0, :cond_0

    .line 292
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 293
    const/4 v0, 0x7

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(I)V

    .line 300
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    const/16 v0, 0x19b

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 249
    packed-switch p1, :pswitch_data_0

    .line 255
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(ILandroid/content/Intent;)V

    .line 258
    :goto_0
    return-void

    .line 251
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h()V

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    :goto_0
    return-void

    .line 210
    :cond_0
    if-eqz p1, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 212
    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->f:Landroid/accounts/Account;

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 213
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    .line 218
    :goto_2
    iput-object p1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    .line 220
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    if-nez v0, :cond_3

    :goto_3
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(Z)V

    .line 221
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 210
    goto :goto_1

    .line 215
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    goto :goto_2

    :cond_3
    move v1, v2

    .line 220
    goto :goto_3
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/i;Lcom/google/checkout/inapp/proto/j;)V
    .locals 6

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->c:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->h()Ljava/lang/String;

    move-result-object v5

    move-object v2, p2

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/ow/d;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;Lcom/google/aa/b/a/a/a/a/i;ZLjava/lang/String;)Lcom/google/android/gms/wallet/ow/d;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 238
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Z)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 243
    return-void
.end method

.method public final b(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 226
    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->f:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/accounts/Account;)V

    .line 227
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-static {p0}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    .line 228
    array-length v1, v0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->k:Z

    .line 230
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i()V

    .line 232
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 275
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 277
    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 282
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 286
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(ILandroid/content/Intent;)V

    .line 287
    return-void
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 264
    if-nez p2, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    if-nez v0, :cond_1

    .line 265
    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->f:Landroid/accounts/Account;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/accounts/Account;)V

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 266
    :cond_1
    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    if-eqz v0, :cond_0

    .line 267
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-static {p0}, Lcom/google/android/gms/wallet/a/a;->b(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/accounts/Account;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 116
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onCreate(Landroid/os/Bundle;)V

    .line 120
    invoke-static {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "buyFlowConfig"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 129
    sget v0, Lcom/google/android/gms/l;->gh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->setContentView(I)V

    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/Window;)V

    .line 137
    :goto_0
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    .line 138
    sget v0, Lcom/google/android/gms/j;->cM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->c:Landroid/view/ViewGroup;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->c:Landroid/view/ViewGroup;

    sget v2, Lcom/google/android/gms/j;->nv:I

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->d:Landroid/widget/CheckBox;

    .line 142
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->j:Ljava/util/HashSet;

    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "com.google.android.gms.wallet.EXTRA_IMMEDIATE_FULL_WALLET_REQUEST"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    .line 146
    if-eqz p1, :cond_3

    .line 147
    const-string v0, "localMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    .line 148
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    .line 149
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 153
    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->j:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 155
    :cond_0
    const-string v0, "walletModeNotPossible"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->k:Z

    .line 156
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i()V

    .line 169
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 170
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    if-nez v0, :cond_5

    move v0, v1

    :goto_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b(Z)V

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->b:Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/accounts/Account;)V

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->g()Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 174
    if-nez v0, :cond_6

    .line 175
    invoke-direct {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h()V

    .line 179
    :cond_1
    :goto_3
    return-void

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 134
    sget v0, Lcom/google/android/gms/l;->gg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->setContentView(I)V

    goto/16 :goto_0

    .line 158
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    .line 160
    sget-object v0, Lcom/google/android/gms/wallet/w;->a:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-virtual {v0, v2}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 161
    sget-object v0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->f:Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    .line 162
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    .line 166
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->e:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "payment_form_activity"

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 170
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 176
    :cond_6
    instance-of v0, v0, Lcom/google/android/gms/wallet/ow/d;

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->c:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_3
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onResume()V

    .line 184
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "PaymentFormActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 189
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 193
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 195
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->i:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 196
    const-string v0, "accountsThatHaveRequestedAuthTokens"

    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->j:Ljava/util/HashSet;

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 199
    const-string v0, "localMode"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 200
    const-string v0, "walletModeNotPossible"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 201
    return-void
.end method
