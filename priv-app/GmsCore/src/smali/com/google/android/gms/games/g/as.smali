.class public final Lcom/google/android/gms/games/g/as;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:I

.field public d:J

.field public e:[Lcom/google/android/gms/games/g/at;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 8332
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8333
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/games/g/as;->b:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/as;->c:I

    iput-wide v2, p0, Lcom/google/android/gms/games/g/as;->d:J

    invoke-static {}, Lcom/google/android/gms/games/g/at;->a()[Lcom/google/android/gms/games/g/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/as;->cachedSize:I

    .line 8334
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 8421
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8422
    iget-object v1, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 8423
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8426
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 8427
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8430
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/as;->c:I

    if-eqz v1, :cond_2

    .line 8431
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/as;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8434
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 8435
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8438
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 8439
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 8440
    iget-object v2, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    aget-object v2, v2, v0

    .line 8441
    if-eqz v2, :cond_4

    .line 8442
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 8439
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 8447
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8348
    if-ne p1, p0, :cond_1

    .line 8375
    :cond_0
    :goto_0
    return v0

    .line 8351
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/as;

    if-nez v2, :cond_2

    move v0, v1

    .line 8352
    goto :goto_0

    .line 8354
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/as;

    .line 8355
    iget-object v2, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 8356
    iget-object v2, p1, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 8357
    goto :goto_0

    .line 8359
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 8360
    goto :goto_0

    .line 8362
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/as;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 8363
    goto :goto_0

    .line 8365
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/g/as;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/as;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 8366
    goto :goto_0

    .line 8368
    :cond_6
    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->d:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/as;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 8369
    goto :goto_0

    .line 8371
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    iget-object v3, p1, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 8373
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 8380
    iget-object v0, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 8383
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/games/g/as;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 8385
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/as;->c:I

    add-int/2addr v0, v1

    .line 8386
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/games/g/as;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 8388
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8390
    return v0

    .line 8380
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8166
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/games/g/as;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/as;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/games/g/as;->d:J

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/games/g/at;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/games/g/at;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/at;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/games/g/at;

    invoke-direct {v3}, Lcom/google/android/gms/games/g/at;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 8396
    iget-object v0, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8397
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8399
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/g/as;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 8400
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 8402
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/as;->c:I

    if-eqz v0, :cond_2

    .line 8403
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/as;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8405
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/games/g/as;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 8406
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/games/g/as;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 8408
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 8409
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 8410
    iget-object v1, p0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    aget-object v1, v1, v0

    .line 8411
    if-eqz v1, :cond_4

    .line 8412
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8409
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8416
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8417
    return-void
.end method
