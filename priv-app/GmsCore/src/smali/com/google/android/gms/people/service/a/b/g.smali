.class public final Lcom/google/android/gms/people/service/a/b/g;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 20
    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 7

    .prologue
    .line 26
    new-instance v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;

    invoke-direct {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;-><init>()V

    const-string v0, "circles.firstTimeAdd.needConsent"

    iput-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->c:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->f:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-boolean v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->b:Z

    iget-object v0, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->f:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->f:Ljava/util/Set;

    iget-object v2, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-boolean v3, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->b:Z

    iget-object v4, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->c:Ljava/lang/String;

    iget v5, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->d:I

    iget-object v6, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/ff;->e:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;ZLjava/lang/String;ILjava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;

    .line 30
    const-string v1, "circles.firstTimeAdd.needConsent"

    invoke-virtual {p2, p3, v1, v0}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/SettingEntity;)V

    .line 32
    sget-object v0, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
