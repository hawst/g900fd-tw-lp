.class public final Lcom/google/android/gms/plus/sharebox/n;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/android/gms/plus/sharebox/o;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 30
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/n;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 45
    const-string v1, "title"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    const-string v1, "message"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    new-instance v1, Lcom/google/android/gms/plus/sharebox/n;

    invoke-direct {v1}, Lcom/google/android/gms/plus/sharebox/n;-><init>()V

    .line 48
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/n;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/sharebox/o;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/n;->l:Lcom/google/android/gms/plus/sharebox/o;

    .line 54
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 66
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/n;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/q;->C:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/n;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 69
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/n;->j:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/n;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040013

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x1040009

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/n;->l:Lcom/google/android/gms/plus/sharebox/o;

    if-eqz v0, :cond_0

    .line 81
    packed-switch p2, :pswitch_data_0

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 83
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/n;->l:Lcom/google/android/gms/plus/sharebox/o;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/o;->b()V

    goto :goto_0

    .line 86
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/n;->l:Lcom/google/android/gms/plus/sharebox/o;

    goto :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 58
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/n;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 60
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/n;->j:Ljava/lang/String;

    .line 61
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/n;->k:Ljava/lang/String;

    .line 62
    return-void
.end method
