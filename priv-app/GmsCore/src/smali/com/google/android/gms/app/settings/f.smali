.class final Lcom/google/android/gms/app/settings/f;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/gms/app/settings/f;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;B)V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0, p1}, Lcom/google/android/gms/app/settings/f;-><init>(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 177
    new-instance v0, Lcom/google/android/gms/common/api/w;

    iget-object v1, p0, Lcom/google/android/gms/app/settings/f;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/wearable/z;->g:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    const-wide/16 v2, 0x5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/api/v;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    if-eq v1, v2, :cond_0

    const-string v0, "ManageSpaceActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to connect to Google Play Services: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/common/c;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v1, Lcom/google/android/gms/wearable/z;->a:Lcom/google/android/gms/wearable/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wearable/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/g;

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 177
    check-cast p1, Lcom/google/android/gms/wearable/g;

    const-wide/16 v0, 0x0

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/wearable/g;->b()Lcom/google/android/gms/wearable/internal/StorageInfoResponse;

    move-result-object v0

    iget-wide v0, v0, Lcom/google/android/gms/wearable/internal/StorageInfoResponse;->c:J

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/app/settings/f;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v2}, Lcom/google/android/gms/app/settings/ManageSpaceActivity;->d(Lcom/google/android/gms/app/settings/ManageSpaceActivity;)Landroid/widget/TextView;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/app/settings/f;->a:Lcom/google/android/gms/app/settings/ManageSpaceActivity;

    invoke-static {v3, v0, v1}, Landroid/text/format/Formatter;->formatFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
