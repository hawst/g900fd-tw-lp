.class final Lcom/google/android/gms/cast/c/ar;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/cast/internal/k;


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "MdnsResponseCache"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/c/ar;->a:Lcom/google/android/gms/cast/internal/k;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/16 v0, 0x20

    .line 23
    invoke-direct {p0, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    .line 24
    iput v0, p0, Lcom/google/android/gms/cast/c/ar;->b:I

    .line 25
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 8

    .prologue
    .line 39
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/ar;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 42
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 43
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aq;

    .line 44
    int-to-long v4, p1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/gms/cast/c/aq;->a(JJ)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 45
    sget-object v4, Lcom/google/android/gms/cast/c/ar;->a:Lcom/google/android/gms/cast/internal/k;

    const-string v5, "removing expired response: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/aq;->f()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/c/ap;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 47
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 50
    :cond_1
    return-void
.end method

.method protected final removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/ar;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/cast/c/ar;->b:I

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
