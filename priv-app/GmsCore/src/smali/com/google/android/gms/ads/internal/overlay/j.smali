.class public final Lcom/google/android/gms/ads/internal/overlay/j;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/ads/internal/p/a;

.field private final b:Landroid/widget/MediaController;

.field private final c:Lcom/google/android/gms/ads/internal/overlay/k;

.field private final d:Landroid/widget/VideoView;

.field private e:J

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/p/a;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 142
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 143
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/overlay/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    .line 146
    new-instance v0, Landroid/widget/VideoView;

    invoke-direct {v0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    .line 147
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v1, 0x11

    invoke-direct {v0, v2, v2, v1}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 151
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/ads/internal/overlay/j;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 154
    new-instance v0, Landroid/widget/MediaController;

    invoke-direct {v0, p1}, Landroid/widget/MediaController;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->b:Landroid/widget/MediaController;

    .line 157
    new-instance v0, Lcom/google/android/gms/ads/internal/overlay/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/overlay/k;-><init>(Lcom/google/android/gms/ads/internal/overlay/j;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->c:Lcom/google/android/gms/ads/internal/overlay/k;

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->c:Lcom/google/android/gms/ads/internal/overlay/k;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/overlay/k;->a()V

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0, p0}, Landroid/widget/VideoView;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 164
    return-void
.end method

.method public static a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 94
    if-nez p2, :cond_1

    const/4 v0, 0x1

    .line 95
    :goto_0
    new-instance v2, Ljava/util/HashMap;

    if-eqz v0, :cond_2

    const/4 v1, 0x2

    :goto_1
    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 96
    const-string v1, "what"

    invoke-virtual {v2, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    if-nez v0, :cond_0

    .line 98
    const-string v0, "extra"

    invoke-virtual {v2, v0, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    :cond_0
    const-string v0, "error"

    invoke-static {p0, v0, v2}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/util/Map;)V

    .line 101
    return-void

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 95
    :cond_2
    const/4 v1, 0x3

    goto :goto_1
.end method

.method private static a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 119
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 120
    invoke-virtual {v0, p2, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/util/Map;)V

    .line 122
    return-void
.end method

.method private static a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 129
    const-string v0, "event"

    invoke-interface {p2, v0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    const-string v0, "onVideoEvent"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/ads/internal/p/a;->a(Ljava/lang/String;Ljava/util/Map;)V

    .line 131
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->c:Lcom/google/android/gms/ads/internal/overlay/k;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/ads/internal/overlay/k;->b:Z

    sget-object v1, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/overlay/k;->a:Ljava/lang/Runnable;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->stopPlayback()V

    .line 169
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->seekTo(I)V

    .line 211
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0, p1}, Landroid/widget/VideoView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 215
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/overlay/j;->f:Ljava/lang/String;

    .line 228
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 218
    if-eqz p1, :cond_0

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/j;->b:Landroid/widget/MediaController;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    .line 224
    :goto_0
    return-void

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->b:Landroid/widget/MediaController;

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setMediaController(Landroid/widget/MediaController;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/j;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/VideoView;->setVideoPath(Ljava/lang/String;)V

    .line 181
    :goto_0
    return-void

    .line 179
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v1, "no_src"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->pause()V

    .line 203
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->start()V

    .line 207
    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 231
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getCurrentPosition()I

    move-result v0

    int-to-long v0, v0

    .line 232
    iget-wide v2, p0, Lcom/google/android/gms/ads/internal/overlay/j;->e:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    .line 233
    long-to-float v2, v0

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v2, v3

    .line 234
    iget-object v3, p0, Lcom/google/android/gms/ads/internal/overlay/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v4, "timeupdate"

    const-string v5, "time"

    invoke-static {v2}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v5, v2}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 235
    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->e:J

    .line 237
    :cond_0
    return-void
.end method

.method public final onCompletion(Landroid/media/MediaPlayer;)V
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v1, "ended"

    new-instance v2, Ljava/util/HashMap;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/HashMap;-><init>(I)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/util/Map;)V

    .line 186
    return-void
.end method

.method public final onError(Landroid/media/MediaPlayer;II)Z
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public final onPrepared(Landroid/media/MediaPlayer;)V
    .locals 4

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/overlay/j;->d:Landroid/widget/VideoView;

    invoke-virtual {v0}, Landroid/widget/VideoView;->getDuration()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v0, v1

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/overlay/j;->a:Lcom/google/android/gms/ads/internal/p/a;

    const-string v2, "canplaythrough"

    const-string v3, "duration"

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    return-void
.end method
