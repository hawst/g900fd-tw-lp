.class public final Lcom/google/android/gms/common/ui/ErrorDialogActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private a:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->setResult(I)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->finish()V

    .line 76
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 68
    iget v0, p0, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->setResult(I)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->finish()V

    .line 70
    return-void
.end method

.method public final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 41
    if-ne p1, v4, :cond_2

    .line 42
    const-string v0, "com.google.android.gms.common.ui.EXTRA_DIALOG_TITLE"

    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 43
    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_MESSAGE"

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 44
    const-string v2, "com.google.android.gms.common.ui.EXTRA_DIALOG_RESULT_CODE"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->a:I

    .line 46
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x104000a

    invoke-virtual {v2, v3, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 49
    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 52
    :cond_0
    if-eqz v1, :cond_1

    .line 53
    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 55
    :cond_1
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 59
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 63
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final onPause()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 36
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->removeDialog(I)V

    .line 37
    return-void
.end method

.method protected final onResume()V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 30
    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/ui/ErrorDialogActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 31
    return-void
.end method
