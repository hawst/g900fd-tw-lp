.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/aq;

.field private static final e:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 2090
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/aq;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/aq;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/aq;

    .line 2106
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2109
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->e:Ljava/util/HashMap;

    const-string v1, "date"

    const-string v2, "date"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2110
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->e:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2113
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2145
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 2146
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->b:I

    .line 2147
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a:Ljava/util/Set;

    .line 2148
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;)V
    .locals 0

    .prologue
    .line 2156
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 2157
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a:Ljava/util/Set;

    .line 2158
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->b:I

    .line 2159
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->c:Ljava/lang/String;

    .line 2160
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 2161
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 2117
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->e:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 2267
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2268
    packed-switch v0, :pswitch_data_0

    .line 2273
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2270
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 2277
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2278
    return-void

    .line 2268
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 2252
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2253
    packed-switch v0, :pswitch_data_0

    .line 2258
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2255
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->c:Ljava/lang/String;

    .line 2261
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2262
    return-void

    .line 2253
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 2221
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2226
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2232
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2228
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->c:Ljava/lang/String;

    .line 2230
    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->d:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    goto :goto_0

    .line 2226
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2211
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/aq;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2295
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;

    if-nez v0, :cond_0

    move v0, v1

    .line 2326
    :goto_0
    return v0

    .line 2300
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 2301
    goto :goto_0

    .line 2304
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;

    .line 2305
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2306
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2307
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2309
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2311
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2316
    goto :goto_0

    .line 2319
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2321
    goto :goto_0

    :cond_5
    move v0, v2

    .line 2326
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2282
    const/4 v0, 0x0

    .line 2283
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2284
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2285
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 2286
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 2288
    goto :goto_0

    .line 2289
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2216
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/aq;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/aq;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Birthdays;Landroid/os/Parcel;I)V

    .line 2217
    return-void
.end method
