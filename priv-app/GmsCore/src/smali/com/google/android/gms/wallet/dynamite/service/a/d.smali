.class final Lcom/google/android/gms/wallet/dynamite/service/a/d;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/dynamite/service/a/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/dynamite/service/a/c;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x5

    const/4 v2, 0x0

    .line 179
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 180
    iget v0, p1, Landroid/os/Message;->what:I

    if-lt v0, v3, :cond_1

    .line 181
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 183
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a:Lcom/google/android/gms/common/internal/a/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v1, v1, Lcom/google/android/gms/wallet/dynamite/service/a/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/a/b;->c(Ljava/lang/Object;)Ljava/lang/Object;

    .line 184
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_0

    .line 185
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v1, v1, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c:[Lcom/google/android/gms/wallet/dynamite/service/a/g;

    aput-object v2, v1, v0

    .line 186
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v1, v1, Lcom/google/android/gms/wallet/dynamite/service/a/c;->d:[Landroid/os/Message;

    aput-object v2, v1, v0

    .line 187
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iput-object v2, v1, Lcom/google/android/gms/wallet/dynamite/service/a/c;->f:Ljava/lang/ref/WeakReference;

    .line 184
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 195
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->d:[Landroid/os/Message;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/service/a/d;->a:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->f:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/dynamite/service/a/a;

    .line 199
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 201
    :pswitch_1
    iget v1, p1, Landroid/os/Message;->arg1:I

    iget v1, p1, Landroid/os/Message;->arg2:I

    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/dynamite/service/a/a;->a(I)V

    goto :goto_0

    .line 205
    :pswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/util/Pair;

    .line 207
    iget v3, p1, Landroid/os/Message;->arg1:I

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/wallet/MaskedWallet;

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/os/Bundle;

    invoke-interface {v0, v3, v2, v1}, Lcom/google/android/gms/wallet/dynamite/service/a/a;->a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    goto :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch

    .line 199
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method
