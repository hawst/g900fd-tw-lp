.class public final Lcom/google/android/gms/wallet/common/af;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "\u2063"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/af;->a:Ljava/util/regex/Pattern;

    .line 28
    const-string v0, "\u203d"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/af;->b:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ag;
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/wallet/common/ag;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/wallet/common/ag;-><init>(Ljava/lang/String;B)V

    return-object v0
.end method

.method public static a()Lcom/google/android/gms/wallet/common/ah;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/wallet/common/ah;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/common/ah;-><init>(B)V

    return-object v0
.end method

.method public static synthetic a(Ljava/lang/String;[Ljava/lang/Object;Z)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 20
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    array-length v5, p1

    move v3, v0

    move v1, v0

    :goto_0
    if-ge v3, v5, :cond_5

    aget-object v0, p1, v3

    if-nez v0, :cond_2

    const-string v0, ""

    move-object v2, v0

    :goto_1
    if-eqz p2, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-eqz v1, :cond_4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    :goto_2
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v1, v0

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    instance-of v2, v0, Lcom/google/protobuf/nano/j;

    if-eqz v2, :cond_3

    check-cast v0, Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->d(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    goto :goto_1

    :cond_4
    const/4 v0, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/wallet/common/af;->a:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method public static synthetic c()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/wallet/common/af;->b:Ljava/util/regex/Pattern;

    return-object v0
.end method
