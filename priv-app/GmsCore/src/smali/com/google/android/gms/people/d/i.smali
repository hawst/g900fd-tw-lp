.class public final Lcom/google/android/gms/people/d/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 673
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 674
    iput-wide v0, p0, Lcom/google/android/gms/people/d/i;->a:J

    iput-wide v0, p0, Lcom/google/android/gms/people/d/i;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/i;->cachedSize:I

    .line 675
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 726
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 727
    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 728
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 731
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 732
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->b:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 735
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 686
    if-ne p1, p0, :cond_1

    .line 699
    :cond_0
    :goto_0
    return v0

    .line 689
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 690
    goto :goto_0

    .line 692
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/i;

    .line 693
    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/i;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 694
    goto :goto_0

    .line 696
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/people/d/i;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 697
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 704
    iget-wide v0, p0, Lcom/google/android/gms/people/d/i;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 707
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/people/d/i;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 709
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 650
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/i;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/d/i;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 715
    iget-wide v0, p0, Lcom/google/android/gms/people/d/i;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 716
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 718
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/people/d/i;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 719
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/people/d/i;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 721
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 722
    return-void
.end method
