.class final Lcom/google/android/gms/blescanner/compat/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/bluetooth/BluetoothAdapter$LeScanCallback;


# instance fields
.field final synthetic a:Lcom/google/android/gms/blescanner/compat/k;


# direct methods
.method constructor <init>(Lcom/google/android/gms/blescanner/compat/k;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/gms/blescanner/compat/l;->a:Lcom/google/android/gms/blescanner/compat/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onLeScan(Landroid/bluetooth/BluetoothDevice;I[B)V
    .locals 8

    .prologue
    .line 154
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/l;->a:Lcom/google/android/gms/blescanner/compat/k;

    iget-object v1, v1, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v1}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/l;->a:Lcom/google/android/gms/blescanner/compat/k;

    iget-object v6, v0, Lcom/google/android/gms/blescanner/compat/k;->f:Ljava/util/List;

    monitor-enter v6

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/l;->a:Lcom/google/android/gms/blescanner/compat/k;

    iget-object v7, v0, Lcom/google/android/gms/blescanner/compat/k;->f:Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/blescanner/compat/p;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/blescanner/compat/p;-><init>(Landroid/bluetooth/BluetoothDevice;I[BJ)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method
