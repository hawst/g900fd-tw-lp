.class public final Lcom/google/android/gms/drive/realtime/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ExecutorService;

.field private static final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private static final c:Lcom/google/c/a/a/b/e/n;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/drive/realtime/cache/o;

.field private final f:Lcom/google/android/gms/drive/realtime/cache/i;

.field private final g:Lcom/google/android/gms/drive/realtime/cache/w;

.field private final h:Lcom/google/android/gms/drive/g/n;

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/realtime/f;->a:Ljava/util/concurrent/ExecutorService;

    .line 60
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/realtime/f;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 62
    new-instance v0, Lcom/google/android/gms/drive/realtime/g;

    invoke-direct {v0}, Lcom/google/android/gms/drive/realtime/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/realtime/f;->c:Lcom/google/c/a/a/b/e/n;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/realtime/cache/w;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/f;->d:Landroid/content/Context;

    .line 83
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/f;->e:Lcom/google/android/gms/drive/realtime/cache/o;

    .line 84
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/f;->f:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/drive/realtime/f;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/realtime/f;->i:I

    .line 86
    iput-object p4, p0, Lcom/google/android/gms/drive/realtime/f;->h:Lcom/google/android/gms/drive/g/n;

    .line 87
    iput-object p5, p0, Lcom/google/android/gms/drive/realtime/f;->g:Lcom/google/android/gms/drive/realtime/cache/w;

    .line 88
    return-void
.end method

.method static synthetic a()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/gms/drive/realtime/f;->b:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/drive/realtime/e;Lcom/google/c/a/a/a/a/a;Lcom/google/c/a/a/b/e/c;Lcom/google/android/gms/drive/realtime/j;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 228
    const/4 v0, 0x0

    .line 229
    if-eqz p5, :cond_0

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/f;->e:Lcom/google/android/gms/drive/realtime/cache/o;

    const-string v1, "RealtimeLoader"

    invoke-virtual {v0, p1, v1, v6}, Lcom/google/android/gms/drive/realtime/cache/o;->a(Lcom/google/android/gms/drive/realtime/e;Ljava/lang/String;Z)Lcom/google/android/gms/drive/realtime/cache/n;

    move-result-object v2

    .line 232
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/f;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/f;->f:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v4, p0, Lcom/google/android/gms/drive/realtime/f;->e:Lcom/google/android/gms/drive/realtime/cache/o;

    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/f;->g:Lcom/google/android/gms/drive/realtime/cache/w;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/realtime/cache/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/w;)V

    .line 235
    :cond_0
    new-instance v1, Lcom/google/android/gms/drive/realtime/i;

    invoke-direct {v1, p0, p4, p2, p1}, Lcom/google/android/gms/drive/realtime/i;-><init>(Lcom/google/android/gms/drive/realtime/f;Lcom/google/android/gms/drive/realtime/j;Lcom/google/c/a/a/a/a/a;Lcom/google/android/gms/drive/realtime/e;)V

    iput-object p3, p2, Lcom/google/c/a/a/a/a/a;->d:Lcom/google/c/a/a/b/e/c;

    iput-object v0, p2, Lcom/google/c/a/a/a/a/a;->c:Lcom/google/c/a/a/b/a/e;

    iget-object v0, p2, Lcom/google/c/a/a/a/a/a;->a:Lcom/google/c/a/a/b/e/e;

    new-instance v2, Lcom/google/c/a/a/a/a/b;

    invoke-direct {v2, p2, v1}, Lcom/google/c/a/a/a/a/b;-><init>(Lcom/google/c/a/a/a/a/a;Lcom/google/c/a/a/a/a/d;)V

    invoke-interface {v0, v2, v6}, Lcom/google/c/a/a/b/e/e;->a(Lcom/google/c/a/a/b/e/f;Z)V

    .line 252
    return-void
.end method

.method private b()I
    .locals 3

    .prologue
    .line 96
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/f;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/f;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;Lcom/google/c/a/a/a/c/u;Lcom/google/android/gms/drive/realtime/j;ZI)V
    .locals 12

    .prologue
    .line 122
    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    sget-object v1, Lcom/google/android/gms/drive/ai;->P:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move/from16 v0, p6

    if-ge v0, v1, :cond_0

    .line 125
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0xd

    const-string v3, "Client version  is no longer supported, update to a more recent version."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 189
    :goto_0
    return-void

    .line 129
    :cond_0
    iget v2, p0, Lcom/google/android/gms/drive/realtime/f;->i:I

    sget-object v1, Lcom/google/android/gms/drive/ai;->O:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v2, v1, :cond_1

    .line 130
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/f;->d:Landroid/content/Context;

    const/4 v2, 0x2

    const-string v3, "com.google.android.gms"

    invoke-static {v3}, Lcom/google/android/gms/common/internal/ay;->c(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-static {v1, v2, v3, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 135
    new-instance v2, Lcom/google/android/gms/common/api/Status;

    const/4 v3, 0x2

    const-string v4, "The installed version of Google Play Services is out of date and cannotopen this document. Update to a more recent version."

    invoke-direct {v2, v3, v4, v1}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    move-object/from16 v0, p4

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    .line 141
    :cond_1
    new-instance v11, Lcom/google/android/gms/drive/realtime/e;

    invoke-direct {v11, p1, p2}, Lcom/google/android/gms/drive/realtime/e;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;)V

    .line 143
    const-string v1, "RealtimeLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loading file "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    new-instance v10, Lcom/google/android/gms/drive/realtime/l;

    invoke-direct {v10}, Lcom/google/android/gms/drive/realtime/l;-><init>()V

    .line 145
    new-instance v9, Lcom/google/c/a/a/a/c/a/e/i;

    sget-object v1, Lcom/google/android/gms/drive/realtime/f;->b:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-direct {v9, v1}, Lcom/google/c/a/a/a/c/a/e/i;-><init>(Ljava/util/concurrent/ScheduledExecutorService;)V

    .line 148
    if-eqz p5, :cond_2

    .line 149
    new-instance v1, Lcom/google/c/a/a/a/c/t;

    invoke-direct {v1}, Lcom/google/c/a/a/a/c/t;-><init>()V

    .line 158
    :goto_1
    new-instance v2, Lcom/google/c/a/a/a/c/j;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3, v9}, Lcom/google/c/a/a/a/c/j;-><init>(Lcom/google/c/a/a/a/c/i;Ljava/lang/String;Lcom/google/c/a/a/a/c/a/e/b;)V

    .line 161
    new-instance v8, Lcom/google/c/a/a/a/a/a;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/realtime/f;->c:Lcom/google/c/a/a/b/e/n;

    invoke-direct {v8, v3, v1, v2, v4}, Lcom/google/c/a/a/a/a/a;-><init>(Ljava/util/Set;Lcom/google/c/a/a/a/c/i;Lcom/google/c/a/a/b/e/e;Lcom/google/c/a/a/b/e/n;)V

    .line 165
    new-instance v9, Lcom/google/android/gms/drive/realtime/h;

    move-object/from16 v0, p4

    invoke-direct {v9, p0, v0}, Lcom/google/android/gms/drive/realtime/h;-><init>(Lcom/google/android/gms/drive/realtime/f;Lcom/google/android/gms/drive/realtime/j;)V

    .line 173
    sget-object v1, Lcom/google/android/gms/drive/ai;->N:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/f;->e:Lcom/google/android/gms/drive/realtime/cache/o;

    invoke-virtual {v1, v11}, Lcom/google/android/gms/drive/realtime/cache/o;->a(Lcom/google/android/gms/drive/realtime/e;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/f;->e:Lcom/google/android/gms/drive/realtime/cache/o;

    const-string v2, "RealtimeLoader"

    const/4 v3, 0x1

    invoke-virtual {v1, v11, v2, v3}, Lcom/google/android/gms/drive/realtime/cache/o;->a(Lcom/google/android/gms/drive/realtime/e;Ljava/lang/String;Z)Lcom/google/android/gms/drive/realtime/cache/n;

    move-result-object v3

    if-nez v3, :cond_3

    :try_start_0
    const-string v1, "RealtimeLoader"

    const-string v2, "Someone already has the cache open in the background, can\'t open now."

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    const-string v4, "Someone already has the cache open in the background, can\'t open now."

    const/4 v5, 0x0

    invoke-direct {v1, v2, v4, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v3}, Lcom/google/android/gms/drive/realtime/cache/n;->h()V

    const-string v1, "RealtimeLoader"

    const-string v2, "Loading from cache failed.  Falling back to the network."

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, v11

    move-object v3, v8

    move-object v4, v9

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/drive/realtime/f;->a(Lcom/google/android/gms/drive/realtime/e;Lcom/google/c/a/a/a/a/a;Lcom/google/c/a/a/b/e/c;Lcom/google/android/gms/drive/realtime/j;Z)V

    goto/16 :goto_0

    .line 151
    :cond_2
    new-instance v1, Lcom/google/c/a/a/a/c/a;

    new-instance v2, Lcom/google/android/gms/drive/realtime/c;

    invoke-direct {v2}, Lcom/google/android/gms/drive/realtime/c;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/realtime/k;

    move-object/from16 v0, p4

    invoke-direct {v3, v0}, Lcom/google/android/gms/drive/realtime/k;-><init>(Lcom/google/android/gms/drive/realtime/j;)V

    sget-object v4, Lcom/google/android/gms/drive/ai;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string v6, "android"

    invoke-static/range {p6 .. p6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcom/google/c/a/a/a/c/a/d/x;

    sget-object v4, Lcom/google/android/gms/drive/realtime/f;->a:Ljava/util/concurrent/ExecutorService;

    invoke-direct {v8, v4}, Lcom/google/c/a/a/a/c/a/d/x;-><init>(Ljava/util/concurrent/ExecutorService;)V

    move-object v4, p3

    invoke-direct/range {v1 .. v10}, Lcom/google/c/a/a/a/c/a;-><init>(Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/a/c/s;Lcom/google/c/a/a/a/c/u;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/a/c/a/d/k;Lcom/google/c/a/a/a/c/a/e/b;Lcom/google/c/a/a/a/c/a/e/f;)V

    goto/16 :goto_1

    .line 175
    :cond_3
    :try_start_1
    invoke-virtual {v3}, Lcom/google/android/gms/drive/realtime/cache/n;->b()Lcom/google/c/a/a/b/a/d;

    move-result-object v7

    new-instance v1, Lcom/google/android/gms/drive/realtime/cache/d;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/f;->d:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/drive/realtime/f;->f:Lcom/google/android/gms/drive/realtime/cache/i;

    iget-object v5, p0, Lcom/google/android/gms/drive/realtime/f;->e:Lcom/google/android/gms/drive/realtime/cache/o;

    iget-object v6, p0, Lcom/google/android/gms/drive/realtime/f;->g:Lcom/google/android/gms/drive/realtime/cache/w;

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/drive/realtime/cache/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/w;)V

    iput-object v9, v8, Lcom/google/c/a/a/a/a/a;->d:Lcom/google/c/a/a/b/e/c;

    instance-of v2, v7, Lcom/google/c/a/a/b/a/a;

    if-eqz v2, :cond_4

    move-object v0, v7

    check-cast v0, Lcom/google/c/a/a/b/a/a;

    move-object v2, v0

    iget-object v4, v2, Lcom/google/c/a/a/b/a/d;->c:Lcom/google/c/a/a/b/c/f;

    iget-object v5, v2, Lcom/google/c/a/a/b/a/a;->a:Ljava/util/List;

    iget-object v2, v2, Lcom/google/c/a/a/b/a/a;->b:Ljava/util/List;

    invoke-virtual {v8, v4, v5, v2}, Lcom/google/c/a/a/a/a/a;->a(Lcom/google/c/a/a/b/c/f;Ljava/util/List;Ljava/util/List;)V

    :goto_2
    iget-object v2, v8, Lcom/google/c/a/a/a/a/a;->e:Lcom/google/c/a/a/b/e/i;

    iget-object v4, v7, Lcom/google/c/a/a/b/a/d;->d:Lcom/google/c/a/a/b/a/f;

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v1, v5}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/a/f;Lcom/google/c/a/a/b/a/e;Z)V

    iput-object v1, v8, Lcom/google/c/a/a/a/a/a;->c:Lcom/google/c/a/a/b/a/e;

    iget-object v1, v11, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    move-object/from16 v0, p4

    invoke-interface {v0, v8, v1}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/c/a/a/a/a/a;Lcom/google/android/gms/drive/DriveId;)V

    goto/16 :goto_0

    :cond_4
    iget-object v2, v7, Lcom/google/c/a/a/b/a/d;->c:Lcom/google/c/a/a/b/c/f;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v8, v2, v4, v5}, Lcom/google/c/a/a/a/a/a;->a(Lcom/google/c/a/a/b/c/f;Ljava/util/List;Ljava/util/List;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 177
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/f;->h:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v1

    if-nez v1, :cond_6

    .line 179
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x7

    const-string v3, "Document is unavailable offline. Try loading again online."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    move-object/from16 v0, p4

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/realtime/j;->a(Lcom/google/android/gms/common/api/Status;)V

    goto/16 :goto_0

    .line 184
    :cond_6
    sget-object v1, Lcom/google/android/gms/drive/ai;->N:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    move-object v1, p0

    move-object v2, v11

    move-object v3, v8

    move-object v4, v9

    move-object/from16 v5, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/drive/realtime/f;->a(Lcom/google/android/gms/drive/realtime/e;Lcom/google/c/a/a/a/a/a;Lcom/google/c/a/a/b/e/c;Lcom/google/android/gms/drive/realtime/j;Z)V

    goto/16 :goto_0
.end method
