.class public final Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# static fields
.field public static final CREATOR:Lcom/google/android/gms/ads/internal/client/b;


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:I

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/ads/internal/client/b;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/client/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->CREATOR:Lcom/google/android/gms/ads/internal/client/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 114
    const/4 v1, 0x2

    const-string v2, "interstitial_mb"

    const/4 v5, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move v4, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(ILjava/lang/String;IIZII[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 122
    return-void
.end method

.method constructor <init>(ILjava/lang/String;IIZII[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    .locals 0

    .prologue
    .line 205
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 206
    iput p1, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->a:I

    .line 207
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->b:Ljava/lang/String;

    .line 208
    iput p3, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    .line 209
    iput p4, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    .line 210
    iput-boolean p5, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    .line 211
    iput p6, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    .line 212
    iput p7, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    .line 213
    iput-object p8, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 214
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/b;)V
    .locals 2

    .prologue
    .line 128
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/ads/b;

    const/4 v1, 0x0

    aput-object p2, v0, v1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(Landroid/content/Context;[Lcom/google/android/gms/ads/b;)V

    .line 129
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;[Lcom/google/android/gms/ads/b;)V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 135
    aget-object v6, p2, v2

    .line 137
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->a:I

    .line 138
    iput-boolean v2, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    .line 139
    invoke-virtual {v6}, Lcom/google/android/gms/ads/b;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    .line 140
    invoke-virtual {v6}, Lcom/google/android/gms/ads/b;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    .line 142
    iget v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    const/4 v3, -0x1

    if-ne v0, v3, :cond_1

    move v0, v1

    .line 143
    :goto_0
    iget v3, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    const/4 v4, -0x2

    if-ne v3, v4, :cond_2

    move v3, v1

    .line 144
    :goto_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v7

    .line 149
    if-eqz v0, :cond_3

    .line 150
    iget v4, v7, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    .line 151
    iget v4, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    int-to-float v4, v4

    iget v5, v7, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v4, v5

    float-to-int v4, v4

    move v5, v4

    .line 157
    :goto_2
    if-eqz v3, :cond_6

    .line 158
    iget v4, v7, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v4, v4

    iget v8, v7, Landroid/util/DisplayMetrics;->density:F

    div-float/2addr v4, v8

    float-to-int v4, v4

    const/16 v8, 0x190

    if-gt v4, v8, :cond_4

    const/16 v4, 0x20

    .line 163
    :goto_3
    invoke-static {v7, v4}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/util/DisplayMetrics;I)I

    move-result v7

    iput v7, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    .line 165
    if-nez v0, :cond_0

    if-eqz v3, :cond_7

    .line 166
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "x"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_as"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->b:Ljava/lang/String;

    .line 172
    :goto_4
    array-length v0, p2

    if-le v0, v1, :cond_8

    .line 173
    array-length v0, p2

    new-array v0, v0, [Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 174
    :goto_5
    array-length v0, p2

    if-ge v2, v0, :cond_9

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    new-instance v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    aget-object v3, p2, v2

    invoke-direct {v1, p1, v3}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/b;)V

    aput-object v1, v0, v2

    .line 174
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_1
    move v0, v2

    .line 142
    goto :goto_0

    :cond_2
    move v3, v2

    .line 143
    goto :goto_1

    .line 153
    :cond_3
    iget v4, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    .line 154
    iget v5, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    invoke-static {v7, v5}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/util/DisplayMetrics;I)I

    move-result v5

    iput v5, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    move v5, v4

    goto :goto_2

    .line 158
    :cond_4
    const/16 v8, 0x2d0

    if-gt v4, v8, :cond_5

    const/16 v4, 0x32

    goto :goto_3

    :cond_5
    const/16 v4, 0x5a

    goto :goto_3

    .line 160
    :cond_6
    iget v4, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    goto :goto_3

    .line 168
    :cond_7
    invoke-virtual {v6}, Lcom/google/android/gms/ads/b;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->b:Ljava/lang/String;

    goto :goto_4

    .line 178
    :cond_8
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->h:[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    .line 180
    :cond_9
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V
    .locals 9

    .prologue
    .line 186
    const/4 v1, 0x2

    iget-object v2, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->b:Ljava/lang/String;

    iget v3, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->c:I

    iget v4, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->d:I

    iget-boolean v5, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->e:Z

    iget v6, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->f:I

    iget v7, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->g:I

    move-object v0, p0

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(ILjava/lang/String;IIZII[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 194
    return-void
.end method

.method public static a(Landroid/content/Context;[Lcom/google/android/gms/ads/b;)Lcom/google/android/gms/ads/internal/client/AdSizeParcel;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 95
    if-eqz p1, :cond_0

    array-length v0, p1

    if-nez v0, :cond_1

    .line 97
    :cond_0
    new-instance v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    const/4 v1, 0x2

    const-string v2, "native_mb"

    const/4 v8, 0x0

    move v4, v3

    move v5, v3

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(ILjava/lang/String;IIZII[Lcom/google/android/gms/ads/internal/client/AdSizeParcel;)V

    .line 106
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>(Landroid/content/Context;[Lcom/google/android/gms/ads/b;)V

    goto :goto_0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 218
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 227
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/ads/internal/client/b;->a(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Landroid/os/Parcel;I)V

    .line 228
    return-void
.end method
