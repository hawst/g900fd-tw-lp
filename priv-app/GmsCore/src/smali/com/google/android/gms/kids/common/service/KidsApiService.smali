.class public Lcom/google/android/gms/kids/common/service/KidsApiService;
.super Lcom/google/android/gms/kids/common/service/a;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/common/service/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/kids/common/service/KidsApiService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/gms/kids/common/service/KidsApiService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0}, Lcom/google/android/gms/kids/common/service/a;-><init>(Lcom/google/android/gms/common/service/d;)V

    .line 26
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/kids/common/c/b;)V
    .locals 5

    .prologue
    .line 29
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 30
    sget-object v0, Lcom/google/android/gms/kids/common/service/KidsApiService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/service/d;->offer(Ljava/lang/Object;)Z

    .line 32
    const-string v0, "KidsApiService"

    const-string v1, "Submitted %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    const-string v0, "com.google.android.gms.kids.common.service.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 34
    return-void
.end method
