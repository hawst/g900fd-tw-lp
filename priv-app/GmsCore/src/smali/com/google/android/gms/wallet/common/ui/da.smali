.class final Lcom/google/android/gms/wallet/common/ui/da;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/cz;

.field private final b:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/cz;Landroid/content/Context;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 939
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/da;->a:Lcom/google/android/gms/wallet/common/ui/cz;

    .line 940
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 942
    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/da;->b:Landroid/view/LayoutInflater;

    .line 943
    return-void
.end method

.method private a(ILandroid/view/View;)Landroid/view/View;
    .locals 7

    .prologue
    const/16 v6, 0x8

    const/4 v5, 0x0

    .line 973
    if-nez p2, :cond_0

    .line 974
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/da;->b:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->hk:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 978
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/da;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 979
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/da;->isEnabled(I)Z

    move-result v2

    .line 981
    sget v0, Lcom/google/android/gms/j;->ka:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 982
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/da;->a:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/common/ui/cz;Landroid/widget/ImageView;Ljava/lang/Object;)Z

    move-result v3

    .line 983
    if-eqz v3, :cond_1

    .line 984
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 985
    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 990
    :goto_0
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 991
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/da;->a:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-static {v3, v1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/common/ui/cz;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 992
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 993
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1000
    :goto_1
    sget v0, Lcom/google/android/gms/j;->me:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 1001
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/da;->a:Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-static {v3, v1}, Lcom/google/android/gms/wallet/common/ui/cz;->b(Lcom/google/android/gms/wallet/common/ui/cz;Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1002
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1003
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1010
    :goto_2
    return-object p2

    .line 987
    :cond_1
    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 995
    :cond_2
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 996
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 997
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 1005
    :cond_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1006
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1007
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method


# virtual methods
.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 969
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/da;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 964
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/da;->a(ILandroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 951
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/da;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 953
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_0

    .line 954
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/da;->a:Lcom/google/android/gms/wallet/common/ui/cz;

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/common/ui/cz;Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    .line 959
    :goto_0
    return v0

    .line 955
    :cond_0
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_1

    .line 956
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/da;->a:Lcom/google/android/gms/wallet/common/ui/cz;

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/android/gms/wallet/common/ui/cz;Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    goto :goto_0

    .line 959
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
