.class public Lcom/google/android/gms/auth/setup/d2d/SourceNfcHandlerActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 16
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 17
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceNfcHandlerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.nfc.extra.NDEF_MESSAGES"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .line 18
    aget-object v0, v0, v2

    check-cast v0, Landroid/nfc/NdefMessage;

    .line 19
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v1

    array-length v1, v1

    if-lez v1, :cond_0

    .line 20
    invoke-virtual {v0}, Landroid/nfc/NdefMessage;->getRecords()[Landroid/nfc/NdefRecord;

    move-result-object v0

    aget-object v0, v0, v2

    .line 21
    invoke-virtual {v0}, Landroid/nfc/NdefRecord;->getPayload()[B

    move-result-object v0

    .line 22
    invoke-static {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceActivity;->a(Landroid/content/Context;[B)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/SourceNfcHandlerActivity;->startActivity(Landroid/content/Intent;)V

    .line 24
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/SourceNfcHandlerActivity;->finish()V

    .line 25
    return-void
.end method
