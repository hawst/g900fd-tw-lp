.class final Lcom/google/android/gms/people/sync/bc;
.super Lcom/google/android/gms/people/sync/az;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/gms/people/sync/ap;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/sync/ap;)V
    .locals 1

    .prologue
    .line 2783
    iput-object p1, p0, Lcom/google/android/gms/people/sync/bc;->b:Lcom/google/android/gms/people/sync/ap;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/sync/az;-><init>(Lcom/google/android/gms/people/sync/ap;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/sync/ap;B)V
    .locals 0

    .prologue
    .line 2783
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/sync/bc;-><init>(Lcom/google/android/gms/people/sync/ap;)V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 2786
    const/4 v0, 0x2

    return v0
.end method

.method protected final synthetic a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2783
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic a(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 2783
    check-cast p2, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;

    const-string v0, "value"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "value2"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "value_type"

    invoke-static {p2}, Lcom/google/android/gms/people/sync/e;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "custom_label"

    invoke-virtual {p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$PhoneNumbers;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "affinity1"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "affinity2"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "affinity3"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "affinity4"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "affinity5"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "logging_id"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "logging_id2"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "logging_id3"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "logging_id4"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "logging_id5"

    invoke-virtual {p1, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/people/sync/y;)V
    .locals 1

    .prologue
    .line 2822
    iget v0, p1, Lcom/google/android/gms/people/sync/y;->J:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/google/android/gms/people/sync/y;->J:I

    .line 2823
    return-void
.end method

.method protected final bridge synthetic b(Landroid/content/ContentValues;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 2783
    return-void
.end method

.method protected final b(Lcom/google/android/gms/people/sync/y;)V
    .locals 0

    .prologue
    .line 2827
    return-void
.end method
