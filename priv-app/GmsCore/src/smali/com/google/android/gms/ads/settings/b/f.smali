.class public final Lcom/google/android/gms/ads/settings/b/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/settings/c/d;


# static fields
.field private static final a:Lcom/google/android/gms/ads/settings/b/f;


# instance fields
.field private b:Lcom/google/android/gms/ads/settings/c/b;

.field private c:Lcom/google/android/gms/ads/identifier/b/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/ads/settings/b/f;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/settings/b/f;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/ads/settings/b/f;->a:Lcom/google/android/gms/ads/settings/b/f;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/gms/ads/settings/c/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/ads/settings/c/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    .line 39
    return-void
.end method

.method public static a()Lcom/google/android/gms/ads/settings/b/f;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/ads/settings/b/f;->a:Lcom/google/android/gms/ads/settings/b/f;

    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->b()V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/ads/settings/c/b;->b(Lcom/google/android/gms/ads/settings/c/d;)V

    .line 88
    iput-object v1, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    .line 90
    :cond_0
    iput-object v1, p0, Lcom/google/android/gms/ads/settings/b/f;->c:Lcom/google/android/gms/ads/identifier/b/b;

    .line 91
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 59
    packed-switch p1, :pswitch_data_0

    .line 79
    :goto_0
    :pswitch_0
    return-void

    .line 69
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->c:Lcom/google/android/gms/ads/identifier/b/b;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/identifier/b/b;->b(Z)V

    .line 70
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/b/f;->b()V

    goto :goto_0

    .line 75
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->c:Lcom/google/android/gms/ads/identifier/b/b;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/identifier/b/b;->b(Z)V

    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/b/f;->b()V

    goto :goto_0

    .line 59
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/ads/identifier/b/b;)V
    .locals 2

    .prologue
    .line 42
    iput-object p1, p0, Lcom/google/android/gms/ads/settings/b/f;->c:Lcom/google/android/gms/ads/identifier/b/b;

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/ads/settings/c/b;->a(Lcom/google/android/gms/ads/settings/c/d;)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/settings/c/b;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->a()V

    iget-object v0, p0, Lcom/google/android/gms/ads/settings/b/f;->b:Lcom/google/android/gms/ads/settings/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/settings/c/b;->e()V

    .line 43
    return-void
.end method
