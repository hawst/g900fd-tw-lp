.class public final Lcom/google/android/gms/games/ui/cp;
.super Lcom/google/android/gms/games/ui/cw;
.source "SourceFile"


# static fields
.field private static final c:I


# instance fields
.field private f:I

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    sget v0, Lcom/google/android/gms/l;->aP:I

    sput v0, Lcom/google/android/gms/games/ui/cp;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IZ)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cw;-><init>(Landroid/content/Context;)V

    .line 27
    iput p2, p0, Lcom/google/android/gms/games/ui/cp;->f:I

    .line 28
    iput-boolean p3, p0, Lcom/google/android/gms/games/ui/cp;->h:Z

    .line 29
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/cp;)Z
    .locals 1

    .prologue
    .line 16
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/cp;->h:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/cp;)I
    .locals 1

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/gms/games/ui/cp;->f:I

    return v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/cx;
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/cp;->e:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/games/ui/cp;->c:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 39
    new-instance v1, Lcom/google/android/gms/games/ui/cq;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/cq;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/gms/games/ui/cp;->c:I

    return v0
.end method
