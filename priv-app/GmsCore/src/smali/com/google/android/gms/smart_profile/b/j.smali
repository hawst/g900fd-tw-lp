.class public final Lcom/google/android/gms/smart_profile/b/j;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# static fields
.field private static final f:Landroid/net/Uri;


# instance fields
.field private g:[Ljava/lang/String;

.field private h:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-string v0, "content://mms-sms/threadID"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/b/j;->f:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/people/identity/models/Person;)V
    .locals 3

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 58
    const-string v0, "SmsCardsLoader"

    const-string v1, "SmsLoader"

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/people/identity/models/Person;->B()Z

    move-result v0

    if-nez v0, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 62
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 63
    invoke-interface {p2}, Lcom/google/android/gms/people/identity/models/Person;->A()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 64
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/s;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/s;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 67
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->g:[Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 221
    sget-object v0, Lcom/google/android/gms/smart_profile/b/j;->f:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    .line 223
    const-string v1, "recipient"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 225
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 227
    const-string v0, "SmsCardsLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v4

    .line 234
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v2, v0

    .line 244
    :goto_0
    if-eqz v2, :cond_1

    .line 246
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 253
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 258
    :goto_1
    return-object v0

    .line 241
    :catch_0
    move-exception v0

    move-object v2, v6

    goto :goto_0

    .line 249
    :cond_0
    :try_start_2
    const-string v0, "SmsCardsLoader"

    const-string v1, "getOrCreateThreadId returned no rows!"

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, ""
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 253
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 257
    :cond_1
    const-string v0, "SmsCardsLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "getOrCreateThreadId failed with uri "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v0, ""

    goto :goto_1
.end method

.method private static a(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/List;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 125
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz p0, :cond_5

    .line 127
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 128
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 129
    new-instance v3, Lcom/google/ac/c/a/a/a/ag;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/ag;-><init>()V

    .line 130
    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v3, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    .line 133
    const-string v1, "body"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    .line 135
    const-string v1, "date"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 138
    const-wide/16 v6, 0x3e8

    div-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v3, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    .line 139
    invoke-static {v4, v5, p1}, Lcom/google/android/gms/smart_profile/as;->a(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    .line 141
    const/4 v1, 0x0

    .line 142
    const-string v4, "read"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->isNull(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 143
    const-string v1, "read"

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v2, :cond_2

    move v1, v2

    .line 149
    :cond_0
    :goto_1
    new-instance v4, Lcom/google/ac/c/a/a/a/af;

    invoke-direct {v4}, Lcom/google/ac/c/a/a/a/af;-><init>()V

    iput-object v4, v3, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    .line 150
    iget-object v4, v3, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v4, Lcom/google/ac/c/a/a/a/af;->a:Ljava/lang/Integer;

    .line 151
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "smsto:"

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, "address"

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    .line 154
    const-string v1, "SmsCardsLoader"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "interaction: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/google/ac/c/a/a/a/ag;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 159
    :catchall_0
    move-exception v0

    if-eqz p0, :cond_1

    .line 160
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 146
    :cond_2
    const/4 v1, 0x2

    goto :goto_1

    .line 159
    :cond_3
    if-eqz p0, :cond_4

    .line 160
    invoke-interface {p0}, Landroid/database/Cursor;->close()V

    .line 165
    :cond_4
    :goto_2
    return-object v0

    .line 164
    :cond_5
    const-string v0, "SmsCardsLoader"

    const-string v1, "Telephony Provider returned null cursor"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_2
.end method

.method private a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 190
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/b/j;->h:Ljava/util/List;

    .line 191
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 192
    invoke-super {p0, p1}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V

    .line 194
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/b/j;->a(Ljava/util/List;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 44
    const-string v0, "SmsCardsLoader"

    const-string v2, "loadInBackground"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.hardware.telephony"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->g:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->g:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/b/j;->g:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_2

    aget-object v5, v2, v0

    iget-object v6, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-static {v6, v5}, Lcom/google/android/gms/smart_profile/b/j;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/b/j;->a(Landroid/database/Cursor;Landroid/content/Context;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v2, v0, :cond_3

    const-string v0, "SmsCardsLoader"

    const-string v2, "interactions.size() greater than LIMIT"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static {v1}, Lcom/google/android/gms/smart_profile/ar;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_4
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "body"

    aput-object v0, v2, v1

    const/4 v0, 0x1

    const-string v1, "date"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "address"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "read"

    aput-object v1, v2, v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "thread_id IN "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/as;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/Telephony$Sms;->CONTENT_URI:Landroid/net/Uri;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "date DESC LIMIT "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Lcom/google/android/gms/smart_profile/a/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    goto :goto_2
.end method

.method protected final e()V
    .locals 2

    .prologue
    .line 170
    const-string v0, "SmsCardsLoader"

    const-string v1, "onStartLoading"

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-super {p0}, Landroid/support/v4/a/a;->e()V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->h:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/j;->a(Ljava/util/List;)V

    .line 177
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/j;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->h:Ljava/util/List;

    if-nez v0, :cond_2

    .line 178
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 180
    :cond_2
    return-void
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 185
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/j;->b()Z

    .line 186
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 198
    invoke-super {p0}, Landroid/support/v4/a/a;->g()V

    .line 201
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/j;->b()Z

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/j;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 205
    :cond_0
    return-void
.end method
