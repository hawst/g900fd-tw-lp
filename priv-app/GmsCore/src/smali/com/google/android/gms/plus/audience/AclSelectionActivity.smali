.class public final Lcom/google/android/gms/plus/audience/AclSelectionActivity;
.super Lcom/google/android/gms/plus/audience/n;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private t:Lcom/google/android/gms/plus/audience/a;

.field private u:Landroid/view/View;

.field private v:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/plus/audience/n;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 34
    sget v0, Lcom/google/android/gms/p;->sW:I

    return v0
.end method

.method protected final synthetic a(Landroid/content/Intent;Landroid/support/v4/app/Fragment;)Lcom/google/android/gms/plus/audience/al;
    .locals 17

    .prologue
    .line 24
    const-string v2, "EXTRA_SEARCH_DEVICE"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->v:Z

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    instance-of v2, v0, Lcom/google/android/gms/plus/audience/a;

    if-eqz v2, :cond_0

    check-cast p2, Lcom/google/android/gms/plus/audience/a;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lcom/google/android/gms/plus/audience/a;

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lcom/google/android/gms/plus/audience/a;

    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    const-string v4, "SHOULD_LOAD_SUGGESTED"

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    const-string v5, "SHOULD_LOAD_GROUPS"

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    const-string v6, "LOAD_CIRCLES"

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    const-string v7, "LOAD_PEOPLE"

    const/4 v8, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v7, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string v8, "DESCRIPTION_TEXT"

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    const-string v9, "com.google.android.gms.common.acl.EXTRA_CLIENT_APPLICATION_ID"

    move-object/from16 v0, p1

    invoke-virtual {v0, v9}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/plus/audience/n;->c:Ljava/lang/String;

    const-string v11, "com.google.android.gms.common.acl.EXTRA_DOMAIN_RESTRICTED"

    const/4 v12, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v11

    const-string v12, "EXTRA_INCLUDE_SUGGESTIONS_WITH_PEOPLE"

    const/4 v13, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v12, v13}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v12

    const-string v13, "EXTRA_MAX_SUGGESTED_IMAGES"

    const/4 v14, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v13, v14}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    const-string v14, "EXTRA_MAX_SUGGESTED_LIST_ITEMS"

    const/4 v15, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v15}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v14

    const-string v15, "EXTRA_MAX_SUGGESTED_DEVICE"

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v15

    const/16 v16, 0x0

    invoke-static/range {v2 .. v16}, Lcom/google/android/gms/plus/audience/a;->a(Ljava/lang/String;Ljava/lang/String;ZZZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIIILjava/util/List;)Lcom/google/android/gms/plus/audience/a;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lcom/google/android/gms/plus/audience/a;

    goto :goto_0
.end method

.method protected final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 39
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/n;->a(Landroid/os/Bundle;)V

    .line 41
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->dC:I

    sget v0, Lcom/google/android/gms/j;->sT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/n;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->u:Landroid/view/View;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->u:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lcom/google/android/gms/plus/audience/a;

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/n;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/a;->b(Z)V

    .line 47
    return-void
.end method

.method protected final a(Lcom/google/android/gms/people/model/g;)V
    .locals 1

    .prologue
    .line 138
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/n;->a(Lcom/google/android/gms/people/model/g;)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/audience/a;->a(Lcom/google/android/gms/people/model/g;)V

    .line 140
    return-void
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 115
    sget-object v0, Lcom/google/android/gms/common/analytics/u;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->h()Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    .line 116
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/n;->b()V

    .line 117
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 121
    sget-object v0, Lcom/google/android/gms/common/analytics/u;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)V

    .line 122
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/n;->c()V

    .line 123
    return-void
.end method

.method protected final d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method protected final e()Lcom/google/android/gms/common/audience/a/d;
    .locals 2

    .prologue
    .line 132
    invoke-super {p0}, Lcom/google/android/gms/plus/audience/n;->e()Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->t:Lcom/google/android/gms/plus/audience/a;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/a;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->c(I)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    return-object v0
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 103
    sget-object v0, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lcom/google/android/gms/common/analytics/v;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 104
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 105
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/d;->c(Landroid/content/Intent;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/n;->f:Lcom/google/android/gms/plus/audience/bg;

    iget-object v2, v2, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v2, v0}, Lcom/google/android/gms/common/people/data/g;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    invoke-virtual {v1, v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 111
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 144
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/audience/n;->onClick(Landroid/view/View;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->u:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 146
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->onSearchRequested()Z

    .line 148
    :cond_0
    return-void
.end method

.method public final onSearchRequested()Z
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/gms/common/audience/a/d;

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/plus/audience/AudienceSearchActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/audience/a/d;-><init>(Landroid/content/Intent;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->e(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->f(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/n;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->g(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->sX:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/audience/a/d;->h(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/d;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->v:Z

    iget-object v2, v0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const-string v3, "EXTRA_SEARCH_DEVICE"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, v0, Lcom/google/android/gms/common/audience/a/d;->a:Landroid/content/Intent;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 96
    sget-object v0, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v1, Lcom/google/android/gms/common/analytics/v;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/AclSelectionActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 97
    const/4 v0, 0x0

    return v0
.end method
