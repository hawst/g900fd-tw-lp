.class public final Lcom/google/android/gms/gcm/nts/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/gms/common/a/d;

.field static final b:Lcom/google/android/gms/common/a/d;

.field static final c:Lcom/google/android/gms/common/a/d;

.field static final d:Lcom/google/android/gms/common/a/d;

.field private static e:Lcom/google/android/gms/gcm/nts/n;


# instance fields
.field private f:Ljava/lang/Object;

.field private g:Ljava/util/List;

.field private h:Landroid/util/SparseArray;

.field private final i:Landroid/content/Context;

.field private j:Lcom/google/android/gms/gcm/nts/q;

.field private k:Lcom/google/android/gms/gcm/nts/t;

.field private final l:Lcom/google/android/gms/gcm/nts/e;

.field private final m:Lcom/google/android/gms/gcm/nts/g;

.field private volatile n:Landroid/os/PowerManager$WakeLock;

.field private final o:Lcom/google/android/gms/gcm/nts/v;

.field private p:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 47
    const-string v0, "nts.scheduler_active"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/nts/n;->a:Lcom/google/android/gms/common/a/d;

    .line 50
    const-string v0, "nts.max_tasks_per_package"

    const/16 v1, 0x32

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/nts/n;->b:Lcom/google/android/gms/common/a/d;

    .line 53
    const-string v0, "nts.max_tasks_per_package"

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/nts/n;->c:Lcom/google/android/gms/common/a/d;

    .line 56
    const-string v0, "nts.max_tasks_runtime"

    const/16 v1, 0xb4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/nts/n;->d:Lcom/google/android/gms/common/a/d;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 371
    new-instance v0, Lcom/google/android/gms/gcm/nts/t;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/nts/t;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/gcm/nts/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/gcm/nts/t;)V

    .line 372
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/gcm/nts/t;)V
    .locals 3

    .prologue
    .line 356
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->f:Ljava/lang/Object;

    .line 112
    new-instance v0, Lcom/google/android/gms/gcm/nts/o;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/gcm/nts/o;-><init>(Lcom/google/android/gms/gcm/nts/n;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->p:Landroid/os/Handler;

    .line 357
    iput-object p1, p0, Lcom/google/android/gms/gcm/nts/n;->i:Landroid/content/Context;

    .line 358
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 359
    const/4 v1, 0x1

    const-string v2, "*net_scheduler*"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->n:Landroid/os/PowerManager$WakeLock;

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->n:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 361
    new-instance v0, Lcom/google/android/gms/gcm/nts/v;

    invoke-direct {v0, p1}, Lcom/google/android/gms/gcm/nts/v;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->o:Lcom/google/android/gms/gcm/nts/v;

    .line 362
    new-instance v0, Lcom/google/android/gms/gcm/nts/q;

    invoke-direct {v0, p1}, Lcom/google/android/gms/gcm/nts/q;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/q;->a()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    .line 364
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    .line 365
    new-instance v0, Lcom/google/android/gms/gcm/nts/e;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/nts/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->l:Lcom/google/android/gms/gcm/nts/e;

    .line 366
    new-instance v0, Lcom/google/android/gms/gcm/nts/g;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/nts/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->m:Lcom/google/android/gms/gcm/nts/g;

    .line 367
    iput-object p2, p0, Lcom/google/android/gms/gcm/nts/n;->k:Lcom/google/android/gms/gcm/nts/t;

    .line 368
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/gcm/nts/n;)Landroid/os/PowerManager$WakeLock;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->n:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static a(Lcom/google/android/gms/gcm/nts/k;)Lcom/google/android/gms/gcm/nts/k;
    .locals 11

    .prologue
    const-wide/16 v0, 0x0

    .line 828
    const-string v2, "NetworkScheduler"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 829
    const-string v2, "NetworkScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Rescheduling executed periodic: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 831
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/gcm/nts/k;->k:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    .line 832
    const-string v2, "NetworkScheduler"

    const-string v3, "Rescheduling a periodic should not have a last run-time of 0."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 836
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gms/gcm/nts/k;->k:J

    sub-long/2addr v4, v6

    sub-long v4, v2, v4

    .line 842
    cmp-long v6, v4, v0

    if-lez v6, :cond_2

    iget-wide v6, p0, Lcom/google/android/gms/gcm/nts/k;->e:J

    cmp-long v6, v4, v6

    if-gez v6, :cond_2

    .line 844
    iget-wide v0, p0, Lcom/google/android/gms/gcm/nts/k;->e:J

    sub-long/2addr v0, v4

    .line 848
    :cond_2
    iget-wide v4, p0, Lcom/google/android/gms/gcm/nts/k;->e:J

    iget-wide v6, p0, Lcom/google/android/gms/gcm/nts/k;->f:J

    sub-long v6, v4, v6

    .line 851
    iget-wide v4, p0, Lcom/google/android/gms/gcm/nts/k;->j:J

    add-long/2addr v4, v2

    add-long/2addr v4, v0

    .line 853
    const-string v8, "NetworkScheduler"

    const/4 v9, 0x2

    invoke-static {v8, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 854
    const-string v8, "NetworkScheduler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "Rescheduling periodic: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/google/android/gms/gcm/nts/k;->d:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 855
    const-string v8, "NetworkScheduler"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "remaining: "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "win start in: "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v8, v4, v2

    sub-long/2addr v8, v6

    invoke-virtual {v1, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 857
    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v8, "win end in: "

    invoke-direct {v1, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v2, v4, v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 859
    :cond_3
    new-instance v0, Lcom/google/android/gms/gcm/nts/k;

    sub-long v2, v4, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/gcm/nts/k;-><init>(Lcom/google/android/gms/gcm/nts/k;JJ)V

    return-object v0
.end method

.method public static a()Lcom/google/android/gms/gcm/nts/n;
    .locals 1

    .prologue
    .line 291
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->e:Lcom/google/android/gms/gcm/nts/n;

    return-object v0
.end method

.method private static a([Ljava/lang/String;)Ljava/util/List;
    .locals 5

    .prologue
    .line 1075
    const/4 v1, 0x0

    .line 1076
    const/4 v0, 0x0

    move v4, v0

    move-object v0, v1

    move v1, v4

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    .line 1077
    aget-object v2, p0, v1

    .line 1078
    const-string v3, "--endpoints"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1080
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1076
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1081
    :cond_1
    const-string v3, "--"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1083
    if-eqz v0, :cond_0

    .line 1085
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1088
    :cond_2
    return-object v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 269
    const-class v1, Lcom/google/android/gms/gcm/nts/n;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->a:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 270
    const-string v0, "NetworkScheduler"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "NetworkScheduler"

    const-string v2, "Not initialising GcmNetworkManager: Turned off."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 284
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 275
    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/gcm/f;->d()I

    move-result v0

    if-eqz v0, :cond_2

    .line 276
    const-string v0, "NetworkScheduler"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 277
    const-string v0, "NetworkScheduler"

    const-string v2, "Not initialising GcmNetworkManager: Not main user."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 281
    :cond_2
    :try_start_2
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->e:Lcom/google/android/gms/gcm/nts/n;

    if-nez v0, :cond_0

    .line 282
    new-instance v0, Lcom/google/android/gms/gcm/nts/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/gcm/nts/n;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/gcm/nts/n;->e:Lcom/google/android/gms/gcm/nts/n;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/gcm/nts/k;Z)V
    .locals 4

    .prologue
    .line 457
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 458
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 459
    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    .line 460
    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/nts/k;

    .line 461
    if-nez p2, :cond_1

    .line 462
    const-string v0, "NetworkScheduler"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    const-string v0, "NetworkScheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not updating task for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/gcm/nts/k;->g:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " as requested by caller."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 466
    :cond_0
    monitor-exit v1

    .line 475
    :goto_0
    return-void

    .line 469
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 470
    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/gcm/nts/q;->b(Lcom/google/android/gms/gcm/nts/k;)Z

    .line 472
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/nts/q;->a(Lcom/google/android/gms/gcm/nts/k;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Lcom/google/android/gms/gcm/nts/k;->a(I)V

    .line 473
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 474
    invoke-direct {p0}, Lcom/google/android/gms/gcm/nts/n;->g()V

    goto :goto_0

    .line 472
    :cond_3
    :try_start_1
    const-string v0, "NetworkScheduler"

    const-string v2, "Error persisting task."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 473
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/gcm/nts/n;Lcom/google/android/gms/gcm/nts/d;)V
    .locals 12

    .prologue
    .line 44
    const-string v0, "NetworkScheduler"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_0

    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Checking queue, size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " elapsedNow: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->i:Landroid/content/Context;

    invoke-interface {p1, v0}, Lcom/google/android/gms/gcm/nts/d;->a(Landroid/content/Context;)V

    invoke-interface {p1}, Lcom/google/android/gms/gcm/nts/d;->a()Ljava/util/Comparator;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {p1}, Lcom/google/android/gms/gcm/nts/d;->a()Ljava/util/Comparator;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->o:Lcom/google/android/gms/gcm/nts/v;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/v;->b()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v7, v0

    :cond_2
    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/gcm/nts/k;

    if-eqz v8, :cond_3

    const-string v0, "NetworkScheduler"

    invoke-virtual {v6}, Lcom/google/android/gms/gcm/nts/k;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-interface {p1, v6}, Lcom/google/android/gms/gcm/nts/d;->a(Lcom/google/android/gms/gcm/nts/k;)Z

    move-result v0

    if-eqz v0, :cond_c

    iget v0, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-static {v0}, Lcom/google/android/gms/gcm/f;->b(I)Z

    move-result v0

    if-nez v0, :cond_7

    iget v0, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-static {v0}, Lcom/google/android/gms/gcm/f;->c(I)Z

    move-result v0

    if-eqz v0, :cond_5

    if-eqz v8, :cond_4

    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "   u"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not valid, assuming user removed and deleting task."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/gcm/nts/q;->b(Lcom/google/android/gms/gcm/nts/k;)Z

    goto :goto_0

    :cond_5
    if-eqz v8, :cond_6

    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "    u"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not running, skipping task."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    const/4 v0, 0x5

    invoke-virtual {v6, v0}, Lcom/google/android/gms/gcm/nts/k;->a(I)V

    goto :goto_0

    :cond_7
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->c:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v1

    iget v0, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/nts/n;->c(I)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    iget v2, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    const/4 v0, 0x6

    invoke-virtual {v6, v0}, Lcom/google/android/gms/gcm/nts/k;->a(I)V

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_a

    if-eqz v8, :cond_2

    const-string v0, "NetworkScheduler"

    const-string v1, "     conflicts with an already active task, waiting for it to complete."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_8
    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-ne v0, v1, :cond_9

    const/4 v0, 0x4

    invoke-virtual {v6, v0}, Lcom/google/android/gms/gcm/nts/k;->a(I)V

    const/4 v0, 0x1

    goto :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_1

    :cond_a
    if-eqz v8, :cond_b

    const-string v0, "NetworkScheduler"

    const-string v1, "Executing. "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-interface {v9}, Ljava/util/Iterator;->remove()V

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/gcm/nts/q;->b(Lcom/google/android/gms/gcm/nts/k;)Z

    new-instance v0, Lcom/google/android/gms/gcm/nts/a;

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->p:Landroid/os/Handler;

    iget-object v3, v6, Lcom/google/android/gms/gcm/nts/k;->d:Ljava/lang/String;

    iget-object v4, v6, Lcom/google/android/gms/gcm/nts/k;->g:Landroid/content/ComponentName;

    iget v5, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/gcm/nts/a;-><init>(Landroid/content/Context;Landroid/os/Handler;Ljava/lang/String;Landroid/content/ComponentName;I)V

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->k:Lcom/google/android/gms/gcm/nts/t;

    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->i:Landroid/content/Context;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/gcm/nts/t;->a(Landroid/content/Context;Lcom/google/android/gms/gcm/nts/a;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :goto_2
    const/4 v0, 0x4

    invoke-static {v6, v0}, Lcom/google/android/gms/gcm/nts/s;->a(Lcom/google/android/gms/gcm/nts/k;I)V

    const/4 v0, 0x0

    :goto_3
    or-int/2addr v0, v7

    move v7, v0

    goto/16 :goto_0

    :pswitch_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v6, Lcom/google/android/gms/gcm/nts/k;->k:J

    iget v1, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/gcm/nts/n;->c(I)V

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    iget v2, v6, Lcom/google/android/gms/gcm/nts/k;->h:I

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/s;->a(I)V

    const/4 v0, 0x1

    invoke-virtual {v6, v0}, Lcom/google/android/gms/gcm/nts/k;->a(I)V

    const/4 v0, 0x1

    goto :goto_3

    :pswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v6}, Lcom/google/android/gms/gcm/nts/n;->b(Lcom/google/android/gms/gcm/nts/k;)Lcom/google/android/gms/gcm/nts/k;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_c
    const/4 v0, 0x2

    invoke-virtual {v6, v0}, Lcom/google/android/gms/gcm/nts/k;->a(I)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->o:Lcom/google/android/gms/gcm/nts/v;

    invoke-virtual {v0, v10, v11, v6}, Lcom/google/android/gms/gcm/nts/v;->a(JLcom/google/android/gms/gcm/nts/k;)V

    goto/16 :goto_0

    :cond_d
    if-eqz v7, :cond_e

    const/4 v0, 0x7

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/s;->a(I)V

    :cond_e
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v10

    invoke-static {v0, v1}, Lcom/google/android/gms/gcm/nts/s;->a(J)V

    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/gcm/nts/n;Lcom/google/android/gms/gcm/nts/k;Z)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/k;Z)V

    return-void
.end method

.method private a(Ljava/io/PrintWriter;)V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 1032
    const-string v1, "\nActive tasks:"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1033
    iget-object v3, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    monitor-enter v3

    move v2, v0

    move v1, v0

    .line 1035
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1036
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 1038
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v0, v1

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/nts/a;

    .line 1039
    const/4 v1, 0x1

    .line 1040
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, v0, Lcom/google/android/gms/gcm/nts/a;->g:J

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v6, v7}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "   "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v0, Lcom/google/android/gms/gcm/nts/a;->b:Landroid/content/ComponentName;

    invoke-virtual {v7}, Landroid/content/ComponentName;->flattenToShortString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", u"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v0, v0, Lcom/google/android/gms/gcm/nts/a;->f:I

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v6, " running: "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v0, v1

    .line 1041
    goto :goto_1

    .line 1035
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 1043
    :cond_1
    if-nez v1, :cond_2

    .line 1044
    const-string v0, "   none."

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1046
    :cond_2
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1047
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1048
    return-void

    .line 1046
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private a(Ljava/io/PrintWriter;Ljava/util/List;)V
    .locals 8

    .prologue
    .line 1001
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "GcmNetworkManager execution stats over the last "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/gcm/nts/s;->a()Lcom/google/android/gms/gcm/nts/s;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/gcm/nts/s;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " secs\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1003
    invoke-static {}, Lcom/google/android/gms/gcm/nts/s;->a()Lcom/google/android/gms/gcm/nts/s;

    move-result-object v2

    .line 1004
    iget-object v3, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    monitor-enter v3

    .line 1005
    :try_start_0
    const-string v0, "Pending:\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1006
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1008
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/gcm/nts/k;

    .line 1009
    invoke-virtual {v1}, Lcom/google/android/gms/gcm/nts/k;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 1010
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "(scheduled) "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1011
    invoke-static {p1, v1}, Lcom/google/android/gms/gcm/nts/s;->a(Ljava/io/PrintWriter;Lcom/google/android/gms/gcm/nts/k;)V

    .line 1012
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1028
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1017
    :cond_2
    :try_start_1
    const-string v0, "Past executions:\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1018
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1020
    invoke-virtual {v2}, Lcom/google/android/gms/gcm/nts/s;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1021
    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 1022
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "(finished) "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1023
    invoke-virtual {v2, p1, v1}, Lcom/google/android/gms/gcm/nts/s;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 1024
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    goto :goto_1

    .line 1028
    :cond_5
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 617
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/gcm/nts/q;->a(Ljava/lang/String;)I

    move-result v1

    sget-object v2, Lcom/google/android/gms/gcm/nts/n;->b:Lcom/google/android/gms/common/a/d;

    invoke-static {v2}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v2

    if-le v1, v2, :cond_0

    .line 619
    const-string v1, "NetworkScheduler"

    const-string v2, "Too many tasks scheduled for this package."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 626
    :goto_0
    return v0

    .line 622
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 623
    const-string v1, "NetworkScheduler"

    const-string v2, "Invalid package name specified."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 626
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Lcom/google/android/gms/gcm/nts/k;)Lcom/google/android/gms/gcm/nts/k;
    .locals 7

    .prologue
    .line 875
    sget-object v0, Lcom/google/android/gms/gcm/nts/k;->b:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    .line 877
    sget-object v1, Lcom/google/android/gms/gcm/nts/k;->c:Lcom/google/android/gms/common/a/d;

    invoke-static {v1}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v1

    .line 880
    int-to-float v0, v0

    iget v2, p0, Lcom/google/android/gms/gcm/nts/k;->l:I

    invoke-static {v0, v2}, Ljava/lang/Math;->scalb(FI)F

    move-result v0

    float-to-int v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 887
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    mul-int/lit16 v2, v6, 0x3e8

    int-to-long v2, v2

    add-long/2addr v2, v0

    .line 890
    new-instance v0, Lcom/google/android/gms/gcm/nts/k;

    move-object v1, p0

    move-wide v4, v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/gcm/nts/k;-><init>(Lcom/google/android/gms/gcm/nts/k;JJ)V

    .line 895
    const-string v1, "NetworkScheduler"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 896
    const-string v1, "NetworkScheduler"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Rescheduling failed task for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/gcm/nts/k;->g:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Failures: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/gcm/nts/k;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", to run at "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gms/gcm/nts/k;->e:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " (now="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") backoff="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "s"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 902
    :cond_0
    iget v1, v0, Lcom/google/android/gms/gcm/nts/k;->l:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/gcm/nts/k;->l:I

    .line 903
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/gcm/nts/n;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->f:Ljava/lang/Object;

    return-object v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 323
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->e:Lcom/google/android/gms/gcm/nts/n;

    .line 324
    if-eqz v0, :cond_0

    .line 325
    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/nts/n;->b(I)V

    .line 327
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 687
    invoke-static {p1}, Lcom/google/android/gms/gcm/nts/s;->a(I)V

    .line 689
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->o:Lcom/google/android/gms/gcm/nts/v;

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/nts/v;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_1

    .line 692
    const-string v0, "NetworkScheduler"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 693
    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dropping wake-up, reason="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    :cond_0
    :goto_0
    return-void

    .line 697
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->p:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 698
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->p:Landroid/os/Handler;

    invoke-static {v0, v1}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    .line 699
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 300
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->e:Lcom/google/android/gms/gcm/nts/n;

    .line 301
    if-nez v0, :cond_1

    .line 302
    invoke-static {p0}, Lcom/google/android/gms/gcm/f;->a(Landroid/content/Context;)V

    .line 303
    invoke-static {}, Lcom/google/android/gms/gcm/f;->d()I

    move-result v0

    .line 304
    if-eqz v0, :cond_0

    .line 305
    new-instance v2, Landroid/content/Intent;

    const-string v0, "com.google.android.gms.gcm.ACTION_HTTP_OK"

    invoke-direct {v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 306
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    const/4 v1, 0x0

    const-string v3, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    move-object v0, p0

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/gcm/f;->a(Landroid/content/Context;ILandroid/content/Intent;Ljava/lang/String;Landroid/content/BroadcastReceiver;Landroid/os/Handler;)V

    .line 315
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    invoke-static {}, Lcom/google/android/gms/gcm/nts/n;->b()V

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/gcm/nts/n;)Lcom/google/android/gms/gcm/nts/e;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->l:Lcom/google/android/gms/gcm/nts/e;

    return-object v0
.end method

.method public static c()V
    .locals 2

    .prologue
    .line 334
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->e:Lcom/google/android/gms/gcm/nts/n;

    .line 335
    if-eqz v0, :cond_0

    .line 336
    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/nts/n;->b(I)V

    .line 338
    :cond_0
    return-void
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 911
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 912
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    new-instance v1, Ljava/util/HashMap;

    sget-object v2, Lcom/google/android/gms/gcm/nts/n;->c:Lcom/google/android/gms/common/a/d;

    invoke-static {v2}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 917
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/gcm/nts/n;)Lcom/google/android/gms/gcm/nts/v;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->o:Lcom/google/android/gms/gcm/nts/v;

    return-object v0
.end method

.method public static d()V
    .locals 2

    .prologue
    .line 345
    sget-object v0, Lcom/google/android/gms/gcm/nts/n;->e:Lcom/google/android/gms/gcm/nts/n;

    .line 346
    if-eqz v0, :cond_0

    .line 347
    const/4 v1, 0x2

    invoke-direct {v0, v1}, Lcom/google/android/gms/gcm/nts/n;->b(I)V

    .line 350
    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/gcm/nts/n;)Lcom/google/android/gms/gcm/nts/g;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->m:Lcom/google/android/gms/gcm/nts/g;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/gcm/nts/n;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/nts/n;->b(I)V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/gcm/nts/n;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->h:Landroid/util/SparseArray;

    return-object v0
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 673
    const-string v0, "NetworkScheduler"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    const-string v0, "NetworkScheduler"

    const-string v1, "sending check queues message"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->p:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 677
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->p:Landroid/os/Handler;

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 678
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/gcm/nts/n;)Ljava/util/List;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 538
    const-string v0, "NetworkScheduler"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539
    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Handling user removed for u"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 541
    :cond_0
    invoke-virtual {p0, v3, v3, p1}, Lcom/google/android/gms/gcm/nts/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 542
    return-void
.end method

.method public final a(Landroid/content/ComponentName;I)V
    .locals 2

    .prologue
    .line 514
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1, p2}, Lcom/google/android/gms/gcm/nts/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 515
    return-void
.end method

.method public final a(Landroid/content/ComponentName;IJJLjava/lang/String;ZZ)V
    .locals 11

    .prologue
    .line 402
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/gcm/nts/n;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 418
    :goto_0
    return-void

    .line 405
    :cond_0
    cmp-long v2, p5, p3

    if-gtz v2, :cond_1

    .line 406
    const-string v2, "NetworkScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Task must be scheduled at a time greater than the provided flex. Start: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s, end: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 410
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 411
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p3

    add-long/2addr v4, v2

    .line 412
    const-wide/16 v6, 0x3e8

    mul-long v6, v6, p5

    add-long/2addr v6, v2

    .line 413
    new-instance v2, Lcom/google/android/gms/gcm/nts/k;

    move-object v3, p1

    move v8, p2

    move-object/from16 v9, p7

    move/from16 v10, p9

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/gcm/nts/k;-><init>(Landroid/content/ComponentName;JJILjava/lang/String;Z)V

    move/from16 v0, p8

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/k;Z)V

    goto :goto_0
.end method

.method public final a(Landroid/content/ComponentName;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 489
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 490
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "NetworkScheduler.cancel(): Invalid package name provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 493
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/n;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 494
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 495
    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 496
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/nts/k;

    .line 497
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/gcm/nts/k;->b(Landroid/content/ComponentName;Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 498
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    .line 499
    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/gcm/nts/q;->b(Lcom/google/android/gms/gcm/nts/k;)Z

    .line 503
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    invoke-direct {p0}, Lcom/google/android/gms/gcm/nts/n;->g()V

    .line 505
    return-void

    .line 503
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Lcom/google/android/gms/gcm/OneoffTask;Landroid/content/ComponentName;I)V
    .locals 11

    .prologue
    .line 386
    invoke-virtual {p1}, Lcom/google/android/gms/gcm/OneoffTask;->a()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/OneoffTask;->b()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/OneoffTask;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/OneoffTask;->e()Z

    move-result v9

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/OneoffTask;->f()Z

    move-result v10

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v1 .. v10}, Lcom/google/android/gms/gcm/nts/n;->a(Landroid/content/ComponentName;IJJLjava/lang/String;ZZ)V

    .line 389
    return-void
.end method

.method final a(Lcom/google/android/gms/gcm/PeriodicTask;Landroid/content/ComponentName;I)V
    .locals 11

    .prologue
    .line 381
    invoke-virtual {p1}, Lcom/google/android/gms/gcm/PeriodicTask;->a()J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/PeriodicTask;->b()J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/PeriodicTask;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/PeriodicTask;->e()Z

    move-result v9

    invoke-virtual {p1}, Lcom/google/android/gms/gcm/PeriodicTask;->f()Z

    move-result v10

    move-object v1, p0

    move-object v2, p2

    move v3, p3

    invoke-virtual/range {v1 .. v10}, Lcom/google/android/gms/gcm/nts/n;->b(Landroid/content/ComponentName;IJJLjava/lang/String;ZZ)V

    .line 383
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 984
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 985
    const-string v0, "Global GcmNetworkManager stats:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    monitor-enter v2

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Earliest eligible task: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/gcm/nts/n;->o:Lcom/google/android/gms/gcm/nts/v;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/nts/v;->c()J

    move-result-wide v4

    sub-long/2addr v4, v0

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Next alarm: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/gcm/nts/n;->o:Lcom/google/android/gms/gcm/nts/v;

    invoke-virtual {v4}, Lcom/google/android/gms/gcm/nts/v;->d()J

    move-result-wide v4

    sub-long v0, v4, v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/google/android/gms/gcm/nts/n;->a(Ljava/io/PrintWriter;)V

    invoke-static {}, Lcom/google/android/gms/gcm/nts/s;->a()Lcom/google/android/gms/gcm/nts/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/nts/s;->a(Ljava/io/PrintWriter;)V

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/gcm/nts/q;->a(Ljava/io/PrintWriter;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 986
    invoke-static {p2}, Lcom/google/android/gms/gcm/nts/n;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 987
    if-nez v0, :cond_0

    .line 988
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 989
    const-string v1, "."

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 991
    :cond_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/gcm/nts/n;->a(Ljava/io/PrintWriter;Ljava/util/List;)V

    .line 992
    return-void

    .line 985
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 524
    const-string v0, "NetworkScheduler"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 525
    const-string v0, "NetworkScheduler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received Package replaced for : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", u"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/gcm/nts/n;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 528
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->i:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/gcm/nts/u;->a(Landroid/content/Context;Ljava/lang/String;I)V

    .line 530
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 554
    const-string v0, "NetworkScheduler"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    .line 555
    if-eqz v1, :cond_0

    .line 556
    const-string v2, "NetworkScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Removing all tasks for "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_5

    const-string v0, "all packages"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " and "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez p2, :cond_6

    const-string v0, "all endpoints"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", u"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/gcm/nts/n;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 562
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 563
    :cond_1
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 564
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/gcm/nts/k;

    .line 565
    iget v4, v0, Lcom/google/android/gms/gcm/nts/k;->h:I

    if-ne v4, p3, :cond_1

    .line 566
    if-eqz p1, :cond_2

    iget-object v4, v0, Lcom/google/android/gms/gcm/nts/k;->g:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 570
    :cond_2
    if-eqz p2, :cond_3

    iget-object v4, v0, Lcom/google/android/gms/gcm/nts/k;->g:Landroid/content/ComponentName;

    invoke-virtual {v4}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 574
    :cond_3
    if-eqz v1, :cond_4

    .line 577
    const-string v4, "NetworkScheduler"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Cancelling task "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 579
    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 580
    iget-object v4, p0, Lcom/google/android/gms/gcm/nts/n;->j:Lcom/google/android/gms/gcm/nts/q;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/gcm/nts/q;->b(Lcom/google/android/gms/gcm/nts/k;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 582
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_5
    move-object v0, p1

    .line 556
    goto :goto_0

    :cond_6
    move-object v0, p2

    goto :goto_1

    .line 582
    :cond_7
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 583
    invoke-direct {p0}, Lcom/google/android/gms/gcm/nts/n;->g()V

    .line 584
    return-void
.end method

.method public final b(Landroid/content/ComponentName;IJJLjava/lang/String;ZZ)V
    .locals 13

    .prologue
    .line 423
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/gcm/nts/n;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 454
    :goto_0
    return-void

    .line 426
    :cond_0
    cmp-long v2, p5, p3

    if-lez v2, :cond_1

    .line 427
    const-string v2, "NetworkScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Frequency must be larger than the provided flex. Frequency: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-wide/from16 v0, p3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s, flex: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-wide/from16 v0, p5

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "s"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 431
    :cond_1
    sget-object v2, Lcom/google/android/gms/gcm/nts/v;->a:Lcom/google/android/gms/common/a/d;

    invoke-static {v2}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v2

    int-to-long v2, v2

    cmp-long v2, p3, v2

    if-gez v2, :cond_2

    .line 432
    const-string v2, "NetworkScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Frequency can not be shorter than "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/gcm/nts/v;->a:Lcom/google/android/gms/common/a/d;

    invoke-static {v4}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 436
    :cond_2
    if-eqz p9, :cond_4

    .line 437
    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/gcm/nts/n;->i:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const-string v4, "android.permission.RECEIVE_BOOT_COMPLETED"

    invoke-virtual {v3, v4, v2}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_3

    const/4 v2, 0x1

    .line 438
    :goto_1
    if-nez v2, :cond_4

    .line 439
    const-string v2, "NetworkScheduler"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " requested task be persisted for tag: \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' but does not hold the permission android.Manifest.permission.RECEIVE_BOOT_COMPLETED. This task won\'t be persisted."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 443
    const/16 p9, 0x0

    move/from16 v12, p9

    .line 446
    :goto_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 447
    const-wide/16 v4, 0x3e8

    mul-long v4, v4, p3

    add-long v6, v2, v4

    .line 448
    const-wide/16 v2, 0x3e8

    mul-long v2, v2, p5

    sub-long v4, v6, v2

    .line 449
    new-instance v2, Lcom/google/android/gms/gcm/nts/k;

    const-wide/16 v8, 0x3e8

    mul-long v10, p3, v8

    move-object v3, p1

    move v8, p2

    move-object/from16 v9, p7

    invoke-direct/range {v2 .. v12}, Lcom/google/android/gms/gcm/nts/k;-><init>(Landroid/content/ComponentName;JJILjava/lang/String;JZ)V

    move/from16 v0, p8

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/gcm/nts/n;->a(Lcom/google/android/gms/gcm/nts/k;Z)V

    goto/16 :goto_0

    .line 437
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    :cond_4
    move/from16 v12, p9

    goto :goto_2
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 644
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/s;->a(I)V

    .line 645
    invoke-direct {p0}, Lcom/google/android/gms/gcm/nts/n;->g()V

    .line 646
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/n;->i:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 658
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 659
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 660
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/gcm/nts/n;->b(I)V

    .line 664
    :goto_0
    return-void

    .line 662
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/gcm/nts/n;->g()V

    goto :goto_0
.end method
