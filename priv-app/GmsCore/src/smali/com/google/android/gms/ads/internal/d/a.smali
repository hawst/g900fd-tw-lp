.class public final Lcom/google/android/gms/ads/internal/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 31
    const-string v1, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40.html"

    const/4 v5, 0x0

    const/4 v7, -0x1

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v6, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/d/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 41
    const/4 v7, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/d/a;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/d/a;->a:Ljava/lang/String;

    .line 14
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/d/a;->b:Ljava/lang/String;

    .line 15
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/d/a;->c:Ljava/lang/String;

    .line 16
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/d/a;->d:Ljava/lang/String;

    .line 18
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/ads/internal/d/a;->e:I

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/d/a;->f:Z

    .line 20
    iput-object v1, p0, Lcom/google/android/gms/ads/internal/d/a;->g:Ljava/lang/String;

    .line 57
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v0, "https://googleads.g.doubleclick.net/mads/static/mad/sdk/native/sdk-core-v40.html"

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/d/a;->a:Ljava/lang/String;

    .line 62
    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/d/a;->b:Ljava/lang/String;

    .line 63
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/d/a;->c:Ljava/lang/String;

    .line 64
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/d/a;->d:Ljava/lang/String;

    .line 65
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/d/a;->g:Ljava/lang/String;

    .line 66
    iput-boolean p5, p0, Lcom/google/android/gms/ads/internal/d/a;->f:Z

    .line 67
    iput p7, p0, Lcom/google/android/gms/ads/internal/d/a;->e:I

    .line 68
    return-void

    .line 60
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/d/a;->a:Ljava/lang/String;

    goto :goto_0
.end method
