.class public final Lcom/google/android/gms/kids/common/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 11
    const-string v0, "gms.kids.kidsmanagement.cache_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->a:Lcom/google/android/gms/common/a/d;

    .line 14
    const-string v0, "gms.kids.kidsmanagement.verbose_logging"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->b:Lcom/google/android/gms/common/a/d;

    .line 17
    const-string v0, "gms.kids.kidsmanagement.apiary_trace"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->c:Lcom/google/android/gms/common/a/d;

    .line 20
    const-string v0, "gms.kids.kidsmanagement.server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->d:Lcom/google/android/gms/common/a/d;

    .line 24
    const-string v0, "gms.kids.kidsmanagement.server_api_path"

    const-string v1, "/kidsmanagement/v1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->e:Lcom/google/android/gms/common/a/d;

    .line 27
    const-string v0, "gms.kids.kidsmanagement.backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->f:Lcom/google/android/gms/common/a/d;

    .line 30
    const-string v0, "gms.kids.kidsmanagement.auth_scope"

    const-string v1, "https://www.googleapis.com/auth/kid.management"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->g:Lcom/google/android/gms/common/a/d;

    .line 34
    const-string v0, "gms.kids.kidsmanagement.sync_interval"

    const-wide/16 v2, 0x5460

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->h:Lcom/google/android/gms/common/a/d;

    .line 37
    const-string v0, "gms.kids.kidsmanagement.account_setup_max_retry_count"

    const-wide/16 v2, 0xa

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->i:Lcom/google/android/gms/common/a/d;

    .line 40
    const-string v0, "gms.kids.kidsmanagement.account_setup_retry_interval"

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->j:Lcom/google/android/gms/common/a/d;

    .line 43
    const-string v0, "gms.kids.kidsmanagement.disabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/kids/common/b/a;->k:Lcom/google/android/gms/common/a/d;

    return-void
.end method
