.class final Lcom/google/android/gms/droidguard/b/p;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/droidguard/b/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/droidguard/b/m;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    .line 30
    return-void
.end method


# virtual methods
.method final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 35
    const/16 v0, 0x47

    :try_start_0
    new-array v0, v0, [I

    fill-array-data v0, :array_0

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v1

    .line 42
    new-instance v0, Landroid/media/MediaDrm;

    invoke-direct {v0, v1}, Landroid/media/MediaDrm;-><init>(Ljava/util/UUID;)V

    .line 43
    invoke-virtual {v0}, Landroid/media/MediaDrm;->openSession()[B
    :try_end_0
    .catch Landroid/media/MediaDrmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    .line 46
    const/16 v2, 0x1d

    :try_start_1
    new-array v2, v2, [I

    fill-array-data v2, :array_1

    .line 50
    const/16 v3, 0x13

    new-array v3, v3, [I

    fill-array-data v3, :array_2

    .line 53
    iget-object v4, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/MediaDrm;->getCryptoSession([BLjava/lang/String;Ljava/lang/String;)Landroid/media/MediaDrm$CryptoSession;

    .line 57
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 59
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    .line 60
    iget-object v3, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x11

    new-array v3, v3, [I

    fill-array-data v3, :array_4

    iget-object v4, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    if-nez v3, :cond_0

    new-instance v2, Lcom/google/android/gms/droidguard/b/q;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Lcom/google/android/gms/droidguard/b/q;-><init>(I)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 76
    :catchall_0
    move-exception v2

    :try_start_2
    invoke-virtual {v0, v1}, Landroid/media/MediaDrm;->closeSession([B)V

    throw v2
    :try_end_2
    .catch Landroid/media/MediaDrmException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1

    .line 78
    :catch_0
    move-exception v0

    .line 79
    new-instance v1, Lcom/google/android/gms/droidguard/b/q;

    invoke-direct {v1, v6, v0}, Lcom/google/android/gms/droidguard/b/q;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 60
    :cond_0
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    const/16 v4, 0xb

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0x83

    aput v4, v2, v3

    .line 64
    const/16 v3, 0x2b

    new-array v3, v3, [I

    fill-array-data v3, :array_5

    .line 68
    iget-object v4, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    iget-object v4, p0, Lcom/google/android/gms/droidguard/b/p;->a:Lcom/google/android/gms/droidguard/b/m;

    invoke-virtual {v4, v3}, Lcom/google/android/gms/droidguard/b/m;->a([I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual/range {v0 .. v5}, Landroid/media/MediaDrm;->getKeyRequest([B[BLjava/lang/String;ILjava/util/HashMap;)Landroid/media/MediaDrm$KeyRequest;

    move-result-object v2

    .line 74
    invoke-virtual {v2}, Landroid/media/MediaDrm$KeyRequest;->getData()[B

    move-result-object v2

    const/16 v3, 0xb

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v2

    .line 76
    :try_start_4
    invoke-virtual {v0, v1}, Landroid/media/MediaDrm;->closeSession([B)V
    :try_end_4
    .catch Landroid/media/MediaDrmException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1

    return-object v2

    .line 80
    :catch_1
    move-exception v0

    .line 81
    new-instance v1, Lcom/google/android/gms/droidguard/b/q;

    invoke-direct {v1, v7, v0}, Lcom/google/android/gms/droidguard/b/q;-><init>(ILjava/lang/Exception;)V

    throw v1

    .line 35
    nop

    :array_0
    .array-data 4
        0x44
        0xdc
        0xdf
        0x14
        0xe4
        0xe3
        0x44
        0x1b
        0xd8
        0x62
        0x89
        0xc5
        0xda
        0xdf
        0xdb
        0xde
        0xcf
        0xe3
        0xc6
        0xc9
        0xe4
        0x13
        0xcf
        0x7
        0xc6
        0xd3
        0xca
        0xd4
        0x14
        0x9c
        0xd6
        0xe2
        0xcc
        0x13
        0xe1
        0x59
        0xc5
        0xd4
        0x79
        0x44
        0x13
        0xc5
        0x69
        0x79
        0x62
        0xc9
        0x13
        0xe4
        0xc9
        0xd4
        0xe0
        0xd8
        0xc7
        0xe3
        0xd2
        0x7
        0xcd
        0xce
        0x14
        0x79
        0x14
        0xe4
        0xcf
        0xe4
        0x73
        0x35
        0x14
        0xc7
        0x35
        0x44
        0x14
    .end array-data

    .line 46
    :array_1
    .array-data 4
        0xdb
        0xc5
        0x44
        0x7f
        0xdd
        0x66
        0x79
        0xdf
        0x89
        0x79
        0x66
        0xc2
        0xe0
        0xd0
        0xd1
        0xcc
        0xbe
        0x47
        0xbf
        0xe2
        0xc0
        0xc0
        0xde
        0xe0
        0xd1
        0xc3
        0x95
        0xd2
        0xb6
    .end array-data

    .line 50
    :array_2
    .array-data 4
        0x4
        0x26
        0xc8
        0xde
        0xd8
        0xbf
        0xbc
        0x7f
        0xcd
        0xd5
        0xde
        0x4
        0xc5
        0xc7
        0xd5
        0xd3
        0xcd
        0x73
        0x9c
    .end array-data

    .line 59
    :array_3
    .array-data 4
        0xd4
        0x26
    .end array-data

    .line 60
    :array_4
    .array-data 4
        0x7f
        0xd3
        0xce
        0xd0
        0x4
        0xd1
        0xde
        0xd8
        0xe5
        0xd5
        0xc5
        0x13
        0xcf
        0xd2
        0xce
        0xdf
        0x35
    .end array-data

    .line 64
    :array_5
    .array-data 4
        0xda
        0xbf
        0xdc
        0x91
        0xe5
        0xcb
        0xcb
        0x91
        0x75
        0xc3
        0xda
        0xbc
        0xd9
        0xbf
        0xba
        0xc3
        0xd7
        0xdd
        0xbe
        0xe2
        0x95
        0xce
        0xc9
        0xcb
        0x66
        0xdc
        0xbe
        0xbc
        0xd0
        0xba
        0xb7
        0xba
        0x13
        0xd6
        0xc4
        0xde
        0xba
        0x9a
        0xe0
        0xd2
        0xb7
        0xbf
        0x26
    .end array-data
.end method
