.class public abstract Lcom/google/android/gms/car/kk;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/kj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 19
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/car/kk;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 20
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/car/kj;
    .locals 2

    .prologue
    .line 27
    if-nez p0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 34
    :goto_0
    return-object v0

    .line 30
    :cond_0
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 31
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/car/kj;

    if-eqz v1, :cond_1

    .line 32
    check-cast v0, Lcom/google/android/gms/car/kj;

    goto :goto_0

    .line 34
    :cond_1
    new-instance v0, Lcom/google/android/gms/car/kl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/kl;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 38
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 42
    sparse-switch p1, :sswitch_data_0

    .line 125
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v1

    :goto_0
    return v1

    .line 46
    :sswitch_0
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 51
    :sswitch_1
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/kn;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/km;

    move-result-object v0

    .line 54
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/kk;->a(Lcom/google/android/gms/car/km;)V

    .line 55
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 60
    :sswitch_2
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/kn;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/km;

    move-result-object v0

    .line 63
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/kk;->b(Lcom/google/android/gms/car/km;)V

    .line 64
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 69
    :sswitch_3
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/kn;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/km;

    move-result-object v0

    .line 72
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/kk;->e(Lcom/google/android/gms/car/km;)V

    .line 73
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 78
    :sswitch_4
    const-string v2, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/car/kn;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/km;

    move-result-object v2

    .line 81
    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/kk;->c(Lcom/google/android/gms/car/km;)Landroid/os/ParcelFileDescriptor;

    move-result-object v2

    .line 82
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 83
    if-eqz v2, :cond_0

    .line 84
    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    invoke-virtual {v2, p3, v1}, Landroid/os/ParcelFileDescriptor;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    .line 88
    :cond_0
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    .line 94
    :sswitch_5
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/kn;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/km;

    move-result-object v0

    .line 97
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/kk;->d(Lcom/google/android/gms/car/km;)V

    .line 98
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 103
    :sswitch_6
    const-string v0, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/kn;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/km;

    move-result-object v0

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 108
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/car/kk;->a(Lcom/google/android/gms/car/km;I)V

    .line 109
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 114
    :sswitch_7
    const-string v2, "com.google.android.gms.car.ICarAudioRecord"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 116
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/car/kn;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/km;

    move-result-object v2

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 119
    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/car/kk;->b(Lcom/google/android/gms/car/km;I)Z

    move-result v2

    .line 120
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 121
    if-eqz v2, :cond_1

    move v0, v1

    :cond_1
    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 42
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
