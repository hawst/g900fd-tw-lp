.class public final Lcom/google/android/gms/auth/f/e;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/android/gms/auth/f/f;

.field private c:Z

.field private d:J

.field private e:Z

.field private f:J

.field private g:Z

.field private h:J

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:I

.field private m:Z

.field private n:I

.field private o:Z

.field private p:I

.field private q:Z

.field private r:I

.field private s:Z

.field private t:I

.field private u:Ljava/util/List;

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 1810
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1975
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/f/e;->b:Lcom/google/android/gms/auth/f/f;

    .line 1995
    iput-wide v2, p0, Lcom/google/android/gms/auth/f/e;->d:J

    .line 2012
    iput-wide v2, p0, Lcom/google/android/gms/auth/f/e;->f:J

    .line 2029
    iput-wide v2, p0, Lcom/google/android/gms/auth/f/e;->h:J

    .line 2046
    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/e;->j:Z

    .line 2063
    iput v1, p0, Lcom/google/android/gms/auth/f/e;->l:I

    .line 2080
    iput v1, p0, Lcom/google/android/gms/auth/f/e;->n:I

    .line 2097
    iput v1, p0, Lcom/google/android/gms/auth/f/e;->p:I

    .line 2114
    iput v1, p0, Lcom/google/android/gms/auth/f/e;->r:I

    .line 2131
    iput v1, p0, Lcom/google/android/gms/auth/f/e;->t:I

    .line 2147
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/f/e;->u:Ljava/util/List;

    .line 2175
    iput-boolean v1, p0, Lcom/google/android/gms/auth/f/e;->w:Z

    .line 2192
    iput v1, p0, Lcom/google/android/gms/auth/f/e;->y:I

    .line 2272
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/f/e;->z:I

    .line 1810
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 2275
    iget v0, p0, Lcom/google/android/gms/auth/f/e;->z:I

    if-gez v0, :cond_0

    .line 2277
    invoke-virtual {p0}, Lcom/google/android/gms/auth/f/e;->b()I

    .line 2279
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/f/e;->z:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2067
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->k:Z

    .line 2068
    iput p1, p0, Lcom/google/android/gms/auth/f/e;->l:I

    .line 2069
    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 1999
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->c:Z

    .line 2000
    iput-wide p1, p0, Lcom/google/android/gms/auth/f/e;->d:J

    .line 2001
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2050
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->i:Z

    .line 2051
    iput-boolean p1, p0, Lcom/google/android/gms/auth/f/e;->j:Z

    .line 2052
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1807
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/android/gms/auth/f/f;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/f;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->a:Z

    iput-object v0, p0, Lcom/google/android/gms/auth/f/e;->b:Lcom/google/android/gms/auth/f/f;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/f/e;->a(J)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/f/e;->b(J)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/f/e;->c(J)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/e;->a(Z)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/e;->a(I)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/e;->b(I)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->o:Z

    iput v0, p0, Lcom/google/android/gms/auth/f/e;->p:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/e;->c(I)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/e;->d(I)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/e;->e(I)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/e;->b(Z)Lcom/google/android/gms/auth/f/e;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->x:Z

    iput v0, p0, Lcom/google/android/gms/auth/f/e;->y:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 2231
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->a:Z

    if-eqz v0, :cond_0

    .line 2232
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/auth/f/e;->b:Lcom/google/android/gms/auth/f/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 2234
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->c:Z

    if-eqz v0, :cond_1

    .line 2235
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/e;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 2237
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->e:Z

    if-eqz v0, :cond_2

    .line 2238
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/e;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 2240
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->g:Z

    if-eqz v0, :cond_3

    .line 2241
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/e;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 2243
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->i:Z

    if-eqz v0, :cond_4

    .line 2244
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/e;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 2246
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->k:Z

    if-eqz v0, :cond_5

    .line 2247
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/auth/f/e;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 2249
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->m:Z

    if-eqz v0, :cond_6

    .line 2250
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/auth/f/e;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 2252
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->o:Z

    if-eqz v0, :cond_7

    .line 2253
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/auth/f/e;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 2255
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->q:Z

    if-eqz v0, :cond_8

    .line 2256
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/auth/f/e;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 2258
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->s:Z

    if-eqz v0, :cond_9

    .line 2259
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/android/gms/auth/f/e;->t:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 2261
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/auth/f/e;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2262
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(II)V

    goto :goto_0

    .line 2264
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->v:Z

    if-eqz v0, :cond_b

    .line 2265
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/e;->w:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 2267
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->x:Z

    if-eqz v0, :cond_c

    .line 2268
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/android/gms/auth/f/e;->y:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 2270
    :cond_c
    return-void
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2284
    .line 2285
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->a:Z

    if-eqz v0, :cond_c

    .line 2286
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/auth/f/e;->b:Lcom/google/android/gms/auth/f/f;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2289
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->c:Z

    if-eqz v2, :cond_0

    .line 2290
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/auth/f/e;->d:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2293
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->e:Z

    if-eqz v2, :cond_1

    .line 2294
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/gms/auth/f/e;->f:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2297
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->g:Z

    if-eqz v2, :cond_2

    .line 2298
    const/4 v2, 0x4

    iget-wide v4, p0, Lcom/google/android/gms/auth/f/e;->h:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 2301
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->i:Z

    if-eqz v2, :cond_3

    .line 2302
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/android/gms/auth/f/e;->j:Z

    invoke-static {v2}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2305
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->k:Z

    if-eqz v2, :cond_4

    .line 2306
    const/4 v2, 0x6

    iget v3, p0, Lcom/google/android/gms/auth/f/e;->l:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2309
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->m:Z

    if-eqz v2, :cond_5

    .line 2310
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/android/gms/auth/f/e;->n:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2313
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->o:Z

    if-eqz v2, :cond_6

    .line 2314
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/android/gms/auth/f/e;->p:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2317
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->q:Z

    if-eqz v2, :cond_7

    .line 2318
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/android/gms/auth/f/e;->r:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 2321
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->s:Z

    if-eqz v2, :cond_b

    .line 2322
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/android/gms/auth/f/e;->t:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    .line 2327
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/auth/f/e;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 2328
    invoke-static {v0}, Lcom/google/protobuf/a/c;->a(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 2330
    goto :goto_2

    .line 2331
    :cond_8
    add-int v0, v2, v1

    .line 2332
    iget-object v1, p0, Lcom/google/android/gms/auth/f/e;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2334
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/e;->v:Z

    if-eqz v1, :cond_9

    .line 2335
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/android/gms/auth/f/e;->w:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2338
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/e;->x:Z

    if-eqz v1, :cond_a

    .line 2339
    const/16 v1, 0xd

    iget v2, p0, Lcom/google/android/gms/auth/f/e;->y:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2342
    :cond_a
    iput v0, p0, Lcom/google/android/gms/auth/f/e;->z:I

    .line 2343
    return v0

    :cond_b
    move v2, v0

    goto :goto_1

    :cond_c
    move v0, v1

    goto/16 :goto_0
.end method

.method public final b(I)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2084
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->m:Z

    .line 2085
    iput p1, p0, Lcom/google/android/gms/auth/f/e;->n:I

    .line 2086
    return-object p0
.end method

.method public final b(J)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2016
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->e:Z

    .line 2017
    iput-wide p1, p0, Lcom/google/android/gms/auth/f/e;->f:J

    .line 2018
    return-object p0
.end method

.method public final b(Z)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->v:Z

    .line 2180
    iput-boolean p1, p0, Lcom/google/android/gms/auth/f/e;->w:Z

    .line 2181
    return-object p0
.end method

.method public final c(I)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2118
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->q:Z

    .line 2119
    iput p1, p0, Lcom/google/android/gms/auth/f/e;->r:I

    .line 2120
    return-object p0
.end method

.method public final c(J)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2033
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->g:Z

    .line 2034
    iput-wide p1, p0, Lcom/google/android/gms/auth/f/e;->h:J

    .line 2035
    return-object p0
.end method

.method public final d(I)Lcom/google/android/gms/auth/f/e;
    .locals 1

    .prologue
    .line 2135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/e;->s:Z

    .line 2136
    iput p1, p0, Lcom/google/android/gms/auth/f/e;->t:I

    .line 2137
    return-object p0
.end method

.method public final e(I)Lcom/google/android/gms/auth/f/e;
    .locals 2

    .prologue
    .line 2161
    iget-object v0, p0, Lcom/google/android/gms/auth/f/e;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2162
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/f/e;->u:Ljava/util/List;

    .line 2164
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/f/e;->u:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2165
    return-object p0
.end method
