.class final Lcom/google/android/gms/smart_profile/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V
    .locals 0

    .prologue
    .line 99
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/ah;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 99
    check-cast p1, Lcom/google/android/gms/people/g;

    invoke-interface {p1}, Lcom/google/android/gms/people/g;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Lcom/google/android/gms/people/g;->c()Lcom/google/android/gms/people/model/h;

    move-result-object v2

    if-eqz v2, :cond_3

    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/people/model/h;->c()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {v2, v1}, Lcom/google/android/gms/people/model/h;->b(I)Lcom/google/android/gms/people/model/g;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v5}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->e(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v5}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v5}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4}, Lcom/google/android/gms/people/model/g;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v5}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->e(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4}, Lcom/google/android/gms/people/model/g;->h()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-interface {v4}, Lcom/google/android/gms/people/model/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;)Ljava/lang/String;

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/h;->c()I

    move-result v3

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/people/model/h;->b(I)Lcom/google/android/gms/people/model/g;

    move-result-object v5

    if-eqz v5, :cond_7

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v6}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_7

    iget-object v6, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v6}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5}, Lcom/google/android/gms/people/model/g;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v5}, Lcom/google/android/gms/people/model/g;->d()Z

    move-result v6

    if-eqz v6, :cond_6

    invoke-interface {v5}, Lcom/google/android/gms/people/model/g;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z

    :cond_1
    :goto_3
    invoke-virtual {v2}, Lcom/google/android/gms/people/model/h;->c()I

    move-result v1

    :goto_4
    if-ge v0, v1, :cond_2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/people/model/h;->b(I)Lcom/google/android/gms/people/model/g;

    move-result-object v3

    if-eqz v3, :cond_8

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v4}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v4}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3}, Lcom/google/android/gms/people/model/g;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-interface {v3}, Lcom/google/android/gms/people/model/g;->i()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-interface {v3}, Lcom/google/android/gms/people/model/g;->j()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    invoke-virtual {v2}, Lcom/google/android/gms/people/model/h;->w_()V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->d(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    return-void

    :cond_4
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v5}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v5}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->f(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4}, Lcom/google/android/gms/people/model/g;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-interface {v4}, Lcom/google/android/gms/people/model/g;->f()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/h;->w_()V

    throw v0

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_6
    :try_start_2
    invoke-interface {v5}, Lcom/google/android/gms/people/model/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/ah;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->h(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4
.end method
