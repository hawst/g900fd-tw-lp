.class public Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Z

.field public final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/l;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 41
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->c:Z

    .line 42
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->d:Ljava/lang/String;

    .line 43
    return-void

    .line 41
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->j:Ljava/lang/String;

    .line 35
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->c:Z

    .line 36
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->d:Ljava/lang/String;

    .line 37
    return-void
.end method

.method public static a(Landroid/content/Context;ZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;-><init>(ZLjava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 50
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 60
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 61
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 63
    return-void

    .line 61
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
