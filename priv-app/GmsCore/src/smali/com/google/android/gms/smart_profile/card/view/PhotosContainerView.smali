.class public Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field private static b:I


# instance fields
.field private a:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 36
    sget v1, Lcom/google/android/gms/g;->bQ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    .line 42
    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 43
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->addView(Landroid/view/View;)V

    .line 43
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 73
    if-gtz v2, :cond_1

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 76
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 77
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v3

    invoke-virtual {v0, v5, v5, v1, v3}, Landroid/widget/ImageView;->layout(IIII)V

    .line 79
    if-lt v2, v6, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 83
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    sget v3, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    add-int/2addr v3, v0

    .line 84
    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    add-int v4, v3, v0

    .line 85
    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v0

    invoke-virtual {v1, v3, v5, v4, v0}, Landroid/widget/ImageView;->layout(IIII)V

    .line 87
    const/4 v0, 0x3

    if-lt v2, v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 91
    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v1

    sget v2, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    add-int/2addr v1, v2

    .line 92
    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v1

    .line 93
    invoke-virtual {v0, v3, v1, v4, v2}, Landroid/widget/ImageView;->layout(IIII)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    const/high16 v8, 0x40000000    # 2.0f

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    .line 51
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 53
    packed-switch v2, :pswitch_data_0

    .line 67
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->setMeasuredDimension(II)V

    .line 68
    return-void

    .line 56
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    move v0, v1

    .line 57
    goto :goto_0

    .line 59
    :pswitch_1
    div-int/lit8 v2, v1, 0x2

    .line 60
    sget v3, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    sub-int v3, v2, v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/widget/ImageView;->measure(II)V

    sub-int v0, v1, v3

    sget v4, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    sub-int v4, v0, v4

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v4, v3}, Landroid/widget/ImageView;->measure(II)V

    move v0, v2

    .line 61
    goto :goto_0

    .line 63
    :pswitch_2
    mul-int/lit8 v2, v1, 0x2

    div-int/lit8 v2, v2, 0x3

    .line 64
    sget v3, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    sub-int v3, v2, v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/widget/ImageView;->measure(II)V

    sub-int v0, v1, v3

    sget v4, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    sub-int v4, v0, v4

    div-int/lit8 v0, v3, 0x2

    sget v5, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    sub-int v5, v0, v5

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    invoke-static {v5, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    invoke-virtual {v0, v6, v7}, Landroid/widget/ImageView;->measure(II)V

    sub-int v0, v3, v5

    sget v3, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->b:I

    sub-int v3, v0, v3

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/PhotosContainerView;->a:Ljava/util/List;

    const/4 v5, 0x2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-static {v4, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v0, v4, v3}, Landroid/widget/ImageView;->measure(II)V

    move v0, v2

    goto/16 :goto_0

    .line 53
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
