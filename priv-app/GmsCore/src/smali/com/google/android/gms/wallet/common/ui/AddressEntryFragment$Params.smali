.class public final Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field a:Z

.field b:[I

.field c:I

.field d:Z

.field e:Z

.field f:[C

.field g:I

.field h:Ljava/util/ArrayList;

.field i:Ljava/util/Collection;

.field j:Z

.field k:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 456
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/r;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    .line 271
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    .line 275
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    .line 277
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    .line 279
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    .line 281
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    .line 283
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    .line 285
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->i:Ljava/util/Collection;

    .line 287
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->j:Z

    .line 289
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->k:Z

    .line 301
    return-void
.end method

.method public static a()Lcom/google/android/gms/wallet/common/ui/s;
    .locals 3

    .prologue
    .line 265
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;-><init>()V

    .line 266
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/s;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/wallet/common/ui/s;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;B)V

    return-object v1
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    .prologue
    .line 438
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 443
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeIntArray([I)V

    .line 445
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 446
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 447
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 448
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeCharArray([C)V

    .line 449
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 450
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Ljava/util/Collection;Landroid/os/Parcel;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->i:Ljava/util/Collection;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Ljava/util/Collection;Landroid/os/Parcel;)V

    .line 452
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->j:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 453
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->k:Z

    if-eqz v0, :cond_4

    :goto_4
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    .line 454
    return-void

    :cond_0
    move v0, v2

    .line 443
    goto :goto_0

    :cond_1
    move v0, v2

    .line 446
    goto :goto_1

    :cond_2
    move v0, v2

    .line 447
    goto :goto_2

    :cond_3
    move v0, v2

    .line 452
    goto :goto_3

    :cond_4
    move v1, v2

    .line 453
    goto :goto_4
.end method
