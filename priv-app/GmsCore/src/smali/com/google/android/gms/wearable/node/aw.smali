.class public final Lcom/google/android/gms/wearable/node/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/node/z;


# instance fields
.field public a:Lcom/google/android/gms/wearable/node/ba;

.field public b:Lcom/google/android/gms/wearable/node/au;

.field private final c:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final d:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 26
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v1, 0x5

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->d:Ljava/util/Map;

    return-void
.end method

.method private a(Lcom/google/android/gms/wearable/node/aa;Lcom/google/android/gms/wearable/node/a;Ljava/lang/String;Ljava/lang/String;[B)I
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, 0x2

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v1

    .line 126
    const-string v0, "rpctransport"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 127
    const-string v4, "rpctransport"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "sendRpc: "

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ", data "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p5, :cond_5

    move-object v0, p5

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_0
    new-instance v4, Lcom/google/android/gms/wearable/c/p;

    invoke-direct {v4}, Lcom/google/android/gms/wearable/c/p;-><init>()V

    .line 132
    iput v1, v4, Lcom/google/android/gms/wearable/c/p;->a:I

    .line 133
    iget-object v0, p2, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/wearable/c/p;->b:Ljava/lang/String;

    .line 134
    iget-object v0, p2, Lcom/google/android/gms/wearable/node/a;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/wearable/c/p;->c:Ljava/lang/String;

    .line 135
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/wearable/c/p;->d:Ljava/lang/String;

    .line 136
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/wearable/c/p;->f:Ljava/lang/String;

    .line 137
    if-eqz p5, :cond_1

    .line 138
    iput-object p5, v4, Lcom/google/android/gms/wearable/c/p;->g:[B

    .line 140
    :cond_1
    new-instance v5, Lcom/google/android/gms/wearable/c/m;

    invoke-direct {v5}, Lcom/google/android/gms/wearable/c/m;-><init>()V

    .line 141
    iput-object v4, v5, Lcom/google/android/gms/wearable/c/m;->j:Lcom/google/android/gms/wearable/c/p;

    .line 143
    const-string v0, "com.google.android.wearable.app"

    iget-object v4, p2, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "/s3"

    invoke-virtual {p4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0x8

    .line 145
    :goto_1
    const/4 v4, 0x0

    :try_start_0
    invoke-interface {p1, v0, v5, v4}, Lcom/google/android/gms/wearable/node/aa;->a(ILcom/google/android/gms/wearable/c/m;Lcom/google/android/gms/wearable/node/y;)V

    .line 146
    const-string v0, "rpctransport"

    const/4 v4, 0x2

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 147
    const-string v0, "rpctransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendRpc: message sent: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_2
    move v0, v2

    .line 163
    :goto_2
    if-eqz v0, :cond_9

    .line 164
    const-string v0, "rpctransport"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 165
    const-string v0, "rpctransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendRpc: removing dead request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_3
    const-string v0, "rpctransport"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 170
    const-string v0, "rpctransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendRpc: failed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    :cond_4
    const/4 v0, -0x1

    .line 174
    :goto_3
    return v0

    .line 127
    :cond_5
    array-length v0, p5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    .line 143
    :cond_6
    const/4 v0, 0x4

    goto :goto_1

    .line 150
    :catch_0
    move-exception v0

    .line 151
    const-string v2, "rpctransport"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 152
    const-string v2, "rpctransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendRpc: failed writing: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_7
    move v0, v3

    .line 161
    goto :goto_2

    .line 155
    :catch_1
    move-exception v0

    .line 156
    const-string v3, "rpctransport"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 157
    const-string v3, "rpctransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendRpc: failed writing: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 160
    :cond_8
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move v0, v2

    goto/16 :goto_2

    :cond_9
    move v0, v1

    .line 174
    goto :goto_3
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wearable/node/a;Ljava/lang/String;Ljava/lang/String;[B)I
    .locals 8

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->a:Lcom/google/android/gms/wearable/node/ba;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/node/ba;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    move-result v1

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->b:Lcom/google/android/gms/wearable/node/au;

    iget-object v2, p1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wearable/node/a;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/wearable/node/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wearable/node/a;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/aw;->a:Lcom/google/android/gms/wearable/node/ba;

    invoke-interface {v3}, Lcom/google/android/gms/wearable/node/ba;->b()Ljava/lang/String;

    move-result-object v5

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wearable/node/au;->a(ILcom/google/android/gms/wearable/node/a;Ljava/lang/String;[BLjava/lang/String;)V

    .line 116
    :goto_0
    return v1

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->d:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/aa;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wearable/node/aw;->a(Lcom/google/android/gms/wearable/node/aa;Lcom/google/android/gms/wearable/node/a;Ljava/lang/String;Ljava/lang/String;[B)I

    move-result v1

    goto :goto_0

    .line 105
    :cond_1
    const-string v0, "rpctransport"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    const-string v0, "rpctransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendRpc occurred on a potentially invalid nodeId: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_2
    const/4 v0, -0x1

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/aw;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v6, v0

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/node/aa;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 110
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wearable/node/aw;->a(Lcom/google/android/gms/wearable/node/aa;Lcom/google/android/gms/wearable/node/a;Ljava/lang/String;Ljava/lang/String;[B)I

    move-result v0

    .line 111
    if-gtz v6, :cond_4

    if-lez v0, :cond_4

    :goto_2
    move v6, v0

    .line 114
    goto :goto_1

    :cond_3
    move v1, v6

    .line 116
    goto :goto_0

    :cond_4
    move v0, v6

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/wearable/node/aa;)V
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->d:Ljava/util/Map;

    invoke-interface {p1}, Lcom/google/android/gms/wearable/node/aa;->a()Lcom/google/android/gms/wearable/c/i;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wearable/c/i;->a:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/wearable/c/m;Lcom/google/android/gms/wearable/node/y;)V
    .locals 6

    .prologue
    .line 36
    iget-object v0, p2, Lcom/google/android/gms/wearable/c/m;->j:Lcom/google/android/gms/wearable/c/p;

    if-eqz v0, :cond_1

    .line 37
    iget-object v4, p2, Lcom/google/android/gms/wearable/c/m;->j:Lcom/google/android/gms/wearable/c/p;

    const-string v0, "rpctransport"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v4, Lcom/google/android/gms/wearable/c/p;->g:[B

    const-string v1, "rpctransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onRpcRequest: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v4, Lcom/google/android/gms/wearable/c/p;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/gms/wearable/c/p;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", data "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez v0, :cond_2

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->a:Lcom/google/android/gms/wearable/node/ba;

    invoke-interface {v0}, Lcom/google/android/gms/wearable/node/ba;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v4, Lcom/google/android/gms/wearable/c/p;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/wearable/node/aw;->b:Lcom/google/android/gms/wearable/node/au;

    iget v1, v4, Lcom/google/android/gms/wearable/c/p;->a:I

    iget-object v2, v4, Lcom/google/android/gms/wearable/c/p;->b:Ljava/lang/String;

    iget-object v3, v4, Lcom/google/android/gms/wearable/c/p;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/wearable/node/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/wearable/node/a;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/gms/wearable/c/p;->f:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/gms/wearable/c/p;->g:[B

    move-object v5, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wearable/node/au;->a(ILcom/google/android/gms/wearable/node/a;Ljava/lang/String;[BLjava/lang/String;)V

    .line 39
    :cond_1
    return-void

    .line 37
    :cond_2
    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method
