.class public final Lcom/google/android/gms/common/ui/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 3

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/gms/common/ui/ErrorDialogActivity;

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_TITLE"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_MESSAGE"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_RESULT_CODE"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 41
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/ui/i;
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.ui.EXTRA_DIALOG_RESULT_CODE"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    return-object p0
.end method
