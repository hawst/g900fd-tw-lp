.class final Lcom/google/android/gms/wearable/service/l;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/service/WearableService;

.field private volatile b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wearable/service/WearableService;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 586
    iput-object p1, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    .line 587
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 584
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wearable/service/l;->b:Z

    .line 588
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/service/l;Lcom/google/android/gms/wearable/service/n;I)Landroid/os/Message;
    .locals 1

    .prologue
    .line 577
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wearable/service/l;->a(Lcom/google/android/gms/wearable/service/n;I)Landroid/os/Message;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/wearable/service/n;I)Landroid/os/Message;
    .locals 1

    .prologue
    .line 775
    invoke-virtual {p0, p2}, Lcom/google/android/gms/wearable/service/l;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 776
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 777
    return-object v0
.end method

.method private a(Lcom/google/android/gms/wearable/service/n;)V
    .locals 4

    .prologue
    .line 634
    invoke-direct {p0, p1}, Lcom/google/android/gms/wearable/service/l;->b(Lcom/google/android/gms/wearable/service/n;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 635
    invoke-static {p1}, Lcom/google/android/gms/wearable/service/l;->c(Lcom/google/android/gms/wearable/service/n;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 636
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wearable/service/l;->a(Lcom/google/android/gms/wearable/service/n;I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x1f40

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/wearable/service/l;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 652
    :goto_0
    return-void

    .line 639
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wearable/service/n;->c(Landroid/content/Context;)V

    .line 645
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wearable/service/l;->a(Lcom/google/android/gms/wearable/service/n;I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    goto :goto_0

    .line 650
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->i()V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/wearable/internal/ae;Lcom/google/android/gms/wearable/service/x;)Z
    .locals 4

    .prologue
    .line 764
    :try_start_0
    invoke-virtual {p1, p0}, Lcom/google/android/gms/wearable/service/x;->a(Lcom/google/android/gms/wearable/internal/ae;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 771
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 765
    :catch_0
    move-exception v0

    .line 766
    const-string v1, "WearableService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 767
    const-string v1, "WearableService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "publishEvent: Failure from remote exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 769
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/wearable/service/n;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v6, 0x3

    const/4 v1, 0x0

    .line 660
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 661
    const-string v1, "WearableService"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 662
    const-string v1, "WearableService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ensureBindStarted: hasConnection: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 718
    :cond_0
    :goto_0
    return v0

    .line 673
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/wearable/node/b;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/wearable/node/a;

    move-result-object v2

    .line 675
    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->a()Lcom/google/android/gms/wearable/node/a;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/gms/wearable/node/a;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 676
    const-string v0, "WearableService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ensureBindStarted: app does not match record\'s app key: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->a()Lcom/google/android/gms/wearable/node/a;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 678
    goto :goto_0

    .line 684
    :catch_0
    move-exception v0

    const-string v0, "WearableService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 685
    const-string v0, "WearableService"

    const-string v2, "ensureBindStarted: unrecognized app in package record."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    move v0, v1

    .line 687
    goto :goto_0

    .line 690
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/wearable/service/n;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 691
    const-string v0, "WearableService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 692
    const-string v0, "WearableService"

    const-string v2, "ensureBindStarted: package does not have a listener service."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    move v0, v1

    .line 694
    goto :goto_0

    .line 699
    :cond_5
    :try_start_1
    const-string v2, "WearableService"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 700
    const-string v2, "WearableService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "ensureBindStarted: binding to: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->c()Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 702
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {p1, v2}, Lcom/google/android/gms/wearable/service/n;->b(Landroid/content/Context;)Z
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 707
    :goto_1
    if-eqz v2, :cond_7

    .line 708
    const-string v1, "WearableService"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 709
    const-string v1, "WearableService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ensureBindStarted: started: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 703
    :catch_1
    move-exception v2

    .line 704
    const-string v3, "WearableService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ensureBindStarted: Permission denied connecting to "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v2, v1

    goto :goto_1

    .line 713
    :cond_7
    const-string v0, "WearableService"

    invoke-static {v0, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 714
    const-string v0, "WearableService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "ensureBindStarted: bind failed, app no longer not exist: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 717
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wearable/service/n;->c(Landroid/content/Context;)V

    move v0, v1

    .line 718
    goto/16 :goto_0
.end method

.method private static c(Lcom/google/android/gms/wearable/service/n;)Z
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v0, 0x1

    .line 729
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/n;->e()Lcom/google/android/gms/wearable/internal/ae;

    move-result-object v2

    .line 730
    if-nez v2, :cond_1

    .line 731
    const-string v1, "WearableService"

    invoke-static {v1, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 732
    const-string v1, "WearableService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "PackageRecord.flush: No service ready: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 755
    :cond_0
    :goto_0
    return v0

    .line 737
    :cond_1
    const-string v1, "WearableService"

    const/4 v3, 0x2

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 738
    const-string v1, "WearableService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PackageRecord.flush: ready: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 740
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/n;->g()Lcom/google/android/gms/wearable/service/x;

    move-result-object v1

    .line 741
    :goto_1
    if-eqz v1, :cond_0

    .line 742
    invoke-static {v2, v1}, Lcom/google/android/gms/wearable/service/l;->a(Lcom/google/android/gms/wearable/internal/ae;Lcom/google/android/gms/wearable/service/x;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 744
    const/4 v0, 0x0

    goto :goto_0

    .line 747
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/n;->h()Lcom/google/android/gms/wearable/service/x;

    .line 748
    const-string v3, "WearableService"

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 749
    const-string v3, "WearableService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "PackageRecord.flush: published: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 753
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/n;->g()Lcom/google/android/gms/wearable/service/x;

    move-result-object v1

    goto :goto_1
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 781
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    const-string v0, "WearableService"

    const-string v1, "Shutting down event handler."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 784
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wearable/service/l;->b:Z

    .line 785
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wearable/service/l;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 786
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/l;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 788
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/WearableService;->e(Lcom/google/android/gms/wearable/service/WearableService;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/n;

    .line 789
    iget-object v2, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/service/n;->c(Landroid/content/Context;)V

    goto :goto_0

    .line 791
    :cond_1
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x3

    .line 592
    const-string v0, "WearableService"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-ge v0, v1, :cond_3

    .line 596
    const-string v0, "WearableService"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 597
    const-string v0, "WearableService"

    const-string v1, "handleMessage: JBmr2+ required."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 599
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/l;->a()V

    .line 630
    :cond_2
    :goto_0
    return-void

    .line 602
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/wearable/service/l;->b:Z

    if-eqz v0, :cond_4

    .line 603
    const-string v0, "WearableService"

    const-string v1, "handleMessage: shutdown."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 607
    :cond_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/wearable/service/n;

    .line 608
    invoke-virtual {p0, v4, v0}, Lcom/google/android/gms/wearable/service/l;->removeMessages(ILjava/lang/Object;)V

    .line 609
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_5

    .line 610
    invoke-direct {p0, v0}, Lcom/google/android/gms/wearable/service/l;->a(Lcom/google/android/gms/wearable/service/n;)V

    goto :goto_0

    .line 611
    :cond_5
    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_6

    .line 616
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "binder"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBinder(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/service/n;->a(Landroid/os/IBinder;)V

    .line 618
    invoke-direct {p0, v0}, Lcom/google/android/gms/wearable/service/l;->a(Lcom/google/android/gms/wearable/service/n;)V

    goto :goto_0

    .line 619
    :cond_6
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v3, :cond_7

    .line 622
    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/n;->f()V

    goto :goto_0

    .line 623
    :cond_7
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v4, :cond_2

    .line 624
    const-string v1, "WearableService"

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 625
    const-string v1, "WearableService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleMessage: unbind "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/n;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    :cond_8
    invoke-static {v0}, Lcom/google/android/gms/wearable/service/l;->c(Lcom/google/android/gms/wearable/service/n;)Z

    .line 628
    iget-object v1, p0, Lcom/google/android/gms/wearable/service/l;->a:Lcom/google/android/gms/wearable/service/WearableService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/service/n;->c(Landroid/content/Context;)V

    goto :goto_0
.end method
