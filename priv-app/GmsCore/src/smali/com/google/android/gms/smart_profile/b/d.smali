.class public final Lcom/google/android/gms/smart_profile/b/d;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# instance fields
.field private f:Ljava/util/List;

.field private g:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/people/identity/models/Person;)V
    .locals 3

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 32
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/people/identity/models/Person;->B()Z

    move-result v0

    if-nez v0, :cond_1

    .line 41
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 36
    invoke-interface {p2}, Lcom/google/android/gms/people/identity/models/Person;->A()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 37
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 38
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/models/s;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/s;->f()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 40
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/b/d;->g:[Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/b/d;->f:Ljava/util/List;

    .line 76
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 77
    invoke-super {p0, p1}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V

    .line 79
    :cond_0
    return-void
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 24
    check-cast p1, Ljava/util/List;

    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/b/d;->a(Ljava/util/List;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 13

    .prologue
    .line 24
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "android.hardware.telephony"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/d;->g:[Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Lcom/google/android/gms/smart_profile/card/a;->a:Lcom/google/android/gms/smart_profile/card/a;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/smart_profile/card/a;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/card/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/smart_profile/card/a;->a:Lcom/google/android/gms/smart_profile/card/a;

    :cond_2
    sget-object v7, Lcom/google/android/gms/smart_profile/card/a;->a:Lcom/google/android/gms/smart_profile/card/a;

    iget-object v8, p0, Lcom/google/android/gms/smart_profile/b/d;->g:[Ljava/lang/String;

    iget-object v9, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    array-length v11, v8

    const/4 v0, 0x0

    move v6, v0

    :goto_1
    if-ge v6, v11, :cond_7

    aget-object v1, v8, v6

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v9}, Lcom/google/android/gms/smart_profile/card/a;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "allow_voicemails"

    const-string v3, "true"

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    :goto_2
    invoke-static {v9}, Lcom/google/android/gms/smart_profile/card/a;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x7

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "date"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "duration"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x5

    const-string v4, "new"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string v4, "voicemail_uri"

    aput-object v4, v2, v3

    :goto_3
    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v12, "date DESC LIMIT "

    invoke-direct {v5, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v12, Lcom/google/android/gms/smart_profile/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v12}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v12

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-gtz v1, :cond_6

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_4
    invoke-interface {v10, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_1

    :cond_4
    sget-object v2, Landroid/provider/CallLog$Calls;->CONTENT_FILTER_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    goto :goto_2

    :cond_5
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "date"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "type"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "duration"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string v4, "number"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string v4, "new"

    aput-object v4, v2, v3

    goto :goto_3

    :cond_6
    invoke-static {v9, v0}, Lcom/google/android/gms/smart_profile/card/a;->a(Landroid/content/Context;Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v0

    goto :goto_4

    :cond_7
    new-instance v0, Lcom/google/android/gms/smart_profile/card/b;

    invoke-direct {v0, v7}, Lcom/google/android/gms/smart_profile/card/b;-><init>(Lcom/google/android/gms/smart_profile/card/a;)V

    invoke-static {v10, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    :goto_5
    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    if-lez v1, :cond_8

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/ag;

    iget-object v3, v0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    add-int/lit8 v0, v1, -0x1

    invoke-interface {v10, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/ag;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    invoke-virtual {v3, v0}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_8
    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v3, v0, :cond_a

    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    :cond_a
    invoke-static {v2}, Lcom/google/android/gms/smart_profile/ar;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/support/v4/a/a;->e()V

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/d;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/d;->f:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/b/d;->a(Ljava/util/List;)V

    .line 51
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/d;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/d;->f:Ljava/util/List;

    if-nez v0, :cond_2

    .line 52
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 54
    :cond_2
    return-void
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/d;->b()Z

    .line 60
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Landroid/support/v4/a/a;->g()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/b/d;->b()Z

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/d;->f:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/b/d;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 71
    :cond_0
    return-void
.end method
