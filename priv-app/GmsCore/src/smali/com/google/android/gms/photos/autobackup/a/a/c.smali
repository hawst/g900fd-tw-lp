.class public final Lcom/google/android/gms/photos/autobackup/a/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/i;


# instance fields
.field private final a:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

.field private final b:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/a/a/c;->b:Lcom/google/android/gms/common/api/Status;

    .line 17
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/a/a/c;->a:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    .line 18
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/photos/autobackup/model/LocalFolder;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/a/a/c;->a:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/a/a/c;->b:Lcom/google/android/gms/common/api/Status;

    .line 23
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a/a/c;->b:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/photos/autobackup/model/LocalFolder;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a/a/c;->a:Lcom/google/android/gms/photos/autobackup/model/LocalFolder;

    return-object v0
.end method
