.class final Lcom/google/android/gms/wearable/service/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/service/f;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wearable/internal/ab;

.field final synthetic b:Lcom/google/android/gms/wearable/service/z;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wearable/service/z;Lcom/google/android/gms/wearable/internal/ab;)V
    .locals 0

    .prologue
    .line 919
    iput-object p1, p0, Lcom/google/android/gms/wearable/service/aq;->b:Lcom/google/android/gms/wearable/service/z;

    iput-object p2, p0, Lcom/google/android/gms/wearable/service/aq;->a:Lcom/google/android/gms/wearable/internal/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 923
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/aq;->b:Lcom/google/android/gms/wearable/service/z;

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/z;->g(Lcom/google/android/gms/wearable/service/z;)Landroid/telecom/TelecomManager;

    move-result-object v0

    if-nez v0, :cond_0

    .line 924
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/aq;->a:Lcom/google/android/gms/wearable/internal/ab;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 933
    :goto_0
    return-void

    .line 926
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/aq;->b:Lcom/google/android/gms/wearable/service/z;

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/z;->g(Lcom/google/android/gms/wearable/service/z;)Landroid/telecom/TelecomManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telecom/TelecomManager;->acceptRingingCall()V

    .line 927
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/aq;->a:Lcom/google/android/gms/wearable/internal/ab;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 929
    :catch_0
    move-exception v0

    .line 930
    const-string v1, "WearableService"

    const-string v2, "acceptRingingCall: exception during processing"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 931
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/aq;->a:Lcom/google/android/gms/wearable/internal/ab;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/wearable/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
