.class public final Lcom/google/android/gms/cast/b/x;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/cast/b/w;

.field private b:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/b/w;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/gms/cast/b/x;->a:Lcom/google/android/gms/cast/b/w;

    .line 17
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 20
    iput p1, p0, Lcom/google/android/gms/cast/b/x;->b:I

    .line 21
    return-void
.end method

.method public final available()I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/gms/cast/b/x;->b:I

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 30
    iget v1, p0, Lcom/google/android/gms/cast/b/x;->b:I

    if-nez v1, :cond_1

    .line 40
    :cond_0
    :goto_0
    return v0

    .line 34
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/cast/b/x;->a:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/w;->e()I

    move-result v2

    if-gtz v2, :cond_2

    const/4 v1, 0x0

    .line 35
    :goto_1
    if-eqz v1, :cond_0

    .line 39
    iget v0, p0, Lcom/google/android/gms/cast/b/x;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/x;->b:I

    .line 40
    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    goto :goto_0

    .line 34
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/w;->g()B

    move-result v1

    invoke-static {v1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v1

    goto :goto_1
.end method

.method public final read([BII)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    .line 46
    if-ltz p2, :cond_0

    if-ltz p3, :cond_0

    add-int v2, p2, p3

    array-length v3, p1

    if-le v2, v3, :cond_1

    .line 47
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 49
    :cond_1
    iget v2, p0, Lcom/google/android/gms/cast/b/x;->b:I

    if-le p3, v2, :cond_2

    .line 50
    iget p3, p0, Lcom/google/android/gms/cast/b/x;->b:I

    .line 53
    :cond_2
    iget v2, p0, Lcom/google/android/gms/cast/b/x;->b:I

    if-nez v2, :cond_3

    move p3, v1

    .line 62
    :goto_0
    return p3

    .line 57
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/cast/b/x;->a:Lcom/google/android/gms/cast/b/w;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/b/w;->e()I

    move-result v3

    if-ge v3, p3, :cond_4

    :goto_1
    if-nez v0, :cond_8

    move p3, v1

    .line 58
    goto :goto_0

    .line 57
    :cond_4
    iget-boolean v3, v2, Lcom/google/android/gms/cast/b/w;->e:Z

    if-eqz v3, :cond_6

    :goto_2
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v3, v2, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v4, v2, Lcom/google/android/gms/cast/b/w;->b:I

    invoke-static {v3, v4, p1, p2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/b/w;->a(I)V

    sub-int v3, p3, v0

    if-lez v3, :cond_5

    add-int/2addr v0, p2

    iget-object v4, v2, Lcom/google/android/gms/cast/b/w;->a:[B

    iget v5, v2, Lcom/google/android/gms/cast/b/w;->b:I

    invoke-static {v4, v5, p1, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/cast/b/w;->a(I)V

    :cond_5
    const/4 v0, 0x1

    goto :goto_1

    :cond_6
    iget v0, v2, Lcom/google/android/gms/cast/b/w;->b:I

    iget v3, v2, Lcom/google/android/gms/cast/b/w;->c:I

    if-ge v0, v3, :cond_7

    iget v0, v2, Lcom/google/android/gms/cast/b/w;->c:I

    iget v3, v2, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v0, v3

    goto :goto_2

    :cond_7
    iget-object v0, v2, Lcom/google/android/gms/cast/b/w;->a:[B

    array-length v0, v0

    iget v3, v2, Lcom/google/android/gms/cast/b/w;->b:I

    sub-int/2addr v0, v3

    goto :goto_2

    .line 61
    :cond_8
    iget v0, p0, Lcom/google/android/gms/cast/b/x;->b:I

    sub-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/gms/cast/b/x;->b:I

    goto :goto_0
.end method
