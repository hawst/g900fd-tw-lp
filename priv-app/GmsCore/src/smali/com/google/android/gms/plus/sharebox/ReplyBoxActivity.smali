.class public Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/sharebox/ag;
.implements Lcom/google/android/gms/plus/sharebox/ak;
.implements Lcom/google/android/gms/plus/sharebox/o;


# instance fields
.field private a:Lcom/google/android/gms/plus/sharebox/ae;

.field private b:Lcom/google/android/gms/plus/sharebox/ah;

.field private c:Lcom/google/android/gms/common/people/data/Audience;

.field private d:Lcom/google/android/gms/plus/audience/bg;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/gms/plus/sharebox/bg;

.field private final g:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 65
    new-instance v0, Lcom/google/android/gms/plus/sharebox/ac;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/ac;-><init>(Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->g:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;)Lcom/google/android/gms/plus/sharebox/n;
    .locals 2

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "confirm_action_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/gms/p;->vk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/n;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/n;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Lcom/google/android/gms/plus/sharebox/n;

    goto :goto_0
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 343
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b(I)V

    .line 344
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->i()V

    .line 345
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 348
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 349
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 310
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "post_error_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/ui/j;

    .line 312
    if-nez v0, :cond_0

    .line 313
    sget v0, Lcom/google/android/gms/p;->vn:I

    invoke-static {v0}, Lcom/google/android/gms/common/ui/j;->a(I)Lcom/google/android/gms/common/ui/j;

    move-result-object v0

    .line 318
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "post_error_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/ui/j;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 320
    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->setResult(I)V

    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->finish()V

    .line 354
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/sharebox/bg;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bg;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 227
    if-nez p1, :cond_0

    .line 228
    sget v0, Lcom/google/android/gms/p;->vl:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    .line 242
    :goto_0
    return-void

    .line 231
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 233
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/common/c;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 235
    :catch_0
    move-exception v0

    const-string v0, "ReplyBox"

    const-string v1, "Failed to start connection resolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 236
    sget v0, Lcom/google/android/gms/p;->vl:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    .line 240
    :cond_1
    const-string v0, "ReplyBox"

    const-string v1, "Failed connection resolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    sget v0, Lcom/google/android/gms/p;->vl:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 297
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 298
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->i()V

    .line 299
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->dismiss()V

    .line 255
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 257
    sget v0, Lcom/google/android/gms/p;->vo:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b(I)V

    .line 258
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->setResult(I)V

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->finish()V

    .line 264
    :goto_0
    return-void

    .line 261
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 262
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->h()V

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/plus/sharebox/ah;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    return-object v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/ae;->d()V

    .line 249
    return-void
.end method

.method public final e()Lcom/google/android/gms/plus/audience/bg;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->d:Lcom/google/android/gms/plus/audience/bg;

    return-object v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/ae;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ae;->a(Z)V

    .line 278
    return-void
.end method

.method public final g()V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/ae;->b()Lcom/google/android/gms/plus/model/posts/Comment;

    move-result-object v0

    .line 283
    if-eqz v0, :cond_0

    .line 284
    sget v1, Lcom/google/android/gms/p;->vK:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/f/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    invoke-virtual {v2}, Landroid/support/v4/app/aj;->b()I

    .line 285
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/plus/model/posts/Comment;)V

    .line 289
    :goto_0
    return-void

    .line 287
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->h()V

    goto :goto_0
.end method

.method public getCallingPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->e:Ljava/lang/String;

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 187
    packed-switch p1, :pswitch_data_0

    .line 196
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->onActivityResult(IILandroid/content/Intent;)V

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 189
    :pswitch_0
    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 190
    const-string v0, "ReplyBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to resolve connection/account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 191
    sget v0, Lcom/google/android/gms/p;->vl:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    .line 187
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 171
    :goto_0
    return-void

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/ah;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 170
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 83
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->e:Ljava/lang/String;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "ReplyBox"

    const-string v1, "No accounts available to reply"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    sget v0, Lcom/google/android/gms/p;->vl:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    .line 141
    :goto_0
    return-void

    .line 89
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 90
    sget v0, Lcom/google/android/gms/p;->vm:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    .line 95
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/plus/sharebox/at;->b(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 96
    const-string v0, "ReplyBox"

    const-string v1, "Invalid reply action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->i()V

    goto :goto_0

    .line 101
    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/sharebox/bg;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bg;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.plus.intent.extra.PLUS_PAGE_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    new-instance v3, Lcom/google/android/gms/plus/model/posts/PlusPage;

    invoke-direct {v3, v2, v4, v4}, Lcom/google/android/gms/plus/model/posts/PlusPage;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v3, v0, Lcom/google/android/gms/plus/sharebox/bg;->b:Lcom/google/android/gms/plus/model/posts/PlusPage;

    :cond_3
    const-string v2, "com.google.android.gms.plus.intent.extra.INTERNAL_REPLY_ACTIVITY_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/plus/sharebox/bg;->g:Ljava/lang/String;

    const-string v2, "com.google.android.gms.plus.intent.extra.INTERNAL_REPLY_ADD_COMMENT_HINT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/plus/sharebox/bg;->h:Ljava/lang/String;

    const-string v2, "com.google.android.gms.plus.intent.extra.SHARE_CONTEXT_TYPE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    const-string v2, "com.google.android.gms.plus.intent.extra.CLIENT_APPLICATION_ID"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 104
    const-string v0, "ReplyBox"

    const-string v1, "No account name provided."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    sget v0, Lcom/google/android/gms/p;->vl:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto :goto_0

    .line 108
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_6

    .line 109
    const-string v0, "ReplyBox"

    const-string v1, "No activity ID provided."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    sget v0, Lcom/google/android/gms/p;->vl:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a(I)V

    goto/16 :goto_0

    .line 108
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 115
    :cond_6
    sget v0, Lcom/google/android/gms/l;->eE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->setContentView(I)V

    .line 117
    if-eqz p1, :cond_9

    .line 118
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->c:Lcom/google/android/gms/common/people/data/Audience;

    .line 122
    :goto_2
    new-instance v0, Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->c:Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/audience/bg;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->d:Lcom/google/android/gms/plus/audience/bg;

    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 126
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v2

    .line 127
    const-string v0, "reply_worker_fragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/ah;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    if-nez v0, :cond_7

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/ah;

    const-string v3, "reply_worker_fragment"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 134
    :cond_7
    const-string v0, "reply_fragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/ae;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    if-nez v0, :cond_8

    .line 136
    new-instance v0, Lcom/google/android/gms/plus/sharebox/ae;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    .line 137
    sget v0, Lcom/google/android/gms/j;->pC:I

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    const-string v3, "reply_fragment"

    invoke-virtual {v2, v0, v1, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 139
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/ae;

    invoke-virtual {v2, v0}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 140
    invoke-virtual {v2}, Landroid/support/v4/app/aj;->a()I

    goto/16 :goto_0

    .line 120
    :cond_9
    sget-object v0, Lcom/google/android/gms/common/people/data/a;->a:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->c:Lcom/google/android/gms/common/people/data/Audience;

    goto :goto_2
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 145
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "confirm_action_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/n;

    .line 149
    if-eqz v0, :cond_0

    .line 150
    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/n;->a(Lcom/google/android/gms/plus/sharebox/o;)V

    .line 152
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 157
    const-string v0, "audience"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->c:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 158
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 175
    sget v0, Lcom/google/android/gms/j;->pC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 176
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/d;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ReplyBoxActivity;->onBackPressed()V

    .line 179
    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 180
    const/4 v0, 0x1

    .line 182
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
