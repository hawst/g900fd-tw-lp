.class final Lcom/google/android/gms/games/ui/common/leaderboards/s;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final k:Landroid/view/View;

.field private final n:Landroid/view/View;

.field private final o:Landroid/view/View;

.field private final p:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    .line 124
    sget v0, Lcom/google/android/gms/j;->gN:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->k:Landroid/view/View;

    .line 126
    sget v0, Lcom/google/android/gms/j;->qT:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->n:Landroid/view/View;

    .line 127
    sget v0, Lcom/google/android/gms/j;->gj:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->o:Landroid/view/View;

    .line 128
    sget v0, Lcom/google/android/gms/j;->sI:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->p:Landroid/view/View;

    .line 129
    return-void
.end method

.method private a(Landroid/view/View;Lcom/google/android/gms/games/e/e;II)V
    .locals 10

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/r;

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 207
    sget v1, Lcom/google/android/gms/j;->pt:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 208
    sget v2, Lcom/google/android/gms/j;->pr:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;

    .line 210
    sget v3, Lcom/google/android/gms/j;->ps:I

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 211
    sget v4, Lcom/google/android/gms/j;->pu:I

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 214
    invoke-interface {p2}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/gms/games/Player;

    .line 215
    invoke-virtual {v2, v5, p3}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->a(Lcom/google/android/gms/games/Player;I)V

    .line 216
    invoke-virtual {v2, v5}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->setTag(Ljava/lang/Object;)V

    .line 217
    invoke-virtual {v2, p0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 220
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->e(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "Must set playerId"

    invoke-static {v7, v8}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 221
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->e(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/p;->kh:I

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 225
    :goto_0
    invoke-interface {p2}, Lcom/google/android/gms/games/e/e;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 226
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 227
    invoke-interface {p2}, Lcom/google/android/gms/games/e/e;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 229
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 230
    invoke-virtual {v6, p4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 231
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 232
    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 233
    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/widget/LeaderboardPodiumAvatarView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 235
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 236
    return-void

    .line 221
    :cond_0
    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private o()Landroid/graphics/drawable/Drawable;
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/r;

    .line 251
    new-instance v1, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    .line 252
    invoke-virtual {v1, v5}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    .line 253
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 255
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/k;->q:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-float v2, v2

    .line 256
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->d(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    const/16 v0, 0x8

    new-array v0, v0, [F

    aput v2, v0, v5

    aput v2, v0, v6

    aput v2, v0, v7

    aput v2, v0, v8

    const/4 v3, 0x4

    aput v2, v0, v3

    const/4 v3, 0x5

    aput v2, v0, v3

    const/4 v3, 0x6

    aput v2, v0, v3

    const/4 v3, 0x7

    aput v2, v0, v3

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    .line 264
    :goto_0
    return-object v1

    .line 261
    :cond_0
    const/16 v0, 0x8

    new-array v0, v0, [F

    aput v2, v0, v5

    aput v2, v0, v6

    aput v2, v0, v7

    aput v2, v0, v8

    const/4 v2, 0x4

    aput v4, v0, v2

    const/4 v2, 0x5

    aput v4, v0, v2

    const/4 v2, 0x6

    aput v4, v0, v2

    const/4 v2, 0x7

    aput v4, v0, v2

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadii([F)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 6

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 133
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/r;

    .line 136
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->a(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 138
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->b(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    const-string v2, "Can\'t have third place without second"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 139
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    const-string v2, "Can\'t have third place without first"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->o:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v2

    sget v4, Lcom/google/android/gms/h;->U:I

    sget v5, Lcom/google/android/gms/g;->W:I

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->a(Landroid/view/View;Lcom/google/android/gms/games/e/e;II)V

    .line 145
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->n:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->b(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v2

    sget v4, Lcom/google/android/gms/h;->V:I

    sget v5, Lcom/google/android/gms/g;->X:I

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->a(Landroid/view/View;Lcom/google/android/gms/games/e/e;II)V

    .line 148
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->p:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->a(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v2

    sget v4, Lcom/google/android/gms/h;->W:I

    sget v5, Lcom/google/android/gms/g;->Y:I

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->a(Landroid/view/View;Lcom/google/android/gms/games/e/e;II)V

    .line 177
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 178
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->k:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 181
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->d(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 184
    sget v4, Lcom/google/android/gms/g;->D:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 187
    :goto_1
    invoke-virtual {v1, v3, v3, v3, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    .line 188
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->k:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 191
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 192
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->k:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->o()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 198
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->k:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->jM:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 199
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->d(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v3, 0x8

    :cond_1
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 200
    return-void

    .line 151
    :cond_2
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->b(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 153
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    const-string v2, "Can\'t have second place without first"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->p:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->o:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v2

    sget v4, Lcom/google/android/gms/h;->U:I

    sget v5, Lcom/google/android/gms/g;->W:I

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->a(Landroid/view/View;Lcom/google/android/gms/games/e/e;II)V

    .line 162
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->n:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->b(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v2

    sget v4, Lcom/google/android/gms/h;->V:I

    sget v5, Lcom/google/android/gms/g;->X:I

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->a(Landroid/view/View;Lcom/google/android/gms/games/e/e;II)V

    goto :goto_0

    .line 165
    :cond_3
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 168
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->n:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->p:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->o:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->c(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/e/e;

    move-result-object v2

    sget v4, Lcom/google/android/gms/h;->U:I

    sget v5, Lcom/google/android/gms/g;->W:I

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->a(Landroid/view/View;Lcom/google/android/gms/games/e/e;II)V

    goto/16 :goto_0

    .line 194
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->k:Landroid/view/View;

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/s;->o()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_5
    move v2, v3

    goto/16 :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/s;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/leaderboards/r;

    .line 241
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 242
    instance-of v2, v1, Lcom/google/android/gms/games/Player;

    if-eqz v2, :cond_0

    .line 243
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/leaderboards/r;->f(Lcom/google/android/gms/games/ui/common/leaderboards/r;)Lcom/google/android/gms/games/ui/common/leaderboards/l;

    move-result-object v2

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {v2, v0}, Lcom/google/android/gms/games/ui/common/leaderboards/l;->a(Lcom/google/android/gms/games/Player;)V

    .line 245
    :cond_0
    return-void
.end method
