.class public final Lcom/google/android/gms/drive/a/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/a/a/q;

.field b:Ljava/lang/Runnable;

.field private final c:Lcom/google/android/gms/drive/database/r;

.field private final d:Lcom/google/android/gms/drive/g/n;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/gms/drive/a/a/h;

.field private final g:Lcom/google/android/gms/drive/g/aw;

.field private final h:Lcom/google/android/gms/drive/a/a/j;

.field private final i:Lcom/google/android/gms/drive/g/au;

.field private final j:Lcom/google/android/gms/drive/b/d;

.field private final k:Z

.field private final l:Ljava/util/List;

.field private final m:Ljava/util/List;

.field private final n:Ljava/util/Map;

.field private o:Ljava/util/concurrent/ThreadPoolExecutor;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;Lcom/google/android/gms/drive/a/a/h;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/g/au;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/q;)V
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->l:Ljava/util/List;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    .line 101
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    .line 126
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    .line 127
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->d:Lcom/google/android/gms/drive/g/n;

    .line 128
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->e:Landroid/content/Context;

    .line 129
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/h;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->f:Lcom/google/android/gms/drive/a/a/h;

    .line 130
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/aw;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->g:Lcom/google/android/gms/drive/g/aw;

    .line 131
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/j;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->h:Lcom/google/android/gms/drive/a/a/j;

    .line 132
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/au;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->i:Lcom/google/android/gms/drive/g/au;

    .line 133
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/d;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->j:Lcom/google/android/gms/drive/b/d;

    .line 134
    invoke-static {p9}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/q;

    iput-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->a:Lcom/google/android/gms/drive/a/a/q;

    .line 135
    sget-object v0, Lcom/google/android/gms/drive/ai;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/a/a/m;->k:Z

    .line 136
    return-void
.end method

.method private static a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/be;)Lcom/google/android/gms/drive/a/a/k;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 172
    :try_start_0
    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/be;->a:J

    invoke-interface {p0, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(J)Lcom/google/android/gms/drive/database/model/a;

    move-result-object v3

    if-eqz v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    new-instance v0, Lorg/json/JSONObject;

    iget-object v4, p1, Lcom/google/android/gms/drive/database/model/be;->b:Ljava/lang/String;

    invoke-direct {v0, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v4, "forward"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    const-string v5, "reverse"

    invoke-virtual {v0, v5}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/a/a/h;->a(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)Lcom/google/android/gms/drive/a/c;

    move-result-object v4

    invoke-static {v3, v0}, Lcom/google/android/gms/drive/a/a/h;->a(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)Lcom/google/android/gms/drive/a/c;

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/drive/a/g;

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/drive/a/g;-><init>(Lcom/google/android/gms/drive/a/c;Lcom/google/android/gms/drive/a/c;)V

    .line 173
    new-instance v0, Lcom/google/android/gms/drive/a/a/k;

    invoke-direct {v0, p1, v3}, Lcom/google/android/gms/drive/a/a/k;-><init>(Lcom/google/android/gms/drive/database/model/be;Lcom/google/android/gms/drive/a/g;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 172
    goto :goto_0

    .line 174
    :catch_0
    move-exception v0

    .line 175
    const-string v3, "PendingActionManager"

    const-string v4, "Discarded action that could not be deserialized."

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 176
    const-string v0, "PendingActionManager"

    const-string v3, "Discarded action that could not be deserialized: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 177
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/be;->j()V

    .line 178
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static a(Lcom/google/android/gms/drive/a/a/k;ZIZLcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/j;)V
    .locals 7

    .prologue
    .line 616
    iget-object v1, p5, Lcom/google/android/gms/drive/b/d;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 617
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    .line 618
    invoke-interface {v0, p4}, Lcom/google/android/gms/drive/a/c;->a(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/a/a/i;

    move-result-object v2

    .line 620
    if-eqz p1, :cond_0

    .line 621
    const-string v0, "ExecutingAction"

    const-string v3, "Undoing action: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/g;->b:Lcom/google/android/gms/drive/a/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v3, "ExecutingAction"

    const-string v4, "Applying locally undo action: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-interface {v0, p4}, Lcom/google/android/gms/drive/a/c;->c(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/a/g;
    :try_end_1
    .catch Lcom/google/android/gms/drive/a/h; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/drive/a/i; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 625
    :try_start_2
    invoke-virtual {p6, p2, v2, p3}, Lcom/google/android/gms/drive/a/a/j;->a(ILcom/google/android/gms/drive/a/a/i;Z)V

    .line 628
    :cond_1
    monitor-exit v1

    return-void

    .line 621
    :catch_0
    move-exception v0

    const-string v3, "ExecutingAction"

    const-string v4, "Failed to undo action."

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 628
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 621
    :catch_1
    move-exception v0

    :try_start_3
    const-string v3, "ExecutingAction"

    const-string v4, "Failed to undo action. Unexpected conflict on undo"

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/drive/a/c;Ljava/util/List;)Z
    .locals 2

    .prologue
    .line 574
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/k;

    .line 575
    iget-object v0, v0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    invoke-interface {v0, p0}, Lcom/google/android/gms/drive/a/c;->a(Lcom/google/android/gms/drive/a/c;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    const/4 v0, 0x1

    .line 579
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Lcom/google/android/gms/drive/a/c;Ljava/util/List;)Ljava/util/List;
    .locals 4

    .prologue
    .line 589
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/c;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 598
    :goto_0
    return-object p1

    .line 592
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 593
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/k;

    .line 594
    iget-object v3, v0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v3, v3, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    invoke-interface {v3, p0}, Lcom/google/android/gms/drive/a/c;->a(Lcom/google/android/gms/drive/a/c;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 595
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object p1, v1

    .line 598
    goto :goto_0
.end method

.method private declared-synchronized b(Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/a;Ljava/util/List;)Ljava/util/List;
    .locals 5

    .prologue
    .line 542
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 543
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->l:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/k;

    .line 544
    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 548
    iget-object v3, v0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v3, v3, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    .line 551
    invoke-interface {v3}, Lcom/google/android/gms/drive/a/c;->g()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/google/android/gms/drive/auth/AppIdentity;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Lcom/google/android/gms/drive/a/c;->a()Lcom/google/android/gms/drive/database/model/a;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/google/android/gms/drive/database/model/a;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 553
    invoke-interface {v3}, Lcom/google/android/gms/drive/a/c;->b()Lcom/google/android/gms/drive/a/a/l;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/a/a/l;->b()Ljava/lang/String;

    move-result-object v4

    .line 554
    if-eqz v4, :cond_1

    invoke-interface {p3, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_1
    invoke-static {v3, v1}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/c;Ljava/util/List;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 556
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 542
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 561
    :cond_3
    :try_start_1
    invoke-static {v3, v1}, Lcom/google/android/gms/drive/a/a/m;->b(Lcom/google/android/gms/drive/a/c;Ljava/util/List;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    move-object v1, v0

    .line 563
    goto :goto_0

    .line 564
    :cond_4
    monitor-exit p0

    return-object v1
.end method

.method private declared-synchronized b(Lcom/google/android/gms/drive/a/a/k;)V
    .locals 3

    .prologue
    .line 373
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 374
    if-nez v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PendingActionManager"

    const-string v2, "ExecutingAction was not found in the list of executing actions"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 381
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/a/a/m;->k:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 382
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PendingActionManager"

    const-string v2, "Executing action list is not empty"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 385
    :cond_1
    monitor-exit p0

    return-void

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized c(Lcom/google/android/gms/drive/a/a/k;)V
    .locals 3

    .prologue
    .line 389
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 390
    if-nez v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "PendingActionManager"

    const-string v2, "ExecutingAction was not found in the list of pending actions"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    monitor-exit p0

    return-void

    .line 389
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()V
    .locals 5

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->l()Lcom/google/android/gms/drive/database/b/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 157
    :try_start_1
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/be;

    .line 158
    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/m;->f:Lcom/google/android/gms/drive/a/a/h;

    invoke-static {v3, v0}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/be;)Lcom/google/android/gms/drive/a/a/k;

    move-result-object v0

    .line 160
    if-eqz v0, :cond_0

    .line 161
    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/m;->l:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 165
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 155
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 165
    :cond_1
    :try_start_3
    invoke-interface {v1}, Lcom/google/android/gms/drive/database/b/d;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 166
    monitor-exit p0

    return-void
.end method

.method private declared-synchronized h()Z
    .locals 1

    .prologue
    .line 221
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()Lcom/google/android/gms/drive/a/a/k;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 247
    monitor-enter p0

    :try_start_0
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->d:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v3

    .line 278
    :goto_0
    monitor-exit p0

    return-object v0

    .line 254
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/k;

    .line 256
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 257
    iget-object v1, v0, Lcom/google/android/gms/drive/a/a/k;->b:Lcom/google/android/gms/drive/a/g;

    iget-object v1, v1, Lcom/google/android/gms/drive/a/g;->a:Lcom/google/android/gms/drive/a/c;

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v7, p0, Lcom/google/android/gms/drive/a/a/m;->d:Lcom/google/android/gms/drive/g/n;

    iget-object v8, p0, Lcom/google/android/gms/drive/a/a/m;->e:Landroid/content/Context;

    invoke-interface {v1, v2, v7, v8}, Lcom/google/android/gms/drive/a/c;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    move v1, v4

    :goto_2
    if-eqz v1, :cond_7

    .line 263
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 247
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 257
    :cond_2
    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/gms/drive/a/a/m;->k:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/c;Ljava/util/List;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static {v1, v5}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/c;Ljava/util/List;)Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_3
    move v1, v4

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v4

    goto :goto_2

    :cond_5
    const/4 v2, 0x1

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/g/at;

    if-eqz v1, :cond_a

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/at;->d()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/at;->f()V

    :cond_6
    move v1, v2

    goto :goto_2

    .line 267
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/gms/drive/a/a/m;->k:Z

    if-nez v1, :cond_8

    move-object v0, v3

    .line 271
    goto :goto_0

    .line 276
    :cond_8
    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :cond_9
    move-object v0, v3

    .line 278
    goto :goto_0

    :cond_a
    move v1, v2

    goto :goto_2
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 146
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->o:Ljava/util/concurrent/ThreadPoolExecutor;

    if-nez v0, :cond_0

    move v0, v7

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 147
    invoke-direct {p0}, Lcom/google/android/gms/drive/a/a/m;->g()V

    .line 150
    new-instance v0, Lcom/google/android/gms/drive/a/a/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->g:Lcom/google/android/gms/drive/g/aw;

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/m;->d:Lcom/google/android/gms/drive/g/n;

    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/m;->h:Lcom/google/android/gms/drive/a/a/j;

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/m;->j:Lcom/google/android/gms/drive/b/d;

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/a/a/d;-><init>(Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/a/a/j;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/m;)V

    iget-boolean v1, p0, Lcom/google/android/gms/drive/a/a/m;->k:Z

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gms/drive/ai;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_1
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move v3, v2

    move-object v7, v0

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->o:Ljava/util/concurrent/ThreadPoolExecutor;

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->o:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->prestartAllCoreThreads()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    monitor-exit p0

    return-void

    .line 146
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v2, v7

    .line 150
    goto :goto_1

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(J)V
    .locals 5

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/database/r;->c(J)Lcom/google/android/gms/drive/database/model/be;

    move-result-object v0

    .line 184
    if-nez v0, :cond_1

    .line 186
    const-string v0, "PendingActionManager"

    const-string v1, "Ignoring addAction (action id: %d): not persisted in DB"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 190
    :cond_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/m;->f:Lcom/google/android/gms/drive/a/a/h;

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/be;)Lcom/google/android/gms/drive/a/a/k;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    .line 193
    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->l:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->o:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->prestartCoreThread()Z

    .line 198
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/gms/drive/a/a/k;)V
    .locals 1

    .prologue
    .line 329
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 330
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/a/a/m;->b(Lcom/google/android/gms/drive/a/a/k;)V

    .line 331
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/a/a/m;->c(Lcom/google/android/gms/drive/a/a/k;)V

    .line 332
    iget-object v0, p1, Lcom/google/android/gms/drive/a/a/k;->a:Lcom/google/android/gms/drive/database/model/be;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/be;->j()V

    .line 333
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/drive/api/ApiService;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 341
    monitor-exit p0

    return-void

    .line 329
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(Lcom/google/android/gms/drive/a/a/k;Z)V
    .locals 2

    .prologue
    .line 355
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->d()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 356
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/a/a/m;->b(Lcom/google/android/gms/drive/a/a/k;)V

    .line 358
    if-eqz p2, :cond_2

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/a/m;->i:Lcom/google/android/gms/drive/g/au;

    invoke-interface {v1}, Lcom/google/android/gms/drive/g/au;->a()Lcom/google/android/gms/drive/g/at;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 361
    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/a/m;->f()V

    .line 368
    :cond_0
    :goto_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    monitor-exit p0

    return-void

    .line 355
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 365
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 355
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/a;Ljava/util/List;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 513
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 514
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 515
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 519
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/a/a/m;->b(Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/a;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/c/cr;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 522
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/a/a/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    const/4 v1, 0x1

    const/4 v2, 0x3

    const/4 v3, 0x1

    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    iget-object v5, p0, Lcom/google/android/gms/drive/a/a/m;->j:Lcom/google/android/gms/drive/b/d;

    iget-object v6, p0, Lcom/google/android/gms/drive/a/a/m;->h:Lcom/google/android/gms/drive/a/a/j;

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/drive/a/a/m;->a(Lcom/google/android/gms/drive/a/a/k;ZIZLcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/a/a/j;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 530
    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/m;->c(Lcom/google/android/gms/drive/a/a/k;)V

    .line 531
    iget-object v0, v0, Lcom/google/android/gms/drive/a/a/k;->a:Lcom/google/android/gms/drive/database/model/be;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/be;->j()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 513
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 515
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 530
    :catchall_1
    move-exception v1

    :try_start_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/a/a/m;->c(Lcom/google/android/gms/drive/a/a/k;)V

    .line 531
    iget-object v0, v0, Lcom/google/android/gms/drive/a/a/k;->a:Lcom/google/android/gms/drive/database/model/be;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/be;->j()V

    throw v1

    .line 533
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 535
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized b()Lcom/google/android/gms/drive/a/a/k;
    .locals 1

    .prologue
    .line 232
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/a/a/m;->i()Lcom/google/android/gms/drive/a/a/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 233
    if-eqz v0, :cond_0

    .line 234
    monitor-exit p0

    return-object v0

    .line 236
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 407
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->c:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 408
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 409
    monitor-exit p0

    return-void

    .line 407
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 1

    .prologue
    .line 413
    monitor-enter p0

    :goto_0
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/a/a/m;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 414
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 416
    :cond_0
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized e()V
    .locals 2

    .prologue
    .line 472
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/at;

    .line 473
    invoke-interface {v0}, Lcom/google/android/gms/drive/g/at;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    const-string v0, "PendingActionManager"

    const-string v1, "Rate-limited action becomes eligible. Waking up threads!"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 475
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 479
    :cond_1
    monitor-exit p0

    return-void

    .line 472
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized f()V
    .locals 5

    .prologue
    .line 487
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->b:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/a/a/m;->n:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 505
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 490
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/drive/ai;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 491
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 492
    new-instance v2, Lcom/google/android/gms/drive/a/a/p;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/a/a/p;-><init>(Lcom/google/android/gms/drive/a/a/m;)V

    iput-object v2, p0, Lcom/google/android/gms/drive/a/a/m;->b:Ljava/lang/Runnable;

    .line 502
    iget-object v2, p0, Lcom/google/android/gms/drive/a/a/m;->a:Lcom/google/android/gms/drive/a/a/q;

    iget-object v3, p0, Lcom/google/android/gms/drive/a/a/m;->b:Ljava/lang/Runnable;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3, v0, v1, v4}, Lcom/google/android/gms/drive/a/a/q;->a(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 487
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
