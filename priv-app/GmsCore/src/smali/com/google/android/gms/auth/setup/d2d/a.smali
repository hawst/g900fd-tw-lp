.class final Lcom/google/android/gms/auth/setup/d2d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private final b:Ljava/util/Queue;

.field private c:Landroid/app/Activity;

.field private d:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "ActivityRunnableQueue"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->b:Ljava/util/Queue;

    .line 40
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/a;Lcom/google/android/gms/auth/setup/d2d/c;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/setup/d2d/a;->b(Lcom/google/android/gms/auth/setup/d2d/c;)V

    return-void
.end method

.method private declared-synchronized b(Lcom/google/android/gms/auth/setup/d2d/c;)V
    .locals 2

    .prologue
    .line 79
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->c:Landroid/app/Activity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->c:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 80
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Activity not destroyed; executing"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->c:Landroid/app/Activity;

    invoke-interface {p1, v0}, Lcom/google/android/gms/auth/setup/d2d/c;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :goto_0
    monitor-exit p0

    return-void

    .line 83
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Activity destroyed; queueing"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 84
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 76
    return-void
.end method

.method public final declared-synchronized a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 43
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Activity bound."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 44
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/a;->c:Landroid/app/Activity;

    .line 45
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->d:Landroid/os/Handler;

    .line 46
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 47
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Polling queue."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 48
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/setup/d2d/c;

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/a;->c:Landroid/app/Activity;

    invoke-interface {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/c;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 50
    :cond_2
    monitor-exit p0

    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/setup/d2d/c;)V
    .locals 2

    .prologue
    .line 58
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/a;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Posting on main thread."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 59
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->d:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/auth/setup/d2d/b;-><init>(Lcom/google/android/gms/auth/setup/d2d/a;Lcom/google/android/gms/auth/setup/d2d/c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/a;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
