.class public Lcom/google/android/gms/update/UpdateFromSdCardService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private final a:Landroid/os/IBinder;

.field private b:Lcom/google/android/gms/update/h;

.field private volatile c:I

.field private d:Ljava/io/File;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 74
    new-instance v0, Lcom/google/android/gms/update/ag;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/ag;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardService;)V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->a:Landroid/os/IBinder;

    .line 95
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/update/UpdateFromSdCardService;I)I
    .locals 0

    .prologue
    .line 58
    iput p1, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/update/UpdateFromSdCardService;)Lcom/google/android/gms/update/h;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->b:Lcom/google/android/gms/update/h;

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Ljava/util/List;
    .locals 7

    .prologue
    .line 191
    const-string v0, "UpdateFromSdCardService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "listFilesInDirectory: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 192
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 193
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    .line 194
    if-eqz p0, :cond_0

    .line 195
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 197
    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 198
    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 199
    invoke-virtual {v4}, Ljava/io/File;->isDirectory()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Ljava/io/File;->isHidden()Z

    move-result v5

    if-nez v5, :cond_2

    .line 200
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 198
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 201
    :cond_2
    invoke-virtual {v4}, Ljava/io/File;->isFile()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->isHidden()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    const-string v6, ".zip"

    invoke-virtual {v5, v6}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 202
    invoke-static {v4}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 203
    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 207
    :cond_3
    return-object v1
.end method

.method private static a(Ljava/io/File;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 178
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/update/ai;->a(Ljava/io/File;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 182
    :cond_0
    :goto_0
    return v0

    .line 179
    :catch_0
    move-exception v1

    .line 180
    const-string v2, "Exception while reading zip file."

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/update/UpdateFromSdCardService;)I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/update/UpdateFromSdCardService;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->d()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/update/UpdateFromSdCardService;)Ljava/io/File;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->d:Ljava/io/File;

    return-object v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 173
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.update.UpdateFromSdCard.STATUS_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->sendBroadcast(Landroid/content/Intent;)V

    .line 174
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 2

    .prologue
    .line 186
    const-string v0, "UpdateFromSdCardService"

    const-string v1, "getStatus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    iget v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    return v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 221
    const-string v0, "UpdateFromSdCardService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "installUpdate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->d:Ljava/io/File;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->d:Ljava/io/File;

    if-nez v0, :cond_0

    .line 223
    const-string v0, "UpdateFromSdCardService"

    const-string v1, "no last verified file! cannot install"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    :cond_0
    new-instance v0, Lcom/google/android/gms/update/af;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/af;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardService;)V

    invoke-virtual {v0}, Lcom/google/android/gms/update/af;->start()V

    .line 236
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 211
    const-string v0, "UpdateFromSdCardService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "verifyUpdate: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 212
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    .line 213
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->d()V

    .line 214
    new-instance v0, Lcom/google/android/gms/update/ai;

    invoke-direct {v0, p0}, Lcom/google/android/gms/update/ai;-><init>(Landroid/content/Context;)V

    .line 215
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    invoke-static {}, Lcom/google/android/gms/a/a;->k()Ljava/util/concurrent/Executor;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/a/a;->a(Landroid/os/AsyncTask;Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 218
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 239
    const-string v0, "UpdateFromSdCardService"

    const-string v1, "clearVerifiedState"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 240
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->d:Ljava/io/File;

    .line 241
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    .line 242
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->d()V

    .line 243
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->a:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 125
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 126
    const-string v0, "UpdateFromSdCardService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    .line 128
    new-instance v0, Lcom/google/android/gms/update/h;

    new-instance v1, Lcom/google/android/gms/update/ah;

    invoke-direct {v1, p0, p0}, Lcom/google/android/gms/update/ah;-><init>(Lcom/google/android/gms/update/UpdateFromSdCardService;Landroid/content/Context;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/update/h;-><init>(Landroid/content/Context;Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->b:Lcom/google/android/gms/update/h;

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->b:Lcom/google/android/gms/update/h;

    invoke-virtual {v0}, Lcom/google/android/gms/update/h;->a()V

    .line 130
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 134
    const-string v0, "UpdateFromSdCardService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->b:Lcom/google/android/gms/update/h;

    invoke-virtual {v0}, Lcom/google/android/gms/update/h;->b()V

    .line 136
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 137
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 141
    const-string v1, "UpdateFromSdCardService"

    const-string v2, "onStartCommand"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/a/a;->b()I

    move-result v1

    if-lez v1, :cond_0

    .line 143
    const/4 v0, 0x2

    .line 168
    :goto_0
    return v0

    .line 145
    :cond_0
    if-eqz p1, :cond_2

    const-string v1, "com.google.android.gms.update.UpdateFromSdCard.VERIFIER_RESULT_CHANGED"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 146
    const-string v1, "verifier_task_result"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->b:Lcom/google/android/gms/update/h;

    invoke-virtual {v1}, Lcom/google/android/gms/update/h;->c()I

    move-result v1

    .line 148
    packed-switch v1, :pswitch_data_0

    .line 159
    const-string v1, "UpdateFromSdCardService"

    const-string v2, "Unknown battery state, cannot handle!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->d()V

    goto :goto_0

    .line 150
    :pswitch_0
    const/4 v1, 0x6

    iput v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    goto :goto_1

    .line 153
    :pswitch_1
    const/4 v1, 0x4

    iput v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    goto :goto_1

    .line 156
    :pswitch_2
    const/4 v1, 0x5

    iput v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    goto :goto_1

    .line 162
    :cond_1
    const/4 v1, 0x3

    iput v1, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    goto :goto_1

    .line 165
    :cond_2
    iput v0, p0, Lcom/google/android/gms/update/UpdateFromSdCardService;->c:I

    goto :goto_1

    .line 148
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
