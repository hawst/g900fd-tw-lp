.class public final Lcom/google/android/gms/plus/apps/k;
.super Lcom/google/android/gms/plus/apps/ar;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# static fields
.field static final i:I


# instance fields
.field private k:Landroid/support/v7/app/a;

.field private l:[Lcom/google/android/gms/plus/apps/p;

.field private m:I

.field private n:Lcom/google/android/gms/plus/apps/u;

.field private o:Lcom/google/android/gms/plus/apps/u;

.field private p:Lcom/google/android/gms/common/api/v;

.field private q:I

.field private r:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    sget v0, Lcom/google/android/gms/h;->cm:I

    sput v0, Lcom/google/android/gms/plus/apps/k;->i:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ar;-><init>()V

    .line 543
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/k;I)I
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/k;)Lcom/google/android/gms/plus/apps/u;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/plus/apps/u;)V
    .locals 2

    .prologue
    .line 419
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 420
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    aget-object v1, v1, v0

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/p;->a:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/plus/apps/u;->a(Ljava/lang/String;I)V

    .line 419
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 422
    :cond_0
    return-void
.end method

.method public static b(I)Lcom/google/android/gms/plus/apps/k;
    .locals 2

    .prologue
    .line 192
    if-ltz p0, :cond_0

    const/4 v0, 0x2

    if-le p0, v0, :cond_1

    .line 194
    :cond_0
    const-string v0, "ConnectedAppsFragment"

    const-string v1, "Invalid filter type"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 196
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 197
    const-string v1, "preselected_filter"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 198
    new-instance v1, Lcom/google/android/gms/plus/apps/k;

    invoke-direct {v1}, Lcom/google/android/gms/plus/apps/k;-><init>()V

    .line 199
    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/k;->setArguments(Landroid/os/Bundle;)V

    .line 200
    return-object v1
.end method

.method static synthetic b(Lcom/google/android/gms/plus/apps/k;)Lcom/google/android/gms/plus/apps/u;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->o:Lcom/google/android/gms/plus/apps/u;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/apps/k;I)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/apps/k;->c(I)V

    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 426
    iput p1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    .line 427
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->h()Lcom/google/android/gms/plus/apps/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/p;->c:Lcom/google/android/gms/plus/apps/aq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ap;->a(Lcom/google/android/gms/plus/apps/aq;)V

    .line 428
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->k:Landroid/support/v7/app/a;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    aget-object v1, v1, p1

    iget v1, v1, Lcom/google/android/gms/plus/apps/p;->d:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(I)V

    .line 429
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->V_()V

    .line 430
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/plus/apps/k;)[Lcom/google/android/gms/plus/apps/p;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/apps/k;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/apps/k;)Landroid/support/v7/app/a;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->k:Landroid/support/v7/app/a;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/apps/k;)I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    return v0
.end method


# virtual methods
.method final a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ap;
    .locals 4

    .prologue
    .line 392
    new-instance v0, Lcom/google/android/gms/plus/apps/ap;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->j:Lcom/google/android/gms/plus/apps/c;

    new-instance v2, Lcom/google/android/gms/plus/apps/o;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/plus/apps/o;-><init>(Lcom/google/android/gms/plus/apps/k;B)V

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/plus/apps/ap;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/apps/c;Lcom/google/android/gms/plus/apps/aq;)V

    return-object v0
.end method

.method public final a(Landroid/support/v7/app/a;)V
    .locals 10

    .prologue
    const/16 v9, 0x15

    const/4 v3, 0x0

    .line 318
    iput-object p1, p0, Lcom/google/android/gms/plus/apps/k;->k:Landroid/support/v7/app/a;

    .line 319
    iget v0, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/k;->c(I)V

    .line 322
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/c;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    .line 323
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 324
    array-length v4, v1

    move v0, v3

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v1, v0

    .line 325
    new-instance v6, Lcom/google/android/gms/plus/apps/l;

    invoke-direct {v6, v5}, Lcom/google/android/gms/plus/apps/l;-><init>(Ljava/lang/String;)V

    .line 326
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 328
    iget v7, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    const/4 v8, -0x1

    if-ne v7, v8, :cond_0

    iget-object v7, p0, Lcom/google/android/gms/plus/apps/k;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v7, v7, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v7, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 330
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v7

    iput v7, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    .line 333
    :cond_0
    invoke-static {v9}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 334
    sget-object v7, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v8, p0, Lcom/google/android/gms/plus/apps/k;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v7, v8, v5}, Lcom/google/android/gms/people/o;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v5

    new-instance v7, Lcom/google/android/gms/plus/apps/n;

    invoke-direct {v7, p0, v6}, Lcom/google/android/gms/plus/apps/n;-><init>(Lcom/google/android/gms/plus/apps/k;Lcom/google/android/gms/plus/apps/l;)V

    invoke-interface {v5, v7}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 324
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 339
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    if-nez v0, :cond_3

    .line 340
    new-instance v0, Lcom/google/android/gms/plus/apps/u;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/apps/u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/apps/u;->a(Ljava/util/ArrayList;)V

    .line 344
    invoke-static {v9}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/k;->a(Lcom/google/android/gms/plus/apps/u;)V

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    sget v1, Lcom/google/android/gms/p;->sJ:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/k;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/u;->a(Ljava/lang/String;)V

    .line 351
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 352
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/u;->b(Ljava/lang/String;)V

    .line 361
    invoke-static {v9}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_4

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/u;->a(I)V

    .line 363
    iget v0, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    .line 364
    new-instance v1, Lcom/google/android/gms/plus/apps/s;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/plus/apps/s;-><init>(Lcom/google/android/gms/plus/apps/k;B)V

    move-object v2, v1

    move v1, v0

    .line 382
    :goto_1
    sget v0, Lcom/google/android/gms/l;->eH:I

    invoke-virtual {p1, v0}, Landroid/support/v7/app/a;->a(I)V

    .line 383
    invoke-virtual {p1}, Landroid/support/v7/app/a;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 384
    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 385
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/k;->n:Lcom/google/android/gms/plus/apps/u;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 386
    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 387
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 388
    return-void

    .line 366
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->o:Lcom/google/android/gms/plus/apps/u;

    if-nez v0, :cond_5

    .line 367
    new-instance v0, Lcom/google/android/gms/plus/apps/u;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/apps/u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/k;->o:Lcom/google/android/gms/plus/apps/u;

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->o:Lcom/google/android/gms/plus/apps/u;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/k;->a(Lcom/google/android/gms/plus/apps/u;)V

    .line 371
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gg:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 372
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->o:Lcom/google/android/gms/plus/apps/u;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 373
    new-instance v1, Lcom/google/android/gms/plus/apps/q;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/plus/apps/q;-><init>(Lcom/google/android/gms/plus/apps/k;B)V

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 374
    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 376
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    iget v2, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/google/android/gms/plus/apps/p;->b:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/k;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->o:Lcom/google/android/gms/plus/apps/u;

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/u;->a(I)V

    .line 378
    iget v0, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    .line 379
    new-instance v1, Lcom/google/android/gms/plus/apps/m;

    invoke-direct {v1, p0, v3}, Lcom/google/android/gms/plus/apps/m;-><init>(Lcom/google/android/gms/plus/apps/k;B)V

    move-object v2, v1

    move v1, v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    .line 286
    const-string v0, "ConnectedAppsFragment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "ConnectedAppsFragment"

    const-string v1, "GoogleApiClient connection failed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_0
    iget v0, p0, Lcom/google/android/gms/plus/apps/k;->q:I

    if-ne v0, v7, :cond_1

    .line 290
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :try_start_0
    iput v0, p0, Lcom/google/android/gms/plus/apps/k;->q:I

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/support/v4/app/q;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 292
    :cond_1
    :goto_0
    return-void

    .line 290
    :catch_0
    move-exception v0

    iput v7, p0, Lcom/google/android/gms/plus/apps/k;->q:I

    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 271
    const-string v0, "ConnectedAppsFragment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 272
    const-string v0, "ConnectedAppsFragment"

    const-string v1, "GoogleApiClient connected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    return-void
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/google/android/gms/plus/apps/p;->b:I

    return v0
.end method

.method protected final d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/p;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method final e()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 397
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/p;->e:Ljava/lang/CharSequence;

    return-object v0
.end method

.method final f()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 402
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/apps/af;->a(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 405
    invoke-static {}, Lcom/google/android/gms/plus/apps/af;->a()Lcom/google/android/gms/plus/apps/af;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/apps/af;->b(Landroid/content/Context;I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 409
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/android/gms/plus/apps/p;->f:Ljava/lang/CharSequence;

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 278
    const-string v0, "ConnectedAppsFragment"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 279
    const-string v0, "ConnectedAppsFragment"

    const-string v1, "GoogleApiClient connection suspended"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 281
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 282
    return-void
.end method

.method final g()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 415
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    iget v2, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/p;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, -0x1

    const/4 v11, 0x2

    const/4 v10, 0x0

    .line 205
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/ar;->onActivityCreated(Landroid/os/Bundle;)V

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    if-nez v0, :cond_0

    .line 208
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/plus/apps/p;

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    iget-object v9, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    new-instance v0, Lcom/google/android/gms/plus/apps/p;

    sget v1, Lcom/google/android/gms/p;->sL:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/k;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->sM:I

    new-instance v3, Lcom/google/android/gms/plus/apps/o;

    invoke-direct {v3, p0, v10}, Lcom/google/android/gms/plus/apps/o;-><init>(Lcom/google/android/gms/plus/apps/k;B)V

    sget v4, Lcom/google/android/gms/h;->K:I

    sget v5, Lcom/google/android/gms/p;->tM:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/apps/k;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    sget v6, Lcom/google/android/gms/p;->tN:I

    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/apps/k;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/common/analytics/d;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/c/a;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v8}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/apps/p;-><init>(Ljava/lang/String;ILcom/google/android/gms/plus/apps/aq;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    aput-object v0, v9, v10

    iget-object v9, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    new-instance v0, Lcom/google/android/gms/plus/apps/p;

    sget v1, Lcom/google/android/gms/p;->sO:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/k;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->sM:I

    new-instance v3, Lcom/google/android/gms/plus/apps/t;

    invoke-direct {v3, p0, v10}, Lcom/google/android/gms/plus/apps/t;-><init>(Lcom/google/android/gms/plus/apps/k;B)V

    sget v4, Lcom/google/android/gms/h;->cs:I

    sget v5, Lcom/google/android/gms/p;->tK:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/apps/k;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v5, Lcom/google/android/gms/plus/c/a;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    sget v6, Lcom/google/android/gms/p;->tO:I

    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/apps/k;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    sget-object v6, Lcom/google/android/gms/plus/c/a;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v7, v6}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/common/analytics/d;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/c/a;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v8}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/apps/p;-><init>(Ljava/lang/String;ILcom/google/android/gms/plus/apps/aq;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    aput-object v0, v9, v13

    iget-object v9, p0, Lcom/google/android/gms/plus/apps/k;->l:[Lcom/google/android/gms/plus/apps/p;

    new-instance v0, Lcom/google/android/gms/plus/apps/p;

    sget v1, Lcom/google/android/gms/p;->sN:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/k;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->sM:I

    new-instance v3, Lcom/google/android/gms/plus/apps/r;

    invoke-direct {v3, p0, v10}, Lcom/google/android/gms/plus/apps/r;-><init>(Lcom/google/android/gms/plus/apps/k;B)V

    sget v4, Lcom/google/android/gms/h;->K:I

    sget v5, Lcom/google/android/gms/p;->tQ:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/plus/apps/k;->getText(I)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v5, Lcom/google/android/gms/plus/c/a;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-static {v6, v5}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v5

    sget v6, Lcom/google/android/gms/p;->tP:I

    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/apps/k;->getText(I)Ljava/lang/CharSequence;

    move-result-object v7

    sget-object v6, Lcom/google/android/gms/plus/c/a;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    invoke-static {v7, v6}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/common/analytics/d;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v8, Lcom/google/android/gms/plus/c/a;->C:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v8}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/plus/apps/p;-><init>(Ljava/lang/String;ILcom/google/android/gms/plus/apps/aq;ILjava/lang/CharSequence;Ljava/lang/CharSequence;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    aput-object v0, v9, v11

    .line 210
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 211
    const-string v1, "preselected_filter"

    invoke-virtual {v0, v1, v12}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 212
    if-eqz p1, :cond_2

    .line 213
    const-string v0, "connected_apps_filter"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    .line 214
    const-string v0, "connected_apps_account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    .line 215
    const-string v0, "signed_in"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/apps/k;->q:I

    .line 235
    :goto_0
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 236
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v2, Lcom/google/android/gms/people/ad;

    invoke-direct {v2}, Lcom/google/android/gms/people/ad;-><init>()V

    const/16 v3, 0x50

    iput v3, v2, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v2}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/plus/f;->d:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/k;->p:Lcom/google/android/gms/common/api/v;

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 247
    iput v11, p0, Lcom/google/android/gms/plus/apps/k;->q:I

    .line 249
    :cond_1
    return-void

    .line 217
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 225
    iput v10, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    .line 230
    :goto_1
    iput v12, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    goto :goto_0

    .line 219
    :pswitch_0
    iput v13, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    goto :goto_1

    .line 222
    :pswitch_1
    iput v11, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    goto :goto_1

    .line 217
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 310
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/ar;->onDetach()V

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->p:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/k;->p:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 314
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 253
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/ar;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 254
    const-string v0, "connected_apps_filter"

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 255
    const-string v0, "connected_apps_account"

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->r:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 256
    const-string v0, "signed_in"

    iget v1, p0, Lcom/google/android/gms/plus/apps/k;->q:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 257
    return-void
.end method
