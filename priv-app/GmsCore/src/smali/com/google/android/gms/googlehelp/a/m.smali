.class public final Lcom/google/android/gms/googlehelp/a/m;
.super Lcom/google/android/gms/googlehelp/a/j;
.source "SourceFile"


# instance fields
.field private final f:Lcom/google/android/gms/googlehelp/common/k;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/k;Lcom/android/volley/toolbox/aa;)V
    .locals 7

    .prologue
    .line 57
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p5

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/a/j;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 58
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/a/m;->f:Lcom/google/android/gms/googlehelp/common/k;

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/m;->h:Ljava/util/HashMap;

    const-string v1, "If-None-Match"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/a/m;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 6

    .prologue
    .line 96
    const-string v0, "Must be called from a worker thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 97
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v5

    .line 98
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v2

    .line 99
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/k;->o()Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    const-string v1, "&key=AIzaSyC4gyROYSkqjyykTdfouAxjwLBLYAk-XJE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x2c

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->E()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->F()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 100
    :goto_0
    new-instance v0, Lcom/google/android/gms/googlehelp/a/m;

    move-object v1, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/a/m;-><init>(Landroid/content/Context;Landroid/accounts/Account;Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/k;Lcom/android/volley/toolbox/aa;)V

    .line 102
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 105
    :try_start_0
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v5, v0, v1, v2}, Lcom/android/volley/toolbox/aa;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    .line 108
    :goto_1
    return-object v0

    :cond_1
    move-object v3, v0

    .line 99
    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    :goto_2
    const-string v1, "GOOGLEHELP_LeafAnswerRequest"

    const-string v2, "Fetching leaf answer failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 108
    const/4 v0, 0x0

    goto :goto_1

    .line 106
    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_2
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 64
    iget v0, p1, Lcom/android/volley/m;->a:I

    const/16 v1, 0x130

    if-ne v0, v1, :cond_0

    .line 65
    const-string v0, "GOOGLEHELP_LeafAnswerRequest"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "NOT_MODIFIED_RESPONSE is returned for: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/a/m;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    sget-object v0, Lcom/google/android/gms/googlehelp/common/k;->a:Lcom/google/android/gms/googlehelp/common/k;

    invoke-static {v0, v3}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    .line 81
    :goto_0
    return-object v0

    .line 70
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    iget-object v2, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    invoke-static {v2}, Lcom/android/volley/toolbox/i;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 72
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/m;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-static {v1, v0}, Lcom/google/android/gms/googlehelp/common/k;->a(Lorg/json/JSONObject;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    .line 74
    const-string v1, "GOOGLEHELP_LeafAnswerRequest"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Leaf answer is returned for: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/a/m;->f:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "GOOGLEHELP_LeafAnswerRequest"

    const-string v2, "Parsing leaf answer response data failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    new-instance v1, Lcom/android/volley/o;

    invoke-direct {v1, v0}, Lcom/android/volley/o;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0

    .line 79
    :catch_1
    move-exception v0

    .line 80
    const-string v1, "GOOGLEHELP_LeafAnswerRequest"

    const-string v2, "Parsing leaf answer response data failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 81
    new-instance v1, Lcom/android/volley/ac;

    invoke-direct {v1, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method
