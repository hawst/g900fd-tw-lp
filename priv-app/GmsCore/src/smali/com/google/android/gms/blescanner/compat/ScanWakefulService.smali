.class public Lcom/google/android/gms/blescanner/compat/ScanWakefulService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const-string v0, "ScanWakefulService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 31
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0, v0, v1}, Lcom/google/android/gms/blescanner/compat/j;->a(Landroid/content/Context;ZZ)Lcom/google/android/gms/blescanner/compat/a;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/blescanner/compat/k;

    if-eqz v1, :cond_0

    .line 37
    check-cast v0, Lcom/google/android/gms/blescanner/compat/k;

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/k;->b()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 42
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/blescanner/compat/ScanWakefulBroadcastReceiver;->a(Landroid/content/Intent;)Z

    .line 43
    :goto_0
    return-void

    .line 39
    :catch_0
    move-exception v0

    .line 40
    :try_start_1
    const-string v1, "Unexpected exception thrown when doing a blocking scan cycle"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Exception;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 42
    invoke-static {p1}, Lcom/google/android/gms/blescanner/compat/ScanWakefulBroadcastReceiver;->a(Landroid/content/Intent;)Z

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {p1}, Lcom/google/android/gms/blescanner/compat/ScanWakefulBroadcastReceiver;->a(Landroid/content/Intent;)Z

    throw v0
.end method
