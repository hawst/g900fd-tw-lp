.class public final Lcom/google/android/gms/photos/autobackup/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field private b:Landroid/os/Bundle;

.field private c:Lcom/google/android/gms/photos/autobackup/service/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;Landroid/os/Bundle;Lcom/google/android/gms/photos/autobackup/service/a/a;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/y;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/y;->b:Landroid/os/Bundle;

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/y;->c:Lcom/google/android/gms/photos/autobackup/service/a/a;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/y;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILjava/util/List;)V

    .line 49
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 5

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/y;->c:Lcom/google/android/gms/photos/autobackup/service/a/a;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/y;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/photos/autobackup/service/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/y;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v2, 0x0

    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/android/gms/photos/autobackup/model/AutoBackupSettings;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/photos/autobackup/a/ac;->a(ILjava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "SetSettingsOp"

    const-string v1, "Failed trying to deliver success"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
