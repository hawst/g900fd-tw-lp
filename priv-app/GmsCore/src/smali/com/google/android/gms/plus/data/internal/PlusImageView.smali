.class public Lcom/google/android/gms/plus/data/internal/PlusImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/plus/internal/an;


# instance fields
.field private a:I

.field private b:Landroid/net/Uri;

.field private c:Z

.field private d:Z

.field private e:Landroid/graphics/Bitmap;

.field private f:Lcom/google/android/gms/plus/internal/ab;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 129
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 121
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 122
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    return-void
.end method

.method static synthetic a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    .line 30
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-double v2, v0

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-double v4, v0

    cmpl-double v0, v2, v4

    if-lez v0, :cond_0

    int-to-double v0, p1

    div-double/2addr v0, v2

    :goto_0
    mul-double/2addr v2, v0

    add-double/2addr v2, v6

    double-to-int v2, v2

    mul-double/2addr v0, v4

    add-double/2addr v0, v6

    double-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {p0, v2, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    int-to-double v0, p1

    div-double/2addr v0, v4

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/data/internal/PlusImageView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->e:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    if-eqz v0, :cond_1

    const-string v0, "android.resource"

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 215
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->d:Z

    if-nez v2, :cond_2

    .line 234
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 213
    goto :goto_0

    .line 218
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    if-nez v2, :cond_3

    .line 220
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 222
    :cond_3
    if-nez v0, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v2}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228
    :cond_4
    if-eqz v0, :cond_5

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setImageURI(Landroid/net/Uri;)V

    .line 233
    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->d:Z

    goto :goto_1

    .line 231
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    iget-object v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    iget v3, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a:I

    invoke-interface {v0, p0, v2, v3}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/an;Landroid/net/Uri;I)V

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/plus/data/internal/PlusImageView;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->c:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/data/internal/PlusImageView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->e:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public final T_()V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a()V

    .line 179
    return-void
.end method

.method public final W_()V
    .locals 0

    .prologue
    .line 183
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 151
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;I)V

    .line 152
    return-void
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    if-nez v0, :cond_2

    if-nez p1, :cond_1

    move v0, v1

    .line 163
    :goto_0
    iget v3, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a:I

    if-ne v3, p2, :cond_0

    move v2, v1

    .line 165
    :cond_0
    if-eqz v0, :cond_3

    if-eqz v2, :cond_3

    .line 174
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 162
    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    .line 169
    :cond_3
    iput-object p1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->b:Landroid/net/Uri;

    .line 170
    iput p2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a:I

    .line 172
    iput-boolean v1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->d:Z

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a()V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/c;Landroid/os/ParcelFileDescriptor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 52
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->d:Z

    .line 59
    if-eqz p2, :cond_0

    .line 60
    new-instance v0, Lcom/google/android/gms/plus/data/internal/a;

    iget v1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a:I

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/plus/data/internal/a;-><init>(Lcom/google/android/gms/plus/data/internal/PlusImageView;I)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/os/ParcelFileDescriptor;

    aput-object p2, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ab;)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    if-eq p1, v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/common/es;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->c(Lcom/google/android/gms/common/es;)V

    .line 140
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/common/es;)V

    .line 143
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 188
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->c:Z

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/common/es;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/common/es;)V

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 198
    :cond_1
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 202
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->c:Z

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/common/es;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/plus/data/internal/PlusImageView;->f:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0, p0}, Lcom/google/android/gms/plus/internal/ab;->c(Lcom/google/android/gms/common/es;)V

    .line 210
    :cond_0
    return-void
.end method
