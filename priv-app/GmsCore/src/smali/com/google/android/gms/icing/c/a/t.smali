.class public final Lcom/google/android/gms/icing/c/a/t;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/android/gms/udc/e/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4879
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4880
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/udc/e/p;->a()[Lcom/google/android/gms/udc/e/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/t;->cachedSize:I

    .line 4881
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 4944
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4945
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4946
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4949
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 4950
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4951
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    aget-object v2, v2, v0

    .line 4952
    if-eqz v2, :cond_1

    .line 4953
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4950
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 4958
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4893
    if-ne p1, p0, :cond_1

    .line 4894
    const/4 v0, 0x1

    .line 4911
    :cond_0
    :goto_0
    return v0

    .line 4896
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/t;

    if-eqz v1, :cond_0

    .line 4899
    check-cast p1, Lcom/google/android/gms/icing/c/a/t;

    .line 4900
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 4901
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4907
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4911
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/t;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4904
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4916
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4919
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4921
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/t;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4922
    return v0

    .line 4916
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4856
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/t;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/udc/e/p;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/udc/e/p;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/udc/e/p;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 4928
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4929
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4931
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 4932
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 4933
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/t;->b:[Lcom/google/android/gms/udc/e/p;

    aget-object v1, v1, v0

    .line 4934
    if-eqz v1, :cond_1

    .line 4935
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4932
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4939
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4940
    return-void
.end method
