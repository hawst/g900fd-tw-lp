.class public Lcom/google/android/gms/wallet/common/a/a;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field protected final f:Landroid/content/Context;

.field private final g:Lcom/android/volley/x;

.field private final h:Ljava/lang/String;

.field private final i:Lcom/google/android/apps/common/a/a/i;

.field private final j:Lcom/google/android/apps/common/a/a/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "https://i18napis.appspot.com/address/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1, p4}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/a;->f:Landroid/content/Context;

    .line 52
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/a/a;->g:Lcom/android/volley/x;

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/a/a;->h:Ljava/lang/String;

    .line 58
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "get_address_metadata"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/a;->i:Lcom/google/android/apps/common/a/a/i;

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/a;->i:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/a;->j:Lcom/google/android/apps/common/a/a/h;

    .line 60
    return-void
.end method

.method private a(Lorg/json/JSONObject;)Lorg/json/JSONObject;
    .locals 4

    .prologue
    const/4 v2, -0x1

    .line 113
    if-nez p1, :cond_0

    .line 114
    new-instance p1, Lorg/json/JSONObject;

    invoke-direct {p1}, Lorg/json/JSONObject;-><init>()V

    .line 117
    :cond_0
    const-string v0, "id"

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/a;->h:Ljava/lang/String;

    .line 121
    const-string v1, "id"

    invoke-virtual {p1, v1, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :cond_1
    :goto_0
    const-string v1, "key"

    invoke-static {p1, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 129
    if-eqz v0, :cond_3

    const/16 v1, 0x2f

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 130
    :goto_1
    if-eq v1, v2, :cond_2

    .line 132
    :try_start_1
    const-string v2, "key"

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 139
    :cond_2
    :goto_2
    return-object p1

    .line 123
    :catch_0
    move-exception v1

    const-string v1, "AddressMetadataRetrievalRequest"

    const-string v3, "Could not specify key... results unknown"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    move v1, v2

    .line 129
    goto :goto_1

    .line 134
    :catch_1
    move-exception v0

    const-string v0, "AddressMetadataRetrievalRequest"

    const-string v1, "Could not specify key... results unknown"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method


# virtual methods
.method protected a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 8

    .prologue
    .line 70
    :try_start_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    iget-object v2, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    invoke-static {v2}, Lcom/android/volley/toolbox/i;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    .line 75
    :try_start_1
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    .line 81
    :try_start_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/a/a;->a(Lorg/json/JSONObject;)Lorg/json/JSONObject;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/a;->i:Lcom/google/android/apps/common/a/a/i;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/a/a;->j:Lcom/google/android/apps/common/a/a/h;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "rpc"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/a;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/a/a;->i:Lcom/google/android/apps/common/a/a/i;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 87
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    new-instance v4, Lcom/android/volley/c;

    invoke-direct {v4}, Lcom/android/volley/c;-><init>()V

    iget-object v0, p1, Lcom/android/volley/m;->b:[B

    iput-object v0, v4, Lcom/android/volley/c;->a:[B

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/android/volley/c;->b:Ljava/lang/String;

    iput-wide v2, v4, Lcom/android/volley/c;->c:J

    sget-object v0, Lcom/google/android/gms/wallet/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    add-long/2addr v2, v6

    iput-wide v2, v4, Lcom/android/volley/c;->d:J

    iget-object v0, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    iput-object v0, v4, Lcom/android/volley/c;->f:Ljava/util/Map;

    iget-wide v2, v4, Lcom/android/volley/c;->d:J

    iput-wide v2, v4, Lcom/android/volley/c;->e:J

    invoke-static {v1, v4}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const-string v1, "AddressMetadataRetrievalRequest"

    const-string v2, "Could not parse server response"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    new-instance v1, Lcom/android/volley/o;

    invoke-direct {v1, v0}, Lcom/android/volley/o;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;
    :try_end_2
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v0

    goto :goto_0

    .line 88
    :catch_1
    move-exception v0

    .line 89
    new-instance v1, Lcom/android/volley/o;

    invoke-direct {v1, v0}, Lcom/android/volley/o;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 34
    check-cast p1, Lorg/json/JSONObject;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/a;->g:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final o()Lcom/android/volley/r;
    .locals 1

    .prologue
    .line 95
    sget-object v0, Lcom/android/volley/r;->c:Lcom/android/volley/r;

    return-object v0
.end method
