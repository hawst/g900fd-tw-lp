.class public Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;
.super Lcom/google/android/gms/wallet/common/ui/bq;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/wallet/ow/t;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# instance fields
.field private a:Lcom/google/android/gms/wallet/ow/v;

.field private g:Lcom/google/aa/b/a/h;

.field private h:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/bq;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    new-instance v0, Lcom/google/android/gms/wallet/ow/v;

    invoke-direct {v0, p1, p0, p2}, Lcom/google/android/gms/wallet/ow/v;-><init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    .line 44
    invoke-super {p0, p0}, Lcom/google/android/gms/wallet/common/ui/bq;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 45
    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/w;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/w;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/h;

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    if-eq v1, v0, :cond_3

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    if-eqz v1, :cond_0

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->qm:I

    invoke-static {v1, v2, v6, p2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 148
    :cond_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v2

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    const-wide/16 v4, -0x1

    move-object v1, p0

    move v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/ow/v;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 155
    sget v0, Lcom/google/android/gms/j;->qm:I

    invoke-static {v2, v0, v7, p2}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 157
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->BV:I

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/ow/w;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/w;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 164
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->l()V

    .line 169
    :cond_2
    :goto_0
    return-void

    .line 165
    :cond_3
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->c:Lcom/google/android/gms/wallet/common/ui/bl;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/bl;->b()V

    goto :goto_0
.end method

.method public final a(Landroid/widget/ArrayAdapter;)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a(Landroid/widget/ArrayAdapter;Z)V

    .line 50
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/h;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/v;->a(Lcom/google/aa/b/a/h;)V

    .line 80
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/ow/u;)V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ow/v;->b:Lcom/google/android/gms/wallet/ow/u;

    .line 60
    return-void
.end method

.method public final a([Lcom/google/aa/b/a/h;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/v;->a([Lcom/google/aa/b/a/h;)V

    .line 69
    const/4 v0, 0x0

    aget-object v0, p1, v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a(Lcom/google/aa/b/a/h;)V

    .line 70
    return-void
.end method

.method protected final c()V
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->qm:I

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;IZZ)Z

    .line 188
    :cond_0
    return-void
.end method

.method protected final d()I
    .locals 4

    .prologue
    .line 196
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getSelectedView()Landroid/view/View;

    move-result-object v1

    .line 197
    const/4 v0, 0x0

    .line 198
    if-eqz v1, :cond_0

    .line 200
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v0

    .line 201
    sget v2, Lcom/google/android/gms/j;->lx:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->e:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->uj:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 205
    sget v2, Lcom/google/android/gms/j;->lx:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->e:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->uj:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 209
    :cond_0
    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 214
    sget v0, Lcom/google/android/gms/n;->C:I

    return v0
.end method

.method protected final g()I
    .locals 1

    .prologue
    .line 219
    sget v0, Lcom/google/android/gms/p;->Ar:I

    return v0
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/ow/w;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/w;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_0

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/ow/w;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->g:Lcom/google/aa/b/a/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/w;->a(Lcom/google/aa/b/a/h;)I

    move-result v0

    .line 113
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->f:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 177
    const/4 v0, 0x1

    invoke-virtual {p0, p3, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a(IZ)V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 181
    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/wallet/ow/v;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 89
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a:Lcom/google/android/gms/wallet/ow/v;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/v;->onNothingSelected(Landroid/widget/AdapterView;)V

    .line 94
    return-void
.end method

.method public synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 1

    .prologue
    .line 25
    check-cast p1, Landroid/widget/ArrayAdapter;

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a(Landroid/widget/ArrayAdapter;Z)V

    return-void
.end method

.method public setEnabled(Z)V
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->isEnabled()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 99
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/bq;->setEnabled(Z)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bq;->b:Landroid/widget/ArrayAdapter;

    check-cast v0, Lcom/google/android/gms/wallet/ow/w;

    .line 101
    if-eqz v0, :cond_0

    .line 102
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/w;->notifyDataSetChanged()V

    .line 105
    :cond_0
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->h:Landroid/widget/AdapterView$OnItemClickListener;

    .line 55
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a(IZ)V

    .line 129
    return-void
.end method
