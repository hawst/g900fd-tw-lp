.class public Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

.field private b:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/googlehelp/fragments/ContactFragment;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->b:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    return-object v0
.end method

.method public final a(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->ak_()Landroid/support/v7/app/a;

    move-result-object v1

    if-eqz p1, :cond_1

    sget v0, Lcom/google/android/gms/p;->oV:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/support/v7/app/a;->c(I)V

    .line 51
    if-eqz p1, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->b:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;->b()V

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 56
    if-eqz p1, :cond_2

    .line 57
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 61
    :goto_1
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 62
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 66
    :goto_2
    return-void

    .line 47
    :cond_1
    sget v0, Lcom/google/android/gms/p;->oW:I

    goto :goto_0

    .line 59
    :cond_2
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    goto :goto_1

    .line 64
    :cond_3
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_2
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->a:Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;

    .line 34
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 35
    sget v1, Lcom/google/android/gms/j;->hN:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/ScrollableContactFragment;->b:Lcom/google/android/gms/googlehelp/fragments/ContactFragment;

    .line 37
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 26
    sget v0, Lcom/google/android/gms/l;->cm:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
