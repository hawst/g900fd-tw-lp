.class public final Lcom/google/android/gms/people/sync/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Ljava/util/ArrayList;

.field c:Ljava/util/ArrayList;

.field d:Ljava/util/ArrayList;

.field e:Ljava/util/ArrayList;

.field f:Lcom/google/android/gms/people/sync/a/n;

.field g:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->b:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->f:Lcom/google/android/gms/people/sync/a/n;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/a/k;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 211
    invoke-static {}, Lcom/google/android/gms/people/internal/at;->a()Ljava/lang/StringBuilder;

    move-result-object v1

    .line 212
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "PeopleSyncDiffs: sync "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/a/k;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "enabled"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/people/sync/a/k;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 215
    const-string v0, "unchanged"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 236
    :cond_0
    :goto_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 212
    :cond_1
    const-string v0, "disabled"

    goto :goto_0

    .line 217
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->f:Lcom/google/android/gms/people/sync/a/n;

    if-eqz v0, :cond_3

    .line 218
    const-string v0, "\'me\' profile changed; "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 220
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " people added; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 223
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->e:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " people deleted; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 226
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    if-eqz v0, :cond_6

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " people updated; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_7

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/people/sync/a/k;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " extraneous groups; "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 232
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/people/sync/a/k;->g:Z

    if-eqz v0, :cond_0

    .line 233
    const-string v0, " Avatar sync required; "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1
.end method
