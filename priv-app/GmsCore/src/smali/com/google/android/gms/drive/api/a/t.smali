.class public final Lcom/google/android/gms/drive/api/a/t;
.super Lcom/google/android/gms/drive/api/a/d;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/api/a/d;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 30
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    .line 31
    new-instance v1, Lcom/google/android/gms/drive/DrivePreferences;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->m()Lcom/google/android/gms/drive/g/ae;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ae;->a()Z

    move-result v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/DrivePreferences;-><init>(Z)V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/t;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnDrivePreferencesResponse;

    invoke-direct {v2, v1}, Lcom/google/android/gms/drive/internal/OnDrivePreferencesResponse;-><init>(Lcom/google/android/gms/drive/DrivePreferences;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnDrivePreferencesResponse;)V

    .line 34
    return-void
.end method
