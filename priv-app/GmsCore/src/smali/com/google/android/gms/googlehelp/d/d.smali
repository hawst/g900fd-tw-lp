.class public final Lcom/google/android/gms/googlehelp/d/d;
.super Lcom/google/android/gms/googlehelp/d/a;
.source "SourceFile"


# static fields
.field private static final b:[Ljava/lang/String;


# instance fields
.field private final c:Lcom/google/android/gms/googlehelp/d/e;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 33
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "app_package_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "product_specific_context"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "user_action"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "session_id"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "click_rank"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "timestamp"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "query"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "extra_info_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "extra_info"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/googlehelp/d/d;->b:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/d/a;-><init>()V

    .line 52
    new-instance v0, Lcom/google/android/gms/googlehelp/d/e;

    invoke-direct {v0, p1}, Lcom/google/android/gms/googlehelp/d/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/d/d;->c:Lcom/google/android/gms/googlehelp/d/e;

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 146
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->b()V

    .line 148
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "app_package_name=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/d;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "metrics"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    throw v0
.end method

.method protected final a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/d;->c:Lcom/google/android/gms/googlehelp/d/e;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/d/e;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/o;)V
    .locals 4

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->b()V

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/d;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    .line 79
    :goto_0
    return-void

    .line 65
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 66
    const-string v1, "app_package_name"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v1, "product_specific_context"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    const-string v1, "user_action"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 69
    const-string v1, "session_id"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 70
    const-string v1, "click_rank"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->e()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 71
    const-string v1, "timestamp"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 72
    const-string v1, "query"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    const-string v1, "extra_info_type"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->h()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 74
    const-string v1, "extra_info"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/o;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/d;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "metrics"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    throw v0
.end method

.method public final d()Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 83
    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->b()V

    .line 86
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/d;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "metrics"

    sget-object v2, Lcom/google/android/gms/googlehelp/d/d;->b:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 87
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    .line 89
    if-eqz v1, :cond_0

    .line 90
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 92
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    return v0

    .line 89
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_0
    if-eqz v1, :cond_1

    .line 90
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 92
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    throw v0

    .line 89
    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final e()Ljava/util/List;
    .locals 27

    .prologue
    .line 97
    const/4 v10, 0x0

    .line 98
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/d/d;->b()V

    .line 100
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/d/d;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "metrics"

    sget-object v4, Lcom/google/android/gms/googlehelp/d/d;->b:[Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v16

    .line 102
    :try_start_1
    const-string v2, "_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 103
    const-string v3, "app_package_name"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 104
    const-string v3, "product_specific_context"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 105
    const-string v3, "user_action"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v19

    .line 106
    const-string v3, "session_id"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v20

    .line 107
    const-string v3, "click_rank"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v21

    .line 108
    const-string v3, "timestamp"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v22

    .line 109
    const-string v3, "query"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v23

    .line 110
    const-string v3, "extra_info_type"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v24

    .line 111
    const-string v3, "extra_info"

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v25

    .line 113
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 114
    :goto_0
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 117
    new-instance v3, Lcom/google/android/gms/googlehelp/common/o;

    move-object/from16 v0, v16

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-interface/range {v16 .. v17}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v16

    move/from16 v1, v20

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v16

    move/from16 v1, v21

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    move-object/from16 v0, v16

    move/from16 v1, v22

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    move-object/from16 v0, v16

    move/from16 v1, v23

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, v16

    move/from16 v1, v24

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move-object/from16 v0, v16

    move/from16 v1, v25

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    invoke-direct/range {v3 .. v15}, Lcom/google/android/gms/googlehelp/common/o;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Z)V

    .line 128
    move-object/from16 v0, v26

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v2

    move-object/from16 v3, v16

    :goto_1
    if-eqz v3, :cond_0

    .line 133
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    throw v2

    .line 132
    :cond_1
    if-eqz v16, :cond_2

    .line 133
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    .line 135
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    return-object v26

    .line 132
    :catchall_1
    move-exception v2

    move-object v3, v10

    goto :goto_1
.end method

.method public final f()I
    .locals 4

    .prologue
    .line 161
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->b()V

    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/d;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "metrics"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/d;->c()V

    throw v0
.end method
