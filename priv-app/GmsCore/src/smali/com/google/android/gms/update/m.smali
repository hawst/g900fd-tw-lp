.class final Lcom/google/android/gms/update/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/SystemUpdateActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V
    .locals 0

    .prologue
    .line 439
    iput-object p1, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->e(Lcom/google/android/gms/update/SystemUpdateActivity;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 442
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->f(Lcom/google/android/gms/update/SystemUpdateActivity;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 460
    :goto_0
    return-void

    .line 445
    :cond_0
    monitor-exit v1

    .line 446
    sget-boolean v0, Lcom/google/android/gms/update/SystemUpdateService;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->g(Lcom/google/android/gms/update/SystemUpdateActivity;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 448
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->h(Lcom/google/android/gms/update/SystemUpdateActivity;)I

    move-result v0

    .line 451
    iget-object v1, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v1}, Lcom/google/android/gms/update/SystemUpdateActivity;->i(Lcom/google/android/gms/update/SystemUpdateActivity;)Landroid/widget/ProgressBar;

    move-result-object v1

    if-ltz v0, :cond_1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 456
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->k(Lcom/google/android/gms/update/SystemUpdateActivity;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 445
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 451
    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 453
    :catch_0
    move-exception v0

    const-string v0, "SystemUpdateActivity"

    const-string v1, "Not connected to service; not updating download percent"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->j(Lcom/google/android/gms/update/SystemUpdateActivity;)V

    goto :goto_2

    .line 458
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->e(Lcom/google/android/gms/update/SystemUpdateActivity;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 459
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/update/m;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->l(Lcom/google/android/gms/update/SystemUpdateActivity;)Z

    .line 460
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method
