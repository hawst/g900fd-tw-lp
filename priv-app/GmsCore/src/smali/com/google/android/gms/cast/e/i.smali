.class public final Lcom/google/android/gms/cast/e/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;

.field private static final b:Lcom/google/android/gms/cast/internal/k;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "Utils"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/e/i;->b:Lcom/google/android/gms/cast/internal/k;

    .line 26
    const-string v0, "CastSDK/%d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x6768a8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/e/i;->a:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/util/List;)Lcom/google/i/a/a/a/a;
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 53
    sget-object v0, Lcom/google/android/gms/cast/e/i;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Number of results: %d"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    new-instance v3, Lcom/google/i/a/a/a/a;

    invoke-direct {v3}, Lcom/google/i/a/a/a/a;-><init>()V

    .line 55
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 57
    iget-object v1, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-nez v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null ssid"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    const/4 v6, 0x6

    if-ge v5, v6, :cond_3

    move v1, v2

    :goto_1
    if-nez v1, :cond_0

    .line 58
    :cond_2
    sget-object v1, Lcom/google/android/gms/cast/e/i;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v5, "wifi ap - %s,%s,%d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    aput-object v7, v6, v2

    iget-object v7, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    aput-object v7, v6, v9

    const/4 v7, 0x2

    iget v8, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v1, v5, v6}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 59
    new-instance v1, Lcom/google/i/a/a/a/d;

    invoke-direct {v1}, Lcom/google/i/a/a/a/d;-><init>()V

    .line 60
    iget-object v5, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/i/a/a/a/d;->a(Ljava/lang/String;)Lcom/google/i/a/a/a/d;

    .line 61
    iget v5, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-virtual {v1, v5}, Lcom/google/i/a/a/a/d;->a(I)Lcom/google/i/a/a/a/d;

    .line 62
    iget v0, v0, Landroid/net/wifi/ScanResult;->frequency:I

    invoke-virtual {v1, v0}, Lcom/google/i/a/a/a/d;->b(I)Lcom/google/i/a/a/a/d;

    .line 63
    invoke-virtual {v3, v1}, Lcom/google/i/a/a/a/a;->a(Lcom/google/i/a/a/a/d;)Lcom/google/i/a/a/a/a;

    goto :goto_0

    .line 57
    :cond_3
    sget-object v5, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-virtual {v1, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    const-string v5, "_nomap"

    invoke-virtual {v1, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    goto :goto_1

    .line 66
    :cond_4
    sget-object v0, Lcom/google/android/gms/cast/e/i;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Number of environment scan results: %d"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-virtual {v3}, Lcom/google/i/a/a/a/a;->c()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {v0, v1, v4}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    return-object v3
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 33
    const-string v0, "%s%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "__cast_nearby__"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()Z
    .locals 3

    .prologue
    const/16 v2, 0x13

    const/4 v0, 0x0

    .line 37
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 38
    if-ge v1, v2, :cond_1

    .line 49
    :cond_0
    :goto_0
    return v0

    .line 44
    :cond_1
    if-ne v1, v2, :cond_2

    const-string v1, "4.4"

    sget-object v2, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 49
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
