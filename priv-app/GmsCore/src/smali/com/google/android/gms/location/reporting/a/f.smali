.class public final Lcom/google/android/gms/location/reporting/a/f;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 6

    .prologue
    .line 54
    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V

    .line 48
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 28
    invoke-static {p1}, Lcom/google/android/gms/location/reporting/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/location/reporting/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 2

    .prologue
    .line 61
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/location/reporting/a/f;->b(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 62
    :catch_0
    move-exception v0

    .line 63
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 64
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 65
    throw v1
.end method

.method public final a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;
    .locals 2

    .prologue
    .line 89
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/location/reporting/a/f;->j()V

    invoke-virtual {p1}, Lcom/google/android/gms/location/reporting/UploadRequest;->b()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1}, Ljava/io/IOException;-><init>()V

    .line 92
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 93
    throw v1

    .line 89
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/location/reporting/a/f;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/location/reporting/a/a;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 153
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->g(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 156
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 136
    const-string v0, "com.google.android.gms.location.reporting.service.START"

    return-object v0
.end method

.method final b(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 1

    .prologue
    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/location/reporting/a/f;->j()V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/gms/location/reporting/a/f;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/location/reporting/a/a;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v0

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const-string v0, "com.google.android.gms.location.reporting.internal.IReportingService"

    return-object v0
.end method

.method public final c(Landroid/accounts/Account;)I
    .locals 1

    .prologue
    .line 76
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/location/reporting/a/f;->d(Landroid/accounts/Account;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 78
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/16 v0, 0x9

    goto :goto_0
.end method

.method final d(Landroid/accounts/Account;)I
    .locals 1

    .prologue
    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/location/reporting/a/f;->j()V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/location/reporting/a/f;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/reporting/a/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/location/reporting/a/a;->b(Landroid/accounts/Account;)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/location/reporting/f;->a(I)I

    move-result v0

    return v0
.end method
