.class public final Lcom/google/android/gms/auth/e/e;
.super Lcom/google/android/gms/auth/e/a;
.source "SourceFile"


# instance fields
.field public d:I

.field public e:J

.field public f:J

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/e/a;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 65
    sget-object v0, Lcom/google/android/gms/auth/b/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/f/i;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/i;-><init>()V

    .line 71
    iget v1, p0, Lcom/google/android/gms/auth/e/e;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/i;->a(I)Lcom/google/android/gms/auth/f/i;

    .line 72
    iget-wide v2, p0, Lcom/google/android/gms/auth/e/e;->e:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/auth/f/i;->a(J)Lcom/google/android/gms/auth/f/i;

    .line 73
    iget-wide v2, p0, Lcom/google/android/gms/auth/e/e;->f:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/auth/f/i;->b(J)Lcom/google/android/gms/auth/f/i;

    .line 74
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/e;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/i;->a(Z)Lcom/google/android/gms/auth/f/i;

    .line 75
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/e;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/i;->b(Z)Lcom/google/android/gms/auth/f/i;

    .line 76
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/e;->i:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/i;->c(Z)Lcom/google/android/gms/auth/f/i;

    .line 77
    iget-wide v2, p0, Lcom/google/android/gms/auth/e/e;->j:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/auth/f/i;->c(J)Lcom/google/android/gms/auth/f/i;

    .line 80
    new-instance v1, Lcom/google/android/gms/auth/f/j;

    invoke-direct {v1}, Lcom/google/android/gms/auth/f/j;-><init>()V

    .line 81
    iget-object v2, p0, Lcom/google/android/gms/auth/e/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/j;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/f/j;

    .line 82
    iget-object v2, p0, Lcom/google/android/gms/auth/e/e;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/j;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/f/j;

    .line 83
    iget-object v2, p0, Lcom/google/android/gms/auth/e/e;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/j;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/f/j;

    .line 84
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/i;->a(Lcom/google/android/gms/auth/f/j;)Lcom/google/android/gms/auth/f/i;

    .line 86
    new-instance v1, Lcom/google/android/gms/auth/f/b;

    invoke-direct {v1}, Lcom/google/android/gms/auth/f/b;-><init>()V

    .line 87
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/b;->a(I)Lcom/google/android/gms/auth/f/b;

    .line 88
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/i;)Lcom/google/android/gms/auth/f/b;

    .line 89
    const-string v0, "GrantCredentialScreen"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/e/e;->a(Ljava/lang/String;Lcom/google/android/gms/auth/f/b;)V

    goto :goto_0
.end method
