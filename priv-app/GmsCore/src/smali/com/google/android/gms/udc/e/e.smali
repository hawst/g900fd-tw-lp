.class public final Lcom/google/android/gms/udc/e/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[B

.field public apiHeader:Lcom/google/android/gms/udc/e/b;

.field public b:Lcom/google/android/gms/udc/e/l;

.field public c:Lcom/google/android/gms/udc/e/w;

.field public d:Lcom/google/android/gms/udc/e/k;

.field public e:Lcom/google/android/gms/udc/e/s;

.field public f:Lcom/google/android/gms/udc/e/s;

.field public g:Lcom/google/android/gms/udc/e/s;

.field public h:[Lcom/google/android/gms/udc/e/q;

.field public i:[Lcom/google/android/gms/udc/e/s;

.field public j:Lcom/google/android/gms/udc/e/s;

.field public k:Lcom/google/android/gms/udc/e/c;

.field public l:Lcom/google/android/gms/udc/e/c;

.field public m:[Lcom/google/android/gms/udc/e/c;

.field public n:Lcom/google/android/gms/udc/e/k;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 69
    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    invoke-static {}, Lcom/google/android/gms/udc/e/q;->a()[Lcom/google/android/gms/udc/e/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    invoke-static {}, Lcom/google/android/gms/udc/e/s;->a()[Lcom/google/android/gms/udc/e/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    invoke-static {}, Lcom/google/android/gms/udc/e/c;->a()[Lcom/google/android/gms/udc/e/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/e;->cachedSize:I

    .line 70
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 323
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 324
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    sget-object v3, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    .line 325
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 328
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    if-eqz v2, :cond_1

    .line 329
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 332
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    if-eqz v2, :cond_2

    .line 333
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 336
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    if-eqz v2, :cond_3

    .line 337
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 340
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    if-eqz v2, :cond_4

    .line 341
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 344
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    if-eqz v2, :cond_5

    .line 345
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 348
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    .line 349
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 350
    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    aget-object v3, v3, v0

    .line 351
    if-eqz v3, :cond_6

    .line 352
    const/16 v4, 0x8

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 349
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_7
    move v0, v2

    .line 357
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    array-length v2, v2

    if-lez v2, :cond_b

    move v2, v0

    move v0, v1

    .line 358
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    array-length v3, v3

    if-ge v0, v3, :cond_a

    .line 359
    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    aget-object v3, v3, v0

    .line 360
    if-eqz v3, :cond_9

    .line 361
    const/16 v4, 0x9

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 358
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_a
    move v0, v2

    .line 366
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    if-eqz v2, :cond_c

    .line 367
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 370
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    if-eqz v2, :cond_d

    .line 371
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 374
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    if-eqz v2, :cond_e

    .line 375
    const/16 v2, 0xc

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 378
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    array-length v2, v2

    if-lez v2, :cond_10

    .line 379
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    array-length v2, v2

    if-ge v1, v2, :cond_10

    .line 380
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    aget-object v2, v2, v1

    .line 381
    if-eqz v2, :cond_f

    .line 382
    const/16 v3, 0xd

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 379
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 387
    :cond_10
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    if-eqz v1, :cond_11

    .line 388
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 391
    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-eqz v1, :cond_12

    .line 392
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 395
    :cond_12
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    if-eqz v1, :cond_13

    .line 396
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 399
    :cond_13
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 95
    if-ne p1, p0, :cond_1

    .line 96
    const/4 v0, 0x1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 98
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/e;

    if-eqz v1, :cond_0

    .line 101
    check-cast p1, Lcom/google/android/gms/udc/e/e;

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v1, :cond_d

    .line 103
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v1, :cond_0

    .line 111
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->a:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    if-nez v1, :cond_e

    .line 115
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    if-nez v1, :cond_0

    .line 123
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    if-nez v1, :cond_f

    .line 124
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    if-nez v1, :cond_0

    .line 132
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_10

    .line 133
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_0

    .line 141
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_11

    .line 142
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 150
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_12

    .line 151
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 159
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_13

    .line 160
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 168
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 176
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_14

    .line 177
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 185
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    if-nez v1, :cond_15

    .line 186
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    if-nez v1, :cond_0

    .line 194
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    if-nez v1, :cond_16

    .line 195
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    if-nez v1, :cond_0

    .line 203
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_17

    .line 208
    iget-object v1, p1, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_0

    .line 216
    :cond_c
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/e;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 107
    :cond_d
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 119
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 128
    :cond_f
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 137
    :cond_10
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 146
    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 155
    :cond_12
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 164
    :cond_13
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 181
    :cond_14
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 190
    :cond_15
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 199
    :cond_16
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 212
    :cond_17
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 224
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 225
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 227
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 229
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 231
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 233
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 235
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 237
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 239
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 241
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 243
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 245
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 247
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 249
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    if-nez v2, :cond_a

    :goto_a
    add-int/2addr v0, v1

    .line 251
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/e;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 252
    return v0

    .line 221
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/b;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 225
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/l;->hashCode()I

    move-result v0

    goto :goto_1

    .line 227
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/w;->hashCode()I

    move-result v0

    goto :goto_2

    .line 229
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/k;->hashCode()I

    move-result v0

    goto :goto_3

    .line 231
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_4

    .line 233
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_5

    .line 235
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_6

    .line 241
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_7

    .line 243
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/c;->hashCode()I

    move-result v0

    goto :goto_8

    .line 245
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/c;->hashCode()I

    move-result v0

    goto :goto_9

    .line 249
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/e/k;->hashCode()I

    move-result v1

    goto :goto_a
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/w;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/udc/e/k;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/udc/e/q;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/android/gms/udc/e/q;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v0, v0

    goto :goto_1

    :cond_8
    new-instance v3, Lcom/google/android/gms/udc/e/q;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_a

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_9

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_b

    new-instance v3, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    array-length v0, v0

    goto :goto_3

    :cond_b
    new-instance v3, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/android/gms/udc/e/c;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_e

    new-instance v0, Lcom/google/android/gms/udc/e/c;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_10

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/udc/e/c;

    if-eqz v0, :cond_f

    iget-object v3, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_11

    new-instance v3, Lcom/google/android/gms/udc/e/c;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    array-length v0, v0

    goto :goto_5

    :cond_11
    new-instance v3, Lcom/google/android/gms/udc/e/c;

    invoke-direct {v3}, Lcom/google/android/gms/udc/e/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    if-nez v0, :cond_12

    new-instance v0, Lcom/google/android/gms/udc/e/k;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-nez v0, :cond_13

    new-instance v0, Lcom/google/android/gms/udc/e/b;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    if-nez v0, :cond_14

    new-instance v0, Lcom/google/android/gms/udc/e/l;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 259
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->a:[B

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    if-eqz v0, :cond_1

    .line 262
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->c:Lcom/google/android/gms/udc/e/w;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    if-eqz v0, :cond_2

    .line 265
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->d:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 267
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_3

    .line 268
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 270
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_4

    .line 271
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->f:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 273
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_5

    .line 274
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->g:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 276
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 277
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 278
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    aget-object v2, v2, v0

    .line 279
    if-eqz v2, :cond_6

    .line 280
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 277
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 284
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    array-length v0, v0

    if-lez v0, :cond_9

    move v0, v1

    .line 285
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 286
    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->i:[Lcom/google/android/gms/udc/e/s;

    aget-object v2, v2, v0

    .line 287
    if-eqz v2, :cond_8

    .line 288
    const/16 v3, 0x9

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 285
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 292
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_a

    .line 293
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->j:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 295
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    if-eqz v0, :cond_b

    .line 296
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->k:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 298
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    if-eqz v0, :cond_c

    .line 299
    const/16 v0, 0xc

    iget-object v2, p0, Lcom/google/android/gms/udc/e/e;->l:Lcom/google/android/gms/udc/e/c;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 301
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    array-length v0, v0

    if-lez v0, :cond_e

    .line 302
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    array-length v0, v0

    if-ge v1, v0, :cond_e

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->m:[Lcom/google/android/gms/udc/e/c;

    aget-object v0, v0, v1

    .line 304
    if-eqz v0, :cond_d

    .line 305
    const/16 v2, 0xd

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 302
    :cond_d
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 309
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    if-eqz v0, :cond_f

    .line 310
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->n:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 312
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    if-eqz v0, :cond_10

    .line 313
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->apiHeader:Lcom/google/android/gms/udc/e/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 315
    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    if-eqz v0, :cond_11

    .line 316
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/gms/udc/e/e;->b:Lcom/google/android/gms/udc/e/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 318
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 319
    return-void
.end method
