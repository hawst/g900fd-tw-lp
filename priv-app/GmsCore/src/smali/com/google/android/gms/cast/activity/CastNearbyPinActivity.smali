.class public Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"


# static fields
.field static final a:J

.field private static final b:Lcom/google/android/gms/cast/e/h;

.field private static final c:Ljava/lang/String;


# instance fields
.field private final d:Landroid/os/Handler;

.field private e:Lcom/google/android/gms/cast/activity/d;

.field private f:Z

.field private g:Lcom/google/android/gms/cast/e/a;

.field private h:I

.field private final i:Ljava/lang/Runnable;

.field private final j:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

.field private final k:Lcom/google/android/gms/audiomodem/bm;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastNearbyPinActivity"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b:Lcom/google/android/gms/cast/e/h;

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/cast/d/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/message"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->c:Ljava/lang/String;

    .line 52
    sget-object v0, Lcom/google/android/gms/cast/a/b;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 55
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d:Landroid/os/Handler;

    .line 58
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->f:Z

    .line 62
    new-instance v0, Lcom/google/android/gms/cast/activity/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/activity/a;-><init>(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->i:Ljava/lang/Runnable;

    .line 69
    invoke-static {}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->a()Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->j:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    .line 72
    new-instance v0, Lcom/google/android/gms/cast/activity/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/activity/b;-><init>(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->k:Lcom/google/android/gms/audiomodem/bm;

    .line 246
    return-void
.end method

.method protected static a([B)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 217
    if-eqz p0, :cond_0

    array-length v0, p0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 218
    :cond_0
    const/4 v0, 0x0

    .line 228
    :goto_0
    return-object v0

    .line 222
    :cond_1
    const-string v0, ""

    .line 223
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v2

    shr-int/lit8 v1, v1, 0x4

    and-int/lit8 v1, v1, 0xf

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 224
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v2

    and-int/lit8 v1, v1, 0xf

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v3

    shr-int/lit8 v1, v1, 0x4

    and-int/lit8 v1, v1, 0xf

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 227
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    aget-byte v1, p0, v3

    and-int/lit8 v1, v1, 0xf

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a(Ljava/lang/String;ZZ)V

    return-void
.end method

.method private a(Ljava/lang/String;ZZ)V
    .locals 2

    .prologue
    .line 295
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.cast.session.CAST_NEARBY_PIN_RESPONSE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 297
    const-string v1, "PIN"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 298
    const-string v1, "MANUAL"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 299
    const-string v1, "CANCELED"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 300
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->f:Z

    .line 301
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 302
    return-void
.end method

.method static synthetic b()Lcom/google/android/gms/cast/e/h;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b:Lcom/google/android/gms/cast/e/h;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->f:Z

    return v0
.end method

.method static b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 232
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "[0-9][0-9][0-9][0-9]"

    invoke-virtual {p0, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->i:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d:Landroid/os/Handler;

    return-object v0
.end method

.method private d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    if-nez v0, :cond_0

    .line 244
    :goto_0
    return-void

    .line 239
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "STOP Listening for audio pin."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 240
    new-instance v0, Lcom/google/android/gms/cast/activity/e;

    const-string v1, "stopaudio"

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/cast/activity/e;-><init>(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;Ljava/lang/String;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/activity/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    iget-object v1, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->k:Lcom/google/android/gms/audiomodem/bm;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/e/a;->a(Lcom/google/android/gms/audiomodem/bm;)V

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/a;->b()V

    .line 243
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    goto :goto_0
.end method

.method private e()V
    .locals 5

    .prologue
    .line 305
    sget-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "finishActivity: mResponseSent: %b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-boolean v4, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->f:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    .line 310
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->finish()V

    .line 311
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e()V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 203
    sget-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "START Listening for audio pin."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 205
    new-instance v0, Lcom/google/android/gms/cast/activity/e;

    const-string v1, "playaudio"

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/cast/activity/e;-><init>(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;Ljava/lang/String;)V

    new-array v1, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/activity/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 206
    iput-boolean v3, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->f:Z

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    if-nez v0, :cond_0

    .line 208
    new-instance v0, Lcom/google/android/gms/cast/e/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/e/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/a;->a()V

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->g:Lcom/google/android/gms/cast/e/a;

    iget-object v1, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->k:Lcom/google/android/gms/audiomodem/bm;

    iget-object v2, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->j:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/a;->a(Lcom/google/android/gms/audiomodem/bm;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->i:Ljava/lang/Runnable;

    sget-wide v2, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 214
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 196
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a(Ljava/lang/String;ZZ)V

    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d()V

    .line 198
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e()V

    .line 199
    return-void
.end method

.method public finish()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 191
    invoke-super {p0}, Landroid/support/v4/app/q;->finish()V

    .line 192
    invoke-virtual {p0, v0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->overridePendingTransition(II)V

    .line 193
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 116
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 117
    if-nez p1, :cond_0

    .line 118
    const-string v0, "google_cast"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 119
    const-string v1, "castnearby.optIn"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dw:I

    new-instance v2, Lcom/google/android/gms/cast/activity/i;

    invoke-direct {v2}, Lcom/google/android/gms/cast/activity/i;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 133
    :cond_0
    :goto_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->requestWindowFeature(I)Z

    .line 134
    sget v0, Lcom/google/android/gms/l;->V:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->setContentView(I)V

    .line 136
    new-instance v0, Lcom/google/android/gms/cast/activity/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/activity/d;-><init>(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    .line 137
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.cast.session.ACTION_CAST_NEARBY_ABORTED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 140
    return-void

    .line 123
    :cond_1
    const-string v1, "castnearby.optIn"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dw:I

    new-instance v2, Lcom/google/android/gms/cast/activity/f;

    invoke-direct {v2}, Lcom/google/android/gms/cast/activity/f;-><init>()V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_0

    .line 128
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dw:I

    invoke-static {v2}, Lcom/google/android/gms/cast/activity/l;->a(Z)Lcom/google/android/gms/cast/activity/l;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_0
.end method

.method protected onRestart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 162
    invoke-super {p0}, Landroid/support/v4/app/q;->onRestart()V

    .line 163
    sget-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onRestart."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 164
    iget-boolean v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->f:Z

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onRestart:finish"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->finish()V

    .line 171
    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    .line 155
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->getRequestedOrientation()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->h:I

    .line 157
    const/16 v0, 0xe

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->setRequestedOrientation(I)V

    .line 158
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 175
    iget v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->setRequestedOrientation(I)V

    .line 176
    invoke-direct {p0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d()V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 179
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->e:Lcom/google/android/gms/cast/activity/d;

    .line 183
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->f:Z

    if-nez v0, :cond_1

    .line 184
    const-string v0, ""

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->a(Ljava/lang/String;ZZ)V

    .line 186
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 187
    return-void
.end method
