.class public final Lcom/google/android/gms/photos/autobackup/ui/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/support/v4/app/Fragment;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->a:Landroid/support/v4/app/Fragment;

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/support/v4/app/Fragment;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->a:Landroid/support/v4/app/Fragment;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;ZZ)Landroid/app/AlertDialog;
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 42
    new-instance v1, Lcom/google/android/gms/photos/autobackup/ui/ac;

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    invoke-direct {v1, p0, v0, p1}, Lcom/google/android/gms/photos/autobackup/ui/ac;-><init>(Lcom/google/android/gms/photos/autobackup/ui/ab;Landroid/content/Context;Ljava/lang/String;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->dw:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 46
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 47
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->se:I

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object p1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 55
    :goto_0
    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    sget v3, Lcom/google/android/gms/p;->sm:I

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->yM:I

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->rk:I

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 49
    :cond_0
    if-nez p2, :cond_1

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->si:I

    invoke-virtual {v0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 52
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/ab;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->sd:I

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v0, v4, v5

    aput-object p1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
