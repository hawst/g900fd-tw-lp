.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/q;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

.field private c:Ljava/lang/String;

.field private final d:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->d:Ljava/util/Set;

    .line 146
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v2whitelisted/models/p;
    .locals 5

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->d:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->b:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->c:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v2whitelisted/models/r;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;
    .locals 2

    .prologue
    .line 158
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->b:Lcom/google/android/gms/plus/service/v2whitelisted/models/PeopleMutateCircleMembershipsRequestEntity$ItemsEntity$CircleMemberIdEntity;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->d:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 160
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;
    .locals 2

    .prologue
    .line 152
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->a:Ljava/lang/String;

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->d:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 154
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v2whitelisted/models/q;
    .locals 2

    .prologue
    .line 167
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->c:Ljava/lang/String;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/q;->d:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 169
    return-object p0
.end method
