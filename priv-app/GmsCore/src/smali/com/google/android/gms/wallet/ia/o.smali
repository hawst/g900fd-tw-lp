.class final Lcom/google/android/gms/wallet/ia/o;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V
    .locals 0

    .prologue
    .line 424
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 458
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->i(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V

    .line 459
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ai;)V
    .locals 3

    .prologue
    .line 427
    iget v0, p1, Lcom/google/checkout/inapp/proto/ai;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 428
    const-string v0, "PickInstrumentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected account status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/checkout/inapp/proto/ai;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V

    .line 444
    :cond_0
    :goto_0
    return-void

    .line 432
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;Lcom/google/checkout/inapp/proto/ai;)Lcom/google/checkout/inapp/proto/ai;

    .line 433
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(Lcom/google/checkout/inapp/proto/ai;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;[Lcom/google/checkout/inapp/proto/j;)V

    .line 435
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 436
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->c(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->d(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->c(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->e(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/i;

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/h;

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->j(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V

    .line 464
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 448
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V

    .line 449
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/o;->a:Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->h(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V

    .line 454
    return-void
.end method
