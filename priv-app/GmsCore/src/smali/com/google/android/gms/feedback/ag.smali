.class final Lcom/google/android/gms/feedback/ag;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/io/File;

.field final synthetic b:Z

.field final synthetic c:Ljava/io/File;

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Lcom/google/android/gms/feedback/Screenshot;


# direct methods
.method constructor <init>(Ljava/io/File;ZLjava/io/File;Ljava/lang/String;Lcom/google/android/gms/feedback/Screenshot;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/gms/feedback/ag;->a:Ljava/io/File;

    iput-boolean p2, p0, Lcom/google/android/gms/feedback/ag;->b:Z

    iput-object p3, p0, Lcom/google/android/gms/feedback/ag;->c:Ljava/io/File;

    iput-object p4, p0, Lcom/google/android/gms/feedback/ag;->d:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/feedback/ag;->e:Lcom/google/android/gms/feedback/Screenshot;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/feedback/ag;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/ag;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/feedback/Screenshot;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 191
    check-cast p1, Lcom/google/android/gms/feedback/Screenshot;

    iget-object v0, p0, Lcom/google/android/gms/feedback/ag;->a:Ljava/io/File;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/ag;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/ag;->c:Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ag;->d:Ljava/lang/String;

    const-string v2, ".bmp"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/w;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/ag;->e:Lcom/google/android/gms/feedback/Screenshot;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/feedback/Screenshot;->a(Lcom/google/android/gms/feedback/Screenshot;)Lcom/google/android/gms/feedback/Screenshot;

    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->f()Lcom/google/android/gms/feedback/t;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/feedback/t;->a(Lcom/google/android/gms/feedback/Screenshot;)V

    return-void
.end method
