.class public abstract Lcom/google/android/gms/cast/c/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:I


# instance fields
.field protected final b:Lcom/google/android/gms/cast/e/h;

.field final c:Landroid/content/Context;

.field final d:Landroid/os/Handler;

.field protected final e:Ljava/util/Map;

.field protected final f:Ljava/lang/String;

.field private final g:Ljava/util/List;

.field private h:I

.field private final i:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final j:Landroid/net/ConnectivityManager;

.field private k:Lcom/google/android/gms/cast/c/x;

.field private final l:Landroid/net/wifi/WifiManager;

.field private m:Ljava/lang/String;

.field private volatile n:Z

.field private o:Z

.field private p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/android/gms/cast/a/c;->h:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/q;->a:I

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "DeviceScanner"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    .line 98
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/c/q;->h:I

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;)V

    .line 112
    iput-object p1, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    .line 113
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    .line 114
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->g:Ljava/util/List;

    .line 115
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 117
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->j:Landroid/net/ConnectivityManager;

    .line 119
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->l:Landroid/net/wifi/WifiManager;

    .line 120
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    .line 121
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->mK:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->f:Ljava/lang/String;

    .line 123
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/q;)I
    .locals 1

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/gms/cast/c/q;->h:I

    return v0
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 392
    iget v0, p0, Lcom/google/android/gms/cast/c/q;->h:I

    if-ne v0, p1, :cond_1

    .line 407
    :cond_0
    :goto_0
    return-void

    .line 395
    :cond_1
    iput p1, p0, Lcom/google/android/gms/cast/c/q;->h:I

    .line 396
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->h()Ljava/util/List;

    move-result-object v0

    .line 397
    if-eqz v0, :cond_0

    .line 398
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/c/r;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/cast/c/r;-><init>(Lcom/google/android/gms/cast/c/q;Ljava/util/List;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/c/q;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->i()V

    return-void
.end method

.method private c()Ljava/util/List;
    .locals 5

    .prologue
    .line 204
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 207
    :try_start_0
    invoke-static {}, Ljava/net/NetworkInterface;->getNetworkInterfaces()Ljava/util/Enumeration;

    move-result-object v2

    .line 209
    if-eqz v2, :cond_1

    .line 210
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    invoke-interface {v2}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/NetworkInterface;

    .line 212
    invoke-virtual {v0}, Ljava/net/NetworkInterface;->isUp()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/q;->a(Ljava/net/NetworkInterface;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 213
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    iget-object v2, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Exception while selecting network interface"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 221
    :cond_1
    return-object v1
.end method

.method static synthetic c(Lcom/google/android/gms/cast/c/q;)V
    .locals 3

    .prologue
    .line 47
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/q;->o:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "reportNetworkError; errorState now true"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/c/q;->o:Z

    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/q;->b()V

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/q;->a(I)V

    :cond_0
    return-void
.end method

.method private declared-synchronized d()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 234
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "startScanInit"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/cast/c/q;->o:Z

    .line 236
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/cast/c/q;->n:Z

    .line 238
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->c()Ljava/util/List;

    move-result-object v3

    .line 239
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 240
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "No suitable network interfaces to scan on!"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 249
    :goto_0
    monitor-exit p0

    return v0

    .line 244
    :cond_0
    const/4 v2, 0x0

    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/cast/c/q;->l:Landroid/net/wifi/WifiManager;

    invoke-virtual {v4}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v2

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/cast/c/q;->m:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "BSSID changed"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/c/q;->m:Ljava/lang/String;

    move v0, v1

    :cond_2
    if-eqz v0, :cond_3

    .line 245
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/q;->b()V

    .line 248
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/q;->p:Z

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/cast/c/q;->a(Ljava/util/List;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 249
    goto :goto_0

    .line 234
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 281
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/google/android/gms/cast/c/q;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v2, v1, :cond_0

    .line 287
    :goto_0
    monitor-exit p0

    return v0

    .line 284
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "stopScanInit from thread %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->getId()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/c/q;->n:Z

    .line 286
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/q;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 287
    goto :goto_0

    .line 281
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h()Ljava/util/List;
    .locals 3

    .prologue
    .line 491
    const/4 v0, 0x0

    .line 492
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->g:Ljava/util/List;

    monitor-enter v1

    .line 493
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/c/q;->g:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 494
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/q;->g:Ljava/util/List;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 496
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 497
    return-object v0

    .line 496
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private i()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->j:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 536
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 537
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "maybeRestartScan: connected? %b, errorState? %b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lcom/google/android/gms/cast/c/q;->o:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 540
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->g()Z

    move-result v3

    .line 542
    if-eqz v0, :cond_3

    .line 544
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "have connectivity; starting scan"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 545
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 546
    if-nez v3, :cond_0

    .line 547
    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/c/q;->a(I)V

    .line 563
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 536
    goto :goto_0

    .line 550
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->j()V

    goto :goto_1

    .line 554
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->m:Ljava/lang/String;

    .line 555
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/q;->b()V

    .line 558
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/q;->o:Z

    if-nez v0, :cond_0

    .line 559
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "lost connectivity while scanning"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 560
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->j()V

    goto :goto_1
.end method

.method private j()V
    .locals 2

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 584
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/c/w;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/c/w;-><init>(Lcom/google/android/gms/cast/c/q;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 591
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final a(Lcom/google/android/gms/cast/c/z;)V
    .locals 3

    .prologue
    .line 139
    if-nez p1, :cond_0

    .line 140
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->g:Ljava/util/List;

    monitor-enter v1

    .line 144
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "the same listener cannot be added twice"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 147
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 148
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method protected abstract a(Ljava/lang/String;)V
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 361
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    monitor-enter v1

    .line 362
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/y;

    .line 363
    if-eqz v0, :cond_0

    .line 364
    iput-wide p2, v0, Lcom/google/android/gms/cast/c/y;->b:J

    .line 366
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract a(Ljava/util/List;Z)V
.end method

.method protected abstract a(Z)V
.end method

.method protected abstract a(Ljava/net/NetworkInterface;)Z
.end method

.method public b()V
    .locals 3

    .prologue
    .line 313
    const/4 v0, 0x0

    .line 315
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    monitor-enter v1

    .line 316
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 318
    const/4 v0, 0x1

    .line 320
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 322
    if-eqz v0, :cond_1

    .line 323
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->h()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/c/v;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/cast/c/v;-><init>(Lcom/google/android/gms/cast/c/q;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 325
    :cond_1
    return-void

    .line 320
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final b(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "notifyDeviceOnline: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 414
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->h()Ljava/util/List;

    move-result-object v0

    .line 415
    if-eqz v0, :cond_0

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/c/s;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/cast/c/s;-><init>(Lcom/google/android/gms/cast/c/q;Ljava/util/List;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 425
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 333
    const/4 v1, 0x0

    .line 335
    iget-object v2, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    monitor-enter v2

    .line 336
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/y;

    .line 337
    if-eqz v0, :cond_1

    .line 338
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/cast/c/y;->b:J

    .line 339
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/cast/c/y;->d:Z

    .line 340
    iget-object v0, v0, Lcom/google/android/gms/cast/c/y;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 342
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 344
    if-eqz v0, :cond_0

    .line 345
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "notifyDeviceOffline: because it was marked invalid"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 346
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/q;->d(Lcom/google/android/gms/cast/CastDevice;)V

    .line 348
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/cast/c/q;->a(Ljava/lang/String;)V

    .line 349
    return-void

    .line 342
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public final declared-synchronized b(Z)V
    .locals 1

    .prologue
    .line 192
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/q;->p:Z

    if-eq v0, p1, :cond_0

    .line 193
    iput-boolean p1, p0, Lcom/google/android/gms/cast/c/q;->p:Z

    .line 194
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/q;->p:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/q;->a(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 196
    :cond_0
    monitor-exit p0

    return-void

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final c(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    .line 431
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "notifyDeviceStateChanged: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 432
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->h()Ljava/util/List;

    move-result-object v0

    .line 433
    if-eqz v0, :cond_0

    .line 434
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/c/t;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/cast/c/t;-><init>(Lcom/google/android/gms/cast/c/q;Ljava/util/List;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 443
    :cond_0
    return-void
.end method

.method protected final d(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "notifyDeviceOffline: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 450
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->h()Ljava/util/List;

    move-result-object v0

    .line 451
    if-eqz v0, :cond_0

    .line 452
    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/cast/c/u;

    invoke-direct {v2, p0, v0, p1}, Lcom/google/android/gms/cast/c/u;-><init>(Lcom/google/android/gms/cast/c/q;Ljava/util/List;Lcom/google/android/gms/cast/CastDevice;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 461
    :cond_0
    return-void
.end method

.method public final declared-synchronized e()V
    .locals 3

    .prologue
    .line 183
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/c/q;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 189
    :goto_0
    monitor-exit p0

    return-void

    .line 187
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->k:Lcom/google/android/gms/cast/c/x;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/cast/c/x;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/cast/c/x;-><init>(Lcom/google/android/gms/cast/c/q;B)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->k:Lcom/google/android/gms/cast/c/x;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/q;->k:Lcom/google/android/gms/cast/c/x;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 188
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->i()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()V
    .locals 3

    .prologue
    .line 264
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/c/q;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 275
    :goto_0
    monitor-exit p0

    return-void

    .line 268
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->k:Lcom/google/android/gms/cast/c/x;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_1

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->k:Lcom/google/android/gms/cast/c/x;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/google/android/gms/cast/c/q;->k:Lcom/google/android/gms/cast/c/x;

    .line 270
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/q;->g()Z

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 273
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/q;->a(I)V

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "scan stopped"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 264
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
