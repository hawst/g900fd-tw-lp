.class public Lcom/google/android/gms/wallet/common/ui/FormEditText;
.super Landroid/widget/AutoCompleteTextView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/do;


# instance fields
.field private final a:Lcom/google/android/gms/wallet/common/ui/validator/d;

.field b:Ljava/util/LinkedList;

.field final c:Landroid/text/TextWatcher;

.field private final d:Lcom/google/android/gms/wallet/common/ui/validator/d;

.field private e:Lcom/google/android/gms/wallet/common/ui/a/b;

.field private f:Lcom/google/android/gms/wallet/common/ui/dn;

.field private g:Lcom/google/android/gms/wallet/common/ui/am;

.field private h:Lcom/google/android/gms/wallet/common/ui/dn;

.field private i:Z

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:I

.field private m:Lcom/google/android/gms/wallet/common/ui/validator/q;

.field private n:Ljava/lang/CharSequence;

.field private o:I

.field private p:Z

.field private final q:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    invoke-direct {p0, p1}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Z

    .line 90
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:Ljava/lang/String;

    .line 91
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:I

    .line 525
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bx;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bx;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Landroid/text/TextWatcher;

    .line 546
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/by;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/by;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->q:Landroid/text/TextWatcher;

    .line 106
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 107
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 108
    iput-object p0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lcom/google/android/gms/wallet/common/ui/dn;

    .line 110
    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 111
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Z

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:Ljava/lang/String;

    .line 91
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:I

    .line 525
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bx;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bx;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Landroid/text/TextWatcher;

    .line 546
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/by;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/by;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->q:Landroid/text/TextWatcher;

    .line 115
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 116
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 117
    iput-object p0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lcom/google/android/gms/wallet/common/ui/dn;

    .line 119
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 123
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Z

    .line 90
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:Ljava/lang/String;

    .line 91
    const v0, 0x7fffffff

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:I

    .line 525
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bx;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bx;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Landroid/text/TextWatcher;

    .line 546
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/by;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/by;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->q:Landroid/text/TextWatcher;

    .line 124
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 125
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 126
    iput-object p0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lcom/google/android/gms/wallet/common/ui/dn;

    .line 128
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 129
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Lcom/google/android/gms/wallet/common/ui/validator/d;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->n:Ljava/lang/CharSequence;

    return-object p1
.end method

.method private a()Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Ljava/util/LinkedList;

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Ljava/util/LinkedList;

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b:Ljava/util/LinkedList;

    return-object v0
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const v6, 0x7fffffff

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 192
    new-array v0, v4, [I

    const v1, 0x1010160

    aput v1, v0, v5

    .line 195
    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 196
    invoke-virtual {v0, v5, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:I

    .line 197
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 199
    sget-object v0, Lcom/google/android/gms/r;->aY:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 201
    sget v0, Lcom/google/android/gms/r;->aZ:I

    invoke-virtual {v2, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    .line 202
    sget v0, Lcom/google/android/gms/r;->bb:I

    invoke-virtual {v2, v0, v5}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Z

    .line 204
    sget v0, Lcom/google/android/gms/r;->bc:I

    invoke-virtual {v2, v0}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 207
    sget v1, Lcom/google/android/gms/r;->be:I

    invoke-virtual {v2, v1, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    .line 210
    packed-switch v1, :pswitch_data_0

    .line 213
    const/4 v0, 0x0

    .line 259
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    if-eqz v1, :cond_1

    .line 260
    sget v1, Lcom/google/android/gms/r;->ba:I

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:Ljava/lang/String;

    .line 262
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    sget v1, Lcom/google/android/gms/p;->Bt:I

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:Ljava/lang/String;

    .line 266
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c()V

    .line 269
    :cond_1
    if-eqz v0, :cond_2

    .line 270
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 272
    :cond_2
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 277
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getImeOptions()I

    move-result v0

    const/high16 v1, 0x2000000

    or-int/2addr v0, v1

    const/high16 v1, 0x10000000

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setImeOptions(I)V

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->q:Landroid/text/TextWatcher;

    invoke-super {p0, v0}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c:Landroid/text/TextWatcher;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Landroid/text/TextWatcher;)V

    .line 283
    invoke-virtual {p0, v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    .line 284
    return-void

    .line 216
    :pswitch_0
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/j;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    sget v0, Lcom/google/android/gms/p;->Bu:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/common/ui/validator/j;-><init>(Ljava/lang/CharSequence;)V

    .line 219
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v0, v1

    .line 220
    goto :goto_0

    .line 222
    :pswitch_1
    sget v1, Lcom/google/android/gms/r;->bd:I

    invoke-virtual {v2, v1}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 224
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/p;

    invoke-direct {v1, v0, v3}, Lcom/google/android/gms/wallet/common/ui/validator/p;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    move-object v0, v1

    .line 225
    goto :goto_0

    .line 227
    :pswitch_2
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/e;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    sget v0, Lcom/google/android/gms/p;->Bp:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_4
    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/common/ui/validator/e;-><init>(Ljava/lang/CharSequence;)V

    .line 236
    const-string v0, "1234567890 "

    invoke-static {v0}, Landroid/text/method/DigitsKeyListener;->getInstance(Ljava/lang/String;)Landroid/text/method/DigitsKeyListener;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setKeyListener(Landroid/text/method/KeyListener;)V

    .line 239
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setSingleLine()V

    .line 241
    new-array v0, v4, [Landroid/text/InputFilter;

    new-instance v3, Landroid/text/InputFilter$LengthFilter;

    const/16 v4, 0x13

    invoke-direct {v3, v4}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v3, v0, v5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    move-object v0, v1

    .line 243
    goto/16 :goto_0

    .line 245
    :pswitch_3
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/h;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    sget v0, Lcom/google/android/gms/p;->Br:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_5
    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/common/ui/validator/h;-><init>(Ljava/lang/CharSequence;)V

    .line 248
    const/16 v0, 0x21

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v0, v1

    .line 250
    goto/16 :goto_0

    .line 252
    :pswitch_4
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/m;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_6

    sget v0, Lcom/google/android/gms/p;->Bv:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_6
    invoke-direct {v1, v0}, Lcom/google/android/gms/wallet/common/ui/validator/m;-><init>(Ljava/lang/CharSequence;)V

    .line 255
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v0, v1

    goto/16 :goto_0

    .line 210
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method

.method private b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 394
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->zr:I

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/FormEditText;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->n:Ljava/lang/CharSequence;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 511
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    if-eqz v0, :cond_2

    .line 516
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->m:Lcom/google/android/gms/wallet/common/ui/validator/q;

    if-nez v0, :cond_0

    .line 517
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/q;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->k:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/common/ui/validator/q;-><init>(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->m:Lcom/google/android/gms/wallet/common/ui/validator/q;

    .line 519
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->m:Lcom/google/android/gms/wallet/common/ui/validator/q;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 523
    :cond_1
    :goto_0
    return-void

    .line 520
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->m:Lcom/google/android/gms/wallet/common/ui/validator/q;

    if-eqz v0, :cond_1

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->m:Lcom/google/android/gms/wallet/common/ui/validator/q;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 336
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 337
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/ah;)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lcom/google/android/gms/wallet/common/ui/a/b;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lcom/google/android/gms/wallet/common/ui/a/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/a/b;->a(Lcom/google/android/gms/wallet/common/ui/ah;)V

    .line 175
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lcom/google/android/gms/wallet/common/ui/a/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->g:Lcom/google/android/gms/wallet/common/ui/am;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->h:Lcom/google/android/gms/wallet/common/ui/dn;

    if-ne v0, p2, :cond_0

    .line 158
    :goto_0
    return-void

    .line 150
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/a/b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/a/b;-><init>(Landroid/widget/EditText;Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lcom/google/android/gms/wallet/common/ui/a/b;

    if-nez v1, :cond_1

    .line 153
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 155
    :cond_1
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e:Lcom/google/android/gms/wallet/common/ui/a/b;

    .line 156
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->g:Lcom/google/android/gms/wallet/common/ui/am;

    .line 157
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->h:Lcom/google/android/gms/wallet/common/ui/dn;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/dn;)V
    .locals 0

    .prologue
    .line 436
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lcom/google/android/gms/wallet/common/ui/dn;

    .line 437
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 343
    return-void
.end method

.method public final a(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->p:Z

    .line 297
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->replaceText(Ljava/lang/CharSequence;)V

    .line 302
    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->p:Z

    .line 303
    return-void

    .line 300
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 417
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->i:Z

    .line 418
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->c()V

    .line 419
    return-void
.end method

.method public addTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 327
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 328
    return-void
.end method

.method public final b(Lcom/google/android/gms/wallet/common/ui/validator/r;)V
    .locals 1

    .prologue
    .line 348
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 352
    return-void
.end method

.method public beginBatchEdit()V
    .locals 1

    .prologue
    .line 584
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 585
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 587
    :cond_0
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->beginBatchEdit()V

    .line 588
    return-void
.end method

.method public final c(Lcom/google/android/gms/wallet/common/ui/validator/r;)V
    .locals 1

    .prologue
    .line 371
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->b(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->b(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 373
    return-void
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 187
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->l:I

    return v0
.end method

.method public dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z
    .locals 2

    .prologue
    .line 625
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->dispatchPopulateAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 629
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/accessibility/AccessibilityEvent;->getEventType()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 632
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 635
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->j:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public enoughToFilter()Z
    .locals 1

    .prologue
    .line 307
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->p:Z

    if-eqz v0, :cond_0

    .line 308
    const/4 v0, 0x0

    .line 310
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->enoughToFilter()Z

    move-result v0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 383
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    .line 384
    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 385
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    .line 389
    :cond_0
    :goto_0
    return v0

    .line 386
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 387
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setError(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method final g()V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 406
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 410
    :cond_0
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .prologue
    .line 592
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v1

    .line 593
    if-eqz v1, :cond_0

    .line 594
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bz;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/wallet/common/ui/bz;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Landroid/view/inputmethod/InputConnection;)V

    .line 596
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1

    .prologue
    .line 441
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lcom/google/android/gms/wallet/common/ui/dn;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f:Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    .line 444
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 445
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->g()V

    .line 447
    :cond_1
    invoke-super {p0, p1, p2, p3}, Landroid/widget/AutoCompleteTextView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 448
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    .prologue
    .line 452
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->onMeasure(II)V

    .line 455
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->o:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    .line 456
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 612
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 613
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 621
    :goto_0
    return-void

    .line 616
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 617
    const-string v0, "superInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/AutoCompleteTextView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 618
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    const-string v1, "focusChangeValidators"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->b(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 620
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    const-string v1, "textChangeValidators"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->b(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 602
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 603
    const-string v1, "superInstanceState"

    invoke-super {p0}, Landroid/widget/AutoCompleteTextView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 604
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a:Lcom/google/android/gms/wallet/common/ui/validator/d;

    const-string v2, "focusChangeValidators"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 606
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->d:Lcom/google/android/gms/wallet/common/ui/validator/d;

    const-string v2, "textChangeValidators"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    .line 607
    return-object v0
.end method

.method public removeTextChangedListener(Landroid/text/TextWatcher;)V
    .locals 1

    .prologue
    .line 332
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a()Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 333
    return-void
.end method

.method public setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 463
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 465
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_0

    .line 466
    check-cast p1, Landroid/text/Spannable;

    .line 467
    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v1, Ljava/lang/Object;

    invoke-interface {p1, v2, v0, v1}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    .line 468
    if-eqz v0, :cond_1

    array-length v0, v0

    if-eqz v0, :cond_1

    .line 469
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Spans in error text are not supported due to Gingerbread limitations."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 473
    :cond_0
    new-instance v0, Landroid/text/SpannableString;

    invoke-direct {v0, p1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object p1, v0

    .line 475
    :cond_1
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    const/high16 v1, -0x1000000

    invoke-direct {v0, v1}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-interface {p1}, Landroid/text/Spannable;->length()I

    move-result v1

    invoke-interface {p1, v0, v2, v1, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 479
    :cond_2
    invoke-super {p0, p1, p2}, Landroid/widget/AutoCompleteTextView;->setError(Ljava/lang/CharSequence;Landroid/graphics/drawable/Drawable;)V

    .line 480
    return-void
.end method

.method public setThreshold(I)V
    .locals 2

    .prologue
    .line 484
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;->o:I

    .line 485
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    float-to-int v1, v1

    div-int/2addr v0, v1

    const/16 v1, 0x8c

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 486
    invoke-super {p0, p1}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 491
    :goto_1
    return-void

    .line 485
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 488
    :cond_1
    const v0, 0x7fffffff

    invoke-super {p0, v0}, Landroid/widget/AutoCompleteTextView;->setThreshold(I)V

    .line 489
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->dismissDropDown()V

    goto :goto_1
.end method
