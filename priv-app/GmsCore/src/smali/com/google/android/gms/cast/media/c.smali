.class final Lcom/google/android/gms/cast/media/c;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/media/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/a;)V
    .locals 0

    .prologue
    .line 283
    iput-object p1, p0, Lcom/google/android/gms/cast/media/c;->a:Lcom/google/android/gms/cast/media/a;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 286
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "GMS Checkin complete - checking flags."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/cast/media/c;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->g(Lcom/google/android/gms/cast/media/a;)Lcom/google/android/gms/cast/c/e;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/cast/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    new-instance v0, Lcom/google/android/gms/cast/c/e;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/c/e;-><init>(Landroid/content/Context;)V

    .line 289
    iget-object v1, p0, Lcom/google/android/gms/cast/media/c;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v1}, Lcom/google/android/gms/cast/media/a;->h(Lcom/google/android/gms/cast/media/a;)Lcom/google/android/gms/cast/c/z;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/c/q;->a(Lcom/google/android/gms/cast/c/z;)V

    .line 290
    iget-object v1, p0, Lcom/google/android/gms/cast/media/c;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v1}, Lcom/google/android/gms/cast/media/a;->b(Lcom/google/android/gms/cast/media/a;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 291
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "CastNearbyDeviceScanner enabled after Checkin."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/cast/media/c;->a:Lcom/google/android/gms/cast/media/a;

    iget-object v0, v0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/c;->a:Lcom/google/android/gms/cast/media/a;

    invoke-static {v1}, Lcom/google/android/gms/cast/media/a;->i(Lcom/google/android/gms/cast/media/a;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 294
    :cond_0
    return-void
.end method
