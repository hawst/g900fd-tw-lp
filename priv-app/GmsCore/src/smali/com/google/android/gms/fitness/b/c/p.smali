.class public final Lcom/google/android/gms/fitness/b/c/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/fitness/b/b;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/fitness/b/c/p;->e:I

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/fitness/b/c/o;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/p;->a:Lcom/google/android/gms/fitness/b/b;

    if-nez v0, :cond_0

    .line 58
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "application not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/p;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 61
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mergedStreamName not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/p;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 64
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "outputDataTypeName not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/p;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 67
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "outputStreamName not set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_3
    iget v0, p0, Lcom/google/android/gms/fitness/b/c/p;->e:I

    if-gez v0, :cond_4

    .line 70
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid readBehindSeconds specified"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_4
    new-instance v0, Lcom/google/android/gms/fitness/b/c/o;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/fitness/b/c/o;-><init>(Lcom/google/android/gms/fitness/b/c/p;B)V

    return-object v0
.end method
