.class public final Lcom/google/android/gms/drive/ui/picker/a/u;
.super Lcom/google/android/gms/drive/ui/picker/a/a/q;
.source "SourceFile"


# instance fields
.field private final b:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ui/picker/a/l;Ljava/text/Collator;[Lcom/google/android/gms/drive/ui/picker/a/w;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/q;-><init>(Lcom/google/android/gms/drive/ui/picker/a/l;[Lcom/google/android/gms/drive/ui/picker/a/a/p;Z)V

    .line 67
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/v;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/drive/ui/picker/a/v;-><init>(Lcom/google/android/gms/drive/ui/picker/a/u;Ljava/text/Collator;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/u;->b:Ljava/util/Comparator;

    .line 78
    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 23
    invoke-interface {p1}, Lcom/google/android/gms/drive/ui/picker/a/l;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "application/vnd.google-apps.folder"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/drive/ui/picker/a/l;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/util/Pair;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v2
.end method

.method protected final a()Ljava/util/Comparator;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/u;->b:Ljava/util/Comparator;

    return-object v0
.end method
