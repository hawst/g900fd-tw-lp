.class public Lcom/google/android/gms/people/settings/PeopleSettingsActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Landroid/widget/Spinner;

.field private c:Landroid/widget/CheckBox;

.field private d:Landroid/webkit/WebView;

.field private e:Landroid/widget/ArrayAdapter;

.field private final f:Ljava/lang/Runnable;

.field private final g:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 66
    new-instance v0, Lcom/google/android/gms/people/settings/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/settings/a;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->f:Ljava/lang/Runnable;

    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->g:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;Lcom/google/android/gms/common/api/Status;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to load log.  result="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 227
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<html><body><pre>"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/text/TextUtils;->htmlEncode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</pre></body></html>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->d:Landroid/webkit/WebView;

    const-string v1, ""

    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->f:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->g:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)Landroid/widget/Spinner;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->c:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->d:Landroid/webkit/WebView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->pageDown(Z)Z

    .line 237
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 265
    const-string v1, "internal_call_method"

    const-string v2, "SET_FORCE_VERBOSE_LOG"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v1, "internal_call_arg_1"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 269
    sget-object v1, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    iget-object v2, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/s;->a(Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/settings/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/people/settings/g;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 277
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 77
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 78
    sget v0, Lcom/google/android/gms/l;->cR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->setContentView(I)V

    .line 80
    const-string v0, "People debug"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 83
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 85
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 88
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 89
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->a(Z)V

    .line 92
    sget v0, Lcom/google/android/gms/j;->rK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b:Landroid/widget/Spinner;

    .line 93
    new-instance v0, Landroid/widget/ArrayAdapter;

    sget v1, Lcom/google/android/gms/l;->cQ:I

    invoke-direct {v0, p0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->e:Landroid/widget/ArrayAdapter;

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->e:Landroid/widget/ArrayAdapter;

    const-string v1, "GServices default"

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->e:Landroid/widget/ArrayAdapter;

    const-string v1, "Disable"

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->e:Landroid/widget/ArrayAdapter;

    const-string v1, "Enable"

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->b:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 103
    sget v0, Lcom/google/android/gms/j;->gn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->c:Landroid/widget/CheckBox;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 106
    sget v0, Lcom/google/android/gms/j;->sH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->d:Landroid/webkit/WebView;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->d:Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/webkit/WebSettings;->setBuiltInZoomControls(Z)V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->d:Landroid/webkit/WebView;

    new-instance v1, Lcom/google/android/gms/people/settings/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/people/settings/b;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 119
    new-instance v0, Lcom/google/android/gms/people/ad;

    invoke-direct {v0}, Lcom/google/android/gms/people/ad;-><init>()V

    const/16 v1, 0x50

    iput v1, v0, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v0}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v0

    .line 121
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p0, v3, v1}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 125
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/m;->A:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 130
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 243
    const-string v1, "internal_call_method"

    const-string v2, "SET_SHOW_SYNC_NOTIFICATION_ERROR"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v1, "internal_call_arg_1"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 247
    sget-object v1, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    iget-object v2, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/s;->a(Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/settings/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/people/settings/f;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 255
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 135
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 136
    sget v2, Lcom/google/android/gms/j;->qL:I

    if-ne v1, v2, :cond_1

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->d:Landroid/webkit/WebView;

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->pageUp(Z)Z

    .line 145
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_1
    return v0

    .line 138
    :cond_1
    sget v2, Lcom/google/android/gms/j;->qK:I

    if-ne v1, v2, :cond_2

    .line 139
    invoke-direct {p0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->e()V

    goto :goto_0

    .line 140
    :cond_2
    sget v2, Lcom/google/android/gms/j;->fS:I

    if-ne v1, v2, :cond_0

    .line 142
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.people.DUMP_DATABASE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method protected onStart()V
    .locals 3

    .prologue
    .line 150
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 152
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "internal_call_method"

    const-string v2, "LOAD_LOG"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    iget-object v2, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/s;->a(Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/settings/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/people/settings/c;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    const-string v0, "Loading log..."

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a(Ljava/lang/String;)V

    .line 153
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "internal_call_method"

    const-string v2, "GET_SHOW_SYNC_NOTIFICATION_ERROR"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    iget-object v2, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/s;->a(Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/settings/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/people/settings/d;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 154
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "internal_call_method"

    const-string v2, "GET_FORCE_VERBOSE_LOG"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    iget-object v2, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/s;->a(Lcom/google/android/gms/common/api/v;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/people/settings/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/people/settings/e;-><init>(Lcom/google/android/gms/people/settings/PeopleSettingsActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 155
    return-void
.end method

.method protected onStop()V
    .locals 2

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->g:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/people/settings/PeopleSettingsActivity;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 161
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 162
    return-void
.end method
