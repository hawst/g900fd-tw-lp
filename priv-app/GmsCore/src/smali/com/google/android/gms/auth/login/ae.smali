.class final Lcom/google/android/gms/auth/login/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/gms/auth/login/ae;->a:Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 56
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 58
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.app.action.CONFIRM_DEVICE_CREDENTIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 61
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/auth/login/ae;->a:Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 66
    :goto_0
    return-void

    .line 63
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Cannot find the Activity for the challenge."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ae;->a:Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/ConfirmAccountDeletionActivity;->finish()V

    goto :goto_0
.end method
