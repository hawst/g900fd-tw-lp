.class final Lcom/google/android/gms/fitness/sensors/a/x;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/k/k/a/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/data/DataType;

.field final synthetic b:Lcom/google/android/gms/fitness/sensors/a/s;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/a/s;Lcom/google/android/gms/fitness/data/DataType;)V
    .locals 0

    .prologue
    .line 301
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/x;->b:Lcom/google/android/gms/fitness/sensors/a/s;

    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/a/x;->a:Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 301
    check-cast p1, Lcom/google/k/c/dn;

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/x;->a:Lcom/google/android/gms/fitness/data/DataType;

    invoke-interface {p1, v0}, Lcom/google/k/c/dn;->c(Ljava/lang/Object;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/a/x;->b:Lcom/google/android/gms/fitness/sensors/a/s;

    invoke-static {v2}, Lcom/google/android/gms/fitness/sensors/a/s;->g(Lcom/google/android/gms/fitness/sensors/a/s;)Lcom/google/android/gms/fitness/sensors/a/e;

    move-result-object v2

    const-string v3, "Stop Listening to: %s"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v3

    and-int/lit8 v3, v3, 0x10

    if-lez v3, :cond_0

    iget-object v3, v2, Lcom/google/android/gms/fitness/sensors/a/e;->b:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    new-instance v3, Lcom/google/android/gms/fitness/sensors/a/r;

    invoke-direct {v3, v2, v0, v5}, Lcom/google/android/gms/fitness/sensors/a/r;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;Landroid/bluetooth/BluetoothGattCharacteristic;B)V

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/sensors/a/e;->a(Lcom/google/android/gms/fitness/sensors/a/o;)V

    goto :goto_0

    :cond_0
    const-string v2, "Characteristic %s is not a notify property"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v0

    aput-object v0, v3, v5

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 315
    const-string v0, "Failed to resolve gatt characteristics"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 316
    return-void
.end method
