.class public interface abstract Lcom/google/android/gms/plus/internal/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Landroid/net/Uri;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Comment;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ZLjava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/f;ZZ)V
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/f;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
.end method

.method public abstract c(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/acls/AclsRequest;)V
.end method

.method public abstract c(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
.end method

.method public abstract c(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
.end method

.method public abstract d(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
.end method

.method public abstract e(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
.end method

.method public abstract f(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
.end method
