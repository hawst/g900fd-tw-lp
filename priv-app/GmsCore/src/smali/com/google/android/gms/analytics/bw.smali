.class public final Lcom/google/android/gms/analytics/bw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/analytics/ay;


# instance fields
.field public a:Z

.field public b:J

.field c:Z

.field public final synthetic d:Lcom/google/android/gms/analytics/bv;

.field private e:I

.field private f:J

.field private g:Lcom/google/android/gms/common/util/p;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/analytics/bv;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 695
    iput-object p1, p0, Lcom/google/android/gms/analytics/bw;->d:Lcom/google/android/gms/analytics/bv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 677
    iput-boolean v2, p0, Lcom/google/android/gms/analytics/bw;->a:Z

    .line 682
    iput v2, p0, Lcom/google/android/gms/analytics/bw;->e:I

    .line 686
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/analytics/bw;->b:J

    .line 689
    iput-boolean v2, p0, Lcom/google/android/gms/analytics/bw;->c:Z

    .line 696
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/analytics/bw;->g:Lcom/google/android/gms/common/util/p;

    .line 697
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 773
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/analytics/at;->aj:Lcom/google/android/gms/analytics/at;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/as;->a(Lcom/google/android/gms/analytics/at;)V

    .line 774
    iget v0, p0, Lcom/google/android/gms/analytics/bw;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/analytics/bw;->e:I

    .line 777
    const/4 v0, 0x0

    iget v1, p0, Lcom/google/android/gms/analytics/bw;->e:I

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/analytics/bw;->e:I

    .line 779
    iget v0, p0, Lcom/google/android/gms/analytics/bw;->e:I

    if-nez v0, :cond_0

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/analytics/bw;->g:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/analytics/bw;->f:J

    .line 782
    :cond_0
    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 745
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/analytics/at;->ai:Lcom/google/android/gms/analytics/at;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/analytics/as;->a(Lcom/google/android/gms/analytics/at;)V

    .line 747
    iget v0, p0, Lcom/google/android/gms/analytics/bw;->e:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/analytics/bw;->g:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gms/analytics/bw;->f:J

    const-wide/16 v8, 0x3e8

    iget-wide v10, p0, Lcom/google/android/gms/analytics/bw;->b:J

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v0, v4, v6

    if-ltz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    .line 748
    iput-boolean v1, p0, Lcom/google/android/gms/analytics/bw;->c:Z

    .line 751
    :cond_0
    iget v0, p0, Lcom/google/android/gms/analytics/bw;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/analytics/bw;->e:I

    .line 752
    iget-boolean v0, p0, Lcom/google/android/gms/analytics/bw;->a:Z

    if-eqz v0, :cond_2

    .line 753
    invoke-virtual {p1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 754
    if-eqz v0, :cond_1

    .line 755
    iget-object v3, p0, Lcom/google/android/gms/analytics/bw;->d:Lcom/google/android/gms/analytics/bv;

    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->isOpaque()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 758
    :cond_1
    :goto_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 759
    const-string v0, "&t"

    const-string v4, "screenview"

    invoke-interface {v3, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 761
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/as;->a(Z)V

    .line 762
    iget-object v4, p0, Lcom/google/android/gms/analytics/bw;->d:Lcom/google/android/gms/analytics/bv;

    const-string v5, "&cd"

    iget-object v0, p0, Lcom/google/android/gms/analytics/bw;->d:Lcom/google/android/gms/analytics/bv;

    iget-object v0, v0, Lcom/google/android/gms/analytics/bv;->e:Lcom/google/android/gms/analytics/bx;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/analytics/bw;->d:Lcom/google/android/gms/analytics/bv;

    iget-object v0, v0, Lcom/google/android/gms/analytics/bv;->e:Lcom/google/android/gms/analytics/bx;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/analytics/bx;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_c

    :goto_2
    invoke-virtual {v4, v5, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    iget-object v0, p0, Lcom/google/android/gms/analytics/bw;->d:Lcom/google/android/gms/analytics/bv;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 767
    invoke-static {}, Lcom/google/android/gms/analytics/as;->a()Lcom/google/android/gms/analytics/as;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/as;->a(Z)V

    .line 769
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 747
    goto :goto_0

    .line 755
    :cond_4
    const-string v4, "referrer"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "http://hostname/?"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    const-string v4, "utm_id"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-object v5, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v6, "&ci"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    const-string v4, "utm_campaign"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_6

    iget-object v5, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v6, "&cn"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    const-string v4, "utm_content"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_7

    iget-object v5, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v6, "&cc"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    const-string v4, "utm_medium"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_8

    iget-object v5, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v6, "&cm"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    const-string v4, "utm_source"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_9

    iget-object v5, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v6, "&cs"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    const-string v4, "utm_term"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_a

    iget-object v5, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v6, "&ck"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_a
    const-string v4, "dclid"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_b

    iget-object v5, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v6, "&dclid"

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_b
    const-string v4, "gclid"

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v3, v3, Lcom/google/android/gms/analytics/bv;->b:Ljava/util/Map;

    const-string v4, "&gclid"

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    :cond_c
    move-object v0, v1

    .line 762
    goto/16 :goto_2

    :cond_d
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2
.end method
