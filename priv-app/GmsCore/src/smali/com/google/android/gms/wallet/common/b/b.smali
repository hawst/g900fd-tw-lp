.class final Lcom/google/android/gms/wallet/common/b/b;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field final f:Ljava/util/HashMap;

.field final g:[B

.field final h:Lcom/android/volley/toolbox/aa;

.field final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;[BLjava/util/ArrayList;Ljava/lang/String;Lcom/android/volley/toolbox/aa;)V
    .locals 4

    .prologue
    .line 369
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p5}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 370
    new-instance v1, Ljava/util/HashMap;

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/b/b;->f:Ljava/util/HashMap;

    .line 371
    if-eqz p3, :cond_1

    .line 372
    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 373
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/b/b;->f:Ljava/util/HashMap;

    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 370
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 376
    :cond_1
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/b/b;->g:[B

    .line 377
    iput-object p5, p0, Lcom/google/android/gms/wallet/common/b/b;->h:Lcom/android/volley/toolbox/aa;

    .line 378
    iput-object p4, p0, Lcom/google/android/gms/wallet/common/b/b;->i:Ljava/lang/String;

    .line 379
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 1

    .prologue
    .line 383
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/android/volley/m;->c:Ljava/util/Map;

    if-eqz v2, :cond_1

    iget-object v0, v0, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v2, "Content-Type"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v2, "application/x-protobuf"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b/b;->h:Lcom/android/volley/toolbox/aa;

    iget-object v1, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    invoke-virtual {v0, v1}, Lcom/android/volley/toolbox/aa;->a(Ljava/lang/Object;)V

    .line 398
    :goto_1
    return-void

    .line 396
    :cond_0
    invoke-super {p0, p1}, Lcom/android/volley/p;->b(Lcom/android/volley/ac;)V

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 358
    check-cast p1, Lcom/android/volley/m;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b/b;->h:Lcom/android/volley/toolbox/aa;

    invoke-virtual {v0, p1}, Lcom/android/volley/toolbox/aa;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b/b;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 417
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b/b;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final m()[B
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/b/b;->g:[B

    return-object v0
.end method

.method public final o()Lcom/android/volley/r;
    .locals 1

    .prologue
    .line 402
    sget-object v0, Lcom/android/volley/r;->d:Lcom/android/volley/r;

    return-object v0
.end method

.method public final q()Lcom/android/volley/z;
    .locals 4

    .prologue
    .line 422
    new-instance v0, Lcom/android/volley/f;

    const/16 v1, 0x9c4

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2, v3}, Lcom/android/volley/f;-><init>(IIF)V

    return-object v0
.end method
