.class public final Lcom/google/android/gms/wearable/node/be;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/Thread;

.field private final b:J

.field private final c:J

.field private d:J

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private final i:Ljava/util/concurrent/atomic/AtomicLong;

.field private final j:Ljava/util/concurrent/atomic/AtomicLong;

.field private final k:Landroid/os/Handler;

.field private l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 637
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 632
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/be;->i:Ljava/util/concurrent/atomic/AtomicLong;

    .line 633
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/be;->j:Ljava/util/concurrent/atomic/AtomicLong;

    .line 638
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->c:J

    .line 639
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->b:J

    .line 640
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/be;->k:Landroid/os/Handler;

    .line 641
    return-void
.end method


# virtual methods
.method final declared-synchronized a()V
    .locals 4

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 653
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 655
    :cond_0
    monitor-exit p0

    return-void

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(I)V
    .locals 4

    .prologue
    .line 664
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 665
    iget v0, p0, Lcom/google/android/gms/wearable/node/be;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wearable/node/be;->g:I

    .line 666
    iget v0, p0, Lcom/google/android/gms/wearable/node/be;->e:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/wearable/node/be;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 668
    :cond_0
    monitor-exit p0

    return-void

    .line 664
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/common/util/ad;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const-wide/16 v10, 0x3e8

    .line 678
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->d:J

    cmp-long v0, v0, v12

    if-nez v0, :cond_2

    .line 679
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/be;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    .line 680
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/be;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 682
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 684
    cmp-long v6, v0, v12

    if-eqz v6, :cond_0

    .line 685
    sub-long v0, v4, v0

    .line 686
    cmp-long v6, v0, v10

    if-lez v6, :cond_0

    .line 687
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "STUCK WHILE PROCESSING READ "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v8, 0x3e8

    div-long/2addr v0, v8

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 691
    :cond_0
    cmp-long v0, v2, v12

    if-eqz v0, :cond_1

    .line 692
    sub-long v0, v4, v2

    .line 693
    cmp-long v2, v0, v10

    if-lez v2, :cond_1

    .line 694
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "STUCK WHILE WRITING "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 699
    :cond_1
    const-string v0, "Current: "

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V

    .line 703
    :goto_0
    const-string v0, ""

    .line 704
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/be;->l:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 705
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/be;->l:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    .line 707
    :goto_1
    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->d:J

    cmp-long v0, v0, v12

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->d:J

    .line 710
    :goto_2
    const-string v3, "%s, writes/reads (%d/%d), bytes (%d/%d), duration %s%s"

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, p0, Lcom/google/android/gms/wearable/node/be;->b:J

    new-instance v8, Landroid/text/format/Time;

    invoke-direct {v8}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {v8, v6, v7}, Landroid/text/format/Time;->set(J)V

    const-string v6, "%Y-%m-%d %H:%M:%S"

    invoke-virtual {v8, v6}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v6, p0, Lcom/google/android/gms/wearable/node/be;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    iget v6, p0, Lcom/google/android/gms/wearable/node/be;->h:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    iget v6, p0, Lcom/google/android/gms/wearable/node/be;->e:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x4

    iget v6, p0, Lcom/google/android/gms/wearable/node/be;->f:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x5

    iget-wide v6, p0, Lcom/google/android/gms/wearable/node/be;->c:J

    sub-long/2addr v0, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    invoke-static {v0, v1}, Landroid/text/format/DateUtils;->formatElapsedTime(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x6

    aput-object v2, v4, v0

    invoke-virtual {p1, v3, v4}, Lcom/google/android/gms/common/util/ad;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 716
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->println()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 717
    monitor-exit p0

    return-void

    .line 701
    :cond_2
    :try_start_1
    const-string v0, "    Old: "

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->print(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 678
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 707
    :cond_3
    :try_start_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    goto :goto_2

    :cond_4
    move-object v2, v0

    goto :goto_1
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 658
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/be;->l:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 659
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/be;->l:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 661
    :cond_0
    monitor-exit p0

    return-void

    .line 658
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 734
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 735
    if-eqz p1, :cond_1

    .line 736
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/be;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 738
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/be;->k:Landroid/os/Handler;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 739
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/be;->k:Landroid/os/Handler;

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-virtual {v0, v1, p0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    .line 741
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/be;->k:Landroid/os/Handler;

    sget-object v0, Lcom/google/android/gms/wearable/a/b;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 751
    :cond_0
    :goto_0
    return-void

    .line 743
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/be;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v2

    .line 744
    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 745
    sub-long/2addr v0, v2

    .line 746
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 747
    const-string v2, "wearable"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "took "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms to perform write"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method final declared-synchronized b(I)V
    .locals 4

    .prologue
    .line 671
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/be;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 672
    iget v0, p0, Lcom/google/android/gms/wearable/node/be;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wearable/node/be;->h:I

    .line 673
    iget v0, p0, Lcom/google/android/gms/wearable/node/be;->f:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/wearable/node/be;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 675
    :cond_0
    monitor-exit p0

    return-void

    .line 671
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 786
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 787
    if-eqz p1, :cond_1

    .line 788
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/be;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    .line 798
    :cond_0
    :goto_0
    return-void

    .line 790
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/be;->i:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->getAndSet(J)J

    move-result-wide v2

    .line 791
    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    .line 792
    sub-long/2addr v0, v2

    .line 793
    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 794
    const-string v2, "wearable"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "took "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms to process the read"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final declared-synchronized b()Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v1, 0x0

    .line 755
    monitor-enter p0

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/be;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v2, v6

    if-eqz v0, :cond_0

    move v0, v1

    .line 774
    :goto_0
    monitor-exit p0

    return v0

    .line 758
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/be;->j:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v2

    .line 759
    const-string v0, "wearable"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 760
    const-string v0, "wearable"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Write start time:  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    :cond_1
    cmp-long v0, v2, v6

    if-lez v0, :cond_3

    .line 764
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 765
    const-string v0, "wearable"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 766
    const-string v0, "wearable"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Last write duration:  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 768
    :cond_2
    sget-object v0, Lcom/google/android/gms/wearable/a/b;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-lez v0, :cond_3

    .line 769
    const-string v0, "wearable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Write has been stuck for more than "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/wearable/a/b;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 771
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 774
    goto :goto_0

    .line 755
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
