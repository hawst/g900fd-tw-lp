.class public final Lcom/google/android/gms/ads/internal/util/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/util/a/a;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:I

.field private final c:Ljava/util/concurrent/BlockingQueue;

.field private d:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->a:Ljava/lang/Object;

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    .line 21
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->c:Ljava/util/concurrent/BlockingQueue;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 54
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/a/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 55
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 58
    :cond_0
    const/4 v0, -0x1

    :try_start_1
    iput v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/util/a/g;

    .line 60
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/util/a/g;->b:Lcom/google/android/gms/ads/internal/util/a/b;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/util/a/b;->a()V

    goto :goto_0

    .line 62
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V
    .locals 3

    .prologue
    .line 27
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/a/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 28
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 29
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->d:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/gms/ads/internal/util/a/e;->a(Ljava/lang/Object;)V

    .line 35
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 30
    :cond_1
    iget v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    const/4 v2, -0x1

    if-ne v0, v2, :cond_2

    .line 31
    invoke-interface {p2}, Lcom/google/android/gms/ads/internal/util/a/b;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 32
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    if-nez v0, :cond_0

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->c:Ljava/util/concurrent/BlockingQueue;

    new-instance v2, Lcom/google/android/gms/ads/internal/util/a/g;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/ads/internal/util/a/g;-><init>(Lcom/google/android/gms/ads/internal/util/a/f;Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V

    invoke-interface {v0, v2}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 40
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/util/a/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 41
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 44
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/util/a/f;->d:Ljava/lang/Object;

    .line 45
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->c:Ljava/util/concurrent/BlockingQueue;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/util/a/g;

    .line 47
    iget-object v0, v0, Lcom/google/android/gms/ads/internal/util/a/g;->a:Lcom/google/android/gms/ads/internal/util/a/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/ads/internal/util/a/e;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 49
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/google/android/gms/ads/internal/util/a/f;->b:I

    return v0
.end method
