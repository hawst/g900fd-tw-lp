.class public final Lcom/google/android/gms/feedback/at;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:[Ljava/lang/String;

.field static final b:[Ljava/lang/String;

.field private static final c:Ljava/lang/Runnable;

.field private static d:Landroid/os/Handler;

.field private static e:Lcom/google/android/gms/feedback/aw;

.field private static f:Lcom/google/android/gms/feedback/ax;

.field private static volatile g:Ljava/lang/Process;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 43
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "logcat"

    aput-object v1, v0, v2

    const-string v1, "-d"

    aput-object v1, v0, v3

    const-string v1, "-v"

    aput-object v1, v0, v4

    const-string v1, "time"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/feedback/at;->a:[Ljava/lang/String;

    .line 46
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "logcat"

    aput-object v1, v0, v2

    const-string v1, "-d"

    aput-object v1, v0, v3

    const-string v1, "-b"

    aput-object v1, v0, v4

    const-string v1, "events"

    aput-object v1, v0, v5

    const-string v1, "-v"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "time"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/feedback/at;->b:[Ljava/lang/String;

    .line 62
    new-instance v0, Lcom/google/android/gms/feedback/au;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/au;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/at;->c:Ljava/lang/Runnable;

    return-void
.end method

.method public static declared-synchronized a()V
    .locals 6

    .prologue
    .line 95
    const-class v1, Lcom/google/android/gms/feedback/at;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/feedback/at;->d:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/gms/feedback/at;->d:Landroid/os/Handler;

    .line 99
    :cond_0
    sget-object v0, Lcom/google/android/gms/feedback/at;->e:Lcom/google/android/gms/feedback/aw;

    if-nez v0, :cond_1

    .line 100
    new-instance v0, Lcom/google/android/gms/feedback/aw;

    const/4 v2, 0x0

    invoke-direct {v0, v2}, Lcom/google/android/gms/feedback/aw;-><init>(B)V

    .line 101
    sput-object v0, Lcom/google/android/gms/feedback/at;->e:Lcom/google/android/gms/feedback/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/aw;->start()V

    .line 102
    sget-object v0, Lcom/google/android/gms/feedback/at;->d:Landroid/os/Handler;

    sget-object v2, Lcom/google/android/gms/feedback/at;->c:Ljava/lang/Runnable;

    const-wide/16 v4, 0x3a98

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    :goto_0
    monitor-exit v1

    return-void

    .line 105
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/feedback/at;->e:Lcom/google/android/gms/feedback/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/aw;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/android/gms/feedback/ax;)V
    .locals 0

    .prologue
    .line 78
    sput-object p0, Lcom/google/android/gms/feedback/at;->f:Lcom/google/android/gms/feedback/ax;

    .line 79
    return-void
.end method

.method static synthetic a([Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/feedback/at;->d:Landroid/os/Handler;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/feedback/at;->d:Landroid/os/Handler;

    sget-object v1, Lcom/google/android/gms/feedback/at;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    sget-object v0, Lcom/google/android/gms/feedback/at;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/feedback/av;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/feedback/av;-><init>([Ljava/lang/String;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method static a([Ljava/lang/String;)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 143
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    .line 144
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 147
    :try_start_0
    invoke-virtual {v0, p0}, Ljava/lang/Runtime;->exec([Ljava/lang/String;)Ljava/lang/Process;

    move-result-object v0

    .line 148
    sput-object v0, Lcom/google/android/gms/feedback/at;->g:Ljava/lang/Process;

    .line 149
    invoke-virtual {v0}, Ljava/lang/Process;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 150
    :try_start_1
    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 151
    new-instance v4, Ljava/io/BufferedReader;

    invoke-direct {v4, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 153
    :goto_0
    invoke-virtual {v4}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    .line 154
    if-eqz v0, :cond_1

    .line 155
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 166
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v1, :cond_0

    .line 167
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 169
    :cond_0
    sput-object v2, Lcom/google/android/gms/feedback/at;->g:Ljava/lang/Process;

    throw v0

    .line 159
    :cond_1
    :try_start_2
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    sget-object v0, Lcom/google/android/gms/feedback/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v4, v0, :cond_3

    .line 160
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    sget-object v0, Lcom/google/android/gms/feedback/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v4, v0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v3, v0, v4}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 164
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 166
    if-eqz v1, :cond_2

    .line 167
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    .line 169
    :cond_2
    sput-object v2, Lcom/google/android/gms/feedback/at;->g:Ljava/lang/Process;

    return-object v0

    .line 166
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_1

    :cond_3
    move-object v0, v3

    goto :goto_2
.end method

.method public static b()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/feedback/at;->e:Lcom/google/android/gms/feedback/aw;

    .line 111
    return-void
.end method

.method static synthetic c()V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/feedback/at;->g:Ljava/lang/Process;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Process;->destroy()V

    :cond_0
    return-void
.end method

.method static synthetic d()Lcom/google/android/gms/feedback/ax;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/feedback/at;->f:Lcom/google/android/gms/feedback/ax;

    return-object v0
.end method
