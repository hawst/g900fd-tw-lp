.class final Lcom/google/android/gms/update/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/gms/update/y;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 128
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "Connected to UpdateFromSdCardService."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    monitor-enter p0

    .line 130
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/update/y;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    check-cast p2, Lcom/google/android/gms/update/ag;

    invoke-virtual {p2}, Lcom/google/android/gms/update/ag;->a()Lcom/google/android/gms/update/UpdateFromSdCardService;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Lcom/google/android/gms/update/UpdateFromSdCardService;)Lcom/google/android/gms/update/UpdateFromSdCardService;

    .line 131
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 132
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/update/y;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->b(Lcom/google/android/gms/update/UpdateFromSdCardActivity;)V

    .line 134
    return-void

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 138
    const-string v0, "UpdateFromSdCardActivity"

    const-string v1, "Unexpectedly disconnected from UpdateFromSdCardService."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    monitor-enter p0

    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/update/y;->a:Lcom/google/android/gms/update/UpdateFromSdCardActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardActivity;->a(Lcom/google/android/gms/update/UpdateFromSdCardActivity;Lcom/google/android/gms/update/UpdateFromSdCardService;)Lcom/google/android/gms/update/UpdateFromSdCardService;

    .line 141
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
