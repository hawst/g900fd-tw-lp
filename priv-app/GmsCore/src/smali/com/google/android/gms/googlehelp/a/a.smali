.class public final Lcom/google/android/gms/googlehelp/a/a;
.super Lcom/google/android/gms/googlehelp/a/i;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/Long;

.field private final j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/Long;ZLcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 7

    .prologue
    .line 56
    invoke-virtual {p2}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p6

    move-object v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/a/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Landroid/accounts/Account;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 57
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/a/a;->i:Ljava/lang/Long;

    .line 58
    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/a/a;->j:Z

    .line 59
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/gms/googlehelp/a/c;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/googlehelp/a/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/a/c;->a([Ljava/lang/Object;)V

    .line 136
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;JLcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 8

    .prologue
    .line 102
    new-instance v1, Lcom/google/android/gms/googlehelp/a/b;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/googlehelp/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;JLcom/android/volley/x;Lcom/android/volley/w;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/a/b;->a([Ljava/lang/Object;)V

    .line 112
    return-void
.end method

.method static synthetic t()Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedAuthority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->encodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "hl"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 3

    .prologue
    .line 74
    iget v0, p1, Lcom/android/volley/m;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    .line 76
    :try_start_0
    iget-object v0, p1, Lcom/android/volley/m;->b:[B

    new-instance v1, Lcom/google/ad/a/a/b;

    invoke-direct {v1}, Lcom/google/ad/a/a/b;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/s;->a([BLcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ad/a/a/b;

    .line 78
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 84
    :goto_0
    return-object v0

    .line 79
    :catch_0
    move-exception v0

    .line 80
    const-string v1, "GOOGLEHELP_ChatRequest"

    const-string v2, "Parsing ChatRequestStatus failed!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 84
    :cond_0
    new-instance v0, Lcom/android/volley/ac;

    invoke-direct {v0, p1}, Lcom/android/volley/ac;-><init>(Lcom/android/volley/m;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/googlehelp/a/l;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/a;->f:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->j:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/a;->f:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->Q()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/a;->i:Ljava/lang/Long;

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->l:Ljava/lang/Long;

    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/a/a;->j:Z

    iput-boolean v0, p1, Lcom/google/android/gms/googlehelp/a/l;->m:Z

    .line 67
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/a;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/gcm/ab;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/a/l;->e:Ljava/lang/String;

    .line 70
    :cond_0
    return-void
.end method
