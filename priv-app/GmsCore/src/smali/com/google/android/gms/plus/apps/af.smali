.class public final Lcom/google/android/gms/plus/apps/af;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/plus/apps/af;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method public static a()Lcom/google/android/gms/plus/apps/af;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lcom/google/android/gms/plus/apps/af;->a:Lcom/google/android/gms/plus/apps/af;

    if-nez v0, :cond_0

    .line 31
    new-instance v0, Lcom/google/android/gms/plus/apps/af;

    invoke-direct {v0}, Lcom/google/android/gms/plus/apps/af;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/apps/af;->a:Lcom/google/android/gms/plus/apps/af;

    .line 33
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/af;->a:Lcom/google/android/gms/plus/apps/af;

    return-object v0
.end method

.method public static a(Landroid/content/Context;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 42
    packed-switch p1, :pswitch_data_0

    .line 53
    const/4 v0, 0x0

    .line 56
    :goto_0
    if-nez v0, :cond_0

    move v0, v1

    .line 68
    :goto_1
    return v0

    .line 44
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/plus/c/a;->L:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 47
    :pswitch_1
    sget-object v0, Lcom/google/android/gms/plus/c/a;->N:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 50
    :pswitch_2
    sget-object v0, Lcom/google/android/gms/plus/c/a;->P:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    .line 60
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    .line 61
    const-string v3, ";"

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 62
    array-length v4, v3

    move v0, v1

    .line 63
    :goto_2
    if-ge v0, v4, :cond_2

    .line 64
    aget-object v5, v3, v0

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 65
    const/4 v0, 0x1

    goto :goto_1

    .line 63
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 68
    goto :goto_1

    .line 42
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;I)Ljava/lang/CharSequence;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 74
    packed-switch p1, :pswitch_data_0

    move-object v1, v0

    .line 91
    :goto_0
    if-eqz v1, :cond_0

    .line 92
    invoke-static {v0, v1}, Lcom/google/android/gms/plus/apps/an;->a(Ljava/lang/CharSequence;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 94
    :cond_0
    return-object v0

    .line 76
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->tL:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 77
    sget-object v0, Lcom/google/android/gms/plus/c/a;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    move-object v0, v1

    move-object v1, v2

    .line 78
    goto :goto_0

    .line 80
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->tS:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 81
    sget-object v0, Lcom/google/android/gms/plus/c/a;->O:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    move-object v0, v1

    move-object v1, v2

    .line 82
    goto :goto_0

    .line 84
    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->ua:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 85
    sget-object v0, Lcom/google/android/gms/plus/c/a;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v2, v0

    move-object v0, v1

    move-object v1, v2

    .line 86
    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
