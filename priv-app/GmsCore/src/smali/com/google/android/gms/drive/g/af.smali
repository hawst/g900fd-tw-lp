.class public abstract enum Lcom/google/android/gms/drive/g/af;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/g/af;

.field public static final enum b:Lcom/google/android/gms/drive/g/af;

.field public static final enum c:Lcom/google/android/gms/drive/g/af;

.field private static final synthetic e:[Lcom/google/android/gms/drive/g/af;


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/drive/g/ag;

    const-string v1, "DISABLE"

    const-string v2, "disable"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/g/ag;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/g/af;->a:Lcom/google/android/gms/drive/g/af;

    .line 30
    new-instance v0, Lcom/google/android/gms/drive/g/ah;

    const-string v1, "WIFI"

    const-string v2, "wifi"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/g/ah;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/g/af;->b:Lcom/google/android/gms/drive/g/af;

    .line 38
    new-instance v0, Lcom/google/android/gms/drive/g/ai;

    const-string v1, "ALWAYS"

    const-string v2, "always"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/g/ai;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/g/af;->c:Lcom/google/android/gms/drive/g/af;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/drive/g/af;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/g/af;->a:Lcom/google/android/gms/drive/g/af;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/drive/g/af;->b:Lcom/google/android/gms/drive/g/af;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/gms/drive/g/af;->c:Lcom/google/android/gms/drive/g/af;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/g/af;->e:[Lcom/google/android/gms/drive/g/af;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 48
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/af;->d:Ljava/lang/String;

    .line 49
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;B)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/g/af;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/g/af;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/google/android/gms/drive/g/af;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/af;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/g/af;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/drive/g/af;->e:[Lcom/google/android/gms/drive/g/af;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/g/af;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/g/af;

    return-object v0
.end method


# virtual methods
.method public abstract a(Lcom/google/android/gms/drive/g/o;)Z
.end method
