.class final Lcom/google/android/gms/plus/audience/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field a:Z

.field final synthetic b:Lcom/google/android/gms/plus/audience/a/i;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/audience/a/i;)V
    .locals 0

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/audience/a/i;B)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/audience/a/j;-><init>(Lcom/google/android/gms/plus/audience/a/i;)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 84
    check-cast p1, Lcom/google/android/gms/people/u;

    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/a/j;->a:Z

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/people/u;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/plus/audience/a/i;)Lcom/google/android/gms/common/data/c;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    new-instance v1, Lcom/google/android/gms/common/data/c;

    invoke-interface {p1}, Lcom/google/android/gms/people/u;->c()Lcom/google/android/gms/people/model/k;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/data/c;-><init>(Lcom/google/android/gms/common/data/d;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/plus/audience/a/i;Lcom/google/android/gms/common/data/c;)Lcom/google/android/gms/common/data/c;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    invoke-interface {p1}, Lcom/google/android/gms/people/u;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/plus/audience/a/i;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/a/i;->b(Lcom/google/android/gms/plus/audience/a/i;)Lcom/google/android/gms/plus/audience/a/j;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    invoke-interface {p1}, Lcom/google/android/gms/people/u;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    invoke-static {v2}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/plus/audience/a/i;)Lcom/google/android/gms/common/data/c;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/plus/audience/a/i;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a/j;->b:Lcom/google/android/gms/plus/audience/a/i;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/plus/audience/a/i;)Lcom/google/android/gms/common/data/c;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/people/u;->c()Lcom/google/android/gms/people/model/k;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/data/c;->a(Lcom/google/android/gms/common/data/c;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/a/i;->a(Lcom/google/android/gms/plus/audience/a/i;Lcom/google/android/gms/common/data/c;)Lcom/google/android/gms/common/data/c;

    goto :goto_0
.end method
