.class public Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;
.super Lcom/google/android/gms/googlehelp/service/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/service/a;-><init>()V

    return-void
.end method

.method public static a(JILjava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 61
    const-string v0, "com.google.android.gms.googlehelp.service.VideoCallStatusUpdateService.START"

    invoke-static {v0, p0, p1, p2, p3}, Lcom/google/android/gms/googlehelp/service/a;->a(Ljava/lang/String;JILjava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 85
    invoke-static {p0, p1}, Lcom/google/android/gms/googlehelp/service/a;->b(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, p2}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_IS_VIDEO"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/net/Uri;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 92
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/common/u;->d(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 94
    :cond_0
    const-string v0, "GOOGLEHELP_VideoCallStatusUpdateService"

    const-string v1, "Tried to parse video call URL, but was null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "Tried to parse video call URL, but was null"

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0

    .line 100
    :cond_1
    :try_start_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 107
    return-object v0

    .line 104
    :catch_0
    move-exception v0

    const-string v0, "GOOGLEHELP_VideoCallStatusUpdateService"

    const-string v1, "Tried to parse video call URL, but was null"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v0, Ljava/text/ParseException;

    const-string v1, "Tried to parse video call URL, but was null"

    invoke-direct {v0, v1, v2}, Ljava/text/ParseException;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 46
    const-string v0, "com.google.android.gms.googlehelp.service.VideoCallStatusUpdateService.START"

    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 1

    .prologue
    .line 37
    const-string v0, "com.google.android.gms.googlehelp.service.VideoCallStatusUpdateService.START"

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 55
    const-string v0, "com.google.android.gms.googlehelp.service.VideoCallStatusUpdateService.START"

    invoke-static {p0, v0}, Lcom/google/android/gms/googlehelp/service/a;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public static f()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 70
    const-string v0, "com.google.android.gms.googlehelp.service.VideoCallStatusUpdateService.START"

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/service/a;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(JILjava/lang/String;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 200
    :cond_0
    new-instance v0, Lcom/google/ad/a/a/z;

    invoke-direct {v0}, Lcom/google/ad/a/a/z;-><init>()V

    .line 201
    iput-wide p1, v0, Lcom/google/ad/a/a/z;->c:J

    .line 202
    iput p3, v0, Lcom/google/ad/a/a/z;->a:I

    .line 203
    iput-object p4, v0, Lcom/google/ad/a/a/z;->b:Ljava/lang/String;

    .line 204
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p5, v1, v0}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;Lcom/google/ad/a/a/z;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    .line 161
    :goto_0
    new-instance v0, Lcom/google/android/gms/googlehelp/c/a/q;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-direct {v0, p0, p1, p0, v1}, Lcom/google/android/gms/googlehelp/c/a/q;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/c/a/p;Lcom/google/android/gms/googlehelp/common/a;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    .line 163
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->e:Lcom/google/android/gms/googlehelp/c/a/n;

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/a/r;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;JLcom/android/volley/x;Lcom/android/volley/w;)V

    .line 165
    return-void

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v2

    goto :goto_0
.end method

.method final b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->s()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I
    .locals 3

    .prologue
    const/4 v0, -0x1

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->f(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method

.method final d(Lcom/google/android/gms/googlehelp/common/HelpConfig;)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 186
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v2, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/u;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0, v1}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v0

    goto :goto_0
.end method

.method final e(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    if-nez v0, :cond_0

    .line 215
    :goto_0
    return-void

    .line 214
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1, v0}, Lcom/google/android/gms/googlehelp/common/e;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    goto :goto_0
.end method

.method final f(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 224
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HelpFragment.VIDEO_CALL_READY"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p1, v1}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string v0, "com.google.android.gms.googlehelp.service.VideoCallStatusUpdateService.START"

    return-object v0
.end method

.method final g(Lcom/google/android/gms/googlehelp/common/HelpConfig;)V
    .locals 0

    .prologue
    .line 230
    invoke-static {p0, p1}, Lcom/google/android/gms/googlehelp/a/r;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 231
    return-void
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 125
    const/16 v0, 0xed

    return v0
.end method

.method final h(Lcom/google/android/gms/googlehelp/common/HelpConfig;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->d:Lcom/google/android/gms/googlehelp/common/a;

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method final i()I
    .locals 1

    .prologue
    .line 130
    const/16 v0, 0x2d3

    return v0
.end method

.method final j()I
    .locals 1

    .prologue
    .line 135
    const/16 v0, 0x2711

    return v0
.end method

.method final k()I
    .locals 1

    .prologue
    .line 140
    sget v0, Lcom/google/android/gms/h;->aU:I

    return v0
.end method

.method final l()I
    .locals 1

    .prologue
    .line 145
    sget v0, Lcom/google/android/gms/p;->pp:I

    return v0
.end method

.method final m()I
    .locals 1

    .prologue
    .line 150
    sget v0, Lcom/google/android/gms/p;->pq:I

    return v0
.end method

.method final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 219
    const-string v0, "com.google.android.gms.googlehelp.HelpFragment.VC_STATUS_UPDATE"

    return-object v0
.end method
