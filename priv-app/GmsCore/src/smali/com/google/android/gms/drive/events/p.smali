.class final Lcom/google/android/gms/drive/events/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/gms/drive/events/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/events/o;Z)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/gms/drive/events/p;->b:Lcom/google/android/gms/drive/events/o;

    iput-boolean p2, p0, Lcom/google/android/gms/drive/events/p;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 8

    .prologue
    .line 210
    iget-boolean v0, p0, Lcom/google/android/gms/drive/events/p;->a:Z

    if-eqz v0, :cond_2

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/drive/events/p;->b:Lcom/google/android/gms/drive/events/o;

    iget-object v1, v0, Lcom/google/android/gms/drive/events/o;->d:Lcom/google/android/gms/drive/events/l;

    iget-object v0, p0, Lcom/google/android/gms/drive/events/p;->b:Lcom/google/android/gms/drive/events/o;

    iget-wide v2, v0, Lcom/google/android/gms/drive/events/o;->a:J

    iget-object v0, v1, Lcom/google/android/gms/drive/events/l;->a:Lcom/google/android/gms/drive/events/t;

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/events/t;->a(J)Lcom/google/android/gms/drive/database/model/bl;

    move-result-object v2

    if-nez v2, :cond_0

    const-string v0, "EventDistributorImpl"

    const-string v1, "Persisted event not found on snooze"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    :goto_0
    return-void

    .line 211
    :cond_0
    iget v0, v2, Lcom/google/android/gms/drive/database/model/bl;->g:I

    if-gtz v0, :cond_1

    const-string v3, "EventDistributorImpl"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Snooze on event without attempts "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :cond_1
    add-int/lit8 v0, v0, -0x1

    iput v0, v2, Lcom/google/android/gms/drive/database/model/bl;->g:I

    iget v0, v2, Lcom/google/android/gms/drive/database/model/bl;->h:I

    add-int/lit8 v3, v0, 0x1

    iput v3, v2, Lcom/google/android/gms/drive/database/model/bl;->h:I

    iget v3, v1, Lcom/google/android/gms/drive/events/l;->d:I

    int-to-double v4, v3

    int-to-double v6, v0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    iget v0, v1, Lcom/google/android/gms/drive/events/l;->c:I

    int-to-double v6, v0

    mul-double/2addr v4, v6

    double-to-long v4, v4

    iget-object v0, v1, Lcom/google/android/gms/drive/events/l;->b:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    add-long/2addr v0, v4

    iput-wide v0, v2, Lcom/google/android/gms/drive/database/model/bl;->d:J

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/bl;->i()V

    goto :goto_0

    .line 213
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/events/p;->b:Lcom/google/android/gms/drive/events/o;

    iget-object v0, v0, Lcom/google/android/gms/drive/events/o;->d:Lcom/google/android/gms/drive/events/l;

    iget-object v1, p0, Lcom/google/android/gms/drive/events/p;->b:Lcom/google/android/gms/drive/events/o;

    iget-object v1, v1, Lcom/google/android/gms/drive/events/o;->b:Lcom/google/android/gms/drive/database/model/bl;

    iget-object v2, p0, Lcom/google/android/gms/drive/events/p;->b:Lcom/google/android/gms/drive/events/o;

    iget-object v2, v2, Lcom/google/android/gms/drive/events/o;->c:Lcom/google/android/gms/drive/events/f;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/events/l;->a(Lcom/google/android/gms/drive/database/model/bl;Lcom/google/android/gms/drive/events/f;I)V

    goto :goto_0
.end method
