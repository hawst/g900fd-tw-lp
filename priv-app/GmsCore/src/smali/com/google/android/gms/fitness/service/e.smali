.class public final Lcom/google/android/gms/fitness/service/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field private final b:Ljava/util/Map;

.field private final c:Lcom/google/android/gms/fitness/h/f;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/h/f;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/e;->b:Ljava/util/Map;

    .line 62
    iput-object p2, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    .line 63
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    .line 64
    return-void
.end method

.method private c(Ljava/lang/String;)Lcom/google/android/gms/fitness/service/i;
    .locals 14

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/service/i;

    .line 88
    if-nez v0, :cond_0

    .line 89
    const-string v0, "starting fitness service for: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->b()Landroid/os/Handler;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->c()Lcom/google/k/k/a/ai;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->d()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/h/f;->a(Landroid/os/Handler;)Lcom/google/android/gms/fitness/service/aq;

    move-result-object v12

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->a()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/h/f;->e()Lcom/google/android/gms/fitness/d/h;

    move-result-object v13

    invoke-interface {v13, p1}, Lcom/google/android/gms/fitness/d/h;->d(Ljava/lang/String;)Lcom/google/android/gms/fitness/d/d;

    move-result-object v10

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/h/a;->a()Lcom/google/android/gms/fitness/l/z;

    move-result-object v5

    invoke-virtual {v1, v3}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/os/Handler;)Lcom/google/android/gms/fitness/sensors/a/a;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/fitness/h/a;->a(Ljava/util/List;Lcom/google/android/gms/fitness/sensors/a/a;)Lcom/google/android/gms/fitness/sensors/a;

    move-result-object v6

    invoke-virtual {v1, v6}, Lcom/google/android/gms/fitness/h/a;->a(Lcom/google/android/gms/fitness/sensors/a;)Lcom/google/android/gms/fitness/k/a;

    move-result-object v7

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->f()Lcom/google/android/gms/fitness/l/a/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/l/a/b;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/l/a/a;

    move-result-object v8

    invoke-virtual {v1, v6, v8}, Lcom/google/android/gms/fitness/h/a;->a(Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/l/a/a;)Lcom/google/android/gms/fitness/service/av;

    move-result-object v9

    invoke-virtual {v1, v7}, Lcom/google/android/gms/fitness/h/a;->a(Lcom/google/android/gms/fitness/k/a;)Lcom/google/android/gms/fitness/a/o;

    move-result-object v11

    new-instance v0, Lcom/google/android/gms/fitness/service/i;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    move-object v2, p1

    invoke-direct/range {v0 .. v13}, Lcom/google/android/gms/fitness/service/i;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Lcom/google/k/k/a/ai;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/k/a;Lcom/google/android/gms/fitness/l/a/a;Lcom/google/android/gms/fitness/service/av;Lcom/google/android/gms/fitness/d/d;Lcom/google/android/gms/fitness/a/o;Lcom/google/android/gms/fitness/service/aq;Lcom/google/android/gms/fitness/d/h;)V

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/service/i;->a()V

    .line 90
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/e;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/fitness/data/Device;->b(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/service/e;->b()Ljava/util/Set;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    const-string v3, "alarm"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0, v1, v5}, Lcom/google/android/gms/fitness/service/e;->a(Ljava/util/Set;Lcom/google/android/gms/wearable/s;)Landroid/content/Intent;

    move-result-object v5

    const/high16 v6, 0x10000000

    invoke-static {v3, v4, v5, v6}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v1, "WearableSyncServiceReceiver.cancel"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v2, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :goto_0
    new-instance v1, Lcom/google/android/gms/fitness/service/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/service/f;-><init>(Lcom/google/android/gms/fitness/service/e;)V

    invoke-static {v1}, Lcom/google/android/location/wearable/LocationWearableListenerService;->a(Ljava/lang/Object;)V

    .line 95
    :cond_0
    return-object v0

    .line 92
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "schedule to sync accounts: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v1, Lcom/google/android/gms/fitness/g/c;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    const/4 v3, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-virtual/range {v2 .. v8}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method final a(Ljava/util/Set;Lcom/google/android/gms/wearable/s;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 190
    new-instance v1, Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/fitness/wearables/WearableSyncServiceReceiver;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 191
    const-string v2, "accounts"

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 193
    if-eqz p2, :cond_0

    .line 194
    const-string v0, "node_id"

    invoke-interface {p2}, Lcom/google/android/gms/wearable/s;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 196
    :cond_0
    return-object v1
.end method

.method public final declared-synchronized a(Ljava/lang/String;)Lcom/google/android/gms/fitness/service/i;
    .locals 1

    .prologue
    .line 83
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/service/e;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/service/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a()V
    .locals 2

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->c:Lcom/google/android/gms/fitness/h/f;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/f;->e()Lcom/google/android/gms/fitness/d/h;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/d/h;->b()Ljava/util/Set;

    move-result-object v0

    .line 68
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 69
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/service/e;->c(Ljava/lang/String;)Lcom/google/android/gms/fitness/service/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 71
    :cond_0
    monitor-exit p0

    return-void
.end method

.method final declared-synchronized a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 77
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/service/i;

    .line 78
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/fitness/service/i;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 80
    :cond_0
    monitor-exit p0

    return-void
.end method

.method final b()Ljava/util/Set;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/e;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 154
    const-string v1, "none"

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 155
    return-object v0
.end method

.method final b(Ljava/lang/String;)Z
    .locals 14

    .prologue
    const-wide/16 v12, 0x3e8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    const-string v3, "fitness_account_service_manager"

    invoke-virtual {v0, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 205
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ".lastChecked"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 206
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ".supported"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 208
    const-wide/high16 v6, -0x8000000000000000L

    invoke-interface {v4, v3, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 209
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 210
    sub-long v6, v8, v6

    .line 212
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->aN:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v10, v0

    mul-long/2addr v10, v12

    cmp-long v0, v6, v10

    if-lez v0, :cond_1

    move v0, v1

    .line 214
    :goto_0
    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    if-eqz v0, :cond_5

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/e;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/fitness/h/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v0

    .line 217
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/h/a;->b()Lcom/google/android/gms/fitness/sync/d;

    move-result-object v0

    .line 219
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/fitness/sync/d;->c()Z

    move-result v0

    .line 220
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v10

    invoke-interface {v10, v3, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3, v5, v0}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 223
    const-string v3, "Account supported from server: %s"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v3, v8}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_0 .. :try_end_0} :catch_0

    .line 241
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 212
    goto :goto_0

    .line 225
    :catch_0
    move-exception v0

    move-object v3, v0

    .line 227
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->aM:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    mul-long/2addr v8, v12

    cmp-long v0, v6, v8

    if-lez v0, :cond_3

    move v0, v1

    .line 228
    :goto_2
    if-nez v0, :cond_2

    invoke-interface {v4, v5}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 229
    :cond_2
    throw v3

    :cond_3
    move v0, v2

    .line 227
    goto :goto_2

    .line 232
    :cond_4
    const-string v0, "Error determining account supported for %s: %s. Returning cached value."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v2

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/sync/g;->getMessage()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v1

    invoke-static {v0, v6}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 239
    :cond_5
    invoke-interface {v4, v5, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 240
    const-string v3, "Account supported from cache: %s"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
