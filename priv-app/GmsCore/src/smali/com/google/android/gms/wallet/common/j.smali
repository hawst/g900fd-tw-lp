.class final Lcom/google/android/gms/wallet/common/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/i;


# instance fields
.field private final a:Lorg/json/JSONObject;


# direct methods
.method protected constructor <init>(Lorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/j;->a:Lorg/json/JSONObject;

    .line 210
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/j;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/j;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    .line 220
    if-nez v2, :cond_1

    .line 221
    const-string v0, "DisplayHintsParser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No JSONArray found for key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const/4 v0, 0x0

    .line 232
    :cond_0
    return-object v0

    .line 224
    :cond_1
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    .line 225
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 226
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 227
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->optJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    .line 228
    if-eqz v4, :cond_2

    .line 229
    new-instance v5, Lcom/google/android/gms/wallet/common/j;

    invoke-direct {v5, v4}, Lcom/google/android/gms/wallet/common/j;-><init>(Lorg/json/JSONObject;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method
