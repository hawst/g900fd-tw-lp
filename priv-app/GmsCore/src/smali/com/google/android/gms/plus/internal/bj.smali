.class final Lcom/google/android/gms/plus/internal/bj;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/aj;)V
    .locals 0

    .prologue
    .line 747
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/bj;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 748
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/bj;->b:Lcom/google/android/gms/plus/internal/aj;

    .line 749
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 753
    .line 754
    if-eqz p2, :cond_1

    .line 755
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 757
    :goto_0
    new-instance v2, Lcom/google/android/gms/common/c;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 759
    if-eqz p3, :cond_0

    .line 760
    new-instance v1, Lcom/google/android/gms/plus/data/a/a;

    invoke-direct {v1, p3}, Lcom/google/android/gms/plus/data/a/a;-><init>(Landroid/os/Bundle;)V

    .line 762
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bj;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v3, Lcom/google/android/gms/plus/internal/bk;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/bj;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/bj;->b:Lcom/google/android/gms/plus/internal/aj;

    invoke-direct {v3, v4, v5, v2, v1}, Lcom/google/android/gms/plus/internal/bk;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/aj;Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/a;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 763
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
