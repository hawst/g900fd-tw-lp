.class final Lcom/google/android/gms/cast/c/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/cast/c/ao;

.field b:Lcom/google/android/gms/cast/c/at;

.field c:Lcom/google/android/gms/cast/c/au;

.field d:Lcom/google/android/gms/cast/c/ak;

.field e:Lcom/google/android/gms/cast/c/al;

.field f:J

.field private final g:J

.field private h:Ljava/net/SocketAddress;


# direct methods
.method public constructor <init>(Ljava/net/SocketAddress;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/c/aq;->f:J

    .line 27
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/c/aq;->g:J

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/cast/c/aq;->h:Ljava/net/SocketAddress;

    .line 29
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/cast/c/ap;)V
    .locals 4

    .prologue
    .line 242
    iget-wide v0, p0, Lcom/google/android/gms/cast/c/aq;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 243
    iget-wide v0, p1, Lcom/google/android/gms/cast/c/ap;->c:J

    iput-wide v0, p0, Lcom/google/android/gms/cast/c/aq;->f:J

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/cast/c/aq;->f:J

    iget-wide v2, p1, Lcom/google/android/gms/cast/c/ap;->c:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/c/aq;->f:J

    goto :goto_0
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JJ)Z
    .locals 5

    .prologue
    .line 237
    iget-wide v0, p0, Lcom/google/android/gms/cast/c/aq;->f:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-static {p3, p4, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 238
    iget-wide v2, p0, Lcom/google/android/gms/cast/c/aq;->g:J

    sub-long v2, p1, v2

    cmp-long v0, v2, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/at;->a()[Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
