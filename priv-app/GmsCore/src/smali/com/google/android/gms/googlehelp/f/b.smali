.class public final Lcom/google/android/gms/googlehelp/f/b;
.super Lcom/google/android/gms/googlehelp/f/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private final b:Lcom/google/android/gms/googlehelp/search/a;

.field private final c:Lcom/google/android/gms/googlehelp/d/b;

.field private final d:Lcom/google/android/gms/googlehelp/d/d;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/f/c;-><init>()V

    .line 39
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/b;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 40
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f()Lcom/google/android/gms/googlehelp/search/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/b;->b:Lcom/google/android/gms/googlehelp/search/a;

    .line 41
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d()Lcom/google/android/gms/googlehelp/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/b;->c:Lcom/google/android/gms/googlehelp/d/b;

    .line 42
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->c()Lcom/google/android/gms/googlehelp/d/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/b;->d:Lcom/google/android/gms/googlehelp/d/d;

    .line 43
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/b;->b:Lcom/google/android/gms/googlehelp/search/a;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/a;->e()I

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/b;->c:Lcom/google/android/gms/googlehelp/d/b;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/d/b;->d()I

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/b;->d:Lcom/google/android/gms/googlehelp/d/d;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/b;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/d/d;->a(Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0
.end method
