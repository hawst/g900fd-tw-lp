.class public final Lcom/google/android/gms/lockbox/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 82
    const-string v0, "gms:lockbox:location:country_code_mcc_mnc_list"

    const-string v1, "BM 350\\d+|31059 NF 50510 PS 425(05|06) GE 289\\d+ AF 412\\d+ AL 276\\d+ DZ 603\\d+ AS 544\\d+ AD 213\\d+ AO 631\\d+ AI 365\\d+ AG 344\\d+ AR 722\\d+ AM 283\\d+ AW 363\\d+ AU 505\\d+ AT 232\\d+ AZ 400\\d+ BS 364\\d+ BH 426\\d+ BD 470\\d+ BB 342\\d+ BY 257\\d+ BE 206\\d+ BZ 702\\d+ BJ 616\\d+ BT 402\\d+ BO 736\\d+ BA 218\\d+ BW 652\\d+ BR 724\\d+ VG 348\\d+ BN 528\\d+ BG 284\\d+ BF 613\\d+ BI 642\\d+ KH 456\\d+ CM 624\\d+ CA 302\\d+ CV 625\\d+ KY 346\\d+ CF 623\\d+ TD 622\\d+ CL 730\\d+ CN 460\\d+ CO 732\\d+ KM 654\\d+ CG 629\\d+ CK 548\\d+ CR 712\\d+ HR 219\\d+ CU 368\\d+ CY 280\\d+ CZ 230\\d+ CD 630\\d+ DK 238\\d+ DJ 638\\d+ DM 366\\d+ DO 370\\d+ TL 514\\d+ EC 740\\d+ EG 602\\d+ SV 706\\d+ GQ 627\\d+ ER 657\\d+ EE 248\\d+ ET 636\\d+ FO 288\\d+ FJ 542\\d+ FI 244\\d+ FR 208\\d+ PF 547\\d+ GA 628\\d+ GM 607\\d+ GE 282\\d+ DE 262\\d+ GH 620\\d+ GI 266\\d+ GR 202\\d+ GL 290\\d+ GD 352\\d+ GT 704\\d+ GN 611\\d+ GW 632\\d+ GY 738\\d+ HT 372\\d+ HN 708\\d+ HK 454\\d+ HU 216\\d+ IS 274\\d+ IN (404|405)\\d+ ID 510\\d+ IR 432\\d+ IQ 418\\d+ IE 272\\d+ IL 425\\d+ IT 222\\d+ CI 612\\d+ JM 338\\d+ JP 440\\d+ JO 416\\d+ KZ 401\\d+ KE 639\\d+ KI 545\\d+ KP 467\\d+ KR 450\\d+ KW 419\\d+ KG 437\\d+ LA 457\\d+ LV 247\\d+ LB 415\\d+ LS 651\\d+ LR 618\\d+ LY 606\\d+ LI 295\\d+ LT 246\\d+ LU 270\\d+ MO 455\\d+ MK 294\\d+ MG 646\\d+ MW 650\\d+ MY 502\\d+ MV 472\\d+ ML 610\\d+ MT 278\\d+ MH 551\\d+ MR 609\\d+ MU 617\\d+ MX 334\\d+ FM 550\\d+ MD 259\\d+ MC 212\\d+ MN 428\\d+ ME 297\\d+ MS 354\\d+ MA 604\\d+ MZ 643\\d+ MM 414\\d+ NA 649\\d+ NR 536\\d+ NP 429\\d+ NL 204\\d+ AN 362\\d+ NC 546\\d+ NZ 530\\d+ NI 710\\d+ NE 614\\d+ NG 621\\d+ NU 555\\d+ NO 242\\d+ OM 422\\d+ PK 410\\d+ PW 552\\d+ PA 714\\d+ PG 537\\d+ PY 744\\d+ PE 716\\d+ PH 515\\d+ PL 260\\d+ PT 268\\d+ PR 330\\d+ QA 427\\d+ RE 647\\d+ RO 226\\d+ RU 250\\d+ RW 635\\d+ KN 356\\d+ LC 358\\d+ PM 308\\d+ VC 360\\d+ WS 549\\d+ SM 292\\d+ ST 626\\d+ SA 420\\d+ SN 608\\d+ RS 220\\d+ SC 633\\d+ SL 619\\d+ SG 525\\d+ SK 231\\d+ SI 293\\d+ SB 540\\d+ SO 637\\d+ ZA 655\\d+ SS 659\\d+ ES 214\\d+ LK 413\\d+ SD 634\\d+ SR 746\\d+ SZ 653\\d+ SE 240\\d+ CH 228\\d+ SY 417\\d+ TW 466\\d+ TJ 436\\d+ TZ 640\\d+ TH 520\\d+ TG 615\\d+ TO 539\\d+ TT 374\\d+ TN 605\\d+ TR 286\\d+ TM 438\\d+ TC 376\\d+ TV 553\\d+ UG 641\\d+ UA 255\\d+ AE 424\\d+ GB (234|235)\\d+ US (310|311|313|316)\\d+ UY 748\\d+ UZ 434\\d+ VU 541\\d+ VA 225\\d+ VE 734\\d+ VN 452\\d+ YE 421\\d+ ZM 645\\d+ ZW 648\\d+"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/lockbox/b/b;->a:Lcom/google/android/gms/common/a/d;

    return-void
.end method
