.class final Lcom/google/android/gms/droidguard/d/ac;
.super Lcom/google/android/gms/droidguard/d/a;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/util/p;

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/common/util/r;

    invoke-direct {v0}, Lcom/google/android/gms/common/util/r;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/droidguard/d/ac;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V

    .line 64
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/google/android/gms/droidguard/d/a;-><init>()V

    .line 55
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d/ac;->c:Ljava/util/List;

    .line 58
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/ac;->a:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/ac;->b:Lcom/google/android/gms/common/util/p;

    .line 60
    return-void
.end method

.method private static a(Lcom/google/android/gms/droidguard/d/ab;Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 183
    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "lib/"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 185
    new-instance v3, Ljava/util/zip/ZipFile;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-direct {v3, v1}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V

    .line 187
    :try_start_0
    invoke-virtual {v3}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v4

    move v1, v0

    .line 188
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    invoke-interface {v4}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    .line 190
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->isDirectory()Z

    move-result v5

    if-nez v5, :cond_0

    .line 191
    invoke-virtual {v0}, Ljava/util/zip/ZipEntry;->getName()Ljava/lang/String;

    move-result-object v5

    .line 194
    invoke-virtual {v5, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 195
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    .line 198
    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    const/16 v6, 0x2f

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    const/4 v7, -0x1

    if-ne v6, v7, :cond_0

    .line 199
    invoke-virtual {v3, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v6

    .line 202
    new-instance v7, Ljava/io/File;

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/ab;->c:Ljava/io/File;

    invoke-direct {v7, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    const/4 v5, 0x0

    :try_start_2
    invoke-static {v6, v0, v5}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;Z)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    invoke-static {v6}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 203
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 204
    goto :goto_0

    .line 202
    :catchall_0
    move-exception v1

    :try_start_5
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v0

    :try_start_6
    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v0

    :try_start_7
    invoke-static {v6}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 206
    :catchall_2
    move-exception v0

    invoke-virtual {v3}, Ljava/util/zip/ZipFile;->close()V

    throw v0

    :cond_1
    invoke-virtual {v3}, Ljava/util/zip/ZipFile;->close()V

    .line 208
    return v1
.end method

.method private a()Lcom/google/android/gms/droidguard/d/ab;
    .locals 2

    .prologue
    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tmp_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/droidguard/d/ag;)Lcom/google/android/gms/droidguard/d/ab;
    .locals 2

    .prologue
    .line 67
    iget-object v0, p1, Lcom/google/android/gms/droidguard/d/ag;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/d/ab;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 70
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    .line 73
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/droidguard/d/ac;->a(Lcom/google/android/gms/droidguard/d/ab;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/droidguard/d/ag;Lcom/google/android/gms/droidguard/d/ab;)Lcom/google/android/gms/droidguard/d/ab;
    .locals 5

    .prologue
    .line 80
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/droidguard/d/ac;->a()Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ac;->c:Ljava/util/List;

    iget-object v2, v0, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/d/ab;->b()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/google/android/gms/droidguard/d/b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to make directores for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 82
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/droidguard/d/ac;->b()V

    throw v0

    .line 80
    :cond_0
    :try_start_1
    iget-object v1, p2, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    iget-object v2, v0, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-static {v1, v2}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/io/File;Ljava/io/File;)V

    sget-object v1, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    sget-object v2, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-static {v0, v1}, Lcom/google/android/gms/droidguard/d/ac;->a(Lcom/google/android/gms/droidguard/d/ab;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-lez v1, :cond_2

    :goto_0
    :try_start_3
    iget-object v1, p1, Lcom/google/android/gms/droidguard/d/ag;->a:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/droidguard/d/ac;->a()Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/droidguard/d/ac;->c:Ljava/util/List;

    iget-object v4, v2, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, v1, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/io/File;Ljava/io/File;)V

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/droidguard/d/ac;->a(Lcom/google/android/gms/droidguard/d/ab;)V

    iget-object v0, v0, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/io/File;Ljava/io/File;)V

    invoke-direct {p0}, Lcom/google/android/gms/droidguard/d/ac;->c()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/droidguard/d/ac;->b()V

    return-object v1

    .line 80
    :cond_2
    :try_start_4
    invoke-static {v0, v2}, Lcom/google/android/gms/droidguard/d/ac;->a(Lcom/google/android/gms/droidguard/d/ab;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_5
    new-instance v1, Lcom/google/android/gms/droidguard/d/b;

    const-string v2, "Failed to extract libs."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/ab;
    .locals 7

    .prologue
    .line 161
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ac;->a:Landroid/content/Context;

    const-string v2, "dg_cache"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/droidguard/d/ab;

    new-instance v2, Ljava/io/File;

    const-string v3, "the.apk"

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v3, Ljava/io/File;

    const-string v4, "opt"

    invoke-direct {v3, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Ljava/io/File;

    const-string v5, "lib"

    invoke-direct {v4, v0, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v5, Ljava/io/File;

    const-string v6, "t"

    invoke-direct {v5, v0, v6}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/droidguard/d/ab;-><init>(Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/io/File;)V

    return-object v1
.end method

.method private a(Lcom/google/android/gms/droidguard/d/ab;)V
    .locals 4

    .prologue
    .line 108
    iget-object v0, p1, Lcom/google/android/gms/droidguard/d/ab;->d:Ljava/io/File;

    .line 110
    :try_start_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z

    move-result v1

    if-nez v1, :cond_0

    .line 111
    new-instance v0, Lcom/google/android/gms/droidguard/d/b;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to touch last-used file for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :catch_0
    move-exception v0

    .line 114
    new-instance v1, Lcom/google/android/gms/droidguard/d/b;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to touch last-used file for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;)V

    throw v1

    .line 116
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ac;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    move-result v0

    if-nez v0, :cond_1

    .line 117
    new-instance v0, Lcom/google/android/gms/droidguard/d/b;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to update last-used timestamp for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_1
    return-void
.end method

.method private static a(Ljava/io/File;Ljava/io/File;)V
    .locals 3

    .prologue
    .line 221
    invoke-virtual {p0, p1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    new-instance v0, Lcom/google/android/gms/droidguard/d/b;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to rename "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -> "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_0
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/ac;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    .line 213
    invoke-static {v0}, Lcom/google/android/gms/common/util/v;->a(Ljava/io/File;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 214
    const-string v2, "DG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to clean up temporary file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 217
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/ac;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 218
    return-void
.end method

.method private c()V
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/ac;->a:Landroid/content/Context;

    const-string v2, "dg_cache"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v1

    .line 229
    iget-object v2, p0, Lcom/google/android/gms/droidguard/d/ac;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 230
    invoke-virtual {v1}, Ljava/io/File;->list()[Ljava/lang/String;

    move-result-object v1

    array-length v4, v1

    :goto_0
    if-ge v0, v4, :cond_2

    aget-object v5, v1, v0

    .line 231
    invoke-direct {p0, v5}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v5

    .line 232
    invoke-virtual {v5}, Lcom/google/android/gms/droidguard/d/ab;->a()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 233
    iget-object v6, v5, Lcom/google/android/gms/droidguard/d/ab;->d:Ljava/io/File;

    .line 236
    invoke-virtual {v6}, Ljava/io/File;->exists()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 237
    invoke-virtual {v6}, Ljava/io/File;->lastModified()J

    move-result-wide v6

    const-wide/32 v8, 0x48190800

    add-long/2addr v6, v8

    .line 238
    cmp-long v6, v2, v6

    if-ltz v6, :cond_1

    .line 239
    :cond_0
    iget-object v5, v5, Lcom/google/android/gms/droidguard/d/ab;->a:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/common/util/v;->a(Ljava/io/File;)Z

    .line 230
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_2
    return-void
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lcom/google/android/gms/droidguard/d/ag;

    invoke-direct {p0, p1}, Lcom/google/android/gms/droidguard/d/ac;->a(Lcom/google/android/gms/droidguard/d/ag;)Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lcom/google/android/gms/droidguard/d/ag;

    check-cast p2, Lcom/google/android/gms/droidguard/d/ab;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/droidguard/d/ac;->a(Lcom/google/android/gms/droidguard/d/ag;Lcom/google/android/gms/droidguard/d/ab;)Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 41
    check-cast p1, Lcom/google/android/gms/droidguard/d/ag;

    iget-object v0, p1, Lcom/google/android/gms/droidguard/d/ag;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/droidguard/d/ac;->a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/ab;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/droidguard/d/ac;->a(Lcom/google/android/gms/droidguard/d/ab;)V

    return-void
.end method
