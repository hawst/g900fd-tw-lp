.class final Lcom/google/android/gms/cast/b/k;
.super Lcom/google/android/gms/cast/b/ae;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/b/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1026
    iput-object p1, p0, Lcom/google/android/gms/cast/b/k;->a:Lcom/google/android/gms/cast/b/e;

    invoke-direct {p0, p2}, Lcom/google/android/gms/cast/b/ae;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 1029
    iget-object v0, p0, Lcom/google/android/gms/cast/b/k;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1030
    iget-object v0, p0, Lcom/google/android/gms/cast/b/k;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->a(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "proximity info name=%s, bssid=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1033
    :cond_0
    invoke-static {}, Lcom/google/android/gms/cast/b/e;->q()Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 1036
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/cast/b/e;->q()Ljava/util/HashMap;

    move-result-object v0

    invoke-static {p2}, Lcom/google/android/gms/cast/e/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/util/Pair;

    iget-object v4, p0, Lcom/google/android/gms/cast/b/k;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v4}, Lcom/google/android/gms/cast/b/e;->c(Lcom/google/android/gms/cast/b/e;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1039
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/cast/b/k;->a:Lcom/google/android/gms/cast/b/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/b/e;->j(Lcom/google/android/gms/cast/b/e;)Lcom/google/android/gms/cast/b/m;

    move-result-object v0

    invoke-interface {v0, p2, p1}, Lcom/google/android/gms/cast/b/m;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    return-void

    .line 1039
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
