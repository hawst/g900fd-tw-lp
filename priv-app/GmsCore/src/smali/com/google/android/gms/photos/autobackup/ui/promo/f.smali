.class final Lcom/google/android/gms/photos/autobackup/ui/promo/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/ui/promo/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)V
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/f;->a:Lcom/google/android/gms/photos/autobackup/ui/promo/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/f;->a:Lcom/google/android/gms/photos/autobackup/ui/promo/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->e(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Landroid/widget/RadioGroup;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->bx:I

    if-ne v0, v1, :cond_1

    .line 121
    sget-object v0, Lcom/google/android/gms/photos/j;->d:Lcom/google/android/gms/photos/j;

    .line 125
    :goto_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(I)V

    .line 126
    sget-object v2, Lcom/google/android/gms/photos/j;->b:Lcom/google/android/gms/photos/j;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/f;->a:Lcom/google/android/gms/photos/autobackup/ui/promo/b;

    invoke-static {v2}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->d(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Lcom/google/android/gms/photos/e;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/f;->a:Lcom/google/android/gms/photos/autobackup/ui/promo/b;

    invoke-static {v3}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->c(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Lcom/google/android/gms/photos/autobackup/l;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/photos/i;->a:Lcom/google/android/gms/photos/i;

    invoke-virtual {v2, v3, v0, v1, v4}, Lcom/google/android/gms/photos/e;->a(Ljava/lang/String;Lcom/google/android/gms/photos/j;Ljava/util/List;Lcom/google/android/gms/photos/i;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/f;->a:Lcom/google/android/gms/photos/autobackup/ui/promo/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->c(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Lcom/google/android/gms/photos/autobackup/l;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/photos/autobackup/l;->a(Z)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ui/promo/f;->a:Lcom/google/android/gms/photos/autobackup/ui/promo/b;

    invoke-static {v0}, Lcom/google/android/gms/photos/autobackup/ui/promo/b;->b(Lcom/google/android/gms/photos/autobackup/ui/promo/b;)Lcom/google/android/gms/photos/autobackup/ui/promo/h;

    move-result-object v0

    invoke-static {}, Lcom/google/android/libraries/social/autobackup/s;->a()Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->b:Landroid/app/Activity;

    iget-object v3, v0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->d:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v3}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/autobackup/s;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->a()V

    .line 131
    :cond_0
    :goto_1
    return-void

    .line 123
    :cond_1
    sget-object v0, Lcom/google/android/gms/photos/j;->c:Lcom/google/android/gms/photos/j;

    goto :goto_0

    .line 130
    :cond_2
    iget-object v3, v0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->a:Landroid/support/v4/app/Fragment;

    invoke-virtual {v3}, Landroid/support/v4/app/Fragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v4

    if-nez v4, :cond_0

    iget-object v4, v0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->d:Lcom/google/android/gms/photos/autobackup/l;

    invoke-virtual {v4}, Lcom/google/android/gms/photos/autobackup/l;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1, v2}, Lcom/google/android/gms/photos/autobackup/ui/aa;->a(Ljava/lang/String;ZZ)Lcom/google/android/gms/photos/autobackup/ui/aa;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/photos/autobackup/ui/promo/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v3, v0}, Lcom/google/android/gms/photos/autobackup/ui/aa;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_1
.end method
