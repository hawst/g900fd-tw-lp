.class final Lcom/google/android/gms/droidguard/d/t;
.super Lcom/google/android/gms/droidguard/d/a;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/util/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/common/util/r;

    invoke-direct {v0}, Lcom/google/android/gms/common/util/r;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/droidguard/d/t;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V

    .line 51
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/util/p;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/google/android/gms/droidguard/d/a;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/t;->a:Landroid/content/Context;

    .line 46
    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/t;->b:Lcom/google/android/gms/common/util/p;

    .line 47
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/gms/droidguard/d/y;)Lcom/google/android/gms/droidguard/d/s;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 55
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/droidguard/d/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/droidguard/d/u;-><init>(Lcom/google/android/gms/droidguard/d/t;)V

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/d/u;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/droidguard/d/t;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long v4, v2, v4

    .line 60
    const-string v1, "main"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v6, "f"

    aput-object v6, v2, v3

    const/4 v3, 0x1

    const-string v6, "d"

    aput-object v6, v2, v3

    const/4 v3, 0x2

    const-string v6, "e"

    aput-object v6, v2, v3

    const/4 v3, 0x3

    const-string v6, "c"

    aput-object v6, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "a = ? AND b <= "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v6, " AND "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " < (b + c"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p1, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const-string v7, "b DESC"

    const-string v8, "1"

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v8

    .line 78
    :try_start_2
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v1

    if-nez v1, :cond_0

    .line 79
    :try_start_3
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 90
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 93
    :try_start_4
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 94
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-object v2, v9

    :goto_0
    monitor-exit p0

    return-object v2

    .line 82
    :cond_0
    :try_start_5
    new-instance v2, Lcom/google/android/gms/droidguard/d/s;

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const/4 v1, 0x1

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x2

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x3

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/droidguard/d/s;-><init>([BLjava/lang/String;Ljava/lang/String;J)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 89
    :try_start_6
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 90
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 93
    :try_start_7
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 94
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_7
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_0

    .line 96
    :catch_0
    move-exception v0

    .line 97
    :try_start_8
    new-instance v1, Lcom/google/android/gms/droidguard/d/b;

    const-string v2, "Database read error."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 89
    :catchall_1
    move-exception v1

    :try_start_9
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 90
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    throw v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 93
    :catchall_2
    move-exception v1

    :try_start_a
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 94
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v1
    :try_end_a
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_0
.end method

.method private declared-synchronized a(Lcom/google/android/gms/droidguard/d/y;Lcom/google/android/gms/droidguard/d/s;)Lcom/google/android/gms/droidguard/d/s;
    .locals 8

    .prologue
    .line 103
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/t;->b:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    .line 104
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 105
    const-string v3, "a"

    iget-object v4, p1, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v3, "b"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 107
    const-string v0, "c"

    iget-wide v4, p2, Lcom/google/android/gms/droidguard/d/s;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 108
    const-string v0, "d"

    iget-object v1, p2, Lcom/google/android/gms/droidguard/d/s;->b:Lcom/google/android/gms/droidguard/d/ag;

    iget-object v1, v1, Lcom/google/android/gms/droidguard/d/ag;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v0, "e"

    iget-object v1, p2, Lcom/google/android/gms/droidguard/d/s;->b:Lcom/google/android/gms/droidguard/d/ag;

    iget-object v1, v1, Lcom/google/android/gms/droidguard/d/ag;->b:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const-string v0, "f"

    iget-object v1, p2, Lcom/google/android/gms/droidguard/d/s;->a:[B

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 112
    new-instance v0, Lcom/google/android/gms/droidguard/d/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/droidguard/d/u;-><init>(Lcom/google/android/gms/droidguard/d/t;)V

    invoke-virtual {v0}, Lcom/google/android/gms/droidguard/d/u;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 113
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 115
    :try_start_1
    const-string v0, "a = ?"

    .line 116
    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/android/gms/droidguard/d/y;->a:Ljava/lang/String;

    aput-object v5, v3, v4

    .line 117
    iget-wide v4, p2, Lcom/google/android/gms/droidguard/d/s;->c:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    .line 119
    const-string v2, "main"

    invoke-virtual {v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 127
    :cond_0
    :goto_0
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 129
    :try_start_2
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 130
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 135
    monitor-exit p0

    return-object p2

    .line 121
    :cond_1
    :try_start_3
    const-string v4, "main"

    invoke-virtual {v1, v4, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 123
    if-nez v0, :cond_0

    .line 124
    const-string v0, "main"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 130
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v0
    :try_end_4
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 134
    :catch_0
    move-exception v0

    .line 133
    :try_start_5
    new-instance v1, Lcom/google/android/gms/droidguard/d/b;

    const-string v2, "Database access error."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/droidguard/d/b;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 103
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/gms/droidguard/d/y;

    invoke-direct {p0, p1}, Lcom/google/android/gms/droidguard/d/t;->a(Lcom/google/android/gms/droidguard/d/y;)Lcom/google/android/gms/droidguard/d/s;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/gms/droidguard/d/y;

    check-cast p2, Lcom/google/android/gms/droidguard/d/s;

    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/droidguard/d/t;->a(Lcom/google/android/gms/droidguard/d/y;Lcom/google/android/gms/droidguard/d/s;)Lcom/google/android/gms/droidguard/d/s;

    move-result-object v0

    return-object v0
.end method
