.class public final Lcom/google/android/gms/drive/g/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/security/Key;Ljava/io/OutputStream;)Ljavax/crypto/CipherOutputStream;
    .locals 2

    .prologue
    .line 73
    :try_start_0
    invoke-interface {p0}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 74
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 76
    new-instance v1, Ljavax/crypto/CipherOutputStream;

    invoke-direct {v1, p1, v0}, Ljavax/crypto/CipherOutputStream;-><init>(Ljava/io/OutputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2

    return-object v1

    .line 77
    :catch_0
    move-exception v0

    .line 78
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 79
    :catch_1
    move-exception v0

    .line 80
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 81
    :catch_2
    move-exception v0

    .line 82
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Ljava/security/Key;Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 4

    .prologue
    .line 94
    :try_start_0
    invoke-interface {p0}, Ljava/security/Key;->getAlgorithm()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 95
    const/4 v1, 0x2

    invoke-virtual {v0, v1, p0}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 97
    const/16 v1, 0x4000

    new-array v1, v1, [B

    .line 98
    :cond_0
    :goto_0
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 100
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljavax/crypto/Cipher;->update([BII)[B

    move-result-object v2

    .line 101
    if-eqz v2, :cond_0

    .line 102
    invoke-virtual {p2, v2}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_0

    .line 106
    :catch_0
    move-exception v0

    .line 107
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 105
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljavax/crypto/Cipher;->doFinal()[B

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljavax/crypto/BadPaddingException; {:try_start_1 .. :try_end_1} :catch_4

    .line 116
    return-void

    .line 108
    :catch_1
    move-exception v0

    .line 109
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 110
    :catch_2
    move-exception v0

    .line 111
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 112
    :catch_3
    move-exception v0

    .line 113
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 114
    :catch_4
    move-exception v0

    .line 115
    new-instance v1, Lcom/google/android/gms/drive/g/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/g/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
