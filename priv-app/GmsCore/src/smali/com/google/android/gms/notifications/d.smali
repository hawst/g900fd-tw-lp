.class final Lcom/google/android/gms/notifications/d;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# instance fields
.field f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 71
    iput-object p2, p0, Lcom/google/android/gms/notifications/d;->g:Ljava/lang/String;

    .line 72
    return-void
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    .line 114
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    invoke-super {p0, v0}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V

    .line 117
    :cond_0
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    :try_start_0
    const-string v0, "oauth2:https://www.google.com/accounts/OAuthLogin"

    .line 123
    iget-object v1, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/notifications/d;->g:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    .line 124
    :catch_0
    move-exception v0

    .line 125
    :goto_1
    const-string v1, "GnotsAuthLoader"

    const-string v2, "Failed to generate auth token."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 127
    const/4 v0, 0x0

    goto :goto_0

    .line 124
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method private l()Ljava/lang/String;
    .locals 4

    .prologue
    .line 132
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "accounts.google.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "OAuthLogin"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "issueuberauth"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "source"

    const-string v2, "gms.gnots"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 140
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v1

    .line 141
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v2

    .line 142
    new-instance v3, Lcom/google/android/gms/notifications/e;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, p0, v0, v2, v2}, Lcom/google/android/gms/notifications/e;-><init>(Lcom/google/android/gms/notifications/d;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 144
    invoke-virtual {v1, v3}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/android/volley/toolbox/aa;->a(Lcom/android/volley/p;)V

    .line 147
    :try_start_0
    invoke-virtual {v2}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 151
    :goto_0
    return-object v0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    :goto_1
    const-string v1, "GnotsAuthLoader"

    const-string v2, "Unable to generate uber auth token."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 151
    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 31
    check-cast p1, Landroid/os/Bundle;

    invoke-direct {p0, p1}, Lcom/google/android/gms/notifications/d;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->f:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/notifications/d;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/notifications/d;->f:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->f:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->h:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/notifications/d;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/notifications/d;->h:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->h:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    const-string v1, "GnotsAuthLoader.uberAuthToken"

    iget-object v2, p0, Lcom/google/android/gms/notifications/d;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    const-string v1, "GnotsAuthLoader.hadError"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 76
    invoke-super {p0}, Landroid/support/v4/a/a;->e()V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    .line 80
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 84
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/notifications/d;->i:Landroid/os/Bundle;

    invoke-direct {p0, v0}, Lcom/google/android/gms/notifications/d;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected final g()V
    .locals 0

    .prologue
    .line 109
    return-void
.end method
