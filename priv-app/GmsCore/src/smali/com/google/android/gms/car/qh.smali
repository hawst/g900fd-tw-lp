.class public Lcom/google/android/gms/car/qh;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/car/qh;


# instance fields
.field private b:J

.field private final c:Landroid/os/HandlerThread;

.field private final d:Landroid/os/Handler;

.field private final e:Ljava/lang/Runnable;

.field private f:I

.field private final g:Ljava/util/LinkedList;

.field private final h:Ljava/util/LinkedList;


# direct methods
.method private constructor <init>()V
    .locals 3

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Landroid/os/HandlerThread;

    const-class v1, Lcom/google/android/gms/car/qh;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/qh;->c:Landroid/os/HandlerThread;

    .line 40
    new-instance v0, Lcom/google/android/gms/car/qi;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/qi;-><init>(Lcom/google/android/gms/car/qh;)V

    iput-object v0, p0, Lcom/google/android/gms/car/qh;->e:Ljava/lang/Runnable;

    .line 90
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/qh;->f:I

    .line 93
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    .line 95
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/qh;->h:Ljava/util/LinkedList;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 183
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/qh;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/qh;->d:Landroid/os/Handler;

    .line 184
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/qh;)I
    .locals 2

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/gms/car/qh;->f:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/google/android/gms/car/qh;->f:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/qh;J)J
    .locals 1

    .prologue
    .line 28
    iput-wide p1, p0, Lcom/google/android/gms/car/qh;->b:J

    return-wide p1
.end method

.method static synthetic a()Lcom/google/android/gms/car/qh;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    return-object v0
.end method

.method private declared-synchronized a(Landroid/os/Handler;Ljava/lang/Runnable;J)Lcom/google/android/gms/car/qk;
    .locals 2

    .prologue
    .line 188
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/qh;->c(Landroid/os/Handler;Ljava/lang/Runnable;)Lcom/google/android/gms/car/qk;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/qk;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/car/qk;-><init>(Landroid/os/Handler;Ljava/lang/Runnable;)V

    iget-object v1, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 193
    :cond_0
    iget v1, p0, Lcom/google/android/gms/car/qh;->f:I

    add-int/lit8 v1, v1, 0xa

    iput v1, v0, Lcom/google/android/gms/car/qk;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 194
    monitor-exit p0

    return-object v0

    .line 188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public static declared-synchronized a(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x1388

    .line 98
    const-class v1, Lcom/google/android/gms/car/qh;

    monitor-enter v1

    :try_start_0
    const-string v0, "CAR.TIME"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "CAR.TIME"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "add handler:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runnable:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_0
    const-wide/16 v2, 0x1f4

    cmp-long v0, v4, v2

    if-gez v0, :cond_1

    .line 102
    const-string v0, "CAR.TIME"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "add timeout value too small:5000"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " min is:500"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_1
    invoke-static {}, Lcom/google/android/gms/car/qh;->c()Z

    move-result v0

    .line 106
    sget-object v2, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    const-wide/16 v4, 0x1388

    invoke-direct {v2, p0, p1, v4, v5}, Lcom/google/android/gms/car/qh;->a(Landroid/os/Handler;Ljava/lang/Runnable;J)Lcom/google/android/gms/car/qk;

    .line 107
    if-eqz v0, :cond_2

    .line 108
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    invoke-direct {v0}, Lcom/google/android/gms/car/qh;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :cond_2
    monitor-exit v1

    return-void

    .line 98
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static declared-synchronized a(Ljava/util/concurrent/Semaphore;)V
    .locals 4

    .prologue
    .line 157
    const-class v1, Lcom/google/android/gms/car/qh;

    monitor-enter v1

    :try_start_0
    const-string v0, "CAR.TIME"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "CAR.TIME"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "release semaphore:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    if-nez v0, :cond_1

    .line 161
    invoke-virtual {p0}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :goto_0
    monitor-exit v1

    return-void

    .line 164
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/qh;->b(Ljava/util/concurrent/Semaphore;)Lcom/google/android/gms/car/ql;

    move-result-object v0

    .line 165
    if-nez v0, :cond_2

    .line 166
    invoke-virtual {p0}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 169
    :cond_2
    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/ql;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public static a(Ljava/util/concurrent/Semaphore;J)Z
    .locals 7

    .prologue
    const-wide/16 v4, 0x1388

    const/4 v6, 0x2

    .line 124
    const-string v0, "CAR.TIME"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 125
    const-string v0, "CAR.TIME"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tryAcquire semaphore:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    :cond_0
    const-wide/16 v0, 0x1f4

    cmp-long v0, v4, v0

    if-gez v0, :cond_1

    .line 128
    const-string v0, "CAR.TIME"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "tryAcquire timeout value too small:5000"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " min is:500"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_1
    invoke-virtual {p0}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v0

    .line 132
    if-eqz v0, :cond_3

    .line 153
    :cond_2
    :goto_0
    return v0

    .line 137
    :cond_3
    const-class v1, Lcom/google/android/gms/car/qh;

    monitor-enter v1

    .line 138
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/car/qh;->c()Z

    move-result v0

    .line 139
    sget-object v2, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    const-wide/16 v4, 0x1388

    invoke-direct {v2, p0, v4, v5}, Lcom/google/android/gms/car/qh;->b(Ljava/util/concurrent/Semaphore;J)Lcom/google/android/gms/car/ql;

    move-result-object v2

    .line 140
    if-eqz v0, :cond_4

    .line 141
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    invoke-direct {v0}, Lcom/google/android/gms/car/qh;->d()V

    .line 143
    :cond_4
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 144
    invoke-virtual {v2}, Lcom/google/android/gms/car/ql;->b()Z

    move-result v0

    .line 145
    const-class v1, Lcom/google/android/gms/car/qh;

    monitor-enter v1

    .line 146
    :try_start_1
    sget-object v3, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    if-eqz v3, :cond_5

    .line 147
    sget-object v3, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    iget-object v3, v3, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    invoke-virtual {v3, v2}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 149
    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 150
    const-string v1, "CAR.TIME"

    invoke-static {v1, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    if-nez v0, :cond_2

    .line 151
    const-string v1, "CAR.TIME"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "tryAcquire failed for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 149
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/qh;)J
    .locals 2

    .prologue
    .line 28
    iget-wide v0, p0, Lcom/google/android/gms/car/qh;->b:J

    return-wide v0
.end method

.method static synthetic b()Lcom/google/android/gms/car/qh;
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    return-object v0
.end method

.method private b(Ljava/util/concurrent/Semaphore;)Lcom/google/android/gms/car/ql;
    .locals 3

    .prologue
    .line 245
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/qj;

    .line 246
    instance-of v2, v0, Lcom/google/android/gms/car/ql;

    if-eqz v2, :cond_0

    .line 247
    check-cast v0, Lcom/google/android/gms/car/ql;

    .line 248
    iget-object v2, v0, Lcom/google/android/gms/car/ql;->b:Ljava/util/concurrent/Semaphore;

    if-ne v2, p1, :cond_0

    .line 253
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private declared-synchronized b(Ljava/util/concurrent/Semaphore;J)Lcom/google/android/gms/car/ql;
    .locals 4

    .prologue
    .line 226
    monitor-enter p0

    const-wide/16 v0, 0xfa

    add-long/2addr v0, p2

    long-to-int v0, v0

    :try_start_0
    div-int/lit16 v0, v0, 0x1f4

    .line 227
    if-nez v0, :cond_1

    .line 228
    const/4 v0, 0x1

    move v1, v0

    .line 230
    :goto_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/qh;->b(Ljava/util/concurrent/Semaphore;)Lcom/google/android/gms/car/ql;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/car/ql;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/ql;-><init>(Ljava/util/concurrent/Semaphore;)V

    iget-object v2, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_0
    iget v2, p0, Lcom/google/android/gms/car/qh;->f:I

    add-int/2addr v1, v2

    iput v1, v0, Lcom/google/android/gms/car/ql;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    monitor-exit p0

    return-object v0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v1, v0

    goto :goto_0
.end method

.method public static declared-synchronized b(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 4

    .prologue
    .line 113
    const-class v1, Lcom/google/android/gms/car/qh;

    monitor-enter v1

    :try_start_0
    const-string v0, "CAR.TIME"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    const-string v0, "CAR.TIME"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "cancel handler:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " runnable:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 120
    :goto_0
    monitor-exit v1

    return-void

    .line 119
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/qh;->d(Landroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private c(Landroid/os/Handler;Ljava/lang/Runnable;)Lcom/google/android/gms/car/qk;
    .locals 3

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/qj;

    .line 208
    instance-of v2, v0, Lcom/google/android/gms/car/qk;

    if-eqz v2, :cond_0

    .line 209
    check-cast v0, Lcom/google/android/gms/car/qk;

    .line 210
    iget-object v2, v0, Lcom/google/android/gms/car/qk;->b:Landroid/os/Handler;

    if-ne v2, p1, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/car/qk;->c:Ljava/lang/Runnable;

    if-ne v2, p2, :cond_0

    .line 215
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    return-object v0
.end method

.method private static c()Z
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    if-nez v0, :cond_0

    .line 175
    new-instance v0, Lcom/google/android/gms/car/qh;

    invoke-direct {v0}, Lcom/google/android/gms/car/qh;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/qh;->a:Lcom/google/android/gms/car/qh;

    .line 176
    const/4 v0, 0x1

    .line 178
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/car/qh;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/gms/car/qh;->f:I

    return v0
.end method

.method private declared-synchronized d()V
    .locals 4

    .prologue
    .line 257
    monitor-enter p0

    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/car/qh;->b:J

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->d:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/qh;->e:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    monitor-exit p0

    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 2

    .prologue
    .line 219
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/qh;->c(Landroid/os/Handler;Ljava/lang/Runnable;)Lcom/google/android/gms/car/qk;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_0

    .line 221
    iget-object v1, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    :cond_0
    monitor-exit p0

    return-void

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/qh;)Ljava/util/LinkedList;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->h:Ljava/util/LinkedList;

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 1

    .prologue
    .line 262
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->g:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    monitor-exit p0

    return-void

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/qh;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/car/qh;->e()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/car/qh;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/qh;->d:Landroid/os/Handler;

    return-object v0
.end method
