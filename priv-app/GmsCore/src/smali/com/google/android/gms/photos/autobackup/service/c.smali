.class final Lcom/google/android/gms/photos/autobackup/service/c;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/c;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    .line 75
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 76
    return-void
.end method


# virtual methods
.method public final p(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/c;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 83
    const-string v0, "AutoBackupService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const-string v0, "AutoBackupService"

    const-string v1, "Verified Package Name."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/c;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 89
    const-string v0, "AutoBackupService"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    const-string v0, "AutoBackupService"

    const-string v1, "Verified Package is Google Signed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/google/android/gms/photos/autobackup/service/a;

    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/service/c;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    iget-object v3, p0, Lcom/google/android/gms/photos/autobackup/service/c;->a:Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;

    invoke-static {v3}, Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;->a(Lcom/google/android/gms/photos/autobackup/service/AutoBackupService;)Lcom/google/android/gms/photos/autobackup/service/a/b;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/photos/autobackup/service/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/photos/autobackup/service/a/b;)V

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/service/a;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 99
    :goto_0
    return-void

    .line 97
    :catch_0
    move-exception v0

    const-string v0, "AutoBackupService"

    const-string v1, "Client died while brokering service."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
