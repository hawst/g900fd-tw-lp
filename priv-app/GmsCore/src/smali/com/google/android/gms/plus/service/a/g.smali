.class public final Lcom/google/android/gms/plus/service/a/g;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Z

.field private final e:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/g;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/g;->b:Ljava/lang/String;

    .line 38
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/g;->c:Ljava/lang/String;

    .line 39
    iput-boolean p4, p0, Lcom/google/android/gms/plus/service/a/g;->d:Z

    .line 40
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/g;->e:Lcom/google/android/gms/plus/internal/f;

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLcom/google/android/gms/plus/internal/f;)V
    .locals 6

    .prologue
    .line 31
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/service/a/g;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/plus/internal/f;)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 46
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/g;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/g;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/g;->c:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/plus/service/a/g;->d:Z

    if-eqz v0, :cond_2

    iget-object v4, p2, Lcom/google/android/gms/plus/b/b;->e:Lcom/google/android/gms/plus/b/d;

    if-eqz v3, :cond_0

    iget-object v3, v4, Lcom/google/android/gms/plus/b/d;->a:Lcom/google/android/gms/plus/service/v1whitelisted/h;

    invoke-virtual {v3, v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/h;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    :cond_0
    iget-object v2, v4, Lcom/google/android/gms/plus/b/d;->b:Lcom/google/android/gms/plus/service/lso/f;

    const-string v3, "RevokeToken"

    if-eqz v0, :cond_1

    const-string v4, "revocation_handle"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/lso/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/plus/service/lso/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/plus/service/lso/f;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/lso/RevokeToken;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    .line 48
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/g;->e:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/f;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 63
    :goto_1
    return-void

    .line 46
    :cond_2
    iget-object v0, p2, Lcom/google/android/gms/plus/b/b;->c:Lcom/google/android/gms/plus/b/n;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/plus/b/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 51
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v7, v0, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 52
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/g;->e:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v8, v1, v6}, Lcom/google/android/gms/plus/internal/f;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    .line 56
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/g;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/g;->e:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v8, v0, v6}, Lcom/google/android/gms/plus/internal/f;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    .line 60
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/g;->e:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v6, v6}, Lcom/google/android/gms/plus/internal/f;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/g;->e:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/g;->e:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/f;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 70
    :cond_0
    return-void
.end method
