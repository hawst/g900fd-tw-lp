.class public Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/PriorityQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 44
    new-instance v0, Ljava/util/PriorityQueue;

    const/16 v1, 0x8

    new-instance v2, Lcom/google/android/gms/people/service/bg/d;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/gms/people/service/bg/d;-><init>(B)V

    invoke-direct {v0, v1, v2}, Ljava/util/PriorityQueue;-><init>(ILjava/util/Comparator;)V

    sput-object v0, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a:Ljava/util/PriorityQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->setIntentRedelivery(Z)V

    .line 58
    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 114
    invoke-static {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->d(Landroid/content/Context;)V

    .line 115
    invoke-static {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->b(Landroid/content/Context;)V

    .line 116
    invoke-static {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->c(Landroid/content/Context;)V

    .line 119
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "PeopleTasks"

    const-string v1, "openSyncBlockingLatch"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lcom/google/android/gms/people/service/bg/c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/service/bg/c;-><init>(B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/bg/e;)V

    .line 128
    invoke-static {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->e(Landroid/content/Context;)V

    .line 129
    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 217
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 218
    const-string v0, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "android.intent.extra.REPLACING"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 220
    :goto_0
    const/4 v3, 0x3

    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 221
    const-string v3, "PeopleTasks"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "handlePackageChanged: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " replacing="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    :cond_0
    if-eqz v0, :cond_3

    .line 235
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v1

    .line 218
    goto :goto_0

    .line 231
    :cond_3
    invoke-static {}, Lcom/google/android/gms/people/c/f;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 234
    new-instance v0, Lcom/google/android/gms/people/service/bg/b;

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/service/bg/b;-><init>(B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/bg/e;)V

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/people/service/bg/e;)V
    .locals 4

    .prologue
    .line 89
    sget-object v1, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a:Ljava/util/PriorityQueue;

    monitor-enter v1

    .line 91
    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/bg/e;

    .line 92
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-ne v0, v3, :cond_0

    .line 93
    monitor-exit v1

    .line 102
    :goto_0
    return-void

    .line 96
    :cond_1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    const-string v0, "PeopleTasks"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Scheduling "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 99
    :cond_2
    sget-object v0, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->offer(Ljava/lang/Object;)Z

    .line 100
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 100
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "PeopleTasks"

    const-string v1, "updateLocaleIfNecessary"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 160
    :cond_0
    invoke-static {}, Lcom/google/android/gms/people/c/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 164
    :goto_0
    return-void

    .line 163
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/service/bg/g;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/service/bg/g;-><init>(B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/bg/e;)V

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 182
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "PeopleTasks"

    const-string v1, "updateSearchIndexIfNecessary"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    :cond_0
    invoke-static {}, Lcom/google/android/gms/people/c/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 189
    :goto_0
    return-void

    .line 188
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/service/bg/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/service/bg/h;-><init>(B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/bg/e;)V

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 255
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    const-string v0, "PeopleTasks"

    const-string v1, "updateAccounts"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    invoke-static {}, Lcom/google/android/gms/people/c/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 262
    :goto_0
    return-void

    .line 261
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/service/bg/f;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/service/bg/f;-><init>(B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/bg/e;)V

    goto :goto_0
.end method

.method public static e(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 291
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    const-string v0, "PeopleTasks"

    const-string v1, "completePendingContactsCleanup"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    :cond_0
    invoke-static {}, Lcom/google/android/gms/people/c/f;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 298
    :goto_0
    return-void

    .line 297
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/service/bg/a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/service/bg/a;-><init>(B)V

    invoke-static {p0, v0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a(Landroid/content/Context;Lcom/google/android/gms/people/service/bg/e;)V

    goto :goto_0
.end method

.method public static f(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 317
    invoke-static {p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/a/c;->d(Z)V

    .line 318
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 65
    :cond_0
    :goto_0
    sget-object v1, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a:Ljava/util/PriorityQueue;

    monitor-enter v1

    .line 66
    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 67
    monitor-exit v1

    return-void

    .line 69
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->a:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/bg/e;

    .line 70
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 74
    const-string v1, "PeopleTasks"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Running "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/people/service/bg/PeopleBackgroundTasks;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/people/service/bg/e;->a(Landroid/content/Context;)V

    .line 78
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 79
    const-string v1, "PeopleTasks"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "  "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " took "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long v2, v4, v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "ms"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
