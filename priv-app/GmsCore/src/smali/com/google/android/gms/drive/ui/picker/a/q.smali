.class public final enum Lcom/google/android/gms/drive/ui/picker/a/q;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/ui/picker/a/q;

.field public static final enum b:Lcom/google/android/gms/drive/ui/picker/a/q;

.field public static final enum c:Lcom/google/android/gms/drive/ui/picker/a/q;

.field public static final enum d:Lcom/google/android/gms/drive/ui/picker/a/q;

.field public static final enum e:Lcom/google/android/gms/drive/ui/picker/a/q;

.field public static final enum f:Lcom/google/android/gms/drive/ui/picker/a/q;

.field private static final synthetic j:[Lcom/google/android/gms/drive/ui/picker/a/q;


# instance fields
.field private final g:Lcom/google/android/gms/drive/query/Filter;

.field private final h:Lcom/google/android/gms/drive/ui/picker/a/a/t;

.field private final i:Ljava/util/EnumSet;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v2, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 22
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    const-string v1, "NONE"

    new-instance v3, Lcom/google/android/gms/drive/query/internal/MatchAllFilter;

    invoke-direct {v3}, Lcom/google/android/gms/drive/query/internal/MatchAllFilter;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/a/a/t;->b:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-direct {v0, v1, v9, v3, v4}, Lcom/google/android/gms/drive/ui/picker/a/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 23
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    const-string v1, "STARRED"

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->y:Lcom/google/android/gms/drive/metadata/internal/a/h;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/i;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/a/a/t;->b:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-direct {v0, v1, v8, v3, v4}, Lcom/google/android/gms/drive/ui/picker/a/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->b:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 24
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    const-string v1, "PINNED"

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/i;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-direct {v0, v1, v10, v3, v4}, Lcom/google/android/gms/drive/ui/picker/a/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->c:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 25
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    const-string v1, "INCOMING"

    new-instance v3, Lcom/google/android/gms/drive/query/internal/FieldOnlyFilter;

    sget-object v4, Lcom/google/android/gms/drive/query/i;->e:Lcom/google/android/gms/drive/metadata/j;

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/query/internal/FieldOnlyFilter;-><init>(Lcom/google/android/gms/drive/metadata/i;)V

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/a/a/t;->c:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    sget-object v5, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    sget-object v6, Lcom/google/android/gms/drive/ui/picker/a/a/t;->c:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-static {v5, v6}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/ui/picker/a/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;Ljava/util/EnumSet;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->d:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 27
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    const-string v1, "MY_DRIVE"

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->w:Lcom/google/android/gms/drive/metadata/internal/a/f;

    const-string v4, "root"

    invoke-static {v4}, Lcom/google/android/gms/drive/DriveId;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/h;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-direct {v0, v1, v11, v3, v4}, Lcom/google/android/gms/drive/ui/picker/a/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->e:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 30
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    const-string v1, "RECENT"

    const/4 v3, 0x5

    new-instance v4, Lcom/google/android/gms/drive/query/internal/FieldOnlyFilter;

    sget-object v5, Lcom/google/android/gms/drive/query/i;->h:Lcom/google/android/gms/drive/metadata/j;

    invoke-direct {v4, v5}, Lcom/google/android/gms/drive/query/internal/FieldOnlyFilter;-><init>(Lcom/google/android/gms/drive/metadata/i;)V

    new-array v5, v8, [Lcom/google/android/gms/drive/query/Filter;

    sget-object v6, Lcom/google/android/gms/drive/query/i;->b:Lcom/google/android/gms/drive/metadata/i;

    const-string v7, "application/vnd.google-apps.folder"

    invoke-static {v6, v7}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/i;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v6

    new-instance v7, Lcom/google/android/gms/drive/query/internal/NotFilter;

    invoke-direct {v7, v6}, Lcom/google/android/gms/drive/query/internal/NotFilter;-><init>(Lcom/google/android/gms/drive/query/Filter;)V

    aput-object v7, v5, v9

    invoke-static {v4, v5}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/query/Filter;[Lcom/google/android/gms/drive/query/Filter;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/ui/picker/a/a/t;->d:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-direct {v0, v1, v3, v4, v5}, Lcom/google/android/gms/drive/ui/picker/a/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;)V

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->f:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 21
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/android/gms/drive/ui/picker/a/q;

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/q;->b:Lcom/google/android/gms/drive/ui/picker/a/q;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/q;->c:Lcom/google/android/gms/drive/ui/picker/a/q;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/q;->d:Lcom/google/android/gms/drive/ui/picker/a/q;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/a/q;->e:Lcom/google/android/gms/drive/ui/picker/a/q;

    aput-object v1, v0, v11

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/a/q;->f:Lcom/google/android/gms/drive/ui/picker/a/q;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->j:[Lcom/google/android/gms/drive/ui/picker/a/q;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;)V
    .locals 6

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->c:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-static {v0}, Ljava/util/EnumSet;->of(Ljava/lang/Enum;)Ljava/util/EnumSet;

    move-result-object v0

    invoke-static {v0}, Ljava/util/EnumSet;->complementOf(Ljava/util/EnumSet;)Ljava/util/EnumSet;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/ui/picker/a/q;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;Ljava/util/EnumSet;)V

    .line 49
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/query/Filter;Lcom/google/android/gms/drive/ui/picker/a/a/t;Ljava/util/EnumSet;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->B:Lcom/google/android/gms/drive/metadata/internal/a/j;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/i;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v0

    .line 41
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/drive/query/Filter;

    aput-object v0, v1, v2

    invoke-static {p3, v1}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/query/Filter;[Lcom/google/android/gms/drive/query/Filter;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/q;->g:Lcom/google/android/gms/drive/query/Filter;

    .line 42
    iput-object p4, p0, Lcom/google/android/gms/drive/ui/picker/a/q;->h:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 43
    iput-object p5, p0, Lcom/google/android/gms/drive/ui/picker/a/q;->i:Ljava/util/EnumSet;

    .line 44
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/a/q;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/q;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/ui/picker/a/q;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->j:[Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/ui/picker/a/q;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/ui/picker/a/q;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/ui/picker/a/a/t;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/q;->h:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/query/Filter;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/q;->g:Lcom/google/android/gms/drive/query/Filter;

    return-object v0
.end method
