.class public final Lcom/google/android/gms/location/copresence/internal/r;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Lcom/google/android/gms/location/copresence/internal/r;


# instance fields
.field private final a:Ljava/util/HashMap;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/r;->a:Ljava/util/HashMap;

    .line 55
    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/gms/location/copresence/internal/r;
    .locals 2

    .prologue
    .line 47
    const-class v1, Lcom/google/android/gms/location/copresence/internal/r;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/location/copresence/internal/r;->b:Lcom/google/android/gms/location/copresence/internal/r;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/r;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/internal/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/internal/r;->b:Lcom/google/android/gms/location/copresence/internal/r;

    .line 50
    :cond_0
    sget-object v0, Lcom/google/android/gms/location/copresence/internal/r;->b:Lcom/google/android/gms/location/copresence/internal/r;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 47
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/copresence/r;)Lcom/google/android/gms/location/copresence/internal/o;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 65
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/r;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/copresence/internal/s;

    .line 68
    if-nez v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/r;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "listener already exists"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    new-instance v0, Lcom/google/android/gms/location/copresence/internal/s;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/location/copresence/internal/s;-><init>(Lcom/google/android/gms/location/copresence/r;B)V

    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/r;->a:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    :cond_0
    return-object v0

    :cond_1
    move v0, v1

    .line 70
    goto :goto_0
.end method
