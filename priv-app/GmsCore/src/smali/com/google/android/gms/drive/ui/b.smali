.class public final Lcom/google/android/gms/drive/ui/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/os/Handler;

.field b:Landroid/database/ContentObserver;

.field final c:Lcom/google/android/gms/drive/ui/e;

.field final d:Lcom/google/android/gms/drive/ui/g;

.field final e:Lcom/google/android/gms/drive/g/al;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ui/e;Lcom/google/android/gms/drive/ui/g;)V
    .locals 5

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/b;->a:Landroid/os/Handler;

    .line 87
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/b;->c:Lcom/google/android/gms/drive/ui/e;

    .line 88
    iput-object p2, p0, Lcom/google/android/gms/drive/ui/b;->d:Lcom/google/android/gms/drive/ui/g;

    .line 89
    new-instance v0, Lcom/google/android/gms/drive/ui/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/ui/c;-><init>(Lcom/google/android/gms/drive/ui/b;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/b;->f:Ljava/lang/Runnable;

    .line 99
    sget-object v0, Lcom/google/android/gms/drive/g/an;->a:Lcom/google/android/gms/drive/g/am;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/b;->f:Ljava/lang/Runnable;

    const/16 v2, 0x7d0

    new-instance v3, Lcom/google/android/gms/drive/g/x;

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/b;->a:Landroid/os/Handler;

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/g/x;-><init>(Landroid/os/Handler;)V

    const-string v4, "ActivityUpdater"

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/g/am;->a(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)Lcom/google/android/gms/drive/g/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/b;->e:Lcom/google/android/gms/drive/g/al;

    .line 101
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 137
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ActivityUpdater[rateLimiter=%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/b;->e:Lcom/google/android/gms/drive/g/al;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
