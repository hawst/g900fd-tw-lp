.class public final Lcom/google/android/gms/social/location/i;
.super Lcom/google/android/gms/social/location/ad;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/social/location/ad;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method private static a(Lcom/google/ac/e/a/a/i;)Ljava/util/List;
    .locals 13

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    if-nez v0, :cond_0

    .line 100
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 109
    :goto_0
    return-object v0

    .line 102
    :cond_0
    new-instance v8, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    array-length v0, v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 103
    iget-object v10, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    array-length v11, v10

    const/4 v0, 0x0

    move v9, v0

    :goto_1
    if-ge v9, v11, :cond_4

    aget-object v4, v10, v9

    .line 104
    iget-object v0, v4, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    iget-object v0, v4, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    iget-object v0, v4, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    new-instance v0, Lcom/google/android/gms/social/location/TimeShare;

    iget-object v1, v4, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    iget-object v2, v4, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    iget-object v3, v4, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "http:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    const-string v5, "https:"

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v12, "https:"

    invoke-direct {v5, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    iget-object v4, v4, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/social/location/TimeShare;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 105
    :goto_2
    if-eqz v0, :cond_2

    .line 106
    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 104
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move-object v0, v8

    .line 109
    goto :goto_0
.end method

.method private static a(Lcom/google/ac/e/a/a/i;I)Ljava/util/List;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 71
    iget-object v1, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    if-eqz v1, :cond_0

    if-ltz p1, :cond_0

    iget-object v1, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    array-length v1, v1

    if-lt p1, v1, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-object v0

    .line 75
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/google/c/f/b/f;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/people/data/c;->a([B)Ljava/util/List;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/social/location/DeviceLocationSettings;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 41
    new-instance v0, Lcom/google/ac/e/a/a/h;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/h;-><init>()V

    .line 42
    new-instance v1, Lcom/google/ac/e/a/a/i;

    invoke-direct {v1}, Lcom/google/ac/e/a/a/i;-><init>()V

    const-string v5, "getowneracl"

    invoke-virtual {p0, v0, v1, v5}, Lcom/google/android/gms/social/location/i;->a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;Ljava/lang/String;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/e/a/a/i;

    .line 43
    if-nez v0, :cond_0

    move-object v0, v4

    .line 53
    :goto_0
    return-object v0

    .line 46
    :cond_0
    iget-object v1, v0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    iget-object v1, v1, Lcom/google/ac/e/a/a/c;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No sharing state exists in response"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, v0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    iget-object v1, v1, Lcom/google/ac/e/a/a/c;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v2, :cond_4

    move v1, v2

    .line 47
    :goto_1
    invoke-static {v0, v3}, Lcom/google/android/gms/social/location/i;->a(Lcom/google/ac/e/a/a/i;I)Ljava/util/List;

    move-result-object v3

    .line 48
    invoke-static {v0, v2}, Lcom/google/android/gms/social/location/i;->a(Lcom/google/ac/e/a/a/i;I)Ljava/util/List;

    move-result-object v2

    .line 49
    invoke-static {v0}, Lcom/google/android/gms/social/location/i;->a(Lcom/google/ac/e/a/a/i;)Ljava/util/List;

    move-result-object v5

    .line 50
    if-eqz v3, :cond_3

    if-nez v2, :cond_5

    :cond_3
    move-object v0, v4

    .line 51
    goto :goto_0

    :cond_4
    move v1, v3

    .line 46
    goto :goto_1

    .line 53
    :cond_5
    new-instance v0, Lcom/google/android/gms/social/location/DeviceLocationSettings;

    invoke-direct {v0, v1, v3, v2, v5}, Lcom/google/android/gms/social/location/DeviceLocationSettings;-><init>(ZLjava/util/List;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0
.end method
