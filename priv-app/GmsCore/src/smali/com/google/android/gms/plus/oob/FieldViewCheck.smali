.class public final Lcom/google/android/gms/plus/oob/FieldViewCheck;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private d:Landroid/widget/CheckBox;

.field private e:Lcom/google/android/gms/plus/f/c;


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 66
    return-void
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 89
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->a:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/l;->dZ:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/l;->dY:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 116
    sget v0, Lcom/google/android/gms/p;->uO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->j()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 120
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->e()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 122
    new-instance v0, Lcom/google/android/gms/plus/f/c;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/f/c;-><init>(Landroid/widget/CheckBox;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->e:Lcom/google/android/gms/plus/f/c;

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->e:Lcom/google/android/gms/plus/f/c;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 124
    return-void

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->d()Z

    move-result v0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 4

    .prologue
    .line 101
    const-string v0, "customAds"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->c:Lcom/google/android/gms/plus/oob/e;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/analytics/l;->s:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/oob/e;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 106
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a:Z

    iget-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->d:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    return-object v0

    .line 102
    :cond_1
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->t:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method

.method protected final g()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    .prologue
    .line 128
    const-string v0, "picasa"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 134
    :goto_0
    return-object v0

    .line 131
    :cond_0
    const-string v0, "customAds"

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 132
    sget-object v0, Lcom/google/android/gms/common/analytics/l;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0

    .line 134
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->c:Lcom/google/android/gms/plus/oob/e;

    invoke-interface {v0}, Lcom/google/android/gms/plus/oob/e;->a()V

    .line 142
    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 78
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;

    if-nez v0, :cond_0

    .line 79
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 85
    :goto_0
    return-void

    .line 82
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;

    .line 83
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    iget-boolean v1, p1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;->a:Z

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 70
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/d;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 71
    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewCheck;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    iput-boolean v0, v1, Lcom/google/android/gms/plus/oob/FieldViewCheck$SavedState;->a:Z

    .line 73
    return-object v1
.end method
