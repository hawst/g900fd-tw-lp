.class public final Lcom/google/android/gms/ads/internal/b/n;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(JI)J
    .locals 6

    .prologue
    const-wide/32 v4, 0x4000ffff

    .line 126
    if-nez p2, :cond_1

    .line 127
    const-wide/16 p0, 0x1

    .line 133
    :cond_0
    :goto_0
    return-wide p0

    .line 128
    :cond_1
    const/4 v0, 0x1

    if-eq p2, v0, :cond_0

    .line 130
    rem-int/lit8 v0, p2, 0x2

    if-nez v0, :cond_2

    .line 131
    mul-long v0, p0, p0

    rem-long/2addr v0, v4

    div-int/lit8 v2, p2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/b/n;->a(JI)J

    move-result-wide v0

    rem-long p0, v0, v4

    goto :goto_0

    .line 133
    :cond_2
    mul-long v0, p0, p0

    rem-long/2addr v0, v4

    div-int/lit8 v2, p2, 0x2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/ads/internal/b/n;->a(JI)J

    move-result-wide v0

    rem-long/2addr v0, v4

    mul-long/2addr v0, p0

    rem-long p0, v0, v4

    goto :goto_0
.end method

.method private static a([Ljava/lang/String;II)Ljava/lang/String;
    .locals 3

    .prologue
    .line 101
    array-length v0, p0

    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 102
    const-string v0, "Unable to construct shingle"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    .line 103
    const-string v0, ""

    .line 111
    :goto_0
    return-object v0

    .line 105
    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    move v0, p1

    .line 106
    :goto_1
    add-int v2, p1, p2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_1

    .line 107
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 108
    const/16 v2, 0x20

    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 110
    :cond_1
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 111
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(IJLjava/lang/String;Ljava/util/PriorityQueue;)V
    .locals 7

    .prologue
    .line 71
    new-instance v1, Lcom/google/android/gms/ads/internal/b/o;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/ads/internal/b/o;-><init>(JLjava/lang/String;)V

    .line 72
    invoke-virtual {p4}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-ne v0, p0, :cond_1

    invoke-virtual {p4}, Ljava/util/PriorityQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/b/o;

    iget-wide v2, v0, Lcom/google/android/gms/ads/internal/b/o;->a:J

    iget-wide v4, v1, Lcom/google/android/gms/ads/internal/b/o;->a:J

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 82
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-virtual {p4, v1}, Ljava/util/PriorityQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 78
    invoke-virtual {p4, v1}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 79
    invoke-virtual {p4}, Ljava/util/PriorityQueue;->size()I

    move-result v0

    if-le v0, p0, :cond_0

    .line 80
    invoke-virtual {p4}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    goto :goto_0
.end method

.method public static a([Ljava/lang/String;IILjava/util/PriorityQueue;)V
    .locals 12

    .prologue
    .line 53
    const/4 v0, 0x0

    aget-object v0, p0, v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/b/l;->a(Ljava/lang/String;)I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x7fffffff

    add-long/2addr v0, v2

    const-wide/32 v2, 0x4000ffff

    rem-long v2, v0, v2

    const/4 v0, 0x1

    :goto_0
    add-int/lit8 v1, p2, 0x0

    if-ge v0, v1, :cond_0

    const-wide/32 v4, 0x1001fff

    mul-long/2addr v2, v4

    const-wide/32 v4, 0x4000ffff

    rem-long/2addr v2, v4

    aget-object v1, p0, v0

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/b/l;->a(Ljava/lang/String;)I

    move-result v1

    int-to-long v4, v1

    const-wide/32 v6, 0x7fffffff

    add-long/2addr v4, v6

    const-wide/32 v6, 0x4000ffff

    rem-long/2addr v4, v6

    add-long/2addr v2, v4

    const-wide/32 v4, 0x4000ffff

    rem-long/2addr v2, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, v0, p2}, Lcom/google/android/gms/ads/internal/b/n;->a([Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v2, v3, v0, p3}, Lcom/google/android/gms/ads/internal/b/n;->a(IJLjava/lang/String;Ljava/util/PriorityQueue;)V

    .line 56
    const-wide/32 v0, 0x1001fff

    add-int/lit8 v4, p2, -0x1

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/ads/internal/b/n;->a(JI)J

    move-result-wide v4

    .line 59
    const/4 v0, 0x1

    :goto_1
    array-length v1, p0

    sub-int/2addr v1, p2

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_1

    .line 60
    add-int/lit8 v1, v0, -0x1

    aget-object v1, p0, v1

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/b/l;->a(Ljava/lang/String;)I

    move-result v1

    add-int v6, v0, p2

    add-int/lit8 v6, v6, -0x1

    aget-object v6, p0, v6

    invoke-static {v6}, Lcom/google/android/gms/ads/internal/b/l;->a(Ljava/lang/String;)I

    move-result v6

    int-to-long v8, v1

    const-wide/32 v10, 0x7fffffff

    add-long/2addr v8, v10

    const-wide/32 v10, 0x4000ffff

    rem-long/2addr v8, v10

    mul-long/2addr v8, v4

    const-wide/32 v10, 0x4000ffff

    rem-long/2addr v8, v10

    const-wide/32 v10, 0x4000ffff

    add-long/2addr v2, v10

    sub-long/2addr v2, v8

    const-wide/32 v8, 0x4000ffff

    rem-long/2addr v2, v8

    const-wide/32 v8, 0x1001fff

    mul-long/2addr v2, v8

    const-wide/32 v8, 0x4000ffff

    rem-long/2addr v2, v8

    int-to-long v6, v6

    const-wide/32 v8, 0x7fffffff

    add-long/2addr v6, v8

    const-wide/32 v8, 0x4000ffff

    rem-long/2addr v6, v8

    add-long/2addr v2, v6

    const-wide/32 v6, 0x4000ffff

    rem-long/2addr v2, v6

    .line 63
    invoke-static {p0, v0, p2}, Lcom/google/android/gms/ads/internal/b/n;->a([Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v2, v3, v1, p3}, Lcom/google/android/gms/ads/internal/b/n;->a(IJLjava/lang/String;Ljava/util/PriorityQueue;)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 66
    :cond_1
    return-void
.end method
