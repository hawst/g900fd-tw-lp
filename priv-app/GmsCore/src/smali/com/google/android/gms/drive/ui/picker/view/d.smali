.class final Lcom/google/android/gms/drive/ui/picker/view/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/ui/picker/a/k;

.field final synthetic b:Z

.field final synthetic c:Lcom/google/android/gms/drive/ui/picker/view/DocListView;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Lcom/google/android/gms/drive/ui/picker/a/k;Z)V
    .locals 0

    .prologue
    .line 479
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->c:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->a:Lcom/google/android/gms/drive/ui/picker/a/k;

    iput-boolean p3, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 3

    .prologue
    .line 479
    check-cast p1, Lcom/google/android/gms/drive/k;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->c:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Z

    const-string v0, "DocListView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Query completed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/drive/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Lcom/google/android/gms/drive/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->c:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->a:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->b:Z

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Lcom/google/android/gms/drive/ui/picker/a/k;ZLcom/google/android/gms/drive/k;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/d;->c:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->b(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    goto :goto_0
.end method
