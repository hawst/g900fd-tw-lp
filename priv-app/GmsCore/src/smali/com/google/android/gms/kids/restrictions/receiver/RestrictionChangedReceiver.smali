.class public Lcom/google/android/gms/kids/restrictions/receiver/RestrictionChangedReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "restriction"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/kids/restrictions/receiver/RestrictionChangedReceiver;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 31
    const-string v0, "RestrictionChangedReceiver"

    const-string v1, "onReceive: action=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 42
    :goto_0
    return-void

    .line 36
    :cond_0
    new-instance v0, Lcom/google/android/gms/kids/restrictions/receiver/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/kids/restrictions/receiver/a;-><init>(Lcom/google/android/gms/kids/restrictions/receiver/RestrictionChangedReceiver;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/service/KidsApiService;->a(Landroid/content/Context;Lcom/google/android/gms/kids/common/c/b;)V

    goto :goto_0
.end method
