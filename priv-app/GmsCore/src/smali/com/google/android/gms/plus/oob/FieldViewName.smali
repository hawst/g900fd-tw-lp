.class public final Lcom/google/android/gms/plus/oob/FieldViewName;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field private d:Landroid/widget/EditText;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/EditText;

.field private g:Landroid/widget/EditText;

.field private h:Landroid/widget/EditText;

.field private i:Landroid/widget/TextView;

.field private j:Landroid/widget/TextView;

.field private k:Z

.field private l:I


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 110
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    .line 114
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 395
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/ae;->a(Landroid/content/Context;Landroid/view/View;)Z

    .line 396
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/oob/FieldViewName;Landroid/view/View;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/oob/FieldViewName;->a(Landroid/view/View;)V

    return-void
.end method

.method private b(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 399
    new-instance v0, Lcom/google/android/gms/plus/oob/m;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/plus/oob/m;-><init>(Lcom/google/android/gms/plus/oob/FieldViewName;Landroid/view/View;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 405
    return-void
.end method

.method private k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 373
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 374
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 375
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->b(Landroid/view/View;)V

    .line 392
    :goto_0
    return-void

    .line 377
    :cond_0
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->b(Landroid/view/View;)V

    goto :goto_0

    .line 382
    :cond_1
    invoke-static {}, Lcom/google/android/gms/plus/oob/af;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->a(Landroid/view/View;)V

    goto :goto_0

    .line 387
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method private n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 408
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->g()Lcom/google/android/gms/plus/service/v1whitelisted/models/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/k;->e()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 412
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->g()Lcom/google/android/gms/plus/service/v1whitelisted/models/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/k;->d()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()Z
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/l;->ej:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/l;->ei:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x0

    .line 144
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 146
    sget v0, Lcom/google/android/gms/j;->ml:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 147
    invoke-static {}, Lcom/google/android/gms/plus/oob/af;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 148
    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v1, :cond_2

    sget v1, Lcom/google/android/gms/l;->ev:I

    .line 151
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 158
    :goto_1
    sget v1, Lcom/google/android/gms/j;->gi:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    .line 159
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->n()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 160
    sget v1, Lcom/google/android/gms/j;->kV:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->o()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/gms/plus/oob/n;

    invoke-direct {v3, p0, v2}, Lcom/google/android/gms/plus/oob/n;-><init>(Lcom/google/android/gms/plus/oob/FieldViewName;B)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 164
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/google/android/gms/plus/oob/n;

    invoke-direct {v3, p0, v2}, Lcom/google/android/gms/plus/oob/n;-><init>(Lcom/google/android/gms/plus/oob/FieldViewName;B)V

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1, v7}, Landroid/widget/EditText;->setVisibility(I)V

    .line 173
    invoke-static {}, Lcom/google/android/gms/plus/oob/af;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    .line 180
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v1, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 183
    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v1, :cond_6

    sget v1, Lcom/google/android/gms/l;->er:I

    .line 186
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 187
    sget v0, Lcom/google/android/gms/j;->dd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    .line 189
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setFocusable(Z)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setInputType(I)V

    .line 196
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v3, Lcom/google/android/gms/p;->uU:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->n()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    const/4 v5, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->o()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 200
    sget v0, Lcom/google/android/gms/j;->fk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->i:Landroid/widget/TextView;

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    sget v0, Lcom/google/android/gms/j;->mk:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewName;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 206
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->d()Ljava/util/List;

    move-result-object v3

    .line 207
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v1, v2

    .line 208
    :goto_5
    if-ge v1, v4, :cond_8

    .line 209
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;

    .line 210
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->l()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->g()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 211
    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->c:Lcom/google/android/gms/plus/oob/e;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->h()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->d()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/d;->f()Ljava/util/List;

    move-result-object v0

    invoke-static {v5, v6, v7, v0}, Lcom/google/android/gms/plus/oob/af;->a(Lcom/google/android/gms/plus/oob/e;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Ljava/lang/CharSequence;

    move-result-object v0

    .line 216
    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 217
    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->j:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 208
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 148
    :cond_2
    sget v1, Lcom/google/android/gms/l;->eu:I

    goto/16 :goto_0

    .line 153
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->a:Z

    if-eqz v1, :cond_4

    sget v1, Lcom/google/android/gms/l;->et:I

    .line 156
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    invoke-virtual {v3, v1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    goto/16 :goto_1

    .line 153
    :cond_4
    sget v1, Lcom/google/android/gms/l;->es:I

    goto :goto_6

    .line 177
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    .line 178
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    goto/16 :goto_2

    .line 183
    :cond_6
    sget v1, Lcom/google/android/gms/l;->eq:I

    goto/16 :goto_3

    .line 193
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->h:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto/16 :goto_4

    .line 222
    :cond_8
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->l()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 6

    .prologue
    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;-><init>()V

    new-instance v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;

    invoke-direct {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;-><init>()V

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;->b:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;->c:Ljava/util/Set;

    const/4 v4, 0x3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;->a:Ljava/lang/String;

    iget-object v0, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;->c:Ljava/util/Set;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

    iget-object v4, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;->c:Ljava/util/Set;

    iget-object v5, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;->a:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/l;->b:Ljava/lang/String;

    invoke-direct {v0, v4, v5, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

    iput-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/AccountField$ValueEntity$NameEntity;

    iget-object v0, v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->d:Ljava/util/Set;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 303
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->dd:I

    if-ne v0, v1, :cond_0

    .line 304
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    if-nez v0, :cond_0

    .line 305
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->m()V

    .line 306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    .line 309
    :cond_0
    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 265
    const/4 v2, 0x5

    if-ne p2, v2, :cond_0

    .line 266
    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v2, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 268
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->requestFocus()Z

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->performClick()Z

    .line 271
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setEnabled(Z)V

    .line 274
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 296
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->dd:I

    if-ne v0, v1, :cond_0

    if-eqz p2, :cond_0

    .line 297
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/oob/FieldViewName;->onClick(Landroid/view/View;)V

    .line 299
    :cond_0
    return-void
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 237
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;

    if-nez v0, :cond_1

    .line 238
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 241
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;

    .line 242
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 243
    iget-boolean v0, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    .line 244
    iget v0, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->b:I

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->l:I

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    iget-object v1, p1, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 247
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    if-eqz v0, :cond_0

    .line 248
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->m()V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 226
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/d;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    .line 227
    new-instance v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;

    invoke-direct {v2, v1}, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 228
    iget-boolean v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->k:Z

    iput-boolean v1, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->a:Z

    .line 229
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/ae;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v1

    :goto_0
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    :cond_0
    :goto_1
    iput v0, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->b:I

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->c:Ljava/lang/String;

    .line 231
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/plus/oob/FieldViewName$SavedState;->d:Ljava/lang/String;

    .line 232
    return-object v2

    :cond_1
    move v1, v0

    .line 229
    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->e:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->isFocused()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_1
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->f:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewName;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 285
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewName;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 287
    :cond_1
    const/4 v0, 0x0

    return v0
.end method
