.class public final Lcom/google/android/gms/appdatasearch/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/net/Uri;

.field public final c:Ljava/util/List;

.field public d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

.field public e:Z

.field public f:Landroid/accounts/Account;

.field public g:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

.field public h:Ljava/lang/String;

.field private final i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 193
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/ai;->i:Ljava/lang/String;

    .line 194
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/ai;->c:Ljava/util/List;

    .line 195
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 10

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/ai;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/ai;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/ai;->b:Landroid/net/Uri;

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/ai;->c:Ljava/util/List;

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/ai;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    invoke-interface {v4, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/ai;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-boolean v6, p0, Lcom/google/android/gms/appdatasearch/ai;->e:Z

    iget-object v7, p0, Lcom/google/android/gms/appdatasearch/ai;->f:Landroid/accounts/Account;

    iget-object v8, p0, Lcom/google/android/gms/appdatasearch/ai;->g:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v9, p0, Lcom/google/android/gms/appdatasearch/ai;->h:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;ZLandroid/accounts/Account;Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/ak;)Lcom/google/android/gms/appdatasearch/ai;
    .locals 2

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ai;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/ak;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 243
    return-object p0
.end method
