.class public final Lcom/google/android/gms/googlehelp/c/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/googlehelp/c/d;

.field private final b:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/c/d;Ljava/util/List;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/ad;->a:Lcom/google/android/gms/googlehelp/c/d;

    .line 40
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/ad;->b:Ljava/util/List;

    .line 41
    return-void
.end method

.method public static a(Ljava/util/List;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 68
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/ad;

    .line 69
    iget-object v1, v0, Lcom/google/android/gms/googlehelp/c/ad;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    const/4 v4, 0x1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/Pair;

    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/googlehelp/c/af;

    if-eqz v2, :cond_2

    invoke-interface {v2}, Lcom/google/android/gms/googlehelp/c/af;->b()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2}, Lcom/google/android/gms/googlehelp/c/af;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_2
    move v1, v3

    :goto_1
    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/c/ad;->a:Lcom/google/android/gms/googlehelp/c/d;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/c/d;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v0, v0, Lcom/google/android/gms/googlehelp/c/ad;->a:Lcom/google/android/gms/googlehelp/c/d;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/c/d;->setVisibility(I)V

    goto :goto_0

    .line 71
    :cond_4
    return-void

    :cond_5
    move v1, v4

    goto :goto_1
.end method
