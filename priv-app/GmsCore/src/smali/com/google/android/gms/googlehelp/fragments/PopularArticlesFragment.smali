.class public Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:[Landroid/view/View;

.field private b:[Landroid/widget/TextView;

.field private c:[Landroid/view/View;

.field private d:[Landroid/view/View;

.field private e:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private f:Lcom/google/android/gms/googlehelp/common/t;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 22
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 27
    new-array v0, v1, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->b:[Landroid/widget/TextView;

    .line 28
    new-array v0, v1, [Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->c:[Landroid/view/View;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;)Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->e:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;)Lcom/google/android/gms/googlehelp/common/t;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->f:Lcom/google/android/gms/googlehelp/common/t;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/googlehelp/common/t;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->f:Lcom/google/android/gms/googlehelp/common/t;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/t;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 72
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->f:Lcom/google/android/gms/googlehelp/common/t;

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->f:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/t;->a()I

    move-result v0

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v4, v2

    :goto_0
    if-ge v4, v3, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->f:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/googlehelp/common/t;->a(I)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->b:[Landroid/widget/TextView;

    aget-object v5, v5, v4

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->g()Z

    move-result v5

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a:[Landroid/view/View;

    aget-object v5, v5, v4

    new-instance v6, Lcom/google/android/gms/googlehelp/fragments/m;

    invoke-direct {v6, p0, v0, v4}, Lcom/google/android/gms/googlehelp/fragments/m;-><init>(Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;Lcom/google/android/gms/googlehelp/common/k;I)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->c:[Landroid/view/View;

    aget-object v5, v5, v4

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->h()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v5, v0}, Landroid/view/View;->setVisibility(I)V

    if-lez v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->d:[Landroid/view/View;

    aget-object v0, v0, v4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a:[Landroid/view/View;

    aget-object v0, v0, v4

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->i()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a:[Landroid/view/View;

    aget-object v5, v5, v4

    new-instance v6, Lcom/google/android/gms/googlehelp/fragments/n;

    invoke-direct {v6, p0, v0, v4}, Lcom/google/android/gms/googlehelp/fragments/n;-><init>(Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;Lcom/google/android/gms/googlehelp/common/k;I)V

    invoke-virtual {v5, v6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v3

    :goto_3
    if-ge v0, v7, :cond_6

    if-lez v0, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->d:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a:[Landroid/view/View;

    aget-object v2, v2, v0

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 74
    :cond_6
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->f:Lcom/google/android/gms/googlehelp/common/t;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->f:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/t;->b()V

    .line 139
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->e:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 69
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x5

    const/4 v2, 0x0

    .line 36
    sget v0, Lcom/google/android/gms/l;->cj:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 37
    sget v0, Lcom/google/android/gms/j;->jb:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->pf:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 39
    new-array v0, v6, [Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->iy:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    sget v1, Lcom/google/android/gms/j;->iz:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v7

    sget v1, Lcom/google/android/gms/j;->iA:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v8

    sget v1, Lcom/google/android/gms/j;->iB:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v9

    const/4 v1, 0x4

    sget v4, Lcom/google/android/gms/j;->iC:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a:[Landroid/view/View;

    move v1, v2

    .line 47
    :goto_0
    if-ge v1, v6, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->c:[Landroid/view/View;

    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a:[Landroid/view/View;

    aget-object v4, v4, v1

    sget v5, Lcom/google/android/gms/j;->hC:I

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v0, v1

    .line 49
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->b:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->a:[Landroid/view/View;

    aget-object v0, v0, v1

    sget v5, Lcom/google/android/gms/j;->hD:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 47
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 53
    :cond_0
    new-array v0, v6, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object v1, v0, v2

    sget v1, Lcom/google/android/gms/j;->iD:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v7

    sget v1, Lcom/google/android/gms/j;->iE:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v8

    sget v1, Lcom/google/android/gms/j;->iF:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v9

    const/4 v1, 0x4

    sget v2, Lcom/google/android/gms/j;->iG:I

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/PopularArticlesFragment;->d:[Landroid/view/View;

    .line 61
    return-object v3
.end method
