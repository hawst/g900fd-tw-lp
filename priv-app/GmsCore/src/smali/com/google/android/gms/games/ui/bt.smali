.class public final Lcom/google/android/gms/games/ui/bt;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/m;


# instance fields
.field private a:[Landroid/widget/BaseAdapter;

.field private b:Landroid/database/DataSetObserver;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 58
    return-void
.end method

.method public varargs constructor <init>([Landroid/widget/BaseAdapter;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bt;->a([Landroid/widget/BaseAdapter;)V

    .line 48
    return-void
.end method

.method private a(I)Lcom/google/android/gms/games/ui/bv;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v3, v1

    move v1, v0

    move v2, v0

    .line 110
    :goto_0
    if-ge v2, v3, :cond_1

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v4, v0, v2

    .line 112
    invoke-virtual {v4}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v0

    add-int/2addr v0, v1

    .line 113
    if-ge p1, v0, :cond_0

    .line 114
    new-instance v0, Lcom/google/android/gms/games/ui/bv;

    sub-int v1, p1, v1

    invoke-direct {v0, v4, v1}, Lcom/google/android/gms/games/ui/bv;-><init>(Landroid/widget/BaseAdapter;I)V

    .line 119
    :goto_1
    return-object v0

    .line 117
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    .line 118
    goto :goto_0

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private varargs a([Landroid/widget/BaseAdapter;)V
    .locals 3

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/gms/games/ui/bu;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/bu;-><init>(Lcom/google/android/gms/games/ui/bt;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bt;->b:Landroid/database/DataSetObserver;

    .line 80
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    .line 82
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/bt;->b:Landroid/database/DataSetObserver;

    invoke-virtual {v1, v2}, Landroid/widget/BaseAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 82
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 3

    .prologue
    .line 189
    const/4 v1, 0x1

    .line 190
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->areAllItemsEnabled()Z

    move-result v2

    and-int/2addr v1, v2

    .line 190
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_0
    return v1
.end method

.method public final getCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 89
    move v1, v0

    .line 90
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 91
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->getCount()I

    move-result v2

    add-int/2addr v1, v2

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return v1
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bt;->a(I)Lcom/google/android/gms/games/ui/bv;

    move-result-object v0

    .line 181
    if-nez v0, :cond_0

    .line 182
    const/4 v0, 0x0

    .line 184
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bv;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Lcom/google/android/gms/games/ui/bv;->b:I

    invoke-virtual {v1, v0, p2, p3}, Landroid/widget/BaseAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bt;->a(I)Lcom/google/android/gms/games/ui/bv;

    move-result-object v0

    .line 125
    if-nez v0, :cond_0

    .line 126
    const/4 v0, 0x0

    .line 128
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bv;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Lcom/google/android/gms/games/ui/bv;->b:I

    invoke-virtual {v1, v0}, Landroid/widget/BaseAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 133
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 147
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bt;->a(I)Lcom/google/android/gms/games/ui/bv;

    move-result-object v2

    .line 148
    if-nez v2, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v1, v0

    .line 153
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 154
    iget-object v3, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v3, v3, v0

    .line 155
    iget-object v4, v2, Lcom/google/android/gms/games/ui/bv;->a:Landroid/widget/BaseAdapter;

    if-eq v3, v4, :cond_2

    .line 156
    invoke-virtual {v3}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v3

    add-int/2addr v1, v3

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 160
    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/games/ui/bv;->a:Landroid/widget/BaseAdapter;

    iget v2, v2, Lcom/google/android/gms/games/ui/bv;->b:I

    invoke-virtual {v0, v2}, Landroid/widget/BaseAdapter;->getItemViewType(I)I

    move-result v0

    .line 163
    if-ltz v0, :cond_0

    .line 164
    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 171
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bt;->a(I)Lcom/google/android/gms/games/ui/bv;

    move-result-object v0

    .line 172
    if-nez v0, :cond_0

    .line 173
    const/4 v0, 0x0

    .line 175
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bv;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Lcom/google/android/gms/games/ui/bv;->b:I

    invoke-virtual {v1, v0, p2, p3}, Landroid/widget/BaseAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public final getViewTypeCount()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 138
    move v1, v0

    .line 139
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 140
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/BaseAdapter;->getViewTypeCount()I

    move-result v2

    add-int/2addr v1, v2

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_0
    return v1
.end method

.method public final isEnabled(I)Z
    .locals 2

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bt;->a(I)Lcom/google/android/gms/games/ui/bv;

    move-result-object v0

    .line 199
    if-nez v0, :cond_0

    .line 200
    const/4 v0, 0x0

    .line 202
    :goto_0
    return v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bv;->a:Landroid/widget/BaseAdapter;

    iget v0, v0, Lcom/google/android/gms/games/ui/bv;->b:I

    invoke-virtual {v1, v0}, Landroid/widget/BaseAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 208
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v0, v0, v1

    .line 210
    instance-of v2, v0, Lcom/google/android/gms/games/ui/m;

    if-eqz v2, :cond_0

    .line 211
    check-cast v0, Lcom/google/android/gms/games/ui/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/m;->m()V

    .line 208
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 214
    :cond_1
    return-void
.end method

.method public final n()V
    .locals 3

    .prologue
    .line 219
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bt;->a:[Landroid/widget/BaseAdapter;

    aget-object v0, v0, v1

    .line 221
    instance-of v2, v0, Lcom/google/android/gms/games/ui/m;

    if-eqz v2, :cond_0

    .line 222
    check-cast v0, Lcom/google/android/gms/games/ui/m;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/m;->n()V

    .line 219
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 225
    :cond_1
    return-void
.end method
