.class public Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/a;


# instance fields
.field final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lcom/google/android/gms/search/global/a;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;->a:I

    .line 60
    return-void
.end method

.method constructor <init>(I)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;->a:I

    .line 75
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/a;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;->CREATOR:Lcom/google/android/gms/search/global/a;

    invoke-static {p0, p1}, Lcom/google/android/gms/search/global/a;->a(Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Request;Landroid/os/Parcel;)V

    .line 88
    return-void
.end method
