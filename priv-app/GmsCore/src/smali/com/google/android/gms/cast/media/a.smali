.class final Lcom/google/android/gms/cast/media/a;
.super Landroid/support/v7/c/f;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast/c/m;


# static fields
.field private static final B:Lcom/google/android/gms/cast/c/aa;

.field private static final D:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final i:Lcom/google/android/gms/cast/e/h;

.field private static j:Lcom/google/android/gms/cast/media/a;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;

.field private static final m:Z

.field private static final n:I

.field private static final o:[Ljava/lang/String;


# instance fields
.field private final A:Ljava/util/List;

.field private final C:Ljava/util/Map;

.field private final p:Ljava/util/List;

.field private q:Z

.field private final r:Lcom/google/android/gms/cast/c/z;

.field private final s:Landroid/content/BroadcastReceiver;

.field private final t:Ljava/util/Map;

.field private final u:Ljava/util/Map;

.field private final v:Lcom/google/android/gms/cast/e/g;

.field private final w:Ljava/util/List;

.field private x:Z

.field private final y:Lcom/google/android/gms/cast/c/h;

.field private final z:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 88
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastMediaRouteProvider"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    .line 91
    sget-object v0, Lcom/google/android/gms/cast/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/cast/media/a;->k:Ljava/lang/String;

    .line 92
    sget-object v0, Lcom/google/android/gms/cast/a/d;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/cast/media/a;->l:Ljava/lang/String;

    .line 93
    sget-object v0, Lcom/google/android/gms/cast/a/d;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/cast/media/a;->m:Z

    .line 94
    sget-object v0, Lcom/google/android/gms/cast/a/d;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/media/a;->n:I

    .line 99
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "android.media.intent.action.PAUSE"

    aput-object v1, v0, v4

    const/4 v1, 0x1

    const-string v2, "android.media.intent.action.RESUME"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "android.media.intent.action.STOP"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "android.media.intent.action.SEEK"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "android.media.intent.action.GET_STATUS"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "android.media.intent.action.START_SESSION"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "android.media.intent.action.GET_SESSION_STATUS"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "android.media.intent.action.END_SESSION"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/cast/media/a;->o:[Ljava/lang/String;

    .line 145
    sget-object v0, Lcom/google/android/gms/cast/media/a;->k:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "applicationId cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string v1, "com.google.android.gms.cast.CATEGORY_CAST"

    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    const-string v3, "[A-F0-9]+"

    invoke-virtual {v1, v3}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid application ID: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    const-string v0, "/"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/c/aa;->a(Ljava/lang/String;)Lcom/google/android/gms/cast/c/aa;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/media/a;->B:Lcom/google/android/gms/cast/c/aa;

    .line 158
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/cast/media/a;->D:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/16 v5, 0x12

    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 164
    invoke-direct {p0, p1}, Landroid/support/v7/c/f;-><init>(Landroid/content/Context;)V

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->w:Ljava/util/List;

    .line 168
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->C:Ljava/util/Map;

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->C:Ljava/util/Map;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->he:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->C:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->hf:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->C:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->hh:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->C:Ljava/util/Map;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->hg:I

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    new-instance v0, Lcom/google/android/gms/cast/e/g;

    const-string v1, "CastMediaRouteProvider"

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/cast/e/g;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->v:Lcom/google/android/gms/cast/e/g;

    .line 181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->t:Ljava/util/Map;

    .line 183
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    .line 184
    new-instance v0, Lcom/google/android/gms/cast/c/h;

    const-string v1, "gms_cast_mrp"

    invoke-direct {v0, p1, v1, p0}, Lcom/google/android/gms/cast/c/h;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/cast/c/m;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->y:Lcom/google/android/gms/cast/c/h;

    .line 186
    new-instance v0, Lcom/google/android/gms/cast/media/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/media/b;-><init>(Lcom/google/android/gms/cast/media/a;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->r:Lcom/google/android/gms/cast/c/z;

    .line 264
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    .line 266
    sget-object v0, Lcom/google/android/gms/cast/a/c;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 267
    new-instance v0, Lcom/google/android/gms/cast/c/af;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/c/af;-><init>(Landroid/content/Context;)V

    .line 268
    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->r:Lcom/google/android/gms/cast/c/z;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/c/q;->a(Lcom/google/android/gms/cast/c/z;)V

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 271
    :cond_0
    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/cast/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 272
    new-instance v0, Lcom/google/android/gms/cast/c/e;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/c/e;-><init>(Landroid/content/Context;)V

    .line 273
    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->r:Lcom/google/android/gms/cast/c/z;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/c/q;->a(Lcom/google/android/gms/cast/c/z;)V

    .line 274
    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastNearbyDeviceScanner enabled."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 277
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 278
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "There aren\'t any device scanners registered."

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 282
    :cond_2
    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 283
    new-instance v0, Lcom/google/android/gms/cast/media/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/media/c;-><init>(Lcom/google/android/gms/cast/media/a;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->s:Landroid/content/BroadcastReceiver;

    .line 297
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.checkin.CHECKIN_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 298
    iget-object v1, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/a;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 303
    :goto_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    .line 304
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->j()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->A:Ljava/util/List;

    .line 306
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->h()V

    .line 307
    return-void

    .line 300
    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/a;->s:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/cast/media/z;)Landroid/support/v7/c/c;
    .locals 13

    .prologue
    .line 501
    const/4 v3, 0x0

    .line 502
    const/4 v2, 0x0

    .line 503
    const/4 v1, 0x0

    .line 505
    iget-object v5, p1, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 506
    iget-object v6, p1, Lcom/google/android/gms/cast/media/z;->b:Ljava/util/Set;

    .line 509
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/y;

    .line 510
    if-eqz v0, :cond_0

    .line 511
    iget-boolean v1, v0, Lcom/google/android/gms/cast/media/y;->c:Z

    .line 512
    iget-object v0, v0, Lcom/google/android/gms/cast/media/y;->a:Lcom/google/android/gms/cast/b/e;

    .line 513
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 514
    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->g()D

    move-result-wide v2

    const-wide/high16 v8, 0x4034000000000000L    # 20.0

    mul-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    .line 515
    const/4 v0, 0x1

    move v3, v2

    move v2, v0

    .line 520
    :cond_0
    invoke-virtual {p0, v5}, Lcom/google/android/gms/cast/media/a;->b(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;

    move-result-object v7

    .line 521
    iget-object v0, p1, Lcom/google/android/gms/cast/media/z;->c:Ljava/lang/String;

    .line 523
    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v4

    if-nez v4, :cond_3

    .line 525
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->k()Lcom/google/android/gms/cast/c/e;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/c/e;->a(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    sget v8, Lcom/google/android/gms/p;->dR:I

    invoke-virtual {v4, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 527
    sget v0, Lcom/google/android/gms/p;->dU:I

    .line 531
    :goto_0
    iget-object v4, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    .line 541
    :goto_1
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 542
    invoke-virtual {v5, v8}, Lcom/google/android/gms/cast/CastDevice;->a(Landroid/os/Bundle;)V

    .line 544
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 545
    invoke-interface {v6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aa;

    .line 546
    new-instance v10, Landroid/content/IntentFilter;

    invoke-direct {v10}, Landroid/content/IntentFilter;-><init>()V

    .line 547
    iget-object v0, v0, Lcom/google/android/gms/cast/c/aa;->a:Ljava/lang/String;

    .line 548
    invoke-virtual {v10, v0}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 551
    const-string v11, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-static {v0, v11}, Lcom/google/android/gms/cast/media/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_1

    const-string v11, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-static {v0, v11}, Lcom/google/android/gms/cast/media/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 554
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->A:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :goto_3
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/IntentFilter;

    .line 555
    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 529
    :cond_2
    sget v0, Lcom/google/android/gms/p;->dT:I

    goto :goto_0

    .line 533
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 534
    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->e()Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 536
    :cond_4
    iget-object v4, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    sget v8, Lcom/google/android/gms/p;->dV:I

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v0, v9, v10

    invoke-virtual {v4, v8, v9}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 558
    :cond_5
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 561
    :cond_6
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v6, "buildRouteDescriptorForDevice: id=%s, description=%s, connecting=%b, volume=%d"

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v7, v10, v11

    const/4 v11, 0x2

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v0, v6, v10}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 564
    new-instance v0, Landroid/support/v7/c/d;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5, v7}, Landroid/support/v7/c/d;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v6, "status"

    invoke-virtual {v5, v6, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v5, "connecting"

    invoke-virtual {v4, v5, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v4, "volumeHandling"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v2, "volume"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v2, "volumeMax"

    const/16 v3, 0x14

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v2, "playbackType"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v9}, Landroid/support/v7/c/d;->a(Ljava/util/Collection;)Landroid/support/v7/c/d;

    move-result-object v0

    iget-object v1, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v2, "extras"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object v1, v0, Landroid/support/v7/c/d;->b:Ljava/util/ArrayList;

    if-eqz v1, :cond_7

    iget-object v1, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    const-string v2, "controlFilters"

    iget-object v3, v0, Landroid/support/v7/c/d;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_7
    new-instance v1, Landroid/support/v7/c/c;

    iget-object v2, v0, Landroid/support/v7/c/d;->a:Landroid/os/Bundle;

    iget-object v0, v0, Landroid/support/v7/c/d;->b:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v0, v3}, Landroid/support/v7/c/c;-><init>(Landroid/os/Bundle;Ljava/util/List;B)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/media/o;)Lcom/google/android/gms/cast/b/e;
    .locals 12

    .prologue
    const/4 v11, 0x1

    .line 86
    iget-object v3, p1, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v10

    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/y;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/cast/media/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/media/y;-><init>(Lcom/google/android/gms/cast/media/a;)V

    sget-object v1, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v2, "creating CastDeviceController for %s"

    new-array v4, v11, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v3, v4, v5

    invoke-virtual {v1, v2, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v8, Lcom/google/android/gms/cast/media/d;

    invoke-direct {v8, p0, v0, v3}, Lcom/google/android/gms/cast/media/d;-><init>(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/media/y;Lcom/google/android/gms/cast/CastDevice;)V

    iget-object v1, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    iget-object v2, p0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    const v4, 0x6768a8

    const-string v5, "gms_cast_mrp"

    const-wide/16 v6, 0x0

    const-string v9, "MRP"

    invoke-static/range {v1 .. v9}, Lcom/google/android/gms/cast/b/e;->a(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/gms/cast/CastDevice;ILjava/lang/String;JLcom/google/android/gms/cast/b/m;Ljava/lang/String;)Lcom/google/android/gms/cast/b/e;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/cast/media/y;->a:Lcom/google/android/gms/cast/b/e;

    iput-boolean v11, v0, Lcom/google/android/gms/cast/media/y;->c:Z

    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    invoke-interface {v1, v10, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->v:Lcom/google/android/gms/cast/e/g;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/e/g;->a()V

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/android/gms/cast/media/y;->a:Lcom/google/android/gms/cast/b/e;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/cast/media/a;
    .locals 3

    .prologue
    .line 149
    const-class v1, Lcom/google/android/gms/cast/media/a;

    monitor-enter v1

    .line 150
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/media/a;->j:Lcom/google/android/gms/cast/media/a;

    if-nez v0, :cond_0

    .line 151
    new-instance v0, Lcom/google/android/gms/cast/media/a;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/cast/media/a;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/cast/media/a;->j:Lcom/google/android/gms/cast/media/a;

    .line 153
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/media/a;->j:Lcom/google/android/gms/cast/media/a;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/CastDevice;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/media/a;->c(Lcom/google/android/gms/cast/CastDevice;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/media/o;Z)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 86
    iget-object v2, p1, Lcom/google/android/gms/cast/media/o;->a:Lcom/google/android/gms/cast/CastDevice;

    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "releaseDeviceControllerFor CastDeviceController for %s"

    new-array v3, v5, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/y;

    if-eqz v0, :cond_4

    iget-object v1, v0, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/y;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v4, "disposing CastDeviceController for %s"

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v6

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/cast/media/y;->a:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v1, p2}, Lcom/google/android/gms/cast/b/e;->a(Z)V

    iget-boolean v1, v0, Lcom/google/android/gms/cast/media/y;->d:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/cast/media/y;->a:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/b/e;->a()Lcom/google/android/gms/cast/CastDevice;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/cast/c/q;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/c/q;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-boolean v1, v0, Lcom/google/android/gms/cast/media/y;->b:Z

    if-eqz v1, :cond_2

    iget-boolean v1, v0, Lcom/google/android/gms/cast/media/y;->d:Z

    if-eqz v1, :cond_3

    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/media/a;->c(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/cast/media/a;->v:Lcom/google/android/gms/cast/e/g;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/e/g;->b()V

    iget-object v0, v0, Lcom/google/android/gms/cast/media/y;->a:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->p()V

    :cond_4
    return-void

    :cond_5
    sget-object v1, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, v0, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    sget-object v2, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Still connected to by CastRouteController %s"

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->f()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 579
    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Lcom/google/android/gms/cast/e/h;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/media/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/media/a;)Lcom/google/android/gms/cast/c/h;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->y:Lcom/google/android/gms/cast/c/h;

    return-object v0
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/gms/cast/media/a;->l:Ljava/lang/String;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Removing Device: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->t:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/aa;

    .line 370
    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/media/aa;->a(Lcom/google/android/gms/cast/CastDevice;)V

    goto :goto_0

    .line 373
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Not removing %s. It\'s still in use."

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 375
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->h()V

    .line 376
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->t:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic d()Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/gms/cast/media/a;->D:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/google/android/gms/cast/media/a;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/cast/media/a;)Ljava/util/List;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->w:Ljava/util/List;

    return-object v0
.end method

.method static synthetic f()I
    .locals 1

    .prologue
    .line 86
    sget v0, Lcom/google/android/gms/cast/media/a;->n:I

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/cast/media/a;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->h()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/cast/media/a;)Lcom/google/android/gms/cast/c/e;
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->k()Lcom/google/android/gms/cast/c/e;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g()Z
    .locals 1

    .prologue
    .line 86
    sget-boolean v0, Lcom/google/android/gms/cast/media/a;->m:Z

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/cast/media/a;)Lcom/google/android/gms/cast/c/z;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->r:Lcom/google/android/gms/cast/c/z;

    return-object v0
.end method

.method private h()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 379
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 380
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 383
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->t:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/z;

    .line 384
    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/media/a;->a(Lcom/google/android/gms/cast/media/z;)Landroid/support/v7/c/c;

    move-result-object v7

    .line 385
    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 386
    iget-object v0, v0, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v3

    .line 387
    goto :goto_0

    .line 389
    :cond_0
    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 393
    :cond_1
    if-eqz v1, :cond_2

    .line 394
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "local device found removing all cast nearby routes - %d"

    new-array v6, v3, [Ljava/lang/Object;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v0, v1, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 395
    invoke-interface {v4, v5}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    .line 397
    :cond_2
    new-instance v0, Landroid/support/v7/c/l;

    invoke-direct {v0}, Landroid/support/v7/c/l;-><init>()V

    invoke-virtual {v0, v4}, Landroid/support/v7/c/l;->a(Ljava/util/Collection;)Landroid/support/v7/c/l;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/c/l;->a()Landroid/support/v7/c/k;

    move-result-object v0

    invoke-static {}, Landroid/support/v7/c/v;->a()V

    iget-object v1, p0, Landroid/support/v7/c/f;->g:Landroid/support/v7/c/k;

    if-eq v1, v0, :cond_3

    iput-object v0, p0, Landroid/support/v7/c/f;->g:Landroid/support/v7/c/k;

    iget-boolean v0, p0, Landroid/support/v7/c/f;->h:Z

    if-nez v0, :cond_3

    iput-boolean v3, p0, Landroid/support/v7/c/f;->h:Z

    iget-object v0, p0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    invoke-virtual {v0, v3}, Landroid/support/v7/c/h;->sendEmptyMessage(I)Z

    .line 399
    :cond_3
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "published %d routes"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 400
    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/cast/media/a;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->s:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method private i()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 415
    .line 420
    new-instance v6, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 423
    iget-object v0, p0, Landroid/support/v7/c/f;->e:Landroid/support/v7/c/e;

    .line 424
    if-eqz v0, :cond_3

    .line 425
    invoke-virtual {v0}, Landroid/support/v7/c/e;->b()Z

    move-result v1

    if-nez v1, :cond_0

    move v1, v2

    .line 427
    :goto_0
    invoke-virtual {v0}, Landroid/support/v7/c/e;->a()Landroid/support/v7/c/t;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Landroid/support/v7/c/t;->a()Ljava/util/List;

    move-result-object v7

    .line 430
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    move v5, v3

    move v4, v3

    .line 431
    :goto_1
    if-ge v5, v8, :cond_4

    .line 432
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 433
    const-string v9, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 437
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/media/a;->l:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/cast/c/aa;

    invoke-direct {v4}, Lcom/google/android/gms/cast/c/aa;-><init>()V

    const-string v9, "android.media.intent.category.REMOTE_PLAYBACK"

    iput-object v9, v4, Lcom/google/android/gms/cast/c/aa;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/cast/c/aa;->b:Ljava/lang/String;

    .line 439
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v2

    .line 431
    :goto_2
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v0

    goto :goto_1

    :cond_0
    move v1, v3

    .line 425
    goto :goto_0

    .line 443
    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_2

    :cond_1
    const-string v9, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK/"

    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "com.google.android.gms.cast.CATEGORY_CAST"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_2

    const-string v9, "com.google.android.gms.cast.CATEGORY_CAST/"

    invoke-virtual {v0, v9}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_f

    .line 452
    :cond_2
    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/cast/c/aa;->a(Ljava/lang/String;)Lcom/google/android/gms/cast/c/aa;

    move-result-object v0

    .line 453
    iget-object v4, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v2

    .line 456
    goto :goto_2

    :catch_1
    move-exception v0

    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v3

    move v4, v3

    .line 461
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/cast/media/a;->x:Z

    if-eqz v0, :cond_6

    .line 462
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    sget-object v4, Lcom/google/android/gms/cast/media/a;->B:Lcom/google/android/gms/cast/c/aa;

    invoke-interface {v0, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    sget-object v4, Lcom/google/android/gms/cast/media/a;->B:Lcom/google/android/gms/cast/c/aa;

    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    move v4, v2

    .line 468
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    move v5, v2

    .line 470
    :goto_3
    if-eqz v4, :cond_7

    if-eqz v5, :cond_a

    .line 471
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/q;

    .line 472
    iget-object v7, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    invoke-static {v7, v0}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/c/q;)V

    goto :goto_4

    :cond_8
    move v5, v3

    .line 468
    goto :goto_3

    .line 474
    :cond_9
    iput-boolean v3, p0, Lcom/google/android/gms/cast/media/a;->q:Z

    .line 477
    :cond_a
    if-eqz v5, :cond_b

    .line 478
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->y:Lcom/google/android/gms/cast/c/h;

    iget-object v5, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/c/h;->a(Ljava/util/Set;)V

    .line 481
    :cond_b
    if-eqz v4, :cond_e

    .line 482
    iget-boolean v0, p0, Lcom/google/android/gms/cast/media/a;->q:Z

    if-eqz v0, :cond_c

    .line 483
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v4, "changing the scan mode, passive=%b"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v2, v3

    invoke-virtual {v0, v4, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 484
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/q;

    .line 485
    iget-object v3, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/cast/c/q;Z)V

    goto :goto_5

    .line 489
    :cond_c
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v4, "starting the scan: %d, passive=%b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/q;

    .line 491
    iget-object v4, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/cast/service/CastDeviceScannerIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/c/q;Z)V

    goto :goto_6

    .line 494
    :cond_d
    iput-boolean v2, p0, Lcom/google/android/gms/cast/media/a;->q:Z

    .line 497
    :cond_e
    return-void

    :cond_f
    move v0, v4

    goto/16 :goto_2
.end method

.method private j()Ljava/util/List;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 585
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 588
    new-instance v3, Landroid/content/IntentFilter;

    invoke-direct {v3}, Landroid/content/IntentFilter;-><init>()V

    .line 589
    const-string v1, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 590
    const-string v1, "android.media.intent.action.PLAY"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 591
    const-string v1, "http"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 592
    const-string v1, "https"

    invoke-virtual {v3, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 594
    iget-object v1, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v4, Lcom/google/android/gms/c;->e:I

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v4

    .line 595
    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    .line 596
    :try_start_0
    invoke-virtual {v3, v6}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 595
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 596
    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 598
    :cond_0
    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    sget-object v1, Lcom/google/android/gms/cast/media/a;->o:[Ljava/lang/String;

    array-length v3, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 601
    new-instance v5, Landroid/content/IntentFilter;

    invoke-direct {v5}, Landroid/content/IntentFilter;-><init>()V

    .line 602
    const-string v6, "android.media.intent.category.REMOTE_PLAYBACK"

    invoke-virtual {v5, v6}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 603
    invoke-virtual {v5, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 604
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 600
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 607
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 608
    const-string v1, "com.google.android.gms.cast.CATEGORY_CAST_REMOTE_PLAYBACK"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addCategory(Ljava/lang/String;)V

    .line 609
    const-string v1, "com.google.android.gms.cast.ACTION_SYNC_STATUS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 610
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 612
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->C:Ljava/util/Map;

    return-object v0
.end method

.method private k()Lcom/google/android/gms/cast/c/e;
    .locals 3

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/q;

    .line 1021
    instance-of v2, v0, Lcom/google/android/gms/cast/c/e;

    if-eqz v2, :cond_0

    .line 1022
    check-cast v0, Lcom/google/android/gms/cast/c/e;

    .line 1025
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Landroid/support/v7/c/j;
    .locals 3

    .prologue
    .line 624
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->t:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/z;

    .line 625
    if-nez v0, :cond_0

    .line 627
    const/4 v0, 0x0

    .line 630
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/cast/media/o;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/gms/cast/media/o;-><init>(Lcom/google/android/gms/cast/media/a;Lcom/google/android/gms/cast/CastDevice;B)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/cast/media/o;
    .locals 4

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/y;

    .line 321
    iget-object v0, v0, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 322
    iget-boolean v1, v0, Lcom/google/android/gms/cast/media/o;->l:Z

    if-eqz v1, :cond_2

    iget-boolean v1, v0, Lcom/google/android/gms/cast/media/o;->m:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    .line 327
    :goto_1
    return-object v0

    .line 322
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .line 327
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/c/e;)V
    .locals 4

    .prologue
    .line 404
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "in onDiscoveryRequestChanged: request=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 405
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->i()V

    .line 406
    if-nez p1, :cond_0

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->r:Lcom/google/android/gms/cast/c/z;

    invoke-interface {v0}, Lcom/google/android/gms/cast/c/z;->a()V

    .line 409
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 4

    .prologue
    .line 1111
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onDeviceRejectedByFilter: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1112
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/media/a;->c(Lcom/google/android/gms/cast/CastDevice;)V

    .line 1113
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1117
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onDeviceFilterError for %s: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1118
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/q;

    .line 1119
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c/q;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 1121
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 1096
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aa;

    .line 1097
    sget-object v2, Lcom/google/android/gms/cast/media/a;->k:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/aa;->b:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/cast/e/i;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1099
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "This device is not allowed to discover Cast receivers that support mirroring."

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1107
    :goto_0
    return-void

    .line 1104
    :cond_1
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "onDeviceAcceptedByFilter: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object p1, v2, v5

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 1105
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "addOrUpdateDevice: device: %s applicationName: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v5

    aput-object p3, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->t:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/z;

    if-eqz v0, :cond_5

    sget-object v1, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v2, "merging in criteria for existing device %s"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, v0, Lcom/google/android/gms/cast/media/z;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/cast/c/aa;

    iget-object v3, v0, Lcom/google/android/gms/cast/media/z;->b:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v3, v0, Lcom/google/android/gms/cast/media/z;->b:Ljava/util/Set;

    invoke-interface {v3, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iput-object p3, v0, Lcom/google/android/gms/cast/media/z;->c:Ljava/lang/String;

    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->w:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/aa;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v0, p1, p3}, Lcom/google/android/gms/cast/media/aa;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->t:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/cast/media/z;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/gms/cast/media/z;-><init>(Lcom/google/android/gms/cast/CastDevice;Ljava/util/Set;Ljava/lang/String;)V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 1106
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->h()V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/media/aa;)V
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 331
    sget-object v0, Lcom/google/android/gms/cast/media/a;->i:Lcom/google/android/gms/cast/e/h;

    const-string v1, "setDiscoverRemoteDisplays() enable=%b"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 332
    iget-boolean v0, p0, Lcom/google/android/gms/cast/media/a;->x:Z

    if-eq v0, p1, :cond_1

    .line 333
    iput-boolean p1, p0, Lcom/google/android/gms/cast/media/a;->x:Z

    .line 334
    if-nez p1, :cond_0

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->z:Ljava/util/Set;

    sget-object v1, Lcom/google/android/gms/cast/media/a;->k:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 337
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->i()V

    .line 339
    :cond_1
    return-void
.end method

.method public final b(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 616
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 617
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/a;->k()Lcom/google/android/gms/cast/c/e;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/c/e;->a(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;

    move-result-object v0

    .line 619
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/cast/media/aa;)V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/cast/media/a;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 317
    return-void
.end method
