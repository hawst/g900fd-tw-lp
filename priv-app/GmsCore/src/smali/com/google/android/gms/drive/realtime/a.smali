.class public final Lcom/google/android/gms/drive/realtime/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/u;


# instance fields
.field private final a:Lcom/google/android/gms/drive/auth/g;

.field private final b:Landroid/content/Context;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/a;->c:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/a;->a:Lcom/google/android/gms/drive/auth/g;

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/a;->b:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 34
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/server/a/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/a;->a:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/g;->b()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/a;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/a/a;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/a;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/a;->c:Ljava/lang/String;

    return-object v0

    .line 37
    :catch_0
    move-exception v0

    .line 40
    const-string v1, "AuthorizedAppOAuthTokenProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get OAuth token; using old token: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/a;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_0
.end method
