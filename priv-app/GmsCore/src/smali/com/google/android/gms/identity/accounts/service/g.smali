.class public final Lcom/google/android/gms/identity/accounts/service/g;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/identity/accounts/service/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/identity/accounts/service/h;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/identity/accounts/service/g;->a:Lcom/google/android/gms/identity/accounts/service/h;

    .line 28
    return-void
.end method


# virtual methods
.method public final n(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 34
    const-string v0, "Callbacks must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    if-lez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Client version must be set to a positive value."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 37
    const-string v0, "Calling package must be set."

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/g;->b:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 41
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 46
    :cond_0
    const-string v0, "Arguments must not be null."

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/g;->a:Lcom/google/android/gms/identity/accounts/service/h;

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 49
    return-void

    :cond_1
    move v0, v1

    .line 35
    goto :goto_0
.end method
