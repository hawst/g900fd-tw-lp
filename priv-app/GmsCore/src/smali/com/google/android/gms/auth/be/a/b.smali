.class public final Lcom/google/android/gms/auth/be/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;

.field private static b:Ljava/lang/ref/WeakReference;


# instance fields
.field private final c:I

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/auth/be/a/a;

.field private final f:Ljava/lang/Object;

.field private final g:Ljava/util/Random;

.field private final h:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "FactoryReset"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "FactoryResetProtectionManager"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    .line 50
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/gms/auth/be/a/b;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILcom/google/android/gms/auth/be/a/a;)V
    .locals 6

    .prologue
    .line 108
    sget-object v2, Lcom/google/android/gms/auth/b/a;->ac:Lcom/google/android/gms/common/a/d;

    new-instance v5, Ljava/security/SecureRandom;

    invoke-direct {v5}, Ljava/security/SecureRandom;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/be/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/a/d;ILcom/google/android/gms/auth/be/a/a;Ljava/util/Random;)V

    .line 114
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/a/d;ILcom/google/android/gms/auth/be/a/a;Ljava/util/Random;)V
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->d:Landroid/content/Context;

    .line 124
    iput-object p2, p0, Lcom/google/android/gms/auth/be/a/b;->h:Lcom/google/android/gms/common/a/d;

    .line 125
    iput p3, p0, Lcom/google/android/gms/auth/be/a/b;->c:I

    .line 126
    iput-object p4, p0, Lcom/google/android/gms/auth/be/a/b;->e:Lcom/google/android/gms/auth/be/a/a;

    .line 127
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->g:Ljava/util/Random;

    .line 128
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->f:Ljava/lang/Object;

    .line 129
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/a/b;
    .locals 4

    .prologue
    .line 54
    const-class v1, Lcom/google/android/gms/auth/be/a/b;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/be/a/b;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/be/a/b;

    .line 55
    if-nez v0, :cond_0

    .line 56
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    .line 57
    new-instance v0, Lcom/google/android/gms/auth/be/a/b;

    invoke-static {p0}, Lcom/google/android/gms/auth/be/a/a;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/a/a;

    move-result-object v3

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/gms/auth/be/a/b;-><init>(Landroid/content/Context;ILcom/google/android/gms/auth/be/a/a;)V

    .line 61
    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v2, Lcom/google/android/gms/auth/be/a/b;->b:Ljava/lang/ref/WeakReference;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    :cond_0
    monitor-exit v1

    return-object v0

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Lcom/google/android/gms/auth/be/a/a/b;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 229
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v2, v2

    if-nez v2, :cond_1

    .line 230
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Invalid ProfileBlock."

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    move v0, v1

    .line 255
    :goto_0
    return v0

    .line 234
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    array-length v2, v2

    iget-object v3, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v3, v3

    iget v4, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    mul-int/2addr v3, v4

    if-eq v2, v3, :cond_3

    .line 235
    :cond_2
    sget-object v0, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Invalid SALT."

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    move v0, v1

    .line 236
    goto :goto_0

    .line 240
    :cond_3
    :try_start_0
    const-string v2, "SHA-256"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v3

    .line 241
    invoke-static {p1}, Lcom/google/android/gms/auth/be/a/b;->b(Ljava/lang/String;)[B

    move-result-object v4

    move v2, v1

    .line 242
    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v5, v5

    if-ge v2, v5, :cond_4

    .line 243
    invoke-virtual {v3}, Ljava/security/MessageDigest;->reset()V

    .line 244
    iget-object v5, p0, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    iget v6, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    mul-int/2addr v6, v2

    iget v7, p0, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    invoke-virtual {v3, v5, v6, v7}, Ljava/security/MessageDigest;->update([BII)V

    .line 245
    invoke-virtual {v3, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 246
    iget-object v5, p0, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    aget-object v5, v5, v2

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    invoke-static {v5, v6}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 247
    sget-object v2, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Check successful for account: %s!"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 251
    :catch_0
    move-exception v2

    .line 252
    sget-object v3, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v4, "Error when checking account presence."

    invoke-virtual {v3, v4, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 254
    :cond_4
    sget-object v2, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Check failed for account: %s."

    new-array v0, v0, [Ljava/lang/Object;

    aput-object p1, v0, v1

    invoke-static {v3, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    move v0, v1

    .line 255
    goto :goto_0

    .line 242
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private b(Ljava/util/List;)Lcom/google/android/gms/auth/be/a/a/b;
    .locals 8

    .prologue
    const/16 v7, 0x20

    .line 316
    const-string v0, "SHA-256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    .line 319
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 320
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 321
    mul-int/lit8 v0, v4, 0x20

    new-array v0, v0, [B

    .line 322
    iget-object v1, p0, Lcom/google/android/gms/auth/be/a/b;->g:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 323
    new-instance v5, Lcom/google/android/gms/auth/be/a/a/b;

    invoke-direct {v5}, Lcom/google/android/gms/auth/be/a/a/b;-><init>()V

    .line 324
    iget v1, p0, Lcom/google/android/gms/auth/be/a/b;->c:I

    iput v1, v5, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    .line 325
    iput v7, v5, Lcom/google/android/gms/auth/be/a/a/b;->b:I

    .line 326
    iput-object v0, v5, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    .line 327
    new-array v0, v4, [[B

    iput-object v0, v5, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    .line 328
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 329
    invoke-virtual {v2}, Ljava/security/MessageDigest;->reset()V

    .line 330
    iget-object v0, v5, Lcom/google/android/gms/auth/be/a/a/b;->c:[B

    mul-int/lit8 v6, v1, 0x20

    invoke-virtual {v2, v0, v6, v7}, Ljava/security/MessageDigest;->update([BII)V

    .line 331
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/be/a/b;->b(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->update([B)V

    .line 332
    iget-object v0, v5, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v6

    aput-object v6, v0, v1

    .line 328
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 334
    :cond_0
    return-object v5
.end method

.method private static b(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 81
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 82
    :catch_0
    move-exception v0

    .line 83
    sget-object v1, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Encoding error."

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 84
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->e:Lcom/google/android/gms/auth/be/a/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->e:Lcom/google/android/gms/auth/be/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/a/a;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/b/a;->ad:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/a/b;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 188
    sget-object v1, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Factory reset protection is not supported!"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 214
    :goto_0
    return v0

    .line 192
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 193
    sget-object v1, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Check failed! Id is null."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 197
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/auth/be/a/b;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 198
    :try_start_0
    sget-object v2, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v4, "Checking account: %s."

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 200
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/b;->e:Lcom/google/android/gms/auth/be/a/a;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/a/a;->b()Lcom/google/android/gms/auth/be/a/a/a;

    move-result-object v2

    .line 201
    if-eqz v2, :cond_2

    iget-object v4, v2, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    if-eqz v4, :cond_2

    iget-object v4, v2, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    array-length v4, v4

    if-nez v4, :cond_3

    .line 204
    :cond_2
    sget-object v1, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Check failed! Invalid DataBlockContainer."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 205
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 207
    :cond_3
    :try_start_1
    iget-object v4, v2, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    array-length v5, v4

    move v2, v0

    :goto_1
    if-ge v2, v5, :cond_5

    aget-object v6, v4, v2

    .line 208
    invoke-static {v6, p1}, Lcom/google/android/gms/auth/be/a/b;->a(Lcom/google/android/gms/auth/be/a/a/b;Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 209
    monitor-exit v3

    move v0, v1

    goto :goto_0

    .line 207
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 212
    :cond_5
    sget-object v1, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Check failed! Account %s wasn\'t installed on any profile!"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 214
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 266
    sget-object v2, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Updating data block with "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " account ids."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;)V

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/a/b;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 268
    sget-object v1, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Update failed! FactoryResetProtection is unsupported."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 309
    :goto_0
    return v0

    .line 272
    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/auth/be/a/b;->f:Ljava/lang/Object;

    monitor-enter v4

    .line 274
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/b;->e:Lcom/google/android/gms/auth/be/a/a;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/be/a/a;->b()Lcom/google/android/gms/auth/be/a/a/a;

    move-result-object v2

    .line 275
    if-nez v2, :cond_6

    .line 276
    new-instance v2, Lcom/google/android/gms/auth/be/a/a/a;

    invoke-direct {v2}, Lcom/google/android/gms/auth/be/a/a/a;-><init>()V

    move-object v3, v2

    .line 278
    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/be/a/b;->b(Ljava/util/List;)Lcom/google/android/gms/auth/be/a/a/b;

    move-result-object v5

    .line 279
    iget-object v2, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    if-nez v2, :cond_3

    .line 281
    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/auth/be/a/a/b;

    const/4 v6, 0x0

    aput-object v5, v2, v6

    iput-object v2, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    .line 304
    :cond_1
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/b;->e:Lcom/google/android/gms/auth/be/a/a;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/be/a/a;->a(Lcom/google/android/gms/auth/be/a/a/a;)J

    move-result-wide v2

    .line 305
    sget-object v5, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v6, "Write complete, result: %d."

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 306
    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-ltz v2, :cond_2

    move v0, v1

    :cond_2
    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_3
    move v2, v0

    .line 285
    :goto_3
    :try_start_2
    iget-object v6, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    array-length v6, v6

    if-ge v2, v6, :cond_5

    .line 286
    iget-object v6, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    aget-object v6, v6, v2

    iget v6, v6, Lcom/google/android/gms/auth/be/a/a/b;->a:I

    iget v7, p0, Lcom/google/android/gms/auth/be/a/b;->c:I

    if-ne v6, v7, :cond_4

    .line 287
    iget-object v6, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    aput-object v5, v6, v2

    move v2, v1

    .line 292
    :goto_4
    if-nez v2, :cond_1

    .line 297
    iget-object v2, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    array-length v2, v2

    .line 298
    add-int/lit8 v6, v2, 0x1

    new-array v6, v6, [Lcom/google/android/gms/auth/be/a/a/b;

    .line 299
    iget-object v7, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {v7, v8, v6, v9, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 300
    aput-object v5, v6, v2

    .line 301
    iput-object v6, v3, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 307
    :catch_0
    move-exception v1

    .line 308
    :goto_5
    :try_start_3
    sget-object v2, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "Update failed!"

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 309
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 285
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 307
    :catch_1
    move-exception v1

    goto :goto_5

    :cond_5
    move v2, v0

    goto :goto_4

    :cond_6
    move-object v3, v2

    goto :goto_1
.end method

.method public final b()Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 144
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/a/b;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 145
    sget-object v0, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Factory reset protection is not supported!"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    move v0, v1

    .line 155
    :goto_0
    return v0

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 150
    sget-object v0, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Factory reset protection is disabled by GservicesFlag!"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    move v0, v1

    .line 151
    goto :goto_0

    .line 154
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/auth/be/a/b;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 155
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/a/b;->e:Lcom/google/android/gms/auth/be/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/a/a;->b()Lcom/google/android/gms/auth/be/a/a/a;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v3, v0, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lcom/google/android/gms/auth/be/a/a/a;->a:[Lcom/google/android/gms/auth/be/a/a/b;

    array-length v4, v3

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_3

    aget-object v5, v3, v0

    iget-object v6, v5, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    if-eqz v6, :cond_2

    iget-object v5, v5, Lcom/google/android/gms/auth/be/a/a/b;->d:[[B

    array-length v5, v5

    if-lez v5, :cond_2

    sget-object v0, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Factory Reset Protection challenge found!"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 156
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 155
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    :try_start_1
    sget-object v0, Lcom/google/android/gms/auth/be/a/b;->a:Lcom/google/android/gms/auth/d/a;

    const-string v3, "No Factory Reset Protection challenges are present."

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_2
.end method
