.class public Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/common/widget/h;
.implements Lcom/google/android/gms/usagereporting/f;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Lcom/google/android/gms/common/widget/SwitchBar;

.field private c:Landroid/widget/CompoundButton;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;Z)V
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/widget/SwitchBar;->setChecked(Z)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->d:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->d:Landroid/widget/TextView;

    if-eqz p1, :cond_3

    sget v0, Lcom/google/android/gms/p;->zl:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    return-void

    :cond_3
    sget v0, Lcom/google/android/gms/p;->zk:I

    goto :goto_0
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 107
    if-eqz p1, :cond_3

    const/4 v0, 0x0

    .line 108
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    if-eqz v1, :cond_0

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    .line 111
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    if-eqz v1, :cond_1

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v1, v0}, Landroid/widget/CompoundButton;->setVisibility(I)V

    .line 114
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->d:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 117
    :cond_2
    return-void

    .line 107
    :cond_3
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private b(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 98
    invoke-virtual {p0, p1}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 99
    if-nez v0, :cond_0

    .line 100
    const-string v0, "UsageReportingActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find view: id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    const/4 v0, 0x0

    .line 103
    :cond_0
    return-object v0
.end method

.method private b(Z)V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 181
    const-string v0, "UsageReportingActivity"

    const-string v1, "It is not connected to the server."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :goto_0
    return-void

    .line 184
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    .line 186
    :goto_1
    sget-object v1, Lcom/google/android/gms/usagereporting/a;->b:Lcom/google/android/gms/usagereporting/d;

    iget-object v2, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a:Lcom/google/android/gms/common/api/v;

    new-instance v3, Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;

    invoke-direct {v3, v0}, Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;-><init>(I)V

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/usagereporting/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/usagereporting/UsageReportingOptInOptions;)Lcom/google/android/gms/common/api/am;

    goto :goto_0

    .line 184
    :cond_1
    const/4 v0, 0x2

    goto :goto_1
.end method

.method private e()V
    .locals 2

    .prologue
    .line 133
    sget-object v0, Lcom/google/android/gms/usagereporting/a;->b:Lcom/google/android/gms/usagereporting/d;

    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/usagereporting/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/usagereporting/settings/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/usagereporting/settings/a;-><init>(Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 145
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 148
    const-string v0, "usage-reporting"

    invoke-static {p0, v0}, Lcom/google/android/gsf/i;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 149
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->startActivity(Landroid/content/Intent;)V

    .line 150
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->e()V

    .line 202
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 3

    .prologue
    .line 219
    const-string v0, "UsageReportingActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not connect to UsageReporting service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a(Z)V

    .line 221
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/widget/SwitchBar;Z)V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0, p2}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b(Z)V

    .line 177
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 206
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a(Z)V

    .line 207
    sget-object v0, Lcom/google/android/gms/usagereporting/a;->b:Lcom/google/android/gms/usagereporting/d;

    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1, p0}, Lcom/google/android/gms/usagereporting/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/usagereporting/f;)Lcom/google/android/gms/common/api/am;

    .line 208
    invoke-direct {p0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->e()V

    .line 209
    return-void
.end method

.method public final f_(I)V
    .locals 3

    .prologue
    .line 213
    const-string v0, "UsageReportingActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConnectionSuspended: cause="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a(Z)V

    .line 215
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    if-ne p1, v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b(Z)V

    .line 169
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->e:Landroid/widget/TextView;

    if-ne p1, v0, :cond_1

    .line 170
    invoke-direct {p0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->f()V

    .line 172
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 50
    sget v0, Lcom/google/android/gms/l;->fU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->setContentView(I)V

    .line 52
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 53
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 55
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 56
    sget v1, Lcom/google/android/gms/h;->I:I

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->b(I)V

    .line 59
    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    .line 60
    iput-object v2, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    .line 61
    iput-object v2, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->d:Landroid/widget/TextView;

    .line 62
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 63
    sget v0, Lcom/google/android/gms/j;->sn:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/SwitchBar;

    iput-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Lcom/google/android/gms/common/widget/h;)V

    .line 76
    :cond_1
    :goto_0
    const v0, 0x1020010

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->e:Landroid/widget/TextView;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->e:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->zj:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->e:Landroid/widget/TextView;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/usagereporting/a;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 88
    return-void

    .line 69
    :cond_3
    const v0, 0x1020001

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    .line 70
    sget v0, Lcom/google/android/gms/j;->cF:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->d:Landroid/widget/TextView;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 93
    sget v1, Lcom/google/android/gms/m;->F:I

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 94
    const/4 v0, 0x1

    return v0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 192
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->rs:I

    if-ne v0, v1, :cond_0

    .line 193
    invoke-direct {p0}, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->f()V

    .line 194
    const/4 v0, 0x1

    .line 196
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 156
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/usagereporting/settings/UsageReportingActivity;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 161
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 162
    return-void
.end method
