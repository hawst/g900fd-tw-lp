.class final Lcom/google/android/gms/update/i;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/SystemUpdateActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/SystemUpdateActivity;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/gms/update/i;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/gms/update/i;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->a(Lcom/google/android/gms/update/SystemUpdateActivity;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/update/i;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v2}, Lcom/google/android/gms/update/SystemUpdateActivity;->b(Lcom/google/android/gms/update/SystemUpdateActivity;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 156
    :cond_0
    const-string v0, "SystemUpdateActivity"

    const-string v1, "screen turned off during countdown; installing immediately"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 157
    const v0, 0x3112a

    const-string v1, "activity-countdown-screen-off-install"

    invoke-static {v0, v1}, Landroid/util/EventLog;->writeEvent(ILjava/lang/String;)I

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/update/i;->a:Lcom/google/android/gms/update/SystemUpdateActivity;

    invoke-static {v0}, Lcom/google/android/gms/update/SystemUpdateActivity;->c(Lcom/google/android/gms/update/SystemUpdateActivity;)V

    .line 161
    :cond_1
    return-void
.end method
