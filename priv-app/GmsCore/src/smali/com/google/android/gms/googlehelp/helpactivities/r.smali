.class public Lcom/google/android/gms/googlehelp/helpactivities/r;
.super Lcom/google/android/gms/common/activity/b;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/googlehelp/common/HelpConfig;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/common/activity/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final f()Lcom/google/android/gms/googlehelp/common/HelpConfig;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 37
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 40
    if-eqz p1, :cond_0

    const-string v0, "EXTRA_HELP_CONFIG"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 41
    const-string v0, "EXTRA_HELP_CONFIG"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v0

    .line 45
    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 46
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We must have an App Package Name set at this point."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    const-string v0, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    const-string v3, "EXTRA_SHOW_CONTACT_CARD_ONLY"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Z)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    goto :goto_0

    :cond_1
    const-string v0, "EXTRA_HELP_CONFIG"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EXTRA_HELP_CONFIG"

    invoke-virtual {v2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No HelpConfig provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move-object v0, v1

    .line 41
    goto :goto_1

    .line 48
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->J()I

    move-result v1

    if-nez v1, :cond_6

    sget v1, Lcom/google/android/gms/q;->G:I

    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->setTheme(I)V

    .line 53
    invoke-super {p0, p1}, Lcom/google/android/gms/common/activity/b;->onCreate(Landroid/os/Bundle;)V

    .line 55
    sget v1, Lcom/google/android/gms/p;->oW:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->setTitle(Ljava/lang/CharSequence;)V

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/support/v7/app/a;->a(Z)V

    invoke-virtual {v2, v5}, Landroid/support/v7/app/a;->b(Z)V

    sget v3, Lcom/google/android/gms/p;->oW:I

    invoke-virtual {v2, v3}, Landroid/support/v7/app/a;->c(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    :try_start_0
    invoke-virtual {v3, v0, v4}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/v7/app/a;->a(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, v0}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_3
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->L()Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->K()I

    move-result v3

    invoke-direct {v1, v3}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v2, v1}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0x15

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Landroid/app/ActivityManager$TaskDescription;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->K()I

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 56
    :cond_5
    return-void

    .line 51
    :cond_6
    sget v1, Lcom/google/android/gms/q;->H:I

    goto :goto_2

    .line 55
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_4
    const-string v3, "GOOGLEHELP_UpEnabledActivity"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ": "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_4
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 131
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 132
    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 133
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;->finish()V

    .line 134
    const/4 v0, 0x1

    .line 137
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/activity/b;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
