.class public final Lcom/google/android/gms/identity/accounts/security/j;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)Lcom/google/android/gms/identity/accounts/security/a;
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v0, 0x0

    .line 59
    const-string v1, "identity_accountDataSharedPrefs"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/gms/identity/accounts/security/j;->b(Landroid/content/Context;)Lcom/google/android/gms/identity/accounts/security/i;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v1, v0

    .line 60
    :goto_0
    if-nez v1, :cond_2

    .line 102
    :cond_0
    :goto_1
    return-object v0

    .line 59
    :cond_1
    new-instance v1, Lcom/google/android/gms/identity/accounts/security/b;

    const-string v4, "AES"

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/identity/accounts/security/b;-><init>(Landroid/content/SharedPreferences;Lcom/google/android/gms/identity/accounts/security/i;Ljava/lang/String;)V

    goto :goto_0

    .line 64
    :cond_2
    new-instance v5, Ljava/security/SecureRandom;

    invoke-direct {v5}, Ljava/security/SecureRandom;-><init>()V

    .line 65
    iget-object v2, v1, Lcom/google/android/gms/identity/accounts/security/b;->b:Ljavax/crypto/SecretKey;

    if-nez v2, :cond_3

    const-string v2, "messageKey"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/identity/accounts/security/b;->a(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/identity/accounts/security/b;->b:Ljavax/crypto/SecretKey;

    :cond_3
    iget-object v2, v1, Lcom/google/android/gms/identity/accounts/security/b;->b:Ljavax/crypto/SecretKey;

    .line 66
    if-nez v2, :cond_4

    .line 67
    invoke-static {v5}, Lcom/google/android/gms/identity/accounts/security/j;->a(Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 68
    const-string v3, "Message key must not be null."

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v2, v1, Lcom/google/android/gms/identity/accounts/security/b;->b:Ljavax/crypto/SecretKey;

    const-string v3, "messageKey"

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/identity/accounts/security/b;->a(Ljava/lang/String;Ljavax/crypto/SecretKey;)V

    .line 71
    :cond_4
    iget-object v3, v1, Lcom/google/android/gms/identity/accounts/security/b;->a:Ljavax/crypto/SecretKey;

    if-nez v3, :cond_5

    const-string v3, "macKey"

    invoke-virtual {v1, v3}, Lcom/google/android/gms/identity/accounts/security/b;->a(Ljava/lang/String;)Ljavax/crypto/SecretKey;

    move-result-object v3

    iput-object v3, v1, Lcom/google/android/gms/identity/accounts/security/b;->a:Ljavax/crypto/SecretKey;

    :cond_5
    iget-object v4, v1, Lcom/google/android/gms/identity/accounts/security/b;->a:Ljavax/crypto/SecretKey;

    .line 72
    if-nez v4, :cond_6

    .line 73
    invoke-static {v5}, Lcom/google/android/gms/identity/accounts/security/j;->a(Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;

    move-result-object v4

    .line 74
    const-string v3, "MAC key must not be null."

    invoke-static {v4, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v4, v1, Lcom/google/android/gms/identity/accounts/security/b;->a:Ljavax/crypto/SecretKey;

    const-string v3, "macKey"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/identity/accounts/security/b;->a(Ljava/lang/String;Ljavax/crypto/SecretKey;)V

    .line 79
    :cond_6
    :try_start_0
    const-string v1, "AES/CBC/PKCS5Padding"

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 94
    :try_start_1
    const-string v1, "HmacSHA512"

    invoke-static {v1}, Ljavax/crypto/Mac;->getInstance(Ljava/lang/String;)Ljavax/crypto/Mac;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v6

    .line 102
    new-instance v0, Lcom/google/android/gms/identity/accounts/security/a;

    new-instance v1, Lcom/google/android/gms/identity/accounts/security/d;

    invoke-direct {v1, v3}, Lcom/google/android/gms/identity/accounts/security/d;-><init>(Ljavax/crypto/Cipher;)V

    new-instance v3, Lcom/google/android/gms/identity/accounts/security/e;

    invoke-direct {v3, v6}, Lcom/google/android/gms/identity/accounts/security/e;-><init>(Ljavax/crypto/Mac;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/identity/accounts/security/a;-><init>(Lcom/google/android/gms/identity/accounts/security/d;Ljavax/crypto/SecretKey;Lcom/google/android/gms/identity/accounts/security/e;Ljavax/crypto/SecretKey;Ljava/security/SecureRandom;)V

    goto :goto_1

    .line 80
    :catch_0
    move-exception v1

    .line 81
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 82
    const-string v2, "AccountDataUtil"

    const-string v3, "Can\'t find AES algorithm."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 85
    :catch_1
    move-exception v1

    .line 86
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 87
    const-string v2, "AccountDataUtil"

    const-string v3, "Can\'t find padding."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    .line 95
    :catch_2
    move-exception v1

    .line 96
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 97
    const-string v2, "AccountDataUtil"

    const-string v3, "Can\'t find MAC algorithm."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1
.end method

.method private static a(Ljava/security/SecureRandom;)Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    .line 243
    const/16 v0, 0x20

    new-array v0, v0, [B

    .line 244
    invoke-virtual {p0, v0}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 245
    new-instance v1, Ljavax/crypto/spec/SecretKeySpec;

    const-string v2, "AES"

    invoke-direct {v1, v0, v2}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method private static b(Landroid/content/Context;)Lcom/google/android/gms/identity/accounts/security/i;
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    .line 120
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_2

    .line 121
    invoke-static {p0}, Lcom/google/android/gms/identity/accounts/security/j;->c(Landroid/content/Context;)Ljava/security/KeyPair;

    move-result-object v1

    .line 122
    if-nez v1, :cond_1

    .line 142
    :cond_0
    :goto_0
    return-object v0

    .line 128
    :cond_1
    :try_start_0
    const-string v2, "RSA/ECB/PKCS1Padding"

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v2

    .line 140
    new-instance v0, Lcom/google/android/gms/identity/accounts/security/h;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/identity/accounts/security/h;-><init>(Ljavax/crypto/Cipher;Ljava/security/KeyPair;)V

    goto :goto_0

    .line 129
    :catch_0
    move-exception v1

    .line 130
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 131
    const-string v2, "AccountDataUtil"

    const-string v3, "Can\'t find required algorithm."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 134
    :catch_1
    move-exception v1

    .line 135
    const-string v2, "AccountDataUtil"

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 136
    const-string v2, "AccountDataUtil"

    const-string v3, "Can\'t find required algorithm."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 142
    :cond_2
    sget-object v0, Lcom/google/android/gms/identity/accounts/security/i;->a:Lcom/google/android/gms/identity/accounts/security/i;

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;)Ljava/security/KeyPair;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v8, 0x3

    .line 149
    :try_start_0
    const-string v0, "AndroidKeyStore"

    invoke-static {v0}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;
    :try_end_0
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 158
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v3, v0}, Ljava/security/KeyStore;->load(Ljava/security/KeyStore$LoadStoreParameter;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/cert/CertificateException; {:try_start_1 .. :try_end_1} :catch_3

    .line 178
    :try_start_2
    const-string v0, "identity_accountWrapKey"

    invoke-virtual {v3, v0}, Ljava/security/KeyStore;->containsAlias(Ljava/lang/String;)Z
    :try_end_2
    .catch Ljava/security/KeyStoreException; {:try_start_2 .. :try_end_2} :catch_4

    move-result v4

    .line 187
    if-eqz v4, :cond_5

    .line 188
    :try_start_3
    const-string v0, "identity_accountWrapKey"

    const/4 v1, 0x0

    invoke-virtual {v3, v0, v1}, Ljava/security/KeyStore;->getEntry(Ljava/lang/String;Ljava/security/KeyStore$ProtectionParameter;)Ljava/security/KeyStore$Entry;

    move-result-object v0

    check-cast v0, Ljava/security/KeyStore$PrivateKeyEntry;

    .line 190
    new-instance v1, Ljava/security/KeyPair;

    invoke-virtual {v0}, Ljava/security/KeyStore$PrivateKeyEntry;->getCertificate()Ljava/security/cert/Certificate;

    move-result-object v5

    invoke-virtual {v5}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object v5

    invoke-virtual {v0}, Ljava/security/KeyStore$PrivateKeyEntry;->getPrivateKey()Ljava/security/PrivateKey;

    move-result-object v0

    invoke-direct {v1, v5, v0}, Ljava/security/KeyPair;-><init>(Ljava/security/PublicKey;Ljava/security/PrivateKey;)V
    :try_end_3
    .catch Ljava/security/KeyStoreException; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_3 .. :try_end_3} :catch_6
    .catch Ljava/security/UnrecoverableEntryException; {:try_start_3 .. :try_end_3} :catch_7

    move-object v0, v1

    .line 238
    :goto_0
    return-object v0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 152
    const-string v1, "AccountDataUtil"

    const-string v3, "Unexpected key store exception."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v0, v2

    .line 154
    goto :goto_0

    .line 159
    :catch_1
    move-exception v0

    .line 160
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 161
    const-string v1, "AccountDataUtil"

    const-string v3, "Error reading key store."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    move-object v0, v2

    .line 163
    goto :goto_0

    .line 164
    :catch_2
    move-exception v0

    .line 165
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 166
    const-string v1, "AccountDataUtil"

    const-string v3, "Can\'t find key store algorithm."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_2
    move-object v0, v2

    .line 168
    goto :goto_0

    .line 169
    :catch_3
    move-exception v0

    .line 170
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 171
    const-string v1, "AccountDataUtil"

    const-string v3, "Certificate problem in key store."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    move-object v0, v2

    .line 173
    goto :goto_0

    .line 179
    :catch_4
    move-exception v0

    .line 180
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 181
    const-string v1, "AccountDataUtil"

    const-string v3, "Unexpected key store exception."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    move-object v0, v2

    .line 183
    goto :goto_0

    .line 192
    :catch_5
    move-exception v0

    .line 193
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 194
    const-string v1, "AccountDataUtil"

    const-string v5, "Unexpected key store exception."

    invoke-static {v1, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 210
    :cond_5
    :goto_1
    if-eqz v4, :cond_6

    .line 212
    :try_start_4
    const-string v0, "identity_accountWrapKey"

    invoke-virtual {v3, v0}, Ljava/security/KeyStore;->deleteEntry(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/security/KeyStoreException; {:try_start_4 .. :try_end_4} :catch_8

    .line 221
    :cond_6
    new-instance v0, Lcom/google/android/gms/identity/accounts/security/g;

    const-string v1, "identity_accountWrapKey"

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/identity/accounts/security/g;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 223
    :try_start_5
    new-instance v1, Ljava/util/GregorianCalendar;

    invoke-direct {v1}, Ljava/util/GregorianCalendar;-><init>()V

    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    const/4 v4, 0x1

    const/16 v5, 0x64

    invoke-virtual {v3, v4, v5}, Ljava/util/Calendar;->add(II)V

    new-instance v4, Landroid/security/KeyPairGeneratorSpec$Builder;

    iget-object v5, v0, Lcom/google/android/gms/identity/accounts/security/g;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/security/KeyPairGeneratorSpec$Builder;-><init>(Landroid/content/Context;)V

    iget-object v5, v0, Lcom/google/android/gms/identity/accounts/security/g;->b:Ljava/lang/String;

    invoke-virtual {v4, v5}, Landroid/security/KeyPairGeneratorSpec$Builder;->setAlias(Ljava/lang/String;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v4

    new-instance v5, Ljavax/security/auth/x500/X500Principal;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "CN="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/identity/accounts/security/g;->b:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljavax/security/auth/x500/X500Principal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSubject(Ljavax/security/auth/x500/X500Principal;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v0

    sget-object v4, Ljava/math/BigInteger;->ONE:Ljava/math/BigInteger;

    invoke-virtual {v0, v4}, Landroid/security/KeyPairGeneratorSpec$Builder;->setSerialNumber(Ljava/math/BigInteger;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v0

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/security/KeyPairGeneratorSpec$Builder;->setStartDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v0

    invoke-virtual {v3}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/security/KeyPairGeneratorSpec$Builder;->setEndDate(Ljava/util/Date;)Landroid/security/KeyPairGeneratorSpec$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/security/KeyPairGeneratorSpec$Builder;->build()Landroid/security/KeyPairGeneratorSpec;

    move-result-object v0

    const-string v1, "RSA"

    const-string v3, "AndroidKeyStore"

    invoke-static {v1, v3}, Ljava/security/KeyPairGenerator;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyPairGenerator;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/security/KeyPairGenerator;->initialize(Ljava/security/spec/AlgorithmParameterSpec;)V

    invoke-virtual {v1}, Ljava/security/KeyPairGenerator;->generateKeyPair()Ljava/security/KeyPair;
    :try_end_5
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_5 .. :try_end_5} :catch_9
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_5 .. :try_end_5} :catch_a
    .catch Ljava/security/NoSuchProviderException; {:try_start_5 .. :try_end_5} :catch_b

    move-result-object v0

    goto/16 :goto_0

    .line 197
    :catch_6
    move-exception v0

    .line 198
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 199
    const-string v1, "AccountDataUtil"

    const-string v5, "Can\'t find algorithm in key store."

    invoke-static {v1, v5, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 203
    :catch_7
    move-exception v0

    const-string v0, "AccountDataUtil"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 204
    const-string v0, "AccountDataUtil"

    const-string v1, "Unrecoverable entry exception."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 213
    :catch_8
    move-exception v0

    .line 214
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 215
    const-string v1, "AccountDataUtil"

    const-string v3, "Unexpected key store exception."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_7
    move-object v0, v2

    .line 217
    goto/16 :goto_0

    .line 224
    :catch_9
    move-exception v0

    .line 225
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 226
    const-string v1, "AccountDataUtil"

    const-string v3, "Bad algorithm parameter."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_8
    move-object v0, v2

    .line 228
    goto/16 :goto_0

    .line 229
    :catch_a
    move-exception v0

    .line 230
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 231
    const-string v1, "AccountDataUtil"

    const-string v3, "Can\'t find algorithm in key store."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_9
    move-object v0, v2

    .line 233
    goto/16 :goto_0

    .line 234
    :catch_b
    move-exception v0

    .line 235
    const-string v1, "AccountDataUtil"

    invoke-static {v1, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 236
    const-string v1, "AccountDataUtil"

    const-string v3, "Can\'t find provider."

    invoke-static {v1, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_a
    move-object v0, v2

    .line 238
    goto/16 :goto_0
.end method
