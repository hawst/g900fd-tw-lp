.class public final Lcom/google/android/gms/smart_profile/n;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/TimeZone;

.field public static final b:Ljava/text/SimpleDateFormat;

.field public static final c:Ljava/text/SimpleDateFormat;

.field public static final d:Ljava/text/SimpleDateFormat;

.field public static final e:Ljava/text/SimpleDateFormat;

.field private static final f:[Ljava/text/SimpleDateFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 32
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/smart_profile/n;->a:Ljava/util/TimeZone;

    .line 38
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "--MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/gms/smart_profile/n;->b:Ljava/text/SimpleDateFormat;

    .line 40
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/gms/smart_profile/n;->c:Ljava/text/SimpleDateFormat;

    .line 42
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/gms/smart_profile/n;->d:Ljava/text/SimpleDateFormat;

    .line 44
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "MMMM dd, yyyy"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcom/google/android/gms/smart_profile/n;->e:Ljava/text/SimpleDateFormat;

    .line 49
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/text/SimpleDateFormat;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/smart_profile/n;->c:Ljava/text/SimpleDateFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/smart_profile/n;->d:Ljava/text/SimpleDateFormat;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyy-MM-dd\'T\'HH:mm\'Z\'"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    aput-object v2, v0, v1

    const/4 v1, 0x3

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    aput-object v2, v0, v1

    const/4 v1, 0x4

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd\'T\'HHmmssSSS\'Z\'"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd\'T\'HHmmss\'Z\'"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v3, "yyyyMMdd\'T\'HHmm\'Z\'"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/smart_profile/n;->f:[Ljava/text/SimpleDateFormat;

    return-void
.end method

.method private static a(JLjava/lang/String;)J
    .locals 2

    .prologue
    .line 160
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 163
    const-string v1, "UTC"

    iput-object v1, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 164
    invoke-virtual {v0, p0, p1}, Landroid/text/format/Time;->set(J)V

    .line 165
    iput-object p2, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 166
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public static a(JJJLjava/lang/String;ZLandroid/content/Context;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 65
    const/4 v8, 0x1

    .line 67
    invoke-static/range {p8 .. p8}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 68
    const/16 v8, 0x81

    .line 71
    :cond_0
    new-instance v10, Landroid/text/format/Time;

    move-object/from16 v0, p6

    invoke-direct {v10, v0}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 72
    move-wide/from16 v0, p4

    invoke-virtual {v10, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 73
    invoke-virtual/range {p8 .. p8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    .line 74
    const/4 v9, 0x0

    .line 75
    if-eqz p7, :cond_3

    .line 77
    move-object/from16 v0, p6

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/smart_profile/n;->a(JLjava/lang/String;)J

    move-result-wide v2

    .line 78
    move-object/from16 v0, p6

    invoke-static {p2, p3, v0}, Lcom/google/android/gms/smart_profile/n;->a(JLjava/lang/String;)J

    move-result-wide v4

    .line 79
    iget-wide v6, v10, Landroid/text/format/Time;->gmtoff:J

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/smart_profile/n;->a(JJJ)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 81
    invoke-virtual/range {p8 .. p8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    iget-wide v6, v10, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v4, p4

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/smart_profile/n;->b(JJJ)I

    move-result v2

    .line 83
    const/4 v3, 0x1

    if-ne v3, v2, :cond_2

    .line 84
    sget v2, Lcom/google/android/gms/p;->xo:I

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 89
    :goto_0
    if-nez v2, :cond_1

    .line 92
    new-instance v3, Ljava/util/Formatter;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v4, 0x32

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 93
    const/16 v8, 0x12

    const-string v9, "UTC"

    move-object/from16 v2, p8

    move-wide v4, p0

    move-wide v6, p2

    invoke-static/range {v2 .. v9}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v2

    .line 129
    :cond_1
    :goto_1
    return-object v2

    .line 85
    :cond_2
    const/4 v3, 0x2

    if-ne v3, v2, :cond_7

    .line 86
    sget v2, Lcom/google/android/gms/p;->xp:I

    invoke-virtual {v11, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 97
    :cond_3
    iget-wide v6, v10, Landroid/text/format/Time;->gmtoff:J

    move-wide v2, p0

    move-wide v4, p2

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/smart_profile/n;->a(JJJ)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v3, p8

    move-wide v4, p0

    move-wide v6, p2

    .line 99
    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/smart_profile/n;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v9

    .line 103
    invoke-virtual/range {p8 .. p8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    iget-wide v6, v10, Landroid/text/format/Time;->gmtoff:J

    move-wide v2, p0

    move-wide/from16 v4, p4

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/smart_profile/n;->b(JJJ)I

    move-result v2

    .line 105
    const/4 v3, 0x1

    if-ne v3, v2, :cond_4

    .line 107
    sget v2, Lcom/google/android/gms/p;->xr:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v9, v3, v4

    invoke-virtual {v11, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 109
    :cond_4
    const/4 v3, 0x2

    if-ne v3, v2, :cond_5

    .line 111
    sget v2, Lcom/google/android/gms/p;->xs:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v9, v3, v4

    invoke-virtual {v11, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 115
    :cond_5
    const/16 v8, 0x12

    move-object/from16 v3, p8

    move-wide v4, p0

    move-wide v6, p2

    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/smart_profile/n;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v2

    .line 117
    sget v3, Lcom/google/android/gms/p;->wP:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    const/4 v2, 0x1

    aput-object v9, v4, v2

    invoke-virtual {v11, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 123
    :cond_6
    or-int/lit8 v2, v8, 0x12

    const/high16 v3, 0x10000

    or-int/2addr v2, v3

    const v3, 0x8000

    or-int v8, v2, v3

    move-object/from16 v3, p8

    move-wide v4, p0

    move-wide v6, p2

    .line 125
    invoke-static/range {v3 .. v8}, Lcom/google/android/gms/smart_profile/n;->a(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_7
    move-object v2, v9

    goto/16 :goto_0
.end method

.method public static a(JLandroid/content/Context;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 138
    invoke-static {p0, p1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    invoke-static {p2, p0, p1, v3}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->xr:I

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 148
    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x12

    invoke-static {p2, p0, p1, v0}, Landroid/text/format/DateUtils;->formatDateTime(Landroid/content/Context;JI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;JJI)Ljava/lang/String;
    .locals 9

    .prologue
    .line 233
    and-int/lit16 v0, p5, 0x2000

    if-eqz v0, :cond_0

    .line 234
    const-string v7, "UTC"

    .line 238
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 239
    new-instance v1, Ljava/util/Formatter;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 240
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    .line 241
    invoke-static/range {v0 .. v7}, Landroid/text/format/DateUtils;->formatDateRange(Landroid/content/Context;Ljava/util/Formatter;JJILjava/lang/String;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    .line 243
    return-object v0

    .line 236
    :cond_0
    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 327
    invoke-static {p0, p1}, Lcom/google/android/gms/smart_profile/n;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;)Ljava/text/DateFormat;
    .locals 4

    .prologue
    .line 381
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/text/SimpleDateFormat;->getDateInstance(I)Ljava/text/DateFormat;

    move-result-object v0

    check-cast v0, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/text/SimpleDateFormat;->toPattern()Ljava/lang/String;

    move-result-object v2

    .line 385
    const-string v0, "de"

    invoke-virtual {v2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "[^Mm]*[Yy]+[^Mm]*"

    .line 389
    :goto_0
    :try_start_0
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v3, ""

    invoke-virtual {v2, v0, v3}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 391
    :goto_1
    return-object v0

    .line 385
    :cond_0
    const-string v0, "[^DdMm]*[Yy]+[^DdMm]*"

    goto :goto_0

    .line 391
    :catch_0
    move-exception v0

    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {p0}, Lcom/google/android/gms/smart_profile/n;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "MMMM dd"

    :goto_2
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object v0, v1

    goto :goto_1

    :cond_1
    const-string v0, "dd MMMM"

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 257
    new-instance v3, Ljava/text/ParsePosition;

    invoke-direct {v3, v0}, Ljava/text/ParsePosition;-><init>(I)V

    .line 259
    const-string v1, "--02-29"

    invoke-virtual {v1, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    sget-object v1, Lcom/google/android/gms/smart_profile/n;->a:Ljava/util/TimeZone;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1, v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->clear()V

    invoke-virtual {v1, v2, v0}, Ljava/util/Calendar;->set(II)V

    const/4 v0, 0x2

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    const/4 v0, 0x5

    const/16 v2, 0x1d

    invoke-virtual {v1, v0, v2}, Ljava/util/Calendar;->set(II)V

    move-object v0, v1

    .line 284
    :goto_0
    return-object v0

    .line 265
    :cond_0
    sget-object v1, Lcom/google/android/gms/smart_profile/n;->b:Ljava/text/SimpleDateFormat;

    monitor-enter v1

    .line 266
    :try_start_0
    sget-object v4, Lcom/google/android/gms/smart_profile/n;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v4, p0, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v4

    .line 267
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    invoke-virtual {v3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v1, v5, :cond_1

    move v1, v2

    .line 270
    :goto_1
    if-eqz v1, :cond_3

    .line 271
    invoke-static {v4, v2}, Lcom/google/android/gms/smart_profile/n;->a(Ljava/util/Date;Z)Ljava/util/Calendar;

    move-result-object v0

    goto :goto_0

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    move v1, v0

    .line 268
    goto :goto_1

    .line 282
    :cond_2
    monitor-exit v1

    .line 274
    add-int/lit8 v0, v0, 0x1

    :cond_3
    sget-object v1, Lcom/google/android/gms/smart_profile/n;->f:[Ljava/text/SimpleDateFormat;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 275
    sget-object v1, Lcom/google/android/gms/smart_profile/n;->f:[Ljava/text/SimpleDateFormat;

    aget-object v1, v1, v0

    .line 276
    monitor-enter v1

    .line 277
    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {v3, v2}, Ljava/text/ParsePosition;->setIndex(I)V

    .line 278
    invoke-virtual {v1, p0, v3}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;Ljava/text/ParsePosition;)Ljava/util/Date;

    move-result-object v2

    .line 279
    invoke-virtual {v3}, Ljava/text/ParsePosition;->getIndex()I

    move-result v4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v4, v5, :cond_2

    .line 280
    const/4 v0, 0x0

    invoke-static {v2, v0}, Lcom/google/android/gms/smart_profile/n;->a(Ljava/util/Date;Z)Ljava/util/Calendar;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    .line 282
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 284
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final a(Ljava/util/Date;Z)Ljava/util/Calendar;
    .locals 3

    .prologue
    .line 288
    sget-object v0, Lcom/google/android/gms/smart_profile/n;->a:Ljava/util/TimeZone;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0, v1}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    .line 289
    invoke-virtual {v0, p0}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 290
    if-eqz p1, :cond_0

    .line 291
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 293
    :cond_0
    return-object v0
.end method

.method public static a(Ljava/util/Calendar;)Ljava/util/Date;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 433
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v3

    .line 434
    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v3, v2}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    .line 438
    const/16 v2, 0xb

    invoke-virtual {v3, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 439
    const/16 v2, 0xc

    invoke-virtual {v3, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 440
    const/16 v2, 0xd

    invoke-virtual {v3, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 441
    const/16 v2, 0xe

    invoke-virtual {v3, v2, v0}, Ljava/util/Calendar;->set(II)V

    .line 443
    invoke-static {p0}, Lcom/google/android/gms/smart_profile/n;->b(Ljava/util/Calendar;)Z

    move-result v4

    .line 444
    invoke-virtual {p0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    .line 445
    const/4 v5, 0x2

    invoke-virtual {p0, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    .line 446
    const/4 v6, 0x5

    invoke-virtual {p0, v6}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 447
    if-ne v5, v1, :cond_0

    const/16 v7, 0x1d

    if-ne v6, v7, :cond_0

    move v0, v1

    .line 448
    :cond_0
    new-instance v7, Ljava/util/GregorianCalendar;

    invoke-direct {v7}, Ljava/util/GregorianCalendar;-><init>()V

    .line 451
    if-nez v4, :cond_1

    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result v2

    :cond_1
    invoke-virtual {v7, v2, v5, v6}, Ljava/util/GregorianCalendar;->set(III)V

    .line 457
    if-nez v4, :cond_4

    .line 458
    invoke-virtual {v3, v1}, Ljava/util/Calendar;->get(I)I

    move-result v1

    .line 459
    invoke-virtual {v7, v3}, Ljava/util/GregorianCalendar;->before(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    if-eqz v0, :cond_4

    invoke-virtual {v7, v1}, Ljava/util/GregorianCalendar;->isLeapYear(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 465
    :cond_2
    add-int/lit8 v1, v1, 0x1

    .line 466
    if-eqz v0, :cond_3

    invoke-virtual {v7, v1}, Ljava/util/GregorianCalendar;->isLeapYear(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 467
    :cond_3
    invoke-virtual {v7, v1, v5, v6}, Ljava/util/GregorianCalendar;->set(III)V

    .line 470
    :cond_4
    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v0

    return-object v0
.end method

.method private static a(JJJ)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 184
    cmp-long v1, p0, p2

    if-nez v1, :cond_1

    .line 192
    :cond_0
    :goto_0
    return v0

    .line 190
    :cond_1
    invoke-static {p0, p1, p4, p5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 191
    const-wide/16 v2, 0x1

    sub-long v2, p2, v2

    invoke-static {v2, v3, p4, p5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 192
    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(JJJ)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 200
    invoke-static {p0, p1, p4, p5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v1

    .line 201
    invoke-static {p2, p3, p4, p5}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v2

    .line 203
    sub-int/2addr v1, v2

    .line 204
    if-ne v1, v0, :cond_1

    .line 205
    const/4 v0, 0x2

    .line 209
    :cond_0
    :goto_0
    return v0

    .line 206
    :cond_1
    if-eqz v1, :cond_0

    invoke-static {p0, p1}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 209
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 342
    if-nez p1, :cond_1

    .line 343
    const/4 v0, 0x0

    .line 368
    :cond_0
    :goto_0
    return-object v0

    .line 346
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    .line 347
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    invoke-static {v0}, Lcom/google/android/gms/smart_profile/n;->a(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v1

    .line 353
    if-eqz v1, :cond_0

    .line 357
    invoke-static {v1}, Lcom/google/android/gms/smart_profile/n;->b(Ljava/util/Calendar;)Z

    move-result v0

    .line 359
    if-nez v0, :cond_2

    .line 360
    invoke-static {p0}, Lcom/google/android/gms/smart_profile/n;->a(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    .line 366
    :goto_1
    monitor-enter v0

    .line 367
    :try_start_0
    sget-object v2, Lcom/google/android/gms/smart_profile/n;->a:Ljava/util/TimeZone;

    invoke-virtual {v0, v2}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 368
    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 362
    :cond_2
    invoke-static {p0}, Landroid/text/format/DateFormat;->getLongDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v0

    goto :goto_1

    .line 369
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 400
    invoke-static {p0}, Landroid/text/format/DateFormat;->getDateFormatOrder(Landroid/content/Context;)[C

    move-result-object v2

    move v0, v1

    .line 401
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 402
    aget-char v3, v2, v0

    const/16 v4, 0x64

    if-ne v3, v4, :cond_1

    .line 409
    :cond_0
    :goto_1
    return v1

    .line 405
    :cond_1
    aget-char v3, v2, v0

    const/16 v4, 0x4d

    if-ne v3, v4, :cond_2

    .line 406
    const/4 v1, 0x1

    goto :goto_1

    .line 401
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private static b(Ljava/util/Calendar;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 312
    invoke-virtual {p0, v0}, Ljava/util/Calendar;->get(I)I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
