.class public final Lcom/google/android/gms/drive/auth/AppIdentity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:Lcom/google/android/gms/drive/auth/AppIdentity;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    const-string v1, "com.google.android.gms"

    const/4 v2, 0x1

    const-string v3, ""

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/auth/AppIdentity;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 44
    new-instance v0, Lcom/google/android/gms/drive/auth/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/auth/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    .line 69
    iput-boolean p2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    .line 70
    if-eqz p2, :cond_0

    .line 71
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    .line 75
    :goto_0
    return-void

    .line 73
    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/drive/g/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a()Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lcom/google/android/gms/drive/auth/AppIdentity;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gms/drive/auth/AppIdentity;-><init>(Ljava/lang/String;ZLjava/lang/String;)V

    return-object v0
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/google/android/gms/drive/auth/AppIdentity;
    .locals 2

    .prologue
    .line 89
    const-string v0, "isSuperuser"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 90
    if-eqz v0, :cond_0

    .line 91
    sget-object v0, Lcom/google/android/gms/drive/auth/AppIdentity;->a:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 95
    :goto_0
    return-object v0

    .line 93
    :cond_0
    const-string v0, "packageName"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    const-string v1, "hash"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 95
    invoke-static {v0, v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 154
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 155
    const-string v1, "isSuperuser"

    iget-boolean v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 156
    iget-boolean v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    if-nez v1, :cond_0

    .line 157
    const-string v1, "packageName"

    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 158
    const-string v1, "hash"

    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 160
    :cond_0
    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 134
    if-ne p0, p1, :cond_1

    .line 141
    :cond_0
    :goto_0
    return v0

    .line 137
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/auth/AppIdentity;

    if-nez v2, :cond_2

    move v0, v1

    .line 138
    goto :goto_0

    .line 140
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 141
    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AppIdentity [packageName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 170
    iget-boolean v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 171
    iget-boolean v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->c:Z

    if-nez v0, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/drive/auth/AppIdentity;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 175
    :cond_0
    return-void

    .line 170
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
