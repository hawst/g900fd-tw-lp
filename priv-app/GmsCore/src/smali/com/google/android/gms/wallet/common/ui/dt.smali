.class public final Lcom/google/android/gms/wallet/common/ui/dt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/wallet/common/ui/dz;

.field public static final b:Lcom/google/android/gms/wallet/common/ui/dz;

.field public static final c:Lcom/google/android/gms/wallet/common/ui/dz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/du;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/du;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/dt;->a:Lcom/google/android/gms/wallet/common/ui/dz;

    .line 50
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dv;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/dv;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    .line 69
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/dw;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/dw;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    return-void
.end method

.method public static a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V
    .locals 5

    .prologue
    const/16 v4, 0xb

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 93
    .line 94
    if-eqz p1, :cond_6

    .line 95
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e()I

    move-result v0

    .line 97
    :goto_0
    invoke-static {v4}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p2, v0}, Lcom/google/android/gms/wallet/common/ui/dz;->a(I)I

    move-result v3

    invoke-virtual {p0, v3}, Landroid/app/Activity;->setTheme(I)V

    .line 100
    :cond_0
    invoke-static {v4}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/dt;->a:Lcom/google/android/gms/wallet/common/ui/dz;

    if-ne p2, v3, :cond_3

    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0xf

    const/4 v4, 0x3

    if-lt v3, v4, :cond_1

    move v1, v2

    :cond_1
    :goto_1
    if-eqz v1, :cond_2

    .line 101
    if-nez v0, :cond_5

    sget v0, Lcom/google/android/gms/h;->dF:I

    :goto_2
    invoke-virtual {p0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Window;->setBackgroundDrawableResource(I)V

    .line 103
    :cond_2
    return-void

    .line 100
    :cond_3
    sget-object v3, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    if-eq p2, v3, :cond_4

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    if-ne p2, v3, :cond_1

    :cond_4
    move v1, v2

    goto :goto_1

    .line 101
    :cond_5
    sget v0, Lcom/google/android/gms/h;->dG:I

    goto :goto_2

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 197
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 211
    :goto_0
    return-void

    .line 200
    :cond_0
    const-string v0, "translationX"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 201
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->bY:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 203
    new-instance v2, Lcom/google/android/gms/wallet/common/ui/dy;

    const/4 v3, 0x3

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/wallet/common/ui/dy;-><init>(IF)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 210
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 200
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 141
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;Z)V

    .line 142
    return-void
.end method

.method public static a(Landroid/view/View;I)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 158
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 160
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 161
    neg-int v0, p1

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 162
    invoke-virtual {p0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 165
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    .line 171
    :goto_0
    return-void

    .line 167
    :cond_0
    invoke-virtual {p0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 168
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->A:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 169
    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Z)V
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Landroid/view/View;->setFilterTouchesWhenObscured(Z)V

    .line 148
    invoke-virtual {p0}, Landroid/view/View;->getFilterTouchesWhenObscured()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 149
    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setFilterTouchesWhenObscured(Z)V

    .line 151
    :cond_0
    return-void

    .line 149
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/view/Window;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 246
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 247
    invoke-virtual {p0}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 248
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 249
    return-void
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 238
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/view/View;IZZ)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 219
    invoke-virtual {p0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    .line 220
    if-nez v0, :cond_0

    move v0, v1

    .line 231
    :goto_0
    return v0

    .line 223
    :cond_0
    invoke-virtual {v0}, Landroid/widget/RadioButton;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    if-nez p3, :cond_1

    .line 225
    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setEnabled(Z)V

    .line 226
    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 227
    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setEnabled(Z)V

    :goto_1
    move v0, v2

    .line 231
    goto :goto_0

    .line 229
    :cond_1
    invoke-virtual {v0, p2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_1
.end method

.method public static b(Landroid/view/View;I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 178
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/dx;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/common/ui/dx;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 189
    :cond_0
    return-void
.end method
