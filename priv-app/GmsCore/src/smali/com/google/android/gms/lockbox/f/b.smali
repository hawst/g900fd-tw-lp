.class public final Lcom/google/android/gms/lockbox/f/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:Z

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 43
    iput-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    iput-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    iput-boolean v0, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    iput v0, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/lockbox/f/b;->cachedSize:I

    .line 44
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 111
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 112
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 113
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    if-eqz v1, :cond_1

    .line 117
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 120
    :cond_1
    iget v1, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    if-eqz v1, :cond_2

    .line 121
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 125
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/lockbox/f/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_2
    check-cast p1, Lcom/google/android/gms/lockbox/f/b;

    .line 64
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/lockbox/f/b;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 65
    goto :goto_0

    .line 67
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/lockbox/f/b;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/lockbox/f/b;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 71
    goto :goto_0

    .line 73
    :cond_5
    iget v2, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    iget v3, p1, Lcom/google/android/gms/lockbox/f/b;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 74
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 81
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 86
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    add-int/2addr v0, v1

    .line 88
    return v0

    .line 86
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 94
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 97
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/lockbox/f/b;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 100
    :cond_1
    iget v0, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    if-eqz v0, :cond_2

    .line 101
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/lockbox/f/b;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 103
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 104
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/lockbox/f/b;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 106
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 107
    return-void
.end method
