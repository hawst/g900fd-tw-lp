.class final Lcom/google/android/gms/wallet/service/i;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/service/h;

.field private final b:Ljava/util/LinkedList;

.field private c:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/h;)V
    .locals 1

    .prologue
    .line 189
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/i;->a:Lcom/google/android/gms/wallet/service/h;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 191
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/i;->b:Ljava/util/LinkedList;

    .line 193
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/service/i;->c:I

    return-void
.end method

.method private a()Z
    .locals 2

    .prologue
    .line 196
    iget v0, p0, Lcom/google/android/gms/wallet/service/i;->c:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/i;->a:Lcom/google/android/gms/wallet/service/h;

    iget-object v1, v1, Lcom/google/android/gms/wallet/service/h;->a:Lcom/google/android/gms/wallet/service/e;

    iget v1, v1, Lcom/google/android/gms/wallet/service/e;->l:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 203
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x7fff

    if-ne v0, v1, :cond_0

    .line 204
    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "SERVICE_CONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 205
    iget v1, p0, Lcom/google/android/gms/wallet/service/i;->c:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    or-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/wallet/service/i;->c:I

    .line 206
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/i;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 207
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->peek()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Message;

    .line 211
    const-string v1, "NetworkPaymentServiceConnection"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Draining deferred message queue what="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/i;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    goto :goto_0

    .line 216
    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const v1, 0x8000

    if-ne v0, v1, :cond_2

    .line 217
    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "SERVICE_DISCONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    iget v1, p0, Lcom/google/android/gms/wallet/service/i;->c:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    xor-int/lit8 v0, v0, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/wallet/service/i;->c:I

    .line 219
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/i;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/i;->a:Lcom/google/android/gms/wallet/service/h;

    iget-object v0, v0, Lcom/google/android/gms/wallet/service/h;->a:Lcom/google/android/gms/wallet/service/e;

    iput-object v2, v0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/i;->a:Lcom/google/android/gms/wallet/service/h;

    iget-object v0, v0, Lcom/google/android/gms/wallet/service/h;->a:Lcom/google/android/gms/wallet/service/e;

    iput-object v2, v0, Lcom/google/android/gms/wallet/service/e;->k:Lcom/google/android/gms/wallet/service/ow/v;

    .line 236
    :cond_1
    :goto_1
    return-void

    .line 223
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/i;->a()Z

    move-result v0

    if-nez v0, :cond_3

    .line 224
    const-string v0, "NetworkPaymentServiceConnection"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage not connected, deferring what="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    invoke-static {p1}, Landroid/os/Message;->obtain(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/i;->b:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 230
    :cond_3
    const/4 v0, 0x0

    :goto_2
    const/4 v1, 0x1

    if-gt v0, v1, :cond_1

    .line 231
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/i;->a:Lcom/google/android/gms/wallet/service/h;

    iget-object v1, v1, Lcom/google/android/gms/wallet/service/h;->a:Lcom/google/android/gms/wallet/service/e;

    iget v2, p1, Landroid/os/Message;->what:I

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/wallet/service/e;->a(ILjava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 232
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method
