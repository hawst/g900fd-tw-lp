.class final Lcom/google/android/gms/cast/media/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/cast/media/af;

.field private final b:Lcom/google/android/gms/cast/internal/k;

.field private final c:Lcom/google/android/gms/cast/b/e;

.field private final d:Landroid/os/Handler;

.field private e:I

.field private f:I

.field private g:Lcom/google/android/gms/cast/ApplicationMetadata;

.field private h:Ljava/lang/String;

.field private i:Lcom/google/android/gms/cast/media/aj;

.field private j:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/b/e;Lcom/google/android/gms/cast/media/af;Landroid/os/Handler;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "MediaRouteSession"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    .line 87
    iput-object p1, p0, Lcom/google/android/gms/cast/media/ae;->c:Lcom/google/android/gms/cast/b/e;

    .line 88
    iput-object p2, p0, Lcom/google/android/gms/cast/media/ae;->a:Lcom/google/android/gms/cast/media/af;

    .line 89
    iput-object p3, p0, Lcom/google/android/gms/cast/media/ae;->d:Landroid/os/Handler;

    .line 90
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    invoke-virtual {v0, p5}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    invoke-virtual {v0, p4}, Lcom/google/android/gms/cast/internal/k;->a(Z)V

    .line 94
    return-void
.end method

.method private b(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V
    .locals 1

    .prologue
    .line 132
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->c:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    .line 135
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 166
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 167
    iget-boolean v0, p0, Lcom/google/android/gms/cast/media/ae;->j:Z

    if-eqz v0, :cond_0

    .line 168
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/media/ae;->j:Z

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->g:Lcom/google/android/gms/cast/ApplicationMetadata;

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->c:Lcom/google/android/gms/cast/b/e;

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/e;->a(Ljava/lang/String;)V

    .line 174
    :goto_0
    return-void

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->c:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->i()V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 1

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->g:Lcom/google/android/gms/cast/ApplicationMetadata;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 227
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onApplicationConnectionFailed; mPendingState=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 229
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v5, :cond_0

    .line 254
    :goto_0
    monitor-exit p0

    return-void

    .line 233
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 251
    :pswitch_1
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/media/ah;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lcom/google/android/gms/cast/media/ah;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 236
    :pswitch_2
    const/4 v0, 0x0

    :try_start_2
    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/media/ah;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lcom/google/android/gms/cast/media/ah;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/aj;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/aj;->b:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/media/ae;->b(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    goto :goto_0

    .line 244
    :pswitch_3
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 245
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/media/ag;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/cast/media/ag;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method final declared-synchronized a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 195
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onApplicationConnected; mPendingState=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 197
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eq v0, v5, :cond_0

    .line 223
    :goto_0
    monitor-exit p0

    return-void

    .line 201
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    packed-switch v0, :pswitch_data_0

    .line 219
    :pswitch_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 220
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 203
    :pswitch_1
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/ae;->c()V

    goto :goto_0

    .line 207
    :pswitch_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 208
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/ae;->c()V

    goto :goto_0

    .line 212
    :pswitch_3
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 213
    iput-object p1, p0, Lcom/google/android/gms/cast/media/ae;->g:Lcom/google/android/gms/cast/ApplicationMetadata;

    .line 214
    iput-object p2, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/media/ai;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/cast/media/ai;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V
    .locals 5

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "starting session for app %s; mState=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 107
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 129
    :goto_0
    monitor-exit p0

    return-void

    .line 109
    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 110
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/cast/media/ae;->b(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 114
    :pswitch_1
    const/4 v0, 0x2

    :try_start_2
    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 115
    new-instance v0, Lcom/google/android/gms/cast/media/aj;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/cast/media/aj;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;

    goto :goto_0

    .line 119
    :pswitch_2
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 120
    new-instance v0, Lcom/google/android/gms/cast/media/aj;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/cast/media/aj;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;

    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/ae;->c()V

    goto :goto_0

    .line 125
    :pswitch_3
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 126
    new-instance v0, Lcom/google/android/gms/cast/media/aj;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/cast/media/aj;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 184
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 185
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "session is not currently stopped! state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 184
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 188
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->c:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/cast/b/e;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 190
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Z)V
    .locals 3

    .prologue
    .line 143
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "stopping session"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 144
    iput-boolean p1, p0, Lcom/google/android/gms/cast/media/ae;->j:Z

    .line 146
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 159
    :goto_0
    monitor-exit p0

    return-void

    .line 149
    :pswitch_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 153
    :pswitch_1
    const/4 v0, 0x4

    :try_start_2
    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    goto :goto_0

    .line 157
    :pswitch_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 158
    invoke-direct {p0}, Lcom/google/android/gms/cast/media/ae;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 303
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b(I)V
    .locals 5

    .prologue
    .line 263
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->b:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "onApplicationDisconnected; mPendingState=%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 265
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 290
    :goto_0
    monitor-exit p0

    return-void

    .line 269
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    packed-switch v0, :pswitch_data_0

    .line 286
    :pswitch_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 287
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 271
    :pswitch_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/media/ag;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lcom/google/android/gms/cast/media/ag;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 272
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/aj;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/ae;->i:Lcom/google/android/gms/cast/media/aj;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/aj;->b:Lcom/google/android/gms/cast/LaunchOptions;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/cast/media/ae;->b(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    goto :goto_0

    .line 279
    :pswitch_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->f:I

    .line 280
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/cast/media/ae;->e:I

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/cast/media/ae;->d:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast/media/ag;

    iget-object v2, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;

    invoke-direct {v1, p0, v2, p1}, Lcom/google/android/gms/cast/media/ag;-><init>(Lcom/google/android/gms/cast/media/ae;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 282
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/media/ae;->h:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 269
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
