.class public final Lcom/google/android/gms/googlehelp/search/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/search/p;
.implements Lcom/google/android/gms/googlehelp/search/q;


# instance fields
.field public final a:Lcom/google/android/gms/googlehelp/search/SearchView;

.field private final b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private final c:Lcom/google/android/gms/googlehelp/search/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Landroid/view/View;I)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/search/c;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 49
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->f()Lcom/google/android/gms/googlehelp/search/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->c:Lcom/google/android/gms/googlehelp/search/a;

    .line 50
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 52
    invoke-virtual {p2, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/search/SearchView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Lcom/google/android/gms/googlehelp/search/p;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Lcom/google/android/gms/googlehelp/search/q;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(I)V

    .line 57
    new-instance v0, Lcom/google/android/gms/googlehelp/search/r;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/googlehelp/search/r;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Lcom/google/android/gms/googlehelp/search/SearchView;)V

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Landroid/support/v4/widget/f;)V

    .line 59
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/CharSequence;Z)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 91
    return-void
.end method

.method public final a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 73
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/search/SearchView;->b()Landroid/support/v4/widget/f;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/widget/f;->a()Landroid/database/Cursor;

    move-result-object v2

    .line 74
    if-eqz v2, :cond_0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move v0, v1

    .line 85
    :goto_0
    return v0

    .line 79
    :cond_1
    :try_start_0
    const-string v3, "suggest_intent_query"

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 80
    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 81
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    const/4 v4, 0x1

    invoke-virtual {v3, v2, v4}, Lcom/google/android/gms/googlehelp/search/SearchView;->a(Ljava/lang/CharSequence;Z)V

    .line 82
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/search/c;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-static {v2}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 63
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->a:Lcom/google/android/gms/googlehelp/search/SearchView;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/search/SearchView;->clearFocus()V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/search/c;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b(Ljava/lang/String;)V

    .line 66
    new-instance v0, Lcom/google/android/gms/googlehelp/f/m;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/search/c;->c:Lcom/google/android/gms/googlehelp/search/a;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/googlehelp/f/m;-><init>(Lcom/google/android/gms/googlehelp/search/a;Ljava/lang/String;Ljava/util/Calendar;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/m;->a([Ljava/lang/Object;)V

    .line 68
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
