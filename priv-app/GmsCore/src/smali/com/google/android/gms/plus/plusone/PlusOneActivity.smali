.class public final Lcom/google/android/gms/plus/plusone/PlusOneActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Lcom/google/android/gms/plus/plusone/p;

.field private b:Landroid/os/Bundle;

.field private c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v2, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0x0

    if-eqz v2, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/gms/l;->dU:I

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setContentView(I)V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "PlusOneActivity#Fragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/plusone/p;

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    if-nez v0, :cond_1

    .line 167
    new-instance v0, Lcom/google/android/gms/plus/plusone/p;

    invoke-direct {v0}, Lcom/google/android/gms/plus/plusone/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->b:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/plusone/p;->setArguments(Landroid/os/Bundle;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 170
    sget v1, Lcom/google/android/gms/j;->ph:I

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    const-string v3, "PlusOneActivity#Fragment"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 171
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 176
    :cond_1
    sget v0, Lcom/google/android/gms/j;->gt:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 177
    sget v0, Lcom/google/android/gms/j;->ph:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 178
    return-void

    .line 159
    :cond_2
    iget v2, v1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0x3

    if-eqz v2, :cond_3

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    sget v0, Lcom/google/android/gms/l;->dT:I

    goto :goto_1
.end method


# virtual methods
.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 236
    packed-switch p1, :pswitch_data_0

    .line 251
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->onActivityResult(IILandroid/content/Intent;)V

    .line 255
    :goto_0
    return-void

    .line 238
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/plusone/p;->a(I)V

    goto :goto_0

    .line 242
    :pswitch_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->d:Z

    goto :goto_0

    .line 245
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    .line 246
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttachedToWindow()V
    .locals 2

    .prologue
    .line 217
    invoke-super {p0}, Landroid/support/v4/app/q;->onAttachedToWindow()V

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    if-eqz v0, :cond_0

    .line 219
    sget v0, Lcom/google/android/gms/j;->pT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    .line 220
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/plusone/p;->a(Landroid/widget/ProgressBar;)V

    .line 222
    :cond_0
    return-void
.end method

.method public final onBackPressed()V
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/p;->a()V

    .line 229
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    .line 230
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 0

    .prologue
    .line 196
    packed-switch p2, :pswitch_data_0

    .line 202
    :goto_0
    return-void

    .line 198
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    goto :goto_0

    .line 196
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 206
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->gt:I

    if-ne v0, v1, :cond_1

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    if-eqz v0, :cond_0

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/p;->a()V

    .line 211
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    .line 213
    :cond_1
    return-void
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v6, 0x0

    .line 88
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 91
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 92
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v4

    .line 93
    const-string v2, "com.google.android.gms.plus.intent.extra.TOKEN"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 94
    const-string v3, "com.google.android.gms.plus.intent.extra.URL"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 95
    const-string v5, "com.google.android.gms.plus.intent.extra.EXTRA_SIGNED_UP"

    invoke-virtual {v0, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v5

    .line 96
    const-string v7, "com.google.android.gms.plus.intent.extra.ACCOUNT"

    invoke-virtual {v0, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->c:Ljava/lang/String;

    .line 98
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_0

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 100
    :cond_0
    const-string v0, "PlusOneActivity"

    const-string v1, "Intent missing required arguments"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->finish()V

    .line 151
    :cond_1
    :goto_0
    return-void

    .line 107
    :cond_2
    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->c:Ljava/lang/String;

    if-nez v7, :cond_3

    .line 108
    const-string v7, "<<default account>>"

    iput-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->c:Ljava/lang/String;

    .line 111
    :cond_3
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    iput-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->b:Landroid/os/Bundle;

    .line 112
    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->b:Landroid/os/Bundle;

    const-string v8, "PlusOneFragment#mCallingPackage"

    invoke-virtual {v7, v8, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->b:Landroid/os/Bundle;

    const-string v8, "PlusOneFragment#mAccount"

    iget-object v9, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->c:Ljava/lang/String;

    invoke-virtual {v7, v8, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->b:Landroid/os/Bundle;

    const-string v8, "PlusOneFragment#mUrl"

    invoke-virtual {v7, v8, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->b:Landroid/os/Bundle;

    const-string v7, "PlusOneFragment#mApplyPlusOne"

    const-string v8, "com.google.android.gms.plus.action.PLUS_ONE"

    invoke-virtual {v8, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v3, v7, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->b:Landroid/os/Bundle;

    const-string v3, "PlusOneFragment#mToken"

    invoke-virtual {v1, v3, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v7

    .line 120
    const-string v0, "gppo0"

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/plus/k;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;Ljava/lang/String;)V

    .line 122
    if-nez p1, :cond_5

    .line 123
    invoke-virtual {p0, v6}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->setResult(I)V

    .line 124
    if-nez v5, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->d:Z

    .line 129
    :goto_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->d:Z

    if-eqz v0, :cond_7

    .line 135
    if-nez p1, :cond_1

    .line 136
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->l()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)V

    .line 139
    :goto_3
    sget-object v1, Lcom/google/android/gms/plus/plusone/p;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v6, v1, :cond_6

    .line 140
    sget-object v1, Lcom/google/android/gms/plus/plusone/p;->a:[Ljava/lang/String;

    aget-object v1, v1, v6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 139
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_4
    move v0, v6

    .line 124
    goto :goto_1

    .line 126
    :cond_5
    const-string v0, "needs_sign_in"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->d:Z

    goto :goto_2

    .line 143
    :cond_6
    new-instance v1, Lcom/google/android/gms/plus/n;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput v10, v1, Lcom/google/android/gms/plus/n;->b:I

    invoke-virtual {v1}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    .line 146
    invoke-virtual {p0, v0, v10}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    .line 149
    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a()V

    goto/16 :goto_0
.end method

.method public final onCreateDialog(ILandroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 183
    packed-switch p1, :pswitch_data_0

    .line 191
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 185
    :pswitch_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 186
    sget v1, Lcom/google/android/gms/p;->uC:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x104000a

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 188
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    .line 183
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected final onResumeFragments()V
    .locals 1

    .prologue
    .line 259
    invoke-super {p0}, Landroid/support/v4/app/q;->onResumeFragments()V

    .line 261
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a:Lcom/google/android/gms/plus/plusone/p;

    if-nez v0, :cond_0

    .line 262
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->a()V

    .line 264
    :cond_0
    return-void
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 155
    const-string v0, "needs_sign_in"

    iget-boolean v1, p0, Lcom/google/android/gms/plus/plusone/PlusOneActivity;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 156
    return-void
.end method
