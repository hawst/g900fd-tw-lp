.class final Lcom/google/android/gms/cast/c/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[B

.field b:I

.field c:I

.field private final d:Ljava/util/Map;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/c/an;->b:I

    .line 21
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/c/an;->c:I

    .line 31
    if-gtz p1, :cond_0

    .line 32
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "invalid size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 35
    :cond_0
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/gms/cast/c/an;->a:[B

    .line 36
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/an;->d:Ljava/util/Map;

    .line 37
    return-void
.end method

.method private d(I)V
    .locals 3

    .prologue
    .line 91
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/an;->c(I)V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/an;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/cast/c/an;->b:I

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 93
    return-void
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 207
    const v0, 0xc000

    or-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 208
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 70
    iget v0, p0, Lcom/google/android/gms/cast/c/an;->c:I

    if-ne v0, v1, :cond_0

    .line 71
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no rewind is in effect"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 73
    :cond_0
    iget v0, p0, Lcom/google/android/gms/cast/c/an;->c:I

    iput v0, p0, Lcom/google/android/gms/cast/c/an;->b:I

    .line 74
    iput v1, p0, Lcom/google/android/gms/cast/c/an;->c:I

    .line 75
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/gms/cast/c/an;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/c/an;->b:I

    if-gt p1, v0, :cond_0

    if-gez p1, :cond_1

    .line 57
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "invalid rewind"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_1
    iget v0, p0, Lcom/google/android/gms/cast/c/an;->b:I

    iput v0, p0, Lcom/google/android/gms/cast/c/an;->c:I

    .line 61
    iput p1, p0, Lcom/google/android/gms/cast/c/an;->b:I

    .line 62
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/gms/cast/internal/j;->c:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 141
    array-length v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/c/an;->d(I)V

    .line 142
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/an;->a([B)V

    .line 143
    return-void
.end method

.method public final a([B)V
    .locals 4

    .prologue
    .line 128
    array-length v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/an;->c(I)V

    .line 129
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/c/an;->b:I

    array-length v3, p1

    invoke-static {p1, v0, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 130
    iget v0, p0, Lcom/google/android/gms/cast/c/an;->b:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/cast/c/an;->b:I

    .line 131
    return-void
.end method

.method public final a([Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 155
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/cast/c/an;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    move v4, v3

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 159
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 160
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 162
    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 163
    invoke-direct {p0, v5}, Lcom/google/android/gms/cast/c/an;->e(I)V

    .line 196
    :cond_0
    :goto_1
    return-void

    .line 165
    :cond_1
    array-length v1, p1

    array-length v7, v0

    sub-int v7, v1, v7

    if-gtz v7, :cond_2

    move v1, v3

    :goto_2
    if-eqz v1, :cond_9

    .line 167
    array-length v1, v0

    if-le v1, v4, :cond_9

    .line 168
    array-length v4, v0

    move v0, v5

    move v1, v4

    :goto_3
    move v2, v0

    move v4, v1

    .line 172
    goto :goto_0

    :cond_2
    move v1, v3

    .line 165
    :goto_4
    array-length v8, v0

    if-ge v1, v8, :cond_4

    aget-object v8, v0, v1

    add-int v9, v1, v7

    aget-object v9, p1, v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    move v1, v3

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_4
    const/4 v1, 0x1

    goto :goto_2

    .line 174
    :cond_5
    if-lez v4, :cond_7

    .line 175
    :goto_5
    array-length v0, p1

    sub-int/2addr v0, v4

    if-ge v3, v0, :cond_6

    .line 176
    aget-object v0, p1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/an;->a(Ljava/lang/String;)V

    .line 175
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 178
    :cond_6
    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/c/an;->e(I)V

    goto :goto_1

    .line 180
    :cond_7
    iget v0, p0, Lcom/google/android/gms/cast/c/an;->b:I

    .line 181
    array-length v0, p1

    new-array v2, v0, [I

    move v0, v3

    .line 182
    :goto_6
    array-length v1, p1

    if-ge v0, v1, :cond_8

    .line 183
    iget v1, p0, Lcom/google/android/gms/cast/c/an;->b:I

    aput v1, v2, v0

    .line 184
    aget-object v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/cast/c/an;->a(Ljava/lang/String;)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 186
    :cond_8
    invoke-direct {p0, v3}, Lcom/google/android/gms/cast/c/an;->d(I)V

    .line 190
    array-length v0, p1

    move v1, v3

    :goto_7
    array-length v4, p1

    if-ge v1, v4, :cond_0

    .line 191
    new-array v4, v0, [Ljava/lang/String;

    .line 192
    invoke-static {p1, v1, v4, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 193
    iget-object v5, p0, Lcom/google/android/gms/cast/c/an;->d:Ljava/util/Map;

    aget v6, v2, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_7

    :cond_9
    move v0, v2

    move v1, v4

    goto :goto_3
.end method

.method public final b(I)V
    .locals 3

    .prologue
    .line 102
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/an;->c(I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/an;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/cast/c/an;->b:I

    ushr-int/lit8 v2, p1, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/an;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/cast/c/an;->b:I

    and-int/lit16 v2, p1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    .line 105
    return-void
.end method

.method final c(I)V
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lcom/google/android/gms/cast/c/an;->a:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/gms/cast/c/an;->b:I

    sub-int/2addr v0, v1

    if-ge v0, p1, :cond_0

    .line 230
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 232
    :cond_0
    return-void
.end method
