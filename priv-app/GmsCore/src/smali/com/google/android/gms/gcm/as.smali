.class final Lcom/google/android/gms/gcm/as;
.super Lcom/google/android/gtalkservice/t;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/gcm/ProxyGTalkService;

.field private volatile b:Lcom/google/android/gtalkservice/s;

.field private volatile c:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/gcm/ProxyGTalkService;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    iput-object p1, p0, Lcom/google/android/gms/gcm/as;->a:Lcom/google/android/gms/gcm/ProxyGTalkService;

    invoke-direct {p0}, Lcom/google/android/gtalkservice/t;-><init>()V

    .line 60
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/as;->c:Ljava/util/concurrent/CountDownLatch;

    .line 64
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gtalkservice.IGTalkService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 66
    const-string v1, "com.google.android.gsf"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    invoke-virtual {p1, v0, p0, v2}, Lcom/google/android/gms/gcm/ProxyGTalkService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 69
    return-void
.end method

.method private g()Lcom/google/android/gtalkservice/s;
    .locals 2

    .prologue
    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/as;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/gcm/as;->b:Lcom/google/android/gtalkservice/s;

    .line 97
    if-nez v0, :cond_0

    .line 98
    const-string v0, "ProxyGTalkService not bound to GSF"

    .line 99
    const-string v1, "GCM"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 92
    :catch_0
    move-exception v0

    const-string v0, "Error while ProxyGTalkService binding to GSF"

    .line 93
    const-string v1, "GCM"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :cond_0
    return-object v0
.end method

.method private h()V
    .locals 4

    .prologue
    .line 110
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/gcm/as;->a:Lcom/google/android/gms/gcm/ProxyGTalkService;

    invoke-static {v0}, Lcom/google/android/gms/gcm/ar;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 115
    return-void

    .line 114
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method


# virtual methods
.method public final a(J)Lcom/google/android/gtalkservice/ab;
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 147
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->h()V

    .line 148
    invoke-interface {v0, p1, p2}, Lcom/google/android/gtalkservice/s;->a(J)Lcom/google/android/gtalkservice/ab;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gtalkservice/m;
    .locals 1

    .prologue
    .line 134
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 135
    invoke-interface {v0, p1}, Lcom/google/android/gtalkservice/s;->a(Ljava/lang/String;)Lcom/google/android/gtalkservice/m;

    move-result-object v0

    return-object v0
.end method

.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->h()V

    .line 129
    invoke-interface {v0}, Lcom/google/android/gtalkservice/s;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 168
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->h()V

    .line 169
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gtalkservice/s;->a(Ljava/lang/String;J)V

    .line 170
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gtalkservice/p;)V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 122
    invoke-interface {v0, p1, p2}, Lcom/google/android/gtalkservice/s;->a(Ljava/lang/String;Lcom/google/android/gtalkservice/p;)V

    .line 123
    return-void
.end method

.method public final b()Lcom/google/android/gtalkservice/m;
    .locals 1

    .prologue
    .line 140
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Lcom/google/android/gtalkservice/s;->b()Lcom/google/android/gtalkservice/m;

    move-result-object v0

    return-object v0
.end method

.method public final b(J)V
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 161
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->h()V

    .line 162
    invoke-interface {v0, p1, p2}, Lcom/google/android/gtalkservice/s;->b(J)V

    .line 163
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 154
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->h()V

    .line 155
    invoke-interface {v0}, Lcom/google/android/gtalkservice/s;->c()V

    .line 156
    return-void
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 175
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->h()V

    .line 176
    invoke-interface {v0}, Lcom/google/android/gtalkservice/s;->d()Z

    move-result v0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 182
    invoke-interface {v0}, Lcom/google/android/gtalkservice/s;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 187
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->g()Lcom/google/android/gtalkservice/s;

    move-result-object v0

    .line 188
    invoke-direct {p0}, Lcom/google/android/gms/gcm/as;->h()V

    .line 189
    invoke-interface {v0}, Lcom/google/android/gtalkservice/s;->f()V

    .line 190
    return-void
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 73
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/gcm/as;->b:Lcom/google/android/gtalkservice/s;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/gcm/as;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 75
    return-void

    .line 73
    :cond_0
    const-string v0, "com.google.android.gtalkservice.IGTalkService"

    invoke-interface {p2, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gtalkservice/s;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/google/android/gtalkservice/s;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gtalkservice/u;

    invoke-direct {v0, p2}, Lcom/google/android/gtalkservice/u;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/gcm/as;->b:Lcom/google/android/gtalkservice/s;

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/gcm/as;->c:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 82
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/gcm/as;->c:Ljava/util/concurrent/CountDownLatch;

    .line 83
    return-void
.end method
