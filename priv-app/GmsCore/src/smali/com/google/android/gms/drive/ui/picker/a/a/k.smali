.class public final Lcom/google/android/gms/drive/ui/picker/a/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:J

.field b:J

.field final synthetic c:Lcom/google/android/gms/drive/ui/picker/a/a/i;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ui/picker/a/a/i;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->c:Lcom/google/android/gms/drive/ui/picker/a/a/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method final a()I
    .locals 8

    .prologue
    const-wide/16 v6, 0xd0

    .line 570
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->c:Lcom/google/android/gms/drive/ui/picker/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a()I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 571
    const/16 v0, 0xd0

    .line 580
    :goto_0
    return v0

    .line 574
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 575
    iget-wide v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->a:J

    iget-wide v4, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->b:J

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    .line 576
    const/4 v0, 0x0

    goto :goto_0

    .line 578
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->a:J

    sub-long/2addr v0, v2

    mul-long/2addr v0, v6

    iget-wide v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->b:J

    div-long/2addr v0, v2

    sub-long v0, v6, v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public final run()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 585
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->c:Lcom/google/android/gms/drive/ui/picker/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a()I

    move-result v0

    if-eq v0, v2, :cond_0

    .line 586
    const-wide/16 v0, 0xc8

    iput-wide v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->b:J

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->a:J

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->c:Lcom/google/android/gms/drive/ui/picker/a/a/i;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a(I)V

    .line 595
    :goto_0
    return-void

    .line 590
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/k;->a()I

    move-result v0

    if-lez v0, :cond_1

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->c:Lcom/google/android/gms/drive/ui/picker/a/a/i;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a(Lcom/google/android/gms/drive/ui/picker/a/a/i;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 593
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/k;->c:Lcom/google/android/gms/drive/ui/picker/a/a/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/i;->a(I)V

    goto :goto_0
.end method
