.class public Lcom/google/android/gms/car/support/Fragment;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ComponentCallbacks;
.implements Landroid/view/View$OnCreateContextMenuListener;


# static fields
.field private static final O:Lcom/google/android/gms/car/support/ab;


# instance fields
.field A:Z

.field B:Z

.field C:Z

.field D:Z

.field E:Z

.field F:Z

.field G:I

.field H:Landroid/view/ViewGroup;

.field I:Landroid/view/View;

.field J:Landroid/view/View;

.field K:Z

.field L:Z

.field M:Z

.field N:Z

.field a:I

.field b:Landroid/view/View;

.field c:I

.field d:Landroid/os/Bundle;

.field e:Landroid/util/SparseArray;

.field f:I

.field g:Ljava/lang/String;

.field h:Landroid/os/Bundle;

.field i:Lcom/google/android/gms/car/support/Fragment;

.field j:I

.field k:I

.field l:Z

.field m:Z

.field n:Z

.field o:Z

.field p:Z

.field q:Z

.field r:I

.field s:Lcom/google/android/gms/car/support/n;

.field t:Lcom/google/android/gms/car/support/j;

.field u:Lcom/google/android/gms/car/support/n;

.field v:Lcom/google/android/gms/car/support/Fragment;

.field w:I

.field x:I

.field y:Ljava/lang/String;

.field z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/gms/car/support/ab;

    invoke-direct {v0}, Lcom/google/android/gms/car/support/ab;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/support/Fragment;->O:Lcom/google/android/gms/car/support/ab;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 380
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/support/Fragment;->a:I

    .line 198
    iput v1, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    .line 210
    iput v1, p0, Lcom/google/android/gms/car/support/Fragment;->j:I

    .line 281
    iput-boolean v2, p0, Lcom/google/android/gms/car/support/Fragment;->E:Z

    .line 303
    iput-boolean v2, p0, Lcom/google/android/gms/car/support/Fragment;->L:Z

    .line 381
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/car/support/Fragment;
    .locals 1

    .prologue
    .line 388
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/car/support/Fragment;->a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/car/support/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/car/support/Fragment;
    .locals 4

    .prologue
    .line 407
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/support/Fragment;->O:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/ab;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 408
    if-nez v0, :cond_0

    .line 410
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 411
    sget-object v1, Lcom/google/android/gms/car/support/Fragment;->O:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/car/support/ab;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 414
    if-eqz p2, :cond_1

    .line 415
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 416
    iput-object p2, v0, Lcom/google/android/gms/car/support/Fragment;->h:Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 418
    :cond_1
    return-object v0

    .line 419
    :catch_0
    move-exception v0

    .line 420
    new-instance v1, Lcom/google/android/gms/car/support/h;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/car/support/h;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 423
    :catch_1
    move-exception v0

    .line 424
    new-instance v1, Lcom/google/android/gms/car/support/h;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/car/support/h;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1

    .line 427
    :catch_2
    move-exception v0

    .line 428
    new-instance v1, Lcom/google/android/gms/car/support/h;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to instantiate fragment "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": make sure class name exists, is public, and has an empty constructor that is public"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/car/support/h;-><init>(Ljava/lang/String;Ljava/lang/Exception;)V

    throw v1
.end method

.method static b(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 444
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/support/Fragment;->O:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/support/ab;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 445
    if-nez v0, :cond_0

    .line 447
    invoke-virtual {p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 448
    sget-object v1, Lcom/google/android/gms/car/support/Fragment;->O:Lcom/google/android/gms/car/support/ab;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/car/support/ab;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    :cond_0
    const-class v1, Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 452
    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()V
    .locals 0

    .prologue
    .line 786
    return-void
.end method

.method public static h()Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 986
    const/4 v0, 0x0

    return-object v0
.end method

.method public static i()V
    .locals 0

    .prologue
    .line 1042
    return-void
.end method


# virtual methods
.method final a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 1491
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1492
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->b()V

    .line 1494
    :cond_0
    const-string v0, "Fragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment createView inflater dpi: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/LayoutInflater;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->densityDpi:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    const/4 v0, 0x0

    return-object v0
.end method

.method final a()V
    .locals 3

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/view/View;->restoreHierarchyState(Landroid/util/SparseArray;)V

    .line 459
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    .line 461
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 462
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 463
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 464
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onViewStateRestored()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 467
    :cond_1
    return-void
.end method

.method final a(ILcom/google/android/gms/car/support/Fragment;)V
    .locals 2

    .prologue
    .line 470
    iput p1, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    .line 471
    if-eqz p2, :cond_0

    .line 472
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p2, Lcom/google/android/gms/car/support/Fragment;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->g:Ljava/lang/String;

    .line 476
    :goto_0
    return-void

    .line 474
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "android:fragment:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 1467
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1468
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->b()V

    .line 1470
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1471
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1472
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1473
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onCreate()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1476
    :cond_1
    if-eqz p1, :cond_3

    .line 1477
    const-string v0, "android:support:fragments"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 1479
    if-eqz v0, :cond_3

    .line 1480
    iget-object v1, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-nez v1, :cond_2

    .line 1481
    new-instance v1, Lcom/google/android/gms/car/support/n;

    invoke-direct {v1}, Lcom/google/android/gms/car/support/n;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    iget-object v1, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    iget-object v2, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    new-instance v3, Lcom/google/android/gms/car/support/g;

    invoke-direct {v3, p0}, Lcom/google/android/gms/car/support/g;-><init>(Lcom/google/android/gms/car/support/Fragment;)V

    invoke-virtual {v1, v2, v3, p0}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/j;Lcom/google/android/gms/car/support/l;Lcom/google/android/gms/car/support/Fragment;)V

    .line 1483
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/support/n;->a(Landroid/os/Parcelable;Ljava/util/ArrayList;)V

    .line 1484
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->c()V

    .line 1487
    :cond_3
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1371
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1372
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->w:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1373
    const-string v0, " mContainerId=#"

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1374
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->x:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1375
    const-string v0, " mTag="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1376
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->a:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1377
    const-string v0, " mIndex="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1378
    const-string v0, " mWho="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->g:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1379
    const-string v0, " mBackStackNesting="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->r:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1380
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAdded="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->l:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1381
    const-string v0, " mRemoving="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->m:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1382
    const-string v0, " mResumed="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->n:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1383
    const-string v0, " mFromLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->o:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1384
    const-string v0, " mInLayout="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->p:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1385
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mHidden="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->z:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1386
    const-string v0, " mDetached="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->A:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1387
    const-string v0, " mMenuVisible="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->E:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1388
    const-string v0, " mHasMenu="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->D:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1389
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mRetainInstance="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->B:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1390
    const-string v0, " mRetaining="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->C:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1391
    const-string v0, " mUserVisibleHint="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->L:Z

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 1392
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->s:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1393
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mFragmentManager="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1394
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->s:Lcom/google/android/gms/car/support/n;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1396
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    if-eqz v0, :cond_1

    .line 1397
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mActivity="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1398
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1400
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->v:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_2

    .line 1401
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mParentFragment="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1402
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->v:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1404
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 1405
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mArguments="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->h:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1407
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_4

    .line 1408
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedFragmentState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1409
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->d:Landroid/os/Bundle;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1411
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    if-eqz v0, :cond_5

    .line 1412
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mSavedViewState="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1413
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->e:Landroid/util/SparseArray;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1415
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->i:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_6

    .line 1416
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTarget="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->i:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 1417
    const-string v0, " mTargetRequestCode="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1418
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->k:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1420
    :cond_6
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->G:I

    if-eqz v0, :cond_7

    .line 1421
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mNextAnim="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->G:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1423
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_8

    .line 1424
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mContainer="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->H:Landroid/view/ViewGroup;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1426
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    if-eqz v0, :cond_9

    .line 1427
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1429
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->J:Landroid/view/View;

    if-eqz v0, :cond_a

    .line 1430
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mInnerView="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->I:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1432
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    if-eqz v0, :cond_b

    .line 1433
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mAnimatingAway="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->b:Landroid/view/View;

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 1434
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mStateAfterAnimating="

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1435
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->c:I

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1437
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_c

    .line 1438
    invoke-virtual {p3, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Child "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1439
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3, p4}, Lcom/google/android/gms/car/support/n;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 1441
    :cond_c
    return-void
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 479
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->r:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Landroid/content/res/Resources;
    .locals 3

    .prologue
    .line 622
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    if-nez v0, :cond_0

    .line 623
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not attached to Activity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 625
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/j;->c()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 924
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/j;->b()Landroid/view/LayoutInflater;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 486
    invoke-super {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 969
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 970
    return-void
.end method

.method public final g()V
    .locals 1

    .prologue
    .line 978
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 979
    return-void
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 493
    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method final j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1188
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    .line 1189
    iput-object v2, p0, Lcom/google/android/gms/car/support/Fragment;->g:Ljava/lang/String;

    .line 1190
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->l:Z

    .line 1191
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->m:Z

    .line 1192
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->n:Z

    .line 1193
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->o:Z

    .line 1194
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->p:Z

    .line 1195
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->q:Z

    .line 1196
    iput v1, p0, Lcom/google/android/gms/car/support/Fragment;->r:I

    .line 1197
    iput-object v2, p0, Lcom/google/android/gms/car/support/Fragment;->s:Lcom/google/android/gms/car/support/n;

    .line 1198
    iput-object v2, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    .line 1199
    iput v1, p0, Lcom/google/android/gms/car/support/Fragment;->w:I

    .line 1200
    iput v1, p0, Lcom/google/android/gms/car/support/Fragment;->x:I

    .line 1201
    iput-object v2, p0, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    .line 1202
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->z:Z

    .line 1203
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->A:Z

    .line 1204
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->C:Z

    .line 1205
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->M:Z

    .line 1206
    iput-boolean v1, p0, Lcom/google/android/gms/car/support/Fragment;->N:Z

    .line 1207
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 1214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1215
    return-void
.end method

.method final l()V
    .locals 3

    .prologue
    .line 1500
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1501
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->b()V

    .line 1503
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1504
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1505
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1506
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onActivityCreated()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1509
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_2

    .line 1510
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->d()V

    .line 1512
    :cond_2
    return-void
.end method

.method final m()V
    .locals 3

    .prologue
    .line 1515
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1516
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->b()V

    .line 1517
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->a()Z

    .line 1519
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1520
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1521
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1522
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStart()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1525
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_2

    .line 1526
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->e()V

    .line 1528
    :cond_2
    return-void
.end method

.method final n()V
    .locals 3

    .prologue
    .line 1531
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1532
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->b()V

    .line 1533
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->a()Z

    .line 1535
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1536
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1537
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1538
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onResume()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1541
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_2

    .line 1542
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->f()V

    .line 1543
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->a()Z

    .line 1545
    :cond_2
    return-void
.end method

.method final o()V
    .locals 3

    .prologue
    .line 1650
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1651
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->g()V

    .line 1653
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1654
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1655
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1656
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onPause()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1659
    :cond_1
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 1132
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1133
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 1

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->t:Lcom/google/android/gms/car/support/j;

    invoke-static {}, Lcom/google/android/gms/car/support/j;->d()V

    .line 1313
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 1157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1158
    return-void
.end method

.method final p()V
    .locals 3

    .prologue
    .line 1662
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1663
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->h()V

    .line 1665
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1666
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1667
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1668
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onStop()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1671
    :cond_1
    return-void
.end method

.method final q()V
    .locals 1

    .prologue
    .line 1674
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1675
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->i()V

    .line 1677
    :cond_0
    return-void
.end method

.method final r()V
    .locals 3

    .prologue
    .line 1680
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1681
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->j()V

    .line 1683
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1684
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1685
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1686
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroyView()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1689
    :cond_1
    return-void
.end method

.method final s()V
    .locals 3

    .prologue
    .line 1692
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    if-eqz v0, :cond_0

    .line 1693
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->u:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/support/n;->k()V

    .line 1695
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1696
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    .line 1697
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/Fragment;->F:Z

    if-nez v0, :cond_1

    .line 1698
    new-instance v0, Lcom/google/android/gms/car/support/ac;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Fragment "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " did not call through to super.onDestroy()"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/support/ac;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1701
    :cond_1
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 498
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v0, 0x80

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 499
    if-nez p0, :cond_3

    const-string v0, "null"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 500
    :goto_0
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    if-ltz v0, :cond_0

    .line 501
    const-string v0, " #"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 502
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->f:I

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 504
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->w:I

    if-eqz v0, :cond_1

    .line 505
    const-string v0, " id=0x"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 506
    iget v0, p0, Lcom/google/android/gms/car/support/Fragment;->w:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 508
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 509
    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 510
    iget-object v0, p0, Lcom/google/android/gms/car/support/Fragment;->y:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 512
    :cond_2
    const/16 v0, 0x7d

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 513
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 499
    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-gtz v2, :cond_5

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    const/16 v2, 0x2e

    invoke-virtual {v0, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-lez v2, :cond_5

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :cond_5
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v0, 0x7b

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
