.class public abstract Lcom/google/android/gms/plus/apps/ar;
.super Lcom/google/android/gms/plus/apps/bf;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/bf;-><init>()V

    return-void
.end method

.method private b(Ljava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 178
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 179
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/apps/ar;->a(Ljava/lang/CharSequence;)V

    .line 184
    :goto_0
    return-void

    .line 182
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->fv:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->fv:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 192
    return-void
.end method


# virtual methods
.method public final V_()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 136
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->h()Lcom/google/android/gms/plus/apps/ap;

    move-result-object v0

    .line 138
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    iget v1, v1, Lcom/google/android/gms/plus/apps/c;->b:I

    packed-switch v1, :pswitch_data_0

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 140
    :pswitch_0
    invoke-super {p0, v2, v2}, Landroid/support/v4/app/ar;->a(ZZ)V

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/apps/ap;->a(Ljava/util/Collection;Z)V

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/c;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/apps/ap;->a(Ljava/util/Collection;)V

    .line 144
    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ap;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->e()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/apps/ar;->b(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 147
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ar;->j()V

    goto :goto_0

    .line 152
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/apps/ar;->j()V

    .line 153
    invoke-super {p0, v2, v2}, Landroid/support/v4/app/ar;->a(ZZ)V

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    invoke-interface {v1}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v1, v1, Lcom/google/android/gms/plus/apps/c;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/apps/ap;->a(Ljava/util/Collection;Z)V

    goto :goto_0

    .line 159
    :pswitch_2
    invoke-super {p0, v2, v2}, Landroid/support/v4/app/ar;->a(ZZ)V

    .line 160
    sget v1, Lcom/google/android/gms/p;->tR:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/apps/ar;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/apps/ar;->b(Ljava/lang/CharSequence;)V

    .line 161
    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ap;->b()V

    goto :goto_0

    .line 164
    :pswitch_3
    invoke-super {p0, v2, v2}, Landroid/support/v4/app/ar;->a(ZZ)V

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->f()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/apps/ar;->b(Ljava/lang/CharSequence;)V

    .line 166
    invoke-virtual {v0}, Lcom/google/android/gms/plus/apps/ap;->b()V

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method abstract a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ap;
.end method

.method public final bridge synthetic b()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ap;

    return-object v0
.end method

.method abstract e()Ljava/lang/CharSequence;
.end method

.method abstract f()Ljava/lang/CharSequence;
.end method

.method abstract g()Landroid/content/Intent;
.end method

.method protected final h()Lcom/google/android/gms/plus/apps/ap;
    .locals 1

    .prologue
    .line 195
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ap;

    .line 196
    if-nez v0, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ar;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ap;

    move-result-object v0

    .line 198
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ar;->a(Landroid/widget/ListAdapter;)V

    .line 200
    :cond_0
    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 43
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->onActivityCreated(Landroid/os/Bundle;)V

    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->i()V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->fv:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 48
    const/16 v1, 0xf

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAutoLinkMask(I)V

    .line 49
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 50
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 95
    packed-switch p1, :pswitch_data_0

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 97
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 98
    const-string v0, "deviceAddress"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/apps/c;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 12

    .prologue
    .line 59
    invoke-super {p0}, Lcom/google/android/gms/plus/apps/bf;->b()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/apps/ap;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/plus/apps/ap;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 60
    instance-of v1, v0, Lcom/google/android/gms/plus/model/a/a;

    if-eqz v1, :cond_3

    .line 61
    check-cast v0, Lcom/google/android/gms/plus/model/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->g()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    iget-object v2, v2, Lcom/google/android/gms/plus/apps/c;->a:Landroid/accounts/Account;

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->f()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->d()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->e()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->i()Z

    move-result v6

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->k()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->l()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->m()Z

    move-result v9

    new-instance v10, Landroid/content/Intent;

    const-string v11, "com.google.android.gms.plus.action.MANAGE_APP"

    invoke-direct {v10, v11}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v11, "com.google.android.gms.plus.ACCOUNT"

    invoke-virtual {v10, v11, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.APP_ID"

    invoke-virtual {v10, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "com.google.android.gms.plus.APP_PACKAGE"

    invoke-virtual {v10, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.APP_NAME"

    invoke-virtual {v10, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.APP_ICON_URL"

    invoke-virtual {v10, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.APP_IS_ASPEN"

    invoke-virtual {v10, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.APP_SCOPES"

    invoke-virtual {v10, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.APP_REVOKE_HANDLE"

    invoke-virtual {v10, v1, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.APP_HAS_CONN_READ"

    invoke-virtual {v10, v1, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v10, v2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ar;->j:Lcom/google/android/gms/plus/apps/c;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/gms/plus/model/a/a;->i()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/common/analytics/d;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/plus/apps/c;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 65
    :cond_0
    :goto_2
    return-void

    .line 61
    :cond_1
    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/gms/common/analytics/d;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1

    .line 62
    :cond_3
    instance-of v1, v0, Lcom/google/android/gms/fitness/data/BleDevice;

    if-eqz v1, :cond_0

    .line 63
    check-cast v0, Lcom/google/android/gms/fitness/data/BleDevice;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    const-class v3, Lcom/google/android/gms/plus/apps/ManageDeviceActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "device"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/apps/ar;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_2
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 75
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 90
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 77
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->g()Landroid/content/Intent;

    move-result-object v0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/plus/apps/ar;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/common/activity/WebViewActivity;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 84
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/apps/ar;->startActivityForResult(Landroid/content/Intent;I)V

    .line 88
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 86
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/apps/ar;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    .line 75
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 3

    .prologue
    const/16 v2, 0x64

    .line 69
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/apps/bf;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 70
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/p;->eU:I

    invoke-interface {p1, v0, v2, v2, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    .line 71
    return-void
.end method
