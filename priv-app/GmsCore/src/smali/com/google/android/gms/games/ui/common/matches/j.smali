.class final Lcom/google/android/gms/games/ui/common/matches/j;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 178
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 179
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 174
    check-cast p3, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    invoke-virtual {p3}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/common/matches/j;->d(Z)V

    invoke-virtual {p3}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/common/matches/j;->a(Landroid/net/Uri;I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/c;->k:Lcom/google/android/gms/games/ui/card/a;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/card/a;->f()Landroid/database/CharArrayBuffer;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/multiplayer/Participant;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/j;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/j;->l:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->jh:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/j;->f(Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/p;->jm:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/j;->h(I)V

    sget v2, Lcom/google/android/gms/p;->jn:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/j;->i(I)V

    sget v2, Lcom/google/android/gms/p;->jf:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/j;->j(I)V

    sget v2, Lcom/google/android/gms/p;->jg:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/j;->k(I)V

    sget v2, Lcom/google/android/gms/m;->h:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/j;->g(I)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/j;->l:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->jd:I

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    aput-object v1, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/j;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/j;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/h;

    .line 262
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/j;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 263
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 264
    sget v3, Lcom/google/android/gms/j;->lO:I

    if-ne v2, v3, :cond_0

    .line 265
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/j;->p()Lcom/google/android/gms/common/data/d;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/x;

    invoke-interface {v2, v1}, Lcom/google/android/gms/common/data/x;->b(Ljava/lang/Object;)V

    .line 267
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/h;->c(Lcom/google/android/gms/games/ui/common/matches/h;)Lcom/google/android/gms/games/ui/common/matches/i;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/i;->b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 268
    const/4 v0, 0x1

    .line 270
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs u()V
    .locals 0

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/j;->y()V

    .line 239
    return-void
.end method

.method public final y()V
    .locals 4

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/j;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/h;

    .line 243
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/j;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 244
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/h;->c(Lcom/google/android/gms/games/ui/common/matches/h;)Lcom/google/android/gms/games/ui/common/matches/i;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/h;->a(Lcom/google/android/gms/games/ui/common/matches/h;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/h;->b(Lcom/google/android/gms/games/ui/common/matches/h;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/i;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    return-void
.end method

.method public final z()V
    .locals 3

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/j;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/h;

    .line 252
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/j;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 253
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/j;->p()Lcom/google/android/gms/common/data/d;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/x;

    invoke-interface {v2, v1}, Lcom/google/android/gms/common/data/x;->b(Ljava/lang/Object;)V

    .line 255
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/h;->c(Lcom/google/android/gms/games/ui/common/matches/h;)Lcom/google/android/gms/games/ui/common/matches/i;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/i;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V

    .line 256
    return-void
.end method
