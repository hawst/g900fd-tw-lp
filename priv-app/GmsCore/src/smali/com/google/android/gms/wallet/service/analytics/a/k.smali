.class public final Lcom/google/android/gms/wallet/service/analytics/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-wide p1, p0, Lcom/google/android/gms/wallet/service/analytics/a/k;->a:J

    .line 25
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/analytics/a/k;->b:Ljava/lang/String;

    .line 26
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-wide/16 v0, -0x1

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/k;->a:J

    .line 20
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/ag;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/k;->b:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 30
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->a()Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/wallet/service/analytics/a/k;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/wallet/common/ah;->a(J)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/analytics/a/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
