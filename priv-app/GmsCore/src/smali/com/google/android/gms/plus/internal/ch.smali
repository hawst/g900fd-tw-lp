.class final Lcom/google/android/gms/plus/internal/ch;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/au;)V
    .locals 0

    .prologue
    .line 411
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/ch;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 412
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/ch;->b:Lcom/google/android/gms/plus/internal/au;

    .line 413
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 417
    .line 418
    if-eqz p2, :cond_1

    .line 419
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 421
    :goto_0
    new-instance v2, Lcom/google/android/gms/common/c;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 423
    if-eqz p3, :cond_0

    .line 424
    new-instance v1, Lcom/google/android/gms/plus/data/a/c;

    invoke-direct {v1, p3}, Lcom/google/android/gms/plus/data/a/c;-><init>(Landroid/os/Bundle;)V

    .line 426
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ch;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v3, Lcom/google/android/gms/plus/internal/ci;

    iget-object v4, p0, Lcom/google/android/gms/plus/internal/ch;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v5, p0, Lcom/google/android/gms/plus/internal/ch;->b:Lcom/google/android/gms/plus/internal/au;

    invoke-direct {v3, v4, v5, v2, v1}, Lcom/google/android/gms/plus/internal/ci;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/au;Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/c;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 427
    return-void

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method
