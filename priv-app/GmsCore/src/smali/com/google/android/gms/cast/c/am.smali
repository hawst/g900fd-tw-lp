.class final Lcom/google/android/gms/cast/c/am;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Lcom/google/android/gms/cast/internal/k;


# instance fields
.field private final a:[B

.field private final b:I

.field private c:I

.field private d:I

.field private final e:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "MdnsPacketReader"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/c/am;->f:Lcom/google/android/gms/cast/internal/k;

    return-void
.end method

.method public constructor <init>(Ljava/net/DatagramPacket;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    invoke-virtual {p1}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    .line 36
    invoke-virtual {p1}, Ljava/net/DatagramPacket;->getLength()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->b:I

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->c:I

    .line 38
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->d:I

    .line 39
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/am;->e:Ljava/util/Map;

    .line 40
    return-void
.end method

.method private c(I)V
    .locals 1

    .prologue
    .line 229
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/am;->b()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 230
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    .line 232
    :cond_0
    return-void
.end method

.method private g()I
    .locals 3

    .prologue
    .line 76
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/am;->c(I)V

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v0, v0, v1

    .line 78
    and-int/lit16 v0, v0, 0xff

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 58
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->d:I

    .line 59
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 49
    if-ltz p1, :cond_0

    .line 50
    iget v0, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->d:I

    .line 52
    :cond_0
    return-void
.end method

.method public final a([B)V
    .locals 4

    .prologue
    .line 192
    array-length v0, p1

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/am;->c(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/am;->c:I

    const/4 v2, 0x0

    array-length v3, p1

    invoke-static {v0, v1, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 194
    iget v0, p0, Lcom/google/android/gms/cast/c/am;->c:I

    array-length v1, p1

    add-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->c:I

    .line 195
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lcom/google/android/gms/cast/c/am;->d:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/cast/c/am;->d:I

    :goto_0
    iget v1, p0, Lcom/google/android/gms/cast/c/am;->c:I

    sub-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/cast/c/am;->b:I

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 205
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/c/am;->c(I)V

    .line 206
    iget v0, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->c:I

    .line 207
    return-void
.end method

.method public final c()I
    .locals 4

    .prologue
    .line 88
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/am;->c(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    .line 90
    iget-object v1, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v2, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    .line 91
    return v0
.end method

.method public final d()J
    .locals 5

    .prologue
    .line 101
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/am;->c(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    int-to-long v0, v0

    const/16 v2, 0x18

    shl-long/2addr v0, v2

    .line 103
    iget-object v2, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v3, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x10

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 104
    iget-object v2, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v3, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    const/16 v4, 0x8

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    .line 105
    iget-object v2, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v3, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v2, v2, v3

    and-int/lit16 v2, v2, 0xff

    int-to-long v2, v2

    or-long/2addr v0, v2

    .line 106
    return-wide v0
.end method

.method public final e()[Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 117
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 118
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 120
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/am;->b()I

    move-result v0

    if-lez v0, :cond_1

    .line 121
    invoke-direct {p0, v2}, Lcom/google/android/gms/cast/c/am;->c(I)V

    iget-object v0, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v1, p0, Lcom/google/android/gms/cast/c/am;->c:I

    aget-byte v0, v0, v1

    .line 123
    if-nez v0, :cond_2

    .line 125
    invoke-virtual {p0, v2}, Lcom/google/android/gms/cast/c/am;->b(I)V

    .line 160
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/am;->e:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 167
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0

    .line 128
    :cond_2
    and-int/lit16 v0, v0, 0xc0

    const/16 v1, 0xc0

    if-ne v0, v1, :cond_3

    move v1, v2

    .line 129
    :goto_1
    iget v7, p0, Lcom/google/android/gms/cast/c/am;->c:I

    .line 131
    if-eqz v1, :cond_5

    .line 134
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/am;->g()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    shl-int/lit8 v0, v0, 0x8

    invoke-direct {p0}, Lcom/google/android/gms/cast/c/am;->g()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v4, v0

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/cast/c/am;->e:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 136
    if-nez v0, :cond_4

    .line 137
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v5, "invalid label pointer "

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, "%04X"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v5, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v1, v3

    .line 128
    goto :goto_1

    :cond_4
    move-object v4, v0

    .line 146
    :goto_2
    invoke-interface {v6, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 149
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 150
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 151
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 152
    invoke-interface {v0, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 141
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/cast/c/am;->f()Ljava/lang/String;

    move-result-object v4

    .line 142
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 143
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v4, v0

    goto :goto_2

    .line 156
    :cond_6
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    if-eqz v1, :cond_0

    goto/16 :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 5

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/am;->g()I

    move-result v0

    .line 178
    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/am;->c(I)V

    .line 179
    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/am;->a:[B

    iget v3, p0, Lcom/google/android/gms/cast/c/am;->c:I

    sget-object v4, Lcom/google/android/gms/cast/internal/j;->c:Ljava/nio/charset/Charset;

    invoke-direct {v1, v2, v3, v0, v4}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    .line 180
    iget v2, p0, Lcom/google/android/gms/cast/c/am;->c:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/android/gms/cast/c/am;->c:I

    .line 181
    return-object v1
.end method
