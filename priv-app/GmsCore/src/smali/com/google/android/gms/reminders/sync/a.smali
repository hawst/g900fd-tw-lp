.class public final Lcom/google/android/gms/reminders/sync/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/reminders/sync/d;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private d:Lcom/google/android/gms/reminders/a/d;

.field private final e:Landroid/content/ContentResolver;

.field private final f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/reminders/sync/d;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/reminders/a/d;I)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-object p2, p0, Lcom/google/android/gms/reminders/sync/a;->b:Lcom/google/android/gms/reminders/sync/d;

    .line 62
    iput-object p3, p0, Lcom/google/android/gms/reminders/sync/a;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 63
    iput-object p4, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    .line 64
    iput-object p1, p0, Lcom/google/android/gms/reminders/sync/a;->a:Landroid/content/Context;

    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->e:Landroid/content/ContentResolver;

    .line 66
    iput p5, p0, Lcom/google/android/gms/reminders/sync/a;->f:I

    .line 67
    return-void
.end method

.method private a(J)Landroid/content/ContentProviderOperation;
    .locals 5

    .prologue
    .line 256
    sget-object v0, Lcom/google/android/gms/reminders/internal/a/g;->a:Landroid/net/Uri;

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    iget-wide v2, v1, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "storage_version"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;[Lcom/google/f/a/w;Lcom/google/android/gms/reminders/a/d;)Ljava/util/ArrayList;
    .locals 12

    .prologue
    .line 208
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 209
    new-instance v1, Lcom/google/f/a/a/k;

    invoke-direct {v1}, Lcom/google/f/a/a/k;-><init>()V

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/reminders/sync/b;->a(Landroid/content/Context;)Lcom/google/f/a/a/t;

    move-result-object v0

    iput-object v0, v1, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    .line 211
    iput-object p2, v1, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->b:Lcom/google/android/gms/reminders/sync/d;

    iget-object v0, v0, Lcom/google/android/gms/reminders/sync/d;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/reminders/get?alt=proto"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/f/a/a/l;

    invoke-direct {v5}, Lcom/google/f/a/a/l;-><init>()V

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/f/a/a/l;

    .line 213
    iget-object v1, v0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 214
    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v5, v3, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    invoke-virtual {v4, v5}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 215
    iget-object v3, v3, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "deleted"

    const/4 v7, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "is_dirty"

    const/4 v7, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v4, v5, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v5, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    const-string v7, "account_id=? AND server_assigned_id=?"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    iget-wide v10, p3, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    iget-object v3, v3, Lcom/google/f/a/w;->a:Ljava/lang/Long;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v8, v9

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_0
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v4, v3}, Lcom/google/android/gms/reminders/sync/a;->a(Landroid/content/ContentValues;Lcom/google/f/a/a/r;)V

    const-string v3, "account_id"

    iget-object v5, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    iget-wide v8, v5, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v3, "is_dirty"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v3, Lcom/google/android/gms/reminders/internal/a/k;->b:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 220
    :cond_1
    return-object v6
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/f/a/a/r;)V
    .locals 3

    .prologue
    .line 263
    iget-object v0, p1, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    if-eqz v0, :cond_0

    const-string v1, "client_assigned_id"

    iget-object v2, v0, Lcom/google/f/a/w;->b:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "server_assigned_id"

    iget-object v2, v0, Lcom/google/f/a/w;->a:Ljava/lang/Long;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v1, "client_assigned_thread_id"

    iget-object v0, v0, Lcom/google/f/a/w;->c:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    :cond_0
    iget-object v0, p1, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    if-eqz v0, :cond_1

    .line 265
    const-string v0, "task_list"

    iget-object v1, p1, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    iget-object v1, v1, Lcom/google/f/a/a/u;->a:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 268
    :cond_1
    const-string v0, "title"

    iget-object v1, p1, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const-string v0, "created_time_millis"

    iget-object v1, p1, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 270
    const-string v0, "archived_time_ms"

    iget-object v1, p1, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 271
    const-string v0, "archived"

    iget-object v1, p1, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 272
    const-string v0, "deleted"

    iget-object v1, p1, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 273
    const-string v0, "pinned"

    iget-object v1, p1, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 274
    const-string v0, "snoozed"

    iget-object v1, p1, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/reminders/c/b;->a(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 275
    const-string v0, "snoozed_time_millis"

    iget-object v1, p1, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 276
    const-string v0, "location_snoozed_until_ms"

    iget-object v1, p1, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 278
    iget-object v0, p1, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    const-string v1, "due_date_"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/reminders/sync/a;->a(Landroid/content/ContentValues;Lcom/google/f/a/h;Ljava/lang/String;)V

    .line 279
    iget-object v0, p1, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    const-string v1, "event_date_"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/reminders/sync/a;->a(Landroid/content/ContentValues;Lcom/google/f/a/h;Ljava/lang/String;)V

    .line 280
    iget-object v0, p1, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    if-nez v0, :cond_2

    const-string v0, "lat"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "lng"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "name"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "radius_meters"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "location_type"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    const-string v0, "display_address"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 281
    :goto_0
    return-void

    .line 280
    :cond_2
    const-string v1, "lat"

    iget-object v2, v0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v1, "lng"

    iget-object v2, v0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Double;)V

    const-string v1, "name"

    iget-object v2, v0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "radius_meters"

    iget-object v2, v0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "location_type"

    iget-object v2, v0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    invoke-virtual {p0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "display_address"

    iget-object v0, v0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    invoke-virtual {p0, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/f/a/h;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 294
    if-nez p1, :cond_0

    .line 295
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "year"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 296
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "month"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 297
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 298
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "period"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 299
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "absolute_time_ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 300
    const/4 v0, 0x0

    invoke-static {p0, v0, p2}, Lcom/google/android/gms/reminders/sync/a;->a(Landroid/content/ContentValues;Lcom/google/f/a/i;Ljava/lang/String;)V

    .line 309
    :goto_0
    return-void

    .line 302
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "year"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 303
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "month"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 304
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "day"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "period"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 306
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "absolute_time_ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 307
    iget-object v0, p1, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    invoke-static {p0, v0, p2}, Lcom/google/android/gms/reminders/sync/a;->a(Landroid/content/ContentValues;Lcom/google/f/a/i;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/f/a/i;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 312
    if-nez p1, :cond_0

    .line 313
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hour"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 314
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "minute"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "second"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 321
    :goto_0
    return-void

    .line 317
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "hour"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/i;->a:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 318
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "minute"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/i;->b:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 319
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "second"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/google/f/a/i;->c:Ljava/lang/Integer;

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_0
.end method

.method private b()Z
    .locals 14

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 98
    move-object v6, v0

    move v7, v2

    .line 102
    :goto_0
    new-instance v4, Lcom/google/f/a/a/m;

    invoke-direct {v4}, Lcom/google/f/a/a/m;-><init>()V

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/a;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/reminders/sync/b;->a(Landroid/content/Context;)Lcom/google/f/a/a/t;

    move-result-object v1

    iput-object v1, v4, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    .line 105
    if-eqz v0, :cond_0

    .line 106
    iput-object v0, v4, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->b:Lcom/google/android/gms/reminders/sync/d;

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/a;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/reminders/sync/d;->a:Lcom/google/android/gms/common/server/q;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "/reminders/list?alt=proto"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/f/a/a/n;

    invoke-direct {v5}, Lcom/google/f/a/a/n;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/f/a/a/n;

    .line 111
    if-eqz v7, :cond_5

    .line 121
    iget-object v1, v0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    .line 122
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "First sync storageVersion:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    move v3, v8

    .line 126
    :goto_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 127
    iget-object v6, v0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    array-length v7, v6

    move v4, v8

    :goto_2
    if-ge v4, v7, :cond_1

    aget-object v9, v6, v4

    .line 128
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    invoke-static {v10, v9}, Lcom/google/android/gms/reminders/sync/a;->a(Landroid/content/ContentValues;Lcom/google/f/a/a/r;)V

    const-string v9, "account_id"

    iget-object v11, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    iget-wide v12, v11, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v9, "is_dirty"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v9, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    sget-object v9, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-static {v9}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    invoke-virtual {v9, v10}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 131
    :cond_1
    iget-object v0, v0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    .line 133
    if-nez v0, :cond_2

    if-eqz v1, :cond_2

    .line 135
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Lcom/google/android/gms/reminders/sync/a;->a(J)Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_2
    iget-object v4, p0, Lcom/google/android/gms/reminders/sync/a;->e:Landroid/content/ContentResolver;

    const-string v6, "ReminderSync"

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v4

    .line 141
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Continuation:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Apply batch result"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 143
    if-nez v4, :cond_3

    .line 149
    :goto_3
    return v8

    .line 148
    :cond_3
    if-nez v0, :cond_4

    move v8, v2

    .line 149
    goto :goto_3

    :cond_4
    move-object v6, v1

    move v7, v3

    .line 151
    goto/16 :goto_0

    :cond_5
    move-object v1, v6

    move v3, v7

    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 74
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v7

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    iget-object v0, v0, Lcom/google/android/gms/reminders/a/d;->c:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/reminders/sync/a;->b()Z

    move-result v2

    .line 89
    :cond_0
    :goto_0
    return v2

    .line 81
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    iget-object v0, v0, Lcom/google/android/gms/reminders/a/d;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    move-wide v4, v0

    move v0, v2

    :goto_1
    if-eqz v0, :cond_2

    new-instance v8, Lcom/google/f/a/a/h;

    invoke-direct {v8}, Lcom/google/f/a/a/h;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/reminders/sync/b;->a(Landroid/content/Context;)Lcom/google/f/a/a/t;

    move-result-object v0

    iput-object v0, v8, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v8, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    iget v0, p0, Lcom/google/android/gms/reminders/sync/a;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v8, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->b:Lcom/google/android/gms/reminders/sync/d;

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/a;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/reminders/sync/d;->a:Lcom/google/android/gms/common/server/q;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "/reminders/history?alt=proto"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/f/a/a/i;

    invoke-direct {v5}, Lcom/google/f/a/a/i;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/f/a/a/i;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v3, v0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v3}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v2, v6

    .line 82
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Forward sync result:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->a()I

    .line 84
    if-nez v2, :cond_0

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    invoke-static {v7, v0}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/reminders/a/d;)V

    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/reminders/sync/a;->b()Z

    move-result v2

    goto :goto_0

    .line 81
    :cond_3
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    iget-object v5, v0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    array-length v9, v5

    move v3, v6

    :goto_2
    if-ge v3, v9, :cond_5

    aget-object v1, v5, v3

    if-eqz v1, :cond_4

    iget-object v10, v1, Lcom/google/f/a/a/j;->b:[Lcom/google/f/a/w;

    array-length v11, v10

    move v1, v6

    :goto_3
    if-ge v1, v11, :cond_4

    aget-object v12, v10, v1

    invoke-interface {v4, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_2

    :cond_5
    invoke-interface {v4}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v3, p0, Lcom/google/android/gms/reminders/sync/a;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/f/a/w;

    invoke-interface {v4, v1}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/f/a/w;

    iget-object v4, p0, Lcom/google/android/gms/reminders/sync/a;->d:Lcom/google/android/gms/reminders/a/d;

    invoke-direct {p0, v3, v1, v4}, Lcom/google/android/gms/reminders/sync/a;->a(Lcom/google/android/gms/common/server/ClientContext;[Lcom/google/f/a/w;Lcom/google/android/gms/reminders/a/d;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_6
    iget-object v1, v0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/reminders/sync/a;->a(J)Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/reminders/sync/a;->e:Landroid/content/ContentResolver;

    const-string v3, "ReminderSync"

    invoke-static {v1, v8, v3}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iget-object v0, v0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1
.end method
