.class public Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/dn;


# static fields
.field public static final a:Lcom/google/android/gms/wallet/common/ui/ag;


# instance fields
.field private b:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private c:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private d:I

.field private e:Z

.field private f:Z

.field private g:Lcom/google/android/gms/wallet/common/ui/ag;

.field private h:Z

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ae;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/ae;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ag;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 95
    invoke-direct {p0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    .line 77
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->d:I

    .line 78
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e:Z

    .line 79
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f:Z

    .line 80
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ag;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->g:Lcom/google/android/gms/wallet/common/ui/ag;

    .line 82
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->i:Z

    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a()V

    .line 97
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 77
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->d:I

    .line 78
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e:Z

    .line 79
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f:Z

    .line 80
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ag;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->g:Lcom/google/android/gms/wallet/common/ui/ag;

    .line 82
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->i:Z

    .line 91
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a()V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 77
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->d:I

    .line 78
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e:Z

    .line 79
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f:Z

    .line 80
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a:Lcom/google/android/gms/wallet/common/ui/ag;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->g:Lcom/google/android/gms/wallet/common/ui/ag;

    .line 82
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->i:Z

    .line 86
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a()V

    .line 87
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)Landroid/widget/AdapterView$OnItemSelectedListener;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->c:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 206
    :goto_0
    return-void

    .line 167
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/af;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/af;-><init>(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->c:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->c:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-super {p0, v0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;)I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->d:I

    return v0
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 136
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->d:I

    .line 137
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 138
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 104
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f:Z

    .line 105
    return-void
.end method

.method public e()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 234
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 251
    :goto_0
    return v0

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    .line 239
    if-nez v0, :cond_1

    move v0, v2

    .line 240
    goto :goto_0

    .line 243
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->getSelectedItemPosition()I

    move-result v3

    .line 244
    const/4 v4, -0x1

    if-ne v3, v4, :cond_2

    move v0, v2

    .line 245
    goto :goto_0

    .line 248
    :cond_2
    instance-of v2, v0, Landroid/widget/ListAdapter;

    if-eqz v2, :cond_3

    .line 249
    check-cast v0, Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0

    :cond_3
    move v0, v1

    .line 251
    goto :goto_0
.end method

.method public f()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    invoke-interface {v0}, Landroid/widget/SpinnerAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must set non-empty adapter before validating"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 220
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->h:Z

    .line 221
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 222
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 223
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->g:Lcom/google/android/gms/wallet/common/ui/ag;

    invoke-interface {v2, v0}, Lcom/google/android/gms/wallet/common/ui/ag;->a(Landroid/view/View;)V

    .line 228
    :goto_1
    return v1

    :cond_0
    move v0, v2

    .line 218
    goto :goto_0

    .line 226
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->g:Lcom/google/android/gms/wallet/common/ui/ag;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->getContext()Landroid/content/Context;

    move-result-object v3

    sget v4, Lcom/google/android/gms/p;->Bt:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v0, v3}, Lcom/google/android/gms/wallet/common/ui/ag;->a(Landroid/view/View;Ljava/lang/CharSequence;)V

    move v1, v2

    .line 228
    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 276
    invoke-super/range {p0 .. p5}, Landroid/widget/Spinner;->onLayout(ZIIII)V

    .line 281
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->i:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 282
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->i:Z

    .line 283
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->h:Z

    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->f()Z

    .line 287
    :cond_0
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 265
    instance-of v0, p1, Landroid/os/Bundle;

    if-nez v0, :cond_0

    .line 266
    invoke-super {p0, p1}, Landroid/widget/Spinner;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 272
    :goto_0
    return-void

    .line 269
    :cond_0
    check-cast p1, Landroid/os/Bundle;

    .line 270
    const-string v0, "superSavedInstanceState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/Spinner;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 271
    const-string v0, "potentialErrorOnConfigChange"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->h:Z

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 257
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 258
    const-string v1, "superSavedInstanceState"

    invoke-super {p0}, Landroid/widget/Spinner;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 259
    const-string v1, "potentialErrorOnConfigChange"

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->h:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 260
    return-object v0
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 154
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 155
    if-nez p1, :cond_2

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->e:Z

    if-eqz v0, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 156
    :cond_1
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->c:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-super {p0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_0

    .line 158
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a()V

    goto :goto_0
.end method
