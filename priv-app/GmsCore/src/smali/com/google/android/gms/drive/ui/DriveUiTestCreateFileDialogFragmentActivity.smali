.class public Lcom/google/android/gms/drive/ui/DriveUiTestCreateFileDialogFragmentActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 29
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 31
    new-instance v1, Lcom/google/android/gms/drive/ui/create/a;

    invoke-direct {v1}, Lcom/google/android/gms/drive/ui/create/a;-><init>()V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/DriveUiTestCreateFileDialogFragmentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 34
    if-nez v0, :cond_0

    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 38
    :cond_0
    const-string v2, "DriveUiTestActivity"

    const-string v3, "Creating activity"

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    .line 42
    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    const-string v4, "ui tester file title"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 43
    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    const-string v4, "application/octet-stream"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 46
    const-string v3, "metadata"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 47
    const-string v2, "accountName"

    const-string v3, "driveUiTester"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v2, "dialogTitle"

    const-string v3, "ui tester dialog title"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/ui/create/a;->setArguments(Landroid/os/Bundle;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/DriveUiTestCreateFileDialogFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v2, "DriveUiTestActivity"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/drive/ui/create/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 51
    return-void
.end method
