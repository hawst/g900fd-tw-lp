.class public Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/bz;
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/ui/common/achievements/b;


# instance fields
.field private l:Lcom/google/android/gms/games/ui/common/achievements/f;

.field private m:Lcom/google/android/gms/games/ui/common/achievements/a;

.field private n:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;-><init>()V

    return-void
.end method


# virtual methods
.method public final B()V
    .locals 4

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 183
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 196
    :goto_0
    return-void

    .line 187
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    .line 188
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 189
    sget-object v1, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 195
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    goto :goto_0

    .line 192
    :cond_1
    sget-object v2, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v3, v1}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;)I
    .locals 3

    .prologue
    .line 201
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 202
    sget v1, Lcom/google/android/gms/g;->aH:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/g;->aG:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sub-int v0, v1, v0

    .line 205
    return v0
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->m:Lcom/google/android/gms/games/ui/common/achievements/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/achievements/a;->f()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/achievement/Achievement;

    .line 155
    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/Achievement;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->n:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 156
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->n:Ljava/lang/String;

    .line 157
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v3

    invoke-static {v1, v2, v3, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Landroid/content/Context;Lcom/google/android/gms/games/ui/bh;Lcom/google/android/gms/games/achievement/Achievement;)V

    .line 162
    :cond_1
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 27
    check-cast p1, Lcom/google/android/gms/games/achievement/d;

    invoke-interface {p1}, Lcom/google/android/gms/games/achievement/d;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v2

    invoke-interface {p1}, Lcom/google/android/gms/games/achievement/d;->c()Lcom/google/android/gms/games/achievement/a;

    move-result-object v3

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/games/achievement/a;->w_()V

    :goto_0
    return-void

    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/games/achievement/a;->w_()V

    goto :goto_0

    :cond_1
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->m:Lcom/google/android/gms/games/ui/common/achievements/a;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/common/achievements/a;->a(Lcom/google/android/gms/common/data/d;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->g:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1}, Landroid/support/v7/widget/RecyclerView;->e()Landroid/support/v7/widget/by;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/support/v7/widget/by;->b()Z

    move-result v4

    if-eqz p0, :cond_2

    if-nez v4, :cond_4

    invoke-interface {p0}, Landroid/support/v7/widget/bz;->a()V

    :cond_2
    :goto_1
    invoke-virtual {v3}, Lcom/google/android/gms/games/achievement/a;->c()I

    move-result v4

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_5

    invoke-virtual {v3, v1}, Lcom/google/android/gms/games/achievement/a;->b(I)Lcom/google/android/gms/games/achievement/Achievement;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/games/achievement/Achievement;->n()I

    move-result v5

    if-nez v5, :cond_3

    add-int/lit8 v0, v0, 0x1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    iget-object v1, v1, Landroid/support/v7/widget/by;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/android/gms/games/achievement/a;->w_()V

    throw v0

    :cond_5
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->l:Lcom/google/android/gms/games/ui/common/achievements/f;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->l:Lcom/google/android/gms/games/ui/common/achievements/f;

    invoke-virtual {v1, v0, v4}, Lcom/google/android/gms/games/ui/common/achievements/f;->e(II)V

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v4, v1}, Lcom/google/android/gms/games/ui/d/p;->a(IIZ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    .line 84
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 85
    sget-object v0, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 91
    :goto_0
    return-void

    .line 88
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->g:Lcom/google/android/gms/games/achievement/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, p1, v2, v0}, Lcom/google/android/gms/games/achievement/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/achievement/Achievement;)V
    .locals 3

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 167
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 168
    const-string v0, "AchievementFrag"

    const-string v1, "Api client not connected; aborting"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    :goto_0
    return-void

    .line 172
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v2

    invoke-static {v0, v1, v2, p1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Landroid/content/Context;Lcom/google/android/gms/games/ui/bh;Lcom/google/android/gms/games/achievement/Achievement;)V

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 42
    new-instance v0, Lcom/google/android/gms/games/ui/common/achievements/a;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/games/ui/common/achievements/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/achievements/b;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->m:Lcom/google/android/gms/games/ui/common/achievements/a;

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->a()Z

    move-result v1

    .line 46
    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/gms/h;->at:I

    :goto_0
    sget v2, Lcom/google/android/gms/p;->hG:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->a(III)V

    .line 49
    if-eqz v1, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 51
    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sget v2, Lcom/google/android/gms/f;->W:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->a(II)V

    .line 55
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 56
    new-instance v1, Lcom/google/android/gms/games/ui/common/achievements/f;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/ui/common/achievements/f;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->l:Lcom/google/android/gms/games/ui/common/achievements/f;

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->l:Lcom/google/android/gms/games/ui/common/achievements/f;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->m:Lcom/google/android/gms/games/ui/common/achievements/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 61
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_1

    .line 66
    const-string v1, "com.google.android.gms.games.ACHIEVEMENT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/achievements/AchievementFragment;->n:Ljava/lang/String;

    .line 67
    const-string v1, "com.google.android.gms.games.ACHIEVEMENT_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    .line 69
    :cond_1
    return-void

    .line 46
    :cond_2
    sget v0, Lcom/google/android/gms/h;->as:I

    goto :goto_0
.end method
