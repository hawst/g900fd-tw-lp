.class public final enum Lcom/google/android/gms/drive/database/model/bu;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum e:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum f:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum g:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum h:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum i:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum j:Lcom/google/android/gms/drive/database/model/bu;

.field public static final enum k:Lcom/google/android/gms/drive/database/model/bu;

.field private static final synthetic m:[Lcom/google/android/gms/drive/database/model/bu;


# instance fields
.field private final l:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const/4 v11, 0x0

    const/4 v10, 0x0

    const/16 v9, 0x1e

    const/4 v8, 0x1

    .line 45
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "ENTRY_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "entryId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    iput-boolean v8, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v4

    invoke-virtual {v3, v4, v11}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v9, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x22

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x22

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "entryId"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v5

    invoke-virtual {v4, v5, v11}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "UNUSED"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "packagingId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v9, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x23

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v8, v2}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->b:Lcom/google/android/gms/drive/database/model/bu;

    .line 64
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "UNUSED_2"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v3, 0x23

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "authorizedApp"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v5

    invoke-virtual {v4, v5, v11}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    new-array v5, v8, [Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    iget-object v6, v6, Lcom/google/android/gms/drive/database/model/bu;->l:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    const/16 v3, 0x24

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v12, v2}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->c:Lcom/google/android/gms/drive/database/model/bu;

    .line 74
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "RESOURCE_ID"

    const/4 v2, 0x3

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x22

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "resourceId"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->d:Lcom/google/android/gms/drive/database/model/bu;

    .line 81
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "AUTHORIZED_APP"

    const/4 v2, 0x4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x24

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "authorizedApp"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v6

    invoke-virtual {v5, v6, v11}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    new-array v6, v12, [Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    iget-object v7, v7, Lcom/google/android/gms/drive/database/model/bu;->l:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    sget-object v7, Lcom/google/android/gms/drive/database/model/bu;->d:Lcom/google/android/gms/drive/database/model/bu;

    iget-object v7, v7, Lcom/google/android/gms/drive/database/model/bu;->l:Lcom/google/android/gms/drive/database/model/ab;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/database/model/au;->a([Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->e:Lcom/google/android/gms/drive/database/model/bu;

    .line 91
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "LAST_ACCESSED"

    const/4 v2, 0x5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "lastAccessed"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->f:Lcom/google/android/gms/drive/database/model/bu;

    .line 99
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "SIZE_BYTES"

    const/4 v2, 0x6

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "size"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->g:Lcom/google/android/gms/drive/database/model/bu;

    .line 106
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "DATABASE_NAME"

    const/4 v2, 0x7

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "databasePath"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->h:Lcom/google/android/gms/drive/database/model/bu;

    .line 114
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "HAS_PENDING_CHANGES"

    const/16 v2, 0x8

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "hasPendingChanges"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    invoke-virtual {v3, v9, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->i:Lcom/google/android/gms/drive/database/model/bu;

    .line 121
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "REQUIRES_SYNC"

    const/16 v2, 0x9

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x25

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "requiresSync"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->j:Lcom/google/android/gms/drive/database/model/bu;

    .line 128
    new-instance v0, Lcom/google/android/gms/drive/database/model/bu;

    const-string v1, "HISTORY_LENGTH"

    const/16 v2, 0xa

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->d()Lcom/google/android/gms/drive/database/model/bt;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    const/16 v4, 0x28

    new-instance v5, Lcom/google/android/gms/drive/database/model/au;

    const-string v6, "historyLength"

    sget-object v7, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v8, v5, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/au;->a()Lcom/google/android/gms/drive/database/model/au;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/bu;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->k:Lcom/google/android/gms/drive/database/model/bu;

    .line 39
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/bu;

    sget-object v1, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v1, v0, v10

    sget-object v1, Lcom/google/android/gms/drive/database/model/bu;->b:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v1, v0, v8

    sget-object v1, Lcom/google/android/gms/drive/database/model/bu;->c:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v1, v0, v12

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->d:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->e:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->f:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->g:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->h:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->i:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->j:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/google/android/gms/drive/database/model/bu;->k:Lcom/google/android/gms/drive/database/model/bu;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/bu;->m:[Lcom/google/android/gms/drive/database/model/bu;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 137
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 138
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/bu;->l:Lcom/google/android/gms/drive/database/model/ab;

    .line 139
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/bu;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/gms/drive/database/model/bu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/bu;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/bu;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/drive/database/model/bu;->m:[Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/bu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/bu;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bu;->l:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/bu;->l:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
