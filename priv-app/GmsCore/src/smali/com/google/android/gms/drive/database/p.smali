.class public final Lcom/google/android/gms/drive/database/p;
.super Lcom/google/android/gms/drive/database/b;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/SortedMap;


# instance fields
.field private final c:Lcom/google/android/gms/drive/g/ae;

.field private d:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 238
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    .line 239
    const/16 v1, 0x26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/database/u;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/u;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/SortedMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSortedMap(Ljava/util/SortedMap;)Ljava/util/SortedMap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/database/p;->b:Ljava/util/SortedMap;

    .line 241
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/g/ae;)V
    .locals 7

    .prologue
    .line 249
    invoke-static {}, Lcom/google/android/gms/drive/database/o;->values()[Lcom/google/android/gms/drive/database/o;

    move-result-object v3

    const/16 v4, 0x2b

    const/4 v5, 0x1

    sget-object v6, Lcom/google/android/gms/drive/database/p;->b:Ljava/util/SortedMap;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/b;-><init>(Landroid/content/Context;Ljava/lang/String;[Lcom/google/android/gms/drive/g/ak;IILjava/util/SortedMap;)V

    .line 245
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/p;->d:J

    .line 252
    iput-object p3, p0, Lcom/google/android/gms/drive/database/p;->c:Lcom/google/android/gms/drive/g/ae;

    .line 253
    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 560
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE VIEW IF NOT EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ar;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS _id, * FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " LEFT OUTER JOIN "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/model/aj;->a()Lcom/google/android/gms/drive/database/model/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aj;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ON ( _id = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/model/aj;->a()Lcom/google/android/gms/drive/database/model/aj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/aj;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/ak;->a:Lcom/google/android/gms/drive/database/model/ak;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ak;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 575
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE VIEW IF NOT EXISTS "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS  SELECT ifnull(sum("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/ba;->c:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), 0) FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/gms/drive/database/p;->c:Lcom/google/android/gms/drive/g/ae;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ae;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/p;->d:J

    .line 591
    return-void
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 259
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v0

    .line 260
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v1

    .line 266
    const-string v2, "EntryView"

    invoke-static {v2}, Lcom/google/android/gms/drive/database/c/e;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/e;

    move-result-object v2

    iput-boolean v7, v2, Lcom/google/android/gms/drive/database/c/e;->a:Z

    invoke-static {v0}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {v3, v1, v4}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/c/d;->b:Z

    iput-object v3, v2, Lcom/google/android/gms/drive/database/c/e;->b:Lcom/google/android/gms/drive/database/c/d;

    .line 272
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/c/e;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 273
    const-string v2, "ScopedEntryView"

    const-string v3, "EntryView"

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 274
    sget-object v2, Lcom/google/android/gms/drive/database/model/as;->z:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    .line 277
    new-instance v3, Lcom/google/android/gms/drive/database/c/a;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/c/a;-><init>()V

    const-string v4, "_id"

    invoke-virtual {v3, v1, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v3

    const-string v4, "sqlId"

    invoke-virtual {v3, v1, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->j:Lcom/google/android/gms/drive/database/model/as;

    const-string v4, "resourceId"

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->a:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->l:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->c:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->g:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/l;->d:Lcom/google/android/gms/drive/metadata/internal/a/o;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->b:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/l;->a:Lcom/google/android/gms/drive/metadata/internal/a/m;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->e:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/l;->e:Lcom/google/android/gms/drive/metadata/internal/a/q;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->o:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->B:Lcom/google/android/gms/drive/metadata/internal/a/j;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->m:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->l:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->f:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->o:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->B:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->b:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->C:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->t:Lcom/google/android/gms/drive/metadata/b;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->D:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->d:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->E:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->k:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->F:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->e:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->G:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->f:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->H:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->g:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->I:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->q:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->J:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->n:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->K:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->s:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->L:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->x:Lcom/google/android/gms/drive/metadata/internal/a/g;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->M:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->C:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->N:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->D:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->O:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->i:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->P:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->h:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->Q:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->G:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->ac:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->F:Lcom/google/android/gms/drive/metadata/internal/c;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->d:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/l;->b:Lcom/google/android/gms/drive/metadata/internal/a/n;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->n:Lcom/google/android/gms/drive/database/model/as;

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->y:Lcom/google/android/gms/drive/metadata/internal/a/h;

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Lcom/google/android/gms/drive/metadata/f;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->S:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " != 0"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->S:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " IS NOT NULL)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/a;->j:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v4}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " IS NOT NULL)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/s;->a:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v4}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "((NOT (LIKE(\'application/vnd.google-apps.folder\',"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->l:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") OR LIKE(\'application/vnd.google-apps%\',"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->l:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "))) AND (NOT "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->o:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ") AND "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/as;->u:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/s;->b:Lcom/google/android/gms/drive/metadata/f;

    invoke-interface {v4}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/database/c/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v1

    .line 329
    new-instance v3, Lcom/google/android/gms/drive/database/c/a;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/c/a;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/database/model/cg;->a:Lcom/google/android/gms/drive/database/model/cg;

    const-string v5, "permissionId"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/cg;->b:Lcom/google/android/gms/drive/database/model/cg;

    const-string v5, "displayName"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/cg;->d:Lcom/google/android/gms/drive/database/model/cg;

    const-string v5, "picture"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/cg;->e:Lcom/google/android/gms/drive/database/model/cg;

    const-string v5, "isAuthenticatedUser"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/drive/database/model/cg;->c:Lcom/google/android/gms/drive/database/model/cg;

    const-string v5, "emailAddress"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/g/ak;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/a;

    move-result-object v3

    .line 337
    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v4

    .line 338
    invoke-static {v0}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->s:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/g/ak;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->R:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/g/ak;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/g/ak;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->c:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/g/ak;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v5

    iget-object v6, v5, Lcom/google/android/gms/drive/database/c/d;->a:Lcom/google/android/gms/drive/database/c/a;

    invoke-virtual {v6, v1}, Lcom/google/android/gms/drive/database/c/a;->a(Lcom/google/android/gms/drive/database/c/a;)Lcom/google/android/gms/drive/database/c/a;

    const-string v1, "lastModifyingUserJoin"

    sget-object v6, Lcom/google/android/gms/drive/metadata/internal/a/a;->u:Lcom/google/android/gms/drive/metadata/internal/n;

    iget-object v6, v6, Lcom/google/android/gms/drive/metadata/a;->a:Ljava/lang/String;

    invoke-virtual {v5, v3, v1, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/database/c/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v1

    const-string v5, "sharingUserJoin"

    sget-object v6, Lcom/google/android/gms/drive/metadata/internal/a/a;->v:Lcom/google/android/gms/drive/metadata/internal/n;

    iget-object v6, v6, Lcom/google/android/gms/drive/metadata/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v5, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/database/c/a;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/database/c/b;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/b;

    move-result-object v3

    const-string v5, "sharingUserJoin"

    iput-object v5, v3, Lcom/google/android/gms/drive/database/c/b;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sharingUserJoin."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/drive/database/model/as;->ab:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v3, Lcom/google/android/gms/drive/database/c/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/database/c/b;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/cf;->a()Lcom/google/android/gms/drive/database/model/cf;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/drive/database/c/b;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/b;

    move-result-object v3

    const-string v5, "lastModifyingUserJoin"

    iput-object v5, v3, Lcom/google/android/gms/drive/database/c/b;->b:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "lastModifyingUserJoin."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/drive/database/model/as;->aa:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/drive/database/c/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/database/c/b;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v1

    .line 367
    const-string v3, "MetadataView"

    invoke-static {v3}, Lcom/google/android/gms/drive/database/c/e;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/e;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/c/e;->a:Z

    iput-object v1, v3, Lcom/google/android/gms/drive/database/c/e;->b:Lcom/google/android/gms/drive/database/c/d;

    .line 368
    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/c/e;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 369
    const-string v1, "ScopedMetadataView"

    const-string v3, "MetadataView"

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/database/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 372
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/q;->a:Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/q;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 376
    const-string v3, "AuthorizedAppScopeView"

    invoke-static {v3}, Lcom/google/android/gms/drive/database/c/e;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/e;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/c/e;->a:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v5

    const-string v6, "_id"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v4

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/c/d;->b:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/p;->a()Lcom/google/android/gms/drive/database/model/p;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/drive/database/c/b;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/b;

    move-result-object v5

    iput-object v1, v5, Lcom/google/android/gms/drive/database/c/b;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/database/c/b;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/drive/database/c/e;->b:Lcom/google/android/gms/drive/database/c/d;

    .line 384
    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/c/e;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 387
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/drive/database/model/h;->b:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/h;->a:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/q;->b:Lcom/google/android/gms/drive/database/model/q;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/q;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/aa;->b:Lcom/google/android/gms/drive/aa;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/aa;->a()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 397
    const-string v3, "AppDataScopeView"

    invoke-static {v3}, Lcom/google/android/gms/drive/database/c/e;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/e;

    move-result-object v3

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/c/e;->a:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v4

    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v5

    const-string v6, "_id"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/drive/database/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v4

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/c/d;->b:Z

    const-string v5, "AuthorizedAppScopeView"

    invoke-static {v5}, Lcom/google/android/gms/drive/database/c/b;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/c/b;

    move-result-object v5

    iput-object v1, v5, Lcom/google/android/gms/drive/database/c/b;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/c/d;->a(Lcom/google/android/gms/drive/database/c/b;)Lcom/google/android/gms/drive/database/c/d;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/drive/database/c/e;->b:Lcom/google/android/gms/drive/database/c/d;

    .line 407
    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/c/e;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 411
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS ParentCollectionView AS SELECT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AS _id,"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/s;->b:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AS childId, * FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " INNER JOIN "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ON ( "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/r;->a()Lcom/google/android/gms/drive/database/model/r;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/s;->a:Lcom/google/android/gms/drive/database/model/s;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/s;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 421
    const-string v1, "ScopedParentCollectionView"

    const-string v3, "ParentCollectionView"

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/database/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 423
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS CannotDeleteFileContentHashView AS SELECT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "=1 UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/bk;->a:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/bk;->h:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/bo;->b:Lcom/google/android/gms/drive/database/model/bo;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bo;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bn;->a()Lcom/google/android/gms/drive/database/model/bn;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 442
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS ReferencedFileContentHashView AS SELECT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/bk;->a:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/bk;->h:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/bo;->b:Lcom/google/android/gms/drive/database/model/bo;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bo;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bn;->a()Lcom/google/android/gms/drive/database/model/bn;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 457
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS WipeoutFileContentHashView AS SELECT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " EXCEPT SELECT * FROM ReferencedFileContentHashView;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 461
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS CachedFileContentHashView AS SELECT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " EXCEPT SELECT * FROM CannotDeleteFileContentHashView EXCEPT SELECT * FROM WipeoutFileContentHashView"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ";"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 466
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS CannotDeleteFilenameView AS SELECT "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->d:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AS filename FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AS filename FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " UNION ALL SELECT "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/v;->a:Lcom/google/android/gms/drive/database/model/v;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/v;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " AS filename FROM "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/drive/database/model/u;->a()Lcom/google/android/gms/drive/database/model/u;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " WHERE filename IS NOT NULL;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 478
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS InternalContentView AS SELECT * FROM "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->d:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IS NOT NULL ORDER BY "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->b:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ASC;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 484
    const-string v1, "InternalContentSizeView"

    const-string v3, "InternalContentView"

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/database/p;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 485
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS SharedContentView AS SELECT * FROM "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/az;->a()Lcom/google/android/gms/drive/database/model/az;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IS NOT NULL ORDER BY "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->b:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ASC;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 491
    const-string v1, "SharedContentSizeView"

    const-string v3, "SharedContentView"

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/database/p;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 492
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS InternalCachedContentView AS SELECT * FROM InternalContentView WHERE "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN CachedFileContentHashView ORDER BY "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->e:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IS NOT NULL , "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->b:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " ASC ;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 500
    const-string v1, "InternalCachedContentSizeView"

    const-string v3, "InternalCachedContentView"

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/database/p;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 502
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS SharedCachedContentView AS SELECT * FROM SharedContentView WHERE "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/drive/database/model/ba;->a:Lcom/google/android/gms/drive/database/model/ba;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ba;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " IN CachedFileContentHashView;"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 506
    const-string v1, "SharedCachedContentSizeView"

    const-string v3, "SharedCachedContentView"

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/database/p;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 509
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "CREATE VIEW IF NOT EXISTS PinnedNoLocalContentView AS SELECT * FROM "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " WHERE "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " = 1 AND "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IS NULL;"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 517
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CREATE VIEW IF NOT EXISTS PinnedOutOfDateView AS SELECT * FROM "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " WHERE "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/as;->p:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " = 1 AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/as;->T:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " != "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/as;->ad:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ae;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " NOT IN (SELECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/database/model/bk;->b:Lcom/google/android/gms/drive/database/model/bk;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bk;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " FROM "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bj;->a()Lcom/google/android/gms/drive/database/model/bj;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ");"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 527
    const-string v0, "CREATE VIEW IF NOT EXISTS PinnedDownloadRequiredView AS SELECT * FROM PinnedNoLocalContentView UNION ALL SELECT * FROM PinnedOutOfDateView;"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 532
    sget-object v0, Lcom/google/android/gms/drive/database/model/as;->A:Lcom/google/android/gms/drive/database/model/as;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/as;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    .line 534
    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v1

    .line 538
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DELETE FROM "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/drive/database/model/ar;->a()Lcom/google/android/gms/drive/database/model/ar;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/ae;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IS NOT NULL AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " NOT IN (SELECT "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/drive/database/model/h;->b:Lcom/google/android/gms/drive/database/model/h;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/h;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " FROM "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ");"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 547
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CREATE TRIGGER IF NOT EXISTS OnDeleteAppAuthMetadata AFTER DELETE ON "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " BEGIN "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " END;"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 550
    return-void
.end method

.method final b()J
    .locals 4

    .prologue
    .line 581
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/p;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 582
    iget-wide v0, p0, Lcom/google/android/gms/drive/database/p;->d:J

    .line 585
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/p;->c:Lcom/google/android/gms/drive/g/ae;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/ae;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/database/p;->d:J

    goto :goto_0
.end method
