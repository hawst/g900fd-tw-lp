.class public final Lcom/google/android/gms/wallet/service/ow/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/wallet/common/b/a;

.field final b:Lcom/google/android/gms/wallet/service/ow/an;

.field private final c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/b/a;Lcom/google/android/gms/wallet/service/ow/an;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/ak;->c:Landroid/content/Context;

    .line 74
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/ak;->a:Lcom/google/android/gms/wallet/common/b/a;

    .line 75
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/ak;->b:Lcom/google/android/gms/wallet/service/ow/an;

    .line 76
    return-void
.end method


# virtual methods
.method final a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    .prologue
    .line 164
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/wallet/shared/f;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 165
    iget-object v7, p0, Lcom/google/android/gms/wallet/service/ow/ak;->c:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/al;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/al;-><init>(Lcom/google/android/gms/wallet/service/ow/ak;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Lcom/google/protobuf/nano/j;II)V

    invoke-static {v7, v0, p6}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method

.method final a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Ljava/util/ArrayList;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    .prologue
    .line 180
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/wallet/shared/f;->b(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 181
    iget-object v7, p0, Lcom/google/android/gms/wallet/service/ow/ak;->c:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/wallet/service/ow/am;

    move-object v1, p0

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/am;-><init>(Lcom/google/android/gms/wallet/service/ow/ak;Ljava/lang/String;Lcom/google/android/gms/wallet/a/b;Ljava/util/ArrayList;II)V

    invoke-static {v7, v0, p6}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/gms/common/util/w;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    return-object v0
.end method
