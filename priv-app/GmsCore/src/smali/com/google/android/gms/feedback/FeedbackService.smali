.class public Lcom/google/android/gms/feedback/FeedbackService;
.super Landroid/app/Service;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 159
    return-void
.end method

.method public static a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/ErrorReport;Ljava/io/File;)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 67
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 68
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    .line 71
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    array-length v0, v0

    if-eqz v0, :cond_1

    .line 72
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    const/4 v1, 0x0

    iget-object v2, p1, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    array-length v2, v2

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 74
    invoke-static {v0}, Lcom/google/android/gms/feedback/Screenshot;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/Screenshot;

    move-result-object v1

    .line 75
    if-eqz v0, :cond_1

    .line 76
    invoke-static {p0, v1}, Lcom/google/android/gms/feedback/e;->a(Lcom/google/android/gms/feedback/ErrorReport;Lcom/google/android/gms/feedback/Screenshot;)V

    .line 80
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 81
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    .line 84
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 85
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    .line 88
    :cond_3
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->J:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    new-instance v1, Landroid/app/ApplicationErrorReport$CrashInfo;

    invoke-direct {v1}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>()V

    iput-object v1, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->K:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->N:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->P:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->J:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->O:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->M:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->L:I

    iput v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    const/4 v1, 0x1

    iput v1, v0, Landroid/app/ApplicationErrorReport;->type:I

    .line 108
    :cond_4
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 109
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    .line 112
    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->R:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 113
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->R:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->R:Ljava/lang/String;

    .line 115
    :cond_6
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    if-eqz v0, :cond_7

    .line 116
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    .line 118
    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, p1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    .line 123
    :cond_8
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    if-eqz v0, :cond_9

    .line 124
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/BitmapTeleporter;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 125
    invoke-static {p2, v0}, Lcom/google/android/gms/feedback/Screenshot;->a(Ljava/io/File;Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    .line 127
    :cond_9
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->V:[Lcom/google/android/gms/feedback/FileTeleporter;

    if-eqz v0, :cond_a

    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->V:[Lcom/google/android/gms/feedback/FileTeleporter;

    array-length v0, v0

    if-eqz v0, :cond_a

    .line 129
    iget-object v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->V:[Lcom/google/android/gms/feedback/FileTeleporter;

    invoke-static {p2, v0}, Lcom/google/android/gms/feedback/w;->a(Ljava/io/File;[Lcom/google/android/gms/feedback/FileTeleporter;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    .line 132
    :cond_a
    iget-boolean v0, p1, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    iput-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    .line 133
    return-object p0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 58
    const-string v0, "com.google.android.gms.feedback.internal.IFeedbackService"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Lcom/google/android/gms/feedback/s;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/feedback/s;-><init>(Lcom/google/android/gms/feedback/FeedbackService;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/s;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
