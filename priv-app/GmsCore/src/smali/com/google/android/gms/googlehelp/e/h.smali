.class public final Lcom/google/android/gms/googlehelp/e/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1256
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1257
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/googlehelp/e/h;->cachedSize:I

    .line 1258
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1330
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1331
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1332
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1335
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1336
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1339
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1340
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1343
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1270
    if-ne p1, p0, :cond_1

    .line 1298
    :cond_0
    :goto_0
    return v0

    .line 1273
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/e/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 1274
    goto :goto_0

    .line 1276
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/e/h;

    .line 1277
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1278
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1279
    goto :goto_0

    .line 1281
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1282
    goto :goto_0

    .line 1284
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1285
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1286
    goto :goto_0

    .line 1288
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1289
    goto :goto_0

    .line 1291
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1292
    iget-object v2, p1, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1293
    goto :goto_0

    .line 1295
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1296
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1303
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1306
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1308
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1310
    return v0

    .line 1303
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1306
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1308
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1230
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1316
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1317
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1319
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1320
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1322
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1323
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1325
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1326
    return-void
.end method
