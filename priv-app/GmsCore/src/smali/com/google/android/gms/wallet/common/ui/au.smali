.class public final Lcom/google/android/gms/wallet/common/ui/au;
.super Lcom/google/android/gms/wallet/common/ui/at;
.source "SourceFile"


# instance fields
.field private c:Landroid/widget/ImageView;

.field private d:[Landroid/view/animation/Animation;

.field private e:[Landroid/view/animation/Animation;

.field private f:[Landroid/view/animation/Animation;

.field private g:[Landroid/view/animation/Animation;

.field private h:[Z


# direct methods
.method public constructor <init>(Landroid/content/Context;[Landroid/widget/ImageView;Landroid/widget/ImageView;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 28
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/at;-><init>([Landroid/widget/ImageView;)V

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/ui/au;->c:Landroid/widget/ImageView;

    .line 33
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 37
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Landroid/view/animation/Animation;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->d:[Landroid/view/animation/Animation;

    .line 38
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Landroid/view/animation/Animation;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->e:[Landroid/view/animation/Animation;

    .line 39
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Landroid/view/animation/Animation;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->f:[Landroid/view/animation/Animation;

    .line 40
    array-length v0, p2

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Landroid/view/animation/Animation;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->g:[Landroid/view/animation/Animation;

    .line 41
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    if-ge v0, v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->d:[Landroid/view/animation/Animation;

    sget v2, Lcom/google/android/gms/b;->A:I

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    aput-object v2, v1, v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->e:[Landroid/view/animation/Animation;

    sget v2, Lcom/google/android/gms/b;->B:I

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    aput-object v2, v1, v0

    .line 44
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->f:[Landroid/view/animation/Animation;

    sget v2, Lcom/google/android/gms/b;->A:I

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    aput-object v2, v1, v0

    .line 45
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->f:[Landroid/view/animation/Animation;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 46
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->g:[Landroid/view/animation/Animation;

    sget v2, Lcom/google/android/gms/b;->B:I

    invoke-static {p1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    aput-object v2, v1, v0

    .line 47
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->g:[Landroid/view/animation/Animation;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 41
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 53
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->b:I

    if-eq p1, v0, :cond_5

    .line 54
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/au;->c(I)I

    move-result v2

    move v0, v1

    .line 55
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 56
    if-ne v0, v2, :cond_1

    .line 57
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aget-boolean v3, v3, v0

    if-nez v3, :cond_0

    .line 58
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/au;->d:[Landroid/view/animation/Animation;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 60
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aput-boolean v5, v3, v0

    .line 55
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 62
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_2

    .line 63
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/au;->e:[Landroid/view/animation/Animation;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 65
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aput-boolean v1, v3, v0

    goto :goto_1

    .line 68
    :cond_3
    const/4 v0, -0x1

    if-ne v2, v0, :cond_6

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v1, v1

    aget-boolean v0, v0, v1

    if-nez v0, :cond_4

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->d:[Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v2, v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 72
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v1, v1

    aput-boolean v5, v0, v1

    .line 79
    :goto_2
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/au;->b:I

    .line 81
    :cond_5
    return-void

    .line 74
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v2, v2

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_7

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->e:[Landroid/view/animation/Animation;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v3, v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 77
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v2, v2

    aput-boolean v1, v0, v2

    goto :goto_2
.end method

.method public final b(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 85
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/au;->c(I)I

    move-result v2

    move v0, v1

    .line 86
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 87
    if-ne v0, v2, :cond_1

    .line 88
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aget-boolean v3, v3, v0

    if-nez v3, :cond_0

    .line 89
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/au;->f:[Landroid/view/animation/Animation;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 91
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aput-boolean v5, v3, v0

    .line 86
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aget-boolean v3, v3, v0

    if-eqz v3, :cond_2

    .line 94
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/au;->g:[Landroid/view/animation/Animation;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 96
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    aput-boolean v1, v3, v0

    goto :goto_1

    .line 99
    :cond_3
    const/4 v0, -0x1

    if-ne v2, v0, :cond_5

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v1, v1

    aget-boolean v0, v0, v1

    if-nez v0, :cond_4

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->c:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->f:[Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v2, v2

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 103
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v1, v1

    aput-boolean v5, v0, v1

    .line 110
    :goto_2
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/au;->b:I

    .line 111
    return-void

    .line 105
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v2, v2

    aget-boolean v0, v0, v2

    if-eqz v0, :cond_6

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->c:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->g:[Landroid/view/animation/Animation;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v3, v3

    aget-object v2, v2, v3

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 108
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/au;->h:[Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/au;->a:[Landroid/widget/ImageView;

    array-length v2, v2

    aput-boolean v1, v0, v2

    goto :goto_2
.end method
