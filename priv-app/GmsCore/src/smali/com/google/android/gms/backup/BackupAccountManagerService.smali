.class public Lcom/google/android/gms/backup/BackupAccountManagerService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "BackupAccountManagerService"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/BackupAccountManagerService;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 35
    return-void
.end method

.method static a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 104
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.backup.GmsBackupAccountManagerService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 106
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/app/GmsApplication;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveService(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/google/android/gms/backup/BackupAccountManagerService;->a:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/backup/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/a;-><init>(Landroid/content/Context;)V

    return-object v0
.end method
