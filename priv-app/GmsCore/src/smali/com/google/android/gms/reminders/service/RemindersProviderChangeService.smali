.class public Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    const-string v0, "RemindersProviderChangeService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 40
    return-void
.end method

.method public static a(Landroid/content/Context;[J)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 104
    const-string v1, "extra_task_ids"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 105
    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.reminders.BIND_LISTENER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 57
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 58
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 59
    iget-object v2, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 60
    new-instance v3, Landroid/content/ComponentName;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    invoke-direct {v3, v2, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-static {}, Lcom/google/android/gms/reminders/packagemanager/a;->a()Lcom/google/android/gms/reminders/packagemanager/a;

    move-result-object v0

    invoke-virtual {v0, p0, v2}, Lcom/google/android/gms/reminders/packagemanager/a;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 67
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 68
    const-string v2, "com.google.android.gms.reminders.BIND_LISTENER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 70
    new-instance v2, Lcom/google/android/gms/common/b;

    invoke-direct {v2}, Lcom/google/android/gms/common/b;-><init>()V

    .line 71
    invoke-virtual {p0, v0, v2, v5}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 74
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/reminders/internal/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/reminders/internal/e;

    move-result-object v0

    .line 75
    invoke-interface {v0, p1}, Lcom/google/android/gms/reminders/internal/e;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 76
    :catch_0
    move-exception v0

    .line 77
    const/4 v3, 0x1

    :try_start_1
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->d()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 81
    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 78
    :catch_1
    move-exception v0

    .line 79
    const/4 v3, 0x1

    :try_start_2
    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {}, Lcom/google/android/gms/reminders/c/e;->d()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 81
    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->unbindService(Landroid/content/ServiceConnection;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0, v2}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->unbindService(Landroid/content/ServiceConnection;)V

    throw v0

    .line 83
    :cond_1
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 44
    const-string v0, "extra_task_ids"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v0

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "reminders._id IN ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ","

    invoke-static {v2, v0}, Lcom/google/android/gms/reminders/c/a;->a(Ljava/lang/CharSequence;[J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/reminders/internal/a/j;->a:Landroid/net/Uri;

    invoke-static {p0, v2, v1, v4}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 46
    invoke-static {}, Lcom/google/android/gms/reminders/service/b;->a()Lcom/google/android/gms/reminders/service/b;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/reminders/service/b;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 47
    invoke-direct {p0, v1}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 48
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reminders._id IN ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, ","

    invoke-static {v3, v0}, Lcom/google/android/gms/reminders/c/a;->a(Ljava/lang/CharSequence;[J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") AND deleted=1 AND is_dirty"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "=0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/reminders/service/RemindersProviderChangeService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 49
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 50
    return-void
.end method
