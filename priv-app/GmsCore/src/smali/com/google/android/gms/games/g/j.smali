.class public final Lcom/google/android/gms/games/g/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/games/g/m;

.field public c:Lcom/google/android/gms/games/g/k;

.field public d:Lcom/google/android/gms/games/g/q;

.field public e:Lcom/google/android/gms/games/g/p;

.field public f:Lcom/google/android/gms/games/g/o;

.field public g:Lcom/google/android/gms/games/g/n;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1526
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1527
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/j;->a:I

    iput-object v1, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    iput-object v1, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    iput-object v1, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    iput-object v1, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    iput-object v1, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    iput-object v1, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/j;->cachedSize:I

    .line 1528
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1672
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1673
    iget v1, p0, Lcom/google/android/gms/games/g/j;->a:I

    if-eqz v1, :cond_0

    .line 1674
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/j;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1677
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    if-eqz v1, :cond_1

    .line 1678
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1681
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    if-eqz v1, :cond_2

    .line 1682
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1685
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    if-eqz v1, :cond_3

    .line 1686
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1689
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    if-eqz v1, :cond_4

    .line 1690
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1693
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    if-eqz v1, :cond_5

    .line 1694
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1697
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    if-eqz v1, :cond_6

    .line 1698
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1701
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1702
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1705
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1545
    if-ne p1, p0, :cond_1

    .line 1616
    :cond_0
    :goto_0
    return v0

    .line 1548
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 1549
    goto :goto_0

    .line 1551
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/j;

    .line 1552
    iget v2, p0, Lcom/google/android/gms/games/g/j;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/j;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1553
    goto :goto_0

    .line 1555
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    if-nez v2, :cond_4

    .line 1556
    iget-object v2, p1, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    if-eqz v2, :cond_5

    move v0, v1

    .line 1557
    goto :goto_0

    .line 1560
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    iget-object v3, p1, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/m;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1561
    goto :goto_0

    .line 1564
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    if-nez v2, :cond_6

    .line 1565
    iget-object v2, p1, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    if-eqz v2, :cond_7

    move v0, v1

    .line 1566
    goto :goto_0

    .line 1569
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    iget-object v3, p1, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1570
    goto :goto_0

    .line 1573
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    if-nez v2, :cond_8

    .line 1574
    iget-object v2, p1, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    if-eqz v2, :cond_9

    move v0, v1

    .line 1575
    goto :goto_0

    .line 1578
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    iget-object v3, p1, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1579
    goto :goto_0

    .line 1582
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    if-nez v2, :cond_a

    .line 1583
    iget-object v2, p1, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    if-eqz v2, :cond_b

    move v0, v1

    .line 1584
    goto :goto_0

    .line 1587
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    iget-object v3, p1, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1588
    goto :goto_0

    .line 1591
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    if-nez v2, :cond_c

    .line 1592
    iget-object v2, p1, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    if-eqz v2, :cond_d

    move v0, v1

    .line 1593
    goto :goto_0

    .line 1596
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    iget-object v3, p1, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1597
    goto :goto_0

    .line 1600
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    if-nez v2, :cond_e

    .line 1601
    iget-object v2, p1, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    if-eqz v2, :cond_f

    move v0, v1

    .line 1602
    goto/16 :goto_0

    .line 1605
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    iget-object v3, p1, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 1606
    goto/16 :goto_0

    .line 1609
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 1610
    iget-object v2, p1, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1611
    goto/16 :goto_0

    .line 1613
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1614
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1621
    iget v0, p0, Lcom/google/android/gms/games/g/j;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1623
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1625
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1627
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1629
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1631
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1633
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 1635
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 1637
    return v0

    .line 1623
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/m;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1625
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/k;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1627
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/q;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1629
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/p;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1631
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/o;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1633
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/n;->hashCode()I

    move-result v0

    goto :goto_5

    .line 1635
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1471
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/j;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/m;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/m;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/g/k;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/g/q;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/games/g/p;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/games/g/o;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/gms/games/g/n;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1643
    iget v0, p0, Lcom/google/android/gms/games/g/j;->a:I

    if-eqz v0, :cond_0

    .line 1644
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/j;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1646
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    if-eqz v0, :cond_1

    .line 1647
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->b:Lcom/google/android/gms/games/g/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1649
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    if-eqz v0, :cond_2

    .line 1650
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->c:Lcom/google/android/gms/games/g/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1652
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    if-eqz v0, :cond_3

    .line 1653
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->d:Lcom/google/android/gms/games/g/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1655
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    if-eqz v0, :cond_4

    .line 1656
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->e:Lcom/google/android/gms/games/g/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1658
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    if-eqz v0, :cond_5

    .line 1659
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->f:Lcom/google/android/gms/games/g/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1661
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    if-eqz v0, :cond_6

    .line 1662
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->g:Lcom/google/android/gms/games/g/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1664
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1665
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/games/g/j;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1667
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1668
    return-void
.end method
