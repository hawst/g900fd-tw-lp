.class public final Lcom/google/android/gms/deviceconnection/service/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/deviceconnection/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/deviceconnection/c/c;

.field private final b:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/deviceconnection/c/c;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/gms/deviceconnection/service/a/b;->a:Lcom/google/android/gms/deviceconnection/c/c;

    .line 22
    iput-object p2, p0, Lcom/google/android/gms/deviceconnection/service/a/b;->b:[Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/deviceconnection/a/a;)V
    .locals 3

    .prologue
    .line 28
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/a/b;->a:Lcom/google/android/gms/deviceconnection/c/c;

    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/service/a/b;->b:[Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/deviceconnection/a/a;->a([Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/deviceconnection/c/c;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 33
    :goto_0
    return-void

    .line 29
    :catch_0
    move-exception v0

    .line 30
    const-string v1, "UpdateDevFeaturesOp"

    const-string v2, "When providing result "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
