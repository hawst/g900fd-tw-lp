.class public Lcom/google/android/gms/games/ui/common/quests/QuestDetailFragment;
.super Lcom/google/android/gms/games/ui/common/quests/a;
.source "SourceFile"


# instance fields
.field private m:Lcom/google/android/gms/games/ui/common/quests/f;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/quests/a;-><init>()V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/bh;Z)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestDetailFragment;->m:Lcom/google/android/gms/games/ui/common/quests/f;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/quests/f;->V()Ljava/lang/String;

    move-result-object v0

    .line 36
    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/bh;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 37
    sget-object v1, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    new-array v2, v4, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-interface {v1, p1, v2}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 44
    :goto_0
    return-void

    .line 40
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->q:Lcom/google/android/gms/games/quest/e;

    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v3

    new-array v4, v4, [Ljava/lang/String;

    aput-object v0, v4, v5

    invoke-interface {v1, p1, v2, v3, v4}, Lcom/google/android/gms/games/quest/e;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/quests/a;->onActivityCreated(Landroid/os/Bundle;)V

    .line 27
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestDetailFragment;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/quests/f;

    const-string v1, "Parent activity did not implement QuestDetailMetadataProvider"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 29
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestDetailFragment;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/quests/f;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/QuestDetailFragment;->m:Lcom/google/android/gms/games/ui/common/quests/f;

    .line 30
    return-void
.end method
