.class public final Lcom/google/android/gms/cast/c/e;
.super Lcom/google/android/gms/cast/c/q;
.source "SourceFile"


# static fields
.field private static final g:I

.field private static final h:[B

.field private static i:Lcom/google/android/gms/cast/CastDevice;

.field private static j:Ljava/lang/Object;


# instance fields
.field private final k:Landroid/net/wifi/WifiManager;

.field private final l:[Ljava/lang/String;

.field private m:I

.field private n:Z

.field private final o:Ljava/util/HashMap;

.field private final p:Landroid/content/BroadcastReceiver;

.field private final q:Landroid/content/IntentFilter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/cast/a/b;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/e;->g:I

    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/cast/c/e;->h:[B

    .line 38
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/c/e;->j:Ljava/lang/Object;

    return-void

    .line 35
    :array_0
    .array-data 1
        0x7ft
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 104
    const-string v0, "CastNearby"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/cast/c/q;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 43
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    .line 46
    new-instance v0, Lcom/google/android/gms/cast/c/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/cast/c/f;-><init>(Lcom/google/android/gms/cast/c/e;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/e;->p:Landroid/content/BroadcastReceiver;

    .line 95
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/e;->q:Landroid/content/IntentFilter;

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/c/e;->n:Z

    .line 107
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/c;->g:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/e;->l:[Ljava/lang/String;

    .line 109
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/e;->g()V

    .line 110
    const-string v0, "wifi"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/google/android/gms/cast/c/e;->k:Landroid/net/wifi/WifiManager;

    .line 111
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/e;)V
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/g;

    iget v2, v0, Lcom/google/android/gms/cast/c/g;->d:I

    add-int/lit8 v2, v2, -0x1

    iput v2, v0, Lcom/google/android/gms/cast/c/g;->d:I

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/e;Z)Z
    .locals 0

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/google/android/gms/cast/c/e;->n:Z

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/cast/c/e;)Landroid/net/wifi/WifiManager;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->k:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/c/e;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->l:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d()I
    .locals 1

    .prologue
    .line 31
    sget v0, Lcom/google/android/gms/cast/c/e;->g:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/c/e;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    return-object v0
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 209
    .line 211
    :try_start_0
    const-string v0, ""

    sget-object v2, Lcom/google/android/gms/cast/c/e;->h:[B

    invoke-static {v0, v2}, Ljava/net/InetAddress;->getByAddress(Ljava/lang/String;[B)Ljava/net/InetAddress;

    move-result-object v0

    .line 212
    instance-of v2, v0, Ljava/net/Inet4Address;

    if-eqz v2, :cond_0

    .line 213
    check-cast v0, Ljava/net/Inet4Address;
    :try_end_0
    .catch Ljava/net/UnknownHostException; {:try_start_0 .. :try_end_0} :catch_0

    .line 219
    :goto_0
    const-string v1, "%s_%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "__cast_nearby__"

    aput-object v3, v2, v5

    iget v3, p0, Lcom/google/android/gms/cast/c/e;->m:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/gms/cast/c/e;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 221
    iget-object v2, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "refreshDevice, newDeviceid=[%s]."

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 222
    sget-object v2, Lcom/google/android/gms/cast/c/e;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 223
    :try_start_1
    invoke-static {v1, v0}, Lcom/google/android/gms/cast/CastDevice;->a(Ljava/lang/String;Ljava/net/Inet4Address;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->dR:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/c;->a(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->dS:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/c;->b(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->dP:I

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/c;->c(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    sput-object v0, Lcom/google/android/gms/cast/c/e;->i:Lcom/google/android/gms/cast/CastDevice;

    .line 228
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void

    .line 216
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 228
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/cast/CastDevice;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/g;

    .line 276
    iget-object v2, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 277
    iget-object v0, v0, Lcom/google/android/gms/cast/c/g;->c:Ljava/lang/String;

    .line 280
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a()V
    .locals 3

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "stopScanInternal"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 134
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/e;->p:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 138
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/g;

    .line 153
    iget-object v2, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 154
    iget-object v2, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/cast/c/e;->d(Lcom/google/android/gms/cast/CastDevice;)V

    .line 156
    iget-object v2, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "deviceInvalidInternal: found device %s, %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v6}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v6}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 160
    iget-object v2, v0, Lcom/google/android/gms/cast/c/g;->b:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/cast/e/i;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 161
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 166
    iget-object v3, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 167
    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/cast/CastDevice;->a(Ljava/lang/String;Ljava/net/Inet4Address;)Lcom/google/android/gms/cast/c;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/cast/c;->a(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/cast/c;->b(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->f()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/cast/c;->c(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/cast/c;->a(I)Lcom/google/android/gms/cast/c;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->h()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/cast/c;->a(Ljava/util/List;)Lcom/google/android/gms/cast/c;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->i()I

    move-result v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/cast/c;->b(I)Lcom/google/android/gms/cast/c;

    move-result-object v2

    invoke-virtual {v3}, Lcom/google/android/gms/cast/CastDevice;->j()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/cast/c;->c(I)Lcom/google/android/gms/cast/c;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 177
    iput-object v2, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    goto/16 :goto_0

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->k:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 183
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v6, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "associateBssidAndDevice, [%s]:[%s]"

    new-array v2, v6, [Ljava/lang/Object;

    aput-object p1, v2, v8

    aput-object p2, v2, v9

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 235
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->c()Ljava/net/Inet4Address;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/cast/CastDevice;->a(Ljava/lang/String;Ljava/net/Inet4Address;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/cast/c;->a(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c;->b(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c;->c(Ljava/lang/String;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->g()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c;->a(I)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->h()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c;->a(Ljava/util/List;)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->i()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c;->b(I)Lcom/google/android/gms/cast/c;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->j()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c;->c(I)Lcom/google/android/gms/cast/c;

    move-result-object v0

    iget-object v2, v0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    .line 245
    new-instance v3, Lcom/google/android/gms/cast/c/g;

    invoke-direct {v3, v1, v2, p3}, Lcom/google/android/gms/cast/c/g;-><init>(Ljava/lang/String;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/g;

    .line 250
    if-eqz v0, :cond_2

    .line 251
    iget-object v4, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 252
    iget-object v4, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v5, "associateBssidAndDevice, preexisting device detected [%s]:[%s]"

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, v0, Lcom/google/android/gms/cast/c/g;->b:Ljava/lang/String;

    aput-object v7, v6, v8

    iget-object v7, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    aput-object v7, v6, v9

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 256
    :cond_1
    iget-object v4, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 257
    iget-object v2, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v4, "associateBssidAndDevice, notifyDeviceOffline device detected [%s]"

    new-array v5, v9, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    aput-object v6, v5, v8

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    iget-object v0, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/e;->d(Lcom/google/android/gms/cast/CastDevice;)V

    .line 263
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 265
    sget-object v1, Lcom/google/android/gms/cast/c/e;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 266
    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/c/e;->i:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v0

    .line 267
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 268
    invoke-virtual {p2}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 269
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/e;->g()V

    .line 271
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->k:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 272
    return-void

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final a(Ljava/util/List;Z)V
    .locals 5

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "startScanInternal: %d network interfaces"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/cast/c/q;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/e;->p:Landroid/content/BroadcastReceiver;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/e;->q:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->k:Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->startScan()Z

    .line 128
    return-void
.end method

.method protected final a(Z)V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method protected final a(Ljava/net/NetworkInterface;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 116
    :try_start_0
    invoke-virtual {p1}, Ljava/net/NetworkInterface;->isLoopback()Z
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 118
    :cond_0
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 147
    return-void
.end method

.method public final c()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 284
    sget-object v1, Lcom/google/android/gms/cast/c/e;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 285
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/e;->n:Z

    if-eqz v0, :cond_1

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "updateCastNearbyRoute: adding mUnpairedDeviceNearby %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    sget-object v5, Lcom/google/android/gms/cast/c/e;->i:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    sget-object v0, Lcom/google/android/gms/cast/c/e;->i:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/e;->b(Lcom/google/android/gms/cast/CastDevice;)V

    .line 292
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 293
    iget-object v0, p0, Lcom/google/android/gms/cast/c/e;->o:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/g;

    .line 294
    iget-object v2, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 295
    iget-object v2, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "updateCastNearbyRoute: %s - %d"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    aput-object v5, v4, v6

    iget v5, v0, Lcom/google/android/gms/cast/c/g;->d:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 298
    :cond_0
    iget v2, v0, Lcom/google/android/gms/cast/c/g;->d:I

    if-lez v2, :cond_2

    .line 299
    iget-object v2, p0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "updateCastNearbyRoute: adding Paired device %s, %s"

    new-array v4, v8, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    iget-object v5, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 302
    iget-object v0, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/e;->b(Lcom/google/android/gms/cast/CastDevice;)V

    goto :goto_1

    .line 290
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/cast/c/e;->i:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/e;->d(Lcom/google/android/gms/cast/CastDevice;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 304
    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/e;->d(Lcom/google/android/gms/cast/CastDevice;)V

    goto :goto_1

    .line 307
    :cond_3
    return-void
.end method
