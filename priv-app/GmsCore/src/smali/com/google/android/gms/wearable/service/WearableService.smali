.class public Lcom/google/android/gms/wearable/service/WearableService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/d/c;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# static fields
.field public static a:Ljava/util/Set;

.field private static final b:I

.field private static final c:I

.field private static d:Ljava/util/Map;


# instance fields
.field private final e:Ljava/util/Map;

.field private final f:Ljava/util/concurrent/ConcurrentHashMap;

.field private volatile g:Lcom/google/android/gms/wearable/service/l;

.field private volatile h:Lcom/google/android/gms/wearable/service/m;

.field private i:Lcom/google/android/gms/wearable/node/l;

.field private j:Lcom/google/android/gms/wearable/node/av;

.field private k:Lcom/google/android/gms/wearable/node/j;

.field private final l:Lcom/google/android/gms/wearable/service/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 89
    new-instance v0, Lcom/google/android/gms/wearable/service/j;

    invoke-direct {v0}, Lcom/google/android/gms/wearable/service/j;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wearable/service/WearableService;->a:Ljava/util/Set;

    .line 99
    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Runtime;->availableProcessors()I

    move-result v0

    .line 102
    sput v0, Lcom/google/android/gms/wearable/service/WearableService;->b:I

    const/16 v1, 0xa

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    sput v0, Lcom/google/android/gms/wearable/service/WearableService;->c:I

    .line 111
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/wearable/service/WearableService;->d:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 113
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->e:Ljava/util/Map;

    .line 116
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 135
    new-instance v0, Lcom/google/android/gms/wearable/service/e;

    sget v1, Lcom/google/android/gms/wearable/service/WearableService;->b:I

    sget v2, Lcom/google/android/gms/wearable/service/WearableService;->c:I

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/wearable/service/e;-><init>(IILjava/util/concurrent/TimeUnit;)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->l:Lcom/google/android/gms/wearable/service/e;

    .line 795
    return-void
.end method

.method private a(Lcom/google/android/gms/wearable/node/a;)Lcom/google/android/gms/wearable/service/n;
    .locals 3

    .prologue
    .line 277
    iget-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->e:Ljava/util/Map;

    monitor-enter v1

    .line 278
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/n;

    .line 279
    if-nez v0, :cond_0

    .line 280
    new-instance v0, Lcom/google/android/gms/wearable/service/n;

    iget-object v2, p0, Lcom/google/android/gms/wearable/service/WearableService;->g:Lcom/google/android/gms/wearable/service/l;

    invoke-direct {v0, v2, p1}, Lcom/google/android/gms/wearable/service/n;-><init>(Lcom/google/android/gms/wearable/service/l;Lcom/google/android/gms/wearable/node/a;)V

    .line 281
    iget-object v2, p0, Lcom/google/android/gms/wearable/service/WearableService;->e:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 283
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 284
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a()Ljava/util/Set;
    .locals 4

    .prologue
    .line 291
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/WearableService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.wearable.BIND_LISTENER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 295
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 296
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 299
    :try_start_0
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->packageName:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/wearable/node/b;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/wearable/node/a;

    move-result-object v0

    .line 300
    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 305
    :catch_0
    move-exception v0

    goto :goto_0

    .line 308
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 309
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/service/z;

    .line 310
    if-eqz v1, :cond_1

    .line 311
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 314
    :cond_2
    const-string v0, "WearableService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "getAllListeningPackages: count="

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    :cond_3
    return-object v2
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/service/WearableService;)V
    .locals 3

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/WearableService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/google/android/gms/wearable/node/o;->a:Lcom/google/android/gms/wearable/node/o;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/o;->a(Ljava/util/Set;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wearable/service/WearableService;Lcom/google/android/gms/wearable/node/a;Lcom/google/android/gms/wearable/service/x;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 65
    const-string v0, "WearableService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "queueEventAndNotify: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/gms/wearable/node/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/wearable/service/WearableService;->a(Lcom/google/android/gms/wearable/node/a;)Lcom/google/android/gms/wearable/service/n;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wearable/service/n;->a(Lcom/google/android/gms/wearable/service/x;)V

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->g:Lcom/google/android/gms/wearable/service/l;

    if-eqz v1, :cond_1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wearable/service/l;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/service/z;

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->h:Lcom/google/android/gms/wearable/service/m;

    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wearable/service/z;->a(Lcom/google/android/gms/wearable/service/x;)V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wearable/service/m;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iput-object v0, v1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    :cond_2
    return-void

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V
    .locals 2

    .prologue
    .line 971
    sget-object v0, Lcom/google/android/gms/wearable/service/WearableService;->d:Ljava/util/Map;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 972
    return-void
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 238
    sget-object v0, Lcom/google/android/gms/wearable/service/WearableService;->a:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wearable/service/WearableService;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->f:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/wearable/service/WearableService;)Lcom/google/android/gms/wearable/service/e;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->l:Lcom/google/android/gms/wearable/service/e;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wearable/service/WearableService;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/wearable/service/WearableService;->a()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wearable/service/WearableService;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->e:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/util/ad;ZZ)V
    .locals 3

    .prologue
    .line 976
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 977
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->l:Lcom/google/android/gms/wearable/service/e;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/wearable/service/e;->a(Lcom/google/android/gms/common/util/ad;ZZ)V

    .line 978
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 980
    const-string v0, "EventHandler:"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 981
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 982
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->g:Lcom/google/android/gms/wearable/service/l;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wearable/service/l;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    .line 983
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 985
    const-string v0, "LiveListenerEventHandler:"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 986
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 987
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->h:Lcom/google/android/gms/wearable/service/m;

    const-string v1, ""

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/wearable/service/m;->dump(Landroid/util/Printer;Ljava/lang/String;)V

    .line 988
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 990
    const-string v0, "Stubs:"

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 991
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 992
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 993
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/service/z;

    .line 994
    if-eqz v1, :cond_0

    .line 995
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/Object;)V

    .line 996
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 997
    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/gms/wearable/service/z;->a(Lcom/google/android/gms/common/util/ad;ZZ)V

    .line 998
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    goto :goto_0

    .line 1001
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 1002
    return-void
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 936
    const-string v0, "user"

    sget-object v1, Landroid/os/Build;->TYPE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/a/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v2, v3

    .line 940
    :goto_0
    :try_start_0
    array-length v8, p3

    move v7, v4

    move-object v1, v6

    move v5, v4

    :goto_1
    if-ge v7, v8, :cond_3

    aget-object v0, p3, v7

    .line 941
    const-string v9, "verbose"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_0

    const-string v9, "-v"

    invoke-virtual {v9, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_2

    :cond_0
    move-object v0, v1

    move v1, v3

    .line 940
    :goto_2
    add-int/lit8 v5, v7, 0x1

    move v7, v5

    move v5, v1

    move-object v1, v0

    goto :goto_1

    :cond_1
    move v2, v4

    .line 936
    goto :goto_0

    :cond_2
    move v1, v5

    .line 945
    goto :goto_2

    .line 947
    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    move-object v6, v0

    .line 948
    :cond_4
    new-instance v7, Lcom/google/android/gms/common/util/ad;

    const-string v0, "  "

    invoke-direct {v7, p2, v0}, Lcom/google/android/gms/common/util/ad;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    .line 950
    sget-object v0, Lcom/google/android/gms/wearable/service/WearableService;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_5
    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 951
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    .line 952
    if-eqz v6, :cond_6

    invoke-virtual {v1, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 953
    :cond_6
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wearable/d/c;

    .line 956
    if-eqz v1, :cond_7

    .line 957
    const-string v9, "#####################################"

    invoke-virtual {v7, v9}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 958
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 959
    if-eqz v5, :cond_8

    if-nez v2, :cond_8

    move v0, v3

    :goto_4
    invoke-interface {v1, v7, v2, v0}, Lcom/google/android/gms/wearable/d/c;->a(Lcom/google/android/gms/common/util/ad;ZZ)V

    .line 961
    :cond_7
    invoke-virtual {v7}, Lcom/google/android/gms/common/util/ad;->println()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    .line 964
    :catch_0
    move-exception v0

    .line 965
    const-string v1, "WearableService"

    const-string v2, "caught exception while dumping"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 966
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "caught exception while dumping"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 968
    :goto_5
    return-void

    :cond_8
    move v0, v4

    .line 959
    goto :goto_4

    .line 963
    :cond_9
    :try_start_1
    invoke-virtual {v7}, Lcom/google/android/gms/common/util/ad;->flush()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_5
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 224
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 225
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    const-string v1, "com.google.android.gms.wearable.BIND"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    new-instance v0, Lcom/google/android/gms/wearable/service/o;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/wearable/service/o;-><init>(Lcom/google/android/gms/wearable/service/WearableService;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/o;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 229
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 142
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    const-string v0, "WearableService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/service/WearableService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wearable/service/y;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    :goto_0
    return-void

    .line 151
    :cond_1
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "WearableService"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 152
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 153
    new-instance v1, Lcom/google/android/gms/wearable/service/l;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/wearable/service/l;-><init>(Lcom/google/android/gms/wearable/service/WearableService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->g:Lcom/google/android/gms/wearable/service/l;

    .line 155
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "WearableServiceLiveListener"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 156
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 157
    new-instance v1, Lcom/google/android/gms/wearable/service/m;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/wearable/service/m;-><init>(Lcom/google/android/gms/wearable/service/WearableService;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->h:Lcom/google/android/gms/wearable/service/m;

    .line 160
    new-instance v0, Lcom/google/android/gms/wearable/service/s;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/wearable/service/s;-><init>(Lcom/google/android/gms/wearable/service/WearableService;B)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->i:Lcom/google/android/gms/wearable/node/l;

    .line 161
    sget-object v0, Lcom/google/android/gms/wearable/node/o;->a:Lcom/google/android/gms/wearable/node/o;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->i:Lcom/google/android/gms/wearable/node/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/node/o;->a(Lcom/google/android/gms/wearable/node/l;)V

    .line 163
    new-instance v0, Lcom/google/android/gms/wearable/service/v;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/wearable/service/v;-><init>(Lcom/google/android/gms/wearable/service/WearableService;B)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->j:Lcom/google/android/gms/wearable/node/av;

    .line 164
    sget-object v0, Lcom/google/android/gms/wearable/node/au;->a:Lcom/google/android/gms/wearable/node/au;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->j:Lcom/google/android/gms/wearable/node/av;

    iget-object v2, v0, Lcom/google/android/gms/wearable/node/au;->c:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iput-object v1, v0, Lcom/google/android/gms/wearable/node/au;->d:Lcom/google/android/gms/wearable/node/av;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    new-instance v0, Lcom/google/android/gms/wearable/service/p;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/wearable/service/p;-><init>(Lcom/google/android/gms/wearable/service/WearableService;B)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->k:Lcom/google/android/gms/wearable/node/j;

    .line 167
    sget-object v0, Lcom/google/android/gms/wearable/node/am;->a:Lcom/google/android/gms/wearable/node/am;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->k:Lcom/google/android/gms/wearable/node/j;

    iput-object v1, v0, Lcom/google/android/gms/wearable/node/am;->b:Lcom/google/android/gms/wearable/node/j;

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->g:Lcom/google/android/gms/wearable/service/l;

    new-instance v1, Lcom/google/android/gms/wearable/service/k;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wearable/service/k;-><init>(Lcom/google/android/gms/wearable/service/WearableService;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wearable/service/l;->post(Ljava/lang/Runnable;)Z

    .line 178
    const-string v0, "WearableService"

    invoke-static {v0, p0}, Lcom/google/android/gms/wearable/service/WearableService;->a(Ljava/lang/String;Lcom/google/android/gms/wearable/d/c;)V

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 183
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 184
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    const-string v0, "WearableService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_0
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    if-nez v0, :cond_1

    .line 212
    :goto_0
    return-void

    .line 194
    :cond_1
    invoke-static {}, Lcom/google/android/gms/wearable/service/y;->b()Lcom/google/android/gms/wearable/node/a/b;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wearable/node/a/b;->b(Landroid/app/Service;)V

    .line 196
    sget-object v0, Lcom/google/android/gms/wearable/node/am;->a:Lcom/google/android/gms/wearable/node/am;

    iput-object v3, v0, Lcom/google/android/gms/wearable/node/am;->b:Lcom/google/android/gms/wearable/node/j;

    .line 197
    iput-object v3, p0, Lcom/google/android/gms/wearable/service/WearableService;->k:Lcom/google/android/gms/wearable/node/j;

    .line 199
    sget-object v0, Lcom/google/android/gms/wearable/node/au;->a:Lcom/google/android/gms/wearable/node/au;

    iget-object v1, v0, Lcom/google/android/gms/wearable/node/au;->c:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-object v2, v0, Lcom/google/android/gms/wearable/node/au;->d:Lcom/google/android/gms/wearable/node/av;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    iput-object v3, p0, Lcom/google/android/gms/wearable/service/WearableService;->j:Lcom/google/android/gms/wearable/node/av;

    .line 202
    sget-object v0, Lcom/google/android/gms/wearable/node/o;->a:Lcom/google/android/gms/wearable/node/o;

    iget-object v1, p0, Lcom/google/android/gms/wearable/service/WearableService;->i:Lcom/google/android/gms/wearable/node/l;

    iget-object v2, v0, Lcom/google/android/gms/wearable/node/o;->h:Ljava/util/HashSet;

    monitor-enter v2

    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/wearable/node/o;->h:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 203
    iput-object v3, p0, Lcom/google/android/gms/wearable/service/WearableService;->i:Lcom/google/android/gms/wearable/node/l;

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->g:Lcom/google/android/gms/wearable/service/l;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/l;->a()V

    .line 206
    iput-object v3, p0, Lcom/google/android/gms/wearable/service/WearableService;->g:Lcom/google/android/gms/wearable/service/l;

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->h:Lcom/google/android/gms/wearable/service/m;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/service/m;->a()V

    .line 209
    iput-object v3, p0, Lcom/google/android/gms/wearable/service/WearableService;->h:Lcom/google/android/gms/wearable/service/m;

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/wearable/service/WearableService;->l:Lcom/google/android/gms/wearable/service/e;

    iget-object v0, v0, Lcom/google/android/gms/wearable/service/e;->c:Ljava/util/concurrent/ThreadPoolExecutor;

    invoke-virtual {v0}, Ljava/util/concurrent/ThreadPoolExecutor;->shutdown()V

    goto :goto_0

    .line 199
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 202
    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 216
    const-string v0, "WearableService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const-string v0, "WearableService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onStartCommand: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
