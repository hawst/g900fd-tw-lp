.class final Lcom/google/android/gms/droidguard/d/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/droidguard/d/q;


# instance fields
.field final a:Ljava/util/Map;

.field final b:Lcom/google/android/gms/droidguard/d/a;

.field private final c:Lcom/google/android/gms/droidguard/d/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/droidguard/d/a;Lcom/google/android/gms/droidguard/d/q;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/c;->b:Lcom/google/android/gms/droidguard/d/a;

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/c;->c:Lcom/google/android/gms/droidguard/d/q;

    .line 35
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;
    .locals 3

    .prologue
    .line 39
    new-instance v1, Lcom/google/android/gms/droidguard/d/k;

    invoke-direct {v1}, Lcom/google/android/gms/droidguard/d/k;-><init>()V

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/c;->b:Lcom/google/android/gms/droidguard/d/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/droidguard/d/a;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 42
    if-eqz v0, :cond_0

    .line 43
    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;

    :goto_0
    move-object v0, v1

    .line 89
    :goto_1
    return-object v0

    .line 45
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    monitor-enter v2
    :try_end_0
    .catch Lcom/google/android/gms/droidguard/d/b; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 46
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/droidguard/d/k;

    .line 47
    if-nez v0, :cond_1

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 52
    :try_start_2
    monitor-exit v2

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/c;->c:Lcom/google/android/gms/droidguard/d/q;

    invoke-interface {v0, p1}, Lcom/google/android/gms/droidguard/d/q;->a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v0

    .line 54
    new-instance v2, Lcom/google/android/gms/droidguard/d/d;

    invoke-direct {v2, p0, p1, v1}, Lcom/google/android/gms/droidguard/d/d;-><init>(Lcom/google/android/gms/droidguard/d/c;Ljava/lang/Object;Lcom/google/android/gms/droidguard/d/k;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/droidguard/d/k;->a(Lcom/google/android/gms/droidguard/d/m;)Lcom/google/android/gms/droidguard/d/k;
    :try_end_2
    .catch Lcom/google/android/gms/droidguard/d/b; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 78
    :catch_0
    move-exception v0

    .line 79
    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    goto :goto_0

    .line 50
    :cond_1
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 52
    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v2

    throw v0
    :try_end_4
    .catch Lcom/google/android/gms/droidguard/d/b; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1

    .line 80
    :catch_1
    move-exception v0

    .line 84
    invoke-virtual {v1, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    .line 85
    iget-object v2, p0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    monitor-enter v2

    .line 86
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/c;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 87
    monitor-exit v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method
