.class public final Lcom/google/android/gms/games/ui/common/players/ProfileSummaryActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/a/b;
.implements Lcom/google/android/gms/games/ui/bj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method


# virtual methods
.method public final B()Lcom/google/android/gms/games/ui/a/a;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/games/ui/a/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/a/a;-><init>(Landroid/support/v4/app/q;)V

    return-object v0
.end method

.method protected final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 26
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/ProfileSummaryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    .line 30
    invoke-static {v0, v2, v2}, Lcom/google/android/gms/games/ui/common/players/g;->a(Lcom/google/android/gms/games/Player;ZZ)Lcom/google/android/gms/games/ui/common/players/g;

    move-result-object v0

    .line 33
    const-string v1, "profile_summary"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 34
    return-void
.end method

.method public final y()Lcom/google/android/gms/games/ui/bh;
    .locals 3

    .prologue
    .line 43
    new-instance v0, Lcom/google/android/gms/games/ui/bh;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/ui/bh;-><init>(Landroid/app/Activity;II)V

    return-object v0
.end method
