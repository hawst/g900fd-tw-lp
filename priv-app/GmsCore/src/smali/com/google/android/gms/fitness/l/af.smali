.class public final Lcom/google/android/gms/fitness/l/af;
.super Lcom/google/android/gms/fitness/l/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/l/z;


# static fields
.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;


# instance fields
.field private final m:Ljava/util/concurrent/atomic/AtomicReference;

.field private final n:Ljava/util/concurrent/atomic/AtomicReference;

.field private final o:Lcom/google/android/gms/fitness/l/d;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 114
    const-string v0, "%s DS JOIN %s DST ON (DS.%s = DST.%s)"

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "DataSources"

    aput-object v2, v1, v4

    const-string v2, "DataSourceTypes"

    aput-object v2, v1, v5

    const-string v2, "_id"

    aput-object v2, v1, v6

    const-string v2, "data_source_id"

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/af;->j:Ljava/lang/String;

    .line 119
    const-string v0, "%s DS JOIN %s DST ON (DS.%s = DST.%s) LEFT JOIN %s ST ON (DS.%s = ST.%s)"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "DataSources"

    aput-object v2, v1, v4

    const-string v2, "DataSourceTypes"

    aput-object v2, v1, v5

    const-string v2, "_id"

    aput-object v2, v1, v6

    const-string v2, "data_source_id"

    aput-object v2, v1, v7

    const-string v2, "SyncStatus"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "data_source_id"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/af;->k:Ljava/lang/String;

    .line 126
    const-string v0, "%s DS JOIN %s AP ON (DS.%s = AP.%s) JOIN %s DST ON (DS.%s = DST.%s)"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "DataSources"

    aput-object v2, v1, v4

    const-string v2, "Applications"

    aput-object v2, v1, v5

    const-string v2, "application_id"

    aput-object v2, v1, v6

    const-string v2, "_id"

    aput-object v2, v1, v7

    const-string v2, "DataSourceTypes"

    aput-object v2, v1, v8

    const/4 v2, 0x5

    const-string v3, "_id"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "data_source_id"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/l/af;->l:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/l/y;)V
    .locals 3

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/l/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/fitness/l/y;)V

    .line 133
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/af;->m:Ljava/util/concurrent/atomic/AtomicReference;

    .line 135
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    .line 137
    new-instance v0, Lcom/google/android/gms/fitness/l/d;

    new-instance v1, Lcom/google/android/gms/fitness/l/ai;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/fitness/l/ai;-><init>(Lcom/google/android/gms/fitness/l/af;B)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/fitness/l/d;-><init>(Lcom/google/android/gms/fitness/l/e;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/af;->o:Lcom/google/android/gms/fitness/l/d;

    .line 150
    return-void
.end method

.method private a(Ljava/util/Set;JLjava/lang/Long;)I
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 1264
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 1265
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1291
    :goto_0
    return v0

    .line 1268
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->f(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v1

    .line 1270
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1271
    const/4 v0, -0x1

    goto :goto_0

    .line 1274
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1275
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1278
    if-nez p4, :cond_2

    .line 1279
    const-string v3, "%s IN (%s) AND %s >= ?"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "data_source_id"

    aput-object v5, v4, v0

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const-string v0, "end_time"

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 1291
    :goto_1
    const-string v3, "DataPointRows"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, v3, v1, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 1284
    :cond_2
    const-string v3, "%s IN (%s) AND %s BETWEEN ? AND ?"

    new-array v4, v4, [Ljava/lang/Object;

    const-string v5, "data_source_id"

    aput-object v5, v4, v0

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    const-string v0, "end_time"

    aput-object v0, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1288
    invoke-virtual {p4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_1
.end method

.method private a(JIJ)J
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1169
    const-string v0, "%s = ?"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "data_source_id"

    aput-object v2, v1, v8

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1170
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1171
    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1174
    const-wide/16 v4, 0x0

    cmp-long v1, p4, v4

    if-ltz v1, :cond_0

    .line 1175
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND %s <= ?"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "end_time"

    aput-object v4, v3, v8

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1176
    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179
    :cond_0
    add-int/lit8 v6, p3, -0x1

    .line 1180
    const-string v1, "DataPointRows"

    new-array v2, v7, [Ljava/lang/String;

    const-string v4, "end_time"

    aput-object v4, v2, v8

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const-string v0, "%s DESC"

    new-array v5, v7, [Ljava/lang/Object;

    const-string v7, "end_time"

    aput-object v7, v5, v8

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v2

    .line 1189
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 1191
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 1189
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 1191
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/google/android/gms/fitness/data/Application;)J
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 447
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 451
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Application;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 452
    const-string v0, "%s = ? AND %s is null"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "package"

    aput-object v2, v1, v5

    const-string v2, "version"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 453
    new-array v4, v6, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Application;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    .line 459
    :goto_0
    const-string v1, "Applications"

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 466
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "_id"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    int-to-long v0, v0

    .line 470
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 455
    :cond_0
    const-string v0, "%s = ? AND %s = ?"

    new-array v1, v4, [Ljava/lang/Object;

    const-string v2, "package"

    aput-object v2, v1, v5

    const-string v2, "version"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 456
    new-array v4, v4, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Application;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Application;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    goto :goto_0

    .line 466
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_1

    .line 470
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/google/android/gms/fitness/data/Device;)J
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 343
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 345
    const-string v1, "Devices"

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    const-string v0, "%s = ? AND %s = ? AND %s = ? AND %s = ?"

    new-array v3, v9, [Ljava/lang/Object;

    const-string v4, "make"

    aput-object v4, v3, v5

    const-string v4, "model"

    aput-object v4, v3, v6

    const-string v4, "version"

    aput-object v4, v3, v7

    const-string v4, "uid"

    aput-object v4, v3, v8

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Device;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Device;->b()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Device;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 355
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "_id"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    int-to-long v0, v0

    .line 359
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 355
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 359
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)J
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2236
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 2237
    const-string v0, "%s = ?"

    new-array v1, v7, [Ljava/lang/Object;

    const-string v2, "app_package"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2238
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2239
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2241
    if-eqz p2, :cond_0

    .line 2242
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND %s = ?"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "data_source_id"

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2243
    invoke-direct {p0, p2}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2245
    :cond_0
    if-eqz p3, :cond_1

    .line 2246
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND %s = ?"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "data_type_id"

    aput-object v4, v3, v6

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2247
    invoke-direct {p0, p3}, Lcom/google/android/gms/fitness/l/af;->c(Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v1, v0

    .line 2250
    const-string v3, "Subscriptions"

    new-array v4, v7, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v4, v6

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, v3, v4, v1, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2257
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "_id"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2261
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 2257
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2261
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Lcom/google/android/gms/fitness/data/Session;)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 2472
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v0

    .line 2474
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 2475
    const-string v2, "app_package"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2476
    const-string v0, "start_time"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2477
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    .line 2478
    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 2479
    const-string v0, "end_time"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2481
    :cond_0
    const-string v0, "name"

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2482
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v0

    .line 2483
    if-eqz v0, :cond_1

    .line 2484
    const-string v2, "identifier"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2486
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->d()Ljava/lang/String;

    move-result-object v0

    .line 2487
    if-eqz v0, :cond_2

    .line 2488
    const-string v2, "description"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2490
    :cond_2
    const-string v0, "activity"

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2491
    return-object v1
.end method

.method private a(Ljava/lang/Long;Landroid/support/v4/g/g;I)Lcom/google/android/gms/fitness/data/DataSource;
    .locals 4

    .prologue
    .line 2174
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    .line 2175
    :cond_0
    const/4 v0, 0x0

    .line 2189
    :cond_1
    :goto_0
    return-object v0

    .line 2178
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Landroid/support/v4/g/g;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 2179
    if-nez v0, :cond_1

    .line 2183
    if-nez p3, :cond_3

    .line 2184
    const-string v0, "Couldn\'t find data source ID %s in pre-computed %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2187
    :cond_3
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->g(J)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    .line 2188
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p2, v2, v3, v0}, Landroid/support/v4/g/g;->a(JLjava/lang/Object;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/fitness/l/y;)Lcom/google/android/gms/fitness/l/af;
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/google/android/gms/fitness/l/af;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/fitness/l/af;-><init>(Landroid/content/Context;Lcom/google/android/gms/fitness/l/y;)V

    return-object v0
.end method

.method private static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 2078
    if-nez p0, :cond_0

    .line 2079
    const-string v0, ""

    .line 2087
    :goto_0
    return-object v0

    .line 2082
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    mul-int/lit8 v0, p0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 2083
    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2084
    const/4 v0, 0x1

    :goto_1
    if-ge v0, p0, :cond_1

    .line 2085
    const-string v2, ",?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2084
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2087
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/support/v4/g/g;)Ljava/util/List;
    .locals 4

    .prologue
    .line 741
    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Landroid/support/v4/g/g;->a()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 742
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/g/g;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 743
    invoke-virtual {p0, v0}, Landroid/support/v4/g/g;->a(I)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/g/g;->a(J)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 742
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 745
    :cond_0
    return-object v1
.end method

.method private a(Ljava/lang/String;IJ)Ljava/util/List;
    .locals 3

    .prologue
    .line 1452
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1454
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1455
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;IJ)Ljava/util/List;

    move-result-object v0

    .line 1456
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1459
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/Boolean;Landroid/support/v4/g/g;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2102
    const/4 v5, 0x0

    .line 2103
    if-eqz p4, :cond_0

    .line 2104
    const-string v1, "%s %s"

    new-array v2, v6, [Ljava/lang/Object;

    const-string v0, "end_time"

    aput-object v0, v2, v3

    invoke-virtual {p4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "ASC"

    :goto_0
    aput-object v0, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 2107
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 2108
    const-string v0, "Performing DB query for selection %s args %s"

    new-array v1, v6, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {p2}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2111
    const-string v1, "DataPointRows"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "data_point"

    aput-object v0, v2, v3

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move v6, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v2

    .line 2122
    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2123
    new-instance v0, Lcom/google/af/a/b/a/a/ak;

    invoke-direct {v0}, Lcom/google/af/a/b/a/a/ak;-><init>()V

    .line 2125
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2126
    const-string v1, "data_point"

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    move-object v1, v0

    .line 2128
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 2130
    :try_start_1
    invoke-virtual {v1}, Lcom/google/af/a/b/a/a/ak;->a()Lcom/google/af/a/b/a/a/ak;

    .line 2131
    invoke-interface {v2, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/ak;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2138
    :try_start_2
    iget-object v1, v0, Lcom/google/af/a/b/a/a/ak;->a:Ljava/lang/Long;

    const/4 v5, 0x0

    invoke-direct {p0, v1, p5, v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/Long;Landroid/support/v4/g/g;I)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    .line 2143
    iget-object v5, v0, Lcom/google/af/a/b/a/a/ak;->b:Ljava/lang/Long;

    const/4 v6, 0x1

    invoke-direct {p0, v5, p5, v6}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/Long;Landroid/support/v4/g/g;I)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v5

    .line 2146
    iget-object v6, v0, Lcom/google/af/a/b/a/a/ak;->c:[Lcom/google/af/a/b/a/a/c;

    const/4 v7, 0x0

    aget-object v6, v6, v7

    invoke-static {v6, v1, v5}, Lcom/google/android/gms/fitness/apiary/c;->a(Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2147
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v1, v0

    .line 2148
    goto :goto_1

    .line 2104
    :cond_1
    const-string v0, "DESC"

    goto :goto_0

    .line 2132
    :catch_0
    move-exception v0

    :try_start_3
    const-string v5, "Couldn\'t parse proto"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v0, v5, v6}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2134
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 2152
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-object v3
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/l/af;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Lcom/google/android/gms/fitness/l/af;->h()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;)Ljava/util/Map;
    .locals 20

    .prologue
    .line 1827
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 1828
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1829
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v2

    .line 1846
    :goto_0
    return-object v2

    .line 1832
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    invoke-interface {v11, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    if-eqz p2, :cond_2

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->e()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lcom/google/android/gms/fitness/data/Application;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_2
    move-object v3, v4

    :goto_2
    invoke-interface {v11, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v5, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataPoint;->d()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataPoint;->d()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    invoke-interface {v5, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->e()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/fitness/data/Application;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "Attempting to add %s to %s which already has app"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object p2, v7, v8

    const/4 v8, 0x1

    aput-object v4, v7, v8

    invoke-static {v3, v7}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v3, v4

    goto :goto_2

    :cond_5
    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->e()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->e()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Application;->b()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_6

    move-object v3, v4

    goto :goto_2

    :cond_6
    new-instance v3, Lcom/google/android/gms/fitness/data/f;

    invoke-direct {v3}, Lcom/google/android/gms/fitness/data/f;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->b()I

    move-result v7

    iput v7, v3, Lcom/google/android/gms/fitness/data/f;->b:I

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v7

    iput-object v7, v3, Lcom/google/android/gms/fitness/data/f;->a:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->g()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Lcom/google/android/gms/fitness/data/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/f;

    move-result-object v3

    move-object/from16 v0, p2

    iput-object v0, v3, Lcom/google/android/gms/fitness/data/f;->e:Lcom/google/android/gms/fitness/data/Application;

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v7

    iput-object v7, v3, Lcom/google/android/gms/fitness/data/f;->d:Lcom/google/android/gms/fitness/data/Device;

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataSource;->c()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/google/android/gms/fitness/data/f;->c:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/f;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    goto :goto_2

    :cond_7
    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/google/android/gms/fitness/l/af;->d(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v12

    const-string v2, "INSERT INTO %s(%s, %s, %s, %s) VALUES(?, ?, ?, ?)"

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "DataPointRows"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "start_time"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "end_time"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "data_source_id"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "data_point"

    aput-object v5, v3, v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v13

    new-instance v10, Ljava/util/LinkedHashMap;

    invoke-direct {v10}, Ljava/util/LinkedHashMap;-><init>()V

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_3
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    invoke-interface {v11, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v12, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataPoint;->d()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    if-nez v3, :cond_9

    const-wide/16 v4, -0x1

    :goto_4
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    const-wide/16 v18, 0x0

    cmp-long v3, v6, v18

    if-lez v3, :cond_a

    :goto_5
    const/4 v3, 0x1

    invoke-virtual {v13, v3, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 v3, 0x2

    invoke-virtual {v13, v3, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 v3, 0x3

    move-wide/from16 v0, v16

    invoke-virtual {v13, v3, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    const/4 v3, 0x4

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static {v2, v6, v7}, Lcom/google/android/gms/fitness/apiary/c;->a(Lcom/google/android/gms/fitness/data/DataPoint;ZZ)Lcom/google/af/a/b/a/a/c;

    move-result-object v6

    new-instance v7, Lcom/google/af/a/b/a/a/ak;

    invoke-direct {v7}, Lcom/google/af/a/b/a/a/ak;-><init>()V

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iput-object v8, v7, Lcom/google/af/a/b/a/a/ak;->a:Ljava/lang/Long;

    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v7, Lcom/google/af/a/b/a/a/ak;->b:Ljava/lang/Long;

    :cond_8
    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/af/a/b/a/a/c;

    const/4 v5, 0x0

    aput-object v6, v4, v5

    iput-object v4, v7, Lcom/google/af/a/b/a/a/ak;->c:[Lcom/google/af/a/b/a/a/c;

    invoke-static {v7}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    invoke-virtual {v13, v3, v4}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    invoke-virtual {v13}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v10, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_9
    invoke-interface {v12, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_4

    :cond_a
    move-wide v6, v8

    goto :goto_5

    .line 1833
    :cond_b
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 1834
    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_c
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1835
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 1836
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/fitness/l/af;->o:Lcom/google/android/gms/fitness/l/d;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v7}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/l/d;->b()Ljava/util/Map;

    move-result-object v7

    if-eqz v7, :cond_d

    invoke-interface {v7, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/c/d;

    if-nez v3, :cond_e

    const-string v3, "New data source found, clearing cache"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v3, v7}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/l/d;->c()V

    .line 1839
    :cond_d
    :goto_7
    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v6

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/data/DataPoint;

    if-nez v3, :cond_f

    invoke-interface {v4, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 1836
    :cond_e
    invoke-virtual {v3, v8, v9}, Lcom/google/android/gms/fitness/c/d;->a(J)V

    goto :goto_7

    .line 1839
    :cond_f
    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v7}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v7}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v7, v8, v14

    if-eqz v7, :cond_10

    cmp-long v7, v8, v12

    if-eqz v7, :cond_10

    cmp-long v3, v8, v12

    if-lez v3, :cond_c

    invoke-interface {v4, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    :cond_10
    sget-object v7, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v7}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v12

    cmp-long v3, v8, v12

    if-lez v3, :cond_c

    invoke-interface {v4, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_6

    .line 1843
    :cond_11
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_12
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 1844
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/data/DataPoint;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v3}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/l/ac;

    invoke-interface {v3, v2}, Lcom/google/android/gms/fitness/l/ac;->a(Lcom/google/android/gms/fitness/data/DataPoint;)Lcom/google/android/gms/fitness/l/ad;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/gms/fitness/l/ad;->a:Ljava/util/Set;

    iget-wide v6, v2, Lcom/google/android/gms/fitness/l/ad;->b:J

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v6, v7, v2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/Set;JLjava/lang/Long;)I

    move-result v2

    const-string v5, "Deleted %d pre-aggregated data points from %d aggregate data sources"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v6, v7

    const/4 v2, 0x1

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v2

    invoke-static {v5, v6}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_8

    :cond_13
    move-object v2, v10

    .line 1846
    goto/16 :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataSource;JJJ)Ljava/util/Set;
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1802
    new-instance v5, Landroid/support/v4/g/g;

    invoke-direct {v5}, Landroid/support/v4/g/g;-><init>()V

    .line 1803
    invoke-virtual {v5, p2, p3, p1}, Landroid/support/v4/g/g;->a(JLjava/lang/Object;)V

    .line 1805
    const-string v0, "%s = ? AND (%s BETWEEN ? AND ?)"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "data_source_id"

    aput-object v2, v1, v3

    const-string v2, "end_time"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    const/4 v3, -0x1

    const/4 v4, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/Boolean;Landroid/support/v4/g/g;)Ljava/util/List;

    move-result-object v0

    .line 1818
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    return-object v1
.end method

.method private a(JLandroid/content/ContentValues;)V
    .locals 7

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 2751
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 2754
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_0

    .line 2755
    const-string v0, "SyncStatus"

    const-string v1, "%s = ?"

    new-array v2, v5, [Ljava/lang/Object;

    const-string v3, "_id"

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v0, p3, v1, v2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2763
    :goto_0
    return-void

    .line 2761
    :cond_0
    const-string v0, "SyncStatus"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)V
    .locals 5

    .prologue
    .line 1709
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 1711
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/l/af;->b(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    .line 1712
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1713
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 1714
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1715
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1719
    :cond_1
    invoke-direct {p0, v2, p3}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;)Ljava/util/Map;

    move-result-object v0

    .line 1721
    if-eqz p4, :cond_2

    .line 1722
    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/Collection;Z)V

    .line 1724
    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/l/af;JLjava/util/List;)V
    .locals 5

    .prologue
    .line 104
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-virtual {v1}, Landroid/content/ContentValues;->clear()V

    const-string v3, "uuid"

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getUuid()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "properties"

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getProperties()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "permissions"

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothGattCharacteristic;->getPermissions()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "device_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "BleCharacteristics"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Ljava/util/Collection;Z)V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 1402
    const-string v0, "INSERT INTO %s(%s, %s, %s, %s) VALUES(?, ?, ?, ?)"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "ChangeLog"

    aput-object v3, v1, v2

    const-string v2, "source_table"

    aput-object v2, v1, v8

    const-string v2, "timestamp"

    aput-object v2, v1, v9

    const-string v2, "is_delete"

    aput-object v2, v1, v10

    const-string v2, "content"

    aput-object v2, v1, v11

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 1410
    new-instance v3, Lcom/google/af/a/b/a/a/o;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/o;-><init>()V

    .line 1411
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 1412
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 1413
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/af/a/b/a/a/o;->b:Ljava/lang/String;

    .line 1414
    invoke-static {v0, v8, v8}, Lcom/google/android/gms/fitness/apiary/c;->a(Lcom/google/android/gms/fitness/data/DataPoint;ZZ)Lcom/google/af/a/b/a/a/c;

    move-result-object v0

    iput-object v0, v3, Lcom/google/af/a/b/a/a/o;->c:Lcom/google/af/a/b/a/a/c;

    .line 1415
    invoke-static {v3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v7

    .line 1417
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1418
    const-string v0, "DataPoints"

    invoke-virtual {v2, v8, v0}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 1419
    invoke-virtual {v2, v9, v4, v5}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1420
    if-eqz p2, :cond_0

    const-wide/16 v0, 0x1

    :goto_1
    invoke-virtual {v2, v10, v0, v1}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1421
    invoke-virtual {v2, v11, v7}, Landroid/database/sqlite/SQLiteStatement;->bindBlob(I[B)V

    .line 1422
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    goto :goto_0

    .line 1420
    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 1424
    :cond_1
    return-void
.end method

.method private a(JLcom/google/android/gms/fitness/data/DataPoint;)Z
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1332
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 1334
    const-string v0, "%s = ? AND %s = ?"

    new-array v1, v9, [Ljava/lang/Object;

    const-string v3, "data_source_id"

    aput-object v3, v1, v4

    const-string v3, "end_time"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1339
    new-array v1, v9, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p3, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1343
    const-string v3, "DataPointRows"

    new-array v5, v9, [Ljava/lang/String;

    const-string v6, "_id"

    aput-object v6, v5, v4

    const-string v6, "data_point"

    aput-object v6, v5, v2

    invoke-virtual {p0, v3, v5, v0, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1352
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 1355
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1356
    const-string v0, "_id"

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 1358
    const-string v0, "data_point"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 1359
    new-instance v5, Lcom/google/af/a/b/a/a/ak;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/ak;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1361
    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1368
    :try_start_2
    iget-object v0, v5, Lcom/google/af/a/b/a/a/ak;->c:[Lcom/google/af/a/b/a/a/c;

    const/4 v5, 0x0

    aget-object v0, v0, v5

    invoke-virtual {p3}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v5

    invoke-virtual {p3}, Lcom/google/android/gms/fitness/data/DataPoint;->d()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v8

    invoke-static {v0, v5, v8}, Lcom/google/android/gms/fitness/apiary/c;->a(Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    .line 1370
    invoke-virtual {v0, p3}, Lcom/google/android/gms/fitness/data/DataPoint;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1371
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 1375
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1362
    :catch_0
    move-exception v0

    :try_start_3
    const-string v5, "Couldn\'t parse proto"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v0, v5, v8}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1364
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1375
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1378
    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 1379
    const-string v0, "DELETE FROM %s WHERE %s in (%s)"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string v5, "DataPointRows"

    aput-object v5, v1, v4

    const-string v5, "_id"

    aput-object v5, v1, v2

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v9

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-array v1, v4, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v5

    .line 1386
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1387
    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v5, v1, v8, v9}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    move v1, v3

    .line 1388
    goto :goto_1

    .line 1390
    :cond_2
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1391
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->executeUpdateDelete()I

    move-result v0

    if-lez v0, :cond_3

    .line 1398
    :goto_2
    return v2

    :cond_3
    move v2, v4

    .line 1391
    goto :goto_2

    .line 1394
    :cond_4
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    goto :goto_2

    :cond_5
    move v2, v4

    .line 1398
    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 144
    invoke-static {p1}, Lcom/google/android/gms/fitness/l/y;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 145
    invoke-virtual {p0, v0}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private b(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)I
    .locals 6

    .prologue
    .line 2290
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2292
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2294
    const-string v0, "%s = ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "app_package"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 2295
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2296
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2298
    if-eqz p2, :cond_0

    .line 2299
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND %s = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "data_source_id"

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2300
    invoke-direct {p0, p2}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2302
    :cond_0
    if-eqz p3, :cond_1

    .line 2303
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND %s = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "data_type_id"

    aput-object v5, v3, v4

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2304
    invoke-direct {p0, p3}, Lcom/google/android/gms/fitness/l/af;->c(Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    move-object v1, v0

    .line 2307
    const-string v3, "Subscriptions"

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, v3, v1, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2311
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2314
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method private b(Lcom/google/android/gms/fitness/data/DataType;)J
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v5, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x1

    .line 170
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 174
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/l/af;->h(Ljava/lang/String;)J

    move-result-wide v0

    .line 175
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 176
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->d(J)Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    .line 177
    invoke-virtual {v2, p1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 207
    :goto_0
    return-wide v0

    .line 180
    :cond_0
    new-instance v0, Lcom/google/android/gms/fitness/l/ab;

    const-string v1, "Conflicting data types!  New: %s, existing: %s"

    new-array v3, v9, [Ljava/lang/Object;

    aput-object p1, v3, v5

    aput-object v2, v3, v8

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/l/ab;-><init>(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataType;)V

    throw v0

    .line 188
    :cond_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 189
    const-string v1, "name"

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 190
    const-string v1, "DataTypes"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 193
    const-string v0, "DataTypeFields"

    new-array v1, v10, [Ljava/lang/String;

    const-string v4, "data_type_id"

    aput-object v4, v1, v5

    const-string v4, "field_name"

    aput-object v4, v1, v8

    const-string v4, "format"

    aput-object v4, v1, v9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 199
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataType;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Field;

    .line 200
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 201
    invoke-virtual {v1, v8, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 202
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Field;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v9, v5}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 203
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Field;->b()I

    move-result v0

    int-to-long v6, v0

    invoke-virtual {v1, v10, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 204
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    goto :goto_1

    :cond_2
    move-wide v0, v2

    .line 207
    goto :goto_0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/support/v4/g/g;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 971
    new-array v2, v1, [Ljava/lang/String;

    const-string v0, "DST.%s"

    new-array v1, v1, [Ljava/lang/Object;

    const-string v3, "data_source_id"

    aput-object v3, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 979
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 980
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 981
    const-string v2, "data_source_id"

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 982
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 986
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 984
    :cond_0
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/l/af;->c(Ljava/util/List;)Landroid/support/v4/g/g;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 986
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private b(Lcom/google/android/gms/fitness/l/b;)Ljava/util/Collection;
    .locals 18

    .prologue
    .line 2019
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 2020
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 2022
    new-instance v13, Landroid/support/v4/g/g;

    invoke-direct {v13}, Landroid/support/v4/g/g;-><init>()V

    .line 2023
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/l/b;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    :goto_0
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/google/android/gms/fitness/data/DataSource;

    .line 2024
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/gms/fitness/l/b;->c(Lcom/google/android/gms/fitness/data/DataSource;)I

    move-result v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/gms/fitness/l/b;->a(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v10

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/gms/fitness/l/b;->b(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v16

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/gms/fitness/l/b;->d(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v3

    if-ltz v5, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/gms/fitness/l/b;->b(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v6

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/fitness/l/af;->a(JIJ)J

    move-result-wide v6

    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    :goto_1
    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, " OR "

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v2, "(%s = ? AND %s BETWEEN ? AND ?)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "data_source_id"

    aput-object v11, v5, v10

    const/4 v10, 0x1

    const-string v11, "end_time"

    aput-object v11, v5, v10

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2025
    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Lcom/google/android/gms/fitness/l/b;->d(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v2

    invoke-virtual {v13, v2, v3, v8}, Landroid/support/v4/g/g;->a(JLjava/lang/Object;)V

    goto :goto_0

    .line 2029
    :cond_1
    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v12, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const/4 v5, -0x1

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    move-object/from16 v2, p0

    move-object v7, v13

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/Boolean;Landroid/support/v4/g/g;)Ljava/util/List;

    move-result-object v2

    return-object v2

    :cond_2
    move-wide v6, v10

    goto :goto_1
.end method

.method private b(Ljava/lang/String;IJ)Ljava/util/List;
    .locals 11

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1465
    const-string v1, "ChangeLog"

    new-array v2, v9, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    const-string v0, "timestamp"

    aput-object v0, v2, v6

    const-string v0, "is_delete"

    aput-object v0, v2, v7

    const-string v0, "content"

    aput-object v0, v2, v8

    const-string v0, "%s = ? AND %s <= ? AND %s <= ? AND %s < ?"

    new-array v3, v9, [Ljava/lang/Object;

    const-string v4, "source_table"

    aput-object v4, v3, v5

    const-string v4, "timestamp"

    aput-object v4, v3, v6

    const-string v4, "last_sync_ms"

    aput-object v4, v3, v7

    const-string v4, "sync_tries"

    aput-object v4, v3, v8

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v9, [Ljava/lang/String;

    aput-object p1, v4, v5

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v7

    const-wide/16 v6, 0xa

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v8

    const/4 v5, 0x0

    move-object v0, p0

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v8

    .line 1489
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1491
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1492
    const-string v1, "_id"

    invoke-static {v8, v1}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 1493
    const-string v1, "timestamp"

    invoke-static {v8, v1}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 1494
    const-string v1, "is_delete"

    invoke-static {v8, v1}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v6

    .line 1495
    const-string v1, "content"

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    .line 1497
    invoke-interface {v8, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v7

    .line 1498
    new-instance v1, Lcom/google/android/gms/fitness/l/c;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/fitness/l/c;-><init>(JJZ[B)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1502
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private static b(Landroid/support/v4/g/g;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 750
    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {p0}, Landroid/support/v4/g/g;->a()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 751
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/support/v4/g/g;->a()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 752
    invoke-virtual {p0, v0}, Landroid/support/v4/g/g;->a(I)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Landroid/support/v4/g/g;->a(J)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 751
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 754
    :cond_0
    return-object v1
.end method

.method private b(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;)Ljava/util/Set;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1776
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 1777
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    .line 1778
    invoke-interface {p2, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    .line 1780
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 1781
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/fitness/data/DataSource;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1782
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " should have dataSource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1785
    :cond_0
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 1786
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    goto :goto_0

    .line 1789
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v2

    .line 1790
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_2

    .line 1791
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 1794
    :goto_1
    return-object v0

    :cond_2
    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/DataSource;JJJ)Ljava/util/Set;

    move-result-object v0

    goto :goto_1
.end method

.method private c(Lcom/google/android/gms/fitness/data/DataType;)J
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/l/af;->h(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private c(Ljava/util/List;)Landroid/support/v4/g/g;
    .locals 14

    .prologue
    const/4 v6, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 896
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 898
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v3

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 899
    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v1

    .line 900
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 901
    goto :goto_0

    .line 903
    :cond_0
    sget-object v1, Lcom/google/android/gms/fitness/l/af;->j:Ljava/lang/String;

    const/16 v0, 0x8

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v3

    const-string v0, "source_name"

    aput-object v0, v2, v7

    const-string v0, "type"

    aput-object v0, v2, v6

    const/4 v0, 0x3

    const-string v5, "application_id"

    aput-object v5, v2, v0

    const/4 v0, 0x4

    const-string v5, "device_id"

    aput-object v5, v2, v0

    const/4 v0, 0x5

    const-string v5, "stream_name"

    aput-object v5, v2, v0

    const/4 v0, 0x6

    const-string v5, "obfuscated"

    aput-object v5, v2, v0

    const/4 v0, 0x7

    const-string v5, "data_type_id"

    aput-object v5, v2, v0

    const-string v0, "DS.%s IN (%s)"

    new-array v5, v6, [Ljava/lang/Object;

    const-string v6, "_id"

    aput-object v6, v5, v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v7

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 921
    :try_start_0
    new-instance v0, Landroid/support/v4/g/g;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v0, v2}, Landroid/support/v4/g/g;-><init>(I)V

    .line 922
    :goto_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 923
    const-string v2, "_id"

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 924
    const-string v4, "source_name"

    invoke-static {v1, v4}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "type"

    invoke-static {v1, v5}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    const-string v6, "device_id"

    invoke-static {v1, v6}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    const-string v8, "application_id"

    invoke-static {v1, v8}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v8

    const-string v10, "stream_name"

    invoke-static {v1, v10}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "obfuscated"

    invoke-static {v1, v11}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/database/Cursor;Ljava/lang/String;)Z

    move-result v11

    const-string v12, "data_type_id"

    invoke-static {v1, v12}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v12

    invoke-direct {p0, v12, v13}, Lcom/google/android/gms/fitness/l/af;->d(J)Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v12

    new-instance v13, Lcom/google/android/gms/fitness/data/f;

    invoke-direct {v13}, Lcom/google/android/gms/fitness/data/f;-><init>()V

    iput-object v12, v13, Lcom/google/android/gms/fitness/data/f;->a:Lcom/google/android/gms/fitness/data/DataType;

    iput-object v4, v13, Lcom/google/android/gms/fitness/data/f;->c:Ljava/lang/String;

    iput v5, v13, Lcom/google/android/gms/fitness/data/f;->b:I

    iput-boolean v11, v13, Lcom/google/android/gms/fitness/data/f;->g:Z

    invoke-static {v10}, Lcom/google/android/gms/common/util/au;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v13, v4}, Lcom/google/android/gms/fitness/data/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/f;

    move-result-object v4

    invoke-direct {p0, v6, v7}, Lcom/google/android/gms/fitness/l/af;->e(J)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v5

    if-eqz v5, :cond_1

    iput-object v5, v4, Lcom/google/android/gms/fitness/data/f;->d:Lcom/google/android/gms/fitness/data/Device;

    :cond_1
    invoke-direct {p0, v8, v9}, Lcom/google/android/gms/fitness/l/af;->f(J)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v5

    if-eqz v5, :cond_2

    iput-object v5, v4, Lcom/google/android/gms/fitness/data/f;->e:Lcom/google/android/gms/fitness/data/Application;

    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/f;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Landroid/support/v4/g/g;->a(JLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 928
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private c(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;
    .locals 12

    .prologue
    .line 2561
    const-string v0, "Sessions"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "app_package"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "start_time"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "end_time"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "name"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "identifier"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "activity"

    aput-object v3, v1, v2

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object p1, Lcom/google/android/gms/fitness/l/af;->b:Ljava/lang/String;

    :cond_0
    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2576
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 2577
    :goto_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2578
    const-string v1, "app_package"

    invoke-static {v11, v1}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 2579
    const-string v1, "start_time"

    invoke-static {v11, v1}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    .line 2580
    const-string v1, "end_time"

    invoke-static {v11, v1}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2581
    const-string v1, "name"

    invoke-static {v11, v1}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 2582
    const-string v1, "identifier"

    invoke-static {v11, v1}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 2583
    const-string v1, "description"

    invoke-static {v11, v1}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2584
    const-string v1, "activity"

    invoke-static {v11, v1}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v9

    .line 2586
    new-instance v1, Lcom/google/android/gms/fitness/data/Session;

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/fitness/data/Session;-><init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2597
    :catchall_0
    move-exception v0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private d(Lcom/google/android/gms/fitness/data/DataSource;)J
    .locals 2

    .prologue
    .line 551
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 552
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/l/af;->d(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method private d(J)Lcom/google/android/gms/fitness/data/DataType;
    .locals 9

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 268
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 269
    const/4 v0, 0x0

    .line 300
    :goto_0
    return-object v0

    .line 272
    :cond_0
    const-string v0, "DT.%s"

    new-array v1, v6, [Ljava/lang/Object;

    const-string v2, "_id"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 273
    const-string v1, "%s DT JOIN %s F ON (%s = F.%s)"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const-string v3, "DataTypes"

    aput-object v3, v2, v5

    const-string v3, "DataTypeFields"

    aput-object v3, v2, v6

    aput-object v0, v2, v4

    const-string v3, "data_type_id"

    aput-object v3, v2, v7

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "name"

    aput-object v3, v2, v5

    const-string v3, "field_name"

    aput-object v3, v2, v6

    const-string v3, "format"

    aput-object v3, v2, v4

    const-string v3, "%s = ?"

    new-array v4, v6, [Ljava/lang/Object;

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 287
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    const-string v2, "Couldn\'t find data type with ID %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 290
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 291
    const-string v0, "name"

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 293
    :cond_1
    const-string v0, "field_name"

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 294
    const-string v4, "format"

    invoke-static {v1, v4}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    .line 295
    new-instance v5, Lcom/google/android/gms/fitness/data/Field;

    invoke-direct {v5, v0, v4}, Lcom/google/android/gms/fitness/data/Field;-><init>(Ljava/lang/String;I)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 296
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 298
    new-instance v0, Lcom/google/android/gms/fitness/data/DataType;

    invoke-direct {v0, v3, v2}, Lcom/google/android/gms/fitness/data/DataType;-><init>(Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 300
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private d(Ljava/util/Collection;)Ljava/util/Map;
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    .line 559
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 561
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v1

    .line 562
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 563
    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 564
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 569
    const-string v2, "source_name"

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const-string v2, "identifier"

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 571
    const-string v2, "version"

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 572
    const-string v2, "type"

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 573
    const-string v2, "obfuscated"

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->j()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 574
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->g()Ljava/lang/String;

    move-result-object v2

    .line 575
    if-eqz v2, :cond_1

    .line 576
    const-string v3, "stream_name"

    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 579
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->e()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v6

    .line 580
    if-eqz v6, :cond_2

    .line 581
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    invoke-direct {p0, v6}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/Application;)J

    move-result-wide v2

    cmp-long v7, v2, v10

    if-eqz v7, :cond_4

    .line 582
    :goto_1
    const-string v6, "application_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 585
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v6

    .line 586
    if-eqz v6, :cond_3

    .line 587
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    invoke-direct {p0, v6}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/Device;)J

    move-result-wide v2

    cmp-long v7, v2, v10

    if-eqz v7, :cond_5

    .line 588
    :goto_2
    const-string v6, "device_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 591
    :cond_3
    const-string v2, "DataSources"

    invoke-virtual {p0, v2, v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 594
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gms/fitness/l/af;->b(Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v6

    .line 595
    invoke-virtual {v5}, Landroid/content/ContentValues;->clear()V

    .line 596
    const-string v8, "data_source_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v5, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 597
    const-string v8, "data_type_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v8, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 598
    const-string v6, "DataSourceTypes"

    invoke-virtual {p0, v6, v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 599
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 581
    :cond_4
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "package"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Application;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "version"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Application;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "details_url"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Application;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Applications"

    invoke-virtual {p0, v3, v2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    goto :goto_1

    .line 587
    :cond_5
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "make"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Device;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "model"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Device;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "version"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Device;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "uid"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Device;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "type"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Device;->e()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v2, v3, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "platform_type"

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/Device;->f()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "Devices"

    invoke-virtual {p0, v3, v2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    goto/16 :goto_2

    .line 601
    :cond_6
    return-object v1
.end method

.method private static d(Ljava/util/List;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 3013
    new-instance v0, Ljava/util/HashSet;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 3014
    invoke-interface {v0, p0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 3015
    invoke-interface {p0}, Ljava/util/List;->clear()V

    .line 3016
    return-object v0
.end method

.method private d(Lcom/google/android/gms/fitness/data/Session;Z)V
    .locals 6

    .prologue
    .line 1428
    sget-object v0, Lcom/google/android/gms/fitness/apiary/c;->a:Lcom/google/android/gms/fitness/apiary/b;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/apiary/b;->a(Ljava/lang/Object;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    .line 1430
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1431
    const-string v2, "source_table"

    const-string v3, "Sessions"

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1432
    const-string v2, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1433
    const-string v2, "is_delete"

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 1434
    const-string v2, "content"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 1435
    const-string v0, "ChangeLog"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1436
    return-void
.end method

.method private e(Lcom/google/android/gms/fitness/data/DataSource;)J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 761
    if-nez p1, :cond_1

    .line 766
    :cond_0
    :goto_0
    return-wide v0

    .line 765
    :cond_1
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/fitness/l/af;->e(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v2

    .line 766
    invoke-interface {v2, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method private e(J)Lcom/google/android/gms/fitness/data/Device;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 381
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 383
    const-string v1, "Devices"

    const/4 v0, 0x6

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "make"

    aput-object v0, v2, v5

    const-string v0, "model"

    aput-object v0, v2, v6

    const/4 v0, 0x2

    const-string v3, "version"

    aput-object v3, v2, v0

    const/4 v0, 0x3

    const-string v3, "uid"

    aput-object v3, v2, v0

    const/4 v0, 0x4

    const-string v3, "type"

    aput-object v3, v2, v0

    const/4 v0, 0x5

    const-string v3, "platform_type"

    aput-object v3, v2, v0

    const-string v0, "%s = ?"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "_id"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 397
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 398
    const-string v0, "make"

    invoke-static {v6, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 399
    const-string v0, "model"

    invoke-static {v6, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 400
    const-string v0, "version"

    invoke-static {v6, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    .line 401
    const-string v0, "uid"

    invoke-static {v6, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 402
    const-string v0, "type"

    invoke-static {v6, v0}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    .line 403
    const-string v0, "platform_type"

    invoke-static {v6, v0}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v5

    .line 404
    new-instance v0, Lcom/google/android/gms/fitness/data/Device;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/data/Device;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 408
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    .line 406
    :cond_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private e(Ljava/util/Collection;)Ljava/util/Map;
    .locals 10

    .prologue
    const/4 v3, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 778
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 779
    invoke-interface {p1}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v6

    .line 808
    :goto_0
    return-object v0

    .line 783
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 784
    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v4

    .line 785
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7, v4}, Ljava/util/HashMap;-><init>(I)V

    .line 786
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 787
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v7, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 790
    :cond_1
    const-string v1, "DataSources"

    new-array v2, v3, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v8

    const-string v0, "identifier"

    aput-object v0, v2, v9

    const-string v0, "%s IN (%s)"

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "identifier"

    aput-object v5, v3, v8

    invoke-static {v4}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v9

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 801
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 802
    const-string v0, "identifier"

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v2, "_id"

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v6, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 808
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_0
.end method

.method private f(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/c/f;
    .locals 10

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 2613
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 2614
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v4

    .line 2615
    const-string v1, "SyncStatus"

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    const-string v6, "is_remote"

    aput-object v6, v2, v0

    const-string v6, "last_synced_time"

    aput-object v6, v2, v3

    const/4 v6, 0x2

    const-string v7, "min_local_timestamp"

    aput-object v7, v2, v6

    const/4 v6, 0x3

    const-string v7, "sync_token"

    aput-object v7, v2, v6

    const-string v6, "%s = ?"

    new-array v7, v3, [Ljava/lang/Object;

    const-string v8, "data_source_id"

    aput-object v8, v7, v0

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-array v7, v3, [Ljava/lang/String;

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v7, v0

    invoke-virtual {p0, v1, v2, v6, v7}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 2627
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2628
    new-instance v1, Lcom/google/android/gms/fitness/c/f;

    const-string v2, "is_remote"

    invoke-static {v9, v2}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    const-string v0, "last_synced_time"

    invoke-static {v9, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    const-string v0, "min_local_timestamp"

    invoke-static {v9, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    const-string v0, "sync_token"

    invoke-static {v9, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    move-object v2, p1

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/fitness/c/f;-><init>(Lcom/google/android/gms/fitness/data/DataSource;ZJJLjava/lang/String;)V

    .line 2634
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2641
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :goto_1
    return-object v1

    :cond_0
    move v3, v0

    .line 2628
    goto :goto_0

    .line 2637
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2638
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    const/4 v1, 0x0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private f(J)Lcom/google/android/gms/fitness/data/Application;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 493
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 495
    const-string v1, "Applications"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "package"

    aput-object v0, v2, v5

    const-string v0, "version"

    aput-object v0, v2, v6

    const/4 v0, 0x2

    const-string v3, "details_url"

    aput-object v3, v2, v0

    const-string v0, "%s = ?"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "_id"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 506
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 507
    const-string v0, "package"

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 508
    const-string v0, "version"

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    .line 509
    const-string v0, "details_url"

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 510
    new-instance v0, Lcom/google/android/gms/fitness/data/Application;

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/fitness/data/Application;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 514
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_0
    return-object v0

    .line 512
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private f(Ljava/util/Collection;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 816
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 817
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 818
    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 820
    :cond_0
    return-object v1
.end method

.method private g(J)Lcom/google/android/gms/fitness/data/DataSource;
    .locals 3

    .prologue
    .line 884
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 885
    const/4 v0, 0x0

    .line 889
    :goto_0
    return-object v0

    .line 888
    :cond_0
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 889
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/l/af;->c(Ljava/util/List;)Landroid/support/v4/g/g;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/g/g;->a(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    goto :goto_0
.end method

.method private h(J)J
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2770
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 2772
    const-string v1, "SyncStatus"

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    const-string v0, "%s = ?"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "data_source_id"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 2778
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "_id"

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2782
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 2778
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2782
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private h(Ljava/lang/String;)J
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 243
    const-string v1, "DataTypes"

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    const-string v0, "%s = ?"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "name"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 251
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-le v0, v6, :cond_0

    .line 252
    const-string v0, "Multiple data types are associated with name %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 254
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "_id"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 258
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    return-wide v0

    .line 254
    :cond_1
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 258
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private h()Ljava/util/Map;
    .locals 17

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 643
    sget-object v2, Lcom/google/android/gms/fitness/l/af;->j:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/fitness/l/af;->b:Ljava/lang/String;

    sget-object v4, Lcom/google/android/gms/fitness/l/af;->c:[Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/support/v4/g/g;

    move-result-object v11

    .line 646
    sget-object v2, Lcom/google/android/gms/fitness/l/af;->k:Ljava/lang/String;

    const-string v3, "%s = 1"

    new-array v4, v8, [Ljava/lang/Object;

    const-string v5, "is_remote"

    aput-object v5, v4, v7

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/fitness/l/af;->c:[Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/support/v4/g/g;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/support/v4/g/g;)Ljava/util/Set;

    move-result-object v12

    .line 652
    const-string v13, "min_end"

    .line 653
    const-string v14, "max_end"

    .line 654
    const-string v3, "DataPointRows"

    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const-string v2, "data_source_id"

    aput-object v2, v4, v7

    const-string v2, "min(%s) as %s"

    new-array v5, v9, [Ljava/lang/Object;

    const-string v6, "end_time"

    aput-object v6, v5, v7

    aput-object v13, v5, v8

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v8

    const-string v2, "max(%s) as %s"

    new-array v5, v9, [Ljava/lang/Object;

    const-string v6, "end_time"

    aput-object v6, v5, v7

    aput-object v14, v5, v8

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v9

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->b:Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/fitness/l/af;->c:[Ljava/lang/String;

    const-string v7, "data_source_id"

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v15

    .line 665
    :try_start_0
    new-instance v16, Ljava/util/HashMap;

    invoke-direct/range {v16 .. v16}, Ljava/util/HashMap;-><init>()V

    .line 666
    :goto_0
    invoke-interface {v15}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 667
    const-string v2, "data_source_id"

    invoke-static {v15, v2}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v3

    .line 668
    invoke-static {v15, v13}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 669
    invoke-static {v15, v14}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v8

    .line 671
    invoke-virtual {v11, v3, v4}, Landroid/support/v4/g/g;->a(J)Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    move-object v10, v0

    .line 672
    invoke-interface {v12, v10}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    .line 673
    new-instance v2, Lcom/google/android/gms/fitness/c/d;

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/fitness/c/d;-><init>(JZJJ)V

    move-object/from16 v0, v16

    invoke-interface {v0, v10, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 678
    :catchall_0
    move-exception v2

    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_0
    invoke-interface {v15}, Landroid/database/Cursor;->close()V

    return-object v16
.end method

.method private i(J)Ljava/util/List;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x12
    .end annotation

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2966
    const-string v1, "BleCharacteristics"

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "uuid"

    aput-object v0, v2, v5

    const-string v0, "properties"

    aput-object v0, v2, v6

    const/4 v0, 0x2

    const-string v3, "permissions"

    aput-object v3, v2, v0

    const-string v0, " %s = ?"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "device_id"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2977
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 2980
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2981
    const-string v2, "uuid"

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2982
    const-string v3, "properties"

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v3

    .line 2983
    const-string v4, "permissions"

    invoke-static {v1, v4}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    .line 2984
    new-instance v5, Landroid/bluetooth/BluetoothGattCharacteristic;

    invoke-static {v2}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    invoke-direct {v5, v2, v3, v4}, Landroid/bluetooth/BluetoothGattCharacteristic;-><init>(Ljava/util/UUID;II)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2988
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 2990
    return-object v0
.end method

.method private i(Ljava/lang/String;)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 827
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    .line 828
    const-string v1, "DataSources"

    new-array v2, v6, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v5

    const-string v0, "%s = ?"

    new-array v3, v6, [Ljava/lang/Object;

    const-string v4, "identifier"

    aput-object v4, v3, v5

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-array v4, v6, [Ljava/lang/String;

    aput-object p1, v4, v5

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 835
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 836
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 837
    const-string v2, "_id"

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 841
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-object v0
.end method

.method private j(J)V
    .locals 5

    .prologue
    .line 3084
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3085
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/ac;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/fitness/l/ac;->a(J)Lcom/google/android/gms/fitness/l/ad;

    move-result-object v0

    .line 3088
    iget-object v1, v0, Lcom/google/android/gms/fitness/l/ad;->a:Ljava/util/Set;

    .line 3089
    iget-wide v2, v0, Lcom/google/android/gms/fitness/l/ad;->b:J

    .line 3090
    const/4 v0, 0x0

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/Set;JLjava/lang/Long;)I

    move-result v0

    .line 3091
    const-string v2, "Deleted %d pre-aggregated data points from %d aggregate data sources"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 3094
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(JJLcom/google/android/gms/fitness/data/DataSource;Z)I
    .locals 9

    .prologue
    .line 1232
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1234
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1235
    invoke-direct {p0, p5}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v2

    move-object v0, p0

    move-object v1, p5

    move-wide v4, p1

    move-wide v6, p3

    .line 1236
    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/DataSource;JJJ)Ljava/util/Set;

    move-result-object v0

    .line 1238
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 1239
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 1240
    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/gms/fitness/l/af;->a(JLcom/google/android/gms/fitness/data/DataPoint;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1241
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1250
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    .line 1244
    :cond_1
    if-eqz p6, :cond_2

    .line 1245
    const/4 v0, 0x1

    :try_start_1
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/Collection;Z)V

    .line 1247
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1248
    invoke-interface {v1}, Ljava/util/List;->size()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 1250
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;J)I
    .locals 8

    .prologue
    .line 1608
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1610
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1611
    invoke-static {p1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    sub-long v4, p2, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/Set;JLjava/lang/Long;)I

    move-result v0

    .line 1617
    if-lez v0, :cond_0

    .line 1618
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v2

    .line 1619
    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/fitness/l/af;->h(J)J

    move-result-wide v4

    .line 1621
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1622
    const-string v6, "data_source_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v6, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1623
    const-string v2, "min_local_timestamp"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1624
    invoke-direct {p0, v4, v5, v1}, Lcom/google/android/gms/fitness/l/af;->a(JLandroid/content/ContentValues;)V

    .line 1626
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1627
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;Z)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1301
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1325
    :goto_0
    return v0

    .line 1304
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1306
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1307
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 1308
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 1309
    const-string v1, "Invalid data source specified: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1310
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    .line 1313
    :cond_1
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1314
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 1315
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v4, v5, v0}, Lcom/google/android/gms/fitness/l/af;->a(JLcom/google/android/gms/fitness/data/DataPoint;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1316
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 1325
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    .line 1319
    :cond_3
    if-eqz p3, :cond_4

    .line 1320
    const/4 v0, 0x1

    :try_start_2
    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/Collection;Z)V

    .line 1322
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 1323
    invoke-interface {v2}, Ljava/util/List;->size()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v0

    .line 1325
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)I
    .locals 12

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 1508
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1525
    :goto_0
    return v0

    .line 1512
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1514
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1515
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    move v6, v1

    move v2, v1

    .line 1517
    :goto_1
    if-ge v6, v7, :cond_3

    .line 1518
    add-int/lit16 v0, v6, 0x1f4

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 1519
    invoke-interface {p1, v6, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v8

    .line 1520
    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v0

    const/16 v3, 0x1f4

    if-gt v0, v3, :cond_1

    move v0, v5

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v0

    new-array v9, v0, [Ljava/lang/String;

    invoke-interface {v8}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    move v3, v1

    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v9, v3

    move v3, v4

    goto :goto_3

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    const-string v0, "ChangeLog"

    const-string v3, "%s IN (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string v11, "_id"

    aput-object v11, v4, v10

    const/4 v10, 0x1

    invoke-interface {v8}, Ljava/util/Collection;->size()I

    move-result v8

    invoke-static {v8}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v10

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v0, v3, v9}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v2, v0

    .line 1517
    add-int/lit16 v0, v6, 0x1f4

    move v6, v0

    goto :goto_1

    .line 1522
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1525
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    move v0, v2

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;)J
    .locals 3

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 522
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 523
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->d(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v0

    .line 524
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-wide v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;I)J
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 1111
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1113
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1114
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v1

    .line 1115
    cmp-long v0, v1, v4

    if-nez v0, :cond_0

    .line 1116
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    move-wide v0, v4

    .line 1120
    :goto_0
    return-wide v0

    .line 1118
    :cond_0
    const-wide/16 v4, -0x1

    move-object v0, p0

    move v3, p2

    :try_start_1
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(JIJ)J

    move-result-wide v0

    .line 1119
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1120
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataType;)J
    .locals 3

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 157
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->b(Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v0

    .line 159
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 160
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-wide v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Ljava/util/Collection;)J
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    .line 1130
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1132
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1133
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->f(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    .line 1135
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-eqz v1, :cond_0

    .line 1136
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 1155
    :goto_0
    return-wide v2

    .line 1139
    :cond_0
    :try_start_1
    const-string v1, "DataPointRows"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "min(%s)"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string v9, "end_time"

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const-string v5, "%s IN (%s)"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string v8, "data_source_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v8

    invoke-static {v8}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v6

    new-array v6, v6, [Ljava/lang/String;

    invoke-interface {v0, v6}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, v1, v4, v5, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 1146
    :try_start_2
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    .line 1150
    :goto_1
    :try_start_3
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 1152
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1155
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    move-wide v2, v0

    goto :goto_0

    :cond_1
    move-wide v0, v2

    .line 1146
    goto :goto_1

    .line 1150
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1155
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Ljava/util/concurrent/TimeUnit;)J
    .locals 4

    .prologue
    .line 3069
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->g:Landroid/content/Context;

    const-string v1, "fitness"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "cache_start_time"

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;JJI)Lcom/google/android/gms/fitness/data/DataSet;
    .locals 8

    .prologue
    .line 1917
    const-string v0, "Reading %s %2$tF %2$tT %3$tF %3$tT"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->k()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p2, p3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, p4, p5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1921
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 1922
    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-nez v2, :cond_0

    move-wide p4, v0

    .line 1925
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1927
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1928
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v6

    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_1

    const-string v0, "Found zero recorded data for %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 1929
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1931
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 1934
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v1

    .line 1935
    const/4 v2, -0x1

    if-eq p6, v2, :cond_3

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_3

    .line 1939
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1940
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 1941
    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/data/DataSet;->a(Ljava/lang/Iterable;)V

    .line 1946
    :goto_1
    const-string v0, "Found %s results"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1947
    return-object v1

    .line 1928
    :cond_1
    :try_start_1
    new-instance v5, Landroid/support/v4/g/g;

    invoke-direct {v5}, Landroid/support/v4/g/g;-><init>()V

    invoke-virtual {v5, v6, v7, p1}, Landroid/support/v4/g/g;->a(JLjava/lang/Object;)V

    const-string v0, "%s = ? AND %s BETWEEN ? AND ?"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "data_source_id"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "end_time"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x3

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x1

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    const/4 v0, -0x1

    if-ne p6, v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    move-object v0, p0

    move v3, p6

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;ILjava/lang/Boolean;Landroid/support/v4/g/g;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    .line 1931
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    .line 1943
    :cond_3
    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataSet;->a(Ljava/lang/Iterable;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataType;
    .locals 4

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 220
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 221
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->h(Ljava/lang/String;)J

    move-result-wide v0

    .line 222
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    .line 223
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v0

    .line 222
    :cond_0
    :try_start_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->d(J)Lcom/google/android/gms/fitness/data/DataType;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/BleDevice;)Lcom/google/k/k/a/f;
    .locals 2

    .prologue
    .line 2788
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/aa;

    .line 2790
    if-eqz v0, :cond_0

    .line 2791
    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/l/aa;->a(Lcom/google/android/gms/fitness/data/BleDevice;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 2795
    :goto_0
    new-instance v1, Lcom/google/android/gms/fitness/l/ag;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/l/ag;-><init>(Lcom/google/android/gms/fitness/l/af;)V

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/l/ah;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/l/ah;-><init>(Lcom/google/android/gms/fitness/l/af;)V

    invoke-static {v0, v1}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/f;

    move-result-object v0

    return-object v0

    .line 2793
    :cond_0
    new-instance v0, Lcom/google/android/gms/fitness/internal/ble/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/fitness/internal/ble/b;-><init>(Lcom/google/android/gms/fitness/data/BleDevice;)V

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/Application;)Ljava/lang/Long;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1637
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, p2, v3}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)Ljava/util/Set;

    move-result-object v0

    .line 1638
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1639
    const/4 v0, 0x0

    .line 1644
    :goto_0
    return-object v0

    .line 1641
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v1

    if-le v1, v2, :cond_1

    .line 1642
    const-string v1, "More than one data point inserted: %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1644
    :cond_1
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    goto :goto_0
.end method

.method public final a(IJ)Ljava/util/List;
    .locals 2

    .prologue
    .line 1441
    const-string v0, "DataPoints"

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;IJ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(J)Ljava/util/List;
    .locals 3

    .prologue
    .line 1447
    const-string v0, "Sessions"

    const/16 v1, 0x64

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;IJ)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataType;Z)Ljava/util/List;
    .locals 7

    .prologue
    .line 685
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 687
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 688
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->c(Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v2

    .line 689
    const-string v0, "%s = ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "data_type_id"

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 690
    if-eqz p2, :cond_0

    .line 691
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND %s = 1"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "is_remote"

    aput-object v6, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 696
    :goto_0
    sget-object v1, Lcom/google/android/gms/fitness/l/af;->k:Ljava/lang/String;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-direct {p0, v1, v0, v4}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/support/v4/g/g;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/support/v4/g/g;)Ljava/util/List;

    move-result-object v0

    .line 700
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 703
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v0

    .line 693
    :cond_0
    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND (%1$s = 0 OR %1$s IS NULL)"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "is_remote"

    aput-object v6, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 703
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;
    .locals 10

    .prologue
    .line 2503
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2505
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2508
    const-string v1, ""

    .line 2509
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2510
    const/4 v0, 0x1

    .line 2512
    if-eqz p1, :cond_0

    .line 2513
    const-string v0, "%s %s = ?"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, ""

    aput-object v4, v1, v3

    const/4 v3, 0x1

    const-string v4, "app_package"

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 2515
    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2516
    const/4 v0, 0x0

    .line 2518
    :cond_0
    if-eqz p2, :cond_1

    .line 2519
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "%s %s = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v0, :cond_5

    const-string v0, ""

    :goto_0
    aput-object v0, v4, v5

    const/4 v0, 0x1

    const-string v5, "name"

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2521
    invoke-interface {v2, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2522
    const/4 v0, 0x0

    .line 2524
    :cond_1
    if-eqz p3, :cond_2

    .line 2525
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " %s %s = ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v0, :cond_6

    const-string v0, ""

    :goto_1
    aput-object v0, v4, v5

    const/4 v0, 0x1

    const-string v5, "identifier"

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2527
    invoke-interface {v2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2528
    const/4 v0, 0x0

    .line 2530
    :cond_2
    const-wide/16 v4, -0x1

    cmp-long v3, p4, v4

    if-eqz v3, :cond_a

    .line 2531
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " %s %s >= ?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    if-eqz v0, :cond_7

    const-string v0, ""

    :goto_2
    aput-object v0, v4, v5

    const/4 v0, 0x1

    const-string v5, "start_time"

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 2533
    invoke-static {p4, p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2534
    const/4 v0, 0x0

    move v8, v0

    move-object v0, v1

    move v1, v8

    .line 2536
    :goto_3
    const-wide/16 v4, -0x1

    cmp-long v3, p6, v4

    if-eqz v3, :cond_3

    .line 2537
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " %s %s <= ?"

    const/4 v0, 0x2

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-eqz v1, :cond_8

    const-string v0, ""

    :goto_4
    aput-object v0, v5, v6

    const/4 v0, 0x1

    const-string v6, "end_time"

    aput-object v6, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2539
    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2541
    :cond_3
    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    const-wide/16 v4, -0x1

    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    .line 2543
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " %s ( %s < ? OR %s > ?)"

    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v6, 0x0

    if-eqz v1, :cond_9

    const-string v0, ""

    :goto_5
    aput-object v0, v5, v6

    const/4 v0, 0x1

    const-string v1, "start_time"

    aput-object v1, v5, v0

    const/4 v0, 0x2

    const-string v1, "end_time"

    aput-object v1, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 2547
    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2548
    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    move-object v1, v0

    .line 2551
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/fitness/l/af;->c(Ljava/lang/String;[Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 2553
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2556
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v0

    .line 2519
    :cond_5
    :try_start_1
    const-string v0, "AND"

    goto/16 :goto_0

    .line 2525
    :cond_6
    const-string v0, "AND"

    goto/16 :goto_1

    .line 2531
    :cond_7
    const-string v0, "AND"

    goto/16 :goto_2

    .line 2537
    :cond_8
    const-string v0, "AND"

    goto/16 :goto_4

    .line 2543
    :cond_9
    const-string v0, "AND"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_5

    .line 2556
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    :cond_a
    move v8, v0

    move-object v0, v1

    move v1, v8

    goto/16 :goto_3
.end method

.method public final a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->o:Lcom/google/android/gms/fitness/l/d;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/d;->a()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/l/b;)Ljava/util/Map;
    .locals 5

    .prologue
    .line 1979
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/l/b;->a()Ljava/util/Set;

    move-result-object v1

    .line 1980
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1981
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 2013
    :goto_0
    return-object v0

    .line 1984
    :cond_0
    const-string v0, "Fitness"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1985
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 1986
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 1987
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1989
    :cond_1
    const-string v0, "Performing bulk query for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v2, v3, v4

    invoke-static {v0, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1994
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1996
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1998
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->b(Lcom/google/android/gms/fitness/l/b;)Ljava/util/Collection;

    move-result-object v3

    .line 2000
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2002
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 2006
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 2007
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 2008
    invoke-static {v0}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v4

    invoke-interface {v2, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 2002
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    .line 2010
    :cond_3
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 2011
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/data/DataSet;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    goto :goto_3

    :cond_4
    move-object v0, v2

    .line 2013
    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;)Ljava/util/Set;
    .locals 6

    .prologue
    .line 1731
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1732
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 1741
    :goto_0
    return-object v0

    .line 1734
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1736
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1737
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 1738
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1741
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    .line 1737
    :cond_1
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/l/af;->b(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;)Ljava/util/Set;

    move-result-object v2

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-interface {v2, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 1741
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    .line 1737
    :cond_2
    :try_start_2
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->k()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v0

    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;)Ljava/util/Map;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public final a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)Ljava/util/Set;
    .locals 2

    .prologue
    .line 1653
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1655
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1656
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;)Ljava/util/Map;

    move-result-object v0

    .line 1657
    if-eqz p3, :cond_0

    .line 1658
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/Collection;Z)V

    .line 1660
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1662
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 1664
    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0

    .line 1662
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/c/c;)V
    .locals 8

    .prologue
    .line 2195
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2197
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2198
    iget-object v0, p1, Lcom/google/android/gms/fitness/c/c;->a:Ljava/lang/String;

    .line 2199
    iget-object v1, p1, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    .line 2200
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    .line 2201
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    .line 2203
    invoke-direct {p0, v0, v3, v2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long v4, v4, v6

    if-eqz v4, :cond_0

    .line 2225
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 2226
    :goto_0
    return-void

    .line 2207
    :cond_0
    :try_start_1
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 2208
    if-eqz v2, :cond_1

    .line 2209
    invoke-direct {p0, v2}, Lcom/google/android/gms/fitness/l/af;->b(Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v6

    .line 2210
    const-string v2, "data_type_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2212
    :cond_1
    if-eqz v3, :cond_2

    .line 2213
    invoke-direct {p0, v3}, Lcom/google/android/gms/fitness/l/af;->d(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v2

    .line 2214
    const-string v5, "data_source_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2216
    :cond_2
    const-string v2, "app_package"

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2217
    const-string v0, "sampling_delay"

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->d()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2218
    const-string v0, "accuracy_mode"

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2219
    const-string v0, "timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2220
    const-string v0, "realm"

    iget-object v1, p1, Lcom/google/android/gms/fitness/c/c;->c:Lcom/google/android/gms/fitness/c/e;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/c/e;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2222
    const-string v0, "Subscriptions"

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2223
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2225
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/Application;)V
    .locals 3

    .prologue
    .line 1670
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v0

    .line 1671
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1682
    :goto_0
    return-void

    .line 1674
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1676
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1677
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v0, p2, v2}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)V

    .line 1679
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1681
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/Application;Z)V
    .locals 1

    .prologue
    .line 1075
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1079
    :goto_0
    return-void

    .line 1078
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)Ljava/util/Set;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/l/aa;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3050
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "DataUpdateListener already set: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/fitness/l/af;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3052
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 3053
    return-void

    :cond_0
    move v0, v2

    .line 3050
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/l/ac;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 3057
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "DataUpdateListener already set: %s"

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v4}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 3059
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 3060
    return-void

    :cond_0
    move v0, v2

    .line 3057
    goto :goto_0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 3074
    const-string v0, "SQLiteFitnessStore:\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 3075
    const-string v0, "  BleClaimListener: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/af;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 3078
    const-string v0, "  DataUpdateListener: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/af;->n:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(C)Ljava/io/PrintWriter;

    .line 3081
    return-void
.end method

.method public final a(Ljava/util/Set;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 1552
    invoke-interface {p1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1579
    :goto_0
    return-void

    .line 1555
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1556
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1558
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1560
    const-string v1, "UPDATE %s set %s = %s + 1, %s = %s where %s in (%s)"

    const/4 v4, 0x7

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "ChangeLog"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "sync_tries"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "sync_tries"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "last_sync_ms"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v4, v5

    const/4 v2, 0x5

    const-string v3, "_id"

    aput-object v3, v4, v2

    const/4 v2, 0x6

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    invoke-virtual {p0, v1, v4}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v3

    .line 1571
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1572
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v3, v1, v6, v7}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    move v1, v2

    .line 1573
    goto :goto_1

    .line 1574
    :cond_1
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 1576
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1578
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/Session;Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2438
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2440
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2441
    const-string v2, "%s = ? AND %s = ? AND %s = ? AND %s = ?"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "app_package"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "name"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "start_time"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "end_time"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2447
    const-string v3, "Sessions"

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v6}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x3

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v6}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v2, v4}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 2457
    if-lez v2, :cond_0

    .line 2458
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v3}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/fitness/l/af;->j(J)V

    .line 2459
    if-eqz p2, :cond_0

    .line 2460
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3}, Lcom/google/android/gms/fitness/l/af;->d(Lcom/google/android/gms/fitness/data/Session;Z)V

    .line 2463
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2464
    const-string v3, "#Sessions deleted: "

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2465
    if-lez v2, :cond_1

    .line 2467
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return v0

    :cond_1
    move v0, v1

    .line 2465
    goto :goto_0

    .line 2467
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final b(J)I
    .locals 5

    .prologue
    .line 1584
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1586
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1588
    const-string v0, "%s <= ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "timestamp"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1590
    const-string v1, "ChangeLog"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 1598
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1599
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/fitness/data/DataSource;)I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1083
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1085
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1086
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->e(Lcom/google/android/gms/fitness/data/DataSource;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v8

    .line 1087
    const-wide/16 v0, -0x1

    cmp-long v0, v8, v0

    if-nez v0, :cond_0

    .line 1088
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    move v0, v6

    .line 1104
    :goto_0
    return v0

    .line 1091
    :cond_0
    :try_start_1
    const-string v1, "DataPointRows"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "COUNT(*)"

    aput-object v3, v2, v0

    const-string v0, "%s = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "data_source_id"

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 1097
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1098
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1101
    :try_start_3
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1104
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    :cond_1
    move v0, v6

    .line 1097
    goto :goto_1

    .line 1101
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1104
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final b(Ljava/util/Collection;)J
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    .line 1198
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1200
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1201
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->f(Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v0

    .line 1203
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v1

    if-eqz v1, :cond_0

    .line 1204
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 1224
    :goto_0
    return-wide v8

    .line 1207
    :cond_0
    :try_start_1
    const-string v1, "DataPointRows"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "end_time"

    aput-object v4, v2, v3

    const-string v3, "%s IN (%s)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "data_source_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/fitness/l/af;->a(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    const-string v0, "%s DESC"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string v7, "end_time"

    aput-object v7, v5, v6

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 1215
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-wide v0

    .line 1219
    :goto_1
    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 1221
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 1224
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    move-wide v8, v0

    goto :goto_0

    :cond_1
    move-wide v0, v8

    .line 1215
    goto :goto_1

    .line 1219
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 1224
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final b(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    .prologue
    .line 709
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 711
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 712
    const-string v0, "%s = ?"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string v3, "package"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 713
    sget-object v1, Lcom/google/android/gms/fitness/l/af;->l:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/support/v4/g/g;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/support/v4/g/g;)Ljava/util/List;

    move-result-object v0

    .line 717
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 720
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/Application;)V
    .locals 3

    .prologue
    .line 1688
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 1690
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 1691
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, p2, v2}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)V

    .line 1696
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1698
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 1699
    return-void

    .line 1698
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/fitness/data/Session;Z)V
    .locals 2

    .prologue
    .line 2380
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2382
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2383
    invoke-static {p1}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/Session;)Landroid/content/ContentValues;

    move-result-object v0

    .line 2384
    const-string v1, "Sessions"

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 2385
    if-eqz p2, :cond_0

    .line 2386
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Lcom/google/android/gms/fitness/data/Session;Z)V

    .line 2388
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->j(J)V

    .line 2389
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2391
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 2392
    return-void

    .line 2391
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final b(Ljava/util/List;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 2737
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2739
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2740
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/f;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->f()V

    iget-object v2, v0, Lcom/google/android/gms/fitness/c/f;->a:Lcom/google/android/gms/fitness/data/DataSource;

    invoke-direct {p0, v2}, Lcom/google/android/gms/fitness/l/af;->d(Lcom/google/android/gms/fitness/data/DataSource;)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/fitness/l/af;->h(J)J

    move-result-wide v4

    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    const-string v7, "data_source_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v6, v7, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "is_remote"

    iget-boolean v3, v0, Lcom/google/android/gms/fitness/c/f;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-wide v2, v0, Lcom/google/android/gms/fitness/c/f;->c:J

    cmp-long v2, v2, v10

    if-lez v2, :cond_0

    const-string v2, "last_synced_time"

    iget-wide v8, v0, Lcom/google/android/gms/fitness/c/f;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/fitness/c/f;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    const-string v2, "sync_token"

    iget-object v3, v0, Lcom/google/android/gms/fitness/c/f;->e:Ljava/lang/String;

    invoke-virtual {v6, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-wide v2, v0, Lcom/google/android/gms/fitness/c/f;->d:J

    cmp-long v2, v2, v10

    if-ltz v2, :cond_2

    const-string v2, "min_local_timestamp"

    iget-wide v8, v0, Lcom/google/android/gms/fitness/c/f;->d:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v6, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    :cond_2
    invoke-direct {p0, v4, v5, v6}, Lcom/google/android/gms/fitness/l/af;->a(JLandroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2743
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    .line 2741
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2743
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 2744
    return-void
.end method

.method public final b(Lcom/google/android/gms/fitness/c/c;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 2268
    iget-object v2, p1, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    .line 2269
    iget-object v3, p1, Lcom/google/android/gms/fitness/c/c;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/Subscription;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    invoke-direct {p0, v3, v4, v2}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)I

    move-result v2

    .line 2271
    if-le v2, v0, :cond_0

    .line 2272
    const-string v3, "Expected to remove 1 app subscription for %s. Removed %d."

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p1, v4, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2276
    :cond_0
    if-lez v2, :cond_1

    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final c()J
    .locals 4

    .prologue
    .line 2679
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2681
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2682
    const-string v0, "min(timestamp)"

    .line 2683
    const-string v1, "ChangeLog"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2688
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v2, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 2693
    :goto_0
    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2696
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-wide v0

    .line 2688
    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0

    .line 2693
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2696
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final c(Ljava/util/Collection;)Landroid/support/v4/g/s;
    .locals 4

    .prologue
    .line 2648
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2650
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2651
    new-instance v1, Landroid/support/v4/g/s;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Landroid/support/v4/g/s;-><init>(I)V

    .line 2653
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 2654
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/l/af;->c(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/c/f;

    move-result-object v3

    .line 2655
    if-eqz v3, :cond_0

    .line 2656
    invoke-virtual {v1, v0, v3}, Landroid/support/v4/g/s;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2661
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v1
.end method

.method public final c(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/c/f;
    .locals 1

    .prologue
    .line 2603
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2605
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2606
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->f(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/c/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2608
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final c(Ljava/lang/String;)Ljava/util/Set;
    .locals 2

    .prologue
    .line 848
    if-nez p1, :cond_0

    .line 849
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 861
    :goto_0
    return-object v0

    .line 852
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 854
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 855
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/af;->i(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 856
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/l/af;->c(Ljava/util/List;)Landroid/support/v4/g/g;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/support/v4/g/g;)Ljava/util/Set;

    move-result-object v0

    .line 858
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 861
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final c(J)V
    .locals 7

    .prologue
    .line 3064
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->g:Landroid/content/Context;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    const-string v2, "fitness"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "cache_start_time"

    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 3065
    return-void
.end method

.method public final c(Lcom/google/android/gms/fitness/data/Session;Z)V
    .locals 8

    .prologue
    .line 2398
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2400
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2401
    invoke-static {p1}, Lcom/google/android/gms/fitness/l/af;->a(Lcom/google/android/gms/fitness/data/Session;)Landroid/content/ContentValues;

    move-result-object v0

    .line 2402
    const-string v1, "Sessions"

    const-string v2, "%s = ? AND ((%s IS NOT NULL AND %s = ?) OR (%s IS NULL AND %s = ? AND %s = ?))"

    const/4 v3, 0x6

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "app_package"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "identifier"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "identifier"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "identifier"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "start_time"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "name"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->h()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v5}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v0, v2, v3}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2423
    if-lez v0, :cond_0

    .line 2424
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/l/af;->j(J)V

    .line 2425
    if-eqz p2, :cond_0

    .line 2426
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/l/af;->d(Lcom/google/android/gms/fitness/data/Session;Z)V

    .line 2429
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2431
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    .line 2432
    return-void

    .line 2431
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final d(Ljava/lang/String;)I
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2282
    invoke-direct {p0, p1, v0, v0}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)I

    move-result v0

    return v0
.end method

.method public final e(Ljava/lang/String;)Ljava/util/List;
    .locals 12

    .prologue
    const/4 v1, 0x2

    .line 2321
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2323
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2326
    sget-object v2, Lcom/google/android/gms/fitness/l/af;->b:Ljava/lang/String;

    .line 2327
    sget-object v0, Lcom/google/android/gms/fitness/l/af;->c:[Ljava/lang/String;

    .line 2328
    if-eqz p1, :cond_0

    .line 2329
    const-string v0, "%s = ?"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "app_package"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 2330
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p1, v0, v3

    .line 2333
    :cond_0
    const-string v3, "Subscriptions"

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "app_package"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "sampling_delay"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "data_type_id"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "data_source_id"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string v6, "accuracy_mode"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string v6, "realm"

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4, v2, v0}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 2347
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 2348
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2349
    const-string v0, "data_type_id"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2350
    const-string v0, "data_source_id"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v6

    .line 2351
    const-string v0, "app_package"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2352
    const-string v0, "sampling_delay"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v10

    .line 2353
    const-string v0, "accuracy_mode"

    invoke-static {v2, v0}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    .line 2355
    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/fitness/l/af;->d(J)Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v4

    .line 2356
    invoke-direct {p0, v6, v7}, Lcom/google/android/gms/fitness/l/af;->g(J)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v5

    .line 2357
    new-instance v6, Lcom/google/android/gms/fitness/data/u;

    invoke-direct {v6}, Lcom/google/android/gms/fitness/data/u;-><init>()V

    iput-object v5, v6, Lcom/google/android/gms/fitness/data/u;->a:Lcom/google/android/gms/fitness/data/DataSource;

    iput-object v4, v6, Lcom/google/android/gms/fitness/data/u;->b:Lcom/google/android/gms/fitness/data/DataType;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MICROSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v10, v11}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v4

    iput-wide v4, v6, Lcom/google/android/gms/fitness/data/u;->c:J

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :pswitch_1
    iput v0, v6, Lcom/google/android/gms/fitness/data/u;->d:I

    invoke-virtual {v6}, Lcom/google/android/gms/fitness/data/u;->a()Lcom/google/android/gms/fitness/data/Subscription;

    move-result-object v0

    .line 2363
    const-string v4, "realm"

    invoke-static {v2, v4}, Lcom/google/android/gms/fitness/l/af;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/fitness/c/e;->a(I)Lcom/google/android/gms/fitness/c/e;

    move-result-object v4

    .line 2365
    new-instance v5, Lcom/google/android/gms/fitness/c/c;

    invoke-direct {v5, v8, v0, v4}, Lcom/google/android/gms/fitness/c/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/fitness/data/Subscription;Lcom/google/android/gms/fitness/c/e;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2370
    :catchall_0
    move-exception v0

    :try_start_2
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2373
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0

    .line 2367
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2370
    :try_start_4
    invoke-interface {v2}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 2373
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v3

    .line 2357
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 3044
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->o:Lcom/google/android/gms/fitness/l/d;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/d;->c()V

    .line 3045
    invoke-super {p0}, Lcom/google/android/gms/fitness/l/a;->e()Z

    move-result v0

    return v0
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2852
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2854
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2856
    const-string v0, "BleDevices"

    const-string v3, "%s = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "address"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {p0, v0, v3, v4}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    .line 2859
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2860
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->m:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/l/aa;

    .line 2861
    if-eqz v0, :cond_0

    .line 2862
    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/l/aa;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2864
    :cond_0
    if-lez v3, :cond_1

    move v0, v1

    .line 2866
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return v0

    :cond_1
    move v0, v2

    .line 2864
    goto :goto_0

    .line 2866
    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/af;->o:Lcom/google/android/gms/fitness/l/d;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/d;->c()V

    .line 213
    return-void
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 6

    .prologue
    .line 3021
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 3023
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 3025
    const-string v1, "BleDevices"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "address"

    aput-object v3, v2, v0

    const-string v0, "%s = ?"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string v5, "address"

    aput-object v5, v3, v4

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v4, v0

    sget-object v5, Lcom/google/android/gms/fitness/l/af;->a:Ljava/lang/String;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v0

    .line 3032
    :try_start_1
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 3034
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 3035
    invoke-interface {v0}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 3038
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return v1

    .line 3034
    :catchall_0
    move-exception v1

    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 3035
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3038
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final o_()Ljava/util/List;
    .locals 3

    .prologue
    .line 726
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 728
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 729
    sget-object v0, Lcom/google/android/gms/fitness/l/af;->k:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/fitness/l/af;->b:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/fitness/l/af;->c:[Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/fitness/l/af;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/support/v4/g/g;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/l/af;->a(Landroid/support/v4/g/g;)Ljava/util/List;

    move-result-object v0

    .line 732
    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 735
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v0
.end method

.method public final p_()Ljava/util/List;
    .locals 30

    .prologue
    .line 2872
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/fitness/l/af;->q_()V

    .line 2874
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/fitness/l/af;->b()V

    .line 2876
    const-string v5, "BleDevices"

    const/16 v4, 0xa

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "_id"

    aput-object v7, v6, v4

    const/4 v4, 0x1

    const-string v7, "address"

    aput-object v7, v6, v4

    const/4 v4, 0x2

    const-string v7, "name"

    aput-object v7, v6, v4

    const/4 v4, 0x3

    const-string v7, "type"

    aput-object v7, v6, v4

    const/4 v4, 0x4

    const-string v7, "device_name"

    aput-object v7, v6, v4

    const/4 v4, 0x5

    const-string v7, "model_number"

    aput-object v7, v6, v4

    const/4 v4, 0x6

    const-string v7, "manufacturer"

    aput-object v7, v6, v4

    const/4 v4, 0x7

    const-string v7, "hardware_revision"

    aput-object v7, v6, v4

    const/16 v4, 0x8

    const-string v7, "firmware_revision"

    aput-object v7, v6, v4

    const/16 v4, 0x9

    const-string v7, "software_revision"

    aput-object v7, v6, v4

    sget-object v7, Lcom/google/android/gms/fitness/l/af;->b:Ljava/lang/String;

    sget-object v8, Lcom/google/android/gms/fitness/l/af;->c:[Ljava/lang/String;

    const-string v9, "address"

    move-object/from16 v4, p0

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/gms/fitness/l/af;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v24

    .line 2894
    const/4 v5, 0x0

    .line 2895
    const/4 v4, 0x0

    .line 2896
    const/4 v7, 0x0

    .line 2897
    const/4 v8, 0x0

    .line 2898
    const/4 v9, 0x0

    .line 2899
    const/4 v10, 0x0

    .line 2900
    const/4 v11, 0x0

    .line 2901
    const/4 v12, 0x0

    .line 2902
    :try_start_1
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 2903
    new-instance v26, Ljava/util/ArrayList;

    invoke-direct/range {v26 .. v26}, Ljava/util/ArrayList;-><init>()V

    .line 2904
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v20, v4

    move-object/from16 v22, v5

    .line 2905
    :goto_0
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2906
    const-string v4, "_id"

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/l/af;->c(Landroid/database/Cursor;Ljava/lang/String;)J

    move-result-wide v4

    .line 2907
    const-string v13, "address"

    move-object/from16 v0, v24

    invoke-static {v0, v13}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 2908
    const-string v13, "name"

    move-object/from16 v0, v24

    invoke-static {v0, v13}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 2909
    const-string v13, "type"

    move-object/from16 v0, v24

    invoke-static {v0, v13}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v27

    .line 2910
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/fitness/l/af;->i(J)Ljava/util/List;

    move-result-object v13

    .line 2912
    const-string v4, "device_name"

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2913
    const-string v4, "model_number"

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2914
    const-string v4, "manufacturer"

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2915
    const-string v4, "hardware_revision"

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2917
    const-string v4, "firmware_revision"

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 2919
    const-string v4, "software_revision"

    move-object/from16 v0, v24

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/l/af;->d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 2922
    move-object/from16 v0, v23

    move-object/from16 v1, v22

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface/range {v25 .. v25}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2923
    invoke-static/range {v25 .. v25}, Lcom/google/android/gms/fitness/l/af;->d(Ljava/util/List;)Ljava/util/Set;

    move-result-object v28

    .line 2924
    invoke-static/range {v28 .. v28}, Lcom/google/android/gms/fitness/c/a/c;->b(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v29

    .line 2926
    new-instance v4, Lcom/google/android/gms/fitness/internal/ble/b;

    new-instance v5, Lcom/google/android/gms/fitness/data/BleDevice;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    move-object/from16 v2, v28

    move-object/from16 v3, v29

    invoke-direct {v5, v0, v1, v2, v3}, Lcom/google/android/gms/fitness/data/BleDevice;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V

    invoke-direct/range {v4 .. v12}, Lcom/google/android/gms/fitness/internal/ble/b;-><init>(Lcom/google/android/gms/fitness/data/BleDevice;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2932
    :cond_0
    move-object/from16 v0, v25

    move-object/from16 v1, v27

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v6, v13

    move-object v12, v14

    move-object v11, v15

    move-object/from16 v10, v16

    move-object/from16 v9, v17

    move-object/from16 v8, v18

    move-object/from16 v7, v19

    move-object/from16 v20, v21

    move-object/from16 v22, v23

    .line 2942
    goto/16 :goto_0

    .line 2944
    :cond_1
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    .line 2945
    invoke-static/range {v25 .. v25}, Lcom/google/android/gms/fitness/l/af;->d(Ljava/util/List;)Ljava/util/Set;

    move-result-object v13

    .line 2946
    invoke-static {v13}, Lcom/google/android/gms/fitness/c/a/c;->b(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v14

    .line 2948
    new-instance v4, Lcom/google/android/gms/fitness/internal/ble/b;

    new-instance v5, Lcom/google/android/gms/fitness/data/BleDevice;

    move-object/from16 v0, v22

    move-object/from16 v1, v20

    invoke-direct {v5, v0, v1, v13, v14}, Lcom/google/android/gms/fitness/data/BleDevice;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)V

    invoke-direct/range {v4 .. v12}, Lcom/google/android/gms/fitness/internal/ble/b;-><init>(Lcom/google/android/gms/fitness/data/BleDevice;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v26

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2956
    :cond_2
    :try_start_2
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 2957
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2960
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    return-object v26

    .line 2956
    :catchall_0
    move-exception v4

    :try_start_3
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 2957
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 2960
    :catchall_1
    move-exception v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/fitness/l/af;->d()V

    throw v4
.end method
