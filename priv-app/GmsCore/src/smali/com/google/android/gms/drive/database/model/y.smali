.class public final Lcom/google/android/gms/drive/database/model/y;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

.field final b:Ljava/lang/Long;

.field private final c:Lcom/google/android/gms/drive/database/model/EntrySpec;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;J)V
    .locals 3

    .prologue
    .line 30
    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->a()Lcom/google/android/gms/drive/database/model/z;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 31
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 32
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    if-nez p4, :cond_0

    invoke-virtual {p3}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/y;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    .line 37
    iput-object p4, p0, Lcom/google/android/gms/drive/database/model/y;->b:Ljava/lang/Long;

    .line 38
    invoke-virtual {p0, p5, p6}, Lcom/google/android/gms/drive/database/model/y;->d(J)V

    .line 39
    return-void

    .line 33
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->a:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/y;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 57
    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->c:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->d:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 61
    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->e:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/y;->a:Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->b()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 63
    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->b:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/y;->b:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 65
    :cond_0
    return-void
.end method
