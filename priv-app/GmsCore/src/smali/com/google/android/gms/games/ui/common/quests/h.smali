.class public final Lcom/google/android/gms/games/ui/common/quests/h;
.super Lcom/google/android/gms/games/ui/common/quests/QuestFragment;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/quests/QuestFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/common/quests/QuestFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 23
    new-instance v1, Lcom/google/android/gms/games/ui/bn;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/quests/h;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/bn;-><init>(Landroid/content/Context;)V

    .line 24
    sget v0, Lcom/google/android/gms/p;->iL:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/bn;->f(I)V

    .line 27
    sget v0, Lcom/google/android/gms/p;->kD:I

    .line 28
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/quests/h;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v2

    .line 29
    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/bh;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 31
    :cond_0
    sget v0, Lcom/google/android/gms/p;->ig:I

    .line 33
    :cond_1
    sget v2, Lcom/google/android/gms/h;->aA:I

    const/4 v3, 0x0

    invoke-virtual {p0, v2, v0, v3}, Lcom/google/android/gms/games/ui/common/quests/h;->a(III)V

    .line 36
    new-instance v0, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 37
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/quests/h;->l:Lcom/google/android/gms/games/ui/common/quests/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 40
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/quests/h;->a(Landroid/support/v7/widget/bv;)V

    .line 41
    return-void
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    .line 45
    invoke-super {p0}, Lcom/google/android/gms/games/ui/common/quests/QuestFragment;->onDetach()V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/quests/h;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    const-string v0, "QuestInboxFragment"

    const-string v1, "Tearing down without finishing creation"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :cond_0
    return-void
.end method
