.class public final Lcom/google/android/gms/wallet/service/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/p;->a:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/p;->b:Ljava/lang/String;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/service/n;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v0, 0x0

    .line 48
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/p;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 49
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 78
    :cond_0
    :goto_0
    return-object v0

    .line 53
    :cond_1
    const/4 v2, 0x0

    .line 55
    :try_start_0
    const-string v1, "get_auth_token"

    move v8, v0

    move-object v0, v2

    move-object v2, v1

    move v1, v8

    .line 56
    :goto_1
    if-gt v1, v7, :cond_0

    .line 57
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v3, "get_auth_tokens"

    invoke-direct {v0, v3}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/p;->a:Landroid/content/Context;

    invoke-virtual {p1, v4}, Lcom/google/android/gms/wallet/service/n;->a(Landroid/content/Context;)Lcom/google/android/gms/wallet/a/b;

    move-result-object v4

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v0, v3, v5}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/p;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 58
    invoke-virtual {p1, v4}, Lcom/google/android/gms/wallet/service/n;->a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 59
    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/service/n;->a(Lcom/google/android/gms/wallet/shared/service/ServerResponse;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 60
    sget-object v3, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/p;->a:Landroid/content/Context;

    iget-object v2, v4, Lcom/google/android/gms/wallet/a/b;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 64
    const-string v2, "get_fresh_auth_token"
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 56
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, v3

    goto :goto_1

    .line 70
    :catch_0
    move-exception v0

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/p;->b:Ljava/lang/String;

    const-string v2, "Could not get auth token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 72
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->c:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0

    .line 73
    :catch_1
    move-exception v0

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/p;->b:Ljava/lang/String;

    const-string v2, "Could not get auth token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 75
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0

    .line 76
    :catch_2
    move-exception v0

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/p;->b:Ljava/lang/String;

    const-string v2, "Could not get auth token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 78
    sget-object v0, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_0
.end method
