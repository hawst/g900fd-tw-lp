.class public abstract Lcom/google/android/gms/plus/service/a/d;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/server/ClientContext;

.field final b:Lcom/google/android/gms/plus/internal/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/d;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/d;->b:Lcom/google/android/gms/plus/internal/c;

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 46
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/plus/service/a/d;->b(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)Landroid/util/Pair;

    move-result-object v3

    .line 49
    iget-object v0, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/c;

    .line 51
    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v4, "pendingIntent"

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 55
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/d;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->c()I

    move-result v5

    iget-object v0, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    invoke-interface {v4, v5, v1, v0}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 67
    :goto_1
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 58
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 59
    const-string v3, "pendingIntent"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/d;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v0, v7, v1, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    .line 62
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/d;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/d;->b:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v1, v7, v0, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    .line 65
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/d;->b:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1

    :cond_0
    move-object v1, v2

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/d;->b:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/d;->b:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 76
    :cond_0
    return-void
.end method

.method public abstract b(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)Landroid/util/Pair;
.end method
