.class public final Lcom/google/android/gms/plus/sharebox/AudienceView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/ImageView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ProgressBar;

.field private final e:Ljava/lang/String;

.field private f:Lcom/google/android/gms/common/people/data/Audience;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/plus/sharebox/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->setSaveEnabled(Z)V

    .line 65
    sget-object v0, Lcom/google/android/gms/common/people/data/a;->a:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    .line 67
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 69
    sget v1, Lcom/google/android/gms/l;->eQ:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 70
    sget v0, Lcom/google/android/gms/j;->bi:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->a:Landroid/view/View;

    .line 71
    sget v0, Lcom/google/android/gms/j;->aS:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->b:Landroid/widget/ImageView;

    .line 72
    sget v0, Lcom/google/android/gms/j;->aT:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->c:Landroid/widget/TextView;

    .line 73
    sget v0, Lcom/google/android/gms/j;->pR:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->d:Landroid/widget/ProgressBar;

    .line 74
    sget v0, Lcom/google/android/gms/p;->vv:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->e:Ljava/lang/String;

    .line 76
    return-void
.end method

.method private c()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v4, 0x0

    const/4 v6, 0x6

    const/4 v5, 0x0

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->a:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->c:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->invalidate()V

    .line 132
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const v1, 0x7fffffff

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, v4

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v2, v6

    :goto_2
    if-lt v2, v1, :cond_1

    if-nez v3, :cond_9

    :cond_1
    move-object v1, v0

    move v0, v2

    :goto_3
    move-object v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v2

    packed-switch v2, :pswitch_data_1

    :pswitch_1
    move v2, v6

    goto :goto_2

    :pswitch_2
    const/4 v2, 0x1

    goto :goto_2

    :pswitch_3
    const/4 v2, 0x2

    goto :goto_2

    :pswitch_4
    const/4 v2, 0x3

    goto :goto_2

    :pswitch_5
    const/4 v2, 0x4

    goto :goto_2

    :pswitch_6
    const/4 v2, 0x5

    goto :goto_2

    :pswitch_7
    move v2, v6

    goto :goto_2

    .line 106
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/google/android/gms/common/people/views/c;->a(Landroid/content/Context;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/views/d;

    move-result-object v0

    iget v2, v0, Lcom/google/android/gms/common/people/views/d;->c:I

    .line 108
    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    :goto_4
    move-object v0, v4

    :goto_5
    if-nez v0, :cond_6

    .line 111
    :goto_6
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v1, v5

    .line 113
    :goto_7
    if-ge v1, v6, :cond_7

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 115
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    add-int/lit8 v0, v6, -0x1

    if-ge v1, v0, :cond_3

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->e:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 108
    :pswitch_8
    invoke-virtual {v3}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    :pswitch_9
    goto :goto_4

    :pswitch_a
    sget v0, Lcom/google/android/gms/f;->ai:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :pswitch_b
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->g:Z

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/gms/f;->ak:I

    :goto_8
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_4
    sget v0, Lcom/google/android/gms/f;->aj:I

    goto :goto_8

    :pswitch_c
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->g:Z

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/gms/f;->ak:I

    :goto_9
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_5
    sget v0, Lcom/google/android/gms/f;->aj:I

    goto :goto_9

    :pswitch_d
    sget v0, Lcom/google/android/gms/f;->ai:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :pswitch_e
    sget v0, Lcom/google/android/gms/f;->aj:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :pswitch_f
    sget v0, Lcom/google/android/gms/f;->ai:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_5

    :cond_6
    new-instance v4, Landroid/graphics/PorterDuffColorFilter;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->SRC_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v0, v1}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    goto/16 :goto_6

    .line 122
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->a:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 125
    if-eqz v4, :cond_8

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 128
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    move-object v1, v3

    goto/16 :goto_3

    .line 102
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
    .end packed-switch

    .line 108
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_8
        :pswitch_f
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_a
        :pswitch_9
        :pswitch_b
        :pswitch_e
        :pswitch_d
        :pswitch_c
    .end packed-switch
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 2

    .prologue
    .line 82
    invoke-static {p1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->d:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->c:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 88
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    .line 90
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->c()V

    .line 91
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->g:Z

    if-eq v0, p1, :cond_0

    .line 253
    iput-boolean p1, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->g:Z

    .line 254
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->c()V

    .line 256
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->d:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method protected final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1

    .prologue
    .line 192
    check-cast p1, Landroid/os/Bundle;

    .line 193
    const-string v0, "parent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 194
    const-string v0, "underage"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->g:Z

    .line 195
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 198
    :cond_0
    return-void
.end method

.method protected final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 202
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 203
    const-string v1, "parent"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 204
    const-string v1, "underage"

    iget-boolean v2, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->g:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/AudienceView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 207
    const-string v1, "audience"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/AudienceView;->f:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 209
    :cond_0
    return-object v0
.end method
