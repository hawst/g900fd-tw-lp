.class public final Lcom/google/android/gms/wallet/common/ui/ba;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method private constructor <init>(II)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 38
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/ba;->a:I

    .line 39
    iput p2, p0, Lcom/google/android/gms/wallet/common/ui/ba;->b:I

    .line 40
    return-void
.end method

.method public static a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/wallet/common/ui/ba;
    .locals 3

    .prologue
    .line 31
    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    new-instance v2, Lcom/google/android/gms/wallet/common/ui/ba;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/ba;-><init>(II)V

    return-object v2
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public final getIntrinsicHeight()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ba;->b:I

    return v0
.end method

.method public final getIntrinsicWidth()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ba;->a:I

    return v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
