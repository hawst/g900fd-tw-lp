.class final Lcom/google/android/gms/googlehelp/helpactivities/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field final synthetic a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/p;->a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/p;->a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->b(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ad/a/a/m;

    iget-object v0, v0, Lcom/google/ad/a/a/m;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 207
    :goto_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    .line 208
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/helpactivities/p;->a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-static {v4}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/TextView;

    move-result-object v4

    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 209
    return-void

    :cond_0
    move v0, v2

    .line 205
    goto :goto_0

    :cond_1
    move v3, v2

    .line 207
    goto :goto_1

    :cond_2
    move v1, v2

    .line 208
    goto :goto_2
.end method

.method public final beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 196
    return-void
.end method

.method public final onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 201
    return-void
.end method
