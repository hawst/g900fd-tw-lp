.class final Lcom/google/android/gms/cast/c/f;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/c/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/c/e;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Wifi scan complete - checking for CastNearby device."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/e;->a(Lcom/google/android/gms/cast/c/e;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-static {v0, v2}, Lcom/google/android/gms/cast/c/e;->a(Lcom/google/android/gms/cast/c/e;Z)Z

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/e;->b(Lcom/google/android/gms/cast/c/e;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v4

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Number of results: %d"

    new-array v3, v9, [Ljava/lang/Object;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    move v1, v2

    .line 54
    :goto_0
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 55
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    .line 56
    if-eqz v0, :cond_1

    iget-object v3, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 59
    iget-object v3, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    .line 62
    iget-object v3, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v6, "wifi ap - %s,%s,%d"

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    iget-object v8, v0, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    aput-object v8, v7, v2

    iget-object v8, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    aput-object v8, v7, v9

    iget v8, v0, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v10

    invoke-virtual {v3, v6, v7}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 64
    iget-object v3, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-static {v3}, Lcom/google/android/gms/cast/c/e;->c(Lcom/google/android/gms/cast/c/e;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_1

    aget-object v8, v6, v3

    .line 65
    invoke-virtual {v5, v8}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 68
    iget-object v3, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 69
    iget-object v3, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v6, "CastNearby device within wifi range %s"

    new-array v7, v9, [Ljava/lang/Object;

    iget-object v0, v0, Landroid/net/wifi/ScanResult;->BSSID:Ljava/lang/String;

    aput-object v0, v7, v2

    invoke-virtual {v3, v6, v7}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/e;->d(Lcom/google/android/gms/cast/c/e;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/g;

    .line 74
    if-eqz v0, :cond_3

    .line 75
    iget-object v3, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 76
    iget-object v3, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v3, v3, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v6, "Previously PAIRED device found - %s, %s"

    new-array v7, v10, [Ljava/lang/Object;

    aput-object v5, v7, v2

    iget-object v5, v0, Lcom/google/android/gms/cast/c/g;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/CastDevice;->b()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v7, v9

    invoke-virtual {v3, v6, v7}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 79
    :cond_0
    invoke-static {}, Lcom/google/android/gms/cast/c/e;->d()I

    move-result v3

    iput v3, v0, Lcom/google/android/gms/cast/c/g;->d:I

    .line 54
    :cond_1
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_0

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "CastNearby device within wifi range"

    new-array v6, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    .line 81
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v3, "unpaired device found - %s"

    new-array v6, v9, [Ljava/lang/Object;

    aput-object v5, v6, v2

    invoke-virtual {v0, v3, v6}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 84
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-static {v0, v9}, Lcom/google/android/gms/cast/c/e;->a(Lcom/google/android/gms/cast/c/e;Z)Z

    goto :goto_3

    .line 64
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_1

    .line 90
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    iget-object v0, v0, Lcom/google/android/gms/cast/c/e;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Wifi scan check complete."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/cast/c/f;->a:Lcom/google/android/gms/cast/c/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/e;->c()V

    .line 92
    return-void
.end method
