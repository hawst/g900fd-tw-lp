.class public Lcom/google/android/gms/drive/api/a/am;
.super Lcom/google/android/gms/drive/api/a/c;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;

.field private final f:Lcom/google/android/gms/drive/realtime/cache/o;

.field private final g:Lcom/google/android/gms/drive/realtime/cache/w;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/drive/api/a/c;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/am;->e:Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;

    .line 41
    invoke-interface {p1}, Lcom/google/android/gms/drive/api/d;->q()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/realtime/b/a;->a(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)Lcom/google/android/gms/drive/realtime/b/a;

    move-result-object v1

    .line 44
    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/b/a;->a()Lcom/google/android/gms/drive/realtime/cache/o;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/api/a/am;->f:Lcom/google/android/gms/drive/realtime/cache/o;

    .line 45
    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->D()Lcom/google/android/gms/drive/realtime/cache/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/api/a/am;->g:Lcom/google/android/gms/drive/realtime/cache/w;

    .line 46
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 3

    .prologue
    .line 61
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 62
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 63
    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 65
    :cond_0
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 51
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/am;->f:Lcom/google/android/gms/drive/realtime/cache/o;

    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/am;->e:Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;->a()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/api/a/am;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/i;->c()V

    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    new-instance v3, Lcom/google/android/gms/drive/realtime/e;

    invoke-direct {v3, v0, p1}, Lcom/google/android/gms/drive/realtime/e;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;)V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/drive/realtime/cache/o;->b(Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/database/model/br;

    move-result-object v0

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/android/gms/drive/database/model/br;->g:Z

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/br;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, v1, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, v1, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/am;->f:Lcom/google/android/gms/drive/realtime/cache/o;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/am;->e:Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/RealtimeDocumentSyncRequest;->b()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/api/a/am;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/drive/realtime/cache/o;->a(Lcom/google/android/gms/drive/auth/g;Ljava/util/List;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/am;->g:Lcom/google/android/gms/drive/realtime/cache/w;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/w;->b()V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/am;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 58
    return-void
.end method
