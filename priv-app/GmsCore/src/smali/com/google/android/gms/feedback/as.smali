.class public final Lcom/google/android/gms/feedback/as;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field private final f:Lcom/android/volley/x;

.field private final g:Lcom/google/android/gms/googlehelp/common/k;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/google/android/gms/googlehelp/common/k;)V
    .locals 2

    .prologue
    .line 36
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/feedback/as;->f:Lcom/android/volley/x;

    .line 38
    iput-object p3, p0, Lcom/google/android/gms/feedback/as;->g:Lcom/google/android/gms/googlehelp/common/k;

    .line 39
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 67
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v2

    .line 68
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 77
    :goto_0
    return-object v0

    .line 71
    :cond_0
    new-instance v0, Lcom/google/android/gms/feedback/as;

    invoke-direct {v0, p0, v2, p1}, Lcom/google/android/gms/feedback/as;-><init>(Ljava/lang/String;Lcom/android/volley/x;Lcom/google/android/gms/googlehelp/common/k;)V

    .line 72
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 74
    :try_start_0
    sget-object v0, Lcom/google/android/gms/feedback/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v0}, Lcom/android/volley/toolbox/aa;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 75
    :catch_0
    move-exception v0

    .line 76
    :goto_1
    const-string v2, "SuggestionsTestRequest"

    const-string v3, "Fetching Test suggestion failed."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 77
    goto :goto_0

    .line 75
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 3

    .prologue
    .line 51
    :try_start_0
    new-instance v1, Ljava/lang/String;

    iget-object v0, p1, Lcom/android/volley/m;->b:[B

    iget-object v2, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    invoke-static {v2}, Lcom/android/volley/toolbox/i;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/feedback/as;->g:Lcom/google/android/gms/googlehelp/common/k;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/googlehelp/common/k;->b:Lcom/google/android/gms/googlehelp/common/k;

    :cond_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-static {v2, v0}, Lcom/google/android/gms/googlehelp/common/k;->a(Lorg/json/JSONObject;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    .line 56
    if-nez v0, :cond_1

    .line 57
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Unsupported/empty response received"

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    .line 59
    :cond_1
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    :goto_1
    const-string v1, "SuggestionsTestRequest"

    const-string v2, "Parsing suggestion response data failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 62
    new-instance v1, Lcom/android/volley/ac;

    invoke-direct {v1, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0

    .line 60
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/gms/googlehelp/common/k;

    iget-object v0, p0, Lcom/google/android/gms/feedback/as;->f:Lcom/android/volley/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/feedback/as;->f:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method
