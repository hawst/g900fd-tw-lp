.class public final Lcom/google/android/gms/plus/oob/FieldViewPronoun;
.super Lcom/google/android/gms/plus/oob/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/oob/t;


# instance fields
.field d:Lcom/google/android/gms/plus/oob/s;

.field e:Lcom/google/android/gms/plus/oob/GenderSpinner;

.field f:Ljava/util/ArrayList;

.field g:I

.field private h:Landroid/widget/TextView;

.field private i:Ljava/util/ArrayList;

.field private j:Ljava/util/Map;

.field private k:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;-><init>(Landroid/content/Context;Z)V

    .line 85
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    .line 89
    return-void
.end method

.method private c(I)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 294
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->r()Z

    move-result v1

    if-nez v1, :cond_0

    .line 301
    :goto_0
    return-object v0

    .line 297
    :cond_0
    if-ltz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lt p1, v1, :cond_2

    .line 298
    :cond_1
    const-string v1, "FieldView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid position for options field: id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->h()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 301
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private l()Lcom/google/android/gms/plus/oob/s;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->u()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    move-object v1, v0

    .line 247
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->f:Ljava/util/ArrayList;

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v3

    .line 249
    :goto_1
    if-ge v2, v4, :cond_2

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;

    .line 251
    if-eqz v1, :cond_0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 253
    iput v2, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    .line 254
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k:Z

    .line 256
    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->f:Ljava/util/ArrayList;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->t()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/i;->l()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 258
    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->eo:I

    iget-object v4, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->f:Ljava/util/ArrayList;

    new-array v3, v3, [Ljava/lang/CharSequence;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/oob/s;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-object v0
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->a:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/l;->el:I

    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/l;->ek:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V
    .locals 6

    .prologue
    .line 145
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/plus/oob/d;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/b;Lcom/google/android/gms/plus/oob/e;)V

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->j:Ljava/util/Map;

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->q()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->d()Ljava/lang/String;

    move-result-object v4

    const-string v5, "s_"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->j:Ljava/util/Map;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->d()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 147
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->l()Lcom/google/android/gms/plus/oob/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    const v1, 0x1090009

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/s;->setDropDownViewResource(I)V

    .line 149
    sget v0, Lcom/google/android/gms/j;->mR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->h:Landroid/widget/TextView;

    .line 150
    sget v0, Lcom/google/android/gms/j;->mS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/oob/GenderSpinner;

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->a(Lcom/google/android/gms/plus/oob/t;)V

    .line 152
    const/4 v0, 0x0

    .line 153
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->n()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 154
    invoke-interface {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/b;->m()Lcom/google/android/gms/plus/service/v1whitelisted/models/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/f;->e()Ljava/lang/String;

    move-result-object v0

    .line 156
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->vi:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 159
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 161
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    .line 164
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k()V

    .line 165
    return-void
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 200
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 207
    :goto_0
    return-void

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/s;->a()V

    .line 204
    iput p1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->c:Lcom/google/android/gms/plus/oob/e;

    invoke-interface {v0}, Lcom/google/android/gms/plus/oob/e;->a()V

    .line 206
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/s;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    .line 135
    :goto_0
    return-object v0

    .line 134
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->i()Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;-><init>()V

    iget v2, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/j;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/i;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/i;)Lcom/google/android/gms/plus/service/v1whitelisted/models/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/c;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/b;

    move-result-object v0

    goto :goto_0
.end method

.method final k()V
    .locals 3

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    .line 266
    :goto_0
    if-nez v0, :cond_2

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->h:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->h:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    :goto_1
    return-void

    .line 265
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->j:Ljava/util/Map;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "s_"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/h;->e()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 270
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->h:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public final onAttachedToWindow()V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    if-eqz v0, :cond_0

    .line 170
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    .line 175
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k()V

    .line 177
    :cond_0
    return-void

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getPrompt()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/s;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 103
    instance-of v0, p1, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;

    if-nez v0, :cond_0

    .line 104
    invoke-super {p0, p1}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 115
    :goto_0
    return-void

    .line 107
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;

    .line 108
    invoke-virtual {p1}, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lcom/google/android/gms/plus/oob/d;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 109
    iget-boolean v0, p1, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k:Z

    .line 110
    iget v0, p1, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;->b:I

    iput v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    .line 111
    iget v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    iget v1, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->g:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/oob/GenderSpinner;->setSelection(I)V

    .line 114
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k()V

    goto :goto_0
.end method

.method public final onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/android/gms/plus/oob/d;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 94
    new-instance v1, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;

    invoke-direct {v1, v0}, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 95
    iget-boolean v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->k:Z

    iput-boolean v0, v1, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;->a:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->d:Lcom/google/android/gms/plus/oob/s;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/s;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    iput v0, v1, Lcom/google/android/gms/plus/oob/FieldViewPronoun$SavedState;->b:I

    .line 98
    return-object v1

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/FieldViewPronoun;->e:Lcom/google/android/gms/plus/oob/GenderSpinner;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/oob/GenderSpinner;->getSelectedItemPosition()I

    move-result v0

    goto :goto_0
.end method
