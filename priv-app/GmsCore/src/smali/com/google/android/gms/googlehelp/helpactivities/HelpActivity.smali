.class public Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;
.super Lcom/google/android/gms/googlehelp/helpactivities/r;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/common/b;


# instance fields
.field private b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private c:Lcom/google/android/gms/googlehelp/common/r;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Lcom/google/android/gms/googlehelp/common/a;)V

    .line 132
    new-instance v0, Lcom/google/android/gms/googlehelp/common/r;

    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-direct {v0, v3, p1}, Lcom/google/android/gms/googlehelp/common/r;-><init>(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->c:Lcom/google/android/gms/googlehelp/common/r;

    .line 135
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->c:Lcom/google/android/gms/googlehelp/common/r;

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/r;->b:Lcom/google/android/gms/googlehelp/common/a;

    const-string v4, "ongoing_chat_request_pool_id"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/r;->b:Lcom/google/android/gms/googlehelp/common/a;

    const-string v4, "ongoing_video_request_pool_id"

    const-string v5, ""

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    if-eqz v0, :cond_5

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/r;->b:Lcom/google/android/gms/googlehelp/common/a;

    const-string v1, "ongoing_session_id"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, v3, Lcom/google/android/gms/googlehelp/common/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->b(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 136
    :cond_1
    :goto_2
    return-void

    :cond_2
    move v0, v2

    .line 135
    goto :goto_0

    :cond_3
    sget-object v4, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v6, v0

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/r;->b:Lcom/google/android/gms/googlehelp/common/a;

    const-string v6, "ongoing_session_last_stopped_ms"

    const-wide/16 v8, 0x0

    invoke-virtual {v0, v6, v8, v9}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;J)J

    move-result-wide v6

    add-long/2addr v4, v6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-ltz v0, :cond_4

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    iget-object v0, v3, Lcom/google/android/gms/googlehelp/common/r;->b:Lcom/google/android/gms/googlehelp/common/a;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "ongoing_session_last_stopped_ms"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "ongoing_session_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_2
.end method

.method public final e()Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 115
    const/16 v0, 0x6f

    if-ne p1, v0, :cond_0

    .line 117
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 118
    const-string v0, "android.speech.extra.RESULTS"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 120
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Ljava/lang/String;)V

    .line 125
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    invoke-super {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onBackPressed()V

    .line 110
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 42
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onCreate(Landroid/os/Bundle;)V

    .line 44
    sget v0, Lcom/google/android/gms/l;->cf:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->setContentView(I)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->im:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {p0, p0, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Lcom/google/android/gms/googlehelp/common/b;Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 52
    return-void
.end method

.method protected onDestroy()V
    .locals 6

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->S()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->S()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->getCacheDir()Ljava/io/File;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    const-string v3, ".bmp"

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/feedback/w;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->getCacheDir()Ljava/io/File;

    move-result-object v4

    const-string v5, ".txt"

    invoke-static {v4, v3, v5}, Lcom/google/android/gms/feedback/w;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onDestroy()V

    .line 68
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2

    .prologue
    .line 87
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const/4 v0, 0x1

    .line 91
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 56
    const-string v0, "GOOGLEHELP_HelpActivity"

    const-string v1, "Cancel all tasks before onSaveInstanceState is called."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->b:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->a(Z)V

    .line 58
    const-string v0, "EXTRA_HELP_CONFIG"

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 59
    invoke-super {p0, p1}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 60
    return-void
.end method

.method public onStop()V
    .locals 6

    .prologue
    .line 96
    invoke-super {p0}, Lcom/google/android/gms/googlehelp/helpactivities/r;->onStop()V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->c:Lcom/google/android/gms/googlehelp/common/r;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/HelpActivity;->c:Lcom/google/android/gms/googlehelp/common/r;

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/common/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/googlehelp/common/r;->b:Lcom/google/android/gms/googlehelp/common/a;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    const-string v2, "ongoing_session_last_stopped_ms"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;J)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    const-string v2, "ongoing_session_id"

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/r;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 102
    :cond_0
    return-void
.end method
