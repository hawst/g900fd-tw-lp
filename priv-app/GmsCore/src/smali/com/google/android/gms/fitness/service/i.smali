.class public final Lcom/google/android/gms/fitness/service/i;
.super Lcom/google/android/gms/fitness/internal/q;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# static fields
.field public static final a:Lcom/google/android/gms/common/api/Status;

.field private static final b:Lcom/google/android/gms/common/api/Status;

.field private static final c:Lcom/google/android/gms/common/api/Status;

.field private static final d:Lcom/google/android/gms/common/api/Status;

.field private static final e:Lcom/google/android/gms/common/api/Status;

.field private static final f:Lcom/google/android/gms/common/api/Status;

.field private static final g:Lcom/google/android/gms/common/api/Status;

.field private static final h:Lcom/google/android/gms/fitness/data/DataSource;

.field private static final i:Lcom/google/android/gms/common/api/Status;


# instance fields
.field private final A:Lcom/google/k/k/a/ai;

.field private final B:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final j:Landroid/content/Context;

.field private final k:Landroid/os/Handler;

.field private final l:Ljava/lang/String;

.field private final m:Lcom/google/android/gms/fitness/l/z;

.field private final n:Lcom/google/android/gms/fitness/sensors/a;

.field private final o:Lcom/google/android/gms/fitness/l/a/a;

.field private final p:Lcom/google/android/gms/fitness/service/av;

.field private final q:Lcom/google/android/gms/fitness/k/a;

.field private final r:Lcom/google/android/gms/fitness/d/d;

.field private final s:Lcom/google/android/gms/fitness/sensors/a/z;

.field private final t:Lcom/google/android/gms/fitness/a/i;

.field private final u:Lcom/google/android/gms/fitness/a/o;

.field private final v:Lcom/google/android/gms/fitness/service/aq;

.field private final w:Landroid/os/RemoteCallbackList;

.field private final x:Lcom/google/android/gms/fitness/service/az;

.field private final y:Lcom/google/android/gms/fitness/settings/i;

.field private final z:Lcom/google/android/gms/fitness/j/b;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1390

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->b:Lcom/google/android/gms/common/api/Status;

    .line 160
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1393

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->c:Lcom/google/android/gms/common/api/Status;

    .line 164
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1392

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->d:Lcom/google/android/gms/common/api/Status;

    .line 168
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1394

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->e:Lcom/google/android/gms/common/api/Status;

    .line 172
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x138f

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->f:Lcom/google/android/gms/common/api/Status;

    .line 176
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1397

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->g:Lcom/google/android/gms/common/api/Status;

    .line 180
    new-instance v0, Lcom/google/android/gms/fitness/data/f;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/f;-><init>()V

    sget-object v1, Lcom/google/android/gms/fitness/data/Application;->a:Lcom/google/android/gms/fitness/data/Application;

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->e:Lcom/google/android/gms/fitness/data/Application;

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->d:Lcom/google/android/gms/fitness/data/DataType;

    iput-object v1, v0, Lcom/google/android/gms/fitness/data/f;->a:Lcom/google/android/gms/fitness/data/DataType;

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/gms/fitness/data/f;->b:I

    const-string v1, "session_activity_segment"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/f;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->h:Lcom/google/android/gms/fitness/data/DataSource;

    .line 189
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, -0x1389

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->i:Lcom/google/android/gms/common/api/Status;

    .line 193
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, -0x1388

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/i;->a:Lcom/google/android/gms/common/api/Status;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/os/Handler;Lcom/google/k/k/a/ai;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/k/a;Lcom/google/android/gms/fitness/l/a/a;Lcom/google/android/gms/fitness/service/av;Lcom/google/android/gms/fitness/d/d;Lcom/google/android/gms/fitness/a/o;Lcom/google/android/gms/fitness/service/aq;Lcom/google/android/gms/fitness/d/h;)V
    .locals 5

    .prologue
    .line 245
    invoke-direct {p0}, Lcom/google/android/gms/fitness/internal/q;-><init>()V

    .line 211
    new-instance v1, Lcom/google/android/gms/fitness/service/ao;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/fitness/service/ao;-><init>(Lcom/google/android/gms/fitness/service/i;B)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->w:Landroid/os/RemoteCallbackList;

    .line 217
    new-instance v1, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 246
    const-string v1, "Creating FitnessService with adapter %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p6, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 247
    const-string v1, "context"

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    .line 248
    const-string v1, "accountName"

    invoke-static {p2, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->l:Ljava/lang/String;

    .line 249
    const-string v1, "handler"

    invoke-static {p3, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/Handler;

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    .line 250
    iput-object p4, p0, Lcom/google/android/gms/fitness/service/i;->A:Lcom/google/k/k/a/ai;

    .line 251
    const-string v1, "fitness store"

    invoke-static {p5, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/l/z;

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    .line 252
    const-string v1, "adapter"

    invoke-static {p6, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/sensors/a;

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->n:Lcom/google/android/gms/fitness/sensors/a;

    .line 253
    const-string v1, "data source manager"

    invoke-static {p7, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/k/a;

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    .line 254
    const-string v1, "auth manager"

    invoke-static {p10, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/d/d;

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    .line 255
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v2, Lcom/google/android/gms/fitness/sensors/a/ag;

    const-string v1, "bluetooth"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/bluetooth/BluetoothManager;

    new-instance v3, Lcom/google/android/gms/fitness/sensors/a/al;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    invoke-direct {v3, v1}, Lcom/google/android/gms/fitness/sensors/a/al;-><init>(Landroid/bluetooth/BluetoothAdapter;)V

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    invoke-direct {v2, v3, v1}, Lcom/google/android/gms/fitness/sensors/a/ag;-><init>(Lcom/google/android/gms/fitness/sensors/a/aa;Landroid/os/Handler;)V

    move-object v1, v2

    :goto_0
    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->s:Lcom/google/android/gms/fitness/sensors/a/z;

    .line 259
    iput-object p8, p0, Lcom/google/android/gms/fitness/service/i;->o:Lcom/google/android/gms/fitness/l/a/a;

    .line 260
    iput-object p9, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    .line 261
    new-instance v1, Lcom/google/android/gms/fitness/a/i;

    invoke-direct {v1, p7, p5}, Lcom/google/android/gms/fitness/a/i;-><init>(Lcom/google/android/gms/fitness/k/a;Lcom/google/android/gms/fitness/l/z;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->t:Lcom/google/android/gms/fitness/a/i;

    .line 262
    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/i;->u:Lcom/google/android/gms/fitness/a/o;

    .line 263
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/i;->v:Lcom/google/android/gms/fitness/service/aq;

    .line 264
    invoke-static {p1}, Lcom/google/android/gms/fitness/service/az;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/service/az;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->x:Lcom/google/android/gms/fitness/service/az;

    .line 266
    new-instance v1, Lcom/google/android/gms/fitness/settings/i;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    move-object/from16 v0, p13

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/fitness/settings/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/fitness/d/h;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->y:Lcom/google/android/gms/fitness/settings/i;

    .line 267
    new-instance v1, Lcom/google/android/gms/fitness/j/b;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->n:Lcom/google/android/gms/fitness/sensors/a;

    iget-object v4, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/fitness/j/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/d/d;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/service/i;->z:Lcom/google/android/gms/fitness/j/b;

    .line 268
    return-void

    .line 255
    :cond_0
    new-instance v1, Lcom/google/android/gms/fitness/sensors/a/ao;

    invoke-direct {v1}, Lcom/google/android/gms/fitness/sensors/a/ao;-><init>()V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/c/c;)I
    .locals 5

    .prologue
    .line 691
    const/4 v0, 0x0

    .line 692
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/c/c;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/c;

    .line 693
    iget-object v3, v0, Lcom/google/android/gms/fitness/c/c;->c:Lcom/google/android/gms/fitness/c/e;

    sget-object v4, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    if-ne v3, v4, :cond_2

    .line 694
    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/service/av;->a(Lcom/google/android/gms/fitness/c/c;)Lcom/google/k/k/a/af;

    move-result-object v0

    .line 695
    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/util/concurrent/Future;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 696
    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 699
    goto :goto_0

    .line 700
    :cond_1
    return v1

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/c/c;)I
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/c/c;)I

    move-result v0

    return v0
.end method

.method private a(Ljava/lang/String;Ljava/util/List;JJ)I
    .locals 9

    .prologue
    .line 1532
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object v1, p1

    move-wide v4, p3

    move-wide v6, p5

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Session;->a()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/google/android/gms/fitness/data/Application;

    const/4 v0, 0x0

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/fitness/data/Application;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/Session;->a(Lcom/google/android/gms/fitness/data/Application;)Lcom/google/android/gms/fitness/data/Session;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1535
    :cond_2
    const/4 v0, 0x0

    .line 1536
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    .line 1537
    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    const/4 v4, 0x1

    invoke-interface {v3, v0, v4}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/Session;Z)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1538
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/service/i;->c(Lcom/google/android/gms/fitness/data/Session;)V

    .line 1539
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1541
    :cond_3
    const-string v3, "Failed to delete app session: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 1545
    :cond_4
    const-string v0, "Deleted %d sessions"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1546
    return v1
.end method

.method private a(Ljava/util/List;JJ)I
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 1519
    .line 1520
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p2, p3}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v2

    .line 1521
    sget-object v0, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p4, p5}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v4

    .line 1522
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v0, v8

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/gms/fitness/data/DataSource;

    .line 1523
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface/range {v1 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(JJLcom/google/android/gms/fitness/data/DataSource;Z)I

    move-result v1

    add-int/2addr v0, v1

    .line 1524
    goto :goto_0

    .line 1526
    :cond_0
    const-string v1, "Deleted %d data points"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1527
    return v0
.end method

.method private a(Lcom/google/android/gms/fitness/request/SessionStartRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 1020
    new-instance v0, Lcom/google/android/gms/fitness/data/Application;

    const/4 v1, 0x0

    invoke-direct {v0, p2, v1}, Lcom/google/android/gms/fitness/data/Application;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1021
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionStartRequest;->a()Lcom/google/android/gms/fitness/data/Session;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/Session;->a(Lcom/google/android/gms/fitness/data/Application;)Lcom/google/android/gms/fitness/data/Session;

    move-result-object v8

    .line 1025
    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1026
    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1035
    :goto_0
    invoke-static {v0, v8}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Session;

    move-result-object v0

    .line 1036
    if-eqz v0, :cond_2

    .line 1037
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Session;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1039
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    new-instance v2, Lcom/google/android/gms/fitness/data/r;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/data/r;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/fitness/data/r;->a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/r;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/google/android/gms/fitness/data/r;->a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/r;->a()Lcom/google/android/gms/fitness/data/Session;

    move-result-object v0

    invoke-interface {v1, v0, v9}, Lcom/google/android/gms/fitness/l/z;->c(Lcom/google/android/gms/fitness/data/Session;Z)V

    .line 1045
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    .line 1069
    :goto_1
    return-object v0

    .line 1028
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v3

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v1}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 1048
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1391

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    goto :goto_1

    .line 1052
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, v8, v9}, Lcom/google/android/gms/fitness/l/z;->b(Lcom/google/android/gms/fitness/data/Session;Z)V

    .line 1055
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1056
    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1057
    const-string v0, "vnd.google.fitness.session"

    invoke-static {v8, v1, v0}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    .line 1058
    const-string v0, "vnd.google.fitness.start_time"

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v2}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1060
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/service/av;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 1062
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 1064
    :catch_0
    move-exception v3

    const-string v3, "Found dead intent listener %s, removing."

    new-array v4, v9, [Ljava/lang/Object;

    aput-object v0, v4, v10

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1065
    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    invoke-virtual {v3, p2, v0}, Lcom/google/android/gms/fitness/service/av;->a(Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_2

    .line 1069
    :cond_3
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataDeleteRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 16

    .prologue
    .line 149
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    const/4 v2, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->d()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->a()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    new-instance v2, Ljava/util/HashSet;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->b()Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    new-instance v3, Ljava/util/HashSet;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->a()Ljava/util/List;

    move-result-object v8

    invoke-direct {v3, v8}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;

    move-result-object v3

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;JJ)I

    move-result v2

    :cond_1
    const/4 v3, 0x0

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->e()Z

    move-result v8

    if-nez v8, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->c()Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_3

    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/DataDeleteRequest;->c()Ljava/util/List;

    move-result-object v11

    move-object/from16 v9, p0

    move-object/from16 v10, p2

    move-wide v12, v4

    move-wide v14, v6

    invoke-direct/range {v9 .. v15}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Ljava/util/List;JJ)I

    move-result v3

    :cond_3
    const-string v4, "Deleted %d data points and %d sessions"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    const/4 v2, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    return-object v2
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataInsertRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 149
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataInsertRequest;->a()Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    new-array v2, v5, [Lcom/google/android/gms/fitness/data/DataSet;

    aput-object v0, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/d/e;->b:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v1, p2, v2, v3}, Lcom/google/android/gms/fitness/d/d;->b(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "App %s does not have access to access data type %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v4

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    aput-object v0, v2, v5

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    new-array v2, v5, [Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/d/e;->b:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v1, p2, v2, v3}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v1

    iget-boolean v2, v1, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v1, v1, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v1, "Failed to get OAuth token and un-recoverable exception was thrown. Proceeding with caution for now."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    invoke-static {p2, v1}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v1

    if-nez v1, :cond_3

    sget-object v0, Lcom/google/android/gms/fitness/service/i;->g:Lcom/google/android/gms/common/api/Status;

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-interface {v2, p2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v2

    invoke-interface {v1, v0, v2, v5}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/Application;Z)V

    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SessionInsertRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 11

    .prologue
    const/4 v8, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v10, 0x1

    .line 149
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSet;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    sget-object v2, Lcom/google/android/gms/fitness/d/e;->b:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v0, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    :goto_2
    return-object v0

    :cond_2
    const-string v0, "Failed to get OAuth token and un-recoverable exception was thrown. Proceeding with caution for now."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->b()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/fitness/d/e;->b:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/gms/fitness/d/d;->b(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->c()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/fitness/d/e;->b:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/gms/fitness/d/d;->c(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v0

    if-nez v0, :cond_5

    :cond_4
    const-string v0, "App %s does not have access to data types in request %s"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p2, v1, v3

    aput-object p1, v1, v10

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    goto :goto_2

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->c()Ljava/util/List;

    move-result-object v1

    invoke-static {p2, v0, v1}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string v0, "DataSource in request: {%s} is inconsistent with package name: %s"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    aput-object p2, v1, v10

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/service/i;->g:Lcom/google/android/gms/common/api/Status;

    goto :goto_2

    :cond_6
    new-instance v0, Lcom/google/android/gms/fitness/data/Application;

    invoke-direct {v0, p2, v8}, Lcom/google/android/gms/fitness/data/Application;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->a()Lcom/google/android/gms/fitness/data/Session;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/Session;->a(Lcom/google/android/gms/fitness/data/Application;)Lcom/google/android/gms/fitness/data/Session;

    move-result-object v9

    invoke-virtual {v9}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {v9}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    :goto_3
    invoke-static {v0, v9}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Session;

    move-result-object v1

    if-nez v1, :cond_8

    move-object v1, v8

    :goto_4
    if-nez v1, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, v9, v10}, Lcom/google/android/gms/fitness/l/z;->b(Lcom/google/android/gms/fitness/data/Session;Z)V

    invoke-direct {p0, v9}, Lcom/google/android/gms/fitness/service/i;->b(Lcom/google/android/gms/fitness/data/Session;)V

    :goto_5
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_6
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSet;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-interface {v3, p2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/Application;)V

    goto :goto_6

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-virtual {v9}, Lcom/google/android/gms/fitness/data/Session;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Lcom/google/android/gms/fitness/data/Session;->c()Ljava/lang/String;

    move-result-object v3

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v9, v1}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v9, v1}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    goto :goto_3

    :cond_8
    new-instance v2, Lcom/google/android/gms/fitness/data/r;

    invoke-direct {v2}, Lcom/google/android/gms/fitness/data/r;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/fitness/data/r;->a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/r;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/google/android/gms/fitness/data/r;->a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/r;->a()Lcom/google/android/gms/fitness/data/Session;

    move-result-object v1

    goto :goto_4

    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v2, v1, v10}, Lcom/google/android/gms/fitness/l/z;->c(Lcom/google/android/gms/fitness/data/Session;Z)V

    invoke-static {v0, v9}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Session;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/service/i;->c(Lcom/google/android/gms/fitness/data/Session;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/service/i;->b(Lcom/google/android/gms/fitness/data/Session;)V

    goto :goto_5

    :cond_a
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionInsertRequest;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-interface {v3, p2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/Application;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/fitness/l/z;->b(Lcom/google/android/gms/fitness/data/DataPoint;Lcom/google/android/gms/fitness/data/Application;)V

    goto :goto_7

    :cond_b
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SessionRegistrationRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "https://www.googleapis.com/auth/fitness.activity.read"

    aput-object v2, v1, v3

    invoke-interface {v0, p2, v1}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v0, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Failed to get OAuth token and un-recoverable exception was thrown. Proceeding with caution for now."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionRegistrationRequest;->a()Landroid/app/PendingIntent;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v2, v0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    new-instance v3, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    invoke-virtual {v2, p2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SessionStartRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/request/SessionStartRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SubscribeRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 149
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SubscribeRequest;->a()Lcom/google/android/gms/fitness/data/Subscription;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Subscription;->f()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    new-array v5, v2, [Lcom/google/android/gms/fitness/data/DataType;

    aput-object v1, v5, v3

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v4, p2, v5, v6}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v4

    iget-boolean v5, v4, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v5, :cond_1

    iget-object v5, v4, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v1, v4, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v4, "Failed to get OAuth token and un-recoverable exception was thrown. Proceeding with caution for now."

    new-array v5, v3, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v4, p2, v5, v6}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string v0, "App: %s does not have access to data type: %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object p2, v4, v3

    aput-object v1, v4, v2

    invoke-static {v0, v4}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    if-nez v1, :cond_4

    move-object v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, p2}, Lcom/google/android/gms/fitness/l/z;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/c;

    iget-object v0, v0, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Subscription;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v5

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    if-nez v0, :cond_8

    new-instance v4, Lcom/google/android/gms/fitness/c/c;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SubscribeRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    sget-object v0, Lcom/google/android/gms/fitness/c/e;->b:Lcom/google/android/gms/fitness/c/e;

    :goto_3
    invoke-direct {v4, p2, v1, v0}, Lcom/google/android/gms/fitness/c/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/fitness/data/Subscription;Lcom/google/android/gms/fitness/c/e;)V

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SubscribeRequest;->b()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0, v4}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/c/c;)I

    move-result v0

    if-nez v0, :cond_7

    const-string v0, "No data sources found for %s.  Subscription is added, and recording will start once sources become available."

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->e()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, v4}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/c/c;)V

    sget-object v0, Lcom/google/android/gms/fitness/service/i;->a:Lcom/google/android/gms/common/api/Status;

    goto/16 :goto_0

    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v4, v1, p2}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/Subscription;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/Subscription;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    :cond_5
    move v0, v3

    goto :goto_2

    :cond_6
    sget-object v0, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, v4}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/c/c;)V

    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto/16 :goto_0

    :cond_8
    const-string v0, "Already subscribed.  Subscribe request is a no-op"

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/service/i;->i:Lcom/google/android/gms/common/api/Status;

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/UnsubscribeRequest;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;
    .locals 13

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, p2}, Lcom/google/android/gms/fitness/l/z;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/c;

    iget-object v1, v0, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/UnsubscribeRequest;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/UnsubscribeRequest;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    invoke-virtual {v2, v3, p2}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/fitness/data/DataSource;->equals(Ljava/lang/Object;)Z

    move-result v1

    :goto_1
    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/c/c;)Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lcom/google/android/gms/fitness/c/c;

    iget-object v1, v2, Lcom/google/android/gms/fitness/c/c;->c:Lcom/google/android/gms/fitness/c/e;

    sget-object v3, Lcom/google/android/gms/fitness/c/e;->a:Lcom/google/android/gms/fitness/c/e;

    if-ne v1, v3, :cond_1

    iget-object v6, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    iget-object v7, v2, Lcom/google/android/gms/fitness/c/c;->a:Ljava/lang/String;

    iget-object v1, v6, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, v7}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Ljava/util/List;

    if-nez v3, :cond_4

    const-string v1, "Couldn\'t find any registrations for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    :goto_3
    if-nez v1, :cond_1

    const-string v1, "Failed to unregister recording listener for %s. Will proceed to remove subscription anyway."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/UnsubscribeRequest;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/UnsubscribeRequest;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Subscription;->f()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    goto :goto_1

    :cond_4
    iget-object v8, v2, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_5
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/sensors/e/g;

    iget-object v10, v1, Lcom/google/android/gms/fitness/sensors/e/g;->d:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v11

    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/Subscription;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v10

    invoke-virtual {v8}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v2, v6, Lcom/google/android/gms/fitness/service/av;->c:Lcom/google/android/gms/fitness/sensors/a;

    iget-object v8, v1, Lcom/google/android/gms/fitness/sensors/e/g;->a:Lcom/google/android/gms/fitness/data/l;

    invoke-interface {v2, v8}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/data/l;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v3, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v1, v6, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v7, v2}, Ljava/util/concurrent/ConcurrentMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z

    const/4 v1, 0x1

    goto :goto_3

    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    :cond_7
    const-string v1, "Recording Listener not found for the specified subscription: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v3, v6

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v1, 0x0

    goto :goto_3

    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/l/z;->b(Lcom/google/android/gms/fitness/c/c;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "Failed to remove subscription. Not found?: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_9
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/DataSet;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 941
    sget-object v0, Lcom/google/android/gms/fitness/service/i;->h:Lcom/google/android/gms/fitness/data/DataSource;

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    .line 942
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->f()I

    move-result v1

    .line 943
    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 944
    const-string v1, "Activity type is unknown. No activity segment will be inserted."

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 957
    :goto_0
    return-object v0

    .line 948
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->a()Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    .line 949
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v2}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v4}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 953
    const/4 v2, 0x1

    new-array v2, v2, [I

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->f()I

    move-result v3

    aput v3, v2, v7

    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a([I)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 954
    sget-object v2, Lcom/google/android/gms/fitness/service/i;->h:Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/Session;->g()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataSource;->a(Lcom/google/android/gms/fitness/data/Application;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->b(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    .line 956
    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Session;
    .locals 3

    .prologue
    .line 1265
    const-string v0, "Existing sessions: "

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1267
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    .line 1268
    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/data/Session;->a(Lcom/google/android/gms/fitness/data/Session;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1272
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/l/z;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataSourcesRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/DataSourcesResult;
    .locals 5

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataSourcesRequest;->a()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v0, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/DataSourcesResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/DataSourcesResult;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataSourcesRequest;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->n:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v3, v0}, Lcom/google/android/gms/fitness/sensors/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/request/DataSourcesRequest;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataSourcesRequest;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/k/a;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/request/DataSourcesRequest;->a(Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    sget-object v2, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v0, p2, v1, v2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/Set;Lcom/google/android/gms/fitness/d/e;)Ljava/util/List;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/fitness/result/DataSourcesResult;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/result/DataSourcesResult;-><init>(Ljava/util/List;Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/result/DataTypeResult;
    .locals 3

    .prologue
    .line 1639
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataType;)J
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ab; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_1

    .line 1646
    new-instance v0, Lcom/google/android/gms/fitness/result/DataTypeResult;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/fitness/result/DataTypeResult;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/fitness/data/DataType;)V

    :goto_0
    return-object v0

    .line 1640
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 1641
    new-instance v0, Lcom/google/android/gms/fitness/result/DataTypeResult;

    sget-object v2, Lcom/google/android/gms/fitness/d/d;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/l/ab;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/fitness/result/DataTypeResult;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/fitness/data/DataType;)V

    goto :goto_0

    .line 1644
    :catch_1
    move-exception v0

    sget-object v0, Lcom/google/android/gms/fitness/service/i;->b:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/DataTypeResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/DataTypeResult;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataTypeCreateRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/DataTypeResult;
    .locals 5

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataTypeCreateRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, p2, v1}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/DataTypeResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/DataTypeResult;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataTypeCreateRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataTypeCreateRequest;->b()Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/data/DataType;-><init>(Ljava/lang/String;Ljava/util/List;)V

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataTypeCreateRequest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/fitness/d/d;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "Inconsistent shareable data type specified. Expected %s, Actual %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v1, 0x1

    aput-object v0, v3, v1

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/DataTypeResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/DataTypeResult;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/result/DataTypeResult;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataTypeReadRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/DataTypeResult;
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 149
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataTypeReadRequest;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    sget-object v2, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v1, p2, v0, v2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "App %s does not have access to data type %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v4

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/DataTypeResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/DataTypeResult;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/gms/fitness/result/DataTypeResult;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/fitness/result/DataTypeResult;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/fitness/data/DataType;)V

    goto :goto_0

    :cond_1
    const-string v1, "No datatype %s found. Checking if this is a shareable one."

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/d/d;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v1

    if-nez v1, :cond_2

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->d:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/DataTypeResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/DataTypeResult;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v2, "Adding shareable data type: %s"

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, v1}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/result/DataTypeResult;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/ListSubscriptionsRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/ListSubscriptionsResult;
    .locals 4

    .prologue
    .line 149
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, p2}, Lcom/google/android/gms/fitness/l/z;->e(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/c/c;

    iget-object v0, v0, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/request/ListSubscriptionsRequest;->a(Lcom/google/android/gms/fitness/data/Subscription;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/gms/fitness/result/ListSubscriptionsResult;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/result/ListSubscriptionsResult;-><init>(Ljava/util/List;Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SessionReadRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/SessionReadResult;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 149
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->c()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    sget-object v2, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v1, p2, v0, v2}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v0, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/SessionReadResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/SessionReadResult;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "Failed to get OAuth token and un-recoverable exception was thrown. Proceeding with caution for now."

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    sget-object v1, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v0, p2, v8, v1}, Lcom/google/android/gms/fitness/d/d;->d(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "App %s does not have access to dataSources: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v3

    const/4 v2, 0x1

    aput-object v8, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/android/gms/fitness/result/SessionReadResult;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/SessionReadResult;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v1, 0x0

    :goto_1
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->b()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v4}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v6}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->a(Lcom/google/android/gms/fitness/data/Session;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v9, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionReadRequest;->g()Z

    move-result v0

    invoke-direct {p0, v8, v9, v0}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;Ljava/util/List;Z)Ljava/util/List;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/fitness/result/SessionReadResult;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v9, v1, v2}, Lcom/google/android/gms/fitness/result/SessionReadResult;-><init>(Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :cond_5
    move-object v1, p2

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/fitness/request/SessionStopRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/SessionStopResult;
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 1119
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionStopRequest;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/SessionStopRequest;->b()Ljava/lang/String;

    move-result-object v3

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    const-wide/16 v4, -0x1

    const-wide/16 v6, 0x0

    move-object v1, p2

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Session;->a()Z

    move-result v2

    const-string v3, "Should return only active sessions: %s"

    new-array v4, v11, [Ljava/lang/Object;

    aput-object v0, v4, v10

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1121
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1123
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1124
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Session;

    .line 1125
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Session;->a()Z

    move-result v5

    const-string v6, "Specified session is not active: %s"

    new-array v7, v11, [Ljava/lang/Object;

    aput-object v0, v7, v10

    invoke-static {v5, v6, v7}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    new-instance v5, Lcom/google/android/gms/fitness/data/r;

    invoke-direct {v5}, Lcom/google/android/gms/fitness/data/r;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/android/gms/fitness/data/r;->a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/r;

    move-result-object v0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v5}, Lcom/google/android/gms/fitness/data/r;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/r;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/r;->a()Lcom/google/android/gms/fitness/data/Session;

    move-result-object v0

    .line 1126
    iget-object v5, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v5, v0, v11}, Lcom/google/android/gms/fitness/l/z;->c(Lcom/google/android/gms/fitness/data/Session;Z)V

    .line 1127
    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/service/i;->b(Lcom/google/android/gms/fitness/data/Session;)V

    .line 1128
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1131
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 1132
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Session;->e()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/fitness/e;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 1133
    const-string v6, "vnd.google.fitness.session"

    invoke-static {v0, v5, v6}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    .line 1134
    const-string v6, "vnd.google.fitness.start_time"

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    invoke-virtual {v5, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1135
    const-string v6, "vnd.google.fitness.end_time"

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    invoke-virtual {v5, v6, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1137
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/service/av;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 1139
    :try_start_0
    iget-object v7, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8, v5}, Landroid/app/PendingIntent;->send(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1141
    :catch_0
    move-exception v7

    const-string v7, "Found dead intent listener %s, removing."

    new-array v8, v11, [Ljava/lang/Object;

    aput-object v0, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1142
    iget-object v7, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    invoke-virtual {v7, p2, v0}, Lcom/google/android/gms/fitness/service/av;->a(Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_1

    .line 1148
    :cond_2
    new-instance v0, Lcom/google/android/gms/fitness/result/SessionStopResult;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/fitness/result/SessionStopResult;-><init>(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SessionStopRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/SessionStopResult;
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/request/SessionStopRequest;Ljava/lang/String;)Lcom/google/android/gms/fitness/result/SessionStopResult;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/a;)Lcom/google/k/k/a/af;
    .locals 24

    .prologue
    .line 462
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    .line 463
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    .line 466
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/gms/fitness/data/DataType;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    sget-object v6, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    move-object/from16 v0, p2

    invoke-interface {v4, v0, v5, v6}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v4

    .line 468
    iget-boolean v5, v4, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v5, :cond_2

    .line 469
    iget-object v5, v4, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v5, :cond_1

    .line 470
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v3, v4, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    .line 541
    :goto_1
    return-object v2

    .line 463
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    goto :goto_0

    .line 473
    :cond_1
    const-string v4, "Failed to get OAuth token and un-recoverable exception was thrown. Proceeding with caution for now."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 479
    :cond_2
    if-nez v3, :cond_3

    .line 480
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    .line 481
    if-nez v3, :cond_c

    .line 482
    const-string v2, "No live data sources available for %s. Returning success.  Will start recording once data source is live"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 485
    sget-object v2, Lcom/google/android/gms/fitness/service/i;->a:Lcom/google/android/gms/common/api/Status;

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    goto :goto_1

    .line 489
    :cond_3
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    move-object/from16 v0, p2

    invoke-virtual {v4, v3, v0}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource;
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    move-object/from16 v17, v3

    .line 495
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    move-object/from16 v0, p2

    invoke-interface {v3, v0, v4, v5}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 497
    const-string v2, "App: %s does not have access to data source: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p2, v3, v4

    const/4 v4, 0x1

    aput-object v17, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 499
    sget-object v2, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    goto :goto_1

    .line 491
    :catch_0
    move-exception v2

    sget-object v2, Lcom/google/android/gms/fitness/service/i;->b:Lcom/google/android/gms/common/api/Status;

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    goto :goto_1

    .line 502
    :cond_4
    if-nez p3, :cond_6

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v14

    .line 503
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v3

    if-eqz v3, :cond_5

    .line 505
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/i;->w:Landroid/os/RemoteCallbackList;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 508
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->f()J

    move-result-wide v6

    .line 509
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->e()J

    move-result-wide v4

    .line 510
    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-gez v3, :cond_b

    .line 512
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/i;->x:Lcom/google/android/gms/fitness/service/az;

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v2}, Lcom/google/android/gms/fitness/service/az;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v4

    .line 515
    const-wide/16 v2, 0x2

    div-long v2, v4, v2

    move-wide/from16 v18, v2

    move-wide/from16 v20, v4

    .line 518
    :goto_4
    const-wide/16 v2, -0x1

    cmp-long v2, v20, v2

    if-nez v2, :cond_7

    .line 519
    const-string v2, "The requested data source [%s] is disabled. Registration request will succeed, but no events will be delivered."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual/range {v17 .. v17}, Lcom/google/android/gms/fitness/data/DataSource;->k()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 522
    sget-object v2, Lcom/google/android/gms/fitness/service/i;->a:Lcom/google/android/gms/common/api/Status;

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    goto/16 :goto_1

    .line 502
    :cond_6
    invoke-static/range {p3 .. p3}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    goto :goto_3

    .line 525
    :cond_7
    const-wide v15, 0x7fffffffffffffffL

    .line 526
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->k()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_8

    .line 527
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->k()J

    move-result-wide v15

    .line 530
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/i;->n:Lcom/google/android/gms/fitness/sensors/a;

    move-object/from16 v22, v0

    new-instance v23, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/i;->z:Lcom/google/android/gms/fitness/j/b;

    new-instance v6, Lcom/google/android/gms/fitness/j/e;

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-direct {v6, v3, v0, v2}, Lcom/google/android/gms/fitness/j/e;-><init>(Lcom/google/android/gms/fitness/j/b;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;B)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v2

    if-eqz v2, :cond_a

    new-instance v5, Lcom/google/android/gms/fitness/j/a;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    if-nez v2, :cond_9

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataType;->c()Ljava/lang/String;

    move-result-object v2

    :goto_5
    move-object/from16 v0, p2

    invoke-direct {v5, v0, v4, v2}, Lcom/google/android/gms/fitness/j/a;-><init>(Ljava/lang/String;Lcom/google/android/gms/fitness/data/l;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/fitness/j/c;

    iget-object v4, v3, Lcom/google/android/gms/fitness/j/b;->a:Landroid/content/Context;

    iget-object v8, v3, Lcom/google/android/gms/fitness/j/b;->d:Lcom/google/android/gms/fitness/d/d;

    iget-object v9, v3, Lcom/google/android/gms/fitness/j/b;->e:Landroid/os/Handler;

    move-object/from16 v7, p3

    move-object v10, v5

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/fitness/j/c;-><init>(Lcom/google/android/gms/fitness/j/b;Landroid/content/Context;Lcom/google/android/gms/fitness/j/a;Lcom/google/android/gms/fitness/j/e;Lcom/google/android/gms/fitness/internal/a;Lcom/google/android/gms/fitness/d/d;Landroid/os/Handler;Lcom/google/android/gms/fitness/j/a;)V

    iget-object v3, v3, Lcom/google/android/gms/fitness/j/b;->b:Lcom/google/android/gms/fitness/j/h;

    invoke-virtual {v3, v5, v2}, Lcom/google/android/gms/fitness/j/h;->a(Ljava/lang/Object;Lcom/google/android/gms/fitness/data/l;)V

    move-object v5, v2

    :goto_6
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->g()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->j()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->h()Ljava/util/List;

    move-result-object v13

    move-object/from16 v3, v23

    move-object/from16 v4, v17

    move-wide/from16 v6, v20

    move-wide/from16 v8, v18

    invoke-direct/range {v3 .. v16}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;-><init>(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/l;JJJILjava/util/List;Ljava/util/List;J)V

    invoke-interface/range {v22 .. v23}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    move-result-object v2

    .line 541
    new-instance v3, Lcom/google/android/gms/fitness/service/aj;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/fitness/service/aj;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)V

    invoke-static {v2, v3}, Lcom/google/k/k/a/n;->b(Lcom/google/k/k/a/af;Lcom/google/k/a/u;)Lcom/google/k/k/a/af;

    move-result-object v2

    goto/16 :goto_1

    .line 530
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataSource;->k()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    :cond_a
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->d()Landroid/app/PendingIntent;

    move-result-object v5

    new-instance v2, Lcom/google/android/gms/fitness/j/d;

    iget-object v4, v3, Lcom/google/android/gms/fitness/j/b;->a:Landroid/content/Context;

    iget-object v8, v3, Lcom/google/android/gms/fitness/j/b;->d:Lcom/google/android/gms/fitness/d/d;

    iget-object v9, v3, Lcom/google/android/gms/fitness/j/b;->e:Landroid/os/Handler;

    move-object/from16 v7, p3

    move-object v10, v5

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/fitness/j/d;-><init>(Lcom/google/android/gms/fitness/j/b;Landroid/content/Context;Landroid/app/PendingIntent;Lcom/google/android/gms/fitness/j/e;Lcom/google/android/gms/fitness/internal/a;Lcom/google/android/gms/fitness/d/d;Landroid/os/Handler;Landroid/app/PendingIntent;)V

    iget-object v3, v3, Lcom/google/android/gms/fitness/j/b;->c:Lcom/google/android/gms/fitness/j/h;

    invoke-virtual {v3, v5, v2}, Lcom/google/android/gms/fitness/j/h;->a(Ljava/lang/Object;Lcom/google/android/gms/fitness/data/l;)V

    move-object v5, v2

    goto :goto_6

    :cond_b
    move-wide/from16 v18, v4

    move-wide/from16 v20, v6

    goto/16 :goto_4

    :cond_c
    move-object/from16 v17, v3

    goto/16 :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/ClaimBleDeviceRequest;)Lcom/google/k/k/a/af;
    .locals 2

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/ClaimBleDeviceRequest;->b()Lcom/google/android/gms/fitness/data/BleDevice;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->s:Lcom/google/android/gms/fitness/sensors/a/z;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/ClaimBleDeviceRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/sensors/a/z;->a(Ljava/lang/String;)Lcom/google/k/k/a/af;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/a;)Lcom/google/k/k/a/af;
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/a;)Lcom/google/k/k/a/af;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataReadRequest;Ljava/lang/String;)Ljava/util/List;
    .locals 13

    .prologue
    .line 149
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->b()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->a()Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    sget-object v3, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v2, p2, v1, v3}, Lcom/google/android/gms/fitness/d/d;->d(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v0, "App %s does not have access to data sources %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    sget-object v0, Lcom/google/android/gms/fitness/d/d;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0, p1}, Lcom/google/android/gms/fitness/result/DataReadResult;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/fitness/request/DataReadRequest;)Lcom/google/android/gms/fitness/result/DataReadResult;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    sget-object v3, Lcom/google/android/gms/fitness/d/e;->a:Lcom/google/android/gms/fitness/d/e;

    invoke-interface {v2, p2, v0, v3}, Lcom/google/android/gms/fitness/d/d;->a(Ljava/lang/String;Ljava/util/List;Lcom/google/android/gms/fitness/d/e;)Lcom/google/android/gms/fitness/d/f;

    move-result-object v0

    iget-boolean v2, v0, Lcom/google/android/gms/fitness/d/f;->b:Z

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v0, v0, Lcom/google/android/gms/fitness/d/f;->a:Landroid/content/Intent;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/d/d;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/fitness/result/DataReadResult;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/fitness/request/DataReadRequest;)Lcom/google/android/gms/fitness/result/DataReadResult;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->d()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->c()Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, v0, v2, p2}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->w:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v2, "Unsupported data type specified for aggregation: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v0, 0x0

    :goto_1
    if-nez v0, :cond_4

    sget-object v0, Lcom/google/android/gms/fitness/service/i;->e:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0, p1}, Lcom/google/android/gms/fitness/result/DataReadResult;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/fitness/request/DataReadRequest;)Lcom/google/android/gms/fitness/result/DataReadResult;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->o:Lcom/google/android/gms/fitness/l/a/a;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/l/a/a;->a()V

    :cond_5
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/request/DataReadRequest;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/request/DataReadRequest;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->g()I

    move-result v0

    if-nez v0, :cond_7

    const/4 v6, -0x1

    :goto_2
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->h()Z

    move-result v7

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;JJIZ)Ljava/util/List;

    move-result-object v12

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->t:Lcom/google/android/gms/fitness/a/i;

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->c(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->e()I

    move-result v8

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->f()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v9

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->h()Z

    move-result v10

    move-object v1, v11

    invoke-virtual/range {v0 .. v10}, Lcom/google/android/gms/fitness/a/i;->a(Ljava/util/List;JJJILcom/google/android/gms/fitness/data/DataSource;Z)Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/service/ay;

    invoke-direct {v1, v12, v0}, Lcom/google/android/gms/fitness/service/ay;-><init>(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, v1, Lcom/google/android/gms/fitness/service/ay;->a:Ljava/util/List;

    iget-object v2, v1, Lcom/google/android/gms/fitness/service/ay;->d:Ljava/util/List;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/fitness/service/ay;->a(Ljava/util/List;Ljava/util/List;)V

    iget-object v0, v1, Lcom/google/android/gms/fitness/service/ay;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/Bucket;

    iget v3, v1, Lcom/google/android/gms/fitness/service/ay;->g:I

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->f()I

    move-result v4

    add-int/2addr v3, v4

    const v4, 0x186a0

    if-le v3, v4, :cond_6

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/service/ay;->a()V

    :cond_6
    iput-object v0, v1, Lcom/google/android/gms/fitness/service/ay;->e:Lcom/google/android/gms/fitness/data/Bucket;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Bucket;->c()Ljava/util/List;

    move-result-object v0

    iget-object v3, v1, Lcom/google/android/gms/fitness/service/ay;->f:Ljava/util/List;

    invoke-virtual {v1, v0, v3}, Lcom/google/android/gms/fitness/service/ay;->a(Ljava/util/List;Ljava/util/List;)V

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/service/ay;->b()V

    goto :goto_3

    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/request/DataReadRequest;->g()I

    move-result v6

    goto :goto_2

    :cond_8
    invoke-virtual {v1}, Lcom/google/android/gms/fitness/service/ay;->a()V

    iget-object v0, v1, Lcom/google/android/gms/fitness/service/ay;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/result/DataReadResult;

    iget-object v3, v1, Lcom/google/android/gms/fitness/service/ay;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/fitness/result/DataReadResult;->a(I)V

    goto :goto_4

    :cond_9
    const-string v0, "Returning %d batches"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, v1, Lcom/google/android/gms/fitness/service/ay;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    iget-object v0, v1, Lcom/google/android/gms/fitness/service/ay;->c:Ljava/util/List;

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/util/List;
    .locals 9

    .prologue
    const-wide/16 v4, -0x1

    const/4 v8, 0x1

    .line 1079
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1080
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    const/4 v2, 0x0

    move-object v1, p1

    move-object v3, p2

    move-wide v6, v4

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)Ljava/util/List;

    move-result-object v1

    .line 1088
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, v8, :cond_0

    move v0, v8

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 1089
    return-object v1

    .line 1088
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;)Ljava/util/List;
    .locals 4

    .prologue
    .line 1581
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/l/z;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 1583
    invoke-interface {p2}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p3}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1597
    :goto_0
    return-object v0

    .line 1587
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1588
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 1589
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v3

    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1590
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1593
    :cond_2
    invoke-interface {p3, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1594
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 1597
    goto :goto_0
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 3

    .prologue
    .line 1353
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1354
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 1355
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1357
    :cond_0
    return-object v1
.end method

.method private a(Ljava/util/List;JJIZ)Ljava/util/List;
    .locals 10

    .prologue
    .line 1829
    new-instance v8, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1830
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/data/DataSource;

    .line 1831
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    move-wide v2, p2

    move-wide v4, p4

    move/from16 v6, p6

    move/from16 v7, p7

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/data/DataSource;JJIZ)Ljava/util/List;

    move-result-object v0

    .line 1833
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1834
    invoke-interface {v8, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1836
    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/fitness/data/DataSet;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1839
    :cond_1
    return-object v8
.end method

.method private a(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)Ljava/util/List;
    .locals 4

    .prologue
    .line 1810
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    .line 1812
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1813
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 1814
    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v3, v0, p3}, Lcom/google/android/gms/fitness/k/a;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1816
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataType;

    .line 1817
    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/fitness/k/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1819
    :cond_1
    return-object v1
.end method

.method private a(Ljava/util/List;Ljava/util/List;Z)Ljava/util/List;
    .locals 11

    .prologue
    .line 1363
    new-instance v9, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-direct {v9, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1369
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/android/gms/fitness/data/Session;

    .line 1370
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v0}, Lcom/google/android/gms/fitness/data/Session;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v8, v0}, Lcom/google/android/gms/fitness/data/Session;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/util/List;JJIZ)Ljava/util/List;

    move-result-object v0

    .line 1376
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSet;

    .line 1377
    new-instance v2, Lcom/google/android/gms/fitness/data/SessionDataSet;

    invoke-direct {v2, v8, v0}, Lcom/google/android/gms/fitness/data/SessionDataSet;-><init>(Lcom/google/android/gms/fitness/data/Session;Lcom/google/android/gms/fitness/data/DataSet;)V

    .line 1378
    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1381
    :cond_1
    return-object v9
.end method

.method private static a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V
    .locals 1

    .prologue
    .line 2209
    sget-object v0, Lcom/google/android/gms/fitness/service/i;->f:Lcom/google/android/gms/common/api/Status;

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/common/api/Status;)V

    .line 2210
    return-void
.end method

.method private static a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 2219
    :try_start_0
    invoke-interface {p1, p2}, Lcom/google/android/gms/fitness/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2223
    :goto_0
    return-void

    .line 2220
    :catch_0
    move-exception v0

    const-string v1, "Couldn\'t send callback to client for %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/service/i;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 149
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/fitness/service/u;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/gms/fitness/service/u;-><init>(Lcom/google/android/gms/fitness/service/i;Ljava/lang/String;Ljava/util/concurrent/atomic/AtomicInteger;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1455
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->d()Ljava/lang/String;

    move-result-object v2

    .line 1456
    if-eqz v2, :cond_0

    invoke-virtual {v2, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1457
    const-string v2, "DataSource is inconsistent with app\'s package name: %s vs %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p1, v3, v0

    aput-object p0, v3, v1

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1461
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1441
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSet;

    .line 1442
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 1451
    :goto_0
    return v0

    .line 1446
    :cond_1
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    .line 1447
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->c()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/fitness/service/i;->a(Ljava/lang/String;Lcom/google/android/gms/fitness/data/DataSource;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1448
    goto :goto_0

    .line 1451
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/gms/fitness/service/i;->b:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/fitness/service/i;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V
    .locals 4

    .prologue
    .line 2213
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.bluetooth.adapter.action.REQUEST_ENABLE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    const/4 v2, 0x0

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x1396

    const-string v3, "Application needs enabled Bluetooth"

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-static {p1, p2, v1}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/common/api/Status;)V

    .line 2214
    return-void
.end method

.method private b(Lcom/google/android/gms/fitness/data/Session;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 967
    const-string v0, "Adding activity segment for session: %s"

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 968
    invoke-static {p1}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    .line 969
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/fitness/data/DataSource;->e()Lcom/google/android/gms/fitness/data/Application;

    move-result-object v2

    invoke-interface {v1, v0, v2, v3}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSet;Lcom/google/android/gms/fitness/data/Application;Z)V

    .line 972
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/fitness/service/i;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/gms/fitness/service/i;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/fitness/data/Session;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 981
    const-string v0, "Deleting activity segment for session: %s"

    new-array v1, v3, [Ljava/lang/Object;

    aput-object p1, v1, v4

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 982
    invoke-static {p1}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/DataSet;

    move-result-object v0

    .line 984
    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->d()Ljava/util/List;

    move-result-object v1

    .line 985
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 986
    const-string v0, "Expected 1 session activity segment data point: %s"

    new-array v2, v3, [Ljava/lang/Object;

    aput-object v1, v2, v4

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 993
    :goto_0
    return-void

    .line 989
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSet;->b()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/fitness/l/z;->a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/util/List;Z)I

    move-result v0

    .line 991
    const-string v1, "Deleted %d session activity segment data points for session: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method static synthetic d()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 149
    sget-object v0, Lcom/google/android/gms/fitness/service/i;->d:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/k/a;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/service/av;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    return-object v0
.end method

.method private static e()Z
    .locals 2

    .prologue
    .line 2199
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/fitness/service/i;)Landroid/os/RemoteCallbackList;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->w:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method private static f()Z
    .locals 1

    .prologue
    .line 2204
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    .line 2205
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/j/b;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->z:Lcom/google/android/gms/fitness/j/b;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/sensors/a;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->n:Lcom/google/android/gms/fitness/sensors/a;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/fitness/service/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/settings/i;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->y:Lcom/google/android/gms/fitness/settings/i;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/d/d;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/fitness/service/i;)Lcom/google/android/gms/fitness/sensors/a/z;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->s:Lcom/google/android/gms/fitness/sensors/a/z;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/fitness/service/i;)Lcom/google/k/k/a/ai;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->A:Lcom/google/k/k/a/ai;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->n:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/sensors/a;->a()V

    .line 276
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/fitness/service/j;

    invoke-direct {v2, p0}, Lcom/google/android/gms/fitness/service/j;-><init>(Lcom/google/android/gms/fitness/service/i;)V

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 277
    new-instance v0, Lcom/google/android/gms/fitness/i/b;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    iget-object v4, p0, Lcom/google/android/gms/fitness/service/i;->r:Lcom/google/android/gms/fitness/d/d;

    iget-object v5, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/i/b;-><init>(Ljava/lang/String;Lcom/google/android/gms/fitness/service/av;Lcom/google/android/gms/fitness/l/z;Lcom/google/android/gms/fitness/d/d;Landroid/os/Handler;)V

    const-string v1, "Registering receiver for app uninstalls."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v2, "package"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const-string v1, "Registering receiver for app disconnects."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.google.android.gms.fitness.app_disconnected"

    invoke-virtual {v2, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :try_start_0
    const-string v1, "vnd.google.android.fitness/app_disconnect"

    invoke-virtual {v2, v1}, Landroid/content/IntentFilter;->addDataType(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentFilter$MalformedMimeTypeException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->v:Lcom/google/android/gms/fitness/service/aq;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->l:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/fitness/l/f;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-direct {v2, v3}, Lcom/google/android/gms/fitness/l/f;-><init>(Lcom/google/android/gms/fitness/l/z;)V

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/aq;->b:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/fitness/service/ar;

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/gms/fitness/service/ar;-><init>(Lcom/google/android/gms/fitness/service/aq;Ljava/lang/String;Lcom/google/android/gms/fitness/l/f;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->v:Lcom/google/android/gms/fitness/service/aq;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->u:Lcom/google/android/gms/fitness/a/o;

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/aq;->b:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/fitness/service/as;

    invoke-direct {v4, v0, v1, v2}, Lcom/google/android/gms/fitness/service/as;-><init>(Lcom/google/android/gms/fitness/service/aq;Ljava/lang/String;Lcom/google/android/gms/fitness/a/o;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 282
    return-void

    .line 277
    :catch_0
    move-exception v1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while adding an intent filter for app disconnects: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1844
    const-string v0, "disableFit request received for: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1845
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1846
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/w;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/fitness/service/w;-><init>(Lcom/google/android/gms/fitness/service/i;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1871
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/internal/ae;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2101
    const-string v0, "listClaimedBleDevices request received"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2102
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 2104
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/af;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/fitness/service/af;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/ae;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2122
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/internal/ble/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 2069
    const-string v0, "listClaimedBleDevices request received"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2070
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 2072
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/ae;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/fitness/service/ae;-><init>(Lcom/google/android/gms/fitness/service/i;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/ble/c;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2096
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/ClaimBleDeviceRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1951
    const-string v0, "claimBleDevice request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1952
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1953
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1954
    invoke-static {p1, p2}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V

    .line 2016
    :goto_0
    return-void

    .line 1957
    :cond_0
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1958
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/service/i;->b(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V

    goto :goto_0

    .line 1962
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/z;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/fitness/service/z;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/ClaimBleDeviceRequest;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/request/DataDeleteRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1467
    const-string v0, "deleteData request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1468
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1470
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/r;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/r;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataDeleteRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1486
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/DataInsertRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1387
    const-string v0, "insertData request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1388
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1390
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/q;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/fitness/service/q;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/fitness/request/DataInsertRequest;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1404
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/DataReadRequest;Lcom/google/android/gms/fitness/internal/g;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1705
    const-string v0, "ReadData request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1706
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1708
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/v;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/v;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataReadRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/g;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1733
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/DataSourcesRequest;Lcom/google/android/gms/fitness/internal/j;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 376
    const-string v0, "findDataSources request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/ag;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/ag;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataSourcesRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/j;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 398
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/DataTypeCreateRequest;Lcom/google/android/gms/fitness/internal/m;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1603
    const-string v0, "createCustomDataType request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1604
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1606
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/s;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/s;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataTypeCreateRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/m;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1618
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/DataTypeReadRequest;Lcom/google/android/gms/fitness/internal/m;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1652
    const-string v0, "readCustomDataType request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1653
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1655
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/t;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/t;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/DataTypeReadRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/m;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1673
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/ListSubscriptionsRequest;Lcom/google/android/gms/fitness/internal/s;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 779
    const-string v0, "listSubscriptions request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 782
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/an;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/fitness/service/an;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/s;Lcom/google/android/gms/fitness/request/ListSubscriptionsRequest;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 798
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v4, Lcom/google/android/gms/fitness/internal/a;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/fitness/e/a;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-direct {v4, v0, p3, v1}, Lcom/google/android/gms/fitness/internal/a;-><init>(ILjava/lang/String;Z)V

    .line 438
    iget-object v6, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/gms/fitness/service/ah;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/service/ah;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/a;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 457
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SensorUnregistrationRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 558
    const-string v0, "unregister request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 559
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 561
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/ak;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/ak;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SensorUnregistrationRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 591
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SessionInsertRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 818
    const-string v0, "insertSession request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 819
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 821
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/k;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/fitness/service/k;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/fitness/request/SessionInsertRequest;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 836
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SessionReadRequest;Lcom/google/android/gms/fitness/internal/v;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1278
    const-string v0, "readSession request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1279
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1281
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/p;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/p;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SessionReadRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/v;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1298
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SessionRegistrationRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1154
    const-string v0, "registerForSessions request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1155
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1157
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/n;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/n;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/SessionRegistrationRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1169
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SessionStartRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 998
    const-string v0, "startSession request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 999
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/l;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/fitness/service/l;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/fitness/request/SessionStartRequest;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1016
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SessionStopRequest;Lcom/google/android/gms/fitness/internal/y;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1095
    const-string v0, "stopSession request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1096
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1098
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/m;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/fitness/service/m;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/y;Lcom/google/android/gms/fitness/request/SessionStopRequest;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1114
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SessionUnregistrationRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1190
    const-string v0, "unregisterForSessions request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1191
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1193
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/o;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/gms/fitness/service/o;-><init>(Lcom/google/android/gms/fitness/service/i;Ljava/lang/String;Lcom/google/android/gms/fitness/request/SessionUnregistrationRequest;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1205
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/StartBleScanRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1879
    const-string v0, "startBleScan request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1880
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1881
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1882
    invoke-static {p1, p2}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V

    .line 1908
    :goto_0
    return-void

    .line 1885
    :cond_0
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1886
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/service/i;->b(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V

    goto :goto_0

    .line 1890
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/x;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/fitness/service/x;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/StartBleScanRequest;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/request/StopBleScanRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1915
    const-string v0, "stopBleScan request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 1916
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 1917
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1918
    invoke-static {p1, p2}, Lcom/google/android/gms/fitness/service/i;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V

    .line 1944
    :goto_0
    return-void

    .line 1921
    :cond_0
    invoke-static {}, Lcom/google/android/gms/fitness/service/i;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1922
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/fitness/service/i;->b(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Lcom/google/android/gms/fitness/internal/ab;)V

    goto :goto_0

    .line 1926
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/y;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/fitness/service/y;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/StopBleScanRequest;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/request/SubscribeRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 596
    const-string v0, "subscribe request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 597
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 599
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/al;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/fitness/service/al;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/fitness/request/SubscribeRequest;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 613
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/UnclaimBleDeviceRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 2035
    const-string v0, "unclaimBleDevice request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2036
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 2037
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/ad;

    invoke-direct {v1, p0, p1, p3, p2}, Lcom/google/android/gms/fitness/service/ad;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/request/UnclaimBleDeviceRequest;Ljava/lang/String;Lcom/google/android/gms/fitness/internal/ab;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 2063
    return-void
.end method

.method public final a(Lcom/google/android/gms/fitness/request/UnsubscribeRequest;Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 721
    const-string v0, "unsubscribe request received %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 722
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 724
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->k:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/fitness/service/am;

    invoke-direct {v1, p0, p2, p1, p3}, Lcom/google/android/gms/fitness/service/am;-><init>(Lcom/google/android/gms/fitness/service/i;Lcom/google/android/gms/fitness/internal/ab;Lcom/google/android/gms/fitness/request/UnsubscribeRequest;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 738
    return-void
.end method

.method public final b(Lcom/google/android/gms/fitness/internal/ab;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 2126
    const-string v0, "deleteAllUserData request received"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 2127
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 2128
    const-string v0, "com.google.android.gms"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2129
    new-instance v0, Ljava/lang/SecurityException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not allowed for package "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2133
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->q:Lcom/google/android/gms/fitness/k/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/k/a;->c()V

    .line 2134
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/fitness/sync/g; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 2141
    :goto_0
    return-void

    .line 2136
    :catch_0
    move-exception v0

    :goto_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/fitness/service/i;->b:Lcom/google/android/gms/common/api/Status;

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/internal/ab;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 2138
    :catch_1
    move-exception v0

    const-string v1, "Couldn\'t return result to caller"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 2136
    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method public final binderDied()V
    .locals 0

    .prologue
    .line 2146
    return-void
.end method

.method protected final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 2150
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/fitness/internal/q;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2151
    const-string v0, "Local device: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->j:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/fitness/data/Device;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/data/Device;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 2154
    const-string v0, "Registrations recreation attempts: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/i;->B:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 2157
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->n:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/fitness/sensors/a;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2158
    iget-object v2, p0, Lcom/google/android/gms/fitness/service/i;->p:Lcom/google/android/gms/fitness/service/av;

    const-string v0, "RecordingManager:\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    iget-object v0, v2, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    const-string v1, "  "

    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v4, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v1

    const-string v4, ":\n"

    invoke-virtual {v1, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/e/g;

    const-string v4, "    "

    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v4

    iget-object v0, v0, Lcom/google/android/gms/fitness/sensors/e/g;->d:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Subscription;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v4, "\n"

    invoke-virtual {v0, v4}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    goto :goto_0

    :cond_1
    const-string v0, "  sessions: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, v2, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1}, Ljava/util/concurrent/ConcurrentHashMap;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    iget-object v0, v2, Lcom/google/android/gms/fitness/service/av;->d:Lcom/google/android/gms/fitness/l/a/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/fitness/l/a/a;->a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 2159
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/i;->m:Lcom/google/android/gms/fitness/l/z;

    invoke-interface {v0, p2}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/io/PrintWriter;)V

    .line 2161
    const-string v0, "WearableSyncService: {\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 2162
    invoke-static {p2}, Lcom/google/android/gms/fitness/wearables/WearableSyncService;->a(Ljava/io/PrintWriter;)V

    .line 2163
    const-string v0, "}\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 2164
    return-void
.end method
