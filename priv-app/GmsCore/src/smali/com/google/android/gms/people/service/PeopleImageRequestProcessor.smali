.class public Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/people/service/c;

.field private static final b:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 27
    new-instance v0, Lcom/google/android/gms/people/service/l;

    invoke-direct {v0}, Lcom/google/android/gms/people/service/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->a:Lcom/google/android/gms/people/service/c;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-string v0, "PeopleIRP"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method

.method static synthetic a()Ljava/util/concurrent/ConcurrentLinkedQueue;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-object v0
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 42
    sget-object v0, Lcom/google/android/gms/people/service/PeopleImageRequestProcessor;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/service/a;

    .line 43
    if-nez v0, :cond_0

    .line 44
    const-string v0, "PeopleIRP"

    const-string v1, "No operation found when processing!"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->f(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    :goto_0
    return-void

    .line 48
    :cond_0
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/people/service/a;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 50
    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/service/a;->a(Z)V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/service/a;->a(Z)V

    throw v1
.end method
