.class public final Lcom/google/android/gms/plus/plusone/e;
.super Lcom/google/android/gms/plus/plusone/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field public static final h:Landroid/net/Uri;

.field public static final i:Landroid/net/Uri;

.field public static final j:Landroid/net/Uri;


# instance fields
.field private A:Lcom/google/android/gms/common/c;

.field private final B:Ljava/lang/Runnable;

.field private final C:Lcom/google/android/gms/plus/internal/aq;

.field private final D:Lcom/google/android/gms/plus/internal/aq;

.field private final E:Lcom/google/android/gms/plus/internal/au;

.field private F:Lcom/google/android/gms/common/es;

.field private G:Lcom/google/android/gms/common/es;

.field private H:Lcom/google/android/gms/common/et;

.field private I:Lcom/google/android/gms/common/et;

.field private J:Ljava/lang/String;

.field protected k:Lcom/google/android/gms/plus/data/a/b;

.field protected l:Z

.field protected m:Z

.field protected n:Z

.field protected o:Z

.field private final p:Landroid/content/Context;

.field private final q:Landroid/view/Display;

.field private r:Landroid/widget/PopupWindow;

.field private s:Z

.field private final t:Landroid/widget/ImageView;

.field private final u:Landroid/widget/ImageView;

.field private v:Landroid/view/View$OnClickListener;

.field private final w:Lcom/google/android/gms/plus/internal/ad;

.field private x:Lcom/google/android/gms/plus/internal/ab;

.field private y:Lcom/google/android/gms/plus/internal/ab;

.field private z:Lcom/google/android/gms/plus/data/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const-string v0, "plus_one_button_popup_beak_up"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/by;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/plusone/e;->h:Landroid/net/Uri;

    .line 62
    const-string v0, "plus_one_button_popup_beak_down"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/by;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/plusone/e;->i:Landroid/net/Uri;

    .line 65
    const-string v0, "plus_one_button_popup_bg"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/by;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/plus/plusone/e;->j:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 309
    sget-object v6, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/plusone/e;-><init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V

    .line 311
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/internal/ad;)V
    .locals 6

    .prologue
    .line 321
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/plusone/a;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;IILjava/lang/String;)V

    .line 129
    new-instance v0, Lcom/google/android/gms/plus/plusone/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/f;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->B:Ljava/lang/Runnable;

    .line 142
    new-instance v0, Lcom/google/android/gms/plus/plusone/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/g;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->C:Lcom/google/android/gms/plus/internal/aq;

    .line 168
    new-instance v0, Lcom/google/android/gms/plus/plusone/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/h;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->D:Lcom/google/android/gms/plus/internal/aq;

    .line 178
    new-instance v0, Lcom/google/android/gms/plus/plusone/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/i;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->E:Lcom/google/android/gms/plus/internal/au;

    .line 212
    new-instance v0, Lcom/google/android/gms/plus/plusone/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/j;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->F:Lcom/google/android/gms/common/es;

    .line 241
    new-instance v0, Lcom/google/android/gms/plus/plusone/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/k;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->G:Lcom/google/android/gms/common/es;

    .line 261
    new-instance v0, Lcom/google/android/gms/plus/plusone/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/l;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->H:Lcom/google/android/gms/common/et;

    .line 288
    new-instance v0, Lcom/google/android/gms/plus/plusone/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/m;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->I:Lcom/google/android/gms/common/et;

    .line 323
    iput-object p6, p0, Lcom/google/android/gms/plus/plusone/e;->w:Lcom/google/android/gms/plus/internal/ad;

    .line 324
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->q:Landroid/view/Display;

    .line 328
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->t:Landroid/widget/ImageView;

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->t:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/plus/plusone/e;->h:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 330
    new-instance v0, Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->u:Landroid/widget/ImageView;

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->u:Landroid/widget/ImageView;

    sget-object v1, Lcom/google/android/gms/plus/plusone/e;->j:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 334
    invoke-virtual {p0, p0}, Lcom/google/android/gms/plus/plusone/e;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 336
    iput-object p5, p0, Lcom/google/android/gms/plus/plusone/e;->J:Ljava/lang/String;

    .line 337
    new-instance v0, Lcom/google/android/gms/plus/internal/cn;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "https://www.googleapis.com/auth/pos"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->J:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->J:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->w:Lcom/google/android/gms/plus/internal/ad;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/e;->F:Lcom/google/android/gms/common/es;

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/e;->H:Lcom/google/android/gms/common/et;

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/plusone/e;->a(Lcom/google/android/gms/plus/internal/ab;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    .line 338
    new-instance v0, Lcom/google/android/gms/plus/internal/cn;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->a()Lcom/google/android/gms/plus/internal/cn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->w:Lcom/google/android/gms/plus/internal/ad;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/e;->G:Lcom/google/android/gms/common/es;

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/e;->I:Lcom/google/android/gms/common/et;

    invoke-interface {v1, v2, v0, v3, v4}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->y:Lcom/google/android/gms/plus/internal/ab;

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/e;->a(Lcom/google/android/gms/plus/internal/ab;)V

    .line 342
    new-instance v0, Lcom/google/android/gms/plus/plusone/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/plusone/n;-><init>(Lcom/google/android/gms/plus/plusone/e;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/e;->setTag(Ljava/lang/Object;)V

    .line 343
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/common/c;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->A:Lcom/google/android/gms/common/c;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/plusone/e;Lcom/google/android/gms/common/c;)Lcom/google/android/gms/common/c;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/e;->A:Lcom/google/android/gms/common/c;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/plusone/e;Lcom/google/android/gms/plus/data/a/c;)Lcom/google/android/gms/plus/data/a/c;
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/e;->z:Lcom/google/android/gms/plus/data/a/c;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 515
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->v:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->v:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 518
    :cond_0
    return-void
.end method

.method private a(Landroid/widget/FrameLayout;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, -0x2

    .line 585
    new-instance v1, Lcom/google/android/gms/plus/plusone/o;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/plusone/o;-><init>(Lcom/google/android/gms/plus/plusone/e;Landroid/widget/FrameLayout;)V

    .line 588
    new-instance v2, Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 589
    iget-boolean v0, v1, Lcom/google/android/gms/plus/plusone/o;->a:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/plus/plusone/e;->i:Landroid/net/Uri;

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    .line 591
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    iget v3, v1, Lcom/google/android/gms/plus/plusone/o;->f:I

    invoke-direct {v0, v4, v4, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 594
    iget v3, v1, Lcom/google/android/gms/plus/plusone/o;->b:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget v4, v1, Lcom/google/android/gms/plus/plusone/o;->d:I

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v3, v5, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 596
    invoke-virtual {p1, v2, v0}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 599
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v3

    invoke-direct {v0, p1, v2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    .line 601
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    .line 604
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->s:Z

    if-eqz v0, :cond_0

    .line 605
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    const/16 v2, 0x33

    iget v3, v1, Lcom/google/android/gms/plus/plusone/o;->g:I

    iget v1, v1, Lcom/google/android/gms/plus/plusone/o;->h:I

    invoke-virtual {v0, p0, v2, v3, v1}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 607
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->B:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/e;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 608
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->B:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/plus/plusone/e;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 610
    :cond_0
    return-void

    .line 589
    :cond_1
    sget-object v0, Lcom/google/android/gms/plus/plusone/e;->h:Landroid/net/Uri;

    goto :goto_0
.end method

.method private b(Landroid/view/View;)Landroid/widget/FrameLayout;
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, -0x2

    .line 656
    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 657
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 659
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p1, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 661
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/ab;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/plusone/e;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->s:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/plusone/e;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/plusone/e;)Landroid/widget/PopupWindow;
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/aq;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->D:Lcom/google/android/gms/plus/internal/aq;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/aq;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->C:Lcom/google/android/gms/plus/internal/aq;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/data/a/c;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->z:Lcom/google/android/gms/plus/data/a/c;

    return-object v0
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic i(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/au;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->E:Lcom/google/android/gms/plus/internal/au;

    return-object v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 536
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->s:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 537
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 538
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    .line 540
    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/ab;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->y:Lcom/google/android/gms/plus/internal/ab;

    return-object v0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 567
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->l:Z

    if-eqz v0, :cond_0

    .line 568
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->l:Z

    .line 570
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    if-eqz v0, :cond_1

    .line 571
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/b;->c()Ljava/lang/String;

    move-result-object v1

    .line 572
    if-eqz v1, :cond_1

    .line 573
    const-string v0, "plus_popup_text"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/e;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v2

    const-string v0, "text"

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/plusone/e;->b(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/plusone/e;->a(Landroid/widget/FrameLayout;)V

    .line 582
    :cond_0
    :goto_0
    return-void

    .line 577
    :cond_1
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578
    const-string v0, "PlusOneButtonView"

    const-string v1, "Text confirmation popup requested but text is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 580
    :cond_2
    invoke-virtual {p0, v2}, Lcom/google/android/gms/plus/plusone/e;->a(I)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/android/gms/plus/plusone/e;)Landroid/view/Display;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->q:Landroid/view/Display;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/plus/plusone/e;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->t:Landroid/widget/ImageView;

    return-object v0
.end method


# virtual methods
.method protected final b()Z
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->n:Z

    return v0
.end method

.method protected final d()Z
    .locals 1

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 462
    invoke-super {p0}, Lcom/google/android/gms/plus/plusone/a;->e()V

    .line 463
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/e;->i()V

    .line 464
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/e;->j()V

    .line 465
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 469
    invoke-super {p0}, Lcom/google/android/gms/plus/plusone/a;->f()V

    .line 470
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/e;->i()V

    .line 471
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/e;->j()V

    .line 472
    return-void
.end method

.method public final g()V
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 476
    invoke-super {p0}, Lcom/google/android/gms/plus/plusone/a;->g()V

    .line 477
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/e;->i()V

    .line 478
    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->s:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->l:Z

    if-eqz v0, :cond_0

    iput-boolean v6, p0, Lcom/google/android/gms/plus/plusone/e;->l:Z

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->z:Lcom/google/android/gms/plus/data/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->z:Lcom/google/android/gms/plus/data/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/c;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/b;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/e;->z:Lcom/google/android/gms/plus/data/a/c;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/data/a/c;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v0, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    iget-object v0, v0, Lcom/google/android/gms/plus/data/a/b;->a:Landroid/os/Bundle;

    const-string v3, "visibility"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    const-string v0, "plus_popup_confirmation"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/e;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->p:Landroid/content/Context;

    invoke-direct {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lcom/google/android/gms/plus/internal/ab;)V

    const/high16 v0, 0x42000000    # 32.0f

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/e;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    invoke-static {v7, v0, v5}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    new-instance v5, Lcom/google/android/gms/common/internal/bs;

    invoke-direct {v5, v2}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    float-to-int v0, v0

    invoke-virtual {v5, v0}, Lcom/google/android/gms/common/internal/bs;->a(I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/x;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    const-string v0, "profile_image"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v4, v6}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;I)V

    const-string v0, "text"

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0, v3}, Lcom/google/android/gms/plus/plusone/e;->b(Landroid/view/View;)Landroid/widget/FrameLayout;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/plusone/e;->a(Landroid/widget/FrameLayout;)V

    .line 479
    :cond_0
    :goto_1
    return-void

    .line 478
    :cond_1
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "PlusOneButtonView"

    const-string v1, "Confirmation popup requested but content view cannot be created"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual {p0, v4}, Lcom/google/android/gms/plus/plusone/e;->a(I)V

    goto :goto_1

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final onAttachedToWindow()V
    .locals 1

    .prologue
    .line 440
    invoke-super {p0}, Lcom/google/android/gms/plus/plusone/a;->onAttachedToWindow()V

    .line 441
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 444
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->s:Z

    .line 445
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x2

    const/4 v1, 0x1

    .line 378
    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/e;->c:Landroid/widget/CompoundButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->toggle()V

    .line 381
    iget-boolean v2, p0, Lcom/google/android/gms/plus/plusone/e;->m:Z

    if-eqz v2, :cond_1

    .line 382
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    const-string v0, "PlusOneButtonView"

    const-string v1, "onClick: result pending, ignoring +1 button click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :cond_0
    :goto_0
    return-void

    .line 388
    :cond_1
    iget-boolean v2, p0, Lcom/google/android/gms/plus/plusone/e;->o:Z

    if-eqz v2, :cond_3

    .line 389
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 390
    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: anonymous button, forwarding to external click listener"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 392
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/plusone/e;->a(Landroid/view/View;)V

    .line 393
    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/e;->n:Z

    goto :goto_0

    .line 397
    :cond_3
    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/e;->l:Z

    .line 399
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/e;->h()Z

    move-result v2

    if-nez v2, :cond_6

    .line 402
    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->l:Z

    .line 405
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 406
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 407
    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: reload +1"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/e;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/e;->C:Lcom/google/android/gms/plus/internal/aq;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/e;->f:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V

    .line 411
    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/e;->m:Z

    .line 435
    :cond_5
    :goto_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/plusone/e;->a(Landroid/view/View;)V

    goto :goto_0

    .line 414
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/plus/plusone/e;->h()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/data/a/b;->a()Z

    move-result v2

    if-eqz v2, :cond_7

    move v0, v1

    :cond_7
    if-eqz v0, :cond_9

    .line 415
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 416
    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: undo +1"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 418
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/e;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/e;->C:Lcom/google/android/gms/plus/internal/aq;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/e;->f:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/plus/internal/ab;->b(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V

    .line 420
    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/e;->m:Z

    goto :goto_1

    .line 423
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 424
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 425
    const-string v0, "PlusOneButtonView"

    const-string v2, "onClick: +1"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/e;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 428
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/e;->C:Lcom/google/android/gms/plus/internal/aq;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/e;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/e;->k:Lcom/google/android/gms/plus/data/a/b;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/data/a/b;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    iput-boolean v1, p0, Lcom/google/android/gms/plus/plusone/e;->m:Z

    goto :goto_1
.end method

.method public final onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 449
    invoke-super {p0}, Lcom/google/android/gms/plus/plusone/a;->onDetachedFromWindow()V

    .line 450
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/e;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 451
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->x:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 453
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/plusone/e;->s:Z

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    .line 455
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 456
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/plusone/e;->r:Landroid/widget/PopupWindow;

    .line 458
    :cond_2
    return-void
.end method

.method public final setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 368
    if-ne p1, p0, :cond_0

    .line 369
    invoke-super {p0, p0}, Lcom/google/android/gms/plus/plusone/a;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 373
    :goto_0
    return-void

    .line 371
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/e;->v:Landroid/view/View$OnClickListener;

    goto :goto_0
.end method
