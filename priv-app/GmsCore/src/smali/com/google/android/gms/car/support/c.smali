.class final Lcom/google/android/gms/car/support/c;
.super Lcom/google/android/gms/car/support/s;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Lcom/google/android/gms/car/support/n;

.field b:Lcom/google/android/gms/car/support/d;

.field c:Lcom/google/android/gms/car/support/d;

.field d:I

.field e:I

.field f:I

.field g:I

.field h:I

.field i:I

.field j:I

.field k:Z

.field l:Z

.field m:Ljava/lang/String;

.field n:Z

.field o:I

.field p:I

.field q:Ljava/lang/CharSequence;

.field r:I

.field s:Ljava/lang/CharSequence;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/support/n;)V
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0}, Lcom/google/android/gms/car/support/s;-><init>()V

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/c;->l:Z

    .line 219
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/c;->o:I

    .line 342
    iput-object p1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    .line 343
    return-void
.end method


# virtual methods
.method final a(I)V
    .locals 6

    .prologue
    .line 583
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/c;->k:Z

    if-nez v0, :cond_1

    .line 605
    :cond_0
    return-void

    .line 586
    :cond_1
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bump nesting in "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " by 1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->b:Lcom/google/android/gms/car/support/d;

    move-object v2, v0

    .line 589
    :goto_0
    if-eqz v2, :cond_0

    .line 590
    iget-object v0, v2, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    if-eqz v0, :cond_3

    .line 591
    iget-object v0, v2, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    iget v1, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    .line 592
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_3

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Bump nesting of "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, v2, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    iget v3, v3, Lcom/google/android/gms/car/support/Fragment;->r:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 595
    :cond_3
    iget-object v0, v2, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 596
    iget-object v0, v2, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_5

    .line 597
    iget-object v0, v2, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 598
    iget v3, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    .line 599
    sget-boolean v3, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v3, :cond_4

    const-string v3, "FragmentManager"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Bump nesting of "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v0, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 603
    :cond_5
    iget-object v0, v2, Lcom/google/android/gms/car/support/d;->a:Lcom/google/android/gms/car/support/d;

    move-object v2, v0

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/car/support/d;)V
    .locals 1

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->b:Lcom/google/android/gms/car/support/d;

    if-nez v0, :cond_0

    .line 378
    iput-object p1, p0, Lcom/google/android/gms/car/support/c;->c:Lcom/google/android/gms/car/support/d;

    iput-object p1, p0, Lcom/google/android/gms/car/support/c;->b:Lcom/google/android/gms/car/support/d;

    .line 384
    :goto_0
    iget v0, p0, Lcom/google/android/gms/car/support/c;->e:I

    iput v0, p1, Lcom/google/android/gms/car/support/d;->e:I

    .line 385
    iget v0, p0, Lcom/google/android/gms/car/support/c;->f:I

    iput v0, p1, Lcom/google/android/gms/car/support/d;->f:I

    .line 386
    iget v0, p0, Lcom/google/android/gms/car/support/c;->g:I

    iput v0, p1, Lcom/google/android/gms/car/support/d;->g:I

    .line 387
    iget v0, p0, Lcom/google/android/gms/car/support/c;->h:I

    iput v0, p1, Lcom/google/android/gms/car/support/d;->h:I

    .line 388
    iget v0, p0, Lcom/google/android/gms/car/support/c;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/support/c;->d:I

    .line 389
    return-void

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->c:Lcom/google/android/gms/car/support/d;

    iput-object v0, p1, Lcom/google/android/gms/car/support/d;->b:Lcom/google/android/gms/car/support/d;

    .line 381
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->c:Lcom/google/android/gms/car/support/d;

    iput-object p1, v0, Lcom/google/android/gms/car/support/d;->a:Lcom/google/android/gms/car/support/d;

    .line 382
    iput-object p1, p0, Lcom/google/android/gms/car/support/c;->c:Lcom/google/android/gms/car/support/d;

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/car/support/c;->a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V

    .line 246
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;Z)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 249
    if-eqz p3, :cond_8

    .line 250
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mName="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->m:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 251
    const-string v0, " mIndex="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/support/c;->o:I

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 252
    const-string v0, " mCommitted="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/support/c;->n:Z

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Z)V

    .line 253
    iget v0, p0, Lcom/google/android/gms/car/support/c;->i:I

    if-eqz v0, :cond_0

    .line 254
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mTransition=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 255
    iget v0, p0, Lcom/google/android/gms/car/support/c;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 256
    const-string v0, " mTransitionStyle=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 257
    iget v0, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 259
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/support/c;->e:I

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/support/c;->f:I

    if-eqz v0, :cond_2

    .line 260
    :cond_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 261
    iget v0, p0, Lcom/google/android/gms/car/support/c;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 262
    const-string v0, " mExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 263
    iget v0, p0, Lcom/google/android/gms/car/support/c;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 265
    :cond_2
    iget v0, p0, Lcom/google/android/gms/car/support/c;->g:I

    if-nez v0, :cond_3

    iget v0, p0, Lcom/google/android/gms/car/support/c;->h:I

    if-eqz v0, :cond_4

    .line 266
    :cond_3
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mPopEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 267
    iget v0, p0, Lcom/google/android/gms/car/support/c;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 268
    const-string v0, " mPopExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 269
    iget v0, p0, Lcom/google/android/gms/car/support/c;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 271
    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/support/c;->p:I

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->q:Ljava/lang/CharSequence;

    if-eqz v0, :cond_6

    .line 272
    :cond_5
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 273
    iget v0, p0, Lcom/google/android/gms/car/support/c;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 274
    const-string v0, " mBreadCrumbTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->q:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 277
    :cond_6
    iget v0, p0, Lcom/google/android/gms/car/support/c;->r:I

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->s:Ljava/lang/CharSequence;

    if-eqz v0, :cond_8

    .line 278
    :cond_7
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "mBreadCrumbShortTitleRes=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 279
    iget v0, p0, Lcom/google/android/gms/car/support/c;->r:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 280
    const-string v0, " mBreadCrumbShortTitleText="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->s:Ljava/lang/CharSequence;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 285
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->b:Lcom/google/android/gms/car/support/d;

    if-eqz v0, :cond_10

    .line 286
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "Operations:"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 287
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "    "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->b:Lcom/google/android/gms/car/support/d;

    move v2, v1

    move-object v3, v0

    .line 290
    :goto_0
    if-eqz v3, :cond_10

    .line 292
    iget v0, v3, Lcom/google/android/gms/car/support/d;->c:I

    packed-switch v0, :pswitch_data_0

    .line 301
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "cmd="

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, Lcom/google/android/gms/car/support/d;->c:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  Op #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 304
    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 305
    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, v3, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 306
    if-eqz p3, :cond_c

    .line 307
    iget v0, v3, Lcom/google/android/gms/car/support/d;->e:I

    if-nez v0, :cond_9

    iget v0, v3, Lcom/google/android/gms/car/support/d;->f:I

    if-eqz v0, :cond_a

    .line 308
    :cond_9
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "enterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 309
    iget v0, v3, Lcom/google/android/gms/car/support/d;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 310
    const-string v0, " exitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 311
    iget v0, v3, Lcom/google/android/gms/car/support/d;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 313
    :cond_a
    iget v0, v3, Lcom/google/android/gms/car/support/d;->g:I

    if-nez v0, :cond_b

    iget v0, v3, Lcom/google/android/gms/car/support/d;->h:I

    if-eqz v0, :cond_c

    .line 314
    :cond_b
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v0, "popEnterAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 315
    iget v0, v3, Lcom/google/android/gms/car/support/d;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 316
    const-string v0, " popExitAnim=#"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 317
    iget v0, v3, Lcom/google/android/gms/car/support/d;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 320
    :cond_c
    iget-object v0, v3, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    if-eqz v0, :cond_f

    iget-object v0, v3, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_f

    move v0, v1

    .line 321
    :goto_2
    iget-object v5, v3, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v0, v5, :cond_f

    .line 322
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 323
    iget-object v5, v3, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_d

    .line 324
    const-string v5, "Removed: "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 332
    :goto_3
    iget-object v5, v3, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/Object;)V

    .line 321
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 293
    :pswitch_0
    const-string v0, "NULL"

    goto/16 :goto_1

    .line 294
    :pswitch_1
    const-string v0, "ADD"

    goto/16 :goto_1

    .line 295
    :pswitch_2
    const-string v0, "REPLACE"

    goto/16 :goto_1

    .line 296
    :pswitch_3
    const-string v0, "REMOVE"

    goto/16 :goto_1

    .line 297
    :pswitch_4
    const-string v0, "HIDE"

    goto/16 :goto_1

    .line 298
    :pswitch_5
    const-string v0, "SHOW"

    goto/16 :goto_1

    .line 299
    :pswitch_6
    const-string v0, "DETACH"

    goto/16 :goto_1

    .line 300
    :pswitch_7
    const-string v0, "ATTACH"

    goto/16 :goto_1

    .line 326
    :cond_d
    if-nez v0, :cond_e

    .line 327
    const-string v5, "Removed:"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 329
    :cond_e
    invoke-virtual {p2, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v5, "  #"

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 330
    const-string v5, ": "

    invoke-virtual {p2, v5}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_3

    .line 335
    :cond_f
    iget-object v3, v3, Lcom/google/android/gms/car/support/d;->a:Lcom/google/android/gms/car/support/d;

    .line 336
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 337
    goto/16 :goto_0

    .line 339
    :cond_10
    return-void

    .line 292
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 637
    sget-boolean v0, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "FragmentManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Run: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/c;->k:Z

    if-eqz v0, :cond_1

    .line 640
    iget v0, p0, Lcom/google/android/gms/car/support/c;->o:I

    if-gez v0, :cond_1

    .line 641
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "addToBackStack() called after commit()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 645
    :cond_1
    invoke-virtual {p0, v8}, Lcom/google/android/gms/car/support/c;->a(I)V

    .line 647
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->b:Lcom/google/android/gms/car/support/d;

    move-object v4, v0

    .line 648
    :goto_0
    if-eqz v4, :cond_b

    .line 649
    iget v0, v4, Lcom/google/android/gms/car/support/d;->c:I

    packed-switch v0, :pswitch_data_0

    .line 712
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cmd: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v4, Lcom/google/android/gms/car/support/d;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 651
    :pswitch_0
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 652
    iget v1, v4, Lcom/google/android/gms/car/support/d;->e:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 653
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;Z)V

    .line 716
    :cond_2
    :goto_1
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->a:Lcom/google/android/gms/car/support/d;

    move-object v4, v0

    goto :goto_0

    .line 656
    :pswitch_1
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 657
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget-object v1, v1, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    if-eqz v1, :cond_9

    move v1, v2

    move-object v3, v0

    .line 658
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget-object v0, v0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_a

    .line 659
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget-object v0, v0, Lcom/google/android/gms/car/support/n;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/support/Fragment;

    .line 660
    sget-boolean v5, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v5, :cond_3

    const-string v5, "FragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "OP_REPLACE: adding="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " old="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    :cond_3
    if-eqz v3, :cond_4

    iget v5, v0, Lcom/google/android/gms/car/support/Fragment;->x:I

    iget v6, v3, Lcom/google/android/gms/car/support/Fragment;->x:I

    if-ne v5, v6, :cond_5

    .line 663
    :cond_4
    if-ne v0, v3, :cond_6

    .line 664
    const/4 v3, 0x0

    iput-object v3, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 658
    :cond_5
    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 666
    :cond_6
    iget-object v5, v4, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    if-nez v5, :cond_7

    .line 667
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    iput-object v5, v4, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    .line 669
    :cond_7
    iget-object v5, v4, Lcom/google/android/gms/car/support/d;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 670
    iget v5, v4, Lcom/google/android/gms/car/support/d;->f:I

    iput v5, v0, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 671
    iget-boolean v5, p0, Lcom/google/android/gms/car/support/c;->k:Z

    if-eqz v5, :cond_8

    .line 672
    iget v5, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    .line 673
    sget-boolean v5, Lcom/google/android/gms/car/support/n;->a:Z

    if-eqz v5, :cond_8

    const-string v5, "FragmentManager"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Bump nesting of "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/google/android/gms/car/support/Fragment;->r:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    :cond_8
    iget-object v5, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget v6, p0, Lcom/google/android/gms/car/support/c;->i:I

    iget v7, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-virtual {v5, v0, v6, v7}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;II)V

    goto :goto_3

    :cond_9
    move-object v3, v0

    .line 681
    :cond_a
    if-eqz v3, :cond_2

    .line 682
    iget v0, v4, Lcom/google/android/gms/car/support/d;->e:I

    iput v0, v3, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 683
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;Z)V

    goto/16 :goto_1

    .line 687
    :pswitch_2
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 688
    iget v1, v4, Lcom/google/android/gms/car/support/d;->f:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 689
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget v3, p0, Lcom/google/android/gms/car/support/c;->i:I

    iget v5, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    .line 692
    :pswitch_3
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 693
    iget v1, v4, Lcom/google/android/gms/car/support/d;->f:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 694
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget v3, p0, Lcom/google/android/gms/car/support/c;->i:I

    iget v5, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/n;->b(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    .line 697
    :pswitch_4
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 698
    iget v1, v4, Lcom/google/android/gms/car/support/d;->e:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 699
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget v3, p0, Lcom/google/android/gms/car/support/c;->i:I

    iget v5, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/n;->c(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    .line 702
    :pswitch_5
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 703
    iget v1, v4, Lcom/google/android/gms/car/support/d;->f:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 704
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget v3, p0, Lcom/google/android/gms/car/support/c;->i:I

    iget v5, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/n;->d(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    .line 707
    :pswitch_6
    iget-object v0, v4, Lcom/google/android/gms/car/support/d;->d:Lcom/google/android/gms/car/support/Fragment;

    .line 708
    iget v1, v4, Lcom/google/android/gms/car/support/d;->e:I

    iput v1, v0, Lcom/google/android/gms/car/support/Fragment;->G:I

    .line 709
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget v3, p0, Lcom/google/android/gms/car/support/c;->i:I

    iget v5, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/car/support/n;->e(Lcom/google/android/gms/car/support/Fragment;II)V

    goto/16 :goto_1

    .line 719
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    iget v1, v1, Lcom/google/android/gms/car/support/n;->n:I

    iget v2, p0, Lcom/google/android/gms/car/support/c;->i:I

    iget v3, p0, Lcom/google/android/gms/car/support/c;->j:I

    invoke-virtual {v0, v1, v2, v3, v8}, Lcom/google/android/gms/car/support/n;->a(IIIZ)V

    .line 722
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/c;->k:Z

    if-eqz v0, :cond_c

    .line 723
    iget-object v0, p0, Lcom/google/android/gms/car/support/c;->a:Lcom/google/android/gms/car/support/n;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/support/n;->a(Lcom/google/android/gms/car/support/c;)V

    .line 725
    :cond_c
    return-void

    .line 649
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 228
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 229
    const-string v1, "BackStackEntry{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 230
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    iget v1, p0, Lcom/google/android/gms/car/support/c;->o:I

    if-ltz v1, :cond_0

    .line 232
    const-string v1, " #"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 233
    iget v1, p0, Lcom/google/android/gms/car/support/c;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 235
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->m:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 236
    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 237
    iget-object v1, p0, Lcom/google/android/gms/car/support/c;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 240
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
