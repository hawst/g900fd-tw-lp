.class public Lcom/google/android/gms/security/verifier/InternalApkUploadService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:J


# instance fields
.field private d:Lcom/google/android/gms/security/verifier/ac;

.field private e:Z

.field private final f:Landroid/content/BroadcastReceiver;

.field private final g:Landroid/content/BroadcastReceiver;

.field private h:Lcom/google/android/gms/security/verifier/h;

.field private i:Landroid/os/HandlerThread;

.field private j:Lcom/google/android/gms/security/verifier/z;

.field private k:Lcom/google/android/gms/security/verifier/ad;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 100
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/security/verifier/ag;->b(I)I

    move-result v0

    sput v0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a:I

    .line 105
    const/16 v0, 0xf

    invoke-static {v0}, Lcom/google/android/gms/security/verifier/ag;->a(I)I

    move-result v0

    sput v0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->b:I

    .line 109
    invoke-static {}, Lcom/google/android/gms/security/verifier/ag;->c()I

    const-wide/32 v0, 0x5265c00

    sput-wide v0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->c:J

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 79
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 119
    iput-boolean v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->e:Z

    .line 120
    new-instance v0, Lcom/google/android/gms/security/verifier/aa;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/security/verifier/aa;-><init>(Lcom/google/android/gms/security/verifier/InternalApkUploadService;B)V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->f:Landroid/content/BroadcastReceiver;

    .line 122
    new-instance v0, Lcom/google/android/gms/security/verifier/ab;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/security/verifier/ab;-><init>(Lcom/google/android/gms/security/verifier/InternalApkUploadService;B)V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->g:Landroid/content/BroadcastReceiver;

    .line 691
    return-void
.end method

.method private a(Ljava/io/File;[B)I
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v6, 0x0

    .line 530
    new-instance v7, Lcom/google/android/gms/security/verifier/a;

    invoke-direct {v7, p0, p1, p2}, Lcom/google/android/gms/security/verifier/a;-><init>(Lcom/google/android/gms/security/verifier/InternalApkUploadService;Ljava/io/File;[B)V

    .line 532
    iget-object v0, v7, Lcom/google/android/gms/security/verifier/a;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/security/verifier/x;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v2, v0

    if-lez v0, :cond_1

    :cond_0
    iget-object v0, v7, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 535
    :goto_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/security/verifier/x;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v7, v0}, Lcom/google/android/gms/security/verifier/a;->a(I)I

    move-result v0

    .line 536
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1

    .line 541
    :goto_1
    return v0

    .line 532
    :cond_1
    long-to-int v0, v2

    iput v0, v7, Lcom/google/android/gms/security/verifier/a;->h:I

    :try_start_1
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, v7, Lcom/google/android/gms/security/verifier/a;->b:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    iput-object v0, v7, Lcom/google/android/gms/security/verifier/a;->i:Ljava/io/InputStream;

    const/4 v0, 0x0

    iput v0, v7, Lcom/google/android/gms/security/verifier/a;->j:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    new-instance v4, Lcom/google/android/gms/security/verifier/l;

    invoke-direct {v4}, Lcom/google/android/gms/security/verifier/l;-><init>()V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/security/verifier/l;->a:Ljava/lang/Integer;

    new-instance v0, Lcom/google/android/gms/security/verifier/n;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/n;-><init>()V

    iput-object v0, v4, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    iget-object v0, v4, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    const-string v1, ""

    iput-object v1, v0, Lcom/google/android/gms/security/verifier/n;->a:Ljava/lang/String;

    iget-object v0, v4, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    iget v1, v7, Lcom/google/android/gms/security/verifier/a;->h:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/verifier/n;->d:Ljava/lang/Long;

    iget-object v0, v4, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    new-instance v1, Lcom/google/android/gms/security/verifier/r;

    invoke-direct {v1}, Lcom/google/android/gms/security/verifier/r;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/security/verifier/n;->c:Lcom/google/android/gms/security/verifier/r;

    iget-object v0, v4, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    iget-object v0, v0, Lcom/google/android/gms/security/verifier/n;->c:Lcom/google/android/gms/security/verifier/r;

    iget-object v1, v7, Lcom/google/android/gms/security/verifier/a;->c:[B

    iput-object v1, v0, Lcom/google/android/gms/security/verifier/r;->a:[B

    iget-object v0, v4, Lcom/google/android/gms/security/verifier/l;->b:Lcom/google/android/gms/security/verifier/n;

    iget-object v1, v7, Lcom/google/android/gms/security/verifier/a;->a:Lcom/google/android/gms/security/verifier/InternalApkUploadService;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/verifier/n;->m:Ljava/lang/Long;

    new-instance v0, Lcom/google/android/gms/security/verifier/v;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/v;-><init>()V

    iput-object v0, v4, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    iget-object v0, v4, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/verifier/v;->a:Ljava/lang/Integer;

    iget-object v0, v4, Lcom/google/android/gms/security/verifier/l;->e:Lcom/google/android/gms/security/verifier/v;

    invoke-static {}, Lcom/google/android/gms/security/verifier/ag;->d()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/security/verifier/v;->c:[B

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/gms/security/verifier/a;->f:Lcom/android/volley/s;

    iget-object v8, v7, Lcom/google/android/gms/security/verifier/a;->f:Lcom/android/volley/s;

    new-instance v0, Lcom/google/android/gms/security/verifier/af;

    sget-object v1, Lcom/google/android/gms/security/verifier/x;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/security/verifier/b;

    invoke-direct {v2, v7}, Lcom/google/android/gms/security/verifier/b;-><init>(Lcom/google/android/gms/security/verifier/a;)V

    new-instance v3, Lcom/google/android/gms/security/verifier/c;

    invoke-direct {v3, v7}, Lcom/google/android/gms/security/verifier/c;-><init>(Lcom/google/android/gms/security/verifier/a;)V

    iget v5, v7, Lcom/google/android/gms/security/verifier/a;->h:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/security/verifier/af;-><init>(Ljava/lang/String;Lcom/android/volley/w;Lcom/android/volley/x;Lcom/google/android/gms/security/verifier/l;I)V

    invoke-virtual {v8, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    goto/16 :goto_0

    :catch_0
    move-exception v0

    iget-object v0, v7, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 539
    :catch_1
    move-exception v0

    const-string v0, "Upload timed out. Canceling upload"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 540
    iput-boolean v9, v7, Lcom/google/android/gms/security/verifier/a;->e:Z

    move v0, v6

    .line 541
    goto/16 :goto_1
.end method

.method static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 308
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.security.verifyapps.BOOT_COMPLETE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 309
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/gms/security/verifier/InternalApkUploadService;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 310
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 311
    return-void
.end method

.method static a(Landroid/content/Context;J)V
    .locals 5

    .prologue
    .line 299
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.security.verifyapps.PROCESS_QUEUE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 300
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/google/android/gms/security/verifier/InternalApkUploadService;

    invoke-direct {v1, p0, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 301
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    .line 302
    const-string v1, "delay"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 304
    :cond_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 305
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/security/verifier/InternalApkUploadService;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Z)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 606
    monitor-enter p0

    if-eqz p1, :cond_1

    .line 607
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->e:Z

    if-nez v0, :cond_0

    .line 608
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 610
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 611
    const-string v1, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 612
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 614
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 615
    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 616
    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 617
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 618
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 619
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 621
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 633
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 626
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->e:Z

    if-eqz v0, :cond_0

    .line 627
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 631
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->e:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 606
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(I)Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 579
    invoke-static {p0}, Lcom/google/android/gms/security/a/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/security/verifier/x;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 580
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->stopSelf(I)V

    move v0, v1

    .line 594
    :goto_0
    return v0

    .line 587
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    monitor-enter v3

    .line 588
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/h;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 589
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v2

    .line 590
    invoke-virtual {p0, p1}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->stopSelf(I)V

    .line 591
    monitor-exit v3

    move v0, v1

    goto :goto_0

    .line 593
    :cond_2
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v0, v1

    .line 594
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v2

    goto :goto_0

    .line 595
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method private a(Landroid/content/Intent;I)Z
    .locals 9

    .prologue
    const/4 v5, -0x1

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 235
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 236
    invoke-static {v1}, Lcom/google/af/c/b/a;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 237
    const-string v0, "No package name specified"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 295
    :goto_0
    return v0

    .line 241
    :cond_0
    const-string v0, "version_code"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 242
    const-string v0, "No version code specified"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 243
    goto :goto_0

    .line 245
    :cond_1
    const-string v0, "version_code"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 247
    const/4 v3, 0x0

    .line 248
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "digest"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 249
    instance-of v4, v0, [B

    if-eqz v4, :cond_4

    .line 250
    check-cast v0, [B

    move-object v3, v0

    .line 262
    :cond_2
    :goto_1
    if-eqz v3, :cond_3

    array-length v0, v3

    const/16 v4, 0x20

    if-eq v0, v4, :cond_5

    .line 263
    :cond_3
    const-string v0, "Incorrect digest length"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 264
    goto :goto_0

    .line 251
    :cond_4
    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_2

    .line 255
    :try_start_0
    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    goto :goto_1

    .line 256
    :catch_0
    move-exception v0

    const-string v1, "Provided digest is invalid"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 258
    goto :goto_0

    .line 267
    :cond_5
    const-string v0, "length"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 268
    const-string v0, "No length provided"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 269
    goto :goto_0

    .line 271
    :cond_6
    const-string v0, "length"

    invoke-virtual {p1, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 272
    sget-object v0, Lcom/google/android/gms/security/verifier/x;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v4, v0, :cond_7

    .line 273
    const-string v0, "Not uploading: apk is too large"

    new-array v1, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    move v0, v6

    .line 274
    goto :goto_0

    .line 280
    :cond_7
    invoke-direct {p0, v7}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Z)V

    .line 282
    const-string v0, "Adding apk for upload: %s:%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v0, v4}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 283
    iget-object v8, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    new-instance v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/security/verifier/ApkUploadEntry;-><init>(Ljava/lang/String;I[BJ)V

    invoke-virtual {v8, v0}, Lcom/google/android/gms/security/verifier/h;->b(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v0, v6, p2, v6}, Lcom/google/android/gms/security/verifier/z;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 293
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    sget v2, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a:I

    int-to-long v2, v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/security/verifier/z;->sendMessageDelayed(Landroid/os/Message;J)Z

    move v0, v7

    .line 295
    goto/16 :goto_0
.end method

.method private a(Landroid/content/pm/PackageInfo;[B)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 446
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    iget-object v3, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->publicSourceDir:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    if-eqz v1, :cond_2

    .line 466
    :cond_0
    :goto_1
    return v0

    :cond_1
    move v1, v0

    .line 446
    goto :goto_0

    .line 450
    :cond_2
    new-instance v1, Ljava/io/File;

    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 452
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->k:Lcom/google/android/gms/security/verifier/ad;

    iget-object v3, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    iget-wide v4, p1, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v2, v3, v4, v5, v1}, Lcom/google/android/gms/security/verifier/ad;->a(Ljava/lang/String;JLjava/io/File;)[B

    move-result-object v1

    .line 454
    if-eqz v1, :cond_0

    .line 458
    invoke-static {v1, p2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    .line 459
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 460
    goto :goto_1

    .line 462
    :catch_0
    move-exception v1

    const-string v2, "exception while reading apk"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1
.end method

.method private static a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)Z
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 499
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->e:J

    sub-long v6, v4, v6

    .line 501
    iget v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    if-nez v0, :cond_1

    .line 502
    sget-object v0, Lcom/google/android/gms/security/verifier/x;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    .line 510
    :goto_0
    cmp-long v0, v6, v4

    if-gtz v0, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->e:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long/2addr v4, v8

    sget-wide v8, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->c:J

    cmp-long v0, v4, v8

    if-lez v0, :cond_3

    move v0, v2

    :goto_1
    if-eqz v0, :cond_4

    :cond_0
    move v0, v2

    .line 512
    :goto_2
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v1

    iget-wide v4, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v2

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v1, v3, v10

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v1

    const/4 v1, 0x4

    invoke-static {v6, v7}, Lcom/google/android/gms/security/verifier/ag;->a(J)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v3, v1

    move v2, v0

    .line 516
    :goto_3
    return v2

    .line 503
    :cond_1
    iget v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    if-ne v0, v2, :cond_2

    .line 504
    sget-object v0, Lcom/google/android/gms/security/verifier/x;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    goto :goto_0

    .line 506
    :cond_2
    const-string v0, "Invalid entry state for entry id=%d: %d."

    new-array v3, v10, [Ljava/lang/Object;

    iget-wide v4, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v1

    iget v1, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v2

    invoke-static {v0, v3}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_3
    move v0, v1

    .line 510
    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/security/verifier/InternalApkUploadService;I)Z
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/security/verifier/InternalApkUploadService;Landroid/content/Intent;I)Z
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Landroid/content/Intent;I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/security/verifier/InternalApkUploadService;)Lcom/google/android/gms/security/verifier/ac;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->d:Lcom/google/android/gms/security/verifier/ac;

    return-object v0
.end method

.method protected static c()I
    .locals 1

    .prologue
    .line 651
    sget v0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->b:I

    return v0
.end method


# virtual methods
.method protected final a()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/h;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;

    .line 327
    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    .line 329
    invoke-virtual {p0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v4

    .line 330
    const/4 v1, 0x0

    .line 332
    :try_start_0
    iget-object v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v4, v6, v7}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    move-object v4, v1

    .line 337
    :goto_1
    if-nez v4, :cond_2

    .line 338
    iget v1, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    if-ne v1, v2, :cond_1

    .line 341
    const-string v1, "Package %s is no longer installed, removing from upload queue"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v6, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 343
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    .line 346
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 347
    const-string v1, "Upload for package %s has expired, removing from upload queue"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v6, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 349
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    goto :goto_0

    :catch_0
    move-exception v4

    move-object v4, v1

    goto :goto_1

    .line 356
    :cond_2
    iget v1, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    if-nez v1, :cond_5

    .line 357
    iget v1, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    iget v6, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    if-ne v1, v6, :cond_a

    .line 358
    iget-wide v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->g:J

    iget-wide v8, v4, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v1, v6, v8

    if-eqz v1, :cond_4

    .line 361
    iget-object v1, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    invoke-direct {p0, v4, v1}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Landroid/content/pm/PackageInfo;[B)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 364
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    iget-wide v6, v4, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v1, v0, v6, v7}, Lcom/google/android/gms/security/verifier/h;->b(Lcom/google/android/gms/security/verifier/ApkUploadEntry;J)V

    move v1, v2

    .line 411
    :goto_2
    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 412
    const-string v1, "Starting upload for package %s"

    new-array v6, v2, [Ljava/lang/Object;

    iget-object v7, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v7, v6, v3

    invoke-static {v1, v6}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 413
    new-instance v1, Ljava/io/File;

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v4, v4, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 414
    iget-object v4, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    invoke-direct {p0, v1, v4}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Ljava/io/File;[B)I

    move-result v1

    .line 416
    if-ne v1, v2, :cond_b

    .line 417
    const-string v1, "Upload for package %s was successful, removing from upload queue"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v6, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 420
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    goto/16 :goto_0

    .line 368
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    iget-wide v6, v4, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v1, v0, v6, v7}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;J)V

    move v1, v3

    goto :goto_2

    :cond_4
    move v1, v3

    .line 378
    goto :goto_2

    .line 381
    :cond_5
    iget v1, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    if-ne v1, v2, :cond_9

    .line 382
    iget v1, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    iget v6, v4, Landroid/content/pm/PackageInfo;->versionCode:I

    if-eq v1, v6, :cond_6

    .line 383
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    goto/16 :goto_0

    .line 386
    :cond_6
    iget-wide v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->g:J

    iget-wide v8, v4, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v1, v6, v8

    if-eqz v1, :cond_8

    .line 387
    iget-object v1, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    invoke-direct {p0, v4, v1}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Landroid/content/pm/PackageInfo;[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 390
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    goto/16 :goto_0

    .line 394
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    iget-wide v6, v4, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v1, v0, v6, v7}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;J)V

    move v1, v2

    .line 398
    goto :goto_2

    :cond_8
    move v1, v2

    .line 403
    goto :goto_2

    .line 406
    :cond_9
    const-string v1, "Invalid entry state for id=%d: %d"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    iget-wide v8, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v3

    iget v7, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {v1, v6}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_a
    move v1, v3

    goto/16 :goto_2

    .line 423
    :cond_b
    sget-object v1, Lcom/google/android/gms/security/verifier/x;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a(I)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 424
    const-string v1, "Retry attempts for package %s exceeded, removing upload"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v6, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 426
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    goto/16 :goto_0

    .line 429
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->c(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    .line 430
    const-string v1, "Upload for package %s was not successful"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v6, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 437
    :cond_d
    invoke-static {v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 438
    const-string v1, "Upload for package %s has expired, removing from upload queue"

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v6, v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v6, v4, v3

    invoke-static {v1, v4}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 440
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/h;->a(Lcom/google/android/gms/security/verifier/ApkUploadEntry;)V

    goto/16 :goto_0

    .line 443
    :cond_e
    return-void
.end method

.method final b()Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 552
    invoke-static {p0}, Lcom/google/android/gms/security/a/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 567
    :goto_0
    return v0

    .line 557
    :cond_0
    sget-object v0, Lcom/google/android/gms/security/verifier/x;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 558
    goto :goto_0

    .line 562
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->d:Lcom/google/android/gms/security/verifier/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/ac;->a()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 563
    goto :goto_0

    .line 567
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 132
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 134
    const-string v0, "connectivity"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    new-instance v1, Lcom/google/android/gms/security/verifier/ac;

    invoke-direct {v1, v0}, Lcom/google/android/gms/security/verifier/ac;-><init>(Landroid/net/ConnectivityManager;)V

    iput-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->d:Lcom/google/android/gms/security/verifier/ac;

    .line 136
    new-instance v0, Lcom/google/android/gms/security/verifier/ad;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/ad;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->k:Lcom/google/android/gms/security/verifier/ad;

    .line 138
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "apk_upload_thread"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->i:Landroid/os/HandlerThread;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 140
    new-instance v0, Lcom/google/android/gms/security/verifier/z;

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->i:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/security/verifier/z;-><init>(Lcom/google/android/gms/security/verifier/InternalApkUploadService;Landroid/os/Looper;B)V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    .line 141
    new-instance v0, Lcom/google/android/gms/security/verifier/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/security/verifier/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->h:Lcom/google/android/gms/security/verifier/h;

    .line 142
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->a(Z)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->i:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 148
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 157
    new-array v3, v1, [Ljava/lang/Object;

    if-nez p1, :cond_0

    const-string v0, "null"

    :goto_0
    aput-object v0, v3, v6

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v2

    .line 160
    invoke-static {p0}, Lcom/google/android/gms/security/a/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 161
    invoke-virtual {p0, p3}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->stopSelf(I)V

    move v0, v1

    .line 226
    :goto_1
    return v0

    .line 157
    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 166
    :cond_1
    sget-object v0, Lcom/google/android/gms/security/verifier/x;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 167
    invoke-virtual {p0, p3}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->stopSelf(I)V

    move v0, v1

    .line 169
    goto :goto_1

    .line 172
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/security/settings/b;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 173
    invoke-virtual {p0, p3}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->stopSelf(I)V

    move v0, v1

    .line 175
    goto :goto_1

    .line 178
    :cond_3
    if-nez p1, :cond_4

    const/4 v0, 0x0

    .line 180
    :goto_2
    if-nez v0, :cond_5

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v0, v2, p3, v6}, Lcom/google/android/gms/security/verifier/z;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 186
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/z;->sendMessage(Landroid/os/Message;)Z

    move v0, v2

    .line 187
    goto :goto_1

    .line 178
    :cond_4
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 190
    :cond_5
    const-string v3, "com.google.android.gms.security.verifyapps.UPLOAD_APK"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 196
    new-instance v0, Lcom/google/android/gms/security/verifier/y;

    invoke-direct {v0, p0, p1, p3}, Lcom/google/android/gms/security/verifier/y;-><init>(Lcom/google/android/gms/security/verifier/InternalApkUploadService;Landroid/content/Intent;I)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/security/verifier/y;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_3
    move v0, v2

    .line 226
    goto :goto_1

    .line 205
    :cond_6
    const-string v3, "com.google.android.gms.security.verifyapps.PROCESS_QUEUE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v0, v6, p3, v6}, Lcom/google/android/gms/security/verifier/z;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 207
    const-string v1, "delay"

    invoke-virtual {p1, v1, v8, v9}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v4

    .line 211
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v1, v6}, Lcom/google/android/gms/security/verifier/z;->removeMessages(I)V

    .line 212
    cmp-long v1, v4, v8

    if-eqz v1, :cond_7

    .line 213
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v1, v0, v4, v5}, Lcom/google/android/gms/security/verifier/z;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_3

    .line 215
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/z;->sendMessage(Landroid/os/Message;)Z

    goto :goto_3

    .line 217
    :cond_8
    const-string v3, "com.google.android.gms.security.verifyapps.BOOT_COMPLETE"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v0, v2, p3, v6}, Lcom/google/android/gms/security/verifier/z;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/z;->sendMessage(Landroid/os/Message;)Z

    goto :goto_3

    .line 221
    :cond_9
    const-string v3, "Unknown action: %s"

    new-array v4, v2, [Ljava/lang/Object;

    aput-object v0, v4, v6

    const-string v0, "ApkUploadService"

    invoke-static {v3, v4}, Lcom/google/android/gms/security/verifier/ah;->c(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v0, v1, p3, v6}, Lcom/google/android/gms/security/verifier/z;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 223
    iget-object v1, p0, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->j:Lcom/google/android/gms/security/verifier/z;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/security/verifier/z;->sendMessage(Landroid/os/Message;)Z

    goto :goto_3
.end method
