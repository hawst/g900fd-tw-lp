.class public Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ab;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field public final c:[B

.field public final d:[Lcom/google/android/gms/appdatasearch/PIMEUpdate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/appdatasearch/ab;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ab;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/ab;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->a:I

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->b:Ljava/lang/String;

    .line 44
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->c:[B

    .line 45
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->d:[Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    .line 46
    return-void
.end method

.method constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, v1, v1}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>(ILjava/lang/String;[B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V

    .line 50
    return-void
.end method

.method constructor <init>([B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>(ILjava/lang/String;[B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V

    .line 54
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 79
    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/ab;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;->CREATOR:Lcom/google/android/gms/appdatasearch/ab;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ab;->a(Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;Landroid/os/Parcel;I)V

    .line 86
    return-void
.end method
