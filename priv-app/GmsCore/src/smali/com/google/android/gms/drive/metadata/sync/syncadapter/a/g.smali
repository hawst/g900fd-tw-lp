.class final Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

.field b:Ljava/util/concurrent/Future;

.field final synthetic c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

.field private final d:Lcom/google/android/gms/drive/auth/g;

.field private final e:Lcom/google/android/gms/drive/database/model/ah;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 369
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 365
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 366
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    .line 370
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->d:Lcom/google/android/gms/drive/auth/g;

    .line 371
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->e:Lcom/google/android/gms/drive/database/model/ah;

    .line 372
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->f:Ljava/lang/String;

    .line 373
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/auth/g;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->d:Lcom/google/android/gms/drive/auth/g;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)Lcom/google/android/gms/drive/database/model/ah;
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->e:Lcom/google/android/gms/drive/database/model/ah;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 382
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/d;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)V

    return-object v0
.end method

.method final b()V
    .locals 2

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 387
    if-nez v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/a;->a(I)V

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->b:Ljava/util/concurrent/Future;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 393
    :cond_0
    return-void
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->c:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->e:Lcom/google/android/gms/drive/database/model/ah;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2, p0}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;->a(Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/g;)V

    .line 378
    return-void
.end method
