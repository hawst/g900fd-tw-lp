.class final Lcom/google/android/gms/wallet/service/ow/ab;
.super Lcom/google/android/gms/wallet/service/n;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/aa/b/a/a/a/a/i;

.field final synthetic c:Lcom/google/aa/b/a/a/a/a/b;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Landroid/util/Pair;

.field final synthetic g:Lcom/google/android/gms/wallet/service/ow/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ow/y;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/i;Lcom/google/aa/b/a/a/a/a/b;Lcom/google/android/gms/wallet/shared/ApplicationParameters;Ljava/lang/String;Landroid/util/Pair;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->g:Lcom/google/android/gms/wallet/service/ow/y;

    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/ab;->c:Lcom/google/aa/b/a/a/a/a/b;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/ab;->d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p6, p0, Lcom/google/android/gms/wallet/service/ow/ab;->e:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/wallet/service/ow/ab;->f:Landroid/util/Pair;

    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/service/n;-><init>(Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 8

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->c:Lcom/google/aa/b/a/a/a/a/b;

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/ao;->a()Lcom/google/aa/b/a/a/a/a/t;

    move-result-object v1

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->o:Lcom/google/aa/b/a/a/a/a/t;

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/ao;->a(I)Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/aa/b/a/a/a/a/i;->k:Z

    .line 196
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v1, v1, Lcom/google/aa/b/a/a/a/a/i;->j:[I

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/i;->j:[I

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->g:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->a(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/common/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/ab;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_1

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    iput-object v0, v1, Lcom/google/aa/b/a/a/a/a/i;->e:Ljava/lang/String;

    .line 208
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->g:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v1}, Lcom/google/android/gms/wallet/service/ow/y;->d(Lcom/google/android/gms/wallet/service/ow/y;)Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/aa/b/a/a/a/a/i;->n:J

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->g:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->c(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ab;->d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/ab;->f:Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/ab;->b:Lcom/google/aa/b/a/a/a/a/i;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "request"

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "otp"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, ":"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "/online-secure/v2/sdk/v1/getFullWallet?s7e=otp"

    const/16 v5, 0x10

    const-string v6, "encrypt_otp_and_get_full_wallet"

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ak;->a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Ljava/util/ArrayList;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method protected final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ab;->d:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
