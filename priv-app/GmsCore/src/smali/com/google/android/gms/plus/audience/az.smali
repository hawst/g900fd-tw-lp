.class final Lcom/google/android/gms/plus/audience/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/audience/aw;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/audience/aw;)V
    .locals 0

    .prologue
    .line 451
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onGlobalLayout()V
    .locals 2

    .prologue
    .line 454
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    .line 455
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/aw;->d(Lcom/google/android/gms/plus/audience/aw;)Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 462
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/aw;->d(Lcom/google/android/gms/plus/audience/aw;)Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/aw;->b(Lcom/google/android/gms/plus/audience/aw;I)I

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/aw;->d(Lcom/google/android/gms/plus/audience/aw;)Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getMeasuredWidth()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/audience/aw;->c(Lcom/google/android/gms/plus/audience/aw;I)I

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/aw;->e(Lcom/google/android/gms/plus/audience/aw;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    invoke-static {v1}, Lcom/google/android/gms/plus/audience/aw;->f(Lcom/google/android/gms/plus/audience/aw;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/audience/aw;->b(I)V

    .line 468
    :cond_0
    return-void

    .line 459
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/az;->a:Lcom/google/android/gms/plus/audience/aw;

    invoke-static {v0}, Lcom/google/android/gms/plus/audience/aw;->d(Lcom/google/android/gms/plus/audience/aw;)Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/AudienceSelectionScrollView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method
