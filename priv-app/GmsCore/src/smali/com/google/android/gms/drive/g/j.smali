.class public abstract enum Lcom/google/android/gms/drive/g/j;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/i;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/g/j;

.field public static final enum b:Lcom/google/android/gms/drive/g/j;

.field public static final enum c:Lcom/google/android/gms/drive/g/j;

.field private static final synthetic d:[Lcom/google/android/gms/drive/g/j;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/drive/g/k;

    const-string v1, "CURRENT_TIME"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/g/k;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/g/j;->a:Lcom/google/android/gms/drive/g/j;

    .line 30
    new-instance v0, Lcom/google/android/gms/drive/g/l;

    const-string v1, "UPTIME"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/g/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/g/j;->b:Lcom/google/android/gms/drive/g/j;

    .line 39
    new-instance v0, Lcom/google/android/gms/drive/g/m;

    const-string v1, "ELAPSED_REALTIME"

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/g/m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/g/j;->c:Lcom/google/android/gms/drive/g/j;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/drive/g/j;

    const/4 v1, 0x0

    sget-object v2, Lcom/google/android/gms/drive/g/j;->a:Lcom/google/android/gms/drive/g/j;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/google/android/gms/drive/g/j;->b:Lcom/google/android/gms/drive/g/j;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/google/android/gms/drive/g/j;->c:Lcom/google/android/gms/drive/g/j;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/g/j;->d:[Lcom/google/android/gms/drive/g/j;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;IB)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/g/j;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/g/j;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/android/gms/drive/g/j;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/j;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/g/j;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/gms/drive/g/j;->d:[Lcom/google/android/gms/drive/g/j;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/g/j;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/g/j;

    return-object v0
.end method


# virtual methods
.method public abstract a()J
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 48
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Clock[type=%s, time=%d]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/g/j;->name()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/g/j;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
