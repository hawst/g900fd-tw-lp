.class public Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field protected a:Landroid/widget/CompoundButton;

.field protected b:Lcom/google/android/location/reporting/service/l;

.field c:Z

.field private d:Lcom/google/android/gms/common/account/g;

.field private e:Ljava/util/concurrent/atomic/AtomicReference;

.field private f:[Ljava/lang/String;

.field private g:Landroid/os/AsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 51
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->e:Ljava/util/concurrent/atomic/AtomicReference;

    return-void
.end method

.method private e()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 187
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f:[Ljava/lang/String;

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_1

    .line 189
    const/4 v1, -0x1

    .line 203
    :cond_0
    :goto_0
    return v1

    .line 192
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f()Ljava/lang/String;

    move-result-object v2

    .line 194
    if-eqz v2, :cond_0

    move v0, v1

    .line 198
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f:[Ljava/lang/String;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 199
    iget-object v3, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v1, v0

    .line 200
    goto :goto_0

    .line 198
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private f()Ljava/lang/String;
    .locals 3

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 212
    :goto_0
    return-object v0

    .line 211
    :cond_0
    const-string v0, "PREFS_NAME"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 212
    const-string v1, "PREFS_KEY_ACCOUNT_NAME"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 284
    iget-boolean v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->c:Z

    if-nez v0, :cond_0

    .line 290
    :goto_0
    return-void

    .line 287
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/settings/e;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/settings/e;

    move-result-object v0

    .line 289
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "ConfirmDeletionDialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 0

    .prologue
    .line 313
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 317
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 318
    sget v1, Lcom/google/android/gms/j;->dq:I

    if-ne v0, v1, :cond_1

    .line 319
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.action.MANAGE_APPS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 320
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->e()I

    move-result v1

    .line 321
    iget-object v2, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f:[Ljava/lang/String;

    aget-object v1, v2, v1

    .line 322
    const-string v2, "com.google.android.gms.extras.ACCOUNT_NAME"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 323
    const-string v1, "com.google.android.gms.extras.PRESELECTED_FILTER"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 324
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 325
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->startActivity(Landroid/content/Intent;)V

    .line 329
    :cond_0
    :goto_0
    return-void

    .line 326
    :cond_1
    sget v1, Lcom/google/android/gms/j;->ee:I

    if-ne v0, v1, :cond_0

    .line 327
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->g()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 91
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 94
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/widget/Switch;

    invoke-direct {v0, p0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->bd:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v3, v3, v1, v3}, Landroid/widget/Switch;->setPadding(IIII)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->a:Landroid/widget/CompoundButton;

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->a:Landroid/widget/CompoundButton;

    invoke-virtual {v0, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 97
    sget v0, Lcom/google/android/gms/l;->av:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->setContentView(I)V

    .line 99
    sget v0, Lcom/google/android/gms/j;->dq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 100
    sget v1, Lcom/google/android/gms/j;->ee:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 102
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->a:Landroid/widget/CompoundButton;

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/account/h;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/account/h;-><init>(Landroid/support/v7/app/a;)V

    sget v2, Lcom/google/android/gms/p;->ep:I

    iput v2, v1, Lcom/google/android/gms/common/account/h;->a:I

    iput-object p0, v1, Lcom/google/android/gms/common/account/h;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->f()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/common/account/h;->c:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/common/account/h;->a()Lcom/google/android/gms/common/account/g;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->d:Lcom/google/android/gms/common/account/g;

    invoke-virtual {v0, v4, v4}, Landroid/support/v7/app/a;->a(II)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 108
    const-string v0, "com.google.android.gms.fitness.settings.DELETE_HISTORY"

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->g()V

    .line 111
    :cond_0
    return-void

    .line 94
    :cond_1
    new-instance v0, Landroid/widget/CheckBox;

    invoke-direct {v0, p0}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 294
    sget v0, Lcom/google/android/gms/p;->hn:I

    invoke-interface {p1, v4, v5, v4, v0}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 297
    const-string v1, "https://support.google.com/mobile/?p=google_settings_fitness"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 298
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 299
    const/high16 v1, 0x10800000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 300
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    .line 301
    if-eqz v1, :cond_0

    .line 302
    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setIntent(Landroid/content/Intent;)Landroid/view/MenuItem;

    .line 303
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 308
    :goto_0
    return v5

    .line 306
    :cond_0
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->d:Lcom/google/android/gms/common/account/g;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/account/g;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 231
    const-string v1, "PREFS_NAME"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 232
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "PREFS_KEY_ACCOUNT_NAME"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->e:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 236
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 240
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 271
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 277
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 274
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->finish()V

    .line 275
    const/4 v0, 0x1

    goto :goto_0

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 115
    invoke-super {p0}, Landroid/support/v7/app/d;->onPause()V

    .line 116
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->c:Z

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->g:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->g:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->g:Landroid/os/AsyncTask;

    .line 118
    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onRestoreInstanceState(Landroid/os/Bundle;)V

    .line 148
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->c:Z

    .line 125
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 126
    invoke-virtual {v0}, Landroid/support/v7/app/a;->b()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->w:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    .line 129
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->d:Lcom/google/android/gms/common/account/g;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 132
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->e()I

    move-result v1

    .line 134
    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 137
    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 138
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 143
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 259
    const-string v0, "FitnessSettingsActivity.onServiceConnected()"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 260
    invoke-static {p2}, Lcom/google/android/location/reporting/service/m;->a(Landroid/os/IBinder;)Lcom/google/android/location/reporting/service/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    .line 261
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 265
    const-string v0, "FitnessSettingsActivity.onServiceDisconnected()"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 266
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    .line 267
    return-void
.end method

.method protected onStart()V
    .locals 0

    .prologue
    .line 244
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 245
    invoke-static {p0, p0}, Lcom/google/android/location/reporting/service/PreferenceService;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 246
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 250
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    if-eqz v0, :cond_0

    .line 252
    invoke-virtual {p0, p0}, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->unbindService(Landroid/content/ServiceConnection;)V

    .line 253
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/settings/FitnessSettingsActivity;->b:Lcom/google/android/location/reporting/service/l;

    .line 255
    :cond_0
    return-void
.end method
