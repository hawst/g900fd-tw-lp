.class public final Lcom/google/android/gms/drive/events/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/internal/cd;


# direct methods
.method public constructor <init>(ILcom/google/android/gms/drive/internal/cd;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    iput p1, p0, Lcom/google/android/gms/drive/events/c;->a:I

    .line 53
    iput-object p2, p0, Lcom/google/android/gms/drive/events/c;->b:Lcom/google/android/gms/drive/internal/cd;

    .line 54
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 66
    instance-of v1, p1, Lcom/google/android/gms/drive/events/c;

    if-nez v1, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/events/c;

    .line 70
    iget v1, p0, Lcom/google/android/gms/drive/events/c;->a:I

    iget v2, p1, Lcom/google/android/gms/drive/events/c;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/events/c;->b:Lcom/google/android/gms/drive/internal/cd;

    invoke-interface {v1}, Lcom/google/android/gms/drive/internal/cd;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/drive/events/c;->b:Lcom/google/android/gms/drive/internal/cd;

    invoke-interface {v2}, Lcom/google/android/gms/drive/internal/cd;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 58
    iget v0, p0, Lcom/google/android/gms/drive/events/c;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 60
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/drive/events/c;->b:Lcom/google/android/gms/drive/internal/cd;

    invoke-interface {v1}, Lcom/google/android/gms/drive/internal/cd;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 61
    return v0
.end method
