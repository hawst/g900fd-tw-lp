.class public final Lcom/google/android/gms/cast_mirroring/f;
.super Lcom/google/android/gms/cast_mirroring/e;
.source "SourceFile"


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:Landroid/app/PendingIntent;

.field private final g:Lcom/google/android/gms/cast_mirroring/b/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 0

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/cast_mirroring/e;-><init>(Lcom/google/android/gms/cast/media/CastMirroringProvider;Lcom/google/android/gms/cast_mirroring/b/a;)V

    .line 99
    iput-object p4, p0, Lcom/google/android/gms/cast_mirroring/f;->e:Ljava/lang/String;

    .line 100
    iput-object p5, p0, Lcom/google/android/gms/cast_mirroring/f;->f:Landroid/app/PendingIntent;

    .line 101
    iput-object p3, p0, Lcom/google/android/gms/cast_mirroring/f;->g:Lcom/google/android/gms/cast_mirroring/b/f;

    .line 102
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast_mirroring/f;)Lcom/google/android/gms/cast_mirroring/b/f;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/f;->g:Lcom/google/android/gms/cast_mirroring/b/f;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast_mirroring/f;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/f;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast_mirroring/f;)Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/f;->f:Landroid/app/PendingIntent;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/gms/cast_mirroring/CastMirroringIntentService;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/cast_mirroring/g;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/cast_mirroring/g;-><init>(Lcom/google/android/gms/cast_mirroring/f;Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/f;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 124
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/f;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    const/16 v1, 0xe

    invoke-interface {v0, v1}, Lcom/google/android/gms/cast_mirroring/b/a;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 127
    :catch_1
    move-exception v0

    goto :goto_0
.end method
