.class abstract Lcom/google/android/gms/plus/oob/x;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/oob/e;


# instance fields
.field protected a:Ljava/lang/String;

.field protected b:Ljava/lang/String;

.field c:I


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 91
    new-instance v0, Lcom/google/android/gms/plus/oob/ad;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/plus/oob/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;
    .locals 7

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/gms/plus/oob/x;->c:I

    invoke-static {p0, p3, p2, v0}, Lcom/google/android/gms/plus/oob/UpgradeAccountInfoActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 144
    const-string v1, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/x;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 145
    const-string v1, "com.google.android.gms.common.oob.EXTRAS_CLIENT_CALLING_APP_PACKAGE"

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/x;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 147
    new-instance v6, Lcom/google/android/gms/plus/oob/b;

    invoke-direct {v6, v0, p1}, Lcom/google/android/gms/plus/oob/b;-><init>(Landroid/content/Intent;Ljava/lang/String;)V

    .line 152
    new-instance v0, Lcom/google/android/gms/plus/oob/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/oob/x;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/x;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/oob/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 158
    new-instance v1, Lcom/google/android/gms/plus/oob/a;

    invoke-direct {v1, p0, v6, v0}, Lcom/google/android/gms/plus/oob/a;-><init>(Landroid/app/Activity;Lcom/google/android/gms/plus/oob/b;Lcom/google/android/gms/plus/oob/c;)V

    return-object v1
.end method


# virtual methods
.method public a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;)Landroid/text/style/ClickableSpan;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 111
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 113
    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->l()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 115
    :cond_0
    sget-object v2, Lcom/google/android/gms/common/analytics/m;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v0

    .line 111
    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/e;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 77
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/e;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 79
    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;)Landroid/text/style/ClickableSpan;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 125
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    .line 127
    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 129
    :cond_0
    sget-object v2, Lcom/google/android/gms/common/analytics/m;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-direct {p0, p1, v1, v0, v2}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v1, v0

    .line 125
    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 85
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fl;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 87
    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    .line 85
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;)Landroid/text/style/ClickableSpan;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 97
    if-eqz p2, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 99
    :goto_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 101
    :cond_0
    const-string v1, "picasa"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/gms/common/analytics/m;->i:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    invoke-direct {p0, p1, v2, v0, v1}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    :cond_1
    move-object v2, v0

    .line 97
    goto :goto_0

    .line 101
    :cond_2
    sget-object v1, Lcom/google/android/gms/common/analytics/m;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/g;Ljava/lang/String;)Landroid/text/style/ClickableSpan;
    .locals 1

    .prologue
    .line 69
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/g;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 71
    :goto_0
    invoke-direct {p0, p1, v0, p3}, Lcom/google/android/gms/plus/oob/x;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/text/style/ClickableSpan;

    move-result-object v0

    return-object v0

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    .prologue
    .line 163
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/oob/x;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 164
    return-void
.end method

.method public a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 6

    .prologue
    .line 168
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/x;->a:Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/google/android/gms/plus/oob/x;->b:Ljava/lang/String;

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 170
    return-void
.end method

.method public b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    sget-object v0, Lcom/google/android/gms/common/analytics/m;->j:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 189
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/analytics/m;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method

.method public b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    .prologue
    .line 174
    sget-object v0, Lcom/google/android/gms/common/analytics/m;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/plus/oob/x;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 175
    return-void
.end method

.method public b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/x;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/oob/x;->b:Ljava/lang/String;

    invoke-static {p0, v0, p1, p2, v1}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 182
    return-void
.end method

.method protected final e()Z
    .locals 2

    .prologue
    .line 210
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/plus/oob/x;->c:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 215
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/x;->setResult(I)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->finish()V

    .line 217
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/x;->setResult(I)V

    .line 221
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->finish()V

    .line 222
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.OVERRIDE_THEME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/plus/oob/x;->c:I

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x103006b

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/x;->setTheme(I)V

    .line 44
    :goto_0
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/x;->requestWindowFeature(I)Z

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 48
    const-string v1, "com.google.android.gms.common.oob.EXTRAS_CLIENT_CALLING_APP_PACKAGE"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/plus/oob/x;->b:Ljava/lang/String;

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/plus/oob/x;->b:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 50
    const-string v0, "UpgradeAccount"

    const-string v1, "Required client calling package extra is unspecified"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->g()V

    .line 60
    :cond_0
    :goto_1
    return-void

    .line 43
    :cond_1
    iget v0, p0, Lcom/google/android/gms/plus/oob/x;->c:I

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/gms/q;->C:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/x;->setTheme(I)V

    goto :goto_0

    :pswitch_0
    sget v0, Lcom/google/android/gms/q;->B:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/oob/x;->setTheme(I)V

    goto :goto_0

    .line 54
    :cond_2
    const-string v1, "com.google.android.gms.common.oob.EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/oob/x;->a:Ljava/lang/String;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/x;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "UpgradeAccount"

    const-string v1, "Required account name extra is unspecified"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/plus/oob/x;->g()V

    goto :goto_1

    .line 43
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
