.class public Lcom/google/android/gms/wallet/service/CrashReportIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, "WalletCrashReport"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/CrashReportIntentService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 30
    const-string v1, "com.google.android.gms.wallet.CRASH_LOG"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 32
    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 33
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 34
    new-instance v2, Lcom/google/android/gms/wallet/shared/common/c/a;

    invoke-direct {v2, v0}, Lcom/google/android/gms/wallet/shared/common/c/a;-><init>(Landroid/content/Context;)V

    .line 35
    invoke-virtual {v2, v1}, Lcom/google/android/gms/wallet/shared/common/c/a;->a(Ljava/lang/CharSequence;)V

    .line 36
    const-string v0, "CrashReportIntentService"

    const-string v1, "Sending crash to server"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/common/c/a;->a()V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/logging/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/dynamite/logging/a;

    move-result-object v0

    .line 41
    if-nez v0, :cond_2

    .line 42
    const-string v0, "CrashReportIntentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Uncaught exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 44
    :cond_2
    const-string v1, "CrashReportIntentService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Uncaught exception:\nPackage name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/wallet/dynamite/logging/a;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/logging/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
