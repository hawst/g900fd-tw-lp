.class public final Lcom/google/android/gms/drive/api/a/o;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x6

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/o;->e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 3

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/o;->e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    const-string v1, "Invalid create request: no request"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/o;->e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid create request: no parent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/o;->e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    const-string v1, "Invalid create request: no metadata"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/o;->c:Lcom/google/android/gms/drive/c/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/o;->e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/c/a;

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/o;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/o;->e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/o;->e:Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 47
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/o;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V

    .line 48
    return-void
.end method
