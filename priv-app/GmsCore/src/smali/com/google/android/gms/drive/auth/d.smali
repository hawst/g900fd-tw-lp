.class public final Lcom/google/android/gms/drive/auth/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/auth/e;

.field public final b:Lcom/google/android/gms/drive/auth/g;

.field public final c:Lcom/google/android/gms/drive/auth/c;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/auth/c;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/e;

    iput-object v0, p0, Lcom/google/android/gms/drive/auth/d;->a:Lcom/google/android/gms/drive/auth/e;

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/drive/auth/d;->b:Lcom/google/android/gms/drive/auth/g;

    .line 22
    iput-object p3, p0, Lcom/google/android/gms/drive/auth/d;->c:Lcom/google/android/gms/drive/auth/c;

    .line 23
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/c;)Lcom/google/android/gms/drive/auth/d;
    .locals 2

    .prologue
    .line 56
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/drive/auth/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "result.isSuccess() must be false"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 58
    new-instance v0, Lcom/google/android/gms/drive/auth/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gms/drive/auth/d;-><init>(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/auth/c;)V

    return-object v0

    .line 57
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/auth/d;
    .locals 2

    .prologue
    .line 50
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/drive/auth/e;->a()Z

    move-result v0

    const-string v1, "result.isSuccess() must be true"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 52
    new-instance v0, Lcom/google/android/gms/drive/auth/d;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/drive/auth/d;-><init>(Lcom/google/android/gms/drive/auth/e;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/auth/c;)V

    return-object v0
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 103
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AuthStatus [result="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/d;->a:Lcom/google/android/gms/drive/auth/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", app="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/d;->b:Lcom/google/android/gms/drive/auth/g;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", failureCause="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/auth/d;->c:Lcom/google/android/gms/drive/auth/c;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
