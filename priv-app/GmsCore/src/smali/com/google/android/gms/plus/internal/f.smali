.class public interface abstract Lcom/google/android/gms/plus/internal/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(ILandroid/os/Bundle;)V
.end method

.method public abstract a(ILandroid/os/Bundle;I)V
.end method

.method public abstract a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/acls/AclsResponse;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Settings;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Ljava/lang/String;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
.end method

.method public abstract a(ILandroid/os/Bundle;Ljava/util/List;)V
.end method

.method public abstract a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract b(ILandroid/os/Bundle;Landroid/os/Bundle;)V
.end method
